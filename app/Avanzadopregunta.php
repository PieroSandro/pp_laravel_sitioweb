<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Avanzadopregunta extends Model
{
    //
    protected $table = "avanzadopreguntas";
    protected $fillable=[
        'question','option1', 'option2', 'option3', 'option4', 'answer',
    ];
    //
    protected $primarykey="id";

    

    public $timestamps=true;
}