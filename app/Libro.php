<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{
    //
    protected $table = "libros";
    //
    protected $fillable=[
        'titulo','body', 'link',
    ];
    protected $primarykey="id";

    public $timestamps=true;
    
}