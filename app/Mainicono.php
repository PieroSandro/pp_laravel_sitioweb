<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mainicono extends Model
{
    //
    protected $table = "mainiconos";

    protected $fillable=[
        'foto'
    ];
    //
    protected $primarykey="id";

    

    public $timestamps=true;
}