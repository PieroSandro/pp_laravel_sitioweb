<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Main2page extends Model
{
    //
    protected $table = "main2pages";

    protected $fillable=[
        'body_es','foto_es','body_en','foto_en','si_primero_foto',
    ];
    //
    protected $primarykey="id";

    

    public $timestamps=true;
}