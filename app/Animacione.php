<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Animacione extends Model
{
    //
    public $table = "animaciones";
    protected $fillable=[   
        'titulo','efecto_id',
    ];
    //
    //protected $primarykey="id";

    
    
    /*public function efecto()
{
   return $this->hasOne('App\Efecto', 'nombre', 'efecto_id');
}*/

public function efecto()
{
    return $this->belongsTo('App\Efecto', 'efecto_id');
    //return $this->belongsTo(Efecto::class);
}

public $timestamps=false;

}