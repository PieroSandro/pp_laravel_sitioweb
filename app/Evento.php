<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    public $table = "eventos";

    protected $fillable = [
    	'curso_id','titulo','fecha_inicio','fecha_final','color',
    ];

    public function Curso() {
        return $this->belongsTo('App\Curso', 'curso_id');
    }

    public $timestamps = false;
}
