<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chatbot extends Model
{
    //
    protected $table = "chatbots";
    protected $fillable=[
        'palabraclave','respuesta',
    ];
    //
    protected $primarykey="id";

    

    public $timestamps=true;
}