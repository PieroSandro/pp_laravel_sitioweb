<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfesorCurso extends Model
{
     protected $table = 'profesorcurso';

    protected $fillable = [
       'persona_id','curso_id',
    ];

     public function Persona(){
    	return $this->belongTo('App\Persona', 'persona_id');
    }

    public function Curso(){
    	return $this->belongTo('App\Curso', 'curso_id');
    }
}
