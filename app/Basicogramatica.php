<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Basicogramatica extends Model
{
    //
    protected $table = "basicogramaticas";
    protected $fillable=[
        'foto','link','body',
    ];
    //
    protected $primarykey="id";

    

    public $timestamps=true;
}