<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Avanzadogramatica extends Model
{
    //
    protected $table = "avanzadogramaticas";
    protected $fillable=[
        'foto','link','body',
    ];
    //
    protected $primarykey="id";

    

    public $timestamps=true;
}