<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vcurso extends Model
{
    //
    protected $table = "vcursos";
    protected $fillable=[
        'titulo','body', 'link',
    ];
    //
    protected $primarykey="id";

    

    public $timestamps=true;
}