<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mainpage extends Model
{
    //
    protected $table = "mainpages";

    protected $fillable=[
        'body_es','foto_es','body_en','foto_en',
    ];
    //
    protected $primarykey="id";

    

    public $timestamps=true;
}