<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    //
    protected $table = "videos";
    //
    protected $fillable=[
        'vid', 'link',
    ];
    protected $primarykey="id";

    public $timestamps=true;
    
}