<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SendContactNotification2 extends Notification
{
    use Queueable;

    protected $request;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        //
        $this->request=$request;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject("Libro de reclamaciones")
                    ->greeting('Hola tienes el siguiente un mensaje:')
                    ->line('Menor de edad: '. $this->request->si_menor_edad)
                    ->line('Nombre: '. $this->request->name)
                    ->line('Apellido paterno: '. $this->request->apellidop)
                    ->line('Apellido materno: '. $this->request->apellidom)
                    ->line('Distrito: '. $this->request->distrito)
                    ->line('Teléfono: '. $this->request->telefono)
                    ->line('Tipo de documento: '. $this->request->t_documento)
                    ->line('Nro. de documento: '. $this->request->n_documento)
                    ->line('Tipo de reclamo: '. $this->request->t_reclamo)
                    ->line('Aqui esta su mensaje:')
                    

                    ->line($this->request->message)
                    ->action('Reply', url('mailto:' . $this->request->email));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
