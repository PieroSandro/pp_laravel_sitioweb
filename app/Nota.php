<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nota extends Model {

    protected $table = 'notas';

    protected $fillable = [
       'curso_id','registro_id','nota1','nota2','nota3','nota4','nota5','nota6','nota7','nota8','pf1','pf2','estado_nota','estado_periodo','prueba_en','prueba_sa',
    ];

    public function Curso() {
        return $this->belongsTo('App\Curso', 'curso_id');
    }
    

}
