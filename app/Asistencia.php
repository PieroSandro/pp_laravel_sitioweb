<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asistencia extends Model {

    protected $table = 'asistencias';

    protected $fillable = [
       'registro_id','turno_id', 'estado','fecha',
    ];

    
    public function Registro(){
    	return $this->belongsTo('App\Registro', 'registro_id');
    }

    public function Turno(){
    	return $this->belongsTo('App\Turno', 'turno_id');
    }

}
