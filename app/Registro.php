<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registro extends Model
{
    protected $table = 'registros';

    protected $fillable = [
       'user_id','carrera_id','curso_id','ciclo_id','periodo_id','turno_id',
    ];


    public function Persona() {
        return $this->belongsTo('App\Persona', 'user_id');
    }

    public function Carrera() {
        return $this->belongsTo('App\Carrera', 'carrera_id');
    }
     public function Curso() {
        return $this->belongsTo('App\Curso', 'curso_id');
    }
     public function Ciclo() {
        return $this->belongsTo('App\Ciclo', 'ciclo_id');
    }
     public function Periodo() {
        return $this->belongsTo('App\Periodo', 'periodo_id');
    }

     public function Turno() {
        return $this->belongsTo('App\Turno', 'turno_id');
    }

    
}
