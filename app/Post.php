<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    protected $table = "posts";
    //
    protected $primarykey="id";

    public $timestamps=true;
    
    public function comentarios()
    {
    return $this->hasMany('App\Comentario');
    }
    
    public function comentariosCount(){
        return $this->comentarios->count();
    }
}
