<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    //
    protected $table = "comentarios";
    //
    protected $primarykey="id";

    public $timestamps=true;
    
    public function respuestas()
    {
    return $this->hasMany('App\Respuesta');
    }

    public function post()
    {
      return $this->belongsTo('App\Post');
    }
    
    public function respuestasCount(){
      return $this->respuestas->count();
  }
}
