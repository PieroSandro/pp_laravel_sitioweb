<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mainword extends Model
{
    //
    protected $table = "mainwords";

    protected $fillable=[
        'titulo','body',
    ];
    //
    protected $primarykey="id";

    

    public $timestamps=true;
}