<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
   protected $table = 'personas';

   protected $fillable = [
       'cod_p','nombre','apellidos','dni','edad','fecha_n','sexo','direccion','foto','rol_id',
    ];

    public function Rol(){
        return $this->belongsTo('App\Rol', 'rol_id');
    }
}
