<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Silabus extends Model {

    protected $table = 'silabus';

    protected $fillable = [
        'curso_id','carrera_id','ciclo_id','periodo_id','nombre','nombre_original','archivo',
    ];

    public function Curso() {
        return $this->belongsTo('App\Curso', 'curso_id');
    }

    public function Carrera() {
        return $this->belongsTo('App\Carrera', 'carrera_id');
    }

    public function Ciclo() {
        return $this->belongsTo('App\Ciclo', 'ciclo_id');
    }

    public function Periodo() {
        return $this->belongsTo('App\Periodo', 'periodo_id');
    }
    

}
