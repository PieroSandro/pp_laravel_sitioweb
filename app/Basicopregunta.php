<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Basicopregunta extends Model
{
    //
    protected $table = "basicopreguntas";
    protected $fillable=[
        'question','option1', 'option2', 'option3', 'option4', 'answer',
    ];
    //
    protected $primarykey="id";

    

    public $timestamps=true;
}