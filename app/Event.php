<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
    protected $table = "events";

    protected $fillable=[
        'foto','titulo','body','link',
    ];
    //
    protected $primarykey="id";

    

    public $timestamps=true;
}