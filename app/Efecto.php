<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Efecto extends Model
{
    //
    protected $table = "efectos";
    //
    protected $primarykey="id";

    //public $timestamps=true;

    protected $fillable = [
        'nombre',
    ];

    public function Animacione()
{
    return $this->hasMany('App\Animacione');

}
}