<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quizresultado extends Model
{
    //
    protected $table = "quizresultados";
    protected $fillable=[
        'numres',
    ];
    //
    protected $primarykey="id";

    

    public $timestamps=true;
}