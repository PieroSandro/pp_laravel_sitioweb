<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Respuesta extends Model
{
    //
    protected $table = "respuestas";
    //
    protected $primarykey="id";

    public $timestamps=true;
    
    /*protected $fillable = [
      'contenido','comentario_id',
   ];*/

    public function comentario()
    {
      return $this->belongsTo('App\Comentario');
    }
    
    
}