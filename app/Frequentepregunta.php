<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Frequentepregunta extends Model
{
    //
    protected $table = "frequentepreguntas";
    protected $fillable=[
        'pregunta_es','respuesta_es', 'pregunta_en', 'respuesta_en',
    ];
    //
    protected $primarykey="id";

    

    public $timestamps=true;
}