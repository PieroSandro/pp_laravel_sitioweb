<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App;
use Config;

class SetLang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::has("locale")) {
            $locale = Session::get("locale");
        } else {
            // check browser lang - https://learninglaravel.net/detect-and-change-language-on-the-fly-with-laravel
            $locale = substr($request->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
 
            if ($locale != 'es' && $locale != 'en') {
                $locale = 'en';
            }
        }
 
        App::setLocale($locale);
        return $next($request);
    }
}
