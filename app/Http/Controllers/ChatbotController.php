<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;


Use Faker\Provider\Image;
use Illuminate\Support\Facades\Hash;

use App\Chatbot;
use App;
Use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

Use Illuminate\Support\Facades\Storage;
use PDF;
Use Illuminate\Http\Response;
use Auth;

use Validator;

use App\Notifications\SendContactNotification;
use Illuminate\Support\Facades\Notification;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class ChatbotController extends Controller
{
    
    
    public function updatechatbot($id,Request $request)
    {
        
    $chatbot = Chatbot::findOrFail($request->id);
    $chatbot->update($request->all());
    $locale=App::getLocale();
    $userna = $request->input('idc');

    //$titulocurso = $request->input('titulo');
    $palabraclave = $request->input('palabraclave');
    $respuesta = $request->input('respuesta');

    if(App::isLocale('en')){
    Session::flash('success','The row '.$userna.' was updated!<br><br>ID: '.$request->id.'<br>Keyword: '.$palabraclave.'<br>Answer: '.$respuesta);
        return back();
    }else{
        Session::flash('success','¡La fila '.$userna.' fue actualizada!<br><br>ID: '.$request->id.'<br>Palabra clave: '.$palabraclave.'<br>Respuesta: '.$respuesta);
        return back();

    }
    
    }



    public function destroychatbot(Request $request)
    {
        
        $chatbot = Chatbot::findOrFail($request->id);
       /* $titulocurso=$vcurso->titulo;
        $bodycurso=$vcurso->body;
        $linkcurso=$vcurso->link;*/
        $palabraclave=$chatbot->palabraclave;
       
        $respuesta=$chatbot->respuesta;

        $chatbot->delete();
        $locale=App::getLocale();
        //--------------------------
        $userna = $request->input('idc2');
        if(App::isLocale('en')){
            Session::flash('success','The row '.$userna.' was deleted!<br><br>ID: '.$request->id.'<br>Keyword: '.$palabraclave.'<br>Answer: '.$respuesta.'<br>');
        }else{
        Session::flash('success','¡La fila '.$userna.' fue eliminada!<br><br>ID: '.$request->id.'<br>Palabra clave: '.$palabraclave.'<br>Respuesta: '.$respuesta.'<br>');}

    
        return back();
        
        }

    public function destroychatbotmore(Request $request)
        {
            
            $delid=$request->input('delid');
            $ccc = $request->input('ccc');

            $counterdeletevcmore=1;
            $counterrowsdelete=' ';

            if(App::isLocale('en')){  
            foreach($delid as $del)
            {
                $chatbot = Chatbot::find($del);
                $palabraclave=$chatbot->palabraclave;
              
                $respuesta=$chatbot->respuesta;
               
                $counterrowsdelete=$counterrowsdelete.$counterdeletevcmore.') ID: '.$del.'<br>Keyword: '.$palabraclave.'<br>Answer: '.$respuesta.'<br>';
                $counterdeletevcmore++;
            }
            $counterdeletevcmore=$counterdeletevcmore-1;

            if($counterdeletevcmore==1){
                Chatbot::whereIn('id',$delid)->delete();
                Session::flash('success','There is '.$counterdeletevcmore.' record deleted.<br><br><br>'.$counterrowsdelete);
               
            }else{  
                Chatbot::whereIn('id',$delid)->delete();  
             Session::flash('success','There are '.$counterdeletevcmore.' records deleted.<br><br><br>'.$counterrowsdelete);
             
            }

            return back();
        }else{

            foreach($delid as $del)
            {
                $chatbot = Chatbot::find($del);
                $palabraclave=$chatbot->palabraclave;
              
                $respuesta=$chatbot->respuesta;
               
                $counterrowsdelete=$counterrowsdelete.$counterdeletevcmore.') ID: '.$del.'<br>Palabra clave: '.$palabraclave.'<br>Respuesta: '.$respuesta.'<br>';
                $counterdeletevcmore++;
            }
         
            
            $counterdeletevcmore=$counterdeletevcmore-1;

            if($counterdeletevcmore==1){
                Chatbot::whereIn('id',$delid)->delete();
                Session::flash('success','Se ha eliminado '.$counterdeletevcmore.' registro.<br><br><br>'.$counterrowsdelete);
               
            }else{  
                Chatbot::whereIn('id',$delid)->delete();  
             Session::flash('success','Se han eliminado '.$counterdeletevcmore.' registros.<br><br><br>'.$counterrowsdelete);
             
            }

            return back();






        }
            }
    
}