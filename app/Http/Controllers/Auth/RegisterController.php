<!--

php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Alumno;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/configuracion';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        
        return Validator::make($data, [
            'cod_al' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:5', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $cod_al = $data['cod_al'];
        $existe = Alumno::select('id')->where('cod_al',$cod_al)->exists();
        $id = Alumno::where('cod_al',$cod_al)->value('id');
        
        if ($existe) {
            return User::create([
            'cod_al' => $id,
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
           
        }else{
            return redirect()->route('register')->with(['message' => 'Lo sentimos: Usted no esta registrado como alumno en el sistema']);
        }
        
    }
}
-->
