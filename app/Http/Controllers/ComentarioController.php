<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App;
use App\Post;
use App\Categoria;
use App\Comentario;
use App\Respuesta;
use App\User;
use Auth;
use Validator;

use App\Notifications\SendContactNotification;
use Illuminate\Support\Facades\Notification;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;
class ComentarioController extends Controller {

    


   

    public function destroy($post_id,$id)
    {
        $id_post = $post_id;
        $comentario = Comentario::find($id);
        
        $comentario->delete();
        return redirect('/blog/home/'.$id_post)->with('success','¡Comentario borrado!');
        
       
    }

    /*public function destroy($id,$post_id)
    {
        $post=Post::find($post_id);
	
        $comentario = Comentario::find($id);
        $comentario->post()->associate($post);
        $comentario->delete();

	Session::flash('success','El comentario fue eliminado');
        

        return View('front.blog2.show')->with('post',$post);
        
       
    }*/
//-----------------------------------------------------------------------------------------------
   
    public function store(Request $request,$post_id, $comentario_id)//,$post_id
    {
       
        
        $this->validate($request,array('nick'=>'nullable','contenido'=>'required|min:5|max:2000'));

        
        //$post=Post::find($post_id);

        $id_post = $post_id;
        $comentario=Comentario::find($comentario_id);
        $respuesta = new Respuesta();
        
        $respuesta->contenido = $request->contenido;
        
        
        $respuesta->comentario()->associate($comentario);
        //$comentario->post()->associate($post);
        $respuesta->save();

        
        Session::flash('success','La respuesta fue añadida');
        

        //return View('front.blog2.show')->with('post',$post);

        return redirect('/blog/home/'.$id_post)->with(['success'=>'Respuesta agregado con exito']);
    }

}