<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;


Use Faker\Provider\Image;
use Illuminate\Support\Facades\Hash;

use App\Libro;
use App;
Use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

Use Illuminate\Support\Facades\Storage;
use PDF;
Use Illuminate\Http\Response;
use Auth;

use Validator;

use App\Notifications\SendContactNotification;
use Illuminate\Support\Facades\Notification;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class LibroController extends Controller
{
    
    
    public function updatelibro($id,Request $request)
    {
        
    $libro = Libro::findOrFail($request->id);
    
    $libro->update($request->all());
    $locale=App::getLocale();
    $userna = $request->input('idc');

    $titulolibro=$request->input('titulo');

    $bodylibro=$request->input('body');

    $linklibro=$request->input('link');
    
       if(App::isLocale('en')){
        Session::flash('success','The record in the row '.$userna.' was updated!<br><br>ID: '.$request->id.'<br>Title: '.$titulolibro.'<br>Body: '.$bodylibro.'<br>Link: '.$linklibro);
    }else{
    Session::flash('success','¡El registro de la fila '.$userna.' fue actualizado!<br><br>ID: '.$request->id.'<br>Título: '.$titulolibro.'<br>Contenido: '.$bodylibro.'<br>Enlace: '.$linklibro);}


    return back();
        }



    public function destroylibro(Request $request)
    {
        
        $libro = Libro::findOrFail($request->id);
        $titulolibro=$libro->titulo;
        $bodylibro=$libro->body;
        $linklibro=$libro->link;

        $libro->delete();
        $locale=App::getLocale();
        //--------------------------
        $userna = $request->input('idc2');
        if(App::isLocale('en')){
            Session::flash('success','The book in the row '.$userna.' was deleted!<br><br>Id: '.$request->id.'<br>Title: '.$titulolibro.'<br>Body: '.$bodylibro.'<br>Link: '.$linklibro.'<br>');
        }else{
        Session::flash('success','¡El libro de la fila '.$userna.' fue eliminado!<br><br>Id: '.$request->id.'<br>Título: '.$titulolibro.'<br>Contenido: '.$bodylibro.'<br>Enlace: '.$linklibro.'<br>');}

    
        return back();
        
        }

    public function destroylibromore(Request $request)
        {
             
            $delid=$request->input('delid');
            $ccc = $request->input('ccc');

            $counterdeletelimore=1;
            $counterrowsdelete=' ';

            if(App::isLocale('en')){  
            foreach($delid as $del)
            {
                $libro = Libro::find($del);
                $titulolibro=$libro->titulo;
                $bodylibro=$libro->body;
                $linklibro=$libro->link;

                $counterrowsdelete=$counterrowsdelete.$counterdeletelimore.') ID: '.$del.'<br>Title: '.$titulolibro.'<br>Body: '.$bodylibro.'<br>Link: '.$linklibro."<br>";
                $counterdeletelimore++;
            }
            $counterdeletelimore=$counterdeletelimore-1;

            if($counterdeletelimore==1){
                Libro::whereIn('id',$delid)->delete();
                Session::flash('success','There is '.$counterdeletelimore.' record deleted.<br><br><br>'.$counterrowsdelete);
               
            }else{  
                Libro::whereIn('id',$delid)->delete();  
             Session::flash('success','There are '.$counterdeletelimore.' records deleted.<br><br><br>'.$counterrowsdelete);
             
            }

            return back();
        }else{

            foreach($delid as $del)
            {
                $libro = Libro::find($del);
                $titulolibro=$libro->titulo;
                $bodylibro=$libro->body;
                $linklibro=$libro->link;

                $counterrowsdelete=$counterrowsdelete.$counterdeletelimore.') ID: '.$del.' Título: '.$titulolibro.'<br>Contenido: '.$bodylibro.'<br>Enlace: '.$linklibro."<br>";
                $counterdeletelimore++;
            }
            $counterdeletelimore=$counterdeletelimore-1;

            if($counterdeletelimore==1){
                Libro::whereIn('id',$delid)->delete();
                Session::flash('success','Se ha eliminado '.$counterdeletelimore.' registro.<br><br><br>'.$counterrowsdelete);
               
            }else{  
                Libro::whereIn('id',$delid)->delete();  
             Session::flash('success','Se han eliminado '.$counterdeletelimore.' registros.<br><br><br>'.$counterrowsdelete);
             
            }

            return back();
        }
           
            }
    
}