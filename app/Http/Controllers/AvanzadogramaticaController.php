<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;


Use Faker\Provider\Image;
use Illuminate\Support\Facades\Hash;
use App;
use App\Avanzadogramatica;

Use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

Use Illuminate\Support\Facades\Storage;
use PDF;
Use Illuminate\Http\Response;
use Auth;

use Validator;

use App\Notifications\SendContactNotification;
use Illuminate\Support\Facades\Notification;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class AvanzadogramaticaController extends Controller
{
    
 

    public function updateavanzadogramatica($id,Request $request)
    {
        $this->validate($request,[
            'link' => 'required',
            'body' => 'required'
          ]);
          
          if($request->hasFile('foto')){
            $filenameWithExt=$request->file('foto')->getClientOriginalName();
            $filename=pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension=$request->file('foto')->getClientOriginalExtension();
            $fileNameToStore=$filename.'_'.time().'.'.$extension;
            $path=$request->file('foto')->storeAs('public/images',$fileNameToStore);
        }


          $avanzadogramatica = Avanzadogramatica::find($id);
          $avanzadogramatica->link = $request->input('link');
          $avanzadogramatica->body = $request->input('body');
          if($request->hasFile('foto')){
              $avanzadogramatica->foto=$fileNameToStore;
          }
          $avanzadogramatica->save();
          $locale=App::getLocale();
          if(App::isLocale('en')){
              Session::flash('success','The topic was updated!');
          }else{
          Session::flash('success','¡El tema fue actualizado!');}
          return back()->with('avanzadogramatica',$avanzadogramatica);
    }



    public function destroyavanzadogramatica(Request $request)
    {
        
        $avanzadogramatica = Avanzadogramatica::findOrFail($request->id);
        $linkavanzadogramatica=$avanzadogramatica->link;
        $bodyavanzadogramatica=$avanzadogramatica->body;
        Storage::delete('public/images/'.$avanzadogramatica->foto);
        $avanzadogramatica->delete();
        $locale=App::getLocale();
        $userna = $request->input('idc2');
        if(App::isLocale('en')){
            Session::flash('success','The topic in the row '.$userna.' was deleted!<br><br>Id: '.$request->id.'<br>Photo: '.$avanzadogramatica->foto.'<br>Link: '.$linkavanzadogramatica.'<br>Body: '.$bodyavanzadogramatica.'<br>');
        }else{
        Session::flash('success','¡El tema de la fila '.$userna.' fue eliminado!<br><br>Id: '.$request->id.'<br>Foto: '.$avanzadogramatica->foto.'<br>Enlace: '.$linkavanzadogramatica.'<br>Contenido: '.$bodyavanzadogramatica.'<br>');}
        
        return back()->with('avanzadogramatica',$avanzadogramatica);
        
        }

            public function destroyavanzadogramaticamore(Request $request)
            {
                $delid=$request->input('delid');
                $counterdeletebamore=1;
            $counterrowsdelete=' ';
            if(App::isLocale('en')){      
            //----------------------------------------------------------------------
                foreach($delid as $del)
        {
        $avanzadogramatica = Avanzadogramatica::find($del);
        $linkavanzadogramatica=$avanzadogramatica->link;
        $bodyavanzadogramatica=$avanzadogramatica->body;
        Storage::delete('public/images/'.$avanzadogramatica->foto);
       
        $counterrowsdelete=$counterrowsdelete.$counterdeletebamore.') ID: '.$del.'<br>Photo: '.$avanzadogramatica->foto.'<br>Link: '.$avanzadogramatica->link.'<br>Body: '.$avanzadogramatica->body."<br><br>";        
        $counterdeletebamore++;
        }
        $counterdeletebamore=$counterdeletebamore-1;

        if($counterdeletebamore==1){
                Avanzadogramatica::whereIn('id',$delid)->delete();
                Session::flash('success','There is '.$counterdeletebamore.' record deleted.<br><br><br>'.$counterrowsdelete);
        }else{
            Avanzadogramatica::whereIn('id',$delid)->delete();
            Session::flash('success','There are '.$counterdeletebamore.' records deleted.<br><br><br>'. $counterrowsdelete);

        }

                return back();
                //---------------------------------------------------------------
    }else{

         //----------------------------------------------------------------------
         foreach($delid as $del)
         {  
         $avanzadogramatica = Avanzadogramatica::find($del);
         $linkavanzadogramatica=$avanzadogramatica->link;
         $bodyavanzadogramatica=$avanzadogramatica->body;
         Storage::delete('public/images/'.$avanzadogramatica->foto);
       
         $counterrowsdelete=$counterrowsdelete.$counterdeletebamore.') ID: '.$del.'<br>Foto: '.$avanzadogramatica->foto.'<br>Enlace: '.$avanzadogramatica->link.'<br>Contenido: '.$avanzadogramatica->body."<br><br>";        
        $counterdeletebamore++;
         }
         $counterdeletebamore=$counterdeletebamore-1;

         if($counterdeletebamore==1){
            Avanzadogramatica::whereIn('id',$delid)->delete();
            Session::flash('success','Se ha eliminado '.$counterdeletebamore.' registro.<br><br><br>'.$counterrowsdelete);
         }else{
            Avanzadogramatica::whereIn('id',$delid)->delete();
            Session::flash('success','Se han eliminado '.$counterdeletebamore.' registros.<br><br><br>'. $counterrowsdelete);

         }

                 return back();
                 //---------------------------------------------------------------
    }

            }
}
