<?php

namespace App\Http\Controllers;
//13-2-2020
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Mail\sendingmail;

//13-2-2020
use Mail;
use App;
use Session;
use Illuminate\Http\Request;
use App\Post;
use App\Categoria;
use App\Comentario;
use App\User;
use Auth;
use Validator;

use App\Notifications\SendContactNotification;
use Illuminate\Support\Facades\Notification;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;
class ContactenosController extends Controller {
	public function contactenossend(Request $request){
        $this->validate($request,[
            'name'=>'required',
            'email'=>'required|email',
            'message'=>'required'
        ]);


        //14_2_2020
        $data=array(
            'name'=>$request->name,
            'email'=>$request->email,
           
            'bodymessage'=>$request->message
        );

        Mail::send('front.contactenos2',$data,function($message) use ($data){
            $message->from($data['email']);
            $message->to('piero.condor26@gmail.com');
            $message->subject($data['name']);
        });


        //14-2-2020
        $locale=App::getLocale();
            /*
        Notification::route('mail','istccueto@gmail.com')
                        ->notify(new SendContactNotification($request));*/

                        if(App::isLocale('en')){
                            Session::flash('success','The e-mail was sent successfully');
        
                            return redirect()->route('contactenos')->with('success','The e-mail was sent!');
                    
                        }else{
                            Session::flash('success','El correo fue enviado exitosamente');
        
                            return redirect()->route('contactenos')->with('success','¡El correo fue enviado!');
                    
                        }
                        
        
    } 
}
