<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App;

Use Faker\Provider\Image;
use Illuminate\Support\Facades\Hash;

use App\Event;

Use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

Use Illuminate\Support\Facades\Storage;
use PDF;
Use Illuminate\Http\Response;
use Auth;

use Validator;

use App\Notifications\SendContactNotification;
use Illuminate\Support\Facades\Notification;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class EventController extends Controller
{
    
    

        public function updateevent($id,Request $request)
    {
        $this->validate($request,[
            'titulo' => 'required',
            'body' => 'required',
            'link' => 'required'
          ]);
          
          if($request->hasFile('foto')){
            $filenameWithExt=$request->file('foto')->getClientOriginalName();
            $filename=pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension=$request->file('foto')->getClientOriginalExtension();
            $fileNameToStore=$filename.'_'.time().'.'.$extension;
            $path=$request->file('foto')->storeAs('public/images',$fileNameToStore);
        }


          //Create Post
          $event = Event::find($id);
          $event->link = $request->input('link');
          $event->titulo = $request->input('titulo');
          $event->body = $request->input('body');

          if($request->hasFile('foto')){
              $event->foto=$fileNameToStore;
          }
          $event->save();
          //Session::flash('success','¡El event fue actualizado!');
          $locale=App::getLocale();
          if(App::isLocale('en')){
              Session::flash('success','The event was updated');   
              ?>    
          <?php 
          }else{
          Session::flash('success','El evento fue actualizado');}
          //return view('front.blog2.editevent')->with('event',$event);
          return back()->with('event',$event);
    }




        public function destroyevent(Request $request)
    {
        
        $event = Event::findOrFail($request->id);
        if($event->foto != 'noimage.jpg'){
            Storage::delete('public/images/'.$event->foto);
        }
        $event->delete();

        return back();
        
        }

    public function destroyeventmore(Request $request)
        {
            $delid=$request->input('delid');
            //$vcurso = Vcurso::findOrFail($request->id);
            //$vcurso->delete();
            //$ = $request->input('eventimg');
            foreach($delid as $del)
    {
        //$file = File::find($id)->first();
        //$path = public_path().'/rea-files/'.$file->rea_number.'/'.$file->file_name;
        //\File::delete($path);
       // echo 'id';
       //if($event->foto != 'noimage.jpg'){
       //--------------------- Storage::delete('public/images/'.$event->foto);
       //--------------------------------Storage::delete($id);
    //}
    $event = Event::find($del);
    //$path = 'public/images/'.$event->foto;
    Storage::delete('public/images/'.$event->foto);
    //Event::delete($path);
    }

    //Event::delete($ids);
    

            Event::whereIn('id',$delid)->delete();
            Session::flash('success','¡El evento fue eliminado!');
            return back();
            
            }


}
