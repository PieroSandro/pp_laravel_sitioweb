<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;


Use Faker\Provider\Image;
use Illuminate\Support\Facades\Hash;
use App;
use App\Intermediogramatica;

Use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

Use Illuminate\Support\Facades\Storage;
use PDF;
Use Illuminate\Http\Response;
use Auth;

use Validator;

use App\Notifications\SendContactNotification;
use Illuminate\Support\Facades\Notification;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class IntermediogramaticaController extends Controller
{
    
 

    public function updateintermediogramatica($id,Request $request)
    {
        $this->validate($request,[
            'link' => 'required',
            'body' => 'required'
          ]);
          
          if($request->hasFile('foto')){
            $filenameWithExt=$request->file('foto')->getClientOriginalName();
            $filename=pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension=$request->file('foto')->getClientOriginalExtension();
            $fileNameToStore=$filename.'_'.time().'.'.$extension;
            $path=$request->file('foto')->storeAs('public/images',$fileNameToStore);
        }


          $intermediogramatica = Intermediogramatica::find($id);
          $intermediogramatica->link = $request->input('link');
          $intermediogramatica->body = $request->input('body');
          if($request->hasFile('foto')){
              $intermediogramatica->foto=$fileNameToStore;
          }
          $intermediogramatica->save();
          $locale=App::getLocale();
          if(App::isLocale('en')){
              Session::flash('success','The topic was updated!');
          }else{
          Session::flash('success','¡El tema fue actualizado!');}
          return back()->with('intermediogramatica',$intermediogramatica);
    }



    public function destroyintermediogramatica(Request $request)
    {
        
        $intermediogramatica = Intermediogramatica::findOrFail($request->id);
        $linkintermediogramatica=$intermediogramatica->link;
        $bodyintermediogramatica=$intermediogramatica->body;
        Storage::delete('public/images/'.$intermediogramatica->foto);
        $intermediogramatica->delete();
        $locale=App::getLocale();
        $userna = $request->input('idc2');
        if(App::isLocale('en')){
            Session::flash('success','The topic in the row '.$userna.' was deleted!<br><br>Id: '.$request->id.'<br>Photo: '.$intermediogramatica->foto.'<br>Link: '.$linkintermediogramatica.'<br>Body: '.$bodyintermediogramatica.'<br>');
        }else{
        Session::flash('success','¡El tema de la fila '.$userna.' fue eliminado!<br><br>Id: '.$request->id.'<br>Foto: '.$intermediogramatica->foto.'<br>Enlace: '.$linkintermediogramatica.'<br>Contenido: '.$bodyintermediogramatica.'<br>');}
        
        return back()->with('intermediogramatica',$intermediogramatica);
        
        }

            public function destroyintermediogramaticamore(Request $request)
            {
                $delid=$request->input('delid');
                $counterdeletebamore=1;
            $counterrowsdelete=' ';
            if(App::isLocale('en')){      
            //----------------------------------------------------------------------
                foreach($delid as $del)
        {
        $intermediogramatica = Intermediogramatica::find($del);
        $linkintermediogramatica=$intermediogramatica->link;
        $bodyintermediogramatica=$intermediogramatica->body;
        Storage::delete('public/images/'.$intermediogramatica->foto);
       
        $counterrowsdelete=$counterrowsdelete.$counterdeletebamore.') ID: '.$del.'<br>Photo: '.$intermediogramatica->foto.'<br>Link: '.$intermediogramatica->link.'<br>Body: '.$intermediogramatica->body."<br><br>";        
        $counterdeletebamore++;
        }
        $counterdeletebamore=$counterdeletebamore-1;

        if($counterdeletebamore==1){
                Intermediogramatica::whereIn('id',$delid)->delete();
                Session::flash('success','There is '.$counterdeletebamore.' record deleted.<br><br><br>'.$counterrowsdelete);
        }else{
            Intermediogramatica::whereIn('id',$delid)->delete();
            Session::flash('success','There are '.$counterdeletebamore.' records deleted.<br><br><br>'. $counterrowsdelete);

        }

                return back();
                //---------------------------------------------------------------
    }else{

         //----------------------------------------------------------------------
         foreach($delid as $del)
         {  
         $intermediogramatica = Intermediogramatica::find($del);
         $linkintermediogramatica=$intermediogramatica->link;
         $bodyintermediogramatica=$intermediogramatica->body;
         Storage::delete('public/images/'.$intermediogramatica->foto);
       
         $counterrowsdelete=$counterrowsdelete.$counterdeletebamore.') ID: '.$del.'<br>Foto: '.$intermediogramatica->foto.'<br>Enlace: '.$intermediogramatica->link.'<br>Contenido: '.$intermediogramatica->body."<br><br>";        
        $counterdeletebamore++;
         }
         $counterdeletebamore=$counterdeletebamore-1;

         if($counterdeletebamore==1){
            Intermediogramatica::whereIn('id',$delid)->delete();
            Session::flash('success','Se ha eliminado '.$counterdeletebamore.' registro.<br><br><br>'.$counterrowsdelete);
         }else{
            Intermediogramatica::whereIn('id',$delid)->delete();
            Session::flash('success','Se han eliminado '.$counterdeletebamore.' registros.<br><br><br>'. $counterrowsdelete);

         }

                 return back();
                 //---------------------------------------------------------------
    }

            }
}
