<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;


Use Faker\Provider\Image;
use Illuminate\Support\Facades\Hash;

use App\Revista;
use App;
Use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

Use Illuminate\Support\Facades\Storage;
use PDF;
Use Illuminate\Http\Response;
use Auth;

use Validator;

use App\Notifications\SendContactNotification;
use Illuminate\Support\Facades\Notification;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class RevistaController extends Controller
{
    
    
    public function updaterevista($id,Request $request)
    {
        
    $revista = Revista::findOrFail($request->id);
    
    $revista->update($request->all());
    $locale=App::getLocale();
    $userna = $request->input('idc');

    $titulorevista=$request->input('titulo');

    $bodyrevista=$request->input('body');

    $linkrevista=$request->input('link');
    
       if(App::isLocale('en')){
        Session::flash('success','The record in the row '.$userna.' was updated!<br><br>ID: '.$request->id.'<br>Title: '.$titulorevista.'<br>Body: '.$bodyrevista.'<br>Link: '.$linkrevista);
    }else{
    Session::flash('success','¡El registro de la fila '.$userna.' fue actualizado!<br><br>ID: '.$request->id.'<br>Título: '.$titulorevista.'<br>Contenido: '.$bodyrevista.'<br>Enlace: '.$linkrevista);}


    return back();
        }



        public function destroyrevista(Request $request)
        {
            
            $revista = Revista::findOrFail($request->id);
            $titulorevista=$revista->titulo;
            $bodyrevista=$revista->body;
        $linkrevista=$revista->link;



            $revista->delete();
            $locale=App::getLocale();
            //--------------------------
            $userna = $request->input('idc2');
            if(App::isLocale('en')){
                Session::flash('success','The magazine in the row '.$userna.' was deleted!<br><br>ID: '.$request->id.'<br>Title: '.$titulorevista.'<br>Body: '.$bodyrevista.'<br>Link: '.$linkrevista.'<br>');
            }else{
            Session::flash('success','¡El libro de la fila '.$userna.' fue eliminado!<br><br>ID: '.$request->id.'<br>Título: '.$titulorevista.'<br>Contenido: '.$bodyrevista.'<br>Enlace: '.$linkrevista.'<br>');}
    
        
            return back();
            
            }

            public function destroyrevistamore(Request $request)
            {
                 
                $delid=$request->input('delid');
                $ccc = $request->input('ccc');
    
                $counterdeleteremore=1;
                $counterrowsdelete=' ';
    
                if(App::isLocale('en')){  
                foreach($delid as $del)
                {
                    $revista = Revista::find($del);
                    $titulorevista=$revista->titulo;
                    $bodyrevista=$revista->body;
                    $linkrevista=$revista->link;
                   
                    $counterrowsdelete=$counterrowsdelete.$counterdeleteremore.') ID: '.$del.' Title: '.$titulorevista.'<br>Body: '.$bodyrevista.'<br>Link: '.$linkrevista."<br>";
                    $counterdeleteremore++;
                }
                $counterdeleteremore=$counterdeleteremore-1;
    
                if($counterdeleteremore==1){
                    Revista::whereIn('id',$delid)->delete();
                    Session::flash('success','There is '.$counterdeleteremore.' record deleted.<br><br><br>'.$counterrowsdelete);
                   
                }else{  
                    Revista::whereIn('id',$delid)->delete();  
                 Session::flash('success','There are '.$counterdeleteremore.' records deleted.<br><br><br>'.$counterrowsdelete);
                 
                }
    
                return back();
            }else{
    
                foreach($delid as $del)
                {
                    $revista = Revista::find($del);
                    $titulorevista=$revista->titulo;
                    $bodyrevista=$revista->body;
                    $linkrevista=$revista->link;
                   
                    $counterrowsdelete=$counterrowsdelete.$counterdeleteremore.') ID: '.$del.' Título: '.$titulorevista.'<br>Contenido: '.$bodyrevista.'<br>Enlace: '.$linkrevista."<br>";
                    $counterdeleteremore++;
                }
                $counterdeleteremore=$counterdeleteremore-1;
    
                if($counterdeleteremore==1){
                    Revista::whereIn('id',$delid)->delete();
                    Session::flash('success','Se ha eliminado '.$counterdeleteremore.' registro.<br><br><br>'.$counterrowsdelete);
                   
                }else{  
                    Revista::whereIn('id',$delid)->delete();  
                 Session::flash('success','Se han eliminado '.$counterdeleteremore.' registros.<br><br><br>'.$counterrowsdelete);
                 
                }
    
                return back();
            }
               
                }
    
}