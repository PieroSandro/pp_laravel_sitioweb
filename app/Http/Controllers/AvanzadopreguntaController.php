<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;


Use Faker\Provider\Image;
use Illuminate\Support\Facades\Hash;

use App\Avanzadopregunta;
use App;
Use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

Use Illuminate\Support\Facades\Storage;
use PDF;
Use Illuminate\Http\Response;
use Auth;

use Validator;

use App\Notifications\SendContactNotification;
use Illuminate\Support\Facades\Notification;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class AvanzadopreguntaController extends Controller
{
    
    
    public function updateavanzadopregunta($id,Request $request)
    {
        
    $avanzadopregunta = Avanzadopregunta::findOrFail($request->id);
    $avanzadopregunta->update($request->all());
    $locale=App::getLocale();
    $userna = $request->input('idc');

    //$titulocurso = $request->input('titulo');
    $question = $request->input('question');
    $option1 = $request->input('option1');
    $option2 = $request->input('option2');
    $option3 = $request->input('option3');
    $option4 = $request->input('option4');
    $answer = $request->input('answer');

    if(App::isLocale('en')){
    Session::flash('success','The row '.$userna.' was updated!<br><br>ID: '.$request->id.'<br>Question: '.$question.'<br>Option 1: '.$option1.'<br>Option 2: '.$option2.'<br>Option 3: '.$option3.'<br>Option 4: '.$option4.'<br>Answer: '.$answer);
        return back();
    }else{
        Session::flash('success','¡La fila '.$userna.' fue actualizada!<br><br>ID: '.$request->id.'<br>Pregunta: '.$question.'<br>Opción 1: '.$option1.'<br>Opción 2: '.$option2.'<br>Opción 3: '.$option3.'<br>Opción 4: '.$option4.'<br>Respuesta: '.$answer);
        return back();

    }
    
    }



    public function destroyavanzadopregunta(Request $request)
    {
        
        $avanzadopregunta = Avanzadopregunta::findOrFail($request->id);
       /* $titulocurso=$vcurso->titulo;
        $bodycurso=$vcurso->body;
        $linkcurso=$vcurso->link;*/
        $question=$avanzadopregunta->question;
        $option1=$avanzadopregunta->option1;
        $option2=$avanzadopregunta->option2;
        $option3=$avanzadopregunta->option3;
        $option4=$avanzadopregunta->option4;
        $answer=$avanzadopregunta->answer;

        $avanzadopregunta->delete();
        $locale=App::getLocale();
        //--------------------------
        $userna = $request->input('idc2');
        if(App::isLocale('en')){
            Session::flash('success','The row '.$userna.' was deleted!<br><br>ID: '.$request->id.'<br>Question: '.$question.'<br>Option 1: '.$option1.'<br>Option 2: '.$option2.'<br>Option 3: '.$option3.'<br>Option 4: '.$option4.'<br>Answer: '.$answer.'<br>');
        }else{
        Session::flash('success','¡La fila '.$userna.' fue eliminada!<br><br>ID: '.$request->id.'<br>Pregunta: '.$question.'<br>Opción 1: '.$option1.'<br>Opción 2: '.$option2.'<br>Opción 3: '.$option3.'<br>Opción 4: '.$option4.'<br>Respuesta: '.$answer.'<br>');}

    
        return back();
        
        }

    public function destroyavanzadopreguntamore(Request $request)
        {
            
            $delid=$request->input('delid');
            $ccc = $request->input('ccc');

            $counterdeletevcmore=1;
            $counterrowsdelete=' ';

            if(App::isLocale('en')){  
            foreach($delid as $del)
            {
                $avanzadopregunta = Avanzadopregunta::find($del);
                $question=$avanzadopregunta->question;
                $option1=$avanzadopregunta->option1;
                $option2=$avanzadopregunta->option2;
                $option3=$avanzadopregunta->option3;
                $option4=$avanzadopregunta->option4;
                $answer=$avanzadopregunta->answer;
               
                $counterrowsdelete=$counterrowsdelete.$counterdeletevcmore.') ID: '.$del.'<br>Question: '.$question.'<br>Option 1: '.$option1.'<br>Option 2: '.$option2.'<br>Option 3: '.$option3.'<br>Option 4: '.$option4.'<br>Answer: '.$answer.'<br>';
                $counterdeletevcmore++;
            }
            $counterdeletevcmore=$counterdeletevcmore-1;

            if($counterdeletevcmore==1){
                Vcurso::whereIn('id',$delid)->delete();
                Session::flash('success','There is '.$counterdeletevcmore.' record deleted.<br><br><br>'.$counterrowsdelete);
               
            }else{  
                Vcurso::whereIn('id',$delid)->delete();  
             Session::flash('success','There are '.$counterdeletevcmore.' records deleted.<br><br><br>'.$counterrowsdelete);
             
            }

            return back();
        }else{

            foreach($delid as $del)
            {
                $avanzadopregunta = Avanzadopregunta::find($del);
                $question=$avanzadopregunta->question;
                $option1=$avanzadopregunta->option1;
                $option2=$avanzadopregunta->option2;
                $option3=$avanzadopregunta->option3;
                $option4=$avanzadopregunta->option4;
                $answer=$avanzadopregunta->answer;
               
                $counterrowsdelete=$counterrowsdelete.$counterdeletevcmore.') ID: '.$del.'<br>Pregunta: '.$question.'<br>Opción 1: '.$option1.'<br>Opción 2: '.$option2.'<br>Opción 3: '.$option3.'<br>Opción 4: '.$option4.'<br>Respuesta: '.$answer.'<br>';
                $counterdeletevcmore++;
            }
         
            
            $counterdeletevcmore=$counterdeletevcmore-1;

            if($counterdeletevcmore==1){
                Vcurso::whereIn('id',$delid)->delete();
                Session::flash('success','Se ha eliminado '.$counterdeletevcmore.' registro.<br><br><br>'.$counterrowsdelete);
               
            }else{  
                Vcurso::whereIn('id',$delid)->delete();  
             Session::flash('success','Se han eliminado '.$counterdeletevcmore.' registros.<br><br><br>'.$counterrowsdelete);
             
            }

            return back();






        }
            }
    
}