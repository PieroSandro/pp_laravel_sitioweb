<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;


Use Faker\Provider\Image;
use Illuminate\Support\Facades\Hash;

use App\Frequentepregunta;
use App;
Use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

Use Illuminate\Support\Facades\Storage;
use PDF;
Use Illuminate\Http\Response;
use Auth;

use Validator;

use App\Notifications\SendContactNotification;
use Illuminate\Support\Facades\Notification;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class FrequentepreguntaController extends Controller
{
    
    
    public function updatefrequentepregunta($id,Request $request)
    {
        
    $frequentepregunta = Frequentepregunta::findOrFail($request->id);
    $frequentepregunta->update($request->all());
    $locale=App::getLocale();
    $userna = $request->input('idc');

    //$titulocurso = $request->input('titulo');
    $pregunta_es = $request->input('pregunta_es');
    $respuesta_es = $request->input('respuesta_es');
    $pregunta_en = $request->input('pregunta_en');
    $respuesta_en = $request->input('respuesta_en');

    if(App::isLocale('en')){
    Session::flash('success','The row '.$userna.' was updated!<br><br>ID: '.$request->id.'<br>Question_ES: '.$pregunta_es.'<br>Answer_ES: '.$respuesta_es.'<br>Question_EN: '.$pregunta_en.'<br>Answer_EN: '.$respuesta_en);
        return back();
    }else{
        Session::flash('success','¡La fila '.$userna.' fue actualizada!<br><br>ID: '.$request->id.'<br>Pregunta_ES: '.$pregunta_es.'<br>Respuesta_ES: '.$respuesta_es.'<br>Pregunta_EN: '.$pregunta_en.'<br>Respuesta_EN: '.$respuesta_en);
        return back();

    }
    
    }



    public function destroyfrequentepregunta(Request $request)
    {
        
        $frequentepregunta = Frequentepregunta::findOrFail($request->id);
       /* $titulocurso=$vcurso->titulo;
        $bodycurso=$vcurso->body;
        $linkcurso=$vcurso->link;*/
        $pregunta_es=$frequentepregunta->pregunta_es;
       
        $respuesta_es=$frequentepregunta->respuesta_es;
        //5_2_2020
        $pregunta_en=$frequentepregunta->pregunta_en;
       
        $respuesta_en=$frequentepregunta->respuesta_en;
        $frequentepregunta->delete();
        $locale=App::getLocale();
        //--------------------------
        $userna = $request->input('idc2');
        if(App::isLocale('en')){
            Session::flash('success','The row '.$userna.' was deleted!<br><br>ID: '.$request->id.'<br>Question_ES: '.$pregunta_es.'<br>Answer_ES: '.$respuesta_es.'<br>Question_EN: '.$pregunta_en.'<br>Answer_EN: '.$respuesta_en.'<br>');
        }else{
        Session::flash('success','¡La fila '.$userna.' fue eliminada!<br><br>ID: '.$request->id.'<br>Pregunta_ES: '.$pregunta_es.'<br>Respuesta_ES: '.$respuesta_es.'<br>Pregunta_EN: '.$pregunta_en.'<br>Respuesta_EN: '.$respuesta_en.'<br>');}

    
        return back();
        
        }

    public function destroyfrequentepreguntamore(Request $request)
        {
            
            $delid=$request->input('delid');
            $ccc = $request->input('ccc');

            $counterdeletevcmore=1;
            $counterrowsdelete=' ';

            if(App::isLocale('en')){  
            foreach($delid as $del)
            {
                $frequentepregunta = Frequentepregunta::find($del);
                $pregunta_es=$frequentepregunta->pregunta_es;
       
        $respuesta_es=$frequentepregunta->respuesta_es;
        //5_2_2020
        $pregunta_en=$frequentepregunta->pregunta_en;
       
        $respuesta_en=$frequentepregunta->respuesta_en;
               
$counterrowsdelete=$counterrowsdelete.$counterdeletevcmore.') ID: '.$del.'<br>Question_ES: '.$pregunta_es.'<br>Answer_ES: '.$respuesta_es.'<br>Question_EN: '.$pregunta_en.'<br>Answer_EN: '.$respuesta_en.'<br>';
                $counterdeletevcmore++;
            }
            $counterdeletevcmore=$counterdeletevcmore-1;

            if($counterdeletevcmore==1){
                Frequentepregunta::whereIn('id',$delid)->delete();
                Session::flash('success','There is '.$counterdeletevcmore.' record deleted.<br><br><br>'.$counterrowsdelete);
               
            }else{  
                Frequentepregunta::whereIn('id',$delid)->delete();  
             Session::flash('success','There are '.$counterdeletevcmore.' records deleted.<br><br><br>'.$counterrowsdelete);
             
            }

            return back();
        }else{

            foreach($delid as $del)
            {
                $frequentepregunta = Frequentepregunta::find($del);
                $pregunta_es=$frequentepregunta->pregunta_es;
       
        $respuesta_es=$frequentepregunta->respuesta_es;
        //5_2_2020
        $pregunta_en=$frequentepregunta->pregunta_en;
       
        $respuesta_en=$frequentepregunta->respuesta_en;
               
                $counterrowsdelete=$counterrowsdelete.$counterdeletevcmore.') ID: '.$del.'<br>Pregunta_ES: '.$pregunta_es.'<br>Respuesta_ES: '.$respuesta_es.'<br>Pregunta_EN: '.$pregunta_en.'<br>Respuesta_EN: '.$respuesta_en.'<br>';
                $counterdeletevcmore++;
            }
         
            
            $counterdeletevcmore=$counterdeletevcmore-1;

            if($counterdeletevcmore==1){
                Frequentepregunta::whereIn('id',$delid)->delete();
                Session::flash('success','Se ha eliminado '.$counterdeletevcmore.' registro.<br><br><br>'.$counterrowsdelete);
               
            }else{  
                Frequentepregunta::whereIn('id',$delid)->delete();  
             Session::flash('success','Se han eliminado '.$counterdeletevcmore.' registros.<br><br><br>'.$counterrowsdelete);
             
            }

            return back();






        }
            }
    
}