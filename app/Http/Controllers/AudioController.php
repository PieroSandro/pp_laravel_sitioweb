<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;


Use Faker\Provider\Image;
use Illuminate\Support\Facades\Hash;

use App\Audio;
use App;
Use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

Use Illuminate\Support\Facades\Storage;
use PDF;
Use Illuminate\Http\Response;
use Auth;

use Validator;

use App\Notifications\SendContactNotification;
use Illuminate\Support\Facades\Notification;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class AudioController extends Controller
{
    
    
    public function updateaudio($id,Request $request)
    {
        
    $audio = Audio::findOrFail($request->id);
    
    $audio->update($request->all());
    $locale=App::getLocale();
    $userna = $request->input('idc');

    $bodyaudio=$request->input('body');

    $linkaudio=$request->input('link');
    
       if(App::isLocale('en')){
        Session::flash('success','The record in the row '.$userna.' was updated!<br><br>ID: '.$request->id.'<br>Link: '.$linkaudio.'<br>Body: '.$bodyaudio);
    }else{
    Session::flash('success','¡El registro de la fila '.$userna.' fue actualizado!<br><br>ID: '.$request->id.'<br>Enlace: '.$linkaudio.'<br>Contenido: '.$bodyaudio);}


    return back();
        }



        public function destroyaudio(Request $request)
        {
            
            $audio = Audio::findOrFail($request->id);
            $linkaudio=$audio->link;
            $bodyaudio=$audio->body;

            $audio->delete();
            $locale=App::getLocale();
            //--------------------------
            $userna = $request->input('idc2');
            if(App::isLocale('en')){
                Session::flash('success','The podcast in the row '.$userna.' was deleted!<br><br>Id: '.$request->id.'<br>Link: '.$linkaudio.'<br>Body: '.$bodyaudio.'<br>');
            }else{
            Session::flash('success','¡El podcast de la fila '.$userna.' fue eliminado!<br><br>Id: '.$request->id.'<br>Enlace: '.$linkaudio.'<br>Contenido: '.$bodyaudio.'<br>');}
    
        
            return back();
            
            }

            public function destroyaudiomore(Request $request)
            {
                 
                $delid=$request->input('delid');
                $ccc = $request->input('ccc');
    
                $counterdeleteaumore=1;
                $counterrowsdelete=' ';
    
                if(App::isLocale('en')){  
                foreach($delid as $del)
                {
                    $audio = Audio::find($del);
                    //estan todos los campos, debe eliminarse uno...
                    $linkaudio=$audio->link;
                    $bodyaudio=$audio->body;
                   
                    $counterrowsdelete=$counterrowsdelete.$counterdeleteaumore.') ID: '.$del.'<br>Link: '.$linkaudio.'<br>Body: '.$bodyaudio."<br>";
                    $counterdeleteaumore++;
                }
                $counterdeleteaumore=$counterdeleteaumore-1;
    
                if($counterdeleteaumore==1){
                    Audio::whereIn('id',$delid)->delete();
                    Session::flash('success','There is '.$counterdeleteaumore.' record deleted.<br><br><br>'.$counterrowsdelete);
                   
                }else{  
                    Audio::whereIn('id',$delid)->delete();  
                 Session::flash('success','There are '.$counterdeleteaumore.' records deleted.<br><br><br>'.$counterrowsdelete);
                 
                }
    
                return back();
            }else{
    
                foreach($delid as $del)
                {
                    $audio = Audio::find($del);
                    $linkaudio=$audio->link;
                    $bodyaudio=$audio->body;
                   
                    $counterrowsdelete=$counterrowsdelete.$counterdeleteaumore.') ID: '.$del.'<br>Enlace: '.$linkaudio.'<br>Contenido: '.$bodyaudio."<br>";
                    $counterdeleteaumore++;
                }
                $counterdeleteaumore=$counterdeleteaumore-1;
    
                if($counterdeleteaumore==1){
                    Audio::whereIn('id',$delid)->delete();
                    Session::flash('success','Se ha eliminado '.$counterdeleteaumore.' registro.<br><br><br>'.$counterrowsdelete);
                   
                }else{  
                    Audio::whereIn('id',$delid)->delete();  
                 Session::flash('success','Se han eliminado '.$counterdeleteaumore.' registros.<br><br><br>'.$counterrowsdelete);
                 
                }
    
                return back();
            }
               
                }
    
}