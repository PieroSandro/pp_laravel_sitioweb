<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App;
use App\Post;
use App\Categoria;
use App\Comentario;
use App\Respuesta;
use App\User;
use Auth;
use Validator;

//-------POST CON IMAGENES-------------------------------------

Use Faker\Provider\Image;
use Illuminate\Support\Facades\Hash;
Use Illuminate\Support\Facades\File;

//--------------------------------------------


use Illuminate\Support\Facades\Input;
Use Illuminate\Support\Facades\Storage;
use App\Notifications\SendContactNotification;
use Illuminate\Support\Facades\Notification;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;
class BlogController extends Controller {

    /*public function __construct()
    {
    $this->middleware('auth');
    }

    public function index() {
        return view('home');
    }*/

    public function podcastbasico(){
        return View('front.blog2.podcast_basico');
    }

    public function podcastintermedio(){
        return View('front.blog2.podcast_intermedio');
    } 

    public function podcastavanzado(){
        return View('front.blog2.podcast_avanzado');
    }  

    

    public function n_basico(){
        return View('front.n_basico');
    }

    public function n_intermedio(){
        return View('front.n_intermedio');
    }

    public function n_avanzado(){
        return View('front.n_avanzado');
    }

    public function p_frecuentes(){
        return View('front.p_frecuentes');
    }

    public function v_estudiantil(){
        return View('front.v_estudiantil');
    }

    public function all_actividades(){
        return View('front.all_actividades');
    }

    public function musica(){
        return View('front.musica');
    }

    public function teatro(){
        return View('front.teatro');
    }

    public function danza(){
        return View('front.danza');
    }

	public function show($id)
    {
        
        $id_coment = $id;
        $post = Post::find($id);
        //$comentarios = Comentario::orderBy('created_at','ASC')->where('post_id',$post)->paginate(10);
        
        return View('front.blog2.show',compact(['post','id_coment']));

        
    }

    /*public function show($id)
    {
        
        //$post= Post::where('id',$id)->firstOrFail();
        $post = Post::find($id);
        $comentarios = Comentario::orderBy('created_at','ASC')->where('post_id', $id)->paginate(10);

        
        return View('front.blog2.show',compact('post','comentarios'));

        
    }*/


    /* -------------------------------------------*/
    /* -------------------------------------------*/
    /* -------------------------------------------*/
    /* -------------------------------------------*/
    /* -------------------------------------------*/

    /*
Route::get('/blog/home', function () {
    
  $posts = Post::orderBy('created_at','DESC')->paginate(10);
  return view('front.blog2.home',compact(
    'posts'));
    
});

    */
/*
public function blogadministracion(){
        $posts = Post::orderBy('created_at','DESC')->where('categoria_id',1)->paginate(10);
        return View('front.blog2.c_administracion')->with('posts',$posts);
    }
*/
    /*public function storecoment(request $request,$post_id)
    {
        $this->validate($request,array(
            'nick'=>'required',
            'contenido'=>'required',
            'correo'=>'required|email'));

        

        $post=Post::find($post_id);
        $comentario = new Comentario();
        $comentario->nick = $request->nick;
        $comentario->contenido = $request->contenido;
        $comentario->correo = $request->correo;
        
        $comentario->post()->associate($post);
        $comentario->save();

        

        Session::flash('success','El comentario fue añadido');
        

        return View('front.blog2.show')->with('post',$post);
    }*/

    /*public function storecoment(Request $request)
    {
        

        if($request->ajax()){
        
        $post = Post::all();
        $post_id = $request->post_id;
        $post=Post::find($post_id);
        $comentario = new Comentario();
        $comentario->nick = $request->nick;
        $comentario->contenido = $request->contenido;
        $comentario->correo = $request->correo;
        
        $comentario->post()->associate($post);
        $comentario->save();

        $locale=App::getLocale();

        $imprimir = '';

        $imprimir .= '<div class="col-12 col-md-offset-2">';
            foreach($post->comentarios as $comentario){
            $imprimir .= '<div class="card"  style="padding-top: 20px;padding-right: 20px;padding-bottom: 20px;padding-left: 20px;">
                <p><strong>'.lang('home.bl_bname').':</strong>'.$comentario->nick.'</p><br>
                @guest
                <p><strong>'.lang('home.bl_coment').':</strong>'.$comentario->contenido.'</p><br>
                <p><small>'.lang('home.bl_fcreacion').': '.$comentario->created_at.'</small></p>
                       
    @else
    <p><strong>'.lang('home.bl_coment').':</strong> '.$comentario->contenido.'</p><br>
    <p><strong>'.lang('home.bl_correo').':</strong> '.$comentario->correo.'</p><br>
    <p><small>'.lang('home.bl_fcreacion').': '.$comentario->created_at.'</small></p>
    <br>
    '.Form::open(['action'=>['ComentarioController@destroy',$post->id,$comentario->id],'method'=>'POST','class'=>'float-right']).'
    @csrf
    '.Form::hidden('_method','DELETE').'
    
    <button type="submit" class="btn btn-danger">
    <i class="fas fa-trash"></i>'.lang('home.bl_borrar').'
    </button>
    <br><br>
    '.Form::close().'
    <br><br>
    @endguest';
        }
        $imprimir .='</div>';


        return $imprimir;
        if(App::isLocale('en')){
            
            return $imprimir;
        }else{
            
        return $imprimir;
        }

        }
        
    }*/

    public function storecoment(Request $request,$post_id)
    {
        $this->validate($request,array(
            'nick'=>'required',
            'contenido'=>'required',
            'correo'=>'required|email'));
        $i=0;
        $lisuras = array("mierda","carajo","puta","rechucha","imbecil","verga","huevada","mrd","ctm","rctm","pendejo","conchatumare","mamada","perro","chupapinga","cmamo","hdp","motherfuck","fuck","holy");

        $contenido = $_POST['contenido'];
        $very= array($contenido);
        foreach($lisuras as $lisura){
            $encontrado=false;
            foreach ($very as $value2) {
            if((strpos(' '.$contenido,$lisura))||($lisura == $value2)){
                $encontrado=true;    
            }
        }
            if($encontrado==false){
            }else{
                $i++;
            }
        }
       
        if($i==0){
            $post=Post::find($post_id);
        $comentario = new Comentario();
        $comentario->nick = $request->nick;
        $comentario->contenido = $contenido;
        $comentario->correo=$request->correo;
        
        $comentario->post()->associate($post);
        $comentario->save();

        $locale=App::getLocale();
        if(App::isLocale('en')){
            Session::flash('success','The comment was added');
        }else{
        Session::flash('success','El comentario fue añadido');}
        return View('front.blog2.show')->with('post',$post);
        }else{
            $post=Post::find($post_id);
        $comentario = new Comentario();
        $comentario->nick = $request->nick;
        $comentario->contenido = $contenido;
        $comentario->correo=$request->correo;
        
        $comentario->post()->associate($post);
            $locale=App::getLocale();

        if(App::isLocale('en')){
            Session::flash('success','Dont tell lisures');
            
            ?>
               
        <?php
            
        }else{
        Session::flash('success','No hables lisuras');}
        

        return View('front.blog2.show')->with('post',$post);
        }

       }
//----------------------------------------------------
        
        

        
    

    

    public function bloghome(){
        $posts = Post::orderBy('created_at','DESC')->paginate(10);
        return View('front.blog2.home')->with('posts',$posts);
    }

    public function blogadministracion(){
        $posts = Post::orderBy('created_at','DESC')->where('categoria_id',1)->paginate(10);
        return View('front.blog2.c_administracion')->with('posts',$posts);
    }

    public function blogcomputacion(){
        $posts = Post::orderBy('created_at','DESC')->where('categoria_id',2)->paginate(10);
        
        return View('front.blog2.c_computacion')->with('posts',$posts);
    }

    public function blogcontabilidad(){
        $posts = Post::orderBy('created_at','DESC')->where('categoria_id',3)->paginate(10);
        return View('front.blog2.c_contabilidad')->with('posts',$posts);
    }

    public function blogelectronica(){
        $posts = Post::orderBy('created_at','DESC')->where('categoria_id',4)->paginate(10);
        return View('front.blog2.c_electronica')->with('posts',$posts);
    }

    public function blogelectrotecnia(){
        $posts = Post::orderBy('created_at','DESC')->where('categoria_id',5)->paginate(10);
        return View('front.blog2.c_electrotecnia')->with('posts',$posts);
    }

    public function bloglaboratorio(){
        $posts = Post::orderBy('created_at','DESC')->where('categoria_id',6)->paginate(10);
        return View('front.blog2.c_laboratorio')->with('posts',$posts);
    }

    public function blogmautomotriz(){
        $posts = Post::orderBy('created_at','DESC')->where('categoria_id',7)->paginate(10);
        return View('front.blog2.c_mautomotriz')->with('posts',$posts);
    }

    public function blogmetalurgia(){
        $posts = Post::orderBy('created_at','DESC')->where('categoria_id',9)->paginate(10);
        return View('front.blog2.c_metalurgia')->with('posts',$posts);
    }

    public function blogmproduccion(){
        $posts = Post::orderBy('created_at','DESC')->where('categoria_id',8)->paginate(10);
        return View('front.blog2.c_mproduccion')->with('posts',$posts);
    }

    public function destroy($id)
    {
        //dd($id);
        $post = Post::find($id);
        if($post->foto != 'noimage.jpg'){
            Storage::delete('public/images/'.$post->foto);
        }
        $post->delete();
        $locale=App::getLocale();
        if(App::isLocale('en')){
            Session::flash('success');
            return redirect()->route('bloghome')->with('success','¡Post deleted!');    
            ?>    
        <?php 
        }else{
        Session::flash('success');
        return redirect()->route('bloghome')->with('success','¡Post borrado!');}  
    }

    public function update($id,Request $request)
    {
        $this->validate($request,[
            'titulo'=> 'required|max:50',
            'body' => 'required'
          ]);
          if($request->hasFile('foto')){
            $filenameWithExt=$request->file('foto')->getClientOriginalName();
            $filename=pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension=$request->file('foto')->getClientOriginalExtension();
            $fileNameToStore=$filename.'_'.time().'.'.$extension;
            $path=$request->file('foto')->storeAs('public/images',$fileNameToStore);
        }
          $post = Post::find($id);
          $post->titulo = $request->input('titulo');
          $post->body = $request->input('body');
          if($request->hasFile('foto')){
            $post->foto=$fileNameToStore;
        }
          $post->save(); 
          $locale=App::getLocale();
          if(App::isLocale('en')){
              Session::flash('success','The post was updated');   
              ?>    
          <?php 
          }else{
          Session::flash('success','El post fue actualizado');}
           return view('front.blog2.edit')->with('post',$post);
    }

    //---------------------------------------
    //---------------------------------------

    public function it_administracion1(){
        return View('front.blog2.it_administracion1');
    }  

    public function it_administracion2(){
        return View('front.blog2.it_administracion2');
    }  

    public function it_administracion3(){
        return View('front.blog2.it_administracion3');
    }  

    public function it_computacion1(){
        return View('front.blog2.it_computacion1');
    }  

    public function it_computacion2(){
        return View('front.blog2.it_computacion2');
    }  

    public function it_computacion3(){
        return View('front.blog2.it_computacion3');
    }  

    public function it_contabilidad1(){
        return View('front.blog2.it_contabilidad1');
    }  

    public function it_contabilidad2(){
        return View('front.blog2.it_contabilidad2');
    }  

    public function it_contabilidad3(){
        return View('front.blog2.it_contabilidad3');
    }  

    public function it_electronica1(){
        return View('front.blog2.it_electronica1');
    }  

    public function it_electronica2(){
        return View('front.blog2.it_electronica2');
    }  

    public function it_electronica3(){
        return View('front.blog2.it_electronica3');
    }

    public function it_electrotecnia1(){
        return View('front.blog2.it_electrotecnia1');
    }  

    public function it_electrotecnia2(){
        return View('front.blog2.it_electrotecnia2');
    }  

    public function it_electrotecnia3(){
        return View('front.blog2.it_electrotecnia3');
    }

    public function it_laboratorio1(){
        return View('front.blog2.it_laboratorio1');
    }  

    public function it_laboratorio2(){
        return View('front.blog2.it_laboratorio2');
    }  

    public function it_laboratorio3(){
        return View('front.blog2.it_laboratorio3');
    } 

    
    public function it_mautomotriz1(){
        return View('front.blog2.it_mautomotriz1');
    }  

    public function it_mautomotriz2(){
        return View('front.blog2.it_mautomotriz2');
    }  

    public function it_mautomotriz3(){
        return View('front.blog2.it_mautomotriz3');
    } 

    public function it_metalurgia1(){
        return View('front.blog2.it_metalurgia1');
    }  

    public function it_metalurgia2(){
        return View('front.blog2.it_metalurgia2');
    }  

    public function it_metalurgia3(){
        return View('front.blog2.it_metalurgia3');
    } 

    public function it_mproduccion1(){
        return View('front.blog2.it_mproduccion1');
    }  

    public function it_mproduccion2(){
        return View('front.blog2.it_mproduccion2');
    }  

    public function it_mproduccion3(){
        return View('front.blog2.it_mproduccion3');
    } 
//---------------------------------------------------------
//---------------------------------------------------------
}

