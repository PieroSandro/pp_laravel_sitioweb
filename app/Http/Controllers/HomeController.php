<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Evento;
Use Illuminate\Http\Response;
use App\Anuncio;
use App;
use Session;
use App\Audio;
use App\Post;
use App\Banner;
use App\Categoria;
use App\Comentario;
use App\Event;
use App\Patro;
use App\User;
use App\Vcurso;
use App\Libro;
use App\Revista;
use App\Efecto;
use App\Animacione;
use App\Video;
use App\Mainpage;
use App\Main2page;
use App\Basicopregunta;
use App\Basicogramatica;
use App\Intermediopregunta;
use App\Intermediogramatica;
use App\Avanzadopregunta;
use App\Avanzadogramatica;
use App\Quizresultado;
use App\Chatbot;
use App\Frequentepregunta;
use App\Mainicono;
use App\Mainword;


use Yajra\DataTables\DataTables;
use Validator;
use Illuminate\Support\Facades\Input;

use App\Notifications\SendContactNotification;
use Illuminate\Support\Facades\Notification;
Use Illuminate\Support\Facades\File;
Use Faker\Provider\Image;
Use Illuminate\Support\Facades\Storage;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;
use App\Rules\Captcha;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    //------------------------------------------------------FRONT
    
    public function backanunciohome(){

        $anuncios = Anuncio::orderBy('created_at','DESC')->paginate(10);
  return view('front.blog2.home_an',compact(
    'anuncios'));
    }

   


    public function backeventhome(){

        $events = App\Event::all();
        //$events = Event::orderBy('id','ASC')->paginate();
  return view('front.blog2.homeevent',compact(
    'events'));
    }

    
    public function backmainwordhome(){

        $mainwords = App\Mainword::all();
        //$events = Event::orderBy('id','ASC')->paginate();
  return view('front.blog2.homemainword',compact(
    'mainwords'));
    }


    public function backmainiconohome(){

        $mainiconos = App\Mainicono::all();
        //$events = Event::orderBy('id','ASC')->paginate();
  return view('front.blog2.homemainicono',compact(
    'mainiconos'));
    }

    public function backpatrohome(){

        $patros = App\Patro::all();
        //$patros = Patro::orderBy('id','ASC');
         return view('front.blog2.homepatro',compact(
           'patros'));
    }

    public function backaudiohome(){

        $audios = App\Audio::all();
        //$audios = Audio::orderBy('id','ASC')->paginate();
         return view('front.blog2.homeaudio',compact(
           'audios'));
    }

    public function backbannerhome(){

        $banners = App\Banner::all();
        //$audios = Audio::orderBy('id','ASC')->paginate();
         return view('front.blog2.homebanner',compact(
           'banners'));
    }

    public function backbasicogramaticahome(){

        $basicogramaticas = App\Basicogramatica::all();
        //$audios = Audio::orderBy('id','ASC')->paginate();
         return view('front.blog2.homebasicogramatica',compact(
           'basicogramaticas'));
    }

    
    public function backintermediogramaticahome(){

        $intermediogramaticas = App\Intermediogramatica::all();
        //$audios = Audio::orderBy('id','ASC')->paginate();
         return view('front.blog2.homeintermediogramatica',compact(
           'intermediogramaticas'));
    }

    
    public function backavanzadogramaticahome(){

        $avanzadogramaticas = App\Avanzadogramatica::all();
        //$audios = Audio::orderBy('id','ASC')->paginate();
         return view('front.blog2.homeavanzadogramatica',compact(
           'avanzadogramaticas'));
    }

    public function backbloghome(){

        $posts = Post::orderBy('created_at','DESC')->paginate(10);
  return view('front.blog2.home',compact(
    'posts'));
    }

    public function backlibrohome(){

        $libros = App\Libro::all();
        //$audios = Audio::orderBy('id','ASC')->paginate();
         return view('front.blog2.homelibro',compact(
           'libros'));
    }

    public function backmainpagehome(){

        $mainpages = App\Mainpage::all();
        //$events = Event::orderBy('id','ASC')->paginate();
  return view('front.blog2.homemainpage',compact(
    'mainpages'));
    }

    
    public function backmain2pagehome(){

        $main2pages = App\Main2page::all();
        //$events = Event::orderBy('id','ASC')->paginate();
  return view('front.blog2.homemain2page',compact(
    'main2pages'));
    }

    public function backrevistahome(){

        $revistas = App\Revista::all();
        //$audios = Audio::orderBy('id','ASC')->paginate();
         return view('front.blog2.homerevista',compact(
           'revistas'));
    }

    public function backvcursohome(){

        $vcursos = App\Vcurso::all();
        //$audios = Audio::orderBy('id','ASC')->paginate();
         return view('front.blog2.homevcurso',compact(
           'vcursos'));
    }

    public function backvideohome(){

        $videos = App\Video::all();
        //$audios = Audio::orderBy('id','ASC')->paginate();
         return view('front.blog2.homevideo',compact(
           'videos'));
    }



    public function backbasicopreguntahome(){

        $basicopreguntas = App\Basicopregunta::all();
        //$audios = Audio::orderBy('id','ASC')->paginate();
         return view('front.blog2.homebasicopregunta',compact(
           'basicopreguntas'));
    }


    public function backintermediopreguntahome(){

        $intermediopreguntas = App\Intermediopregunta::all();
        //$audios = Audio::orderBy('id','ASC')->paginate();
         return view('front.blog2.homeintermediopregunta',compact(
           'intermediopreguntas'));
    }

    
    public function backavanzadopreguntahome(){

        $avanzadopreguntas = App\Avanzadopregunta::all();
        //$audios = Audio::orderBy('id','ASC')->paginate();
         return view('front.blog2.homeavanzadopregunta',compact(
           'avanzadopreguntas'));
    }

    
    public function backchatbothome(){

        $chatbots = App\Chatbot::all();
        //$audios = Audio::orderBy('id','ASC')->paginate();
         return view('front.blog2.homechatbot',compact(
           'chatbots'));
    }

    //

    
    public function backfrequentepreguntahome(){

        $frequentepreguntas = App\Frequentepregunta::all();
        //$audios = Audio::orderBy('id','ASC')->paginate();
         return view('front.blog2.homefrequentepregunta',compact(
           'frequentepreguntas'));
    }
/*
    Route::get('/cursosonline', function () {
        $vcursos = Vcurso::orderBy('created_at','DESC')->paginate(5);
      return view('front.cursosonline',compact(
        'vcursos'));
    })->name('cursosonline');*/


    /*public function cursosonline(){
        $vcursos = Vcurso::orderBy('created_at','DESC')->paginate(5);
        return View('front.cursosonline')->with('vcursos',$vcursos);
    }*/


    public function bloghome(){
        $posts = Post::orderBy('created_at','DESC')->paginate(10);
        return View('front.blog2.home_prueba')->with('posts',$posts);
    }


   

    public function index() {


        return view('home');

        session_start();
          if((time()-$_SESSION['last_time'])>30)
    {
      
        header("location:session_killer.php");
        
    }
    else
    {
        $_SESSION['last_time']=time();
        }

    }

    public function index2() {
        return view('home2');
    }

    public function test() {
        $ciclos = \App\Ciclo::all();
        return view('test', [
            'ciclos' => $ciclos
        ]);
    }


public function principal(){
        return View('front.index');
    }


    public function quienes(){
        return View('front.quienes');
    }

    public function mision(){
        return View('front.mision');
    }

    public function vision(){
        return View('front.vision');
    }

    public function docentes(){
        return View('front.docentes');
    }

    public function suficiencia(){
        return View('front.suficiencia');
    }

    public function extraordinarios(){
        return View('front.extraordinarios');
    }

    public function t_english(){
        return View('front.t_english');
    }

    public function videos(){
        return View('front.videos');
    }

    public function reclamaciones(){
        return View('front.reclamaciones');
    }    
    
    public function reclamacionessend(Request $request){
        $this->validate($request,[
            'm_edad'=>'nullable',
            'name'=>'required|max:50|alpha',
            'apellidop'=>'required|max:50|alpha',
            'apellidom'=>'required|max:50|alpha',
            
            'email'=>'required|email|max:50',
            'message'=>'required'
        ]);

        Notification::route('mail','vilmajuarezgaribay13@gmail.com')
                        ->notify(new SendContactNotification2($request));

        
        Session::flash('success','El mensaje fue enviado exitosamente');
        
        return redirect()->route('reclamaciones')->with('success','Mensaje enviado!!');
    } 

    public function intranet(){
        return View('front.intranet');
    }
    
    public function contactenos(){
        return View('front.contactenos');
    } 

    
    public function chatbotvista(){
        return View('front.chatbotvista');
    } 

    public function contactenossend(Request $request){
        $this->validate($request,[
            'name'=>'required|max:50|alpha',
            'email'=>'required|email|max:50',
            'message'=>'required'
        ]);

        Notification::route('mail','vilmajuarezgaribay13@gmail.com')
                        ->notify(new SendContactNotification($request));

        
        Session::flash('success','El correo fue enviado exitosamente');
        
        return redirect()->route('contactenos')->with('success','Correo enviado!!');
    } 
    //mail:vilmajuarezgaribay13@gmail.com
    //pass: 09466346



    public function basico(){
        return View('front.quiz.basico');
    } 

    public function intermedio(){
        return View('front.quiz.intermedio');
    } 

    public function intermedio_2(){
        return View('front.quiz.intermedio_2');
    } 

    public function create_an()
    {
        
    
        return View('front.blog2.create_an');
    }

    public function createbanner()
    {
        
    
        return View('front.blog2.createbanner');
    }

    public function createvideo()
    {
        
        return View('front.blog2.createvideo');
    }

    public function createevent()
    {
        
    
        return View('front.blog2.createevent');
    }

    
    public function createmainword()
    {
        
    
        return View('front.blog2.createmainword');
    }

    public function createmainicono()
    {
        
    
        return View('front.blog2.createmainicono');
    }

    public function createpatro()
    {
        
    
        return View('front.blog2.createpatro');
    }

    public function createbasicogramatica()
    {
        
    
        return View('front.blog2.createbasicogramatica');
    }

    
    public function createintermediogramatica()
    {
        
    
        return View('front.blog2.createintermediogramatica');
    }


    
    public function createavanzadogramatica()
    {
        
    
        return View('front.blog2.createavanzadogramatica');
    }

        public function create()
    {
        $categorias=Categoria::all();
        return View('front.blog2.create',compact('categorias'));
    }

    public function createanima()
    {
        $efectos=Efecto::all();
        return View('front.blog2.createanima',compact('efectos'));
    }

    
    public function store(Request $request)
    {
        $this->validate($request,[
          'titulo'=> 'required',
          'body' => 'required',
          'foto' =>'image|nullable',
          'categoria_id' => 'required'
        ]);
        
        if($request->hasFile('foto')){
            $filenameWithExt=$request->file('foto')->getClientOriginalName();
            $filename=pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension=$request->file('foto')->getClientOriginalExtension();
            $fileNameToStore=$filename.'_'.time().'.'.$extension;
            $path=$request->file('foto')->storeAs('public/images',$fileNameToStore);
        }else{
            $fileNameToStore='noimage.jpg';
        }
        //Create Post
        $post = new Post;
        $post->titulo = $request->input('titulo');
        $post->body = $request->input('body');
        $post->foto=$fileNameToStore;
        
        $post->categoria_id = $request->input('categoria_id');
        $post->save();
        //return redirect('/blog/home')->with('success','Post Created!!');
        Session::flash('success','El post fue enviado exitosamente');

        //return redirect('/index/anuncios')->with('success','¡Anuncio creado!');
        return redirect()->route('create')->with('success','¡Post creado!');
    }

    public function storeanima(Request $request)
    {
        $this->validate($request,[
          'titulo'=> 'required',
          'efecto_id' => 'required'
        ]);
        
       
        $animacione = new Animacione;
        $animacione->titulo = $request->input('titulo');
        
        $animacione->efecto_id = $request->input('efecto_id');
        $animacione->save();
       
        Session::flash('success','La animacion fue enviado exitosamente');

        //return redirect('/index/anuncios')->with('success','¡Anuncio creado!');
        return redirect()->route('createanima')->with('success','¡Animación creada!');
    }

       public function store2(Request $request)
    {
        $this->validate($request,[
          'titulo'=> 'required|max:50',
          'body' => 'required',
          'foto' =>'image|nullable',//mimes:jpeg,bmp,png
          'fecha_vence' => 'required|date'
        ]);
        
        if($request->hasFile('foto')){
            $filenameWithExt=$request->file('foto')->getClientOriginalName();
            $filename=pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension=$request->file('foto')->getClientOriginalExtension();
            $fileNameToStore=$filename.'_'.time().'.'.$extension;
            $path=$request->file('foto')->storeAs('public/images',$fileNameToStore);
        }else{
            $fileNameToStore='noimage.jpg';
        }

        $anuncio = new Anuncio;
        $anuncio->titulo = $request->input('titulo');
        $anuncio->body = $request->input('body');
        $anuncio->foto=$fileNameToStore;
        
        $anuncio->fecha_vence = $request->input('fecha_vence');
        $anuncio->save();
        
        Session::flash('success','El anuncio fue enviado exitosamente');
        //return redirect('/index/anuncios')->with('success','¡Anuncio creado!');
        return redirect()->route('create_an')->with('success','¡Anuncio creado!');
    }

    public function storeevent(Request $request)
    {
        $this->validate($request,[
            'titulo'=> 'required|max:50',
          'body' => 'required',
          'foto'=> 'image|required',
          'link' => 'required',
        ]);
        
            $filenameWithExt=$request->file('foto')->getClientOriginalName();
            $filename=pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension=$request->file('foto')->getClientOriginalExtension();
            $fileNameToStore=$filename.'_'.time().'.'.$extension;
            $path=$request->file('foto')->storeAs('public/images',$fileNameToStore);
       
        $event = new Event;
        $event->link = $request->input('link');
        $event->titulo = $request->input('titulo');
        $event->body = $request->input('body');
        
        $event->foto=$fileNameToStore;
       
        $event->save();
        
        Session::flash('success','El evento fue enviado exitosamente');
        return redirect()->route('createevent')->with('success','¡Evento creado!');
    }

    
    public function storemainword(Request $request)
    {
        $this->validate($request,[
            'titulo'=> 'required|max:50',
          'body' => 'required',
       
        ]);
        
          
       
        $mainword = new Mainword;
       
        $mainword->titulo = $request->input('titulo');
        $mainword->body = $request->input('body');
        
       
       
        $mainword->save();
        
        Session::flash('success','El mainword fue enviado exitosamente');
        return redirect()->route('createmainword')->with('success','¡Mainword creado!');
    }

    public function storemainicono(Request $request)
    {
        $this->validate($request,[
            
          'foto'=> 'image|required',
          
        ]);
        
            $filenameWithExt=$request->file('foto')->getClientOriginalName();
            $filename=pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension=$request->file('foto')->getClientOriginalExtension();
            $fileNameToStore=$filename.'_'.time().'.'.$extension;
            $path=$request->file('foto')->storeAs('public/images',$fileNameToStore);
       
        $mainicono = new Mainicono;
       
        
        $mainicono->foto=$fileNameToStore;
       
        $mainicono->save();
        
        Session::flash('success','El icono fue enviado exitosamente');
        return redirect()->route('createmainicono')->with('success','¡Mainicono creado!');
    }

    public function storepatro(Request $request)
{
    
        $files = $request->file('foto');
        $destinationPath = 'C:\xampp\htdocs\intranet\storage\app\public\images';
       
        $titulos = $request->input('titulo');
        $bodys = $request->input('body');
        
        foreach($files as $file) {
            $filename = $file->getClientOriginalName();
            $upload_success = $file->move($destinationPath, $filename);
        $fotoarray[]=$filename;
        }

        foreach($titulos as $titulo) {
        $tituloarray[]=$titulo;
        }
        $counter = 0;
        foreach($bodys as $body) {
            $bodyarray[]=$body;
           
            $counter++;
            }
        for($i=0;$i<$counter;$i++){
            $patro = new Patro;
           
            $patro->foto=$fotoarray[$i];
            $patro->titulo=$tituloarray[$i];
            $patro->body=$bodyarray[$i];
            $patro->save();

        }
            return redirect()->route('createpatro')->with('success', 'Your task added successfully!');
 }


 public function storebanner(Request $request)
 {
    $files = $request->file('foto');
    $destinationPath = 'C:\xampp\htdocs\intranet\storage\app\public\images';
    $links = $request->input('link');
    $counterstorevc=1;
        $counterwordsvc='';
    foreach($files as $file) {
        $filename = $file->getClientOriginalName();
        $upload_success = $file->move($destinationPath, $filename);
    $fotoarray[]=$filename;
    }
    $counter = 0;
    foreach($links as $link) {
    $linkarray[]=$link;
    $counter++;
    }
  
     $locale=App::getLocale();
     if(App::isLocale('en')){
        for($i=0;$i<$counter;$i++){
            $banner = new Banner;
           
            $banner->foto=$fotoarray[$i];
            $banner->link=$linkarray[$i];
            $banner->save();
            $counterwordsvc=$counterwordsvc.$counterstorevc.') Image: '.$fotoarray[$i].'<br>Link: '.$linkarray[$i]."<br><br>";
             $counterstorevc++;
        }
    
        $counterstorevc=$counterstorevc-1;
        if($counterstorevc==1){
            Session::flash('success','There is '.$counterstorevc.' record saved.<br><br>'.$counterwordsvc);
        }else{    
         Session::flash('success','There are '.$counterstorevc.' records saved.<br><br>'.$counterwordsvc);
        }
     }else{
        for($i=0;$i<$counter;$i++){
            $banner = new Banner;
           
            $banner->foto=$fotoarray[$i];
            $banner->link=$linkarray[$i];
            $banner->save();
            $counterwordsvc=$counterwordsvc.$counterstorevc.') Imagen: '.$fotoarray[$i].'<br>Enlace: '.$linkarray[$i]."<br><br>";
             $counterstorevc++;
        }
    
        $counterstorevc=$counterstorevc-1;
        if($counterstorevc==1){
            Session::flash('success','Se ha guardado '.$counterstorevc.' registro.<br><br>'.$counterwordsvc);
        }else{ 
         Session::flash('success','Se han guardado '.$counterstorevc.' registros.<br><br>'.$counterwordsvc);
        }
         }
    
     return back();
 } 

 //-------------31-01---------------------------------------
 public function storebasicogramatica(Request $request)
 {
    $files = $request->file('foto');
    $destinationPath = 'C:\xampp\htdocs\intranet\storage\app\public\images';
    $links = $request->input('link');
    $bodys = $request->input('body');
    $counterstorevc=1;
        $counterwordsvc='';
    foreach($files as $file) {
        $filename = $file->getClientOriginalName();
        $upload_success = $file->move($destinationPath, $filename);
    $fotoarray[]=$filename;
    }
    $counter = 0;
    $counter2 = 0;
    foreach($links as $link) {
    $linkarray[]=$link;
    $counter++;
    }
    
    foreach($bodys as $body) {
        $bodyarray[]=$body;
        $counter2++;
        }
  
     $locale=App::getLocale();
     if(App::isLocale('en')){
        for($i=0;$i<$counter;$i++){
            $basicogramatica = new Basicogramatica;
           
            $basicogramatica->foto=$fotoarray[$i];
            $basicogramatica->link=$linkarray[$i];
//---31-01
$basicogramatica->body=$bodyarray[$i];
//---31-01
            $basicogramatica->save();
            $counterwordsvc=$counterwordsvc.$counterstorevc.') Image: '.$fotoarray[$i].'<br>Link: '.$linkarray[$i].'<br>Body: '.$bodyarray[$i]."<br><br>";
             $counterstorevc++;
        }
    
        $counterstorevc=$counterstorevc-1;
        if($counterstorevc==1){
            Session::flash('success','There is '.$counterstorevc.' record saved.<br><br>'.$counterwordsvc);
        }else{    
         Session::flash('success','There are '.$counterstorevc.' records saved.<br><br>'.$counterwordsvc);
        }
     }else{
        for($i=0;$i<$counter;$i++){
            $basicogramatica = new Basicogramatica;
           
            $basicogramatica->foto=$fotoarray[$i];
            $basicogramatica->link=$linkarray[$i];



            $basicogramatica->body=$bodyarray[$i];
            $basicogramatica->save();
            $counterwordsvc=$counterwordsvc.$counterstorevc.') Imagen: '.$fotoarray[$i].'<br>Enlace: '.$linkarray[$i].'<br>Contenido: '.$bodyarray[$i]."<br><br>";
             $counterstorevc++;
        }
    
        $counterstorevc=$counterstorevc-1;
        if($counterstorevc==1){
            Session::flash('success','Se ha guardado '.$counterstorevc.' registro.<br><br>'.$counterwordsvc);
        }else{ 
         Session::flash('success','Se han guardado '.$counterstorevc.' registros.<br><br>'.$counterwordsvc);
        }
         }
    
     return back();
 } 

 public function storeintermediogramatica(Request $request)
 {
    $files = $request->file('foto');
    $destinationPath = 'C:\xampp\htdocs\intranet\storage\app\public\images';
    $links = $request->input('link');
    $bodys = $request->input('body');
    $counterstorevc=1;
        $counterwordsvc='';
    foreach($files as $file) {
        $filename = $file->getClientOriginalName();
        $upload_success = $file->move($destinationPath, $filename);
    $fotoarray[]=$filename;
    }
    $counter = 0;
    $counter2 = 0;
    foreach($links as $link) {
    $linkarray[]=$link;
    $counter++;
    }
    
    foreach($bodys as $body) {
        $bodyarray[]=$body;
        $counter2++;
        }
  
     $locale=App::getLocale();
     if(App::isLocale('en')){
        for($i=0;$i<$counter;$i++){
            $intermediogramatica = new Intermediogramatica;
           
            $intermediogramatica->foto=$fotoarray[$i];
            $intermediogramatica->link=$linkarray[$i];
//---31-01
$intermediogramatica->body=$bodyarray[$i];
//---31-01
            $intermediogramatica->save();
            $counterwordsvc=$counterwordsvc.$counterstorevc.') Image: '.$fotoarray[$i].'<br>Link: '.$linkarray[$i].'<br>Body: '.$bodyarray[$i]."<br><br>";
             $counterstorevc++;
        }
    
        $counterstorevc=$counterstorevc-1;
        if($counterstorevc==1){
            Session::flash('success','There is '.$counterstorevc.' record saved.<br><br>'.$counterwordsvc);
        }else{    
         Session::flash('success','There are '.$counterstorevc.' records saved.<br><br>'.$counterwordsvc);
        }
     }else{
        for($i=0;$i<$counter;$i++){
            $intermediogramatica = new Intermediogramatica;
           
            $intermediogramatica->foto=$fotoarray[$i];
            $intermediogramatica->link=$linkarray[$i];



            $intermediogramatica->body=$bodyarray[$i];
            $intermediogramatica->save();
            $counterwordsvc=$counterwordsvc.$counterstorevc.') Imagen: '.$fotoarray[$i].'<br>Enlace: '.$linkarray[$i].'<br>Contenido: '.$bodyarray[$i]."<br><br>";
             $counterstorevc++;
        }
    
        $counterstorevc=$counterstorevc-1;
        if($counterstorevc==1){
            Session::flash('success','Se ha guardado '.$counterstorevc.' registro.<br><br>'.$counterwordsvc);
        }else{ 
         Session::flash('success','Se han guardado '.$counterstorevc.' registros.<br><br>'.$counterwordsvc);
        }
         }
    
     return back();
 } 

 
 public function storeavanzadogramatica(Request $request)
 {
    $files = $request->file('foto');
    $destinationPath = 'C:\xampp\htdocs\intranet\storage\app\public\images';
    $links = $request->input('link');
    $bodys = $request->input('body');
    $counterstorevc=1;
        $counterwordsvc='';
    foreach($files as $file) {
        $filename = $file->getClientOriginalName();
        $upload_success = $file->move($destinationPath, $filename);
    $fotoarray[]=$filename;
    }
    $counter = 0;
    $counter2 = 0;
    foreach($links as $link) {
    $linkarray[]=$link;
    $counter++;
    }
    
    foreach($bodys as $body) {
        $bodyarray[]=$body;
        $counter2++;
        }
  
     $locale=App::getLocale();
     if(App::isLocale('en')){
        for($i=0;$i<$counter;$i++){
            $avanzadogramatica = new Avanzadogramatica;
           
            $avanzadogramatica->foto=$fotoarray[$i];
            $avanzadogramatica->link=$linkarray[$i];
//---31-01
$avanzadogramatica->body=$bodyarray[$i];
//---31-01
            $avanzadogramatica->save();
            $counterwordsvc=$counterwordsvc.$counterstorevc.') Image: '.$fotoarray[$i].'<br>Link: '.$linkarray[$i].'<br>Body: '.$bodyarray[$i]."<br><br>";
             $counterstorevc++;
        }
    
        $counterstorevc=$counterstorevc-1;
        if($counterstorevc==1){
            Session::flash('success','There is '.$counterstorevc.' record saved.<br><br>'.$counterwordsvc);
        }else{    
         Session::flash('success','There are '.$counterstorevc.' records saved.<br><br>'.$counterwordsvc);
        }
     }else{
        for($i=0;$i<$counter;$i++){
            $avanzadogramatica = new Avanzadogramatica;
           
            $avanzadogramatica->foto=$fotoarray[$i];
            $avanzadogramatica->link=$linkarray[$i];



            $avanzadogramatica->body=$bodyarray[$i];
            $avanzadogramatica->save();
            $counterwordsvc=$counterwordsvc.$counterstorevc.') Imagen: '.$fotoarray[$i].'<br>Enlace: '.$linkarray[$i].'<br>Contenido: '.$bodyarray[$i]."<br><br>";
             $counterstorevc++;
        }
    
        $counterstorevc=$counterstorevc-1;
        if($counterstorevc==1){
            Session::flash('success','Se ha guardado '.$counterstorevc.' registro.<br><br>'.$counterwordsvc);
        }else{ 
         Session::flash('success','Se han guardado '.$counterstorevc.' registros.<br><br>'.$counterwordsvc);
        }
         }
    
     return back();
 } 
 //--------------31-01----------------------------------------
 public function storevideo(Request $request)
 {
    $vids = $request->input('vid');
    $links = $request->input('link');
    $counterstorevc=1;
        $counterwordsvc='';
    foreach($vids as $vid) {
    $vidarray[]=$vid;
    }
    $counter = 0;
    foreach($links as $link) {
    $linkarray[]=$link;
    $counter++;
    }
     $locale=App::getLocale();
     if(App::isLocale('en')){
        for($i=0;$i<$counter;$i++){
            $video = new Video;
            $video->vid=$vidarray[$i];
            $video->link=$linkarray[$i];
            $video->save();
            $counterwordsvc=$counterwordsvc.$counterstorevc.') Video: '.$vidarray[$i].'<br>Body: '.$linkarray[$i]."<br>";
             $counterstorevc++;
        }
        $counterstorevc=$counterstorevc-1;
        if($counterstorevc==1){
            Session::flash('success','There is '.$counterstorevc.' record saved.<br><br><br>'.$counterwordsvc);
        }else{    
         Session::flash('success','There are '.$counterstorevc.' records saved.<br><br><br>'.$counterwordsvc);
        }
     }else{
        for($i=0;$i<$counter;$i++){
            $video = new Video;
            $video->vid=$vidarray[$i];
            $video->link=$linkarray[$i];
            $video->save();
            $counterwordsvc=$counterwordsvc.$counterstorevc.') Video: '.$vidarray[$i].'<br>Contenido: '.$linkarray[$i]."<br>";
             $counterstorevc++;
        }
        $counterstorevc=$counterstorevc-1;
        if($counterstorevc==1){
            Session::flash('success','Se ha guardado '.$counterstorevc.' registro.<br><br><br>'.$counterwordsvc);
        }else{ 
         Session::flash('success','Se han guardado '.$counterstorevc.' registros.<br><br><br>'.$counterwordsvc);
        }
         }
     return back();
 } 
 //------------------------------------------------------------------------------
 //------------------------------------------------------------------------------
 public function storeaudio(Request $request)
 {

    $rows = $request->input('rows');
    $counterstorevc=1;
        $counterwordsvc='';
        $locale=App::getLocale();
        if(App::isLocale('en')){
        foreach ($rows as $row)
{
    $audio = new Audio([
        'link' => $row['Link'],    
        'body' => $row['Body'],
    ]); 
        $audio->save();
        
        $counterwordsvc=$counterwordsvc.$counterstorevc.') Link: '.$row['Link'].'<br>Body: '.$row['Body'].'<br>';
         $counterstorevc++;
}
        $counterstorevc=$counterstorevc-1;
        if($counterstorevc==1){
        return redirect()->route('createaudio')->with('success', 'There is '.$counterstorevc.' record saved.<br><br><br>'.$counterwordsvc);
        }else{
            return redirect()->route('createaudio')->with('success', 'There are '.$counterstorevc.' records saved.<br><br><br>'.$counterwordsvc);
        }

}else{
    foreach ($rows as $row)
{
    $audio = new Audio([
        'link' => $row['Link'], 
        'body' => $row['Body'],
    ]); 
        $audio->save();
        $counterwordsvc=$counterwordsvc.$counterstorevc.') Enlace: '.$row['Link'].'<br>Contenido: '.$row['Body'].'<br>';
         $counterstorevc++;
}
        $counterstorevc=$counterstorevc-1;
        if($counterstorevc==1){
            return redirect()->route('createaudio')->with('success', 'Se ha guardado '.$counterstorevc.' registro.<br><br><br>'.$counterwordsvc);
            }else{
                return redirect()->route('createaudio')->with('success', 'Se han guardado '.$counterstorevc.' registros.<br><br><br>'.$counterwordsvc);
            }
}

 }
 //------------------------------------------------------------------------------
 public function storelibro(Request $request)
 {

    $rows = $request->input('rows');

    $counterstorevc=1;
        $counterwordsvc='';
        $locale=App::getLocale();
        if(App::isLocale('en')){
        foreach ($rows as $row)
{
    $libro = new Libro([
        'titulo' => $row['Titulo'],
        'body' => $row['Body'],
        'link' => $row['Link'],    
    ]); 
        $libro->save();
        
        $counterwordsvc=$counterwordsvc.$counterstorevc.') Title: '.$row['Titulo'].' <br>Body: '.$row['Body'].' Link: '.$row['Link'].'<br><br>';
         $counterstorevc++;
         
}
        $counterstorevc=$counterstorevc-1;
        if($counterstorevc==1){
        return redirect()->route('createlibro')->with('success', 'There is '.$counterstorevc.' record saved.<br><br>'.$counterwordsvc);
        }else{
            return redirect()->route('createlibro')->with('success', 'There are '.$counterstorevc.' records saved.<br><br>'.$counterwordsvc);
    
        }

}else{
    foreach ($rows as $row)
{
    $libro = new Libro([
        'titulo' => $row['Titulo'],
        'body' => $row['Body'],
        'link' => $row['Link'],    
    ]); 
        $libro->save();
        
        $counterwordsvc=$counterwordsvc.$counterstorevc.') Título: '.$row['Titulo'].' <br>Contenido: '.$row['Body'].' Enlace: '.$row['Link'].'<br><br>';
         $counterstorevc++;
         
}
        $counterstorevc=$counterstorevc-1;
        //return redirect()->route('createlibro')->with('success', 'Your task added successfully: '.$counterstorevc.'<br><br>'.$counterwordsvc);
        if($counterstorevc==1){
            return redirect()->route('createlibro')->with('success', 'Se ha guardado '.$counterstorevc.' registro.<br><br>'.$counterwordsvc);
            }else{
                return redirect()->route('createlibro')->with('success', 'Se han guardado '.$counterstorevc.' registros.<br><br>'.$counterwordsvc);
        
            }
}
  
 } 
 //------------------------------------------------------------------------------
 public function storerevista(Request $request)
 {

    $rows = $request->input('rows');

    $counterstorevc=1;
        $counterwordsvc='';
        $locale=App::getLocale();
        if(App::isLocale('en')){
        foreach ($rows as $row)
{
    $revista = new Revista([
        'titulo' => $row['Titulo'],
        'body' => $row['Body'],
        'link' => $row['Link'],    
    ]); 
        $revista->save();
        
        $counterwordsvc=$counterwordsvc.$counterstorevc.') Title: '.$row['Titulo'].' <br>Body: '.$row['Body'].' Link: '.$row['Link'].'<br><br>';
         $counterstorevc++;
         
}
        $counterstorevc=$counterstorevc-1;
        if($counterstorevc==1){
        return redirect()->route('createrevista')->with('success', 'There is '.$counterstorevc.' record saved.<br><br>'.$counterwordsvc);
        }else{
            return redirect()->route('createrevista')->with('success', 'There are '.$counterstorevc.' records saved.<br><br>'.$counterwordsvc);
    
        }

}else{
    foreach ($rows as $row)
{
    $revista = new Revista([
        'titulo' => $row['Titulo'],
        'body' => $row['Body'],
        'link' => $row['Link'],    
    ]); 
        $revista->save();
        
        $counterwordsvc=$counterwordsvc.$counterstorevc.') Título: '.$row['Titulo'].' <br>Contenido: '.$row['Body'].' Enlace: '.$row['Link'].'<br><br>';
         $counterstorevc++;
         
}
        $counterstorevc=$counterstorevc-1;
        if($counterstorevc==1){
            return redirect()->route('createrevista')->with('success', 'Se ha guardado '.$counterstorevc.' registro.<br><br>'.$counterwordsvc);
            }else{
                return redirect()->route('createrevista')->with('success', 'Se han guardado '.$counterstorevc.' registros.<br><br>'.$counterwordsvc);
        
            }
}
  
 }


 //------------------------------------------------------------------------------
    public function storevcurso(Request $request)
{
        $rows = $request->input('rows');
        $counterstorevc=1;
        $counterwordsvc='';
        $locale=App::getLocale();
        if(App::isLocale('en')){
        foreach ($rows as $row)
{
    $vcurso = new Vcurso([
        'titulo' => $row['Titulo'],
        'body' => $row['Body'],
        'link' => $row['Link'],    
    ]); 
        $vcurso->save();
        $counterwordsvc=$counterwordsvc.$counterstorevc.') Title: '.$row['Titulo'].'<br> Body: '.$row['Body'].' Link: '.$row['Link'].'<br><br>';
         $counterstorevc++; 
}
        $counterstorevc=$counterstorevc-1;
        if($counterstorevc==1){
    return redirect()->route('createvcurso')->with('success', 'There is '.$counterstorevc.' record saved.<br><br>'.$counterwordsvc);    
}else{    
         return redirect()->route('createvcurso')->with('success', 'There are '.$counterstorevc.' records saved.<br><br>'.$counterwordsvc);
        }
}else{
    foreach ($rows as $row)
    {
        $vcurso = new Vcurso([
            'titulo' => $row['Titulo'],
            'body' => $row['Body'],
            'link' => $row['Link'],    
        ]); 
            $vcurso->save();  
            $counterwordsvc=$counterwordsvc.$counterstorevc.') Título: '.$row['Titulo'].'<br> Contenido: '.$row['Body'].' Enlace: '.$row['Link'].'<br><br>';
             $counterstorevc++;  
    }
            $counterstorevc=$counterstorevc-1;
    if($counterstorevc==1){
    return redirect()->route('createvcurso')->with('success', 'Se ha guardado '.$counterstorevc.' registro.<br><br>'.$counterwordsvc);    
}else{    
         return redirect()->route('createvcurso')->with('success', 'Se han guardado '.$counterstorevc.' registros.<br><br>'.$counterwordsvc);
        }
}

    }
//---------------------------------------------------------------------------------------------------------------------------------------------
public function storebasicopregunta(Request $request)
{
        $rows = $request->input('rows');
        $counterstorevc=1;
        $counterwordsvc='';
        $locale=App::getLocale();
        if(App::isLocale('en')){
        foreach ($rows as $row)
{
    $basicopregunta = new Basicopregunta([
        'question' => $row['Body'],
        'option1' => $row['Option1'],
        'option2' => $row['Option2'],
        'option3' => $row['Option3'],
        'option4' => $row['Option4'],
        'answer' => $row['Answer'],
        //'link' => $row['Link'],    
    ]); 
        $basicopregunta->save();
        $counterwordsvc=$counterwordsvc.$counterstorevc.') Question: '.$row['Body'].'<br>Option 1: '.$row['Option1'].'<br>Option 2: '.$row['Option2'].'<br>Option 3: '.$row['Option3'].'<br>Option 4: '.$row['Option4'].'<br>Answer: '.$row['Answer'].'<br><br>';
         $counterstorevc++; 
}
        $counterstorevc=$counterstorevc-1;
        if($counterstorevc==1){
    return redirect()->route('createbasicopregunta')->with('success', 'There is '.$counterstorevc.' record saved.<br><br>'.$counterwordsvc);    
}else{    
         return redirect()->route('createbasicopregunta')->with('success', 'There are '.$counterstorevc.' records saved.<br><br>'.$counterwordsvc);
        }
}else{
    foreach ($rows as $row)
    {
        $basicopregunta = new Basicopregunta([            
            'question' => $row['Body'],
        'option1' => $row['Option1'],
        'option2' => $row['Option2'],
        'option3' => $row['Option3'],
        'option4' => $row['Option4'],
        'answer' => $row['Answer'], 
        ]); 
        $basicopregunta->save();            
        $counterwordsvc=$counterwordsvc.$counterstorevc.') Pregunta: '.$row['Body'].'<br>Opción 1: '.$row['Option1'].'<br>Opción 2: '.$row['Option2'].'<br>Opción 3: '.$row['Option3'].'<br>Opción 4: '.$row['Option4'].'<br>Respuesta: '.$row['Answer'].'<br><br>';
             $counterstorevc++;  
    }
            $counterstorevc=$counterstorevc-1;
    if($counterstorevc==1){
    return redirect()->route('createbasicopregunta')->with('success', 'Se ha guardado '.$counterstorevc.' registro.<br><br>'.$counterwordsvc);    
}else{    
         return redirect()->route('createbasicopregunta')->with('success', 'Se han guardado '.$counterstorevc.' registros.<br><br>'.$counterwordsvc);
        }
}

    }










    public function storeintermediopregunta(Request $request)
{
        $rows = $request->input('rows');
        $counterstorevc=1;
        $counterwordsvc='';
        $locale=App::getLocale();
        if(App::isLocale('en')){
        foreach ($rows as $row)
{
    $intermediopregunta = new Intermediopregunta([
        'question' => $row['Body'],
        'option1' => $row['Option1'],
        'option2' => $row['Option2'],
        'option3' => $row['Option3'],
        'option4' => $row['Option4'],
        'answer' => $row['Answer'],
        //'link' => $row['Link'],    
    ]); 
        $intermediopregunta->save();
        $counterwordsvc=$counterwordsvc.$counterstorevc.') Question: '.$row['Body'].'<br>Option 1: '.$row['Option1'].'<br>Option 2: '.$row['Option2'].'<br>Option 3: '.$row['Option3'].'<br>Option 4: '.$row['Option4'].'<br>Answer: '.$row['Answer'].'<br><br>';
         $counterstorevc++; 
}
        $counterstorevc=$counterstorevc-1;
        if($counterstorevc==1){
    return redirect()->route('createintermediopregunta')->with('success', 'There is '.$counterstorevc.' record saved.<br><br>'.$counterwordsvc);    
}else{    
         return redirect()->route('createintermediopregunta')->with('success', 'There are '.$counterstorevc.' records saved.<br><br>'.$counterwordsvc);
        }
}else{
    foreach ($rows as $row)
    {
        $intermediopregunta = new Intermediopregunta([            
            'question' => $row['Body'],
        'option1' => $row['Option1'],
        'option2' => $row['Option2'],
        'option3' => $row['Option3'],
        'option4' => $row['Option4'],
        'answer' => $row['Answer'], 
        ]); 
        $intermediopregunta->save();            
        $counterwordsvc=$counterwordsvc.$counterstorevc.') Pregunta: '.$row['Body'].'<br>Opción 1: '.$row['Option1'].'<br>Opción 2: '.$row['Option2'].'<br>Opción 3: '.$row['Option3'].'<br>Opción 4: '.$row['Option4'].'<br>Respuesta: '.$row['Answer'].'<br><br>';
             $counterstorevc++;  
    }
            $counterstorevc=$counterstorevc-1;
    if($counterstorevc==1){
    return redirect()->route('createintermediopregunta')->with('success', 'Se ha guardado '.$counterstorevc.' registro.<br><br>'.$counterwordsvc);    
}else{    
         return redirect()->route('createintermediopregunta')->with('success', 'Se han guardado '.$counterstorevc.' registros.<br><br>'.$counterwordsvc);
        }
}

    }



























    


    public function storeavanzadopregunta(Request $request)
{
        $rows = $request->input('rows');
        $counterstorevc=1;
        $counterwordsvc='';
        $locale=App::getLocale();
        if(App::isLocale('en')){
        foreach ($rows as $row)
{
    $avanzadopregunta = new Avanzadopregunta([
        'question' => $row['Body'],
        'option1' => $row['Option1'],
        'option2' => $row['Option2'],
        'option3' => $row['Option3'],
        'option4' => $row['Option4'],
        'answer' => $row['Answer'],
        //'link' => $row['Link'],    
    ]); 
        $avanzadopregunta->save();
        $counterwordsvc=$counterwordsvc.$counterstorevc.') Question: '.$row['Body'].'<br>Option 1: '.$row['Option1'].'<br>Option 2: '.$row['Option2'].'<br>Option 3: '.$row['Option3'].'<br>Option 4: '.$row['Option4'].'<br>Answer: '.$row['Answer'].'<br><br>';
         $counterstorevc++; 
}
        $counterstorevc=$counterstorevc-1;
        if($counterstorevc==1){
    return redirect()->route('createavanzadopregunta')->with('success', 'There is '.$counterstorevc.' record saved.<br><br>'.$counterwordsvc);    
}else{    
         return redirect()->route('createavanzadopregunta')->with('success', 'There are '.$counterstorevc.' records saved.<br><br>'.$counterwordsvc);
        }
}else{
    foreach ($rows as $row)
    {
        $avanzadopregunta = new Avanzadopregunta([            
            'question' => $row['Body'],
        'option1' => $row['Option1'],
        'option2' => $row['Option2'],
        'option3' => $row['Option3'],
        'option4' => $row['Option4'],
        'answer' => $row['Answer'], 
        ]); 
        $avanzadopregunta->save();            
        $counterwordsvc=$counterwordsvc.$counterstorevc.') Pregunta: '.$row['Body'].'<br>Opción 1: '.$row['Option1'].'<br>Opción 2: '.$row['Option2'].'<br>Opción 3: '.$row['Option3'].'<br>Opción 4: '.$row['Option4'].'<br>Respuesta: '.$row['Answer'].'<br><br>';
             $counterstorevc++;  
    }
            $counterstorevc=$counterstorevc-1;
    if($counterstorevc==1){
    return redirect()->route('createavanzadopregunta')->with('success', 'Se ha guardado '.$counterstorevc.' registro.<br><br>'.$counterwordsvc);    
}else{    
         return redirect()->route('createavanzadopregunta')->with('success', 'Se han guardado '.$counterstorevc.' registros.<br><br>'.$counterwordsvc);
        }
}

    }
















    public function storechatbot(Request $request)
    {
            $rows = $request->input('rows');
            $counterstorevc=1;
            $counterwordsvc='';
            $locale=App::getLocale();
            if(App::isLocale('en')){
            foreach ($rows as $row)
    {
        $chatbot = new Chatbot([
            'palabraclave' => $row['Body'],
            'respuesta' => $row['Body2'],
            //'link' => $row['Link'],    
        ]); 
            $chatbot->save();
            $counterwordsvc=$counterwordsvc.$counterstorevc.') Keyword: '.$row['Body'].'<br>Answer: '.$row['Body2'].'<br><br>';
             $counterstorevc++; 
    }
            $counterstorevc=$counterstorevc-1;
            if($counterstorevc==1){
        return redirect()->route('createchatbot')->with('success', 'There is '.$counterstorevc.' record saved.<br><br>'.$counterwordsvc);    
    }else{    
             return redirect()->route('createchatbot')->with('success', 'There are '.$counterstorevc.' records saved.<br><br>'.$counterwordsvc);
            }
    }else{
        foreach ($rows as $row)
        {
            $chatbot = new Chatbot([            
                'palabraclave' => $row['Body'],
            'respuesta' => $row['Body2'], 
            ]); 
            $chatbot->save();            
            $counterwordsvc=$counterwordsvc.$counterstorevc.') Palabra clave: '.$row['Body'].'<br>Respuesta: '.$row['Body2'].'<br><br>';
                 $counterstorevc++;  
        }
                $counterstorevc=$counterstorevc-1;
        if($counterstorevc==1){
        return redirect()->route('createchatbot')->with('success', 'Se ha guardado '.$counterstorevc.' registro.<br><br>'.$counterwordsvc);    
    }else{    
             return redirect()->route('createchatbot')->with('success', 'Se han guardado '.$counterstorevc.' registros.<br><br>'.$counterwordsvc);
            }
    }
    
        }

        //----------5-2-2020
        public function storefrequentepregunta(Request $request)
        {
                $rows = $request->input('rows');
                $counterstorevc=1;
                $counterwordsvc='';
                $locale=App::getLocale();
                if(App::isLocale('en')){
                foreach ($rows as $row)
        {
            $frequentepregunta = new Frequentepregunta([
                'pregunta_es' => $row['Body'],
                'respuesta_es' => $row['Body2'],
                //'link' => $row['Link'],  
                'pregunta_en' => $row['Body3'],
                'respuesta_en' => $row['Body4'],  
            ]); 
                $frequentepregunta->save();
                $counterwordsvc=$counterwordsvc.$counterstorevc.') Question_ES: '.$row['Body'].'<br>Answer_ES: '.$row['Body2'].'<br>Question_EN: '.$row['Body3'].'<br>Answer_EN: '.$row['Body4'].'<br><br>';
                 $counterstorevc++; 
        }
                $counterstorevc=$counterstorevc-1;
                if($counterstorevc==1){
            return redirect()->route('createfrequentepregunta')->with('success', 'There is '.$counterstorevc.' record saved.<br><br>'.$counterwordsvc);    
        }else{    
                 return redirect()->route('createfrequentepregunta')->with('success', 'There are '.$counterstorevc.' records saved.<br><br>'.$counterwordsvc);
                }
        }else{
            foreach ($rows as $row)
            {
                $frequentepregunta = new Frequentepregunta([            
                    'pregunta_es' => $row['Body'],
                'respuesta_es' => $row['Body2'], 
                'pregunta_en' => $row['Body3'],
                'respuesta_en' => $row['Body4'], 
                ]); 
                $frequentepregunta->save();            
                $counterwordsvc=$counterwordsvc.$counterstorevc.') Pregunta_ES: '.$row['Body'].'<br>Respuesta_ES: '.$row['Body2'].'<br>Pregunta_EN: '.$row['Body3'].'<br>Respuesta_EN: '.$row['Body4'].'<br><br>';
                     $counterstorevc++;  
            }
                    $counterstorevc=$counterstorevc-1;
            if($counterstorevc==1){
            return redirect()->route('createfrequentepregunta')->with('success', 'Se ha guardado '.$counterstorevc.' registro.<br><br>'.$counterwordsvc);    
        }else{    
                 return redirect()->route('createfrequentepregunta')->with('success', 'Se han guardado '.$counterstorevc.' registros.<br><br>'.$counterwordsvc);
                }
        }
        
            }   
        //----------5-2-2020
        /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $post = Post::find($id);
        
        return view('front.blog2.edit')->with('post',$post);
    
    }

    public function edit_an($id)
    {
        //
        $anuncio = Anuncio::find($id);
        
        return view('front.blog2.edit_an')->with('anuncio',$anuncio);
    
    }

    public function editbanner($id)
    {
        //
        $banner = Banner::find($id);
        
        return view('front.blog2.editbanner')->with('banner',$banner);
    
    }

    
    public function editbasicogramatica($id)
    {
        //
        $basicogramatica = Basicogramatica::find($id);
        
        return view('front.blog2.editbasicogramatica')->with('basicogramatica',$basicogramatica);
    
    }

    
    public function editintermediogramatica($id)
    {
        //
        $intermediogramatica = Intermediogramatica::find($id);
        
        return view('front.blog2.editintermediogramatica')->with('intermediogramatica',$intermediogramatica);
    
    }

    
    public function editavanzadogramatica($id)
    {
        //
        $avanzadogramatica = Avanzadogramatica::find($id);
        
        return view('front.blog2.editavanzadogramatica')->with('avanzadogramatica',$avanzadogramatica);
    
    }

    public function editevent($id)
    {
        //
        $event = Event::find($id);
        
        return view('front.blog2.editevent')->with('event',$event);
        //return view('games', $data);
    
    }

    
    public function editmainicono($id)
    {
        //
        $mainicono = Mainicono::find($id);
        
        return view('front.blog2.editmainicono')->with('mainicono',$mainicono);
        //return view('games', $data);
    
    }


    public function editmainpage($id)
    {
        //
        $mainpage = Mainpage::find($id);
        
        return view('front.blog2.editmainpage')->with('mainpage',$mainpage);
        //return view('games', $data);
    
    }

    
    public function editmain2page($id)
    {
        //
        $main2page = Main2page::find($id);
        
        return view('front.blog2.editmain2page')->with('main2page',$main2page);
        //return view('games', $data);
    
    }

    public function editpatro($id)
    {
        //
        $patro = Patro::find($id);
        
        return view('front.blog2.editpatro')->with('patro',$patro);
        //return view('games', $data);
    
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'titulo'=> 'required',
            'body' => 'required'
          ]);
          
          //Create Post
          $post = Post::find($id);
          $post->titulo = $request->input('titulo');
          $post->body = $request->input('body');
          $post->save();
          return redirect('/blog/home')->with('success','Post actualizado!!');
      
    }

    public function update_an(Request $request, $id)
    {
        $this->validate($request,[
            'titulo'=> 'required|max:50',
            'body' => 'required',
            'fecha_vence' => 'required|date'
          ]);
          
          //Create Post
          $anuncio = Anuncio::find($id);
          $anuncio->titulo = $request->input('titulo');
          $anuncio->body = $request->input('body');
          $anuncio->fecha_vence = $request->input('fecha_vence');
          $anuncio->save();
          return redirect('/index/anuncios')->with('success','Anuncio actualizado!!');
      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post = Post::find($id);
        $post->delete();
        return redirect('/blog/home')->with('success','Post borrado!!');
    }

    public function destroy_an($id)
    {
        //
        $anuncio = Anuncio::find($id);
        $anuncio->delete();
        return redirect('/index/anuncios')->with('success','Anuncio borrado!!');
    }

    public function bloglogin(){
        return View('auth.login');
    }

    public function checklogin(Request $request)
    {
        $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required|alphaNum|min:3',
            'g-recaptcha-response' => new Captcha()
        ]);

        $user_data = array(
            'email' => $request->get('email'),
            'password' => $request->get('password')
        );

        if(Auth::attempt($user_data))
        {
            return redirect('/blog/home');
        }
        else{
            return back()->with('error','Error con el login');
        }
    }

    public function successlogin()
    {
        return view('front.blog2.home');
    }

    public function blogregister(){
        return View('auth.register');
    }

    public function storeuser(request $request){
        //print_r($request->input());
        $name=$request->input('name');
        $email=$request->input('email');
        $password=$request->input('password');
        DB::insert('insert into users(id,role,name,email,password) values(?,?,?,?,?)',[null,'user',$name,$email,$password]);
    }

    public function loger(request $request){
        $request->session()->put('password',$request->input('password'));
        return view('auth.successlogin')->with
        ('password',$request->session()->get('password'));
    }


    public function show_an($id)
    {
        $anuncio = Anuncio::find($id);
        return View('front.blog2.show_an')->with('anuncio',$anuncio);

        
    }

   


    /*
    public function cursosonline(){
        return View('front.cursosonline');
    }*/

    public function createvcurso()
    {
        return View('front.blog2.createvcurso');
    }

    public function createbasicopregunta()
    {
        return View('front.blog2.createbasicopregunta');
    }


    
    public function createintermediopregunta()
    {
        return View('front.blog2.createintermediopregunta');
    }

    
    public function createavanzadopregunta()
    {
        return View('front.blog2.createavanzadopregunta');
    }

    
    public function createchatbot()
    {
        return View('front.blog2.createchatbot');
    }

    
    public function createfrequentepregunta()
    {
        return View('front.blog2.createfrequentepregunta');
    }

    public function createrevista()
    {
        return View('front.blog2.createrevista');
    }

    public function createlibro()
    {
        return View('front.blog2.createlibro');
    }

    public function createaudio()
    {
        return View('front.blog2.createaudio');
    }

    public function storevcurso2(Request $request)
    {

        if($request->ajax())
        {
            $rules=array(
                'titulo.*' => 'required',
                'body.*'=>'required',
                'link.*'=>'required'

            );

            $error=Validator::make($request->all(),$rules);
            if($error->fails())
            {
                return response()->json([
                    'error'=>$error->errors()->all()
                ]);
            }
            $titulo=$request->titulo;
            $body=$request->body;
            $link=$request->link;

            for($count=0;$count<count($titulo);$count++)
            {
                $data=array(
                    'titulo'=>$titulo[$count],
                    'body'=>$body[$count],
                    'link'=>$link[$count]
                );

                $insert_data[]=$data;
            }

            Vcurso::insert($insert_data);
            return response()->json([
                'success'=>'Data agregada'
            ]);
        }
      
    
    }

    public function editvcurso($id)
    {
        //
        $vcurso = Vcurso::find($id);
        
        return view('front.blog2.editvcurso')->with('vcurso',$vcurso);
    
    }

   //---------------------23_12------------------------------------------------------------------------

   public function createmainpage()
   {
       
   
       return View('front.blog2.createmainpage');
   }

   //---------------store body MAIN PAGE-------------------------------------------------------------
 public function storemainpage(Request $request)
 {
     $this->validate($request,[
       'body_es' => 'required',
       'foto_es' =>'image|nullable',
       'body_en' => 'required',
       'foto_en' =>'image|nullable',
     ]);
     
     if($request->hasFile('foto_es')){
         $filenameWithExt=$request->file('foto_es')->getClientOriginalName();
         $filename=pathinfo($filenameWithExt,PATHINFO_FILENAME);
         $extension=$request->file('foto_es')->getClientOriginalExtension();
         $fileNameToStore=$filename.'_'.time().'.'.$extension;
         $path=$request->file('foto_es')->storeAs('public/images',$fileNameToStore);
     }else{
         $fileNameToStore='noimage.jpg';
     }

     if($request->hasFile('foto_en')){
        $filenameWithExt=$request->file('foto_en')->getClientOriginalName();
        $filename2=pathinfo($filenameWithExt,PATHINFO_FILENAME);
        $extension=$request->file('foto_en')->getClientOriginalExtension();
        $fileNameToStore2=$filename2.'_'.time().'.'.$extension;
        $path=$request->file('foto_en')->storeAs('public/images',$fileNameToStore2);
    }else{
        $fileNameToStore2='noimage.jpg';
    }
     //Create Post
     $mainpage = new Mainpage;
     
     $mainpage->body_es = $request->input('body_es');
     $mainpage->foto_es=$fileNameToStore;
    
     $mainpage->body_en = $request->input('body_en');
     $mainpage->foto_en=$fileNameToStore2;

     $mainpage->save();
     //return redirect('/blog/home')->with('success','Post Created!!');
     Session::flash('success','El mp fue enviado exitosamente');

     //return redirect('/index/anuncios')->with('success','¡Anuncio creado!');
     return redirect()->route('create')->with('success','¡Post creado!');
 }

 
 //------------------------------------------------------------------------------


 //------16-2-2020------------------------------------------------------------------------

 public function createmain2page()
 {
     
 
     return View('front.blog2.createmain2page');
 }

 //---------------store body MAIN PAGE-------------------------------------------------------------
public function storemain2page(Request $request)
{
   $this->validate($request,[
     'body_es' => 'required',
     'foto_es' =>'image|nullable',
     'body_en' => 'required',
     'foto_en' =>'image|nullable',
     'si_primero_foto'=>'nullable'
   ]);
   
   if($request->hasFile('foto_es')){
       $filenameWithExt=$request->file('foto_es')->getClientOriginalName();
       $filename=pathinfo($filenameWithExt,PATHINFO_FILENAME);
       $extension=$request->file('foto_es')->getClientOriginalExtension();
       $fileNameToStore=$filename.'_'.time().'.'.$extension;
       $path=$request->file('foto_es')->storeAs('public/images',$fileNameToStore);
   }else{
       $fileNameToStore='noimage.jpg';
   }

   if($request->hasFile('foto_en')){
      $filenameWithExt=$request->file('foto_en')->getClientOriginalName();
      $filename2=pathinfo($filenameWithExt,PATHINFO_FILENAME);
      $extension=$request->file('foto_en')->getClientOriginalExtension();
      $fileNameToStore2=$filename2.'_'.time().'.'.$extension;
      $path=$request->file('foto_en')->storeAs('public/images',$fileNameToStore2);
  }else{
      $fileNameToStore2='noimage.jpg';
  }
   //Create Post
   $main2page = new Main2page;
   
   $main2page->body_es = $request->input('body_es');
   $main2page->foto_es=$fileNameToStore;
  
   $main2page->body_en = $request->input('body_en');
   $main2page->foto_en=$fileNameToStore2;
//new
$main2page->si_primero_foto = $request->input('si_primero_foto');
//new
   $main2page->save();
   //return redirect('/blog/home')->with('success','Post Created!!');
   Session::flash('success','El mp fue enviado exitosamente');

   //return redirect('/index/anuncios')->with('success','¡Anuncio creado!');
   return redirect()->route('create')->with('success','¡Post creado!');
}
  //------16-2-2020------------------------------------------------------------------------

}
