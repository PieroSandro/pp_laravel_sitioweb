<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;


Use Faker\Provider\Image;
use Illuminate\Support\Facades\Hash;

use App\Vcurso;
use App;
Use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

Use Illuminate\Support\Facades\Storage;
use PDF;
Use Illuminate\Http\Response;
use Auth;

use Validator;

use App\Notifications\SendContactNotification;
use Illuminate\Support\Facades\Notification;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class VcursoController extends Controller
{
    
    
    public function updatevcurso($id,Request $request)
    {
        
    $vcurso = Vcurso::findOrFail($request->id);
    $vcurso->update($request->all());
    $locale=App::getLocale();
    $userna = $request->input('idc');

    $titulocurso = $request->input('titulo');
    $bodycurso = $request->input('body');
    $linkcurso = $request->input('link');

    if(App::isLocale('en')){
    Session::flash('success','The online course in the row '.$userna.' was updated!<br><br>ID: '.$request->id.'<br>Title: '.$titulocurso.'<br>Body: '.$bodycurso.'<br>Link: '.$linkcurso);
        return back();
    }else{
        Session::flash('success','¡El curso virtual de la fila '.$userna.' fue actualizado!<br><br>ID: '.$request->id.'<br>Título: '.$titulocurso.'<br>Contenido: '.$bodycurso.'<br>Enlace: '.$linkcurso);
        return back();

    }
    
    }



    public function destroyvcurso(Request $request)
    {
        
        $vcurso = Vcurso::findOrFail($request->id);
        $titulocurso=$vcurso->titulo;
        $bodycurso=$vcurso->body;
        $linkcurso=$vcurso->link;
        $vcurso->delete();
        $locale=App::getLocale();
        //--------------------------
        $userna = $request->input('idc2');
        if(App::isLocale('en')){
            Session::flash('success','The online course in the row '.$userna.' was deleted!<br><br>ID: '.$request->id.'<br>Title: '.$titulocurso.'<br>Body: '.$bodycurso.'<br>Link: '.$linkcurso.'<br>');
        }else{
        Session::flash('success','¡El curso virtual de la fila '.$userna.' fue eliminado!<br><br>ID: '.$request->id.'<br>Título: '.$titulocurso.'<br>Contenido: '.$bodycurso.'<br>Enlace: '.$linkcurso.'<br>');}

    
        return back();
        
        }

    public function destroyvcursomore(Request $request)
        {
            
            $delid=$request->input('delid');
            $ccc = $request->input('ccc');

            $counterdeletevcmore=1;
            $counterrowsdelete=' ';

            if(App::isLocale('en')){  
            foreach($delid as $del)
            {
                $vcurso = Vcurso::find($del);
                $titulocurso=$vcurso->titulo;
                $bodycurso=$vcurso->body;
                $linkcurso=$vcurso->link;
               
                $counterrowsdelete=$counterrowsdelete.$counterdeletevcmore.') ID: '.$del.' Title: '.$titulocurso.'<br>Body: '.$bodycurso.'<br>Link: '.$linkcurso."<br>";
                $counterdeletevcmore++;
            }
            $counterdeletevcmore=$counterdeletevcmore-1;

            if($counterdeletevcmore==1){
                Vcurso::whereIn('id',$delid)->delete();
                Session::flash('success','There is '.$counterdeletevcmore.' record deleted.<br><br><br>'.$counterrowsdelete);
               
            }else{  
                Vcurso::whereIn('id',$delid)->delete();  
             Session::flash('success','There are '.$counterdeletevcmore.' records deleted.<br><br><br>'.$counterrowsdelete);
             
            }

            return back();
        }else{

            foreach($delid as $del)
            {
                $vcurso = Vcurso::find($del);
                $titulocurso=$vcurso->titulo;
                $bodycurso=$vcurso->body;
                $linkcurso=$vcurso->link;
               
                $counterrowsdelete=$counterrowsdelete.$counterdeletevcmore.') ID: '.$del.' Título: '.$titulocurso.'<br>Contenido: '.$bodycurso.'<br>Enlace: '.$linkcurso."<br>";
                $counterdeletevcmore++;
            }
         
            
            $counterdeletevcmore=$counterdeletevcmore-1;

            if($counterdeletevcmore==1){
                Vcurso::whereIn('id',$delid)->delete();
                Session::flash('success','Se ha eliminado '.$counterdeletevcmore.' registro.<br><br><br>'.$counterrowsdelete);
               
            }else{  
                Vcurso::whereIn('id',$delid)->delete();  
             Session::flash('success','Se han eliminado '.$counterdeletevcmore.' registros.<br><br><br>'.$counterrowsdelete);
             
            }

            return back();






        }
            }
    
}