<!--
	php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Persona;
use App\Rules\Captcha;

class LoginController extends Controller
{
    public function Registrar(Request $request)
    {
    	$this->validate($request,[
    		'cod_al' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:5', 'confirmed'],
            'g-recaptcha-response' => new Captcha(),
    	]);

    	$cod_p = $request['cod_al'];
        $existe = Persona::select('id')->where('cod_p',$cod_p)->exists();
        $id = Persona::where('cod_p',$cod_p)->value('id');
        $existe2 = User::select('id')->where('persona_id',$id)->exists();
        
        if ($existe) {
        	if(!$existe2){
        		User::create([
	            'persona_id' => $id,
	            'email' => $request['email'],
	            'password' => Hash::make($request['password']),
	        	]);
            	return redirect()->route('register')->with(['message-success' => 'Gracias por registrarte: Ya puedes acceder al sistema']);
        	}else{
        		return redirect()->route('register')->with(['message-warning' => 'Lo sentimos: El código que está intentando registrar ya está siendo utilizada por otro estudiante. No insistir.']);
        	}
            
           
        }else{
            return redirect()->route('register')->with(['message-danger' => 'Lo sentimos: Usted no esta registrado como alumno en el sistema']);
        }
    }
}
-->
