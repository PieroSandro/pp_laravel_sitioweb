<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App;

Use Faker\Provider\Image;
use Illuminate\Support\Facades\Hash;

use App\Main2page;

Use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

Use Illuminate\Support\Facades\Storage;
use PDF;
Use Illuminate\Http\Response;
use Auth;

use Validator;

use App\Notifications\SendContactNotification;
use Illuminate\Support\Facades\Notification;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class Main2pageController extends Controller
{
    
    

        public function updatemain2page($id,Request $request)
    {
        $this->validate($request,[
           
            'body_es' => 'required',
            'body_en' => 'required',
            //'si_primero_foto'=>'nullable'
            'cuadrocheck'=>'nullable'
          ]);
          
          if($request->hasFile('foto_es')){
            $filenameWithExt=$request->file('foto_es')->getClientOriginalName();
            $filename=pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension=$request->file('foto_es')->getClientOriginalExtension();
            $fileNameToStore=$filename.'_'.time().'.'.$extension;
            $path=$request->file('foto_es')->storeAs('public/images',$fileNameToStore);
        }

        if($request->hasFile('foto_en')){
            $filenameWithExt=$request->file('foto_en')->getClientOriginalName();
            $filename2=pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension=$request->file('foto_en')->getClientOriginalExtension();
            $fileNameToStore2=$filename2.'_'.time().'.'.$extension;
            $path=$request->file('foto_en')->storeAs('public/images',$fileNameToStore2);
        }

         
          $main2page = Main2page::find($id);
         
          $main2page->body_es = $request->input('body_es');
          $main2page->body_en = $request->input('body_en');
          //$main2page->si_primero_foto = $request->input('si_primero_foto');
          $main2page->si_primero_foto = $request->input('cuadrocheck');
          if($request->hasFile('foto_es')){
              $main2page->foto_es=$fileNameToStore;
          }

          if($request->hasFile('foto_en')){
            $main2page->foto_en=$fileNameToStore2;
        }
          $main2page->save();
         
          $locale=App::getLocale();
          if(App::isLocale('en')){
              Session::flash('success','The section was updated');   
              ?>    
          <?php 
          }else{
          Session::flash('success','La sección fue actualizada');}
        
          return back()->with('main2page',$main2page);
    }



}