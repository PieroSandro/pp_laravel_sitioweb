<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App;

Use Faker\Provider\Image;
use Illuminate\Support\Facades\Hash;

use App\Patro;

Use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

Use Illuminate\Support\Facades\Storage;
use PDF;
Use Illuminate\Http\Response;
use Auth;

use Validator;

use App\Notifications\SendContactNotification;
use Illuminate\Support\Facades\Notification;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class PatroController extends Controller
{
    
    

    

    

    

    public function updatepatro($id,Request $request)
    {
        $this->validate($request,[
            'titulo' => 'required',
            'body' => 'required',
            
          ]);
          
          if($request->hasFile('foto')){
            $filenameWithExt=$request->file('foto')->getClientOriginalName();
            $filename=pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension=$request->file('foto')->getClientOriginalExtension();
            $fileNameToStore=$filename.'_'.time().'.'.$extension;
            $path=$request->file('foto')->storeAs('public/images',$fileNameToStore);
        }


          //Create Post
          $patro = Patro::find($id);
        
          $patro->titulo = $request->input('titulo');
          $patro->body = $request->input('body');

          if($request->hasFile('foto')){
              $patro->foto=$fileNameToStore;
          }
          $patro->save();
          //Session::flash('success','¡El event fue actualizado!');
          $locale=App::getLocale();
          if(App::isLocale('en')){
              Session::flash('success','The sponsor was updated');   
              ?>    
          <?php 
          }else{
          Session::flash('success','El patrocinador fue actualizado');}
          //return view('front.blog2.editevent')->with('event',$event);
          return back()->with('patro',$patro);
    }




  
}
