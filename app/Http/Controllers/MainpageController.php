<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App;

Use Faker\Provider\Image;
use Illuminate\Support\Facades\Hash;

use App\Mainpage;

Use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

Use Illuminate\Support\Facades\Storage;
use PDF;
Use Illuminate\Http\Response;
use Auth;

use Validator;

use App\Notifications\SendContactNotification;
use Illuminate\Support\Facades\Notification;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class MainpageController extends Controller
{
    
    

        public function updatemainpage($id,Request $request)
    {
        $this->validate($request,[
           
            'body_es' => 'required',
            'body_en' => 'required'
          ]);
          
          if($request->hasFile('foto_es')){
            $filenameWithExt=$request->file('foto_es')->getClientOriginalName();
            $filename=pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension=$request->file('foto_es')->getClientOriginalExtension();
            $fileNameToStore=$filename.'_'.time().'.'.$extension;
            $path=$request->file('foto_es')->storeAs('public/images',$fileNameToStore);
        }

        if($request->hasFile('foto_en')){
            $filenameWithExt=$request->file('foto_en')->getClientOriginalName();
            $filename2=pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension=$request->file('foto_en')->getClientOriginalExtension();
            $fileNameToStore2=$filename2.'_'.time().'.'.$extension;
            $path=$request->file('foto_en')->storeAs('public/images',$fileNameToStore2);
        }

          //Create Post
          $mainpage = Mainpage::find($id);
         
          $mainpage->body_es = $request->input('body_es');
          $mainpage->body_en = $request->input('body_en');

          if($request->hasFile('foto_es')){
              $mainpage->foto_es=$fileNameToStore;
          }

          if($request->hasFile('foto_en')){
            $mainpage->foto_en=$fileNameToStore2;
        }
          $mainpage->save();
          //Session::flash('success','¡El event fue actualizado!');
          $locale=App::getLocale();
          if(App::isLocale('en')){
              Session::flash('success','The section was updated');   
              ?>    
          <?php 
          }else{
          Session::flash('success','La sección fue actualizada');}
          //return view('front.blog2.editevent')->with('event',$event);
          return back()->with('mainpage',$mainpage);
    }



}