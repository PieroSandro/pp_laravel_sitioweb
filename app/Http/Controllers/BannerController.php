<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;


Use Faker\Provider\Image;
use Illuminate\Support\Facades\Hash;
use App;
use App\Banner;

Use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

Use Illuminate\Support\Facades\Storage;
use PDF;
Use Illuminate\Http\Response;
use Auth;

use Validator;

use App\Notifications\SendContactNotification;
use Illuminate\Support\Facades\Notification;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class BannerController extends Controller
{
    
 

    public function updatebanner($id,Request $request)
    {
        $this->validate($request,[
            'link' => 'required'
          ]);
          
          if($request->hasFile('foto')){
            $filenameWithExt=$request->file('foto')->getClientOriginalName();
            $filename=pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension=$request->file('foto')->getClientOriginalExtension();
            $fileNameToStore=$filename.'_'.time().'.'.$extension;
            $path=$request->file('foto')->storeAs('public/images',$fileNameToStore);
        }


          $banner = Banner::find($id);
          $banner->link = $request->input('link');

          if($request->hasFile('foto')){
              $banner->foto=$fileNameToStore;
          }
          $banner->save();
          $locale=App::getLocale();
          if(App::isLocale('en')){
              Session::flash('success','The banner was updated!');
          }else{
          Session::flash('success','¡El banner fue actualizado!');}
          return back()->with('banner',$banner);
    }



    public function destroybanner(Request $request)
    {
        
        $banner = Banner::findOrFail($request->id);
        $linkbanner=$banner->link;
        Storage::delete('public/images/'.$banner->foto);
        $banner->delete();
        $locale=App::getLocale();
        $userna = $request->input('idc2');
        if(App::isLocale('en')){
            Session::flash('success','The online course in the row '.$userna.' was deleted!<br><br>Id: '.$request->id.'<br>Photo: '.$banner->foto.'<br>Link: '.$linkbanner.'<br>');
        }else{
        Session::flash('success','¡El curso virtual de la fila '.$userna.' fue eliminado!<br><br>Id: '.$request->id.'<br>Foto: '.$banner->foto.'<br>Enlace: '.$linkbanner.'<br>');}
        
        return back()->with('banner',$banner);
        
        }

            public function destroybannermore(Request $request)
            {
                $delid=$request->input('delid');
                $counterdeletebamore=1;
            $counterrowsdelete=' ';
            if(App::isLocale('en')){      
            //----------------------------------------------------------------------
                foreach($delid as $del)
        {
        $banner = Banner::find($del);
        $linkbanner=$banner->link;
        Storage::delete('public/images/'.$banner->foto);
        //$counterrowsdelete=$counterrowsdelete.$del.' '.$linkbanner;
        $counterrowsdelete=$counterrowsdelete.$counterdeletebamore.') ID: '.$del.'<br>Photo: '.$banner->foto.'<br>Link: '.$banner->link."<br><br>";        
        $counterdeletebamore++;
        }
        $counterdeletebamore=$counterdeletebamore-1;

        if($counterdeletebamore==1){
                Banner::whereIn('id',$delid)->delete();
                Session::flash('success','There is '.$counterdeletebamore.' record deleted.<br><br><br>'.$counterrowsdelete);
        }else{
            Banner::whereIn('id',$delid)->delete();
            Session::flash('success','There are '.$counterdeletebamore.' records deleted.<br><br><br>'. $counterrowsdelete);

        }

                return back();
                //---------------------------------------------------------------
    }else{

         //----------------------------------------------------------------------
         foreach($delid as $del)
         {  
         $banner = Banner::find($del);
         $linkbanner=$banner->link;
         Storage::delete('public/images/'.$banner->foto);
         //$counterrowsdelete=$counterrowsdelete.$del.' '.$linkbanner;
         $counterrowsdelete=$counterrowsdelete.$counterdeletebamore.') ID: '.$del.'<br>Foto: '.$banner->foto.'<br>Enlace: '.$banner->link."<br><br>";        
        $counterdeletebamore++;
         }
         $counterdeletebamore=$counterdeletebamore-1;

         if($counterdeletebamore==1){
            Banner::whereIn('id',$delid)->delete();
            Session::flash('success','Se ha eliminado '.$counterdeletebamore.' registro.<br><br><br>'.$counterrowsdelete);
         }else{
            Banner::whereIn('id',$delid)->delete();
            Session::flash('success','Se han eliminado '.$counterdeletebamore.' registros.<br><br><br>'. $counterrowsdelete);

         }

                 return back();
                 //---------------------------------------------------------------
    }

            }
}
