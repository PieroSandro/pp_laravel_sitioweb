<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;


Use Faker\Provider\Image;
use Illuminate\Support\Facades\Hash;
use App;
use App\Basicogramatica;

Use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

Use Illuminate\Support\Facades\Storage;
use PDF;
Use Illuminate\Http\Response;
use Auth;

use Validator;

use App\Notifications\SendContactNotification;
use Illuminate\Support\Facades\Notification;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class BasicogramaticaController extends Controller
{
    
 

    public function updatebasicogramatica($id,Request $request)
    {
        $this->validate($request,[
            'link' => 'required',
            'body' => 'required'
          ]);
          
          if($request->hasFile('foto')){
            $filenameWithExt=$request->file('foto')->getClientOriginalName();
            $filename=pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension=$request->file('foto')->getClientOriginalExtension();
            $fileNameToStore=$filename.'_'.time().'.'.$extension;
            $path=$request->file('foto')->storeAs('public/images',$fileNameToStore);
        }


          $basicogramatica = Basicogramatica::find($id);
          $basicogramatica->link = $request->input('link');
          $basicogramatica->body = $request->input('body');
          if($request->hasFile('foto')){
              $basicogramatica->foto=$fileNameToStore;
          }
          $basicogramatica->save();
          $locale=App::getLocale();
          if(App::isLocale('en')){
              Session::flash('success','The topic was updated!');
          }else{
          Session::flash('success','¡El tema fue actualizado!');}
          return back()->with('basicogramatica',$basicogramatica);
    }



    public function destroybasicogramatica(Request $request)
    {
        
        $basicogramatica = Basicogramatica::findOrFail($request->id);
        $linkbasicogramatica=$basicogramatica->link;
        $bodybasicogramatica=$basicogramatica->body;
        Storage::delete('public/images/'.$basicogramatica->foto);
        $basicogramatica->delete();
        $locale=App::getLocale();
        $userna = $request->input('idc2');
        if(App::isLocale('en')){
            Session::flash('success','The topic in the row '.$userna.' was deleted!<br><br>Id: '.$request->id.'<br>Photo: '.$basicogramatica->foto.'<br>Link: '.$linkbasicogramatica.'<br>Body: '.$bodybasicogramatica.'<br>');
        }else{
        Session::flash('success','¡El tema de la fila '.$userna.' fue eliminado!<br><br>Id: '.$request->id.'<br>Foto: '.$basicogramatica->foto.'<br>Enlace: '.$linkbasicogramatica.'<br>Contenido: '.$bodybasicogramatica.'<br>');}
        
        return back()->with('basicogramatica',$basicogramatica);
        
        }

            public function destroybasicogramaticamore(Request $request)
            {
                $delid=$request->input('delid');
                $counterdeletebamore=1;
            $counterrowsdelete=' ';
            if(App::isLocale('en')){      
            //----------------------------------------------------------------------
                foreach($delid as $del)
        {
        $basicogramatica = Basicogramatica::find($del);
        $linkbasicogramatica=$basicogramatica->link;
        $bodybasicogramatica=$basicogramatica->body;
        Storage::delete('public/images/'.$basicogramatica->foto);
       
        $counterrowsdelete=$counterrowsdelete.$counterdeletebamore.') ID: '.$del.'<br>Photo: '.$basicogramatica->foto.'<br>Link: '.$basicogramatica->link.'<br>Body: '.$basicogramatica->body."<br><br>";        
        $counterdeletebamore++;
        }
        $counterdeletebamore=$counterdeletebamore-1;

        if($counterdeletebamore==1){
                Basicogramatica::whereIn('id',$delid)->delete();
                Session::flash('success','There is '.$counterdeletebamore.' record deleted.<br><br><br>'.$counterrowsdelete);
        }else{
            Basicogramatica::whereIn('id',$delid)->delete();
            Session::flash('success','There are '.$counterdeletebamore.' records deleted.<br><br><br>'. $counterrowsdelete);

        }

                return back();
                //---------------------------------------------------------------
    }else{

         //----------------------------------------------------------------------
         foreach($delid as $del)
         {  
         $basicogramatica = Basicogramatica::find($del);
         $linkbasicogramatica=$basicogramatica->link;
         $bodybasicogramatica=$basicogramatica->body;
         Storage::delete('public/images/'.$basicogramatica->foto);
       
         $counterrowsdelete=$counterrowsdelete.$counterdeletebamore.') ID: '.$del.'<br>Foto: '.$basicogramatica->foto.'<br>Enlace: '.$basicogramatica->link.'<br>Contenido: '.$basicogramatica->body."<br><br>";        
        $counterdeletebamore++;
         }
         $counterdeletebamore=$counterdeletebamore-1;

         if($counterdeletebamore==1){
            Basicogramatica::whereIn('id',$delid)->delete();
            Session::flash('success','Se ha eliminado '.$counterdeletebamore.' registro.<br><br><br>'.$counterrowsdelete);
         }else{
            Basicogramatica::whereIn('id',$delid)->delete();
            Session::flash('success','Se han eliminado '.$counterdeletebamore.' registros.<br><br><br>'. $counterrowsdelete);

         }

                 return back();
                 //---------------------------------------------------------------
    }

            }
}
