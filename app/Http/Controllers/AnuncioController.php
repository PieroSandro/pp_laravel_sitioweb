<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;


Use Faker\Provider\Image;
use Illuminate\Support\Facades\Hash;

use App\Anuncio;

Use Illuminate\Support\Facades\File;
Use Illuminate\Support\Facades\Storage;
use PDF;
Use Illuminate\Http\Response;
use Auth;

use Validator;

use App\Notifications\SendContactNotification;
use Illuminate\Support\Facades\Notification;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class AnuncioController extends Controller
{
    
    

     /*public function show_an($id)
    {
        $anuncio = Anuncio::find($id);
        return View('front.blog2.show_an')->with('anuncio',$anuncio);

        
    }*/

    public function destroy_an($id)
    {
        //
        $anuncio = Anuncio::find($id);
        if($anuncio->foto != 'noimage.jpg'){
            Storage::delete('public/images/'.$anuncio->foto);
        }
        $anuncio->delete();
        Session::flash('success','El anuncio fue eliminado exitosamente');
        //return redirect('/index/anuncios')->with('success','¡Anuncio borrado!');
        //Session::flash('success','El anuncio fue eliminado exitosamente');
        return redirect()->route('backanunciohome')->with('success','¡Anuncio borrado!');
    }

    /*public function edit_an($id)
    {
        
        $anuncio = Anuncio::find($id);
        
        return view('front.blog2.edit_an')->with('anuncio',$anuncio);
    
    }*/

    public function update_an($id,Request $request)
    {
        $this->validate($request,[
            'titulo'=> 'required|max:50',
            'body' => 'required',
            'fecha_vence' => 'required|date'
          ]);
          
          if($request->hasFile('foto')){
            $filenameWithExt=$request->file('foto')->getClientOriginalName();
            $filename=pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension=$request->file('foto')->getClientOriginalExtension();
            $fileNameToStore=$filename.'_'.time().'.'.$extension;
            $path=$request->file('foto')->storeAs('public/images',$fileNameToStore);
        }


          //Create Post
          $anuncio = Anuncio::find($id);
          $anuncio->titulo = $request->input('titulo');
          $anuncio->body = $request->input('body');
          $anuncio->fecha_vence = $request->input('fecha_vence');
          if($request->hasFile('foto')){
              $anuncio->foto=$fileNameToStore;
          }
          $anuncio->save();
          Session::flash('success','¡El anuncio fue actualizado!');
          //return redirect('/index/anuncios')->with('success','Anuncio actualizado!!');
          //return redirect()->route('backanunciohome')->with('success','Anuncio actualizado!!');
          //return redirect('/index/anuncios/')->with('success','¡Anuncio actualizado!');
          return view('front.blog2.edit_an')->with('anuncio',$anuncio)->with('success','¡Anuncio actualizado!');
    }
}
