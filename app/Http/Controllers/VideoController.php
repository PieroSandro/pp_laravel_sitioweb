<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;


Use Faker\Provider\Image;
use Illuminate\Support\Facades\Hash;

use App\Video;
use App;
Use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

Use Illuminate\Support\Facades\Storage;
use PDF;
Use Illuminate\Http\Response;
use Auth;

use Validator;

use App\Notifications\SendContactNotification;
use Illuminate\Support\Facades\Notification;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class VideoController extends Controller
{
    
    public function updatevideo($id,Request $request)
    {
        
    $video = Video::findOrFail($request->id);
    $video->update($request->all());
   
    $locale=App::getLocale();
    $userna = $request->input('idc');
    $vidvideo = $request->input('vid');
    $linkvideo = $request->input('link');

    if(App::isLocale('en')){
        Session::flash('success','The video in the row '.$userna.' was updated!<br><br>ID: '.$request->id.'<br>Video: '.$vidvideo.'<br>Body: '.$linkvideo);
            return back();
        }else{
            Session::flash('success','¡El video de la fila '.$userna.' fue actualizado!<br><br>ID: '.$request->id.'<br>Video: '.$vidvideo.'<br>Contenido: '.$linkvideo);
            return back();
    
        }

    //Session::flash('success','¡El curso virtual de la fila '.$userna.' fue actualizado!');
        //return back();
        }



    public function destroyvideo(Request $request)
    {
        $video = Video::findOrFail($request->id);
        //--------------------------------------------------
        $vidvideo=$video->vid;
        $linkvideo=$video->link;
        //-------------------------------------------------
        $video->delete();
        $locale=App::getLocale();
        //--------------------------
        $userna = $request->input('idc2');
        if(App::isLocale('en')){
            Session::flash('success','The video in the row '.$userna.' was deleted!<br><br>Id: '.$request->id.'<br>Video: '.$vidvideo.'<br>Body: '.$linkvideo.'<br>');
        }else{
        Session::flash('success','¡El video de la fila '.$userna.' fue eliminado!<br><br>Id: '.$request->id.'<br>Video: '.$vidvideo.'<br>Contenido: '.$linkvideo.'<br>');}

    
        return back();
        }





        public function destroyvideomore(Request $request)
        {
            
            $delid=$request->input('delid');
            $ccc = $request->input('ccc');

            $counterdeletevcmore=1;
            $counterrowsdelete=' ';

            if(App::isLocale('en')){  
            foreach($delid as $del)
            {
                $video = Video::find($del);
                $vidvideo=$video->vid;
                $linkvideo=$video->link;
                //$bodycurso=$vcurso->body;
               
                $counterrowsdelete=$counterrowsdelete.$counterdeletevcmore.') ID: '.$del.'<br>Video: '.$vidvideo.'<br>Body: '.$linkvideo."<br>";
                $counterdeletevcmore++;
            }
         
            
            $counterdeletevcmore=$counterdeletevcmore-1;

            if($counterdeletevcmore==1){
                Video::whereIn('id',$delid)->delete();
                Session::flash('success','There is '.$counterdeletevcmore.' record deleted.<br><br><br>'.$counterrowsdelete);
               
            }else{  
                Video::whereIn('id',$delid)->delete();  
             Session::flash('success','There are '.$counterdeletevcmore.' records deleted.<br><br><br>'.$counterrowsdelete);
             
            }

            return back();
        }else{

            foreach($delid as $del)
            {
                $video = Video::find($del);
                $vidvideo=$video->vid;
                $linkvideo=$video->link;
                //$bodycurso=$vcurso->body;
               
                $counterrowsdelete=$counterrowsdelete.$counterdeletevcmore.') ID: '.$del.'<br>Video: '.$vidvideo.'<br>Contenido: '.$linkvideo."<br>";
                $counterdeletevcmore++;
            }
         
            
            $counterdeletevcmore=$counterdeletevcmore-1;

            if($counterdeletevcmore==1){
               Video::whereIn('id',$delid)->delete();
                Session::flash('success','Se ha eliminado '.$counterdeletevcmore.' registro.<br><br><br>'.$counterrowsdelete);
               
            }else{  
                Video::whereIn('id',$delid)->delete();  
             Session::flash('success','Se han eliminado '.$counterdeletevcmore.' registros.<br><br><br>'.$counterrowsdelete);
             
            }

            return back();






        }
            }

}