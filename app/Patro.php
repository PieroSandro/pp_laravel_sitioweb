<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patro extends Model
{
    //
    protected $table = "patros";
    protected $fillable=[
        'foto','titulo','body',
    ];
    //
    protected $primarykey="id";

    

    public $timestamps=true;
}