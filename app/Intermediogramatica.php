<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Intermediogramatica extends Model
{
    //
    protected $table = "intermediogramaticas";
    protected $fillable=[
        'foto','link','body',
    ];
    //
    protected $primarykey="id";

    

    public $timestamps=true;
}