<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Revista extends Model
{
    //
    protected $table = "revistas";
    protected $fillable=[
        'titulo','body', 'link',
    ];
    //
    protected $primarykey="id";

    

    public $timestamps=true;
}