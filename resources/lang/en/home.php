

<?php
//English
use App\Anuncio;
$tituloanuncio='';
return[
//SLIDER
'ver_slider'=>'See more',
//boton ariba
'boton_subir'=>'up',

//redes sociales
'rs_right'=>'Check our social networks',

//MENU
    'home_menu'=>'Home',
    'about_menu'=>'About Us',
    'mision_menu'=>'Mission',
    'vision_menu'=>'Vision',
    'docentes_menu'=>'Foreign Language Module Teachers',
    'examenes_menu'=>'Exams',
    'exsuficiencia_menu'=>'Sufficiency Exams',
    'exextraordinarios_menu'=>'Extraordinary Exams',
    'noticias_modal'=>'News',
    'close_modal'=>'Close',
    'testenglish_menu'=>'Test your English',
    'publi_menu'=>'Announcements',
    'revistas_menu'=>'Magazines',
    'biblioteca_menu'=>'Library',
    'contactenos_menu'=>'Contact us',
    'conocenos_menu'=>'Meet us',
    'reclamaciones_menu'=>'Complaints&#39 Book',//'Complaints&acute Book'
    'contactanos_menu'=>'Contact us',
    'regcursos_menu'=>'Courses&#39 Registration',

    //instagram

    'instagram_menu'=>'Instagram photos and videos',
    'insta2_menu'=>'See Instagram photos and videos from Juventud Globalizada (@juventudglobalizada)',
    //INDEX
        'title2_index'=>'English as a Foreign Language',
        'content_index'=>'<p style="text-align: justify; font-family: Montserrat, sans-serif;">English course is a compulsory curricular course desde Sección de 3 años del nivel inicial hasta el último año del nivel secundario. A partir de 4to grado del nivel primario, este espacio curricular se encuentra organizado por niveles determinados por el  rendimiento académico de cada alumno. Este tipo de organización permite que “…cada alumno pueda desarrollar y realizar los objetivos a un ritmo acomodado a su capacidad individual y a las características de su propia personalidad.” Y surge como una necesidad pedagógica que acompañe la oferta extra programática a contra turno de la Carga Intensiva por la que un grupo importante de nuestros alumnos opta, quienes logran un mayor nivel de conocimiento y habilidades.</p>
        <p style="text-align: justify; font-family: Montserrat, sans-serif;">
        English as a foreign language starts in the beginner level, at the age of 3, y culmina en el último año de la enseñanza media, momento en que los alumnos que asistieron al programa intensivo de enseñanza del inglés, obtienen las siguientes certificaciones que avalan el nivel de manejo del idioma alcanzado:</p>
            <ul style="padding-right: 40px;
          
          padding-left: 40px; text-align: justify; font-family: Montserrat, sans-serif;">
            <li>	
            Certificación de competencias en la Lengua Extranjera autorizada por el MEC bajo Resoluciones 1259/89 y 413/98.
            </li>
            <li>	
            Certificación de Formación Profesional y Capacitación Laboral del Área No Formal del MEC bajo disposición 83/09. Programas de contenidos encuadrados bajo el Marco Común Europeo de Referencia de las Lenguas.
            </li>
        </ul>
        <p style="text-align: justify; font-family: Montserrat, sans-serif;">
        Los grupos de alumnos graduados hasta el momento han alcanzado el nivel “avanzado” en el manejo del idioma, equivalente al nivel B2 y C1 del “Marco Común Europeo de Referencia para las lenguas”, elaborado por el Consejo de Europa.</p>
        <p style="text-align: justify; font-family: Montserrat, sans-serif;">
        Los alumnos que concurren al programa intensivo de enseñanza del inglés también tienen la posibilidad de certificar su nivel de lengua a través de los exámenes internacionales “Pearson Test of English”, que evalúan las habilidades de lectura, escritura, habla y escucha en un marco comunicativo. Nuestra institución ha sido centro autorizado para dichos exámenes desde 1993. Estos exámenes se encuentran encuadrados dentro del Marco Común Europeo de Referencia de las Lenguas, lo cual permite a los alumnos la obtención de becas y el ingreso y acreditación de la asignatura inglés en universidades locales e internacionales.</p>
        <p style="text-align: justify; font-family: Montserrat, sans-serif;">
        La bibliografía y metodologías más modernas y actualizadas son utilizadas en las clases de inglés, siempre adaptándolas a los intereses y edades de los alumnos. El proceso de enseñanza-aprendizaje es enriquecido a través de la lectura de cuentos, novelas, juegos, canciones, películas, obras de teatro, navegación en Internet y actividades interactivas.</p>
        <p style="text-align: justify; font-family: Montserrat, sans-serif;">
        Como característica distintiva, la academia de inglés ofrece a sus alumnos colonias de inmersión al idioma, visitas guiadas a lugares de interés cultural, asistencia a obras de teatro y cine, entrevistas con hablantes nativos, etc.</p>
        <p style="text-align: justify; font-family: Montserrat, sans-serif;">
        El progreso y evolución de los alumnos son testeados a través de evaluaciones formales e informales, entrevistas, exámenes parciales y finales, abarcando las 4 grandes habilidades: lectura, escritura, habla y escucha.</p>',


        'historieta_index'=>'<h2 style="color: black;font-family: Montserrat, sans-serif;">Cartoon: "Are you ready?"</h2>
        <h3 style="font-family: Montserrat, sans-serif;">About "Are you Ready?"</h3>
      <p style="text-align: justify; font-family: Montserrat, sans-serif;">
      The "Are you ready?" campaign is designed to inform students of the relevance and requirements of the accreditation process necessary to complete their studies at our institution and promote the practice of the English language.</p>',

    //CONTACTENOS
    'nombre_contactenos'=>'Name',
    'email_contactenos'=>'E-mail',
    'message1_contactenos'=>'You must enter your name',
    'message2_contactenos'=>'You must enter your e-mail',
    'message3_contactenos'=>'You must enter your message',
    'mensaje_contactenos'=>'Message',
    'boton_contactenos'=>'Send e-mail',

    'titulo_index'=>'ENGLISH COURSE',
    'principal_index'=>'Main',
    //'tituloanuncio_index'=>$tituloanuncio,




    //Q SOMOS
    'somos_index'=>'About us',
    'content_somos'=>'<p style="text-align: justify; font-size:23px; font-family: Montserrat, sans-serif;">Welcome</p>
    <p style="text-align: justify; font-family: Montserrat, sans-serif; padding-right: 40px;

    padding-left: 40px;">We are the English academic area of the Carlos Cueto Fernandini Higher Public Technological Institute.
    On this page you will find information about everything related to the English course,
    as well as the projects, exam dates and other resources that will help them
     to learning this important language.</p>
    <p style="text-align: justify; font-size:23px; font-family: Montserrat, sans-serif;">
Values
    <ul style="padding-right: 40px;

padding-left: 40px; text-align: justify; font-family: Montserrat, sans-serif;">
<li>	
<strong>Ethical conduct: </strong>We act with moral principles that define our social responsibility, generating professionalism, integrity, and respect for people.</li>
<li>	
<strong>Tolerance: </strong>Encourage and promote respect for institutions, students and teachers, taking into account their opinions and ideas.</li>
<li>	
<strong>Leadership: </strong>We have initiative and act responsibly in the promotion of enhanced activities, we have the ability to solve problems.</li>
<li>	
<strong>Teamwork: </strong>Encourage the participation of all students and teachers to achieve a common goal by sharing the necessary information and the required knowledge.</li>
<li>	
<strong>Honesty: </strong>We work with honesty, dignity, equity, solidarity and modesty.</li>
<li>	
<strong>Quality of services: </strong>Satisfy the expectations and needs required by students.</li>
</ul></p>',


//mision
'mision_index'=>'Mission',
'mision_content'=>'
<p style="text-align: justify; font-family: Montserrat, sans-serif;">
Improve the technical English management of our students constantly searching for new methods and means to ensure that each day something new is learned, even outside the classroom.
</p>
<p style="text-align: justify; font-family: Montserrat, sans-serif;">
The ISTP "Carlos Cueto Fernandini", center responsible for the teaching of the English language Technical (English for Specific Purposes-Technical English), providing to his students the knowledge of the mastery of the 4 skills of the English Language (Listening, Speaking, Reading and Writing ) essential for a better personal, academic and professional training, thus enhancing the consolidation of interculturality.
</p>',

//vision
'vision_index'=>'Vision',
'vision_content'=>'<p style="text-align: justify; font-family: Montserrat, sans-serif;">
Raise the professional level of our students, regardless of the career to which they belong, making them competitive through the command of technical English.
</p>
<p style="text-align: justify; font-family: Montserrat, sans-serif;">
To be a leading and competitive center in the teaching of English Technical Language (English for Specific Purposes-Technical English), committed to academic excellence and quality in the service, standing out for its desire for continuous improvement and for its relevant role as part of the ISTP "Carlos Cueto Fernandini", which is a benchmark for quality at the national level.
</p>',

//modulo transversales=modulo idioma extranjero
'ie_index'=>'Foreign Language Module',
    
'ie_content'=>'<p style="text-align: justify; font-family: Montserrat, sans-serif;">
Module Foreign Language in the English teaching unit, has the main function of offering technical English course, to graduates, students of different educational levels and the community in general of the Carlos Cueto Fernandini Institute, through intensive courses, with flexible hours .
<br><br>
Goals 
<br><br>
1. Providing the student with the means to express himself orally, starting with simple conversations, until reaching conversations.<br>
2. Promoting the reading of articles, magazines, books and texts written in English.<br>
3. Orienting conversational practice from the first level.<br>
4. Motivating the student towards a continuous learning of English.<br><br>
Foreign Language Module has highly qualified teachers and has audiovisual support that serves as support for a better language teaching.</p>',


//examenes de suficiencia
'exsuficiencia_cont'=>'<p style="text-align: justify; font-family: Montserrat, sans-serif;">
	   
To demonstrate your knowledge in a language, you must attach to your titling folder, a certificate or proof that proves it, from a study center authorized by the Ministry of Education (MINEDU).
<br><br>
This can be a Language Center to a State or Private University, a binational educational institution (Example: Peruano Norteamericano, Peruano Británico, Alianza Francesa, Goethe Institute, Peruano Italiano, among others).



<br><br>
It can also be an educational institution specialized in languages, in which you can verify that it is authorized by the MINEDU to teach this type of courses.
<br><br>
If you wish, you can request a proficiency test at the Instituto Argentina del Idioma Inglés, presenting a FUT by the table of parties, enclosing the receipt for the payment of the educational fee. These types of evaluations are scheduled every so often. A teacher will be assigned to be evaluated based on the syllabus shown above.
		</p>',

    'exsuficiencia_botones'=>'<div class="btn-group-vertical" role="group" aria-label="Basic example">
    <a type="button" href="https://getbootstrap.com/docs/4.3/components/button-group/" class="btn btn-primary" target="_blank">Management</a>
    <a type="button" href="https://getbootstrap.com/docs/4.3/components/button-group/" class="btn btn-primary">Computing</a>
    <a type="button" href="https://getbootstrap.com/docs/4.3/components/button-group/" class="btn btn-primary">Accounting</a>
    <a type="button" href="https://getbootstrap.com/docs/4.3/components/button-group/" class="btn btn-primary">Electronics</a>
    <a type="button" href="https://getbootstrap.com/docs/4.3/components/button-group/" class="btn btn-primary">Electrical Engineering</a>
    <a type="button" href="https://getbootstrap.com/docs/4.3/components/button-group/" class="btn btn-primary">Clinical Laboratory</a>
    <a type="button" href="https://getbootstrap.com/docs/4.3/components/button-group/" class="btn btn-primary">Automotive Mechanics</a>
    <a type="button" href="https://getbootstrap.com/docs/4.3/components/button-group/" class="btn btn-primary">Production Mechanics</a>
    <a type="button" href="https://getbootstrap.com/docs/4.3/components/button-group/" class="btn btn-primary">Metallurgy</a>
    </div>',




//test your english
'tenglish_cont'=>'<p style="text-align: justify; font-family: Montserrat, sans-serif;">
Instituto Superior Tecnológico Público Cueto invites you to test you english knowledge with these three exams with different levels. In the test, there are 50 questions and in each grammar question you have to choose the correct answer. At the end of the test you will get your score that will show your current english level.

</p>',
'te_basico'=>'Basic',
'te_intermedio'=>'Intermediate',
'te_intermedio2'=>'Intermediate II',


//videos
'vi_contabilidad'=>'Accounting',


//blog

'bl_home'=>'Home',
'bl_carrerast'=>'Technical Careers',
'bl_admin'=>'Management',
'bl_compu'=>'Computing',
'bl_conta'=>'Accounting',
'bl_elec1'=>'Electronics',
'bl_elec2'=>'Electrical Engineering',
'bl_lacli'=>'Clinical Laboratory',
'bl_mecau'=>'Automotive Mechanics',
'bl_mepro'=>'Production Mechanics',
'bl_metal'=>'Metallurgy',
'bl_acultural'=>'Cultural Activities',
'bl_fcreacion'=>'Date of creation',
'bl_factualizacion'=>'Last update',
'bl_nopost'=>'There is no post',
'bl_nopost2'=>'There is no post in this section',


'bl_admincoment'=>'Most commented Management posts',
'bl_title'=>'Title: ',
'bl_comments'=>'Comments: ',

'bl_compucoment'=>'Most commented Computing posts',
'bl_contacoment'=>'Most commented Accounting posts',
'bl_elec1coment'=>'Most commented Electronics posts',
'bl_elec2coment'=>'Most commented Electrical Engineering posts',
'bl_laclicoment'=>'Most commented Clinical Laboratory posts',
'bl_mecaucoment'=>'Most commented Automotive Mechanics posts',
'bl_meprocoment'=>'Most commented Production Mechanics posts',
'bl_metalcoment'=>'Most commented Metallurgy posts',

'bl_danza'=>'Dance',
'bl_musica'=>'Music',
'bl_teatro'=>'Theater',

'bl_vestudiantil'=>'Student Life',
'bl_basico'=>'Basic',
'bl_intermedio'=>'Intermediate',
'bl_avanzado'=>'Advanced',

'bl_ginglesa'=>'English Grammar',
'bl_pfrecuentes'=>'Frequent Questions',

//(blogapp)
'bl_welcome'=>'Welcome',
'bl_createp'=>'Create Post',
'bl_editdeletep'=>'Edit/Delete Post',

'bl_createa'=>'Create Ad',
'bl_editdeletea'=>'Edit/Delete Ad',

'bl_logout'=>'Logout',
'bl_blogmensaje2'=>'Message',
'bl_blogmensaje3'=>'You are logged in!',
//(actividadescul)

//(blog.blade.php)


//(show.blade.php)
'bl_back'=>'Go back',
'bl_totcomentarios'=>'Total comments',
'bl_bname'=>'Name',
'bl_coment'=>'Comment',
'bl_correo'=>'E-mail',
'bl_responder'=>'Answer:',
'bl_arespuesta'=>'Add answer',
'bl_bname2'=>'Name:',
'bl_bcorreo2'=>'E-mail:',
'bl_bconte2'=>'Comment:',
'messageco_contactenos'=>'You must enter your comment',
'bl_addcoment'=>'Add comment',
'bl_gencoment'=>'Most commented posts',
'bl_edit4'=>'Edit',
'bl_borrar'=>'Delete',

//(edit.blade.php)
'bl_editpost'=>'Edit: ',
'bl_editar2'=>'Edit Post',
'bl_titulo'=>'Title',
'bl_body'=>'Body',
'bl_change'=>'Save changes',

//(actividades culturales)
'bl_dance'=>'Dance',
'bl_dcontent'=>'<p style="text-align: justify;">
Raise the professional level of our students, regardless of the career to which they belong, making them competitive through the command of technical English.
</p>
<p style="text-align: justify;">
To be a leading and competitive center in the teaching of English Technical Language (English for Specific Purposes-Technical English), committed to academic excellence and quality in the service, standing out for its desire for continuous improvement and for its relevant role as part of the ISTP "Carlos Cueto Fernandini", which is a benchmark for quality at the national level.
</p>',
'bl_music'=>'Music',

//(v_estudiantil)
'bl_ves1'=>'From a perspective, it can be affirmed that in the learning of foreign languages, the attitudes of the students towards the language and towards their learning is a relevant category that is framed in the affective component. Therefore, these attitudinal components should not be overlooked in facilitating the learning of foreign languages, due to their overlap in the processes of knowledge construction.',
'bl_ves2'=>'After 1973, the education system
had a reform process whereby the
Ministry of Education structured education
basic in three cycles: initial, primary and
high school; structure defined by the
General Education Law approved with Decree
Law 19326-72, which stipulated that
foreign languages must be taught without constituting a factor of alienation or imposition
cultural. Also, it prohibited the education
of foreign languages in the first
cycle. Finally, this law established that
students could choose the language that
they wanted to learn; however, the language
English was the most widespread throughout the country.',

//podcast
'bl_podcastbasico'=>'Podcasts Basic Level',
'bl_podcastinter'=>'Podcasts Intermediate Level',
'bl_podcastadv'=>'Podcasts Advanced Level',
//gramatica
'bl_bgr'=>'Basic grammar',
'bl_bpa'=>'Are you a beginner (CEFR A1) or elementary (CEFR A2) learner of English? This grammar section gives short and clear explanations. There are exercises and worksheets to help you.
',
'bl_topic'=>'Choose a grammar topic',
'bl_class'=>'Class',


//basic topics
'bl_basic1'=>'Adjectives & Prepositions',
'bl_basic2'=>'Adjectives ending in -ed and –ing',
'bl_basic3'=>'Articles',
'bl_basic5'=>'Countable & Uncountable nouns',
'bl_basic7'=>'Past Continuous and Past Simple',
'bl_basic8'=>'Question forms & subject/object questions',
'bl_basic9'=>'Verbs followed by ‘ing’ or by ‘to + infinitive’ 1',
'bl_recursos'=>'Other resources',

//gramatica intermedio
'bl_igr'=>'Intermediate grammar',
'bl_ipa'=>'Are you an intermediate (CEFR B1) or upper intermediate (CEFR B2) learner of English? This grammar section gives short and clear explanations. There are online exercises and worksheets to help you.',
'bl_int1'=>'"As" and "like"',
'bl_int2'=>'Adjectives (gradable / non-gradable)',
'bl_int3'=>'British English and American English',
'bl_int4'=>'Capital letters and apostrophes',
'bl_int5'=>'Comparing and contrasting – modifying comparatives',
'bl_int6'=>'Conditionals',
'bl_int9'=>'Future continuous & Future perfect',

//gramatica avanzado
'bl_agr'=>'Advanced grammar',
'bl_apa'=>'Description',
'bl_adv1'=>'Continuos verb forms',
'bl_adv2'=>'Perfect verb forms',
'bl_adv3'=>'Modals and related verbs',
'bl_adv4'=>'Use and non-use of passive forms',
'bl_adv5'=>'Time and tense',
'bl_adv6'=>'Infinitives and -ing forms',
'bl_adv7'=>'Adverbs',
'bl_adv8'=>'Future forms',
'bl_adv9'=>'Noun phrases, ellipsis and substitution',


//post modal
'bl_deleteconf'=>'Delete confirmation',
'bl_modalq'=>'Are you sure you want to delete this post',

'bl_modalcancel'=>'No, cancel',
'bl_modaldelete'=>'Yes, delete',
//preguntas frecuentes

'bl_pfrecuentes'=>'Frequent questions',
'bl_pf1'=>'What is the attention hour?',
'bl_rf1'=>'Monday through Friday from 3:15 to 5:15 pm and from 6:45 to 8:15 pm.',
'bl_pf2'=>'What are the study topics for the exam?',
'bl_pf3'=>'What are the requirements for before and after the exam?',


//blogsesion
'bl_blsesion'=>'Welcome to this session',

//iniciosesion
'bl_blogin'=>'Login',

//createpost
'bl_createpost'=>'Create Post',
'bl_bcategoria'=>'Category',
'bl_bcategoria2'=>'-- Choose the category --',
'bl_bcategoria3'=>'You must select your blog&#39s category',


'bl_bgtitulo'=>'Title',
'bl_bgtitulo2'=>'You must enter your blog&#39s title',

'bl_bgbody'=>'Body',
'bl_bgbody2'=>'Enter the content',
'bl_bgbody3'=>'You must enter your blog&#39s body',

//createanuncio
'bl_createanuncio'=>'Create Ad',
'bl_antitulo2'=>'Enter your ad&#39s title',

'bl_antitulo3'=>'You must enter your ad&#39s title',
'bl_an4'=>'You must enter your ad&#39s body',
'bl_blfoto'=>'Photo (optional)',

'bl_fechacadu'=>'Expiration date',

'bl_an5'=>'You must enter your ad&#39s expiration date',

//showanuncio
'bl_shanuncio'=>'Ad: ',
'bl_vanuncio'=>'Ad&#39s expiration date:',

'bl_modalq2'=>'Are you sure you want to delete this ad',

'bl_listan'=>'Ads&#39 list',
'bl_an2'=>'Ads',
'bl_an3'=>'There is no ad',
'bl_an4'=>'Edit Ad',
'bl_an5'=>'Photo',


//login
'bl_loginemail'=>'E-mail',
'bl_loginpass'=>'Password',



//inglestecnico
'bl_itecnico'=>'Technical English',

//contactenos form name
'cf_nombre'=>'You must enter your name',
'cf_nombre2'=>'The name must be greater than 4 characters',
'cf_nombre3'=>'The name must contain only letters and be greater than 4 characters',
'cf_nombre4'=>'The name must be less than 45 characters',
'cf_nombre5'=>'The name must contain only letters and be less than 45 characters',
'cf_nombre6'=>'Correct information',
'cf_nombre7'=>'The name must contain only letters',

//contactenos form email
'cf_email'=>'You must enter your email',
'cf_email2'=>'Correct email',
'cf_email3'=>'Incorrect email',
'cf_email4'=>'The email must be less than 320 characters',
'cf_email5'=>'The email is incorrect and must be less than 320 characters',

//contactenos form message
'cf_message'=>'You must enter your message',
'cf_message2'=>'The message must be greater than 4 characters',
'cf_message3'=>'The message must be less than 320 characters',
'cf_message4'=>'Correct message',
'cf_message5'=>'The message mustn&#39t contain SWEAR WORDS',
'cf_message6'=>'The message must be greater than 4 characters
 and mustn&#39t contain SWEAR WORDS',

//contactenos form response

'cf_tresponse'=>'Warning!',
'cf_tresponse1'=>'There is',
'cf_tresponse2'=>'There are',
'cf_tresponse3'=>'errors',

//-------------
'cf_bresponse'=>'The email was sent successfully!',
'cf_bresponse1'=>'Information registered!',
'cf_bresponse2'=>'Name',
'cf_bresponse3'=>'Email',

//reclamaciones nombre
'cf_nombre4r'=>'The name must be less than 30 characters',
'cf_nombre5r'=>'The name must contain only letters and be less than 30 characters',

//reclamaciones apep
'cf_apellidop'=>'You must enter your father&#39s last name',
'cf_apellidop2'=>'The father&#39s last name must be greater than 1 character',
'cf_apellidop3'=>'The father&#39s last name must contain only letters and be greater than 1 character',
'cf_apellidop4'=>'The father&#39s last name must be less than 20 characters',
'cf_apellidop5'=>'The father&#39s last name must contain only letters and be less than 20 characters',
'cf_apellidop6'=>'Correct information',
'cf_apellidop7'=>'The father&#39s last name must contain only letters',

//reclamaciones apem
'cf_apellidom'=>'You must enter your mother&#39s last name',
'cf_apellidom2'=>'The mother&#39s last name must be greater than 1 character',
'cf_apellidom3'=>'The mother&#39s last name must contain only letters and be greater than 1 character',
'cf_apellidom4'=>'The mother&#39s last name must be less than 20 characters',
'cf_apellidom5'=>'The mother&#39s last name must contain only letters and be less than 20 characters',
'cf_apellidom6'=>'Correct information',
'cf_apellidom7'=>'The mother&#39s last name must contain only letters',

//reclamaciones select
'cf_distrito'=>'You must select your district',
'cf_distrito2'=>'Selected district',

//reclamaciones fono
'cf_telefono'=>'You must enter your telephone number',
'cf_telefono2'=>'The telephone number must be greater than 4 digits',
'cf_telefono3'=>'The telephone number must contain only numbers and be greater than 4 digits',
'cf_telefono4'=>'The telephone number must be less than 13 digits',
'cf_telefono5'=>'The telephone number must contain only numbers and be less than 13 characters',
'cf_telefono6'=>'Correct telephone number',
'cf_telefono7'=>'The telephone number must contain only numbers',

//reclamaciones documento
'cf_t_documento'=>'You must select your document type',
'cf_t_documento2'=>'Selected document type',

//reclamaciones n_documento
'cf_n_documento'=>'You must enter your document number',
'cf_n_documento2'=>'The document number must be greater than 4 digits',
'cf_n_documento3'=>'The document number must contain only numbers and be greater than 4 digits',
'cf_n_documento4'=>'The document number must be less than 13 digits',
'cf_n_documento5'=>'The document number must contain only numbers and be less than 13 characters',
'cf_n_documento6'=>'Correct document number',
'cf_n_documento7'=>'The document number must contain only numbers',

//reclamaciones t_reclamo
'cf_t_r1'=>'Claim',
'cf_t_r2'=>'Complaint',
'cf_t_r3'=>'Suggestion',
'cf_t_reclamo'=>'You must select your complaint type',
'cf_t_reclamo2'=>'Selected complaint type',

//reclamaciones success
//'cf_bresponse'=>'The email was sent successfully!',
//'cf_bresponse1'=>'Information registered!',
//'cf_bresponse2'=>'Name',
//'cf_bresponse3'=>'Email',
'cf_bresponse4'=>'Father&#39s last name',
'cf_bresponse5'=>'Mother&#39s last name',
'cf_bresponse6'=>'District',
'cf_bresponse7'=>'Telephone number',
'cf_bresponse8'=>'Document type',
'cf_bresponse9'=>'Document number',
'cf_bresponse10'=>'Complaint type',
'cf_bresponse11'=>'I am a minor: Yes',
'cf_bresponse12'=>'I am a minor: No',

//checkbox
'cf_ch'=>'I am a minor',
'cf_ch2'=>'I am a minor (Write your father&#39s data or your mother&#39s data)',

//reclamaciones headers
'rf_nombres'=>'Name(s)',
'rf_distrito'=>'Select your district',
'rf_documento'=>'Document',
'rf_documento2'=>'Select your document',
'rf_documento3'=>'ID document',
'rf_documento4'=>'Passport',
'rf_documento5'=>'Complaint&#39s Detail',

//show post comment
'bl_newcom'=>'New comment (last 24 hours)',

//bloghome
'bl_lmas'=>'Read more',
'bl_newpost'=>'New post (last 24 hours)',
'bl_newpost2'=>'New posts',

//cursosonline
'cursos_index'=>'Courses',

'bl_newcourse'=>'New (last 24 hours)',
//revistas
'revistas_menu'=>'Magazines',


//biblioteca
'biblioteca_menu'=>'Library',


//datatable
'emptyTable'=>'There is no available data in the table',
'info'=>'From _START_ to _END_ of _TOTAL_ ',
'infoEmpty'=>'Showing 0 records out of a total of 0.',
'infoFiltered'=>'(filtered from a total of _MAX_ records)',
'infoPostFix'=>'(updated)',
'lengthMenu'=>'Show _MENU_ records',
'loadingRecords'=>'Loading...',
'processing'=>'Processing...',
'searchPlaceholder'=>'Search...',
'zeroRecords'=>'No matches found.',
'first'=>'First',
'last'=>'Last',
'sortAscending'=>'Ascending ordering',
'sortDescending'=>'Descending ordering',

'dt_foto'=>'Photo',
'dt_titulo'=>'Title',
'dt_contenido'=>'Body',
'dt_accion'=>'Action',

'dt_edit'=>'Edit',

//patrocinadores
'namepatros'=>'Management: Sponsors',
'editpatros'=>'Edit sponsor',
'warning_adver'=>'Warning!',
'warning_adver_2'=>'Please select an image with format .gif, .jpeg, .png!',
'times_close'=>'Close',

//audios
'createaudios'=>'Create podcast',
'goaudios'=>'Go to podcasts',
'linkaudios'=>'Link',
'addrecord'=>'Add record',
'saverecord'=>'Save',
'deleterecord'=>'Delete record',

'nameaudios'=>'Management: Podcasts',
'deleteaudios'=>'Delete',
'deleteaudiosmore'=>'Delete selected',
'audio_qdelete_1'=>'Are you sure you want to delete the row #',
'audio_yes'=>'Yes',
'audio_no'=>'No',
'modal_descargar'=>'Download',
'soloedit'=>'Edit',

//banner
'createbanners'=>'Create banner',
'gobanners'=>'Go to banners',
'namebanners'=>'Management: Banners',
'editbanners'=>'Edit banner',


//events

'nameevents'=>'Management: Events',
'editevents'=>'Edit event',

//libros

'createlibros'=>'Create book',
'golibros'=>'Go to books',

'namelibros'=>'Management: Books',

//mainpages

'namemainpages'=>'Management: Sections',
'editmainpages'=>'Edit section',
'dt_contenido_ES'=>'Body_ES',
'dt_foto_ES'=>'Photo_ES',
'dt_contenido_EN'=>'Body_EN',
'dt_foto_EN'=>'Photo_EN',


//revistas

'createrevistas'=>'Create magazine',
'gorevistas'=>'Go to magazines',

'namerevistas'=>'Management: Magazines',

//vcursos

'createvcursos'=>'Create course',
'govcursos'=>'Go to courses',

'namevcursos'=>'Management: Courses',

//videos

'createvideos'=>'Create video',
'govideos'=>'Go to videos',

'namevideos'=>'Management: Videos',


//basicopreguntas

'createbasicopreguntas'=>'Basic Level: Create question',
'gobasicopreguntas'=>'Go to basic level questions',

'namebasicopreguntas'=>'Management: Basic Level Questions',


'namequestion'=>'Question',
'nameoption1'=>'Option 1',
'nameoption2'=>'Option 2',
'nameoption3'=>'Option 3',
'nameoption4'=>'Option 4',
'nameanswer'=>'Answer',


//intermediopreguntas

'createintermediopreguntas'=>'Intermediate Level: Create question',
'gointermediopreguntas'=>'Go to intermediate level questions',

'nameintermediopreguntas'=>'Management: Intermediate Level Questions',
'namepuntajeaprobatorio'=>'Minimum passing score',


//avanzadopreguntas

'createavanzadopreguntas'=>'Advanced Level: Create question',
'goavanzadopreguntas'=>'Go to advanced level questions',

'nameavanzadopreguntas'=>'Management: Advanced Level Questions',




//chatbots

'createchatbots'=>'Chatbot: Create question',
'gochatbots'=>'Go to chatbot',

'namechatbots'=>'Management: Chatbot',

'namepalabraclave'=>'Keyword',

//basicogramatica

'createbasicogramaticas'=>'Basic Grammar: Create topic',
'gobasicogramaticas'=>'Go to basic grammar',

'namebasicogramaticas'=>'Management: Basic Grammar',
'editbasicogramaticas'=>'Edit topic',
//intermediogramatica

'createintermediogramaticas'=>'Intermediate Grammar: Create topic',
'gointermediogramaticas'=>'Go to intermediate grammar',

'nameintermediogramaticas'=>'Management: Intermediate Grammar',
'editintermediogramaticas'=>'Edit topic',

//avanzadogramatica

'createavanzadogramaticas'=>'Advanced Grammar: Create topic',
'goavanzadogramaticas'=>'Go to advanced grammar',

'nameavanzadogramaticas'=>'Management: Advanced Grammar',
'editavanzadogramaticas'=>'Edit topic',


//administracion
'bl_administracion1'=>'Management - Basic',
'bl_administracion2'=>'Management - Intermediate',
'bl_administracion3'=>'Management - Advanced',
//computacion
'bl_computacion1'=>'Computing - Basic',
'bl_computacion2'=>'Computing - Intermediate',
'bl_computacion3'=>'Computing - Advanced',
//contabilidad
'bl_contabilidad1'=>'Accounting - Basic',
'bl_contabilidad2'=>'Accounting - Intermediate',
'bl_contabilidad3'=>'Accounting - Advanced',
//electronica
'bl_electronica1'=>'Electronics - Basic',
'bl_electronica2'=>'Electronics - Intermediate',
'bl_electronica3'=>'Electronics - Advanced',
//electrotecnia
'bl_electrotecnia1'=>'Electrical Engineering - Basic',
'bl_electrotecnia2'=>'Electrical Engineering - Intermediate',
'bl_electrotecnia3'=>'Electrical Engineering - Advanced',
//lab clinico
'bl_labclinico1'=>'Clinical Laboratory - Basic',
'bl_labclinico2'=>'Clinical Laboratory - Intermediate',
'bl_labclinico3'=>'Clinical Laboratory - Advanced',
//mecanica automotriz
'bl_mecautomotriz1'=>'Automotive Mechanics - Basic',
'bl_mecautomotriz2'=>'Automotive Mechanics - Intermediate',
'bl_mecautomotriz3'=>'Automotive Mechanics - Advanced',
//mecanica produccion
'bl_meproduccion1'=>'Production Mechanics - Basic',
'bl_meproduccion2'=>'Production Mechanics - Intermediate',
'bl_meproduccion3'=>'Production Mechanics - Advanced',
//metalurgia
'bl_metalurgia1'=>'Metallurgy - Basic',
'bl_metalurgia2'=>'Metallurgy - Intermediate',
'bl_metalurgia3'=>'Metallurgy - Advanced',


//frequentepreguntas

'createfrequentepreguntas'=>'Create frequent question',
'gofrequentepreguntas'=>'Go to frequent questions',

'namefrequentepreguntas'=>'Management: Frequent Questions',
'namefpregunta_ES'=>'Question_ES',
'namefpregunta_EN'=>'Question_EN',
'namefrespuesta_ES'=>'Answer_ES',
'namefrespuesta_EN'=>'Answer_EN',



//mainiconos

'namemainiconos'=>'Management: Icons',
'editmainiconos'=>'Edit icon',

//mainwords

'namemainwords'=>'Management: Key Information',

//checkiffoto
'checkphoto'=>'Put the photo above the body',
'firstphoto'=>'Photo first',


];