<?php

//Spanish
use App\Anuncio;
$tituloanuncio='';
return[
//SLIDER
'ver_slider'=>'Ver más',

//boton arriba
'boton_subir'=>'subir',

//redes sociales
'rs_right'=>'Ve nuestras redes sociales',

//MENU
    'home_menu'=>'Inicio',
    'about_menu'=>'Quienes Somos',
    'mision_menu'=>'Misión',
    'vision_menu'=>'Visión',
    'docentes_menu'=>'Docentes de Módulo Idioma Extranjero',
    'examenes_menu'=>'Exámenes',
    'exsuficiencia_menu'=>'Exámenes de Suficiencia',
    'exextraordinarios_menu'=>'Exámenes Extraordinarios',
    'close_modal'=>'Cerrar',
    'noticias_modal'=>'Noticias',
    'testenglish_menu'=>'Prueba tu Inglés',
    'publi_menu'=>'Publicaciones',
    'revistas_menu'=>'Revistas',
    'biblioteca_menu'=>'Biblioteca',
    'contactenos_menu'=>'Contáctenos',
    'conocenos_menu'=>'Conócenos',
    'reclamaciones_menu'=>'Libro de Reclamaciones',
    'contactanos_menu'=>'Contáctanos',
    'regcursos_menu'=>'Registro Cursos',

    //instagram
    'instagram_menu'=>'Fotos y videos de Instagram',
    'insta2_menu'=>'Ver fotos y videos de Instagram de Juventud Globalizada (@juventudglobalizada)',

//INDEX
'title2_index'=>'Inglés como Lengua Extranjera',
'content_index'=>'<p style="text-align: justify; font-family: Montserrat, sans-serif;">La asignatura Inglés es una materia curricular obligatoria desde Sección de 3 años del nivel inicial hasta el último año del nivel secundario. A partir de 4to grado del nivel primario, este espacio curricular se encuentra organizado por niveles determinados por el  rendimiento académico de cada alumno. Este tipo de organización permite que “…cada alumno pueda desarrollar y realizar los objetivos a un ritmo acomodado a su capacidad individual y a las características de su propia personalidad.” Y surge como una necesidad pedagógica que acompañe la oferta extra programática a contra turno de la Carga Intensiva por la que un grupo importante de nuestros alumnos opta, quienes logran un mayor nivel de conocimiento y habilidades.</p>
<p style="text-align: justify; font-family: Montserrat, sans-serif;">
Inglés como lengua extranjera comienza en el nivel inicial, a la edad de 3 años, y culmina en el último año de la enseñanza media, momento en que los alumnos que asistieron al programa intensivo de enseñanza del inglés, obtienen las siguientes certificaciones que avalan el nivel de manejo del idioma alcanzado:</p>
	<ul style="padding-right: 40px;
  
  padding-left: 40px; text-align: justify; font-family: Montserrat, sans-serif;">
	<li>	
    Certificación de competencias en la Lengua Extranjera autorizada por el MEC bajo Resoluciones 1259/89 y 413/98.
    </li>
    <li>	
    Certificación de Formación Profesional y Capacitación Laboral del Área No Formal del MEC bajo disposición 83/09. Programas de contenidos encuadrados bajo el Marco Común Europeo de Referencia de las Lenguas.
    </li>
</ul>
<p style="text-align: justify; font-family: Montserrat, sans-serif;">
Los grupos de alumnos graduados hasta el momento han alcanzado el nivel “avanzado” en el manejo del idioma, equivalente al nivel B2 y C1 del “Marco Común Europeo de Referencia para las lenguas”, elaborado por el Consejo de Europa.</p>
<p style="text-align: justify; font-family: Montserrat, sans-serif;">
Los alumnos que concurren al programa intensivo de enseñanza del inglés también tienen la posibilidad de certificar su nivel de lengua a través de los exámenes internacionales “Pearson Test of English”, que evalúan las habilidades de lectura, escritura, habla y escucha en un marco comunicativo. Nuestra institución ha sido centro autorizado para dichos exámenes desde 1993. Estos exámenes se encuentran encuadrados dentro del Marco Común Europeo de Referencia de las Lenguas, lo cual permite a los alumnos la obtención de becas y el ingreso y acreditación de la asignatura inglés en universidades locales e internacionales.</p>
<p style="text-align: justify; font-family: Montserrat, sans-serif;">
La bibliografía y metodologías más modernas y actualizadas son utilizadas en las clases de inglés, siempre adaptándolas a los intereses y edades de los alumnos. El proceso de enseñanza-aprendizaje es enriquecido a través de la lectura de cuentos, novelas, juegos, canciones, películas, obras de teatro, navegación en Internet y actividades interactivas.</p>
<p style="text-align: justify; font-family: Montserrat, sans-serif;">
Como característica distintiva, la academia de inglés ofrece a sus alumnos colonias de inmersión al idioma, visitas guiadas a lugares de interés cultural, asistencia a obras de teatro y cine, entrevistas con hablantes nativos, etc.</p>
<p style="text-align: justify; font-family: Montserrat, sans-serif;">
El progreso y evolución de los alumnos son testeados a través de evaluaciones formales e informales, entrevistas, exámenes parciales y finales, abarcando las 4 grandes habilidades: lectura, escritura, habla y escucha.</p>',

'historieta_index'=>'<h2 style="color: black;font-family: Montserrat, sans-serif;">Historieta: "Are you ready?"</h2>
<h3 style="font-family: Montserrat, sans-serif;">Sobre "Are you Ready?"</h3>
<p style="text-align: justify; font-family: Montserrat, sans-serif;">
La campaña "Are you ready?" está diseñada para informar a los estudiantes de la relevancia y requerimientos del proceso de acreditación necesarios para completar sus estudios en nuestra institución y promover la práctica del idioma inglés.</p>',




//CONTACTENOS
    'nombre_contactenos'=>'Nombre',
    'email_contactenos'=>'Correo',
    'message1_contactenos'=>'Debe ingresar su nombre',
    'message2_contactenos'=>'Debe ingresar su correo',
    'message3_contactenos'=>'Debe ingresar su mensaje',
    'mensaje_contactenos'=>'Mensaje',
    'boton_contactenos'=>'Enviar correo',

//mision
'titulo_index'=>'CURSO DE INGLÉS',
'principal_index'=>'Principal',
//'tituloanuncio_index'=>$tituloanuncio,



//Q SOMOS
'somos_index'=>'Quienes somos',
'content_somos'=>'<p style="text-align: justify; font-size:23px; font-family: Montserrat, sans-serif;">Bienvenidos</p>
<p style="text-align: justify; font-family: Montserrat, sans-serif; padding-right: 40px;

padding-left: 40px;">Somos el área académica de inglés del Instituto Superior Tecnológico Público Carlos Cueto Fernandini.
 En esta página van a encontrar información acerca de todo lo referente al curso de inglés, 
 así como los proyectos, fechas de exámenes y otros recursos que les ayudarán
  al aprendizaje de este importante idioma.</p>
<p style="text-align: justify; font-size:23px; font-family: Montserrat, sans-serif;">
Valores
<ul style="padding-right: 40px;

padding-left: 40px; text-align: justify; font-family: Montserrat, sans-serif;">
<li>	
<strong>Conducta ética: </strong>Actuamos con principios morales que definen nuestra responsabilidad social generando profesionalidad, integridad, y respeto a las personas.</li>
<li>	
<strong>Tolerancia: </strong>Fomentar y promovemos el respeto a las instituciones, estudiantes y docentes, tomando en cuenta sus opiniones e ideas.</li>
<li>	
<strong>Liderazgo: </strong>Tenemos iniciativa y actuamos con responsabilidad en la promoción de las actividades realzadas, contamos con la capacidad en resolución de problemas.</li>
<li>	
<strong>Trabajo en equipo: </strong>Incentivar la participación de todos los estudiantes y docentes para alcanzar un objetivo en común compartiendo la información necesaria y los conocimientos requeridos.</li>
<li>	
<strong>Honestidad: </strong>Trabajamos con honradez, dignidad, equidad, solidaridad y modestia.</li>
<li>	
<strong>Calidad de servicios: </strong>Satisfacer las expectativas y necesidades requeridas por los estudiantes.</li>
</ul></p>',

//mision
'mision_index'=>'Misión',
'mision_content'=>'<p style="text-align: justify; font-family: Montserrat, sans-serif;">
Mejorar el manejo de inglés técnico de nuestros alumnos de manera constante buscando nuevos métodos y medios para lograr que cada día se aprenda algo nuevo incluso fuera del aula.
</p>
<p style="text-align: justify; font-family: Montserrat, sans-serif;">
El I.S.T.P “Carlos Cueto Fernandini”, centro responsable de la enseñanza del idioma inglés Técnico (English for Specific Purposes-Technical English), proporcionando a sus estudiantes los conocimientos del dominio de las 4 habilidades del Idioma Ingles (Listening, Speaking, Reading and Writing) imprescindibles para una mejor formación personal, académica y profesional, potenciando así la consolidación de la interculturalidad.
</p>',

//vision
'vision_index'=>'Visión',
'vision_content'=>'<p style="text-align: justify; font-family: Montserrat, sans-serif;">
Elevar el nivel profesional de nuestros alumnos, indistintamente de la carrera a la que pertenezcan, haciéndolos competitivos por medio del dominio del inglés técnico.
</p>
<p style="text-align: justify; font-family: Montserrat, sans-serif;">
Ser un centro líder y competitivo en la enseñanza de idioma Ingles Técnico (English for Specific Purposes-Technical English), comprometido con la excelencia académica y la calidad en el servicio, destacando por su afán de mejora continua y por su papel relevante como parte del I.S.T.P “Carlos Cueto Fernandini”, que sea referente de calidad a nivel nacional.
</p>',

//modulo transversales=modulo idioma extranjero
'ie_index'=>'Módulo Idioma Extranjero',
'ie_content'=>'<p style="text-align: justify; font-family: Montserrat, sans-serif;">
Módulo Idioma Extranjero en la unidad didáctica de inglés, posee la función principal de ofrecer curso de inglés técnico, a egresados, estudiantes de los diferentes niveles educativos y a la comunidad en general del Instituto Carlos Cueto Fernandini, a través de cursos intensivos, con horarios flexibles.
<br><br>
Objetivos 
<br><br>
1. Facilitar al estudiante los medios para que se exprese en forma oral, partiendo de conversaciones simples, hasta llegar a conversaciones.<br>
2. Promover la lectura de artículos, revistas, libros y textos escritos en inglés.<br>
3. Orientar la práctica conversacional desde el primer nivel.<br>
4. Motivar al estudiante hacia un continuo aprendizaje del inglés.<br><br>
Módulo Idioma Extranjero tiene profesores altamente calificados y cuenta con ayuda audiovisual que sirven como apoyo para una mejor enseñanza del idioma.</p>
',

//examenes de suficiencia
'exsuficiencia_cont'=>'<p style="text-align: justify; font-family: Montserrat, sans-serif;">
	   

Para demostrar tus conocimientos en un idioma, debes adjuntar a en tu carpeta de titulación, un certificado o constancia que lo demuestre, de algún centro de estudios autorizado por el Ministerio de Educación (MINEDU).
<br><br>
Este puede ser un Centro de Idiomas a alguna Universidad Estatal o Particular, una institución educativa binacional (Ejemplo: Peruano Norteamericano, Peruano Británico, Alianza Francesa, Instituto Goethe, Peruano Italiano, entre otros).
<br><br>
Tambien puede ser una institución educativa especializada en idiomas, en el que puedas verificar que esta autorizada por el MINEDU para impartir este tipo de cursos.
<br><br>
Si deseas puedes solicitar un examen de suficiencia en el Instituto Argentina del Idioma Inglés, presentando por mesa de partes un FUT  adjuntando el recibo por el pago de la tasa educativa. Cada cierto tiempo se programan este tipo de evaluaciones. Un docente te sera asignado para ser evaluado tomando como base el temario arriba mostrado.

		</p>',

'exsuficiencia_botones'=>'<div class="btn-group-vertical" role="group" aria-label="Basic example">
<a type="button" href="https://getbootstrap.com/docs/4.3/components/button-group/" class="btn btn-primary" target="_blank">Administración</a>
<a type="button" href="https://getbootstrap.com/docs/4.3/components/button-group/" class="btn btn-primary">Computación</a>
<a type="button" href="https://getbootstrap.com/docs/4.3/components/button-group/" class="btn btn-primary">Contabilidad</a>
<a type="button" href="https://getbootstrap.com/docs/4.3/components/button-group/" class="btn btn-primary">Electrónica</a>
<a type="button" href="https://getbootstrap.com/docs/4.3/components/button-group/" class="btn btn-primary">Electrotecnia</a>
<a type="button" href="https://getbootstrap.com/docs/4.3/components/button-group/" class="btn btn-primary">Laboratorio Clínico</a>
<a type="button" href="https://getbootstrap.com/docs/4.3/components/button-group/" class="btn btn-primary">Mecánica Automotriz</a>
<a type="button" href="https://getbootstrap.com/docs/4.3/components/button-group/" class="btn btn-primary">Mecánica de Producción</a>
<a type="button" href="https://getbootstrap.com/docs/4.3/components/button-group/" class="btn btn-primary">Metalurgia</a>
</div>',


//test your english
'tenglish_cont'=>'<p style="text-align: justify; font-family: Montserrat, sans-serif;">
El Instituto Superior Tecnológico Público Cueto te invita a evaluar tus conocimientos de inglés con estos tres exámenes de diferentes niveles. En la prueba, hay 50 preguntas y en cada pregunta de gramática tienes que elegir la respuesta correcta. Al final de la prueba obtendrás tu puntuación que mostrará tu nivel de inglés actual.

</p>',
'te_basico'=>'Básico',
'te_intermedio'=>'Intermedio',
'te_intermedio2'=>'Intermedio II',


//videos
'vi_contabilidad'=>'Contabilidad',



//BLOG-------
//(bloggram)
'bl_home'=>'Inicio',

'bl_carrerast'=>'Carreras Técnicas',
'bl_admin'=>'Administración',
'bl_compu'=>'Computación',
'bl_conta'=>'Contabilidad',
'bl_elec1'=>'Electrónica',
'bl_elec2'=>'Electrotecnia',
'bl_lacli'=>'Laboratorio Clínico',
'bl_mecau'=>'Mecánica Automotriz',
'bl_mepro'=>'Mecánica de Producción',
'bl_metal'=>'Metalurgia',
'bl_acultural'=>'Actividades Culturales',
'bl_fcreacion'=>'Fecha de creación',
'bl_factualizacion'=>'Última actualización',
'bl_nopost'=>'No hay posts',
'bl_nopost2'=>'No hay posts en esta sección',

'bl_admincoment'=>'Posts de Administración más comentados',
'bl_title'=>'Título: ',
'bl_comments'=>'Comentarios: ',

'bl_compucoment'=>'Posts de Computación más comentados',
'bl_contacoment'=>'Posts de Contabilidad más comentados',
'bl_elec1coment'=>'Posts de Electrónica más comentados',
'bl_elec2coment'=>'Posts de Electrotecnia más comentados',
'bl_laclicoment'=>'Posts de Laboratorio Clínico más comentados',
'bl_mecaucoment'=>'Posts de Mecánica Automotriz más comentados',
'bl_meprocoment'=>'Posts de Mecánica de Producción más comentados',
'bl_metalcoment'=>'Posts de Metalurgia más comentados',

'bl_danza'=>'Danza',
'bl_musica'=>'Música',
'bl_teatro'=>'Teatro',

'bl_vestudiantil'=>'Vida Estudiantil',
'bl_basico'=>'Básico',
'bl_intermedio'=>'Intermedio',
'bl_avanzado'=>'Avanzado',

'bl_ginglesa'=>'Gramática Inglesa',
'bl_pfrecuentes'=>'Preguntas Frecuentes',

//(blogapp)
'bl_welcome'=>'Bienvenida',
'bl_createp'=>'Crear Post',
'bl_editdeletep'=>'Editar/Eliminar Post',

'bl_createa'=>'Crear Anuncio',
'bl_editdeletea'=>'Editar/Eliminar Anuncio',

'bl_logout'=>'Cerrar sesión',
'bl_blogmensaje2'=>'Mensaje',
'bl_blogmensaje3'=>'¡Has iniciado sesión!',
//(actividadescul)


//(blog.blade.php)






//(show.blade.php)
'bl_back'=>'Regresar',
'bl_totcomentarios'=>'Total de comentarios',
'bl_bname'=>'Nombre',
'bl_coment'=>'Comentario',
'bl_correo'=>'Correo',
'bl_responder'=>'Responder:',
'bl_arespuesta'=>'Agregar respuesta',
'bl_bname2'=>'Nombre:',
'bl_bcorreo2'=>'Correo:',
'bl_bconte2'=>'Comentario:',
'messageco_contactenos'=>'Debe ingresar su comentario',
'bl_addcoment'=>'Agregar comentario',
'bl_gencoment'=>'Posts más comentados',  
'bl_edit4'=>'Editar',
'bl_borrar'=>'Eliminar',


//(edit.blade.php)
'bl_editpost'=>'Editar: ',
'bl_editar2'=>'Editar Post',
'bl_titulo'=>'Título',
'bl_body'=>'Contenido',
'bl_change'=>'Guardar cambios',

//(actividades culturales)
'bl_dance'=>'Danza',
'bl_dcontent'=>'<p style="text-align: justify;">
Raise the professional level of our students, regardless of the career to which they belong, making them competitive through the command of technical English.
</p>
<p style="text-align: justify;">
To be a leading and competitive center in the teaching of English Technical Language (English for Specific Purposes-Technical English), committed to academic excellence and quality in the service, standing out for its desire for continuous improvement and for its relevant role as part of the ISTP "Carlos Cueto Fernandini", which is a benchmark for quality at the national level.
</p>',
'bl_music'=>'Música',

//(v_estudiantil)
'bl_ves1'=>'Desde una perspectiva, se puede afirmar que en el aprendizaje de lenguas extranjeras, las actitudes de los estudiantes hacia el idioma y hacia su aprendizaje es una categoría relevante que se enmarca en el componente afectivo. Por lo tanto, estos componentes actitudinales no se deben obviar en la facilitación de los aprendizajes de lenguas extranjeras, por su imbricación en los procesos de construcción del conocimiento.',
'bl_ves2'=>'Después del año 1973, el sistema educativo
tuvo un proceso de reforma por lo cual el
Ministerio de Educación estructuró la educación
básica en tres ciclos: inicial, primaria y
secundaria; estructura definida mediante la
Ley General de Educación aprobada con Decreto
Ley 19326-72, que estipuló que los
idiomas extranjeros deben enseñarse sin que constituyan factor de alienación o imposición
cultural. Asimismo, prohibía la enseñanza
de idiomas extranjeros en el primer
ciclo. Finalmente, esta ley establecía que los
estudiantes podían elegir el idioma que
querían aprender; sin embargo, el idioma
inglés era el más difundido en todo el país.',

//podcast
'bl_podcastbasico'=>'Podcasts Nivel Básico',
'bl_podcastinter'=>'Podcasts Nivel Intermedio',
'bl_podcastadv'=>'Podcasts Nivel Avanzado',
//gramatica
'bl_bgr'=>'Gramática básica',
'bl_bpa'=>'¿Es usted un principiante (CEFR A1) o elemental (CEFR A2) para aprender inglés? Esta sección de gramática da explicaciones cortas y claras. Hay ejercicios y hojas de trabajo para ayudarle.',
'bl_topic'=>'Escoja un tema de gramática',
'bl_class'=>'Clase',
//basic topics
'bl_basic1'=>'Adjetivos y Preposiciones',
'bl_basic2'=>'Adjetivos terminados en -ed y -ing',
'bl_basic3'=>'Artículos',
'bl_basic5'=>'Sustantivos contables e incontables',
'bl_basic7'=>'Pasado continuo y Pasado simple',
'bl_basic8'=>'Formas de pregunta y preguntas "subject/object"',
'bl_basic9'=>'Verbos seguidos por ‘ing’ o por ‘to + infinitive’ 1',
'bl_recursos'=>'Otros recursos',

//gramatica intermedio
'bl_igr'=>'Gramática intermedia',
'bl_ipa'=>'¿Es usted un estudiante de inglés intermedio (MCER B1) o intermedio superior (MCER B2)? Esta sección de gramática da explicaciones cortas y claras. Hay ejercicios en línea y hojas de trabajo para ayudarle.',
'bl_int1'=>'"As" y "like"',
'bl_int2'=>'Adjetivos (graduables / no graduables)',
'bl_int3'=>'Inglés Británico e Inglés Americano',
'bl_int4'=>'Letras mayúsculas y apóstrofes',
'bl_int5'=>'Comparando y contrastando - modificando comparativos',
'bl_int6'=>'Condicionales',
'bl_int9'=>'Futuro continuo y futuro perfecto',

//gramatica avanzado
'bl_agr'=>'Gramática avanzada',
'bl_apa'=>'Descripción',
'bl_adv1'=>'Formas verbales continuas',
'bl_adv2'=>'Formas verbales perfectas',
'bl_adv3'=>'"Modals" y verbos relacionados',
'bl_adv4'=>'Uso y no uso de formas pasivas',
'bl_adv5'=>'Tiempo y tiempo gramatical',
'bl_adv6'=>'Infinitivos y formas terminadas en -ing',
'bl_adv7'=>'Adverbios',
'bl_adv8'=>'Formas futuras',
'bl_adv9'=>'Frases nominales, elipsis y substitución',

//post modal
'bl_deleteconf'=>'Confirmar borrado',
'bl_modalq'=>'¿Está ud. seguro de que quiere borrar este post',

'bl_modalcancel'=>'No, cancelar',
'bl_modaldelete'=>'Sí, borrar',
//preguntas frecuentes

'bl_pfrecuentes'=>'Preguntas frecuentes',
'bl_pf1'=>'¿Cuál es el horario de atención?',
'bl_rf1'=>'Lunes a viernes de 3:15 a 5:15pm y de 6:45 a 8:15pm.',
'bl_pf2'=>'¿Cuáles son los temas de estudio para el examen?',
'bl_pf3'=>'¿Cuáles son los requisitos para antes y después del examen?',

//blogsesion

'bl_blsesion'=>'Bienvenida a esta sesión',

//iniciosesion
'bl_blogin'=>'Inicio de sesión',

//createpost
'bl_createpost'=>'Crear Post',
'bl_bcategoria'=>'Categoría',
'bl_bcategoria2'=>'-- Escoja la categoría --',
'bl_bcategoria3'=>'Debe seleccionar la categoría de su blog',

'bl_bgtitulo'=>'Título',
'bl_bgtitulo2'=>'Debe ingresar el título de su blog',

'bl_bgbody'=>'Contenido',
'bl_bgbody2'=>'Ingrese el contenido',
'bl_bgbody3'=>'Debe ingresar el contenido de su blog',

//createanuncio
'bl_createanuncio'=>'Crear Anuncio',
'bl_antitulo2'=>'Ingrese el título de su anuncio',

'bl_antitulo3'=>'Debe ingresar el título de su anuncio',

'bl_an4'=>'Debe ingresar el contenido de su anuncio',
'bl_blfoto'=>'Foto (opcional)',

'bl_fechacadu'=>'Fecha caducidad',

'bl_an5'=>'Debe ingresar la fecha de caducidad de su anuncio',

//showanuncio
'bl_shanuncio'=>'Anuncio: ',
'bl_vanuncio'=>'Vencimiento de anuncio:',

'bl_modalq2'=>'¿Está ud. seguro de que quiere borrar este anuncio',

'bl_listan'=>'Lista de anuncios',
'bl_an2'=>'Anuncios',
'bl_an3'=>'No hay anuncios',
'bl_an4'=>'Editar Anuncio',
'bl_an5'=>'Foto',


//login
'bl_loginemail'=>'Correo electrónico',
'bl_loginpass'=>'Contraseña',



//inglestecnico
'bl_itecnico'=>'Inglés Técnico',

//contactenos form
'cf_nombre'=>'Debe ingresar su nombre',
'cf_nombre2'=>'El nombre debe ser mayor que 4 caracteres',
'cf_nombre3'=>'El nombre debe contener sólo letras y ser mayor que 4 caracteres',
'cf_nombre4'=>'El nombre debe ser menor que 45 caracteres',
'cf_nombre5'=>'El nombre debe contener sólo letras y ser menor que 45 caracteres',
'cf_nombre6'=>'Datos correctos',
'cf_nombre7'=>'El nombre debe contener sólo letras',

//contactenos form email
'cf_email'=>'Debe ingresar su correo',
'cf_email2'=>'Correo correcto',
'cf_email3'=>'Correo incorrecto',
'cf_email4'=>'El correo debe ser menor que 320 caracteres',
'cf_email5'=>'El correo es incorrecto y debe ser menor que 320 caracteres',

//contactenos form message
'cf_message'=>'Debe ingresar su mensaje',
'cf_message2'=>'El mensaje debe ser mayor que 4 caracteres',
'cf_message3'=>'El mensaje debe ser menor que 320 caracteres',
'cf_message4'=>'Mensaje correcto',
'cf_message5'=>'El mensaje no debe contener LISURAS',
'cf_message6'=>'El mensaje debe ser mayor que 4 caracteres y no debe contener
 LISURAS',

//contactenos form response

'cf_tresponse'=>'¡Atención!',
'cf_tresponse1'=>'Hay',
'cf_tresponse2'=>'Hay',
'cf_tresponse3'=>'errores',

//-------------
'cf_bresponse'=>'¡El correo fue enviado exitosamente!',
'cf_bresponse1'=>'¡Datos registrados!',
'cf_bresponse2'=>'Nombre',
'cf_bresponse3'=>'Correo',

//reclamaciones nombre
'cf_nombre4r'=>'El nombre debe ser menor que 30 caracteres',
'cf_nombre5r'=>'El nombre debe contener sólo letras y ser menor que 30 caracteres',

//reclamaciones apep
'cf_apellidop'=>'Debe ingresar su apellido paterno',
'cf_apellidop2'=>'El apellido paterno debe ser mayor que 1 caracter',
'cf_apellidop3'=>'El apellido paterno debe contener sólo letras y ser mayor que 1 caracter',
'cf_apellidop4'=>'El apellido paterno debe ser menor que 20 caracteres',
'cf_apellidop5'=>'El apellido paterno debe contener sólo letras y ser menor que 20 caracteres',
'cf_apellidop6'=>'Datos correctos',
'cf_apellidop7'=>'El apellido paterno debe contener sólo letras',

//reclamaciones apem
'cf_apellidom'=>'Debe ingresar su apellido materno',
'cf_apellidom2'=>'El apellido materno debe ser mayor que 1 caracter',
'cf_apellidom3'=>'El apellido materno debe contener sólo letras y ser mayor que 1 caracter',
'cf_apellidom4'=>'El apellido materno debe ser menor que 20 caracteres',
'cf_apellidom5'=>'El apellido materno debe contener sólo letras y ser menor que 20 caracteres',
'cf_apellidom6'=>'Datos correctos',
'cf_apellidom7'=>'El apellido materno debe contener sólo letras',

//reclamaciones select
'cf_distrito'=>'Debe seleccionar su distrito',
'cf_distrito2'=>'Distrito seleccionado',

//reclamaciones fono
'cf_telefono'=>'Debe ingresar su número telefónico',
'cf_telefono2'=>'El número telefónico debe ser mayor que 4 dígitos',
'cf_telefono3'=>'El número telefónico debe contener sólo números y ser mayor que 4 dígitos',
'cf_telefono4'=>'El número telefónico debe ser menor que 13 dígitos',
'cf_telefono5'=>'El número telefónico debe contener sólo números y ser menor que 13 caracteres',
'cf_telefono6'=>'Número telefónico correcto',
'cf_telefono7'=>'El número telefónico debe contener sólo números',

//reclamaciones documento
'cf_t_documento'=>'Debe seleccionar su tipo de documento',
'cf_t_documento2'=>'Tipo de documento seleccionado',

//reclamaciones n_documento
'cf_n_documento'=>'Debe ingresar su número de documento',
'cf_n_documento2'=>'El número de documento debe ser mayor que 4 dígitos',
'cf_n_documento3'=>'El número de documento debe contener sólo números y ser mayor que 4 dígitos',
'cf_n_documento4'=>'El número de documento debe ser menor que 13 dígitos',
'cf_n_documento5'=>'El número de documento debe contener sólo números y ser menor que 13 caracteres',
'cf_n_documento6'=>'Número de documento correcto',
'cf_n_documento7'=>'El número de documento debe contener sólo números',

//reclamaciones t_reclamo
'cf_t_r1'=>'Reclamo',
'cf_t_r2'=>'Queja',
'cf_t_r3'=>'Sugerencia',
'cf_t_reclamo'=>'Debe seleccionar su tipo de reclamo',
'cf_t_reclamo2'=>'Tipo de reclamo seleccionado',

//reclamaciones success
'cf_bresponse4'=>'Apellido paterno',
'cf_bresponse5'=>'Apellido materno',
'cf_bresponse6'=>'Distrito',
'cf_bresponse7'=>'Número de teléfono',
'cf_bresponse8'=>'Tipo de documento',
'cf_bresponse9'=>'N° documento',
'cf_bresponse10'=>'Tipo de reclamo',
'cf_bresponse11'=>'Soy menor de edad: Sí',
'cf_bresponse12'=>'Soy menor de edad: No',

//checkbox
'cf_ch'=>'Soy menor de edad',
'cf_ch2'=>'Soy menor de edad (Escriba los datos del padre o madre)',

//reclamaciones headers
'rf_nombres'=>'Nombre(s)',
'rf_distrito'=>'Seleccione su distrito',
'rf_documento'=>'Documento',
'rf_documento2'=>'Seleccione su documento',
'rf_documento3'=>'DNI',
'rf_documento4'=>'Pasaporte',
'rf_documento5'=>'Detalle de la Reclamación',

//show post comment
'bl_newcom'=>'Nuevo comentario (últimas 24 horas)',

//bloghome
'bl_lmas'=>'Leer más',
'bl_newpost'=>'Nuevo post (últimas 24 horas)',
'bl_newpost2'=>'Nuevos',

//cursosonline
'cursos_index'=>'Cursos',

'bl_newcourse'=>'Nuevo (últimas 24 horas)',
//revistas
'revistas_menu'=>'Revistas',

//biblioteca
'biblioteca_menu'=>'Biblioteca',

//datatable
'emptyTable'=>'No hay datos disponibles en la tabla.',
'info'=>'Del _START_ al _END_ de _TOTAL_ ',
'infoEmpty'=>'Mostrando 0 registros de un total de 0.',
'infoFiltered'=>'(filtrados de un total de _MAX_ registros)',
'infoPostFix'=>'(actualizados)',
'lengthMenu'=>'Mostrar _MENU_ registros',
'loadingRecords'=>'Cargando...',
'processing'=>'Procesando...',
'searchPlaceholder'=>'Buscar...',
'zeroRecords'=>'No se han encontrado coincidencias.',
'first'=>'Primera',
'last'=>'Última',
'sortAscending'=>'Ordenación ascendente',
'sortDescending'=>'Ordenación descendente',

'dt_foto'=>'Foto',
'dt_titulo'=>'Título',
'dt_contenido'=>'Contenido',
'dt_accion'=>'Acción',

'dt_edit'=>'Editar',

//patrocinadores
'namepatros'=>'Administración: Patrocinadores',
'editpatros'=>'Editar patrocinador',
'warning_adver'=>'¡Advertencia!',
'warning_adver_2'=>'¡Por favor seleccione una imagen con formato .gif, .jpeg, .png!',
'times_close'=>'Cerrar',

//audios
'createaudios'=>'Crear podcast',
'goaudios'=>'Ir a podcasts',
'linkaudios'=>'Enlace',
'addrecord'=>'Agregar registro',
'saverecord'=>'Guardar',
'deleterecord'=>'Eliminar registro',

'nameaudios'=>'Administración: Podcasts',
'deleteaudios'=>'Borrar',
'deleteaudiosmore'=>'Borrar seleccionado(s)',
'audio_qdelete_1'=>'¿Está ud. seguro de que quiere borrar la fila #',
'audio_yes'=>'Sí',
'audio_no'=>'No',
'modal_descargar'=>'Descargar',
'soloedit'=>'Editar',

//banner
'createbanners'=>'Crear banner',
'gobanners'=>'Ir a banners',
'namebanners'=>'Administración: Banners',
'editbanners'=>'Editar banner',

//events

'nameevents'=>'Administración: Eventos',
'editevents'=>'Editar evento',

//libros

'createlibros'=>'Crear libro',
'golibros'=>'Ir a libros',

'namelibros'=>'Administración: Libros',

//mainpages

'namemainpages'=>'Administración: Secciones',
'editmainpages'=>'Editar sección',
'dt_contenido_ES'=>'Contenido_ES',
'dt_foto_ES'=>'Foto_ES',
'dt_contenido_EN'=>'Contenido_EN',
'dt_foto_EN'=>'Foto_EN',

//revistas

'createrevistas'=>'Crear revista',
'gorevistas'=>'Ir a revistas',

'namerevistas'=>'Administración: Revistas',

//vcursos

'createvcursos'=>'Crear curso',
'govcursos'=>'Ir a cursos',

'namevcursos'=>'Administración: Cursos',

//videos

'createvideos'=>'Crear video',
'govideos'=>'Ir a videos',

'namevideos'=>'Administración: Videos',


//basicopreguntas

'createbasicopreguntas'=>'Nivel Básico: Crear pregunta',
'gobasicopreguntas'=>'Ir a preguntas de nivel básico',

'namebasicopreguntas'=>'Administración: Preguntas Nivel Básico',

'namequestion'=>'Pregunta',
'nameoption1'=>'Opción 1',
'nameoption2'=>'Opción 2',
'nameoption3'=>'Opción 3',
'nameoption4'=>'Opción 4',
'nameanswer'=>'Respuesta',


//intermediopreguntas

'createintermediopreguntas'=>'Nivel Intermedio: Crear pregunta',
'gointermediopreguntas'=>'Ir a preguntas de nivel intermedio',

'nameintermediopreguntas'=>'Administración: Preguntas Nivel Intermedio',

'namepuntajeaprobatorio'=>'Puntaje mínimo aprobatorio',

//avanzadopreguntas

'createavanzadopreguntas'=>'Nivel Avanzado: Crear pregunta',
'goavanzadopreguntas'=>'Ir a preguntas de nivel avanzado',

'nameavanzadopreguntas'=>'Administración: Preguntas Nivel Avanzado',

//chatbots

'createchatbots'=>'Chatbot: Crear pregunta',
'gochatbots'=>'Ir a chatbot',

'namechatbots'=>'Administración: Chatbot',

'namepalabraclave'=>'Palabra clave',

//basicogramatica

'createbasicogramaticas'=>'Gramática Básica: Crear tema',
'gobasicogramaticas'=>'Ir a gramática básica',

'namebasicogramaticas'=>'Administración: Gramática Básica',
'editbasicogramaticas'=>'Editar tema',

//intermediogramatica


'createintermediogramaticas'=>'Gramática Intermedia: Crear tema',
'gointermediogramaticas'=>'Ir a gramática intermedia',

'nameintermediogramaticas'=>'Administración: Gramática Intermedia',
'editintermediogramaticas'=>'Editar tema',


//avanzadogramatica

'createavanzadogramaticas'=>'Gramática Avanzada: Crear tema',
'goavanzadogramaticas'=>'Ir a gramática avanzada',

'nameavanzadogramaticas'=>'Administración: Gramática Avanzada',
'editavanzadogramaticas'=>'Editar tema',


//administracion
'bl_administracion1'=>'Administración - Básico',
'bl_administracion2'=>'Administración - Intermedio',
'bl_administracion3'=>'Administración - Avanzado',
//computacion
'bl_computacion1'=>'Computación - Básico',
'bl_computacion2'=>'Computación - Intermedio',
'bl_computacion3'=>'Computación - Avanzado',
//contabilidad
'bl_contabilidad1'=>'Contabilidad - Básico',
'bl_contabilidad2'=>'Contabilidad - Intermedio',
'bl_contabilidad3'=>'Contabilidad - Avanzado',
//electronica
'bl_electronica1'=>'Electrónica - Básico',
'bl_electronica2'=>'Electrónica - Intermedio',
'bl_electronica3'=>'Electrónica - Avanzado',
//electrotecnia
'bl_electrotecnia1'=>'Electrotecnia - Básico',
'bl_electrotecnia2'=>'Electrotecnia - Intermedio',
'bl_electrotecnia3'=>'Electrotecnia - Avanzado',
//lab clinico
'bl_labclinico1'=>'Laboratorio Clínico - Básico',
'bl_labclinico2'=>'Laboratorio Clínico - Intermedio',
'bl_labclinico3'=>'Laboratorio Clínico - Avanzado',
//mecanica automotriz
'bl_mecautomotriz1'=>'Mecánica Automotriz - Básico',
'bl_mecautomotriz2'=>'Mecánica Automotriz - Intermedio',
'bl_mecautomotriz3'=>'Mecánica Automotriz - Avanzado',
//mecanica produccion
'bl_meproduccion1'=>'Mecánica de Producción - Básico',
'bl_meproduccion2'=>'Mecánica de Producción - Intermedio',
'bl_meproduccion3'=>'Mecánica de Producción - Avanzado',
//metalurgia
'bl_metalurgia1'=>'Metalurgia - Básico',
'bl_metalurgia2'=>'Metalurgia - Intermedio',
'bl_metalurgia3'=>'Metalurgia - Avanzado',


//frequentepreguntas

'createfrequentepreguntas'=>'Crear pregunta frequente',
'gofrequentepreguntas'=>'Ir a preguntas frequentes',

'namefrequentepreguntas'=>'Administración: Preguntas Frequentes',
'namefpregunta_ES'=>'Pregunta_ES',
'namefpregunta_EN'=>'Pregunta_EN',
'namefrespuesta_ES'=>'Respuesta_ES',
'namefrespuesta_EN'=>'Respuesta_EN',



//mainiconos

'namemainiconos'=>'Administración: Íconos',
'editmainiconos'=>'Editar ícono',


//mainwords

'namemainwords'=>'Administración: Información Clave',

//checkiffoto
'checkphoto'=>'Colocar la foto encima del contenido',
'firstphoto'=>'Foto primero',
];
