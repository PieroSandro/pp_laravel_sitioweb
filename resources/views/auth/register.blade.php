@extends('layouts.app')
@section('content')
<br><br><br><br><br><br>
    
    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block"><img src="{{ asset('img/cueto.jpg') }}" width="100%" height="100%"></div>
          <div class="col-lg-7">
            <div class="p-5">
              @if(session('message-danger'))
                <div class="alert alert-danger">
                  {{ session('message-danger') }}
                </div>
              @elseif(session('message-success'))
                <div class="alert alert-success">
                    {{ session('message-success') }}
                </div>
              @elseif(session('message-warning'))
                <div class="alert alert-warning">
                    {{ session('message-warning') }}
                </div>
              @endif
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">REGISTRATE</h1>
              </div>
               <form method="POST" action="{{ route('login.registrar') }}" aria-label="{{ __('Register') }}">
                        @csrf
        
                 <div class="form-group">
              <input id="cod_al" type="text" class="form-control form-control-user{{ $errors->has('cod_al') ? ' is-invalid' : '' }}" name="cod_al" value="{{ old('cod_al') }}" placeholder="Código Estudiante..."  autofocus required="">

                                @if ($errors->has('cod_al'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cod_al') }}</strong>
                                    </span>
                                @endif
                </div>

                <div class="form-group">
              <input id="email" type="email" class="form-control form-control-user{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="E-mail.."  autofocus required="">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                </div>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input id="password" type="password" class="form-control form-control-user{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required="" >

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                  </div>
                  <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Repetir Password" required="">
                            </div>
                </div>
                <div class="form-group">
                      <div class="g-recaptcha" data-sitekey="{{env('CAPTCHA_KEY')}}">

                        
                      </div>
                       @if ($errors->has('g-recaptcha-response'))
                                    <span class="invalid-feedback" style="display: block;">
                                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                    </span>
                                @endif
                    </div>

                   <button type="submit" class="btn btn-primary btn-user btn-block">
                                    {{ __('REGISTRAR') }}
                  </button>
                
                </form>
              <hr>
               <div class="text-center">
                    <a class="small" href="{{ route('login') }}">Logueate</a>
                  </div>
            </div>
          </div>
        </div>

      </div>
    </div>

  </div>
    <script src="{{ asset('js/user/verificar-cod-alumno.js') }}"></script>

@endsection
