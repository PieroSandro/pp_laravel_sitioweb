@extends('layouts.actividadescul')
@section('titleblog')
@lang('home.bl_blogin')

@endsection






<!-- ------------ style="transform:scale(0.77);-webkit-transform:scale(0.77);"------------------------------------------------------------>

@section('carusel')

              @endsection


<!-- ------------------------------------------->





@section('content')


<!-------------------------POPOVER---------------------------------->

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<!----------------------POPOVER------------------------------------->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<link rel="stylesheet" href="{{url('/css/animate.css')}}">


<!--------------ANIMATE ON SCROLL------------------------->
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        
        <script>
        AOS.init({
          duration:3000,
        });
        </script>
        <!------------------------------------------->

<!--codigo php   
session_start();
if(isset($_POST["sub"]))
{
    $_SESSION["email"]=$_POST["email"];
    $_SESSION['last_login_timestamp']=time();
    header("location:home.blade.php");
}


?>-->
<br>
<label id="qs"></label>





<br>
<br>




<br>
<br>






<div class="container" style="font-family: Montserrat, sans-serif;">
    <div class="row justify-content-center">



       
        <div class="col-lg-8">
            <div class="card">




                
                <div class="card-header"><i class="fas fa-user"></i> @lang('home.bl_blogin')</div>

                <div class="card-body">

 
                    <form method="POST" action="{{route('bloglogin')}}">
                    <div>
                    <!--<div style="text-align:center;">-->
                    @csrf
                        <div class="form-group {{$errors->has('email') ? 'has-error' :''}}">
                            


                            <!--<div style="text-align:center;">-->
                                <div>
                            <div class="col-xs-12 col-sm-8 col-md-12 col-lg-8" style="margin-left: auto;
    margin-right: auto;">
                                <label for="email">@lang('home.bl_loginemail')</label>

                                <input style="position: relative;display: center; margin : 0 auto;" class="form-control" type="email" name="email"
                                value="{{old('email')}}">
                                {!!$errors->first('email','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="form-group {{$errors->has('password') ? 'has-error' :''}}">
                            
                            <div class="col-xs-12 col-sm-8 col-md-12 col-lg-8" style="margin-left: auto;
    margin-right: auto;">
                                <label for="password">@lang('home.bl_loginpass')</label>

                                <input type="password" class="form-control" name="password">
                                {!!$errors->first('password','<span class="help-block">:message</span>') !!}
                                
                                
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="form-group">
                        <div class="col-xs-12 col-sm-8 col-md-12 col-lg-8" style="margin-left: auto;
    margin-right: auto;">
                      <div class="g-recaptcha" data-sitekey="{{env('CAPTCHA_KEY')}}">
                        </div>
                         @if ($errors->has('g-recaptcha-response'))
                                    <span class="invalid-feedback" style="display: block;">
                                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                    </span>
                                @endif
                      </div>
                      
                    </div>
                        <br>
                        <br>


                        <label class="col text-center">
                        <div class="form-group">
                            <div class="col-xs-12 col-sm-8 col-md-12 col-lg-8" style="margin-left: auto;
    margin-right: auto;">
                                <button type="submit" name="sub" id="sub" 
                                class="btn" style="color: white; border: rgba(11, 11, 132, 0.993); background-color:rgba(11, 11, 132, 0.993) ;font-family: Montserrat, sans-serif;">
                                <i class="fas fa-key"></i> {{ __('Login') }}
                                </button>

                                
                            </div>
                        </div>
                        </label>





</div>
                    </form>


                </div>
            </div>

            
        </div>
    </div>
</div>
@endsection
@section('content2')

<?php

$postcomentados=array();
$postcantidad=array();
$linkcomentados=array();

$py=array();
$arr='';
$ct=0;

use App\Post;
      $posts=Post::all();
//use App\Post;
//$posts=Post::all();
foreach($posts as $post){

$titulopost='';
$linkpost='';
$fpo='';
$counter = 0;
foreach($post->comentarios as $comentario){
    $counter++;
    } //$titulopost=$post->titulo;

    $pbody=$post->body;
    //----------------------------------------------------------------------------
    
$extract=substr(htmlspecialchars(trim(strip_tags($pbody))), 0, 150);



$pbody1=htmlspecialchars(trim(strip_tags($pbody)));
//$extract=mb_strimwidth($bodypost, 1, 90, "...");
if(strlen($pbody)>151){

  $locale=App::getLocale();
//echo $extract, PHP_EOL;
if(App::isLocale('en')){
$pbo=htmlspecialchars_decode($extract, ENT_NOQUOTES).'... <small><a href="/intranet/public/blog/home/'.$post->id.'" style="text-decoration: none; color: blue;">
Read more</a></small>';
}else{
  $pbo=htmlspecialchars_decode($extract, ENT_NOQUOTES).'... <small><a href="/intranet/public/blog/home/'.$post->id.'" style="text-decoration: none; color: blue;">
Leer más</a></small>';
}
/*echo "... ";?>
<small><a href="/intranet/public/blog/home/{{$post->id}}" style="text-decoration: none; color: blue;">
@lang('home.bl_lmas')</a></small>
<?php*/
}
else{
 $pbo=htmlspecialchars_decode($pbody1, ENT_NOQUOTES);
}

//-------------------------------------------------------------
$fpost=$post->foto;
$fpo='';
if($fpost=='noimage.jpg'){  
$fpo='';
}else{



$fpo='/intranet/storage/app/public/images/{{$post->foto}}';

}

//$fpo="<img src='/intranet/storage/app/public/images/".$post->foto."' />";

$tpost="<div style='color:blue'>".$post->titulo."</div>";

    //-----------------------------------------------------------------------------
    $linkpost="
    <style>
   .popover {
    
    color: black;
    text-align:justify;
    background-color: white;
  }
  
  .popover-title {
    background-color: coral; 
    color: coral; 
    font-size: 40px;
    text-align:center;
  }
 
  .popover-content {
    background-color: green;
    color: blue;
    padding: 25px;
  }
  
  .arrow {
    border-right-color: red !important;
  }
   
   
   </style>
    <a href='/intranet/public/blog/home/".$post->id."'
    title='".$post->titulo."' data-by='".$pbo."' data-img='/intranet/storage/app/public/images/".$post->foto."' class='popi' data-toggle='popover' data-placement='bottom' style='text-decoration: none; color: blue; font-family: Montserrat, sans-serif;'>".$post->titulo."</a>

   ";?>
   
<?php
    $linkcomentados[]=$linkpost;
    //$postcomentados[]=$titulopost;
    $postcantidad[]=$counter;

$ct++;
}



for($i=0;$i<$ct;$i++){   
$py[$linkcomentados[$i]]=$postcantidad[$i];

}

arsort($py);?>

<div style="text-align:center;">
<div class="plustar1" id="plustar2"><div style="margin:10px; padding-left:10px; padding-right:10px; padding-top:2px; text-align:center; float: center; color:white; font-family: 'Montserrat', sans-serif;"><strong><i class="fas fa-star plustar" style="color:yellow"></i>  @lang('home.bl_gencoment')  <i class="fas fa-star plustar" style="color:yellow"></i></strong></div></div>
</div>
<!--@lang('home.bl_gencoment')-->


<?php
echo "<br>";
echo "<br>";
$i=0; 
foreach($py as $x => $x_value) {?>
  <label style="font-family: Montserrat, sans-serif;">
  <b>
	@lang('home.bl_title')
  </b>
	</label>
	<?php
	
    echo $x."&nbsp<label style='font-family: Montserrat, sans-serif;'><b>|</b></label>&nbsp"?><label style="font-family: Montserrat, sans-serif;"><b>@lang('home.bl_comments') </b></label>&nbsp<?php 
    echo "<label style='font-family: Montserrat, sans-serif;'>";
    echo $x_value;
    echo "</label>";
		echo "<br>";
		 
		if($i==9){
			break;
		}
	
		else{
		$i++;}
  }
  
  if($i==0){?>
    @lang('home.bl_nopost')
    <?php }


//--------------------------------
/*$age = array("Ben"=>"37", "Peter"=>"35", "Joe"=>"43");
print_r($age);
arsort($age);

foreach($age as $x => $x_value) {
    echo "Key=" . $x . ", Value=" . $x_value;
    echo "<br>";
}*/
//------------------------------------------------


$counter1 = 0;
$counter2= 0;
foreach($posts as $post){
    $counter1++;
      if((time() - strtotime($post->created_at)) <= 86400)
      {
          $counter2++;
      }
		}

$counter1;
$counter2;



?>

<script>

   $('.popi').popover({
    delay: 1150,
          //trigger: 'focus',
		  trigger: 'hover',
          html: true,
          content: function () {

            if($(this).data('img')=='/intranet/storage/app/public/images/noimage.jpg')
            {
            return $(this).data("by");
            }else{
              
				return $(this).data("by")+'<br><br><img class="img-fluid" src="'+$(this).data('img') + '" />';
            }
          },
          title: 'Toolbox'
    });   
 
 </script>
 <script>



          $(function(){

            
            
            $("#NumPosts").html("Total: <?php echo $counter1 ?>");

            var c2 = "<?php echo $counter2 ?>";
            if(c2>0){   
            $("#NewPosts").html(" @lang('home.bl_newpost2') <i class='fas fa-file comicon2' style='color:white'></i> : <?php echo $counter2 ?>");
            }else{
              $("#NewPosts").hide();
            }

            $("#dance_id2").hide();

            $("#danza_id").mouseover(function(){
              $("#dance_id1").hide();
              $("#dance_id2").show();
                });

            $("#danza_id").mouseout(function(){
              $("#dance_id2").hide();
              $("#dance_id1").show();
                });
            
              
          
          });
        </script>
@endsection