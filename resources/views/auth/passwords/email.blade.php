@extends('layouts.app')

@section('content')
<br><br><br><br><br><br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Resetear Password') }}</div>

                <div class="card-body">

                    @if (session('status'))
                        
                            <figure class="loginReset__icon" style="text-align: center">
                                <img
                                        src="{{asset('img/check.png')}}"
                                        srcset="{{asset('img/check@2x.png')}} 2x"
                                        sizes="93px"
                                        alt="">
                            </figure>
                            <p class="u-text-center" style="text-align: center;">Te enviamos las instrucciones.<br>Revise su bandeja de entrada
                                para ver cómo restablecer su contraseña</p>
                            <div style="text-align: center">
                                <a href="/password/reset" class="btn btn-primary">Ingresar nuevamente</a>
                            </div>
                        
                        <style> 
                        .form {
                                display: none;
                            }
                        </style>
                    @endif


                    <form method="POST" action="{{ route('password.email') }}" class="form">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Resetear Password') }}
                                </button>

                            </div>
                            
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
