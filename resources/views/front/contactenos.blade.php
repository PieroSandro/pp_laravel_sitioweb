@extends('layouts.master2')
@section('title')
@lang('home.contactenos_menu')
@endsection
@section('content')
  
<style>


.swal2-modal .swal2-styled{
    
    background-color: rgba(4, 10, 100, 0.808);
    font-family: 'Montserrat', sans-serif;
}

.swal2-title{
    font-family: 'Montserrat', sans-serif;
}


#swal2-content{
    font-family: 'Montserrat', sans-serif;
}
</style>
<div class="row" style="font-family: 'Montserrat', sans-serif;">
    <div class="col-12 col-sm-8 col-md-12 col-lg-8 col-xl-8 offset-sm-2 offset-md-0 offset-lg-2 offset-xl-2">
        <br>
        <h1><div style="color: black; text-align: center; font-family: 'Montserrat', sans-serif;">@lang('home.contactenos_menu')</div></h1>
<br>
        
       
        <form class="sf" id="submit_form" data-route="{{route('contactenos.send')}}" method="POST" enctype="multipart/form-data">
            @csrf
            @if(session('success'))
                

            @endif
            <div class="form-group">
            
            <label for="name" class="mt-3">@lang('home.nombre_contactenos')</label>

            <input type="text" class="form-control" name="name" id="name" style="box-shadow: 0 0 0px;">
            

            <br>
            
            <label id="lblName" style="color:red"></label>
            <label id="lblName2" style="color:green"></label>
        </div>
            <!-- -->
            <div class="form-group">
            
            <label for="email" class="mt-3">@lang('home.email_contactenos')</label>

            <input type="text" class="form-control" name="email" id="email" style="box-shadow: 0 0 0px;">
            

                   <br>
            
            <label id="lblEmail" style="color:red"></label>
            <label id="lblEmail2" style="color:green"></label>
        </div>
<!-- -->
<div class="form-group">

<label for="message" class="mt-3">@lang('home.mensaje_contactenos')</label>
<!--{{Form::label('message','Mensaje')}}-->
<!-------<input type="textarea" class="form-control" cols="30" rows="10" name="message" id="message">-->
<textarea class="form-control" cols="30" rows="10" name="message" id="message" style="box-shadow: 0 0 0px;"></textarea>
            
            <!--{{Form::textarea('message','',['name'=>'message','class'=>'form-control'])}}-->
                  <br>
            
                  <label id="lblMessage" style="color:red"></label>
                  <label id="lblMessage2" style="color:green"></label>
        </div>
            
            <!--
            <input type="submit" class="btn btn-primary" name="submit" id="submit" 
            value="@lang('home.boton_contactenos')">-->

            <label class="col text-center">
        <button type="submit" name="submit" id="submit" class="btn" 
        style="color: white; border: rgba(11, 11, 132, 0.993); background-color:rgba(11, 11, 132, 0.993) ;font-family: Montserrat, sans-serif;">
<i class="fas fa-envelope"></i> @lang('home.boton_contactenos')
</button>        
</label>        
        
            <span id="error_message" class="text-danger"></span>
            <span id="success_message" class="text-success"></span>
        
        
        </form>
        <!--{{Form::submit('Enviar Correo',['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}-->
       <br>








    </div>
     <!--13_2_2020-->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
   <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

   <!--13_2_2020-->

    <script>
            $(function(){
                
                $("#lblName").hide();
                $("#lblEmail").hide();
                $("#lblMessage").hide();
                $("#greencheck").hide();

                var name=false;
                var email=false;
                var message=false;
                var i_name=0;
                var i_email=0;
                var i_message=0;
                var s_name='';
                var s_email='';
                var s_message='';

                $("#name").keypress(function(){
                    check_name();
                });

                $("#name").keydown(function(){
                    check_name();
                });

                $("#name").keyup(function(){
                    check_name();
                });

                $("#name").focusin(function(){
                    check_name();
                });

               
                

                $("#name").focusout(function(){
                    check_name2();
                });
//------------------------------------------------------
                $("#email").keypress(function(){
                    check_email();
                });

                $("#email").keydown(function(){
                    check_email();
                });

                $("#email").keyup(function(){
                    check_email();
                });

                $("#email").focusin(function(){
                    check_email();
                });

                $("#email").focusout(function(){
                    check_email2();
                });
//-------------------------------------------------
                $("#message").keypress(function(){
                    check_message();
                });

                $("#message").keydown(function(){
                    check_message();
                });

                $("#message").keyup(function(){
                    check_message();
                });

                $("#message").focusin(function(){
                    check_message();
                });

                $("#message").focusout(function(){
                    check_message2();
                });

                function check_name(){
                    var name_length = $("#name").val().length;
                    
                   
                        if(name_length<5){
                            if(name_length==0){
                                $("#greencheck").hide();
                       $("#lblName2").hide();
                       $("#lblName").fadeIn();
                   $("#lblName").html("@lang('home.cf_nombre')");
                   
                   document.getElementById("name").style.borderColor = "red";
                  document.getElementById("name").style.backgroundColor = "#FFEBEB";
                  document.getElementById("name").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                   name=true;
                   i_name++;
                   s_name='@lang('home.cf_nombre')<br>';
     
               }else{
                if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#name").val())){
                       // if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                        $("#greencheck").hide();
                            $("#lblName2").hide();
                        
                        $("#lblName").fadeIn();
                        $("#lblName").html("@lang('home.cf_nombre2')");//html("@lang('home.mensaje_contactenos')");
                        
                        document.getElementById("name").style.borderColor = "red";
                       document.getElementById("name").style.backgroundColor = "#FFEBEB";
                       document.getElementById("name").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        name=true;
                        i_name++;
                        s_name='@lang('home.cf_nombre2')<br>';
                        }else{
                            $("#greencheck").hide();
                            $("#lblName2").hide();
                            $("#lblName").fadeIn();
                        $("#lblName").html("@lang('home.cf_nombre3')");
                        
                        document.getElementById("name").style.borderColor = "red";
                       document.getElementById("name").style.backgroundColor = "#FFEBEB";
                       document.getElementById("name").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        name=true;
                        i_name++;
                        s_name='@lang('home.cf_nombre3')<br>';
                        }
                        //i_name++;
                      }
                        
                    }
                    else if(name_length>45){
                        $("#greencheck").hide();
                        $("#lblName2").hide();
                        
                        $("#lblName").fadeIn();

                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#name").val())){
                            
                        
                        $("#lblName").html("@lang('home.cf_nombre4')");//html("@lang('home.mensaje_contactenos')");
                        s_name='@lang('home.cf_nombre4')<br>';
                        }else{
                            $("#lblName").html("@lang('home.cf_nombre5')");
                            s_name='@lang('home.cf_nombre5')<br>';
                        }

                        document.getElementById("name").style.borderColor = "red";
                       document.getElementById("name").style.backgroundColor = "#FFEBEB";
                       document.getElementById("name").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        name=true;
                    i_name++;
                    
                    }

                    else{
                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#name").val())){
                            //if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                            document.getElementById("name").style.borderColor = "green";
                        document.getElementById("name").style.backgroundColor = "#E4FFEF";
                        document.getElementById("name").style.boxShadow = "0 0 1.5px 2.9px #75DA8D";
                        $("#lblName").hide();
                        $("#lblName2").html("@lang('home.cf_nombre6')");
                        $('#lblName2').fadeIn();
                        $('#greencheck').fadeIn();
                        }else{
                            $("#greencheck").hide();
                            $("#lblName2").hide();
                        
                        $("#lblName").fadeIn();
                        $("#lblName").html("@lang('home.cf_nombre7')");
                        document.getElementById("name").style.borderColor = "red";
                       document.getElementById("name").style.backgroundColor = "#FFEBEB";
                       document.getElementById("name").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        name=true;
                        i_name++;
                        s_name='@lang('home.cf_nombre7')<br>';
                        }
                        
                    }
                    //i_name=1;
                }

                function check_name2(){
                    var name_length = $("#name").val().length;
                    
                    
                if(name_length<5){


                    if(name_length==0){
                        $("#greencheck").hide();
                       $("#lblName2").hide();
                       $("#lblName").fadeIn();
                   $("#lblName").html("@lang('home.cf_nombre')");
                   
                   document.getElementById("name").style.borderColor = "red";
                  document.getElementById("name").style.backgroundColor = "#FFEBEB";
                  document.getElementById("name").style.boxShadow = "0 0 0px 0px";
                   name=true;
                   i_name++;
                   s_name='@lang('home.cf_nombre')<br>';
               }else{

                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#name").val())){
                            $("#greencheck").hide();
                            $("#lblName2").hide();
                        
                        $("#lblName").fadeIn();
                        $("#lblName").html("@lang('home.cf_nombre2')");
                        
                        document.getElementById("name").style.borderColor = "red";
                       document.getElementById("name").style.backgroundColor = "#FFEBEB";
                       document.getElementById("name").style.boxShadow = "0 0 0px 0px";
                        name=true;
                        i_name++;
                        s_name='@lang('home.cf_nombre2')<br>';
                        }else{
                            $("#greencheck").hide();
                            $("#lblName2").hide();
                            $("#lblName").fadeIn();
                        $("#lblName").html("@lang('home.cf_nombre3')");
                        
                        document.getElementById("name").style.borderColor = "red";
                       document.getElementById("name").style.backgroundColor = "#FFEBEB";
                       document.getElementById("name").style.boxShadow = "0 0 0px 0px";
                        name=true;
                        i_name++;
                        s_name='@lang('home.cf_nombre3')<br>';
                        }
                    }
                        
                    }
                    
                    
                    
                    else if(name_length>45){
                        $("#greencheck").hide();
                        $("#lblName2").hide();
                        
                        $("#lblName").fadeIn();

                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#name").val())){
                        
                        
                        $("#lblName").html("@lang('home.cf_nombre4')");
                        s_name='@lang('home.cf_nombre4')<br>';
                        }else{
                            $("#lblName").html("@lang('home.cf_nombre5')");
                        s_name='@lang('home.cf_nombre5')<br>';
                        }

                        document.getElementById("name").style.borderColor = "red";
                       document.getElementById("name").style.backgroundColor = "#FFEBEB";
                       document.getElementById("name").style.boxShadow = "0 0 0px 0px";
                        name=true;
                        i_name++;
                    }

                    else{
                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#name").val())){
                            document.getElementById("name").style.borderColor = "green";
                        document.getElementById("name").style.backgroundColor = "#E4FFEF";
                        document.getElementById("name").style.boxShadow = "0 0 0px 0px";
                        $("#lblName").hide();
                        $("#lblName2").html("@lang('home.cf_nombre6')");
                        $('#lblName2').fadeOut();
                        $('#greencheck').fadeIn();
                        
                        }else{
                            $("#greencheck").hide();
                            $("#lblName2").hide();
                        
                        $("#lblName").fadeIn();
                        $("#lblName").html("@lang('home.cf_nombre7')");
                        document.getElementById("name").style.borderColor = "red";
                       document.getElementById("name").style.backgroundColor = "#FFEBEB";
                       document.getElementById("name").style.boxShadow = "0 0 0px 0px";
                        name=true;
                        i_name++;
                        s_name='@lang('home.cf_nombre7')<br>';
                        }
                        
                    }
                   
                }



                function check_email(){

                    var pattern=new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
                    var email_length = $("#email").val().length;

                    if(email_length<=320){//320
                    if(email_length==0){
                                //$("#greencheck").hide();
                       $("#lblEmail2").hide();
                       $("#lblEmail").fadeIn();
                   $("#lblEmail").html("@lang('home.cf_email')");
                   document.getElementById("email").style.borderColor = "red";
                  document.getElementById("email").style.backgroundColor = "#FFEBEB";
                  document.getElementById("email").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                   email=true;
                   i_email++;
                   s_email='@lang('home.cf_email')<br>';
               }else{
                if(pattern.test($("#email").val())){
                    //$("#greencheck").hide();
                    document.getElementById("email").style.borderColor = "green";
                        document.getElementById("email").style.backgroundColor = "#E4FFEF";
                        document.getElementById("email").style.boxShadow = "0 0 1.5px 2.9px #75DA8D";
                            $("#lblEmail").hide();
                            $("#lblEmail2").html("@lang('home.cf_email2')");
                            $('#lblEmail2').fadeIn();
                            //$('#greencheck').fadeIn();
                }else{
                    $("#lblEmail2").hide();
                        $("#lblEmail").fadeIn();
                        $("#lblEmail").html("@lang('home.cf_email3')");//html("@lang('home.mensaje_contactenos')");
                        document.getElementById("email").style.borderColor = "red";
                       document.getElementById("email").style.backgroundColor = "#FFEBEB";
                       document.getElementById("email").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        email=true;
                        i_email++;
                        s_email='@lang('home.cf_email3')<br>';
                }
               }
                    }else{
                        $("#lblEmail2").hide();
                        $("#lblEmail").fadeIn();
                        if(pattern.test($("#email").val())){ 
                        //----------------------------------------------------------
                            $("#lblEmail").html("@lang('home.cf_email4')");//html("@lang('home.mensaje_contactenos')");
                            s_email='@lang('home.cf_email4')<br>';
                            }else{
                                $("#lblEmail").html("@lang('home.cf_email5')");
                            s_email='@lang('home.cf_email5')<br>';
                            }
    
                            document.getElementById("email").style.borderColor = "red";
                           document.getElementById("email").style.backgroundColor = "#FFEBEB";
                           document.getElementById("email").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                            email=true;
                            i_email++;
                       
                    }

                    
                   
                }

                function check_email2(){

var pattern=new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
var email_length = $("#email").val().length;

if(email_length<=320){
if(email_length==0){
            //$("#greencheck").hide();
   $("#lblEmail2").hide();
   $("#lblEmail").fadeIn();
$("#lblEmail").html("@lang('home.cf_email')");

document.getElementById("email").style.borderColor = "red";
document.getElementById("email").style.backgroundColor = "#FFEBEB";
document.getElementById("email").style.boxShadow = "0 0 0px 0px";
email=true;
i_email++;
s_email='@lang('home.cf_email')<br>';

}else{
if(pattern.test($("#email").val())){
//$("#greencheck").hide();
document.getElementById("email").style.borderColor = "green";
    document.getElementById("email").style.backgroundColor = "#E4FFEF";
    document.getElementById("email").style.boxShadow = "0 0 0px 0px";
    
        $("#lblEmail").hide();
        $("#lblEmail2").html("@lang('home.cf_email2')");
        $('#lblEmail2').fadeOut();
        //$('#greencheck').fadeIn();
}else{ 
$("#lblEmail2").hide();
    $("#lblEmail").fadeIn();
    $("#lblEmail").html("@lang('home.cf_email3')");//html("@lang('home.mensaje_contactenos')");
    
    document.getElementById("email").style.borderColor = "red";
   document.getElementById("email").style.backgroundColor = "#FFEBEB";
   document.getElementById("email").style.boxShadow = "0 0 0px 0px";
    email=true;
    i_email++;
    s_email='@lang('home.cf_email3')<br>';
}



}
}else{
    $("#lblEmail2").hide();
    
    $("#lblEmail").fadeIn();

    if(pattern.test($("#email").val())){
        
    //----------------------------------------------------------
        $("#lblEmail").html("@lang('home.cf_email4')");//html("@lang('home.mensaje_contactenos')");
        s_email='@lang('home.cf_email4')<br>';
        }else{
            $("#lblEmail").html("@lang('home.cf_email5')");
            s_email='@lang('home.cf_email5')<br>';
        }

        document.getElementById("email").style.borderColor = "red";
       document.getElementById("email").style.backgroundColor = "#FFEBEB";
       document.getElementById("email").style.boxShadow = "0 0 0px 0px";
        email=true;
        i_email++;
}



}

//----------------------------------------------------------
function check_message(){
                    var message_length = $("#message").val().length;
                        if(message_length<5){
                            if(message_length==0){
                                //$("#greencheck").hide();
                       $("#lblMessage2").hide();
                       $("#lblMessage").fadeIn();
                   $("#lblMessage").html("@lang('home.cf_message')");
                   
                   document.getElementById("message").style.borderColor = "red";
                  document.getElementById("message").style.backgroundColor = "#FFEBEB";
                  document.getElementById("message").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                   message=true;
                   i_message++;
                   s_message='@lang('home.cf_message')<br>';
               }else{
                            $("#lblMessage2").hide();
                        
                        $("#lblMessage").fadeIn();
                        $("#lblMessage").html("@lang('home.cf_message2')");//html("@lang('home.mensaje_contactenos')");
                        
                        document.getElementById("message").style.borderColor = "red";
                       document.getElementById("message").style.backgroundColor = "#FFEBEB";
                       document.getElementById("message").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        message=true;
                        i_message++;
                        s_message='@lang('home.cf_message2')<br>';
                      }   
                    }
                    else if(message_length>320){
                        //$("#greencheck").hide();
                        $("#lblMessage2").hide();
                        
                        $("#lblMessage").fadeIn();

                        $("#lblMessage").html("@lang('home.cf_message3')");//html("@lang('home.mensaje_contactenos')");
                        
                        document.getElementById("message").style.borderColor = "red";
                       document.getElementById("message").style.backgroundColor = "#FFEBEB";
                       document.getElementById("message").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        message=true;
                        i_message++;
                        s_message='@lang('home.cf_message3')<br>';
                    }
                    else{
                         document.getElementById("message").style.borderColor = "green";
                        document.getElementById("message").style.backgroundColor = "#E4FFEF";
                        document.getElementById("message").style.boxShadow = "0 0 1.5px 2.9px #75DA8D";
                        $("#lblMessage").hide();
                        $("#lblMessage2").html("@lang('home.cf_message4')");
                        $('#lblMessage2').fadeIn();
                    }
                }
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                function check_message2(){
                    var message_length = $("#message").val().length;
                    
                    
                if(message_length<5){


                    if(message_length==0){
                        //$("#greencheck").hide();
                       $("#lblMessage2").hide();
                       $("#lblMessage").fadeIn();
                   $("#lblMessage").html("@lang('home.cf_message')");
                   
                   document.getElementById("message").style.borderColor = "red";
                  document.getElementById("message").style.backgroundColor = "#FFEBEB";
                  document.getElementById("message").style.boxShadow = "0 0 0px 0px";
                   message=true;
                   i_message++;
                   s_message='@lang('home.cf_message')<br>';
               }else{

                        
                            //$("#greencheck").hide();
                            $("#lblMessage2").hide();
                        
                        $("#lblMessage").fadeIn();
                        $("#lblMessage").html("@lang('home.cf_message2')");
                        
                        document.getElementById("message").style.borderColor = "red";
                       document.getElementById("message").style.backgroundColor = "#FFEBEB";
                       document.getElementById("message").style.boxShadow = "0 0 0px 0px";
                        message=true;
                        i_message++;
                        s_message='@lang('home.cf_message2')<br>';
                    }
                        
                    }
                    
                    else if(message_length>320){
                        //$("#greencheck").hide();
                        $("#lblMessage2").hide();
                        
                        $("#lblMessage").fadeIn();

                        
                        
                        
                        $("#lblMessage").html("@lang('home.cf_message3')");
                        

                        document.getElementById("message").style.borderColor = "red";
                       document.getElementById("message").style.backgroundColor = "#FFEBEB";
                       document.getElementById("message").style.boxShadow = "0 0 0px 0px";
                        message=true;
                        i_message++;
                        s_message='@lang('home.cf_message3')<br>';
                    }

                    else{
                       
                            document.getElementById("message").style.borderColor = "green";
                        document.getElementById("message").style.backgroundColor = "#E4FFEF";
                        document.getElementById("message").style.boxShadow = "0 0 0px 0px";
                        $("#lblMessage").hide();
                        $("#lblMessage2").html("@lang('home.cf_message4')");
                        $('#lblMessage2').fadeOut();
                        //$('#greencheck').fadeIn();
                        
                       
                        
                    }
                   
                }


               

            $("#submit_form").submit(function(e){

                //var n=i_name+i_email+i_message;
                    name=false;
                    email=false;
                    message=false;
                    i_name=0;
                    i_email=0;
                    i_message=0;
                    s_name='';
                    s_email='';
                    s_message='';

                    check_name2();
                    //check_email();
                    check_email2();
                    check_message2();

                    

                    if(name==false && email==false && message==false){
                        //e.preventDefault();
                        var route=$('#submit-form').data('route');
                        var submit_form=$(this);
                        
                        $.ajax({
                           // url:"{{route('contactenos.send')}}",
                            type:"POST",
                            url: route,
                            //data:{name:name,email:email,message:message},
                            data: submit_form.serialize(),
                            success:function(Response){
               
                                Swal.fire({
                                    type: 'success',
  
  title:'@lang('home.cf_bresponse')',
  html: '<i class="fas fa-check-circle" style="color:green"></i> @lang('home.cf_bresponse1') <i class="fas fa-check-circle"  style="color:green"></i>'+
    '<br><br><div class="alert alert-success" role="alert">'+'@lang('home.cf_bresponse2'): '+$('#name').val()+'<br>'+'@lang('home.cf_bresponse3'): '+$('#email').val()+'</div>',
  animation: false,
  customClass: {
    popup: 'animated tada'
  }
  /*title: 'hola',
			type: 'warning',
			
			showConfirmButton: false,
			timer:1500*/
})
                            }
                        });
                        e.preventDefault();
                    }else{
                        var n=i_name+i_email+i_message;
                        var st=s_name+s_email+s_message;
                        if(n==1){  
                        Swal.fire({
                            
  type: 'error',
  title: '@lang('home.cf_tresponse')',
  /*text: 'Se ha encontrado '+n+' error'+' /n'+st,*/
  html: '<i class="fas fa-exclamation-triangle" style="color:red"></i> @lang('home.cf_tresponse1') '+n+' error <i class="fas fa-exclamation-triangle"  style="color:red"></i>'+
    '<br><br><div class="alert alert-danger" role="alert">'+st+'</div>',
  
})}else{

    Swal.fire({
                            
                            type: 'error',
                            title: '@lang('home.cf_tresponse')',
                            html: '<i class="fas fa-exclamation-triangle" style="color:red"></i> @lang('home.cf_tresponse2') '+
                            n+' @lang('home.cf_tresponse3') <i class="fas fa-exclamation-triangle"  style="color:red"></i>'+
                            '<br><br><div class="alert alert-danger" role="alert">'+st+'</div>',
                            
                          })

}
return false;
                        
                        
                    }

                   
                });

               

        });
           
				
				
			</script>
    

</div>

<script>
$(function(){
    document.getElementsByClassName("swal2-confirm swal2-styled").setAttribute("id", "jojo");

});
</script>
@endsection