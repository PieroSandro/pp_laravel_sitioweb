<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link rel="shortcut icon" type="image/x-icon" href="{{url('/images/ESTRELLA.png')}}"/>
        
		<title>English Quiz</title>
		
		<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet"> 
        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
		
		<link href="{{url('/css/quiz.css')}}" rel="stylesheet">

		<!--------------MODALALERT----------------->
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
      <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>

      <link rel="stylesheet" type="text/css" href="{{url('/css/sweetalert2.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('/css/sweetalert2.min.css')}}">
        <script type="text/javascript" src="{{url('/js/sweetalert2.js')}}"></script>
        <script type="text/javascript" src="{{url('/js/sweetalert2.min.js')}}"></script>
        
        <!-------------------AJAXXXX------------------------------------------------->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-------------------------------------------------------------------->
        <!------------------------------------------->
		<link rel="stylesheet" href="{{url('/css/animate.css')}}">
       <link rel="stylesheet" type="text/css" href="{{url('/css/swiper.min.css')}}">
       <link rel="stylesheet" href="{{url('/font.css')}}">
       <link rel='stylesheet' type='text/css' href="{{url('/css/stylesheet.css')}}">
        <link rel='stylesheet' type='text/css' href="{{url('/css/bootstrap.min.css')}}">
        <link rel='stylesheet' href="https://fonts.googleapis.com/css?family=Coolvetica">
        <link rel='stylesheet' type='text/css' href="{{url('/css/font.css')}}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <script type='text/javascript' src="{{url('/js/jquery-2.1.3.min.js')}}"></script>
        <script type='text/javascript' src="{{url('/js/jQuery-2.1.4.min.js')}}"></script>
        <script type='text/javascript' src="{{url('/js/jquery-3.3.1.min.js')}}"></script>
        <script type='text/javascript' src="{{url('/js/jquery-1.11.3.min.js')}}"></script>
        <script type='text/javascript' src="{{url('/js/bootstrap.min.js')}}"></script>
        <script defer src="{{url('/js/all.js')}}" integrity="sha384-kW+oWsYx3YpxvjtZjFXqazFpA7UP/MbiY4jvs+RWZo2+N94PFZ36T6TFkc9O3qoB" crossorigin="anonymous"></script>
  
	</head>
	<body>
	<style>
			div.title2{
	text-align: center;
	text-transform: uppercase;
	
}



.next-btn{
	border: none;
	outline: none;
	background: rgba(173, 180, 182, 0.835);
	width: 180px;
	height: 35px;
	border-radius: 20px;
	cursor: pointer;
	margin: 10px;
}

.next-btn:hover{
	background: #08038C;
	color: #f6f6f6;
}


@media screen and (min-width: 601px) {
  
  .next-btn{
	
	float:right;
  }

  #imglogoce{
	width:165px;
  }
  
}

@media screen and (max-width: 600px) {
  
  .next-btn{
	
	float:center;
  }

  #imglogoce{
	width:100px;
	}
}

@media screen and (min-width: 1201px) {
	

	#imglogoce{
		padding-top: 17px;
	}
	div.title2 {
		padding-top: 2px;
    font-size: 48px;
  }
}


@media screen and (min-width: 961px) and (max-width: 1200px){
	
	#imglogoce{
		padding-top: 10px;
	}
  div.title2 {
	padding-top: 8px;
	font-size: 30px;
  }
}

@media screen and (max-width: 960px) {
	#imglogoce{
		padding-top: 0px;
	}
  div.title2 {
	padding-top: 0px;
	font-size: 30px;
  }
}
		</style><!--Upper-intermediate and Advanced test -->
		<div id="quizContainer" class="container-fluid">
		<div class="row as" id="top">
		<div class="col-12 col-md-12 col-lg-1">
            <div class="container text-center text-md-center text-lg-center">
            <a href="{{url('/index')}}" class="navbar-brand"><img src="{{url('/images/logoce.png')}}" id="imglogoce"></a><!--style="width:165px;" -->
            </div>
        
        </div>
        
        <div class="col-12 col-md-12 col-lg-11">
            <div class="container text-center text-md-center text-lg-center">
            <div class="title2" style="color:white; font-family: 'Montserrat', sans-serif;">Upper-intermediate and Advanced test</div></div>
        </div>

		</div>
			<div id="question" style="font-family: 'Montserrat', sans-serif;" class="question"></div>
			<div class="card" style="padding-top: 20px;padding-right: 20px;
  padding-bottom: 20px;
  padding-left: 20px; border-radius:30px;">
			<label class="option"><input type="radio" name="option" value="1" /> <span style="font-family: 'Montserrat', sans-serif;" id="opt1"></span></label>
			<label class="option"><input type="radio" name="option" value="2" /> <span style="font-family: 'Montserrat', sans-serif;" id="opt2"></span></label>
			<label class="option"><input type="radio" name="option" value="3" /> <span style="font-family: 'Montserrat', sans-serif;" id="opt3"></span></label>
			<label class="option"><input type="radio" name="option" value="4" /> <span style="font-family: 'Montserrat', sans-serif;" id="opt4"></span></label>
			<div style="text-align:center;">
			<button id="nextButton" class="next-btn" onclick="loadNextQuestion();"><div style="font-family: 'Montserrat', sans-serif;">Next Question <i class="fas fa-angle-right nba" id="fright"></i></div></button>
			</div>
			<div>
			<br>
			
			
		</div>
		
			
		<div id="result" class="container result" style="display:none;">
		</div>
		
	</body>
</html>





<!--------------------------------------------------------------------------------->
<?php
              use App\Avanzadopregunta;
              //$patros=Patro::orderBy('id','ASC')->where('id',1)->paginate();
              //$patros=Patro::all();
              $avanzadopreguntas=Avanzadopregunta::all();

			  
			  $counter1=1;?>
			  <script>
					var questions=[];
			  </script><?php
              foreach($avanzadopreguntas as $avanzadopregunta){
				  
				//$htmlString=$vcurso->titulo;
				//$htmlString_2=$vcurso->link;
				$question=$avanzadopregunta->question;
				$option1=$avanzadopregunta->option1;
				$option2=$avanzadopregunta->option2;
				$option3=$avanzadopregunta->option3;
				$option4=$avanzadopregunta->option4;
				$answer=$avanzadopregunta->answer;
				 ?>
				  
				
				
				<script>

				var q_json={
	"question": "{{$question}}",
	"option1": "a. {{$option1}}",
	"option2": "b. {{$option2}}",
	"option3": "c. {{$option3}}",
	"option4": "d. {{$option4}}",
	"answer": "{{$answer}}"

}
				questions.push(q_json);
				</script><?php 
				//$htmlString=$patro->titulo;
				$counter1++;
				

				
				  }
				  
				 
				 
				 
				 ?>
<!---------------------------------------------------------------------------------------->
<?php $numbien= 2; 
?>

<?php
use App\Quizresultado;
$quizresultados=Quizresultado::where('id',3)->get();
foreach($quizresultados as $quizresultado){
	$puntajevalor=$quizresultado->numres;
}?>
<script>



var currentQuestion = 0;
var score = 0;
var totQuestions = questions.length;

var container = document.getElementById('quizContainer');
var questionEl = document.getElementById('question');
var opt1 = document.getElementById('opt1');
var opt2 = document.getElementById('opt2');
var opt3 = document.getElementById('opt3');
var opt4 = document.getElementById('opt4');
var nextButton = document.getElementById('nextButton');
var resultCont = document.getElementById('result');

function loadQuestion (questionIndex) {
	var q = questions[questionIndex];
	questionEl.textContent = (questionIndex + 1) + '. ' + q.question;
	opt1.textContent = q.option1;
	opt2.textContent = q.option2;
	opt3.textContent = q.option3;
	opt4.textContent = q.option4;
};

function loadNextQuestion () {
	var selectedOption = document.querySelector('input[type=radio]:checked');
	if(!selectedOption){
		
		Swal.fire({
			
			html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
			'<br><br><h1>Warning!</h1><br>Please select your answer!',
			
			showConfirmButton: false,
			timer:1500
		  })
		  
		return;
	}
	var answer = selectedOption.value;
	if(questions[currentQuestion].answer == answer){
		score += 1;//10
	}
	selectedOption.checked = false;
	currentQuestion++;
	if(currentQuestion == totQuestions - 1){
		nextButton.textContent = 'Finish';
		nextButton.style.fontFamily ="Montserrat,sans-serif";
	}
	if(currentQuestion == totQuestions){
		container.style.display = 'none';
		resultCont.style.display = '';
		









		var ptvr=<?php echo $puntajevalor; ?>;
		if(score<ptvr)
		{
			Swal.fire({
				
				html: '<i class="fas fa-info-circle" style="color:#1CDDE6; font-size:150px;"></i>'+
			'<br><br><h1>The test is over!</h1><br><p>You are in <b>Beginner Level</b>, your score is:</p><br>'+score,
			animation: false,
			customClass: {
			  popup: 'animated wobble'
			}
				
			  })
			
			
		return;
		}
		else
		{
			
			
			Swal.fire({
				html: '<i class="fas fa-info-circle" style="color:#1CDDE6; font-size:150px;"></i>'+
			'<br><br><h1>The test is over!</h1><br><p>You are in <b>Elementary Level</b>, your score is:</p><br>'+score+
			'<br><br><h3>Good Job!</h3>',
			animation: false,
			customClass: {
			  popup: 'animated wobble'
			}
				
				
			  })
			return;
		
		
			
		}
		
	}
	loadQuestion(currentQuestion);
}

loadQuestion(currentQuestion);

</script>