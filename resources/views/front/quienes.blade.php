@extends('layouts.master2')
@section('title')
@lang('home.somos_index')

@endsection







@section('content')
<!--------------ANIMATE ON SCROLL------------------------->
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        
        <script>
        AOS.init({
          duration:3000,
        });
        </script>
        <!------------------------------------------->
<style>

.formpar{
    
    
    vertical-align: middle;
  border-bottom: 35px solid rgba(228, 20, 20, 0.931);
  border-left: 20px solid transparent;
  height: 0;
  /*width: 50%;*/
  box-shadow: 5px 0px white, 10px 0px rgba(26, 26, 161, 0.993), 15px 0px white, 20px 0px rgba(26, 26, 161, 0.993), 25px 0px white, 30px 0px rgba(26, 26, 161, 0.993);
  transform: skew(30deg);
  /*border-left:10px;*/

}
</style>

<label id="qs">

<br>
<br>

<div class="text">
<div data-aos="fade-right" style="padding-top: 40px;
  padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">

    <div class="formpar" id="fpr"
    ><h3><div style="transform: skewX(-45deg); color: white; font-family: 'Montserrat', sans-serif;">@lang('home.somos_index')</div></h3></div>
    <br>  
   
<!------------------------------------------------------------------------------------>

<?php
$locale=App::getLocale();
            if(App::isLocale('en')){
              ?>
         @if(count($main2pages)>0)
            @foreach($main2pages as $main2page)
            @if($main2page->si_primero_foto=='si_primero_foto')       
<center>
<img src="/intranet/storage/app/public/images/{{$main2page->foto_en}}" id="img1"/></center>  
            <br><br>
            <div id="txt4" style="text-align: justify; font-family: Montserrat, sans-serif; width:100%;">
<p style="text-align: justify; font-family: Montserrat, sans-serif;">{!!$main2page->body_en!!}</p>
</div>
            @else
            <div id="txt4" style="text-align: justify; font-family: Montserrat, sans-serif; width:100%;">
<p style="text-align: justify; font-family: Montserrat, sans-serif;">{!!$main2page->body_en!!}</p>
</div>
<br><br>
<center>
<img src="/intranet/storage/app/public/images/{{$main2page->foto_en}}" id="img1"/></center>  
            @endif      
            @endforeach
            {{$main2pages->links()}}
            @else
              @endif
                <?php                }else{
            ?>
             @if(count($main2pages)>0)
            @foreach($main2pages as $main2page)
            @if($main2page->si_primero_foto=='si_primero_foto')
            <center><img src="/intranet/storage/app/public/images/{{$main2page->foto_es}}" id="img1"/></center>
<br><br><div id="txt4" style="text-align: justify; font-family: Montserrat, sans-serif; width:100%;">
<p style="text-align: justify; font-family: Montserrat, sans-serif;">{!!$main2page->body_es!!}</p></div>
            @else
            <div id="txt4" style="text-align: justify; font-family: Montserrat, sans-serif; width:100%;">
<p style="text-align: justify; font-family: Montserrat, sans-serif;">{!!$main2page->body_es!!}</p></div><br><br>
<center><img src="/intranet/storage/app/public/images/{{$main2page->foto_es}}" id="img1"/></center>
            @endif
            <!-- -------------------------------------------------------------------->
                        <!-- -------------------------------------------------------------------->
            @endforeach
            {{$main2pages->links()}}
            @else
              @endif
              <?php
            }
?>
<!------------------------------------------------------------------------------------>
</div>
</div>
<br>
</label>

<script>
    $( document ).ready(function() {
      function Scrolldown() {
     //window.scroll(0,562); 
     document.getElementById("qs").scrollIntoView();
}

window.onload = Scrolldown;
    });

    $( window ).on( "load", function() {
      function Scrolldown() {
     //window.scroll(0,562); 
     document.getElementById("qs").scrollIntoView();
}

window.onload = Scrolldown;
    });
 
    /*$( window ).on( "load", function() {
        console.log( "window loaded" );
    });*/
    </script>
<script>




/*$(window).load(function()() {
  var elmnt = document.getElementById("img1");
  elmnt.scrollIntoView();
}*/



          $(function(){
            
                $(window).resize(function(){
                    winSize();
               });

               function winSize(){
               
        if ($(window).width() < 768) {
          document.getElementById("fpr").style.width = "100%";
          
        }
    
    else{
      if ($(window).width() < 992) {
      document.getElementById("fpr").style.width = "100%";
     
      }else{
        if($(window).width() < 1200){
          document.getElementById("fpr").style.width = "50%";
          
        }else{
          document.getElementById("fpr").style.width = "50%";
         
        }
      }
    }
          }
           

               $(window).show(function(){
                    win2();
               });

               function win2(){
               
        if ($(window).width() < 768) {
          document.getElementById("fpr").style.width = "100%";
          
        }
    
    else{
      if ($(window).width() < 992) {
      document.getElementById("fpr").style.width = "100%";
      
      }else{
        if($(window).width() < 1200){
          document.getElementById("fpr").style.width = "50%";
          
        }else{
          document.getElementById("fpr").style.width = "50%";
          
        }
      }
    }
          }
          });
        </script>
@endsection


