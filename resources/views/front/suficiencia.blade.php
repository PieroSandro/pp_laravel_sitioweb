@extends('layouts.master2')
@section('title')
@lang('home.exsuficiencia_menu')
@endsection
@section('content')


<!--------------ANIMATE ON SCROLL------------------------->
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        
        <script>
        AOS.init({
          duration:3000,
        });
        </script>
        <!------------------------------------------->

        <style>
      #boton_bal:focus {
  outline: 0;
}
/*
#boton_bal:focus-visible {
  outline: 0;
}*/
      </style>
<style>
#img33
{
	
	width:70%;
	/*max-width:600px;
	min-width:300px;*/
	border-radius: 20%;
	
	
}

.formpar{
    
    vertical-align: middle;
  /*border-bottom: 35px solid rgba(228, 20, 20, 0.931);
  border-left: 20px solid transparent;
  height: 0;*/
  /*width:92%;*/
  box-shadow: 5px 0px white, 10px 0px rgba(26, 26, 161, 0.993), 15px 0px white, 20px 0px rgba(26, 26, 161, 0.993), 25px 0px white, 30px 0px rgba(26, 26, 161, 0.993);
  transform: skew(30deg);
 
}
</style>


<label id="qs5">

<br>
<br>

<div class="text">
<div data-aos="fade-right" style="padding-top: 40px;
  padding-right: 40px;
  padding-bottom: 0px;
  padding-left: 40px;">
            
            
   
  <!------------------------------------------------------------------------------------>

  <?php

$locale=App::getLocale();
          

            if(App::isLocale('en')){
              ?>
         
        
         @if(count($mainpages)>0)
            @foreach($mainpages as $mainpage)
           
            <center>
            <img src="/intranet/storage/app/public/images/{{$mainpage->foto_en}}" id="img1"/></center>           
            @endforeach

            {{$mainpages->links()}}
            @else
              @endif
                <?php    
                        
          }else{
            ?>
             
             @if(count($mainpages)>0)
            @foreach($mainpages as $mainpage)
           
           <center>
           <img src="/intranet/storage/app/public/images/{{$mainpage->foto_es}}" id="img1"/></center>         
            @endforeach

            {{$mainpages->links()}}
            @else
              @endif
         
              <?php
            }
?>
<!------------------------------------------------------------------------------------>

         
            
            
            <!--<center>
  	<img src="images/Engligh_class.jpg" id="img1"/></center>-->
    
    
    
    
    
    
    
    
    
    <br><br>

</div>

<div style="padding-top: 0px;
  padding-right: 00px;
  padding-bottom: 0px;
  padding-left: 0px;">

	  <div class="formpar" id="fpr"><h3><div style="transform: skewX(-45deg); color: white; font-family: 'Montserrat', sans-serif;">@lang('home.exsuficiencia_menu')</div></h3></div>
        <!--<h3>@lang('home.exsuficiencia_menu')</h3>-->
</div>

        <div style="padding-top: 0px;
  padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">
        <br>
		<!--@lang('home.exsuficiencia_cont')-->
 <!------------------------------------------------------------------------------------>

 <?php

$locale=App::getLocale();
          

            if(App::isLocale('en')){
              ?>
         
        
         @if(count($mainpages)>0)
            @foreach($mainpages as $mainpage)
            <div id="txt4" style="text-align: justify; font-family: Montserrat, sans-serif; width:100%;">
            <p style="text-align: justify; font-family: Montserrat, sans-serif;">{!!$mainpage->body_en!!}</p>
            </div>
            <br><br>
            @endforeach

            {{$mainpages->links()}}
            @else
              @endif
                <?php    
                        
          }else{
            ?>
             
             @if(count($mainpages)>0)
            @foreach($mainpages as $mainpage)
            <div id="txt4" style="text-align: justify; font-family: Montserrat, sans-serif; width:100%;">
            <p style="text-align: justify; font-family: Montserrat, sans-serif;">{!!$mainpage->body_es!!}</p>
           </div>
           <br><br>
            @endforeach

            {{$mainpages->links()}}
            @else
              @endif
         
              <?php
            }
?>
<!------------------------------------------------------------------------------------>







	<br>



      <div class="text-center"> 
            <a class="btn btn-primary" id="boton_bal" href="APE_INDICE.pdf" target="_blank" style="font-family: Montserrat, sans-serif; border:none; background-color: rgba(4, 10, 100, 0.808);">Balotario</a>

            <!--<button class="myButton" id="myButton" href="APE_INDICE.pdf" target="_blank" role="button" style="box-shadow: rgb(39, 104, 115) 0px 10px 14px -7px; background: rgb(89, 155, 179) linear-gradient(rgb(89, 155, 179) 5%, rgb(64, 140, 153) 100%) repeat scroll 0% 0%; border-radius: 8px; display: inline-block; cursor: pointer; color: rgb(255, 255, 255); font-family: Arial; font-size: 20px; font-weight: bold; padding: 13px 32px; text-decoration: none; text-shadow: rgb(61, 118, 138) 0px 1px 0px;">turquoise</button>
-->
          
            
        </div> 
			<!--<center>
			<a class="btn btn-primary" href="APE_INDICE.pdf" target="_blank" role="button">Balotario</a>
      
      
      
      
      
      <br><br>
		
	

		<br>
		@lang('home.exsuficiencia_botones')
		
</center>-->
		</div>
    </div>
    
    

		<br>
    </label>
    <script>
    $( document ).ready(function() {
      function Scrolldown() {
     //window.scroll(0,562); 
     document.getElementById("qs5").scrollIntoView();
}

window.onload = Scrolldown;
    });

    $( window ).on( "load", function() {
      function Scrolldown() {
     
     document.getElementById("qs5").scrollIntoView();
}

window.onload = Scrolldown;
    });
 
   
    </script>
		<?php
           
            $locale=App::getLocale();

            if(App::isLocale('en')){
              ?>











<script>
          $(function(){
            
                $(window).resize(function(){
                    winSize();
               });


               //border-bottom: 80px solid rgba(228, 20, 20, 0.931);
 //border-left: 46px solid transparent;
 //height: 80px;
 function winSize(){
               
               if ($(window).width() < 394) {
                 //document.getElementById("fpr").style.width = "100%";
                 document.getElementById("fpr").style.marginLeft = "20px";
               document.getElementById("fpr").style.marginRight = "20px";
                 document.getElementById("fpr").style.borderBottom = "80px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "46px solid transparent";
                 document.getElementById("fpr").style.height = "80px";
                 document.getElementById("fpr").style.width = "76%";
               
               }
           
           else{
             if ($(window).width() < 751) {
               document.getElementById("fpr").style.marginLeft = "33px";
               document.getElementById("fpr").style.marginRight = "33px";
               document.getElementById("fpr").style.borderBottom = "35px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "20px solid transparent";
                 document.getElementById("fpr").style.height = "0px";
                 document.getElementById("fpr").style.width = "80%";
             //document.getElementById("fpr").style.width = "100%";
            
             }else{
               if($(window).width() < 845){
                document.getElementById("fpr").style.marginLeft = "20px";
               document.getElementById("fpr").style.marginRight = "20px";
                 document.getElementById("fpr").style.borderBottom = "80px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "46px solid transparent";
                 document.getElementById("fpr").style.height = "80px";
                 document.getElementById("fpr").style.width = "76%";
                 //document.getElementById("fpr").style.width = "50%";
                 
               }else{
                 
                  document.getElementById("fpr").style.marginLeft = "33px";
               document.getElementById("fpr").style.marginRight = "33px";
               document.getElementById("fpr").style.borderBottom = "35px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "20px solid transparent";
                 document.getElementById("fpr").style.height = "0px";
                 document.getElementById("fpr").style.width = "80%";
               }
             }
           }
                 }
           

               $(window).show(function(){
                    win2();
               });

               function win2(){
               
               if ($(window).width() < 394) {
                 //document.getElementById("fpr").style.width = "100%";
                 document.getElementById("fpr").style.marginLeft = "20px";
               document.getElementById("fpr").style.marginRight = "20px";
                 document.getElementById("fpr").style.borderBottom = "80px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "46px solid transparent";
                 document.getElementById("fpr").style.height = "80px";
                 document.getElementById("fpr").style.width = "76%";
               
               }
           
           else{
             if ($(window).width() < 751) {
               document.getElementById("fpr").style.marginLeft = "33px";
               document.getElementById("fpr").style.marginRight = "33px";
               document.getElementById("fpr").style.borderBottom = "35px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "20px solid transparent";
                 document.getElementById("fpr").style.height = "0px";
                 document.getElementById("fpr").style.width = "80%";
             //document.getElementById("fpr").style.width = "100%";
            
             }else{
               if($(window).width() < 845){
                document.getElementById("fpr").style.marginLeft = "20px";
               document.getElementById("fpr").style.marginRight = "20px";
                 document.getElementById("fpr").style.borderBottom = "80px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "46px solid transparent";
                 document.getElementById("fpr").style.height = "80px";
                 document.getElementById("fpr").style.width = "76%";
                 //document.getElementById("fpr").style.width = "50%";
                 
               }else{
                 
                  document.getElementById("fpr").style.marginLeft = "33px";
               document.getElementById("fpr").style.marginRight = "33px";
               document.getElementById("fpr").style.borderBottom = "35px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "20px solid transparent";
                 document.getElementById("fpr").style.height = "0px";
                 document.getElementById("fpr").style.width = "80%";
                
               }
             }
           }
                 }
          });
        </script>


<?php    
                        
          }else{?>

<script>
          $(function(){
            
                $(window).resize(function(){
                    winSize();
               });


               //border-bottom: 80px solid rgba(228, 20, 20, 0.931);
 //border-left: 46px solid transparent;
 //height: 80px;
 function winSize(){
               
               if ($(window).width() < 550) {
                 //document.getElementById("fpr").style.width = "100%";
                 document.getElementById("fpr").style.marginLeft = "20px";
               document.getElementById("fpr").style.marginRight = "20px";
                 document.getElementById("fpr").style.borderBottom = "80px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "46px solid transparent";
                 document.getElementById("fpr").style.height = "80px";
                 document.getElementById("fpr").style.width = "76%";
               
               }
           
           else{
             if ($(window).width() < 751) {
               document.getElementById("fpr").style.marginLeft = "33px";
               document.getElementById("fpr").style.marginRight = "33px";
               document.getElementById("fpr").style.borderBottom = "35px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "20px solid transparent";
                 document.getElementById("fpr").style.height = "0px";
                 document.getElementById("fpr").style.width = "80%";
             //document.getElementById("fpr").style.width = "100%";
            
             }else{
               if($(window).width() < 975){
                document.getElementById("fpr").style.marginLeft = "20px";
               document.getElementById("fpr").style.marginRight = "20px";
                 document.getElementById("fpr").style.borderBottom = "80px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "46px solid transparent";
                 document.getElementById("fpr").style.height = "80px";
                 document.getElementById("fpr").style.width = "76%";
                 //document.getElementById("fpr").style.width = "50%";
                 
               }else{
                 
                  document.getElementById("fpr").style.marginLeft = "33px";
               document.getElementById("fpr").style.marginRight = "33px";
               document.getElementById("fpr").style.borderBottom = "35px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "20px solid transparent";
                 document.getElementById("fpr").style.height = "0px";
                 document.getElementById("fpr").style.width = "80%";
                
               }
             }
           }
                 }
           

               $(window).show(function(){
                    win2();
               });

               function win2(){
               
               if ($(window).width() < 550) {
                 //document.getElementById("fpr").style.width = "100%";
                 document.getElementById("fpr").style.marginLeft = "20px";
               document.getElementById("fpr").style.marginRight = "20px";
                 document.getElementById("fpr").style.borderBottom = "80px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "46px solid transparent";
                 document.getElementById("fpr").style.height = "80px";
                 document.getElementById("fpr").style.width = "76%";
               
               }
           
           else{
             if ($(window).width() < 751) {
               document.getElementById("fpr").style.marginLeft = "33px";
               document.getElementById("fpr").style.marginRight = "33px";
               document.getElementById("fpr").style.borderBottom = "35px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "20px solid transparent";
                 document.getElementById("fpr").style.height = "0px";
                 document.getElementById("fpr").style.width = "80%";
             //document.getElementById("fpr").style.width = "100%";
            
             }else{
               if($(window).width() < 975){
                document.getElementById("fpr").style.marginLeft = "20px";
               document.getElementById("fpr").style.marginRight = "20px";
                 document.getElementById("fpr").style.borderBottom = "80px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "46px solid transparent";
                 document.getElementById("fpr").style.height = "80px";
                 document.getElementById("fpr").style.width = "76%";
                 //document.getElementById("fpr").style.width = "50%";
                 
               }else{
                 
                  document.getElementById("fpr").style.marginLeft = "33px";
               document.getElementById("fpr").style.marginRight = "33px";
               document.getElementById("fpr").style.borderBottom = "35px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "20px solid transparent";
                 document.getElementById("fpr").style.height = "0px";
                 document.getElementById("fpr").style.width = "80%";
                
               }
             }
           }
                 }
          });
        </script>
<?php
            }

          ?>

  
@endsection