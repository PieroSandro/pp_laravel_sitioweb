@extends('layouts.master2')
@section('title')
@lang('home.reclamaciones_menu')
@endsection


@section('content')
<style>


option:hover{
    background-color:black;
}

.swal2-modal .swal2-styled{
    
    background-color: rgba(4, 10, 100, 0.808);
    font-family: 'Montserrat', sans-serif;
}


.swal2-title{
    font-family: 'Montserrat', sans-serif;
}


#swal2-content{
    font-family: 'Montserrat', sans-serif;
}
</style>

<div class="row" style="font-family: 'Montserrat', sans-serif;">
    <!--<div class="col-sm-6 offset-sm-3">-->

    <div class="col-12 col-sm-8 col-md-12 col-lg-8 col-xl-8 offset-sm-2 offset-md-0 offset-lg-2 offset-xl-2">

        <br>
        <h1><div style="color: black; font-family: 'Montserrat', sans-serif;">@lang('home.reclamaciones_menu')</div></h1>
    <br>
    
        <form class="sfr" id="submit_form" data-route="{{route('reclamaciones.send')}}" method="POST" enctype="multipart/form-data">
            @csrf
            @if(session('success'))
                
                    {{session('success')}}
                
            @endif

            <div class="form-group">
            <div class="checkbox">
                <label for="si_menor_edad">
                    
                
                <input type="checkbox" id="ch1" name="si_menor_edad" value="si_menor_edad"> @lang('home.cf_ch2')
                    </label>
            </div>
            <br>
            <label id="lblCh1" style="color:green"></label>
            </div>
            
            <div class="form-group">
            
            <label for="name" class="mt-3">@lang('home.rf_nombres')</label>
            <input type="text" class="form-control" name="name" id="name" style="box-shadow: 0 0 0px;">

                   <br>
            
                   <label id="lblName" style="color:red"></label>
            <label id="lblName2" style="color:green"></label>
               
            </div>


            <div class="form-group">
            <label for="apellidop" class="mt-3">@lang('home.cf_bresponse4')</label>
            <input type="text" class="form-control" name="apellidop" id="apellidop" style="box-shadow: 0 0 0px;">

                   <br>
                   <label id="lblApellidop" style="color:red"></label>
            <label id="lblApellidop2" style="color:green"></label>
                
        </div>
        
        <div class="form-group">
        <label for="apellidom" class="mt-3">@lang('home.cf_bresponse5')</label>
            <input type="text" class="form-control" name="apellidom" id="apellidom" style="box-shadow: 0 0 0px;">

                   <br>
                   <label id="lblApellidom" style="color:red"></label>
            <label id="lblApellidom2" style="color:green"></label>
        </div>






        <div class="form-group">
        
             
            
            <label for="distrito">@lang('home.cf_bresponse6')</label>
            <select class="form-control input-sm" name="distrito" id="distrito">
                <option value="">-- @lang('home.rf_distrito') --</option>
                <option value="Ancón">Ancón</option>
                <option value="Ate Vitarte">Ate Vitarte</option>
                <option value="Barranco">Barranco</option>
                <option value="Breña">Breña</option>   
                <option value="Chorrillos">Chorrillos</option>
                <option value="Comas">Comas</option>
            </select>
        <br>  
        <label id="lblDistrito" style="color:red"></label>
            <label id="lblDistrito2" style="color:green"></label>
            </div>
            

            <div class="form-group">
            <label for="telefono" class="mt-3">@lang('home.cf_bresponse7')</label>
        

            <input type="text" name="telefono" id="telefono" class="form-control">
            
            <br>
            <label id="lblTelefono" style="color:red"></label>
            <label id="lblTelefono2" style="color:green"></label>
            </div>

          
            <div class="form-group">
            <label for="t_documento" class="mt-3">@lang('home.rf_documento')</label>
            <select class="form-control input-sm" name="t_documento" id="t_documento">
                <option value="">-- @lang('home.rf_documento2') --</option>
                <option value="DNI">@lang('home.rf_documento3')</option>
                <option value="Pasaporte">@lang('home.rf_documento4')</option>
                
            </select>
        <br>
        <label id="lblT_documento" style="color:red"></label>
            <label id="lblT_documento2" style="color:green"></label>
            </div>



            <div class="form-group">
            <label for="n_documento" class="mt-3">@lang('home.cf_bresponse9')</label>
              <input type="text" name="n_documento" id="n_documento" class="form-control">
            <br>
            <label id="lblN_documento" style="color:red"></label>
            <label id="lblN_documento2" style="color:green"></label>
            </div>
           
            <div class="form-group">
            
            <label for="email" class="mt-3">@lang('home.email_contactenos')</label>

            <input type="text" class="form-control" name="email" id="email" style="box-shadow: 0 0 0px;">
                   <br>
            
            <label id="lblEmail" style="color:red"></label>
            <label id="lblEmail2" style="color:green"></label>
        </div>
            
            <h5><div style="color: black; font-family: 'Montserrat', sans-serif;">@lang('home.rf_documento5')</div></h5>
            <div id="t_rs">
            <input type="radio" name="t_reclamo" id="reclamo" value="@lang('home.cf_t_r1')">
            @lang('home.cf_t_r1')
            <input type="radio" name="t_reclamo" id="queja" value="@lang('home.cf_t_r2')">
            @lang('home.cf_t_r2')
            <input type="radio" name="t_reclamo" id="sugerencia" value="@lang('home.cf_t_r3')">
            @lang('home.cf_t_r3')
            </div>

            <br>  
            <label id="lblD_reclamacion" style="color:red"></label>
            <label id="lblD_reclamacion2" style="color:green"></label>

           
            <div class="form-group">

<label for="message" class="mt-3">@lang('home.mensaje_contactenos')</label>

<textarea class="form-control" cols="30" rows="10" name="message" id="message" style="box-shadow: 0 0 0px;"></textarea>
            
       
                  <br>
            
                  <label id="lblMessage" style="color:red"></label>
                  <label id="lblMessage2" style="color:green"></label>
        </div>
            

    <!--        
    <input type="submit" class="btn btn-primary" name="submit" id="submit" 
    value="@lang('home.boton_contactenos')">-->

    <label class="col text-center">
        <button type="submit" name="submit" id="submit" class="btn" 
        style="color: white; border: rgba(11, 11, 132, 0.993); background-color:rgba(11, 11, 132, 0.993) ;font-family: Montserrat, sans-serif;">
<i class="fas fa-envelope"></i> @lang('home.boton_contactenos')
</button>        
</label>        
        
        <span id="error_message" class="text-danger"></span>
        <span id="success_message" class="text-success"></span>
    
    
    </form>
          
        <br>


    </div>
<!------------------ANALISIS AJAX------------------------------------------>

     <!--13_2_2020-->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
   <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

   <!--13_2_2020-->

<script>

        $(function(){

            $("#lblName").hide();
            $("#lblApellidop").hide();
            $("#lblApellidom").hide();
            $("#lblDistrito").hide();
            $("#lblTelefono").hide();
            $("#lblT_documento").hide();
            $("#lblN_documento").hide();
            $("#lblEmail").hide();
            $("#lblD_reclamacion").hide();
            $("#lblMessage").hide();

            var name=false;
            var apellidop=false;
            var apellidom=false;
            var distrito=false;
            var telefono=false;
            var t_documento=false;
            var n_documento=false;
            var email=false;
            var t_reclamo=false;
            var message=false;

            var i_name=0;
            var i_apellidop=0;
            var i_apellidom=0;
            var i_distrito=0;
            var i_telefono=0;
            var i_t_documento=0;
            var i_n_documento=0;
            var i_email=0;
            var i_t_reclamo=0;
            var i_message=0;

            var s_name='';
            var s_apellidop='';
            var s_apellidom='';
            var s_distrito='';
            var s_telefono='';
            var s_t_documento='';
            var s_n_documento='';
            var s_email='';
            var s_t_reclamo='';
            var s_message='';

            
           


	
            //------------------------------------------

            $("#name").keypress(function(){
                
                    check_name();
                });

                $("#name").keydown(function(){
                    check_name();
                });

                $("#name").keyup(function(){
                    check_name();
                });

                $("#name").focusin(function(){
                    check_name();
                });

                $("#name").focusout(function(){
                    check_name2();
                });
                //------------------------
                $("#apellidop").keypress(function(){
                    check_apellidop();
                });

                $("#apellidop").keydown(function(){
                    check_apellidop();
                });

                $("#apellidop").keyup(function(){
                    check_apellidop();
                });

                $("#apellidop").focusin(function(){
                    check_apellidop();
                });

                $("#apellidop").focusout(function(){
                    check_apellidop2();
                });

                //-----------------------------
                $("#apellidom").keypress(function(){
                    check_apellidom();
                });

                $("#apellidom").keydown(function(){
                    check_apellidom();
                });

                $("#apellidom").keyup(function(){
                    check_apellidom();
                });

                $("#apellidom").focusin(function(){
                    check_apellidom();
                });

                $("#apellidom").focusout(function(){
                    check_apellidom2();
                });

                //------------------------------------
                $("#distrito").click(function(){
                    check_distrito();
                });

              
                $("#distrito").focusout(function(){
                    check_distrito2();
                });
                //-----------------------------------
                
                $("#telefono").keypress(function(){
                    check_telefono();
                });

                $("#telefono").keydown(function(){
                    check_telefono();
                });

                $("#telefono").keyup(function(){
                    check_telefono();
                });

                $("#telefono").focusin(function(){
                    check_telefono();
                });

                $("#telefono").focusout(function(){
                    check_telefono2();
                });

                //------------------------------------
                $("#t_documento").click(function(){
                    check_t_documento();
                });
               
                $("#t_documento").focusout(function(){
                    check_t_documento2();
                });

                //-----------------------------------

                $("#n_documento").keypress(function(){
                    check_n_documento();
                });

                $("#n_documento").keydown(function(){
                    check_n_documento();
                });

                $("#n_documento").keyup(function(){
                    check_n_documento();
                });

                $("#n_documento").focusin(function(){
                    check_n_documento();
                });

                $("#n_documento").focusout(function(){
                    check_n_documento2();
                });

                //-----------------------------------

                $("#email").keypress(function(){
                    check_email();
                });

                $("#email").keydown(function(){
                    check_email();
                });

                $("#email").keyup(function(){
                    check_email();
                });

                $("#email").focusin(function(){
                    check_email();
                });

                $("#email").focusout(function(){
                    check_email2();
                });


                $("#message").keypress(function(){
                    check_message();
                });

                $("#message").keydown(function(){
                    check_message();
                });

                $("#message").keyup(function(){
                    check_message();
                });

                $("#message").focusin(function(){
                    check_message();
                });

                $("#message").focusout(function(){
                    check_message2();
                });



                function check_name(){
                    var name_length = $("#name").val().length;
                
                   
                        if(name_length<5){
                            if(name_length==0){
                                $("#greencheck").hide();
                       $("#lblName2").hide();
                       $("#lblName").fadeIn();
                   $("#lblName").html("@lang('home.cf_nombre')");
                   
                   document.getElementById("name").style.borderColor = "red";
                  document.getElementById("name").style.backgroundColor = "#FFEBEB";
                  document.getElementById("name").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                   name=true;
                   i_name++;
                   s_name='@lang('home.cf_nombre')<br>';
     
               }else{
                if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#name").val())){
                       
                        $("#greencheck").hide();
                            $("#lblName2").hide();
                        
                        $("#lblName").fadeIn();
                        $("#lblName").html("@lang('home.cf_nombre2')");
                        
                        document.getElementById("name").style.borderColor = "red";
                       document.getElementById("name").style.backgroundColor = "#FFEBEB";
                       document.getElementById("name").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        name=true;
                        i_name++;
                        s_name='@lang('home.cf_nombre2')<br>';
                        }else{
                            $("#greencheck").hide();
                            $("#lblName2").hide();
                            $("#lblName").fadeIn();
                        $("#lblName").html("@lang('home.cf_nombre3')");
                        
                        document.getElementById("name").style.borderColor = "red";
                       document.getElementById("name").style.backgroundColor = "#FFEBEB";
                       document.getElementById("name").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        name=true;
                        i_name++;
                        s_name='@lang('home.cf_nombre3')<br>';
                        }
                        //i_name++;
                      }
                        
                    }
                    
                    else if(name_length>30){
                        $("#greencheck").hide();
                        $("#lblName2").hide();
                        
                        $("#lblName").fadeIn();

                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#name").val())){
                            
                        
                        $("#lblName").html("@lang('home.cf_nombre4r')");//html("@lang('home.mensaje_contactenos')");
                        s_name='@lang('home.cf_nombre4r')<br>';
                        }else{
                            $("#lblName").html("@lang('home.cf_nombre5r')");
                            s_name='@lang('home.cf_nombre5r')<br>';
                        }

                        document.getElementById("name").style.borderColor = "red";
                       document.getElementById("name").style.backgroundColor = "#FFEBEB";
                       document.getElementById("name").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        name=true;
                    i_name++;
                    
                    }

                    else{
                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#name").val())){
                            //if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                            document.getElementById("name").style.borderColor = "green";
                        document.getElementById("name").style.backgroundColor = "#E4FFEF";
                        document.getElementById("name").style.boxShadow = "0 0 1.5px 2.9px #75DA8D";
                        $("#lblName").hide();
                        $("#lblName2").html("@lang('home.cf_nombre6')");
                        $('#lblName2').fadeIn();
                        $('#greencheck').fadeIn();
                        }else{
                            $("#greencheck").hide();
                            $("#lblName2").hide();
                        
                        $("#lblName").fadeIn();
                        $("#lblName").html("@lang('home.cf_nombre7')");
                        document.getElementById("name").style.borderColor = "red";
                       document.getElementById("name").style.backgroundColor = "#FFEBEB";
                       document.getElementById("name").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        name=true;
                        i_name++;
                        s_name='@lang('home.cf_nombre7')<br>';
                        }
                        
                    }
                    //i_name=1;
                }

                function check_name2(){
                    var name_length = $("#name").val().length;
                    
                    
                if(name_length<5){


                    if(name_length==0){
                        $("#greencheck").hide();
                       $("#lblName2").hide();
                       $("#lblName").fadeIn();
                   $("#lblName").html("@lang('home.cf_nombre')");
                   
                   document.getElementById("name").style.borderColor = "red";
                  document.getElementById("name").style.backgroundColor = "#FFEBEB";
                  document.getElementById("name").style.boxShadow = "0 0 0px 0px";
                   name=true;
                   i_name++;
                   s_name='@lang('home.cf_nombre')<br>';
               }else{

                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#name").val())){
                            $("#greencheck").hide();
                            $("#lblName2").hide();
                        
                        $("#lblName").fadeIn();
                        $("#lblName").html("@lang('home.cf_nombre2')");
                        
                        document.getElementById("name").style.borderColor = "red";
                       document.getElementById("name").style.backgroundColor = "#FFEBEB";
                       document.getElementById("name").style.boxShadow = "0 0 0px 0px";
                        name=true;
                        i_name++;
                        s_name='@lang('home.cf_nombre2')<br>';
                        }else{
                            $("#greencheck").hide();
                            $("#lblName2").hide();
                            $("#lblName").fadeIn();
                        $("#lblName").html("@lang('home.cf_nombre3')");
                        
                        document.getElementById("name").style.borderColor = "red";
                       document.getElementById("name").style.backgroundColor = "#FFEBEB";
                       document.getElementById("name").style.boxShadow = "0 0 0px 0px";
                        name=true;
                        i_name++;
                        s_name='@lang('home.cf_nombre3')<br>';
                        }
                    }
                        
                    }
                    
                    
                    
                    else if(name_length>30){
                        $("#greencheck").hide();
                        $("#lblName2").hide();
                        
                        $("#lblName").fadeIn();

                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#name").val())){
                        
                        
                        $("#lblName").html("@lang('home.cf_nombre4r')");
                        s_name='@lang('home.cf_nombre4r')<br>';
                        }else{
                            $("#lblName").html("@lang('home.cf_nombre5r')");
                        s_name='@lang('home.cf_nombre5r')<br>';
                        }

                        document.getElementById("name").style.borderColor = "red";
                       document.getElementById("name").style.backgroundColor = "#FFEBEB";
                       document.getElementById("name").style.boxShadow = "0 0 0px 0px";
                        name=true;
                        i_name++;
                    }

                    else{
                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#name").val())){
                            document.getElementById("name").style.borderColor = "green";
                        document.getElementById("name").style.backgroundColor = "#E4FFEF";
                        document.getElementById("name").style.boxShadow = "0 0 0px 0px";
                        $("#lblName").hide();
                        $("#lblName2").html("@lang('home.cf_nombre6')");
                        $('#lblName2').fadeOut();
                        $('#greencheck').fadeIn();
                        
                        }else{
                            $("#greencheck").hide();
                            $("#lblName2").hide();
                        
                        $("#lblName").fadeIn();
                        $("#lblName").html("@lang('home.cf_nombre7')");
                        document.getElementById("name").style.borderColor = "red";
                       document.getElementById("name").style.backgroundColor = "#FFEBEB";
                       document.getElementById("name").style.boxShadow = "0 0 0px 0px";
                        name=true;
                        i_name++;
                        s_name='@lang('home.cf_nombre7')<br>';
                        }
                        
                    }
                   
                }
                //---------------------------
                
                //----------------------------------------------------

                function check_apellidop(){
                    var apellidop_length = $("#apellidop").val().length;
                    
                   
                        if(apellidop_length<2){
                            if(apellidop_length==0){
                                //$("#greencheck").hide();
                       $("#lblApellidop2").hide();
                       $("#lblApellidop").fadeIn();
                   $("#lblApellidop").html("@lang('home.cf_apellidop')");
                   
                   document.getElementById("apellidop").style.borderColor = "red";
                  document.getElementById("apellidop").style.backgroundColor = "#FFEBEB";
                  document.getElementById("apellidop").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                   apellidop=true;
                   i_apellidop++;
                   s_apellidop='@lang('home.cf_apellidop')<br>';
     
               }else{
                if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#apellidop").val())){
                       // if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                        //$("#greencheck").hide();
                            $("#lblApellidop2").hide();
                        
                        $("#lblApellidop").fadeIn();
                        $("#lblApellidop").html("@lang('home.cf_apellidop2')");//html("@lang('home.mensaje_contactenos')");
                        
                        document.getElementById("apellidop").style.borderColor = "red";
                       document.getElementById("apellidop").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidop").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       apellidop=true;
                        i_apellidop++;
                        s_apellidop='@lang('home.cf_apellidop2')<br>';
                        }else{
                            //$("#greencheck").hide();
                            $("#lblApellidop2").hide();
                            $("#lblApellidop").fadeIn();
                        $("#lblApellidop").html("@lang('home.cf_apellidop3')");
                        
                        document.getElementById("apellidop").style.borderColor = "red";
                       document.getElementById("apellidop").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidop").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       apellidop=true;
                        i_apellidop++;
                        s_apellidop='@lang('home.cf_apellidop3')<br>';
                        }
                        //i_name++;
                      }
                        
                    }

                    //---------------------
                    //---------------------

                    else if(apellidop_length>20){
                        //$("#greencheck").hide();
                        $("#lblApellidop2").hide();
                        
                        $("#lblApellidop").fadeIn();

                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#apellidop").val())){
                            
                        
                        $("#lblApellidop").html("@lang('home.cf_apellidop4')");//html("@lang('home.mensaje_contactenos')");
                        s_apellidop='@lang('home.cf_apellidop4')<br>';
                        }else{
                            $("#lblApellidop").html("@lang('home.cf_apellidop5')");
                            s_apellidop='@lang('home.cf_cf_apellidop5')<br>';
                        }

                        document.getElementById("apellidop").style.borderColor = "red";
                       document.getElementById("apellidop").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidop").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       apellidop=true;
                    i_apellidop++;
                    
                    }

                    else{
                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#apellidop").val())){
                            //if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                            document.getElementById("apellidop").style.borderColor = "green";
                        document.getElementById("apellidop").style.backgroundColor = "#E4FFEF";
                        document.getElementById("apellidop").style.boxShadow = "0 0 1.5px 2.9px #75DA8D";
                        $("#lblApellidop").hide();
                        $("#lblApellidop2").html("@lang('home.cf_apellidop6')");
                        $('#lblApellidop2').fadeIn();
                        //$('#greencheck').fadeIn();
                        }else{
                            //$("#greencheck").hide();
                            $("#lblApellidop2").hide();
                        
                        $("#lblApellidop").fadeIn();
                        $("#lblApellidop").html("@lang('home.cf_apellidop7')");
                        document.getElementById("apellidop").style.borderColor = "red";
                       document.getElementById("apellidop").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidop").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       apellidop=true;
                        i_apellidop++;
                        s_apellidop='@lang('home.cf_apellidop7')<br>';
                        }
                        
                    }
                    //i_name=1;
                }

                function check_apellidop2(){
                    var apellidop_length = $("#apellidop").val().length;
                    
                    
                if(apellidop_length<2){


                    if(apellidop_length==0){
                        //$("#greencheck").hide();
                       $("#lblApellidop2").hide();
                       $("#lblApellidop").fadeIn();
                   $("#lblApellidop").html("@lang('home.cf_apellidop')");
                   
                   document.getElementById("apellidop").style.borderColor = "red";
                  document.getElementById("apellidop").style.backgroundColor = "#FFEBEB";
                  document.getElementById("apellidop").style.boxShadow = "0 0 0px 0px";
                  apellidop=true;
                   i_apellidop++;
                   s_apellidop='@lang('home.cf_apellidop')<br>';
               }else{

                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#apellidop").val())){
                            //$("#greencheck").hide();
                            $("#lblApellidop2").hide();
                        
                        $("#lblApellidop").fadeIn();
                        $("#lblApellidop").html("@lang('home.cf_apellidop2')");
                        
                        document.getElementById("apellidop").style.borderColor = "red";
                       document.getElementById("apellidop").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidop").style.boxShadow = "0 0 0px 0px";
                       apellidop=true;
                        i_apellidop++;
                        s_apellidop='@lang('home.cf_apellidop2')<br>';
                        }else{
                            //$("#greencheck").hide();
                            $("#lblApellidop2").hide();
                            $("#lblApellidop").fadeIn();
                        $("#lblApellidop").html("@lang('home.cf_apellidop3')");
                        
                        document.getElementById("apellidop").style.borderColor = "red";
                       document.getElementById("apellidop").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidop").style.boxShadow = "0 0 0px 0px";
                       apellidop=true;
                        i_apellidop++;
                        s_apellidop='@lang('home.cf_apellidop3')<br>';
                        }
                    }
                        
                    }
                    
                    
                    
                    else if(apellidop_length>20){
                        //$("#greencheck").hide();
                        $("#lblApellidop2").hide();
                        
                        $("#lblApellidop").fadeIn();

                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#apellidop").val())){
                        
                        
                        $("#lblApellidop").html("@lang('home.cf_apellidop4')");
                        s_apellidop='@lang('home.cf_apellidop4')<br>';
                        }else{
                            $("#lbllblApellidop").html("@lang('home.cf_apellidop5')");
                        s_apellidop='@lang('home.cf_apellidop5')<br>';
                        }

                        document.getElementById("apellidop").style.borderColor = "red";
                       document.getElementById("apellidop").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidop").style.boxShadow = "0 0 0px 0px";
                       apellidop=true;
                        i_apellidop++;
                    }

                    else{
                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#apellidop").val())){
                            document.getElementById("apellidop").style.borderColor = "green";
                        document.getElementById("apellidop").style.backgroundColor = "#E4FFEF";
                        document.getElementById("apellidop").style.boxShadow = "0 0 0px 0px";
                        $("#lblApellidop").hide();
                        $("#lblApellidop2").html("@lang('home.cf_apellidop6')");
                        $('#lblApellidop2').fadeOut();
                        //$('#greencheck').fadeIn();
                        
                        }else{
                            //$("#greencheck").hide();
                            $("#lblApellidop2").hide();
                        
                        $("#lblApellidop").fadeIn();
                        $("#lblApellidop").html("@lang('home.cf_apellidop7')");
                        document.getElementById("apellidop").style.borderColor = "red";
                       document.getElementById("apellidop").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidop").style.boxShadow = "0 0 0px 0px";
                       apellidop=true;
                        i_apellidop++;
                        s_apellidop='@lang('home.cf_apellidop7')<br>';
                        }
                        
                    }
                   
                }


                function check_apellidom(){
                    var apellidom_length = $("#apellidom").val().length;
                    
                   
                        if(apellidom_length<2){
                            if(apellidom_length==0){
                                //$("#greencheck").hide();
                       $("#lblApellidom2").hide();
                       $("#lblApellidom").fadeIn();
                   $("#lblApellidom").html("@lang('home.cf_apellidom')");
                   
                   document.getElementById("apellidom").style.borderColor = "red";
                  document.getElementById("apellidom").style.backgroundColor = "#FFEBEB";
                  document.getElementById("apellidom").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                   apellidom=true;
                   i_apellidom++;
                   s_apellidom='@lang('home.cf_apellidom')<br>';
     
               }else{
                if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#apellidom").val())){
                       // if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                        //$("#greencheck").hide();
                            $("#lblApellidom2").hide();
                        
                        $("#lblApellidom").fadeIn();
                        $("#lblApellidom").html("@lang('home.cf_apellidom2')");//html("@lang('home.mensaje_contactenos')");
                        
                        document.getElementById("apellidom").style.borderColor = "red";
                       document.getElementById("apellidom").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidom").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       apellidom=true;
                        i_apellidom++;
                        s_apellidom='@lang('home.cf_apellidom2')<br>';
                        }else{
                            //$("#greencheck").hide();
                            $("#lblApellidom2").hide();
                            $("#lblApellidom").fadeIn();
                        $("#lblApellidom").html("@lang('home.cf_apellidom3')");
                        
                        document.getElementById("apellidom").style.borderColor = "red";
                       document.getElementById("apellidom").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidom").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       apellidom=true;
                        i_apellidom++;
                        s_apellidom='@lang('home.cf_apellidom3')<br>';
                        }
                        //i_name++;
                      }
                        
                    }

                    //---------------------
                    //---------------------

                    else if(apellidom_length>20){
                        //$("#greencheck").hide();
                        $("#lblApellidom2").hide();
                        
                        $("#lblApellidom").fadeIn();

                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#apellidom").val())){
                            
                        
                        $("#lblApellidom").html("@lang('home.cf_apellidom4')");//html("@lang('home.mensaje_contactenos')");
                        s_apellidom='@lang('home.cf_apellidom4')<br>';
                        }else{
                            $("#lblApellidom").html("@lang('home.cf_apellidom5')");
                            s_apellidom='@lang('home.cf_cf_apellidom5')<br>';
                        }

                        document.getElementById("apellidom").style.borderColor = "red";
                       document.getElementById("apellidom").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidom").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       apellidom=true;
                    i_apellidom++;
                    
                    }

                    else{
                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#apellidom").val())){
                            //if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                            document.getElementById("apellidom").style.borderColor = "green";
                        document.getElementById("apellidom").style.backgroundColor = "#E4FFEF";
                        document.getElementById("apellidom").style.boxShadow = "0 0 1.5px 2.9px #75DA8D";
                        $("#lblApellidom").hide();
                        $("#lblApellidom2").html("@lang('home.cf_apellidom6')");
                        $('#lblApellidom2').fadeIn();
                        //$('#greencheck').fadeIn();
                        }else{
                            //$("#greencheck").hide();
                            $("#lblApellidom2").hide();
                        
                        $("#lblApellidom").fadeIn();
                        $("#lblApellidom").html("@lang('home.cf_apellidom7')");
                        document.getElementById("apellidom").style.borderColor = "red";
                       document.getElementById("apellidom").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidom").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       apellidom=true;
                        i_apellidom++;
                        s_apellidom='@lang('home.cf_apellidom7')<br>';
                        }
                        
                    }
                    //i_name=1;
                }

                function check_apellidom2(){
                    var apellidom_length = $("#apellidom").val().length;
                    
                    
                if(apellidom_length<2){


                    if(apellidom_length==0){
                        //$("#greencheck").hide();
                       $("#lblApellidom2").hide();
                       $("#lblApellidom").fadeIn();
                   $("#lblApellidom").html("@lang('home.cf_apellidom')");
                   
                   document.getElementById("apellidom").style.borderColor = "red";
                  document.getElementById("apellidom").style.backgroundColor = "#FFEBEB";
                  document.getElementById("apellidom").style.boxShadow = "0 0 0px 0px";
                  apellidom=true;
                   i_apellidom++;
                   s_apellidom='@lang('home.cf_apellidom')<br>';
               }else{

                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#apellidom").val())){
                            //$("#greencheck").hide();
                            $("#lblApellidom2").hide();
                        
                        $("#lblApellidom").fadeIn();
                        $("#lblApellidom").html("@lang('home.cf_apellidom2')");
                        
                        document.getElementById("apellidom").style.borderColor = "red";
                       document.getElementById("apellidom").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidom").style.boxShadow = "0 0 0px 0px";
                       apellidom=true;
                        i_apellidom++;
                        s_apellidom='@lang('home.cf_apellidom2')<br>';
                        }else{
                            //$("#greencheck").hide();
                            $("#lblApellidom2").hide();
                            $("#lblApellidom").fadeIn();
                        $("#lblApellidom").html("@lang('home.cf_apellidom3')");
                        
                        document.getElementById("apellidom").style.borderColor = "red";
                       document.getElementById("apellidom").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidom").style.boxShadow = "0 0 0px 0px";
                       apellidom=true;
                        i_apellidom++;
                        s_apellidom='@lang('home.cf_apellidom3')<br>';
                        }
                    }
                        
                    }
                    
                    
                    
                    else if(apellidom_length>20){
                        //$("#greencheck").hide();
                        $("#lblApellidom2").hide();
                        
                        $("#lblApellidom").fadeIn();

                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#apellidom").val())){
                        
                        
                        $("#lblApellidom").html("@lang('home.cf_apellidom4')");
                        s_apellidom='@lang('home.cf_apellidom4')<br>';
                        }else{
                            $("#lbllblApellidom").html("@lang('home.cf_apellidom5')");
                        s_apellidom='@lang('home.cf_apellidom5')<br>';
                        }

                        document.getElementById("apellidom").style.borderColor = "red";
                       document.getElementById("apellidom").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidom").style.boxShadow = "0 0 0px 0px";
                       apellidom=true;
                        i_apellidom++;
                    }

                    else{
                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#apellidom").val())){
                            document.getElementById("apellidom").style.borderColor = "green";
                        document.getElementById("apellidom").style.backgroundColor = "#E4FFEF";
                        document.getElementById("apellidom").style.boxShadow = "0 0 0px 0px";
                        $("#lblApellidom").hide();
                        $("#lblApellidom2").html("@lang('home.cf_apellidom6')");
                        $('#lblApellidom2').fadeOut();
                        //$('#greencheck').fadeIn();
                        
                        }else{
                            //$("#greencheck").hide();
                            $("#lblApellidom2").hide();
                        
                        $("#lblApellidom").fadeIn();
                        $("#lblApellidom").html("@lang('home.cf_apellidom7')");
                        document.getElementById("apellidom").style.borderColor = "red";
                       document.getElementById("apellidom").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidom").style.boxShadow = "0 0 0px 0px";
                       apellidom=true;
                        i_apellidom++;
                        s_apellidom='@lang('home.cf_apellidom7')<br>';
                        }
                        
                    }
                   
                }


 //------------distrito--------------
 function check_distrito(){
                    var d= document.getElementById("distrito");

                    if(d.selectedIndex==0){
                        $("#lblDistrito2").hide();
                        $("#lblDistrito").fadeIn();
                   $("#lblDistrito").html("@lang('home.cf_distrito')");
                   document.getElementById("distrito").style.borderColor = "red";
                       document.getElementById("distrito").style.backgroundColor = "#FFEBEB";
                       document.getElementById("distrito").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        distrito=true;
                        i_distrito++;
                        s_distrito='@lang('home.cf_distrito')<br>';
                        }else{
                            
                   document.getElementById("distrito").style.borderColor = "green";
                        document.getElementById("distrito").style.backgroundColor = "#E4FFEF";
                        document.getElementById("distrito").style.boxShadow = "0 0 1.5px 2.9px #75DA8D";
                        $("#lblDistrito").hide();
                        $("#lblDistrito2").html("@lang('home.cf_distrito2')");
                        $('#lblDistrito2').fadeIn();
                        
                    }
                 
                }

                function check_distrito2(){
                    var d= document.getElementById("distrito");

                    if(d.selectedIndex==0){
                        $("#lblDistrito2").hide();
                        $("#lblDistrito").fadeIn();
                   $("#lblDistrito").html("@lang('home.cf_distrito')");
                   document.getElementById("distrito").style.borderColor = "red";
                       document.getElementById("distrito").style.backgroundColor = "#FFEBEB";
                       document.getElementById("distrito").style.boxShadow = "0 0 0px 0px";
                        distrito=true;
                        i_distrito++;
                        s_distrito='@lang('home.cf_distrito')<br>';
                        }else{
                            
                   document.getElementById("distrito").style.borderColor = "green";
                        document.getElementById("distrito").style.backgroundColor = "#E4FFEF";
                        document.getElementById("distrito").style.boxShadow = "0 0 0px 0px";
                        $("#lblDistrito").hide();
                        $("#lblDistrito2").html("@lang('home.cf_distrito2')");
                        $('#lblDistrito2').fadeOut();
                        
                    }
                 
                }

//-------------------------------------- 
function check_telefono(){
                    var telefono_length = $("#telefono").val().length;
                    
                   
                        if(telefono_length<5){
                            if(telefono_length==0){
                                //$("#greencheck").hide();
                       $("#lblTelefono2").hide();
                       $("#lblTelefono").fadeIn();
                   $("#lblTelefono").html("@lang('home.cf_telefono')");
                   
                   document.getElementById("telefono").style.borderColor = "red";
                  document.getElementById("telefono").style.backgroundColor = "#FFEBEB";
                  document.getElementById("telefono").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                  telefono=true;
                   i_telefono++;
                   s_telefono='@lang('home.cf_telefono')<br>';
     
               }else{//^[0-9-+s()]*$
                if(/^[0-9]+$/.test($("#telefono").val())){
                       // if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                        //$("#greencheck").hide();
                            $("#lblTelefono2").hide();
                        
                        $("#lblTelefono").fadeIn();
                        $("#lblTelefono").html("@lang('home.cf_telefono2')");//html("@lang('home.mensaje_contactenos')");
                        
                        document.getElementById("telefono").style.borderColor = "red";
                       document.getElementById("telefono").style.backgroundColor = "#FFEBEB";
                       document.getElementById("telefono").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       telefono=true;
                        i_telefono++;
                        s_telefono='@lang('home.cf_telefono2')<br>';
                        }else{
                            //$("#greencheck").hide();
                            $("#lblTelefono2").hide();
                            $("#lblTelefono").fadeIn();
                        $("#lblTelefono").html("@lang('home.cf_telefono3')");
                        
                        document.getElementById("telefono").style.borderColor = "red";
                       document.getElementById("telefono").style.backgroundColor = "#FFEBEB";
                       document.getElementById("telefono").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       telefono=true;
                        i_telefono++;
                        s_telefono='@lang('home.cf_telefono3')<br>';
                        }
                        //i_name++;
                      }
                        
                    }
                    else if(telefono_length>12){
                        //$("#greencheck").hide();
                        $("#lblTelefono2").hide();
                        
                        $("#lblTelefono").fadeIn();

                        if(/^[0-9]+$/.test($("#telefono").val())){
                            
                        
                        $("#lblTelefono").html("@lang('home.cf_telefono4')");//html("@lang('home.mensaje_contactenos')");
                        s_telefono='@lang('home.cf_telefono4')<br>';
                        }else{
                            $("#lblTelefono").html("@lang('home.cf_telefono5')");
                            s_telefono='@lang('home.cf_telefono5')<br>';
                        }

                        document.getElementById("telefono").style.borderColor = "red";
                       document.getElementById("telefono").style.backgroundColor = "#FFEBEB";
                       document.getElementById("telefono").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       telefono=true;
                    i_telefono++;
                    
                    }

                    else{
                        if(/^[0-9]+$/.test($("#telefono").val())){
                            //if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                            document.getElementById("telefono").style.borderColor = "green";
                        document.getElementById("telefono").style.backgroundColor = "#E4FFEF";
                        document.getElementById("telefono").style.boxShadow = "0 0 1.5px 2.9px #75DA8D";
                        $("#lblTelefono").hide();
                        $("#lblTelefono2").html("@lang('home.cf_telefono6')");
                        $('#lblTelefono2').fadeIn();
                        //$('#greencheck').fadeIn();
                        }else{
                            //$("#greencheck").hide();
                            $("#lblTelefono2").hide();
                        
                        $("#lblTelefono").fadeIn();
                        $("#lblTelefono").html("@lang('home.cf_telefono7')");
                        document.getElementById("telefono").style.borderColor = "red";
                       document.getElementById("telefono").style.backgroundColor = "#FFEBEB";
                       document.getElementById("telefono").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       telefono=true;
                        i_telefono++;
                        s_telefono='@lang('home.cf_telefono7')<br>';
                        }
                        
                    }
                    //i_name=1;
                }

                function check_telefono2(){
                    var telefono_length = $("#telefono").val().length;
                    
                   
                        if(telefono_length<5){
                            if(telefono_length==0){
                                
                       $("#lblTelefono2").hide();
                       $("#lblTelefono").fadeIn();
                   $("#lblTelefono").html("@lang('home.cf_telefono')");
                   
                   document.getElementById("telefono").style.borderColor = "red";
                  document.getElementById("telefono").style.backgroundColor = "#FFEBEB";
                  document.getElementById("telefono").style.boxShadow = "0 0 0px 0px";
                  telefono=true;
                   i_telefono++;
                   s_telefono='@lang('home.cf_telefono')<br>';
     
               }else{//^[0-9-+s()]*$
                if(/^[0-9]+$/.test($("#telefono").val())){
                       
                            $("#lblTelefono2").hide();
                        
                        $("#lblTelefono").fadeIn();
                        $("#lblTelefono").html("@lang('home.cf_telefono2')");//html("@lang('home.mensaje_contactenos')");
                        
                        document.getElementById("telefono").style.borderColor = "red";
                       document.getElementById("telefono").style.backgroundColor = "#FFEBEB";
                       document.getElementById("telefono").style.boxShadow = "0 0 0px 0px";
                       telefono=true;
                        i_telefono++;
                        s_telefono='@lang('home.cf_telefono2')<br>';
                        }else{
                            //$("#greencheck").hide();
                            $("#lblTelefono2").hide();
                            $("#lblTelefono").fadeIn();
                        $("#lblTelefono").html("@lang('home.cf_telefono3')");
                        
                        document.getElementById("telefono").style.borderColor = "red";
                       document.getElementById("telefono").style.backgroundColor = "#FFEBEB";
                       document.getElementById("telefono").style.boxShadow = "0 0 0px 0px";
                       telefono=true;
                        i_telefono++;
                        s_telefono='@lang('home.cf_telefono3')<br>';
                        }
                        //i_name++;
                      }
                        
                    }
                    else if(telefono_length>12){
                        //$("#greencheck").hide();
                        $("#lblTelefono2").hide();
                        
                        $("#lblTelefono").fadeIn();

                        if(/^[0-9]+$/.test($("#telefono").val())){
                            
                        
                        $("#lblTelefono").html("@lang('home.cf_telefono4')");//html("@lang('home.mensaje_contactenos')");
                        s_telefono='@lang('home.cf_telefono4')<br>';
                        }else{
                            $("#lblTelefono").html("@lang('home.cf_telefono5')");
                            s_telefono='@lang('home.cf_telefono5')<br>';
                        }

                        document.getElementById("telefono").style.borderColor = "red";
                       document.getElementById("telefono").style.backgroundColor = "#FFEBEB";
                       document.getElementById("telefono").style.boxShadow = "0 0 0px 0px";
                       telefono=true;
                    i_telefono++;
                    
                    }

                    else{
                        if(/^[0-9]+$/.test($("#telefono").val())){
                            //if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                            document.getElementById("telefono").style.borderColor = "green";
                        document.getElementById("telefono").style.backgroundColor = "#E4FFEF";
                        document.getElementById("telefono").style.boxShadow = "0 0 0px 0px";
                        $("#lblTelefono").hide();
                        $("#lblTelefono2").html("@lang('home.cf_telefono6')");
                        $('#lblTelefono2').fadeOut();
                        //$('#greencheck').fadeIn();
                        }else{
                            //$("#greencheck").hide();
                            $("#lblTelefono2").hide();
                        
                        $("#lblTelefono").fadeIn();
                        $("#lblTelefono").html("@lang('home.cf_telefono7')");
                        document.getElementById("telefono").style.borderColor = "red";
                       document.getElementById("telefono").style.backgroundColor = "#FFEBEB";
                       document.getElementById("telefono").style.boxShadow = "0 0 0px 0px";
                       telefono=true;
                        i_telefono++;
                        s_telefono='@lang('home.cf_telefono7')<br>';
                        }
                        
                    }
                    //i_name=1;
                }


//----------------------------------------------------------------------------

function check_t_documento(){
                    var d= document.getElementById("t_documento");

                    if(d.selectedIndex==0){
                        $("#lblT_documento2").hide();
                        $("#lblT_documento").fadeIn();
                   $("#lblT_documento").html("@lang('home.cf_t_documento')");
                   document.getElementById("t_documento").style.borderColor = "red";
                       document.getElementById("t_documento").style.backgroundColor = "#FFEBEB";
                       document.getElementById("t_documento").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       t_documento=true;
                        i_t_documento++;
                        s_t_documento='@lang('home.cf_t_documento')<br>';
                        }else{
                            
                   document.getElementById("t_documento").style.borderColor = "green";
                        document.getElementById("t_documento").style.backgroundColor = "#E4FFEF";
                        document.getElementById("t_documento").style.boxShadow = "0 0 1.5px 2.9px #75DA8D";
                        $("#lblT_documento").hide();
                        $("#lblT_documento2").html("@lang('home.cf_t_documento2')");
                        $('#lblT_documento2').fadeIn();
                        
                    }
                 
                }

                function check_t_documento2(){
                    var d= document.getElementById("t_documento");

                    if(d.selectedIndex==0){
                        $("#lblT_documento2").hide();
                        $("#lblT_documento").fadeIn();
                   $("#lblT_documento").html("@lang('home.cf_t_documento')");
                   document.getElementById("t_documento").style.borderColor = "red";
                       document.getElementById("t_documento").style.backgroundColor = "#FFEBEB";
                       document.getElementById("t_documento").style.boxShadow = "0 0 0px 0px";
                       t_documento=true;
                        i_t_documento++;
                        s_t_documento='@lang('home.cf_t_documento')<br>';
                        }else{
                            
                   document.getElementById("t_documento").style.borderColor = "green";
                        document.getElementById("t_documento").style.backgroundColor = "#E4FFEF";
                        document.getElementById("t_documento").style.boxShadow = "0 0 0px 0px";
                        $("#lblT_documento").hide();
                        $("#lblT_documento2").html("@lang('home.cf_t_documento2')");
                        $('#lblT_documento2').fadeOut();
                        
                    }
                 
                }

//--------------------------------------------------------------------------------------------------------------------------------------

function check_n_documento(){
                    var n_documento_length = $("#n_documento").val().length;
                    
                   
                        if(n_documento_length<5){
                            if(n_documento_length==0){
                                //$("#greencheck").hide();
                       $("#lblN_documento2").hide();
                       $("#lblN_documento").fadeIn();
                   $("#lblN_documento").html("@lang('home.cf_n_documento')");
                   
                   document.getElementById("n_documento").style.borderColor = "red";
                  document.getElementById("n_documento").style.backgroundColor = "#FFEBEB";
                  document.getElementById("n_documento").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                  n_documento=true;
                   i_n_documento++;
                   s_n_documento='@lang('home.cf_n_documento')<br>';
     
               }else{//^[0-9-+s()]*$
                if(/^[0-9]+$/.test($("#n_documento").val())){
                       // if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                        //$("#greencheck").hide();
                            $("#lblN_documento2").hide();
                        
                        $("#lblN_documento").fadeIn();
                        $("#lblN_documento").html("@lang('home.cf_n_documento2')");//html("@lang('home.mensaje_contactenos')");
                        
                        document.getElementById("n_documento").style.borderColor = "red";
                       document.getElementById("n_documento").style.backgroundColor = "#FFEBEB";
                       document.getElementById("n_documento").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       n_documento=true;
                        i_n_documento++;
                        s_n_documento='@lang('home.cf_n_documento2')<br>';
                        }else{
                            //$("#greencheck").hide();
                            $("#lblN_documento2").hide();
                            $("#lblN_documento").fadeIn();
                        $("#lblN_documento").html("@lang('home.cf_n_documento3')");
                        
                        document.getElementById("n_documento").style.borderColor = "red";
                       document.getElementById("n_documento").style.backgroundColor = "#FFEBEB";
                       document.getElementById("n_documento").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       n_documento=true;
                        i_n_documento++;
                        s_n_documento='@lang('home.cf_n_documento3')<br>';
                        }
                        //i_name++;
                      }
                        
                    }
                    else if(n_documento_length>12){
                        //$("#greencheck").hide();
                        $("#lblN_documento2").hide();
                        
                        $("#lblN_documento").fadeIn();

                        if(/^[0-9]+$/.test($("#n_documento").val())){
                            
                        
                        $("#lblN_documento").html("@lang('home.cf_n_documento4')");//html("@lang('home.mensaje_contactenos')");
                        s_n_documento='@lang('home.cf_n_documento4')<br>';
                        }else{
                            $("#lblN_documento").html("@lang('home.cf_n_documento5')");
                            s_n_documento='@lang('home.cf_n_documento5')<br>';
                        }

                        document.getElementById("n_documento").style.borderColor = "red";
                       document.getElementById("n_documento").style.backgroundColor = "#FFEBEB";
                       document.getElementById("n_documento").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       n_documento=true;
                    i_n_documento++;
                    
                    }

                    else{
                        if(/^[0-9]+$/.test($("#n_documento").val())){
                            //if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                            document.getElementById("n_documento").style.borderColor = "green";
                        document.getElementById("n_documento").style.backgroundColor = "#E4FFEF";
                        document.getElementById("n_documento").style.boxShadow = "0 0 1.5px 2.9px #75DA8D";
                        $("#lblN_documento").hide();
                        $("#lblN_documento2").html("@lang('home.cf_n_documento6')");
                        $('#lblN_documento2').fadeIn();
                        //$('#greencheck').fadeIn();
                        }else{
                            //$("#greencheck").hide();
                            $("#lblN_documento2").hide();
                        
                        $("#lblN_documento").fadeIn();
                        $("#lblN_documento").html("@lang('home.cf_n_documento7')");
                        document.getElementById("n_documento").style.borderColor = "red";
                       document.getElementById("n_documento").style.backgroundColor = "#FFEBEB";
                       document.getElementById("n_documento").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       n_documento=true;
                        i_n_documento++;
                        s_n_documento='@lang('home.cf_n_documento7')<br>';
                        }
                        
                    }
                    //i_name=1;
                }

                function check_n_documento2(){
                    var n_documento_length = $("#n_documento").val().length;
                    
                   
                        if(n_documento_length<5){
                            if(n_documento_length==0){
                                //$("#greencheck").hide();
                       $("#lblN_documento2").hide();
                       $("#lblN_documento").fadeIn();
                   $("#lblN_documento").html("@lang('home.cf_n_documento')");
                   
                   document.getElementById("n_documento").style.borderColor = "red";
                  document.getElementById("n_documento").style.backgroundColor = "#FFEBEB";
                  document.getElementById("n_documento").style.boxShadow = "0 0 0px 0px";
                  n_documento=true;
                   i_n_documento++;
                   s_n_documento='@lang('home.cf_n_documento')<br>';
     
               }else{//^[0-9-+s()]*$
                if(/^[0-9]+$/.test($("#n_documento").val())){
                       // if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                        //$("#greencheck").hide();
                            $("#lblN_documento2").hide();
                        
                        $("#lblN_documento").fadeIn();
                        $("#lblN_documento").html("@lang('home.cf_n_documento2')");//html("@lang('home.mensaje_contactenos')");
                        
                        document.getElementById("n_documento").style.borderColor = "red";
                       document.getElementById("n_documento").style.backgroundColor = "#FFEBEB";
                       document.getElementById("n_documento").style.boxShadow = "0 0 0px 0px";
                       n_documento=true;
                        i_n_documento++;
                        s_n_documento='@lang('home.cf_n_documento2')<br>';
                        }else{
                            //$("#greencheck").hide();
                            $("#lblN_documento2").hide();
                            $("#lblN_documento").fadeIn();
                        $("#lblN_documento").html("@lang('home.cf_n_documento3')");
                        
                        document.getElementById("n_documento").style.borderColor = "red";
                       document.getElementById("n_documento").style.backgroundColor = "#FFEBEB";
                       document.getElementById("n_documento").style.boxShadow = "0 0 0px 0px";
                       n_documento=true;
                        i_n_documento++;
                        s_n_documento='@lang('home.cf_n_documento3')<br>';
                        }
                        //i_name++;
                      }
                        
                    }
                    else if(n_documento_length>12){
                        //$("#greencheck").hide();
                        $("#lblN_documento2").hide();
                        
                        $("#lblN_documento").fadeIn();

                        if(/^[0-9]+$/.test($("#n_documento").val())){
                            
                        
                        $("#lblN_documento").html("@lang('home.cf_n_documento4')");//html("@lang('home.mensaje_contactenos')");
                        s_n_documento='@lang('home.cf_n_documento4')<br>';
                        }else{
                            $("#lblN_documento").html("@lang('home.cf_n_documento5')");
                            s_n_documento='@lang('home.cf_n_documento5')<br>';
                        }

                        document.getElementById("n_documento").style.borderColor = "red";
                       document.getElementById("n_documento").style.backgroundColor = "#FFEBEB";
                       document.getElementById("n_documento").style.boxShadow = "0 0 0px 0px";
                       n_documento=true;
                    i_n_documento++;
                    
                    }

                    else{
                        if(/^[0-9]+$/.test($("#n_documento").val())){
                            //if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                            document.getElementById("n_documento").style.borderColor = "green";
                        document.getElementById("n_documento").style.backgroundColor = "#E4FFEF";
                        document.getElementById("n_documento").style.boxShadow = "0 0 0px 0px";
                        $("#lblN_documento").hide();
                        $("#lblN_documento2").html("@lang('home.cf_n_documento6')");
                        $('#lblN_documento2').fadeOut();
                        //$('#greencheck').fadeIn();
                        }else{
                            //$("#greencheck").hide();
                            $("#lblN_documento2").hide();
                        
                        $("#lblN_documento").fadeIn();
                        $("#lblN_documento").html("@lang('home.cf_n_documento7')");
                        document.getElementById("n_documento").style.borderColor = "red";
                       document.getElementById("n_documento").style.backgroundColor = "#FFEBEB";
                       document.getElementById("n_documento").style.boxShadow = "0 0 0px 0px";
                       n_documento=true;
                        i_n_documento++;
                        s_n_documento='@lang('home.cf_n_documento7')<br>';
                        }
                        
                    }
                    //i_name=1;
                }
//-----------------------------------------------------------------

function check_email(){

var pattern=new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
var email_length = $("#email").val().length;

if(email_length<=320){//320
if(email_length==0){
            //$("#greencheck").hide();
   $("#lblEmail2").hide();
   $("#lblEmail").fadeIn();
$("#lblEmail").html("@lang('home.cf_email')");
document.getElementById("email").style.borderColor = "red";
document.getElementById("email").style.backgroundColor = "#FFEBEB";
document.getElementById("email").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
email=true;
i_email++;
s_email='@lang('home.cf_email')<br>';
}else{
if(pattern.test($("#email").val())){
//$("#greencheck").hide();
document.getElementById("email").style.borderColor = "green";
    document.getElementById("email").style.backgroundColor = "#E4FFEF";
    document.getElementById("email").style.boxShadow = "0 0 1.5px 2.9px #75DA8D";
        $("#lblEmail").hide();
        $("#lblEmail2").html("@lang('home.cf_email2')");
        $('#lblEmail2').fadeIn();
        //$('#greencheck').fadeIn();
}else{
$("#lblEmail2").hide();
    $("#lblEmail").fadeIn();
    $("#lblEmail").html("@lang('home.cf_email3')");//html("@lang('home.mensaje_contactenos')");
    document.getElementById("email").style.borderColor = "red";
   document.getElementById("email").style.backgroundColor = "#FFEBEB";
   document.getElementById("email").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
    email=true;
    i_email++;
    s_email='@lang('home.cf_email3')<br>';
}
}
}else{
    $("#lblEmail2").hide();
    $("#lblEmail").fadeIn();
    if(pattern.test($("#email").val())){ 
    //----------------------------------------------------------
        $("#lblEmail").html("@lang('home.cf_email4')");//html("@lang('home.mensaje_contactenos')");
        s_email='@lang('home.cf_email4')<br>';
        }else{
            $("#lblEmail").html("@lang('home.cf_email5')");
        s_email='@lang('home.cf_email5')<br>';
        }

        document.getElementById("email").style.borderColor = "red";
       document.getElementById("email").style.backgroundColor = "#FFEBEB";
       document.getElementById("email").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
        email=true;
        i_email++;
   
}



}

function check_email2(){

var pattern=new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
var email_length = $("#email").val().length;

if(email_length<=320){
if(email_length==0){
//$("#greencheck").hide();
$("#lblEmail2").hide();
$("#lblEmail").fadeIn();
$("#lblEmail").html("@lang('home.cf_email')");

document.getElementById("email").style.borderColor = "red";
document.getElementById("email").style.backgroundColor = "#FFEBEB";
document.getElementById("email").style.boxShadow = "0 0 0px 0px";
email=true;
i_email++;
s_email='@lang('home.cf_email')<br>';

}else{
if(pattern.test($("#email").val())){
//$("#greencheck").hide();
document.getElementById("email").style.borderColor = "green";
document.getElementById("email").style.backgroundColor = "#E4FFEF";
document.getElementById("email").style.boxShadow = "0 0 0px 0px";

$("#lblEmail").hide();
$("#lblEmail2").html("@lang('home.cf_email2')");
$('#lblEmail2').fadeOut();
//$('#greencheck').fadeIn();
}else{ 
$("#lblEmail2").hide();
$("#lblEmail").fadeIn();
$("#lblEmail").html("@lang('home.cf_email3')");//html("@lang('home.mensaje_contactenos')");

document.getElementById("email").style.borderColor = "red";
document.getElementById("email").style.backgroundColor = "#FFEBEB";
document.getElementById("email").style.boxShadow = "0 0 0px 0px";
email=true;
i_email++;
s_email='@lang('home.cf_email3')<br>';
}



}
}else{
$("#lblEmail2").hide();

$("#lblEmail").fadeIn();

if(pattern.test($("#email").val())){

//----------------------------------------------------------
$("#lblEmail").html("@lang('home.cf_email4')");//html("@lang('home.mensaje_contactenos')");
s_email='@lang('home.cf_email4')<br>';
}else{
$("#lblEmail").html("@lang('home.cf_email5')");
s_email='@lang('home.cf_email5')<br>';
}

document.getElementById("email").style.borderColor = "red";
document.getElementById("email").style.backgroundColor = "#FFEBEB";
document.getElementById("email").style.boxShadow = "0 0 0px 0px";
email=true;
i_email++;
}



}

 
//------------------------------------------------------
function check_message(){
                    var message_length = $("#message").val().length;
                        if(message_length<5){
                            if(message_length==0){
                                //$("#greencheck").hide();
                       $("#lblMessage2").hide();
                       $("#lblMessage").fadeIn();
                   $("#lblMessage").html("@lang('home.cf_message')");
                   
                   document.getElementById("message").style.borderColor = "red";
                  document.getElementById("message").style.backgroundColor = "#FFEBEB";
                  document.getElementById("message").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                   message=true;
                   i_message++;
                   s_message='@lang('home.cf_message')<br>';
               }else{
                            $("#lblMessage2").hide();
                        
                        $("#lblMessage").fadeIn();
                        $("#lblMessage").html("@lang('home.cf_message2')");//html("@lang('home.mensaje_contactenos')");
                        
                        document.getElementById("message").style.borderColor = "red";
                       document.getElementById("message").style.backgroundColor = "#FFEBEB";
                       document.getElementById("message").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        message=true;
                        i_message++;
                        s_message='@lang('home.cf_message2')<br>';
                      }   
                    }
                    else if(message_length>320){
                        //$("#greencheck").hide();
                        $("#lblMessage2").hide();
                        
                        $("#lblMessage").fadeIn();

                        $("#lblMessage").html("@lang('home.cf_message3')");//html("@lang('home.mensaje_contactenos')");
                        
                        document.getElementById("message").style.borderColor = "red";
                       document.getElementById("message").style.backgroundColor = "#FFEBEB";
                       document.getElementById("message").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        message=true;
                        i_message++;
                        s_message='@lang('home.cf_message3')<br>';
                    }
                    else{
                         document.getElementById("message").style.borderColor = "green";
                        document.getElementById("message").style.backgroundColor = "#E4FFEF";
                        document.getElementById("message").style.boxShadow = "0 0 1.5px 2.9px #75DA8D";
                        $("#lblMessage").hide();
                        $("#lblMessage2").html("@lang('home.cf_message4')");
                        $('#lblMessage2').fadeIn();
                    }
                }
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                function check_message2(){
                    var message_length = $("#message").val().length;
                    
                    
                if(message_length<5){


                    if(message_length==0){
                        //$("#greencheck").hide();
                       $("#lblMessage2").hide();
                       $("#lblMessage").fadeIn();
                   $("#lblMessage").html("@lang('home.cf_message')");
                   
                   document.getElementById("message").style.borderColor = "red";
                  document.getElementById("message").style.backgroundColor = "#FFEBEB";
                  document.getElementById("message").style.boxShadow = "0 0 0px 0px";
                   message=true;
                   i_message++;
                   s_message='@lang('home.cf_message')<br>';
               }else{

                        
                            //$("#greencheck").hide();
                            $("#lblMessage2").hide();
                        
                        $("#lblMessage").fadeIn();
                        $("#lblMessage").html("@lang('home.cf_message2')");
                        
                        document.getElementById("message").style.borderColor = "red";
                       document.getElementById("message").style.backgroundColor = "#FFEBEB";
                       document.getElementById("message").style.boxShadow = "0 0 0px 0px";
                        message=true;
                        i_message++;
                        s_message='@lang('home.cf_message2')<br>';
                    }
                        
                    }
                    
                    else if(message_length>320){
                        //$("#greencheck").hide();
                        $("#lblMessage2").hide();
                        
                        $("#lblMessage").fadeIn();

                        
                        
                        
                        $("#lblMessage").html("@lang('home.cf_message3')");
                        

                        document.getElementById("message").style.borderColor = "red";
                       document.getElementById("message").style.backgroundColor = "#FFEBEB";
                       document.getElementById("message").style.boxShadow = "0 0 0px 0px";
                        message=true;
                        i_message++;
                        s_message='@lang('home.cf_message3')<br>';
                    }

                    else{
                       
                            document.getElementById("message").style.borderColor = "green";
                        document.getElementById("message").style.backgroundColor = "#E4FFEF";
                        document.getElementById("message").style.boxShadow = "0 0 0px 0px";
                        $("#lblMessage").hide();
                        $("#lblMessage2").html("@lang('home.cf_message4')");
                        $('#lblMessage2').fadeOut();
                        //$('#greencheck').fadeIn();
                        
                       
                        
                    }
                   
                }


       

  







        });
</script>




<script>
        $(function(){

            $("#lblName").hide();
            $("#lblApellidop").hide();
            $("#lblApellidom").hide();
            $("#lblDistrito").hide();
            $("#lblTelefono").hide();
            $("#lblT_documento").hide();
            $("#lblN_documento").hide();
            $("#lblEmail").hide();
            $("#lblD_reclamacion").hide();
            $("#lblMessage").hide();

            var name=false;
            var apellidop=false;
            var apellidom=false;
            var distrito=false;
            var telefono=false;
            var t_documento=false;
            var n_documento=false;
            var email=false;
            var t_reclamo=false;
            var message=false;

            var i_name=0;
            var i_apellidop=0;
            var i_apellidom=0;
            var i_distrito=0;
            var i_telefono=0;
            var i_t_documento=0;
            var i_n_documento=0;
            var i_email=0;
            var i_t_reclamo=0;
            var i_message=0;

            var s_name='';
            var s_apellidop='';
            var s_apellidom='';
            var s_distrito='';
            var s_telefono='';
            var s_t_documento='';
            var s_n_documento='';
            var s_email='';
            var s_t_reclamo='';
            var s_message='';

            
           


	
            //------------------------------------------

            $("#name").keypress(function(){
                console.log('okkkka');
                    check_name();
                });

                $("#name").keydown(function(){
                    check_name();
                });

                $("#name").keyup(function(){
                    check_name();
                });

                $("#name").focusin(function(){
                    check_name();
                });

                $("#name").focusout(function(){
                    check_name2();
                });
                //------------------------
                $("#apellidop").keypress(function(){
                    check_apellidop();
                });

                $("#apellidop").keydown(function(){
                    check_apellidop();
                });

                $("#apellidop").keyup(function(){
                    check_apellidop();
                });

                $("#apellidop").focusin(function(){
                    check_apellidop();
                });

                $("#apellidop").focusout(function(){
                    check_apellidop2();
                });

                //-----------------------------
                $("#apellidom").keypress(function(){
                    check_apellidom();
                });

                $("#apellidom").keydown(function(){
                    check_apellidom();
                });

                $("#apellidom").keyup(function(){
                    check_apellidom();
                });

                $("#apellidom").focusin(function(){
                    check_apellidom();
                });

                $("#apellidom").focusout(function(){
                    check_apellidom2();
                });

                //------------------------------------
                $("#distrito").click(function(){
                    check_distrito();
                });

              
                $("#distrito").focusout(function(){
                    check_distrito2();
                });
                //-----------------------------------
                
                $("#telefono").keypress(function(){
                    check_telefono();
                });

                $("#telefono").keydown(function(){
                    check_telefono();
                });

                $("#telefono").keyup(function(){
                    check_telefono();
                });

                $("#telefono").focusin(function(){
                    check_telefono();
                });

                $("#telefono").focusout(function(){
                    check_telefono2();
                });

                //------------------------------------
                $("#t_documento").click(function(){
                    check_t_documento();
                });
               
                $("#t_documento").focusout(function(){
                    check_t_documento2();
                });

                //-----------------------------------

                $("#n_documento").keypress(function(){
                    check_n_documento();
                });

                $("#n_documento").keydown(function(){
                    check_n_documento();
                });

                $("#n_documento").keyup(function(){
                    check_n_documento();
                });

                $("#n_documento").focusin(function(){
                    check_n_documento();
                });

                $("#n_documento").focusout(function(){
                    check_n_documento2();
                });

                //-----------------------------------

                $("#email").keypress(function(){
                    check_email();
                });

                $("#email").keydown(function(){
                    check_email();
                });

                $("#email").keyup(function(){
                    check_email();
                });

                $("#email").focusin(function(){
                    check_email();
                });

                $("#email").focusout(function(){
                    check_email2();
                });


                $("#message").keypress(function(){
                    check_message();
                });

                $("#message").keydown(function(){
                    check_message();
                });

                $("#message").keyup(function(){
                    check_message();
                });

                $("#message").focusin(function(){
                    check_message();
                });

                $("#message").focusout(function(){
                    check_message2();
                });



                function check_name(){
                    var name_length = $("#name").val().length;
                
                   
                        if(name_length<5){
                            if(name_length==0){
                                $("#greencheck").hide();
                       $("#lblName2").hide();
                       $("#lblName").fadeIn();
                   $("#lblName").html("@lang('home.cf_nombre')");
                   
                   document.getElementById("name").style.borderColor = "red";
                  document.getElementById("name").style.backgroundColor = "#FFEBEB";
                  document.getElementById("name").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                   name=true;
                   i_name++;
                   s_name='@lang('home.cf_nombre')<br>';
     
               }else{
                if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#name").val())){
                       
                        $("#greencheck").hide();
                            $("#lblName2").hide();
                        
                        $("#lblName").fadeIn();
                        $("#lblName").html("@lang('home.cf_nombre2')");
                        
                        document.getElementById("name").style.borderColor = "red";
                       document.getElementById("name").style.backgroundColor = "#FFEBEB";
                       document.getElementById("name").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        name=true;
                        i_name++;
                        s_name='@lang('home.cf_nombre2')<br>';
                        }else{
                            $("#greencheck").hide();
                            $("#lblName2").hide();
                            $("#lblName").fadeIn();
                        $("#lblName").html("@lang('home.cf_nombre3')");
                        
                        document.getElementById("name").style.borderColor = "red";
                       document.getElementById("name").style.backgroundColor = "#FFEBEB";
                       document.getElementById("name").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        name=true;
                        i_name++;
                        s_name='@lang('home.cf_nombre3')<br>';
                        }
                        //i_name++;
                      }
                        
                    }
                    
                    else if(name_length>30){
                        $("#greencheck").hide();
                        $("#lblName2").hide();
                        
                        $("#lblName").fadeIn();

                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#name").val())){
                            
                        
                        $("#lblName").html("@lang('home.cf_nombre4r')");//html("@lang('home.mensaje_contactenos')");
                        s_name='@lang('home.cf_nombre4r')<br>';
                        }else{
                            $("#lblName").html("@lang('home.cf_nombre5r')");
                            s_name='@lang('home.cf_nombre5r')<br>';
                        }

                        document.getElementById("name").style.borderColor = "red";
                       document.getElementById("name").style.backgroundColor = "#FFEBEB";
                       document.getElementById("name").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        name=true;
                    i_name++;
                    
                    }

                    else{
                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#name").val())){
                            //if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                            document.getElementById("name").style.borderColor = "green";
                        document.getElementById("name").style.backgroundColor = "#E4FFEF";
                        document.getElementById("name").style.boxShadow = "0 0 1.5px 2.9px #75DA8D";
                        $("#lblName").hide();
                        $("#lblName2").html("@lang('home.cf_nombre6')");
                        $('#lblName2').fadeIn();
                        $('#greencheck').fadeIn();
                        }else{
                            $("#greencheck").hide();
                            $("#lblName2").hide();
                        
                        $("#lblName").fadeIn();
                        $("#lblName").html("@lang('home.cf_nombre7')");
                        document.getElementById("name").style.borderColor = "red";
                       document.getElementById("name").style.backgroundColor = "#FFEBEB";
                       document.getElementById("name").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        name=true;
                        i_name++;
                        s_name='@lang('home.cf_nombre7')<br>';
                        }
                        
                    }
                    //i_name=1;
                }

                function check_name2(){
                    var name_length = $("#name").val().length;
                    
                    
                if(name_length<5){


                    if(name_length==0){
                        $("#greencheck").hide();
                       $("#lblName2").hide();
                       $("#lblName").fadeIn();
                   $("#lblName").html("@lang('home.cf_nombre')");
                   
                   document.getElementById("name").style.borderColor = "red";
                  document.getElementById("name").style.backgroundColor = "#FFEBEB";
                  document.getElementById("name").style.boxShadow = "0 0 0px 0px";
                   name=true;
                   i_name++;
                   s_name='@lang('home.cf_nombre')<br>';
               }else{

                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#name").val())){
                            $("#greencheck").hide();
                            $("#lblName2").hide();
                        
                        $("#lblName").fadeIn();
                        $("#lblName").html("@lang('home.cf_nombre2')");
                        
                        document.getElementById("name").style.borderColor = "red";
                       document.getElementById("name").style.backgroundColor = "#FFEBEB";
                       document.getElementById("name").style.boxShadow = "0 0 0px 0px";
                        name=true;
                        i_name++;
                        s_name='@lang('home.cf_nombre2')<br>';
                        }else{
                            $("#greencheck").hide();
                            $("#lblName2").hide();
                            $("#lblName").fadeIn();
                        $("#lblName").html("@lang('home.cf_nombre3')");
                        
                        document.getElementById("name").style.borderColor = "red";
                       document.getElementById("name").style.backgroundColor = "#FFEBEB";
                       document.getElementById("name").style.boxShadow = "0 0 0px 0px";
                        name=true;
                        i_name++;
                        s_name='@lang('home.cf_nombre3')<br>';
                        }
                    }
                        
                    }
                    
                    
                    
                    else if(name_length>30){
                        $("#greencheck").hide();
                        $("#lblName2").hide();
                        
                        $("#lblName").fadeIn();

                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#name").val())){
                        
                        
                        $("#lblName").html("@lang('home.cf_nombre4r')");
                        s_name='@lang('home.cf_nombre4r')<br>';
                        }else{
                            $("#lblName").html("@lang('home.cf_nombre5r')");
                        s_name='@lang('home.cf_nombre5r')<br>';
                        }

                        document.getElementById("name").style.borderColor = "red";
                       document.getElementById("name").style.backgroundColor = "#FFEBEB";
                       document.getElementById("name").style.boxShadow = "0 0 0px 0px";
                        name=true;
                        i_name++;
                    }

                    else{
                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#name").val())){
                            document.getElementById("name").style.borderColor = "green";
                        document.getElementById("name").style.backgroundColor = "#E4FFEF";
                        document.getElementById("name").style.boxShadow = "0 0 0px 0px";
                        $("#lblName").hide();
                        $("#lblName2").html("@lang('home.cf_nombre6')");
                        $('#lblName2').fadeOut();
                        $('#greencheck').fadeIn();
                        
                        }else{
                            $("#greencheck").hide();
                            $("#lblName2").hide();
                        
                        $("#lblName").fadeIn();
                        $("#lblName").html("@lang('home.cf_nombre7')");
                        document.getElementById("name").style.borderColor = "red";
                       document.getElementById("name").style.backgroundColor = "#FFEBEB";
                       document.getElementById("name").style.boxShadow = "0 0 0px 0px";
                        name=true;
                        i_name++;
                        s_name='@lang('home.cf_nombre7')<br>';
                        }
                        
                    }
                   
                }
                //---------------------------
                
                //----------------------------------------------------

                function check_apellidop(){
                    var apellidop_length = $("#apellidop").val().length;
                    
                   
                        if(apellidop_length<2){
                            if(apellidop_length==0){
                                //$("#greencheck").hide();
                       $("#lblApellidop2").hide();
                       $("#lblApellidop").fadeIn();
                   $("#lblApellidop").html("@lang('home.cf_apellidop')");
                   
                   document.getElementById("apellidop").style.borderColor = "red";
                  document.getElementById("apellidop").style.backgroundColor = "#FFEBEB";
                  document.getElementById("apellidop").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                   apellidop=true;
                   i_apellidop++;
                   s_apellidop='@lang('home.cf_apellidop')<br>';
     
               }else{
                if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#apellidop").val())){
                       // if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                        //$("#greencheck").hide();
                            $("#lblApellidop2").hide();
                        
                        $("#lblApellidop").fadeIn();
                        $("#lblApellidop").html("@lang('home.cf_apellidop2')");//html("@lang('home.mensaje_contactenos')");
                        
                        document.getElementById("apellidop").style.borderColor = "red";
                       document.getElementById("apellidop").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidop").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       apellidop=true;
                        i_apellidop++;
                        s_apellidop='@lang('home.cf_apellidop2')<br>';
                        }else{
                            //$("#greencheck").hide();
                            $("#lblApellidop2").hide();
                            $("#lblApellidop").fadeIn();
                        $("#lblApellidop").html("@lang('home.cf_apellidop3')");
                        
                        document.getElementById("apellidop").style.borderColor = "red";
                       document.getElementById("apellidop").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidop").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       apellidop=true;
                        i_apellidop++;
                        s_apellidop='@lang('home.cf_apellidop3')<br>';
                        }
                        //i_name++;
                      }
                        
                    }

                    //---------------------
                    //---------------------

                    else if(apellidop_length>20){
                        //$("#greencheck").hide();
                        $("#lblApellidop2").hide();
                        
                        $("#lblApellidop").fadeIn();

                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#apellidop").val())){
                            
                        
                        $("#lblApellidop").html("@lang('home.cf_apellidop4')");//html("@lang('home.mensaje_contactenos')");
                        s_apellidop='@lang('home.cf_apellidop4')<br>';
                        }else{
                            $("#lblApellidop").html("@lang('home.cf_apellidop5')");
                            s_apellidop='@lang('home.cf_cf_apellidop5')<br>';
                        }

                        document.getElementById("apellidop").style.borderColor = "red";
                       document.getElementById("apellidop").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidop").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       apellidop=true;
                    i_apellidop++;
                    
                    }

                    else{
                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#apellidop").val())){
                            //if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                            document.getElementById("apellidop").style.borderColor = "green";
                        document.getElementById("apellidop").style.backgroundColor = "#E4FFEF";
                        document.getElementById("apellidop").style.boxShadow = "0 0 1.5px 2.9px #75DA8D";
                        $("#lblApellidop").hide();
                        $("#lblApellidop2").html("@lang('home.cf_apellidop6')");
                        $('#lblApellidop2').fadeIn();
                        //$('#greencheck').fadeIn();
                        }else{
                            //$("#greencheck").hide();
                            $("#lblApellidop2").hide();
                        
                        $("#lblApellidop").fadeIn();
                        $("#lblApellidop").html("@lang('home.cf_apellidop7')");
                        document.getElementById("apellidop").style.borderColor = "red";
                       document.getElementById("apellidop").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidop").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       apellidop=true;
                        i_apellidop++;
                        s_apellidop='@lang('home.cf_apellidop7')<br>';
                        }
                        
                    }
                    //i_name=1;
                }

                function check_apellidop2(){
                    var apellidop_length = $("#apellidop").val().length;
                    
                    
                if(apellidop_length<2){


                    if(apellidop_length==0){
                        //$("#greencheck").hide();
                       $("#lblApellidop2").hide();
                       $("#lblApellidop").fadeIn();
                   $("#lblApellidop").html("@lang('home.cf_apellidop')");
                   
                   document.getElementById("apellidop").style.borderColor = "red";
                  document.getElementById("apellidop").style.backgroundColor = "#FFEBEB";
                  document.getElementById("apellidop").style.boxShadow = "0 0 0px 0px";
                  apellidop=true;
                   i_apellidop++;
                   s_apellidop='@lang('home.cf_apellidop')<br>';
               }else{

                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#apellidop").val())){
                            //$("#greencheck").hide();
                            $("#lblApellidop2").hide();
                        
                        $("#lblApellidop").fadeIn();
                        $("#lblApellidop").html("@lang('home.cf_apellidop2')");
                        
                        document.getElementById("apellidop").style.borderColor = "red";
                       document.getElementById("apellidop").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidop").style.boxShadow = "0 0 0px 0px";
                       apellidop=true;
                        i_apellidop++;
                        s_apellidop='@lang('home.cf_apellidop2')<br>';
                        }else{
                            //$("#greencheck").hide();
                            $("#lblApellidop2").hide();
                            $("#lblApellidop").fadeIn();
                        $("#lblApellidop").html("@lang('home.cf_apellidop3')");
                        
                        document.getElementById("apellidop").style.borderColor = "red";
                       document.getElementById("apellidop").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidop").style.boxShadow = "0 0 0px 0px";
                       apellidop=true;
                        i_apellidop++;
                        s_apellidop='@lang('home.cf_apellidop3')<br>';
                        }
                    }
                        
                    }
                    
                    
                    
                    else if(apellidop_length>20){
                        //$("#greencheck").hide();
                        $("#lblApellidop2").hide();
                        
                        $("#lblApellidop").fadeIn();

                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#apellidop").val())){
                        
                        
                        $("#lblApellidop").html("@lang('home.cf_apellidop4')");
                        s_apellidop='@lang('home.cf_apellidop4')<br>';
                        }else{
                            $("#lbllblApellidop").html("@lang('home.cf_apellidop5')");
                        s_apellidop='@lang('home.cf_apellidop5')<br>';
                        }

                        document.getElementById("apellidop").style.borderColor = "red";
                       document.getElementById("apellidop").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidop").style.boxShadow = "0 0 0px 0px";
                       apellidop=true;
                        i_apellidop++;
                    }

                    else{
                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#apellidop").val())){
                            document.getElementById("apellidop").style.borderColor = "green";
                        document.getElementById("apellidop").style.backgroundColor = "#E4FFEF";
                        document.getElementById("apellidop").style.boxShadow = "0 0 0px 0px";
                        $("#lblApellidop").hide();
                        $("#lblApellidop2").html("@lang('home.cf_apellidop6')");
                        $('#lblApellidop2').fadeOut();
                        //$('#greencheck').fadeIn();
                        
                        }else{
                            //$("#greencheck").hide();
                            $("#lblApellidop2").hide();
                        
                        $("#lblApellidop").fadeIn();
                        $("#lblApellidop").html("@lang('home.cf_apellidop7')");
                        document.getElementById("apellidop").style.borderColor = "red";
                       document.getElementById("apellidop").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidop").style.boxShadow = "0 0 0px 0px";
                       apellidop=true;
                        i_apellidop++;
                        s_apellidop='@lang('home.cf_apellidop7')<br>';
                        }
                        
                    }
                   
                }


                function check_apellidom(){
                    var apellidom_length = $("#apellidom").val().length;
                    
                   
                        if(apellidom_length<2){
                            if(apellidom_length==0){
                                //$("#greencheck").hide();
                       $("#lblApellidom2").hide();
                       $("#lblApellidom").fadeIn();
                   $("#lblApellidom").html("@lang('home.cf_apellidom')");
                   
                   document.getElementById("apellidom").style.borderColor = "red";
                  document.getElementById("apellidom").style.backgroundColor = "#FFEBEB";
                  document.getElementById("apellidom").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                   apellidom=true;
                   i_apellidom++;
                   s_apellidom='@lang('home.cf_apellidom')<br>';
     
               }else{
                if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#apellidom").val())){
                       // if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                        //$("#greencheck").hide();
                            $("#lblApellidom2").hide();
                        
                        $("#lblApellidom").fadeIn();
                        $("#lblApellidom").html("@lang('home.cf_apellidom2')");//html("@lang('home.mensaje_contactenos')");
                        
                        document.getElementById("apellidom").style.borderColor = "red";
                       document.getElementById("apellidom").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidom").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       apellidom=true;
                        i_apellidom++;
                        s_apellidom='@lang('home.cf_apellidom2')<br>';
                        }else{
                            //$("#greencheck").hide();
                            $("#lblApellidom2").hide();
                            $("#lblApellidom").fadeIn();
                        $("#lblApellidom").html("@lang('home.cf_apellidom3')");
                        
                        document.getElementById("apellidom").style.borderColor = "red";
                       document.getElementById("apellidom").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidom").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       apellidom=true;
                        i_apellidom++;
                        s_apellidom='@lang('home.cf_apellidom3')<br>';
                        }
                        //i_name++;
                      }
                        
                    }

                    //---------------------
                    //---------------------

                    else if(apellidom_length>20){
                        //$("#greencheck").hide();
                        $("#lblApellidom2").hide();
                        
                        $("#lblApellidom").fadeIn();

                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#apellidom").val())){
                            
                        
                        $("#lblApellidom").html("@lang('home.cf_apellidom4')");//html("@lang('home.mensaje_contactenos')");
                        s_apellidom='@lang('home.cf_apellidom4')<br>';
                        }else{
                            $("#lblApellidom").html("@lang('home.cf_apellidom5')");
                            s_apellidom='@lang('home.cf_cf_apellidom5')<br>';
                        }

                        document.getElementById("apellidom").style.borderColor = "red";
                       document.getElementById("apellidom").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidom").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       apellidom=true;
                    i_apellidom++;
                    
                    }

                    else{
                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#apellidom").val())){
                            //if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                            document.getElementById("apellidom").style.borderColor = "green";
                        document.getElementById("apellidom").style.backgroundColor = "#E4FFEF";
                        document.getElementById("apellidom").style.boxShadow = "0 0 1.5px 2.9px #75DA8D";
                        $("#lblApellidom").hide();
                        $("#lblApellidom2").html("@lang('home.cf_apellidom6')");
                        $('#lblApellidom2').fadeIn();
                        //$('#greencheck').fadeIn();
                        }else{
                            //$("#greencheck").hide();
                            $("#lblApellidom2").hide();
                        
                        $("#lblApellidom").fadeIn();
                        $("#lblApellidom").html("@lang('home.cf_apellidom7')");
                        document.getElementById("apellidom").style.borderColor = "red";
                       document.getElementById("apellidom").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidom").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       apellidom=true;
                        i_apellidom++;
                        s_apellidom='@lang('home.cf_apellidom7')<br>';
                        }
                        
                    }
                    //i_name=1;
                }

                function check_apellidom2(){
                    var apellidom_length = $("#apellidom").val().length;
                    
                    
                if(apellidom_length<2){


                    if(apellidom_length==0){
                        //$("#greencheck").hide();
                       $("#lblApellidom2").hide();
                       $("#lblApellidom").fadeIn();
                   $("#lblApellidom").html("@lang('home.cf_apellidom')");
                   
                   document.getElementById("apellidom").style.borderColor = "red";
                  document.getElementById("apellidom").style.backgroundColor = "#FFEBEB";
                  document.getElementById("apellidom").style.boxShadow = "0 0 0px 0px";
                  apellidom=true;
                   i_apellidom++;
                   s_apellidom='@lang('home.cf_apellidom')<br>';
               }else{

                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#apellidom").val())){
                            //$("#greencheck").hide();
                            $("#lblApellidom2").hide();
                        
                        $("#lblApellidom").fadeIn();
                        $("#lblApellidom").html("@lang('home.cf_apellidom2')");
                        
                        document.getElementById("apellidom").style.borderColor = "red";
                       document.getElementById("apellidom").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidom").style.boxShadow = "0 0 0px 0px";
                       apellidom=true;
                        i_apellidom++;
                        s_apellidom='@lang('home.cf_apellidom2')<br>';
                        }else{
                            //$("#greencheck").hide();
                            $("#lblApellidom2").hide();
                            $("#lblApellidom").fadeIn();
                        $("#lblApellidom").html("@lang('home.cf_apellidom3')");
                        
                        document.getElementById("apellidom").style.borderColor = "red";
                       document.getElementById("apellidom").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidom").style.boxShadow = "0 0 0px 0px";
                       apellidom=true;
                        i_apellidom++;
                        s_apellidom='@lang('home.cf_apellidom3')<br>';
                        }
                    }
                        
                    }
                    
                    
                    
                    else if(apellidom_length>20){
                        //$("#greencheck").hide();
                        $("#lblApellidom2").hide();
                        
                        $("#lblApellidom").fadeIn();

                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#apellidom").val())){
                        
                        
                        $("#lblApellidom").html("@lang('home.cf_apellidom4')");
                        s_apellidom='@lang('home.cf_apellidom4')<br>';
                        }else{
                            $("#lbllblApellidom").html("@lang('home.cf_apellidom5')");
                        s_apellidom='@lang('home.cf_apellidom5')<br>';
                        }

                        document.getElementById("apellidom").style.borderColor = "red";
                       document.getElementById("apellidom").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidom").style.boxShadow = "0 0 0px 0px";
                       apellidom=true;
                        i_apellidom++;
                    }

                    else{
                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#apellidom").val())){
                            document.getElementById("apellidom").style.borderColor = "green";
                        document.getElementById("apellidom").style.backgroundColor = "#E4FFEF";
                        document.getElementById("apellidom").style.boxShadow = "0 0 0px 0px";
                        $("#lblApellidom").hide();
                        $("#lblApellidom2").html("@lang('home.cf_apellidom6')");
                        $('#lblApellidom2').fadeOut();
                        //$('#greencheck').fadeIn();
                        
                        }else{
                            //$("#greencheck").hide();
                            $("#lblApellidom2").hide();
                        
                        $("#lblApellidom").fadeIn();
                        $("#lblApellidom").html("@lang('home.cf_apellidom7')");
                        document.getElementById("apellidom").style.borderColor = "red";
                       document.getElementById("apellidom").style.backgroundColor = "#FFEBEB";
                       document.getElementById("apellidom").style.boxShadow = "0 0 0px 0px";
                       apellidom=true;
                        i_apellidom++;
                        s_apellidom='@lang('home.cf_apellidom7')<br>';
                        }
                        
                    }
                   
                }


 //------------distrito--------------
 function check_distrito(){
                    var d= document.getElementById("distrito");

                    if(d.selectedIndex==0){
                        $("#lblDistrito2").hide();
                        $("#lblDistrito").fadeIn();
                   $("#lblDistrito").html("@lang('home.cf_distrito')");
                   document.getElementById("distrito").style.borderColor = "red";
                       document.getElementById("distrito").style.backgroundColor = "#FFEBEB";
                       document.getElementById("distrito").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        distrito=true;
                        i_distrito++;
                        s_distrito='@lang('home.cf_distrito')<br>';
                        }else{
                            
                   document.getElementById("distrito").style.borderColor = "green";
                        document.getElementById("distrito").style.backgroundColor = "#E4FFEF";
                        document.getElementById("distrito").style.boxShadow = "0 0 1.5px 2.9px #75DA8D";
                        $("#lblDistrito").hide();
                        $("#lblDistrito2").html("@lang('home.cf_distrito2')");
                        $('#lblDistrito2').fadeIn();
                        
                    }
                 
                }

                function check_distrito2(){
                    var d= document.getElementById("distrito");

                    if(d.selectedIndex==0){
                        $("#lblDistrito2").hide();
                        $("#lblDistrito").fadeIn();
                   $("#lblDistrito").html("@lang('home.cf_distrito')");
                   document.getElementById("distrito").style.borderColor = "red";
                       document.getElementById("distrito").style.backgroundColor = "#FFEBEB";
                       document.getElementById("distrito").style.boxShadow = "0 0 0px 0px";
                        distrito=true;
                        i_distrito++;
                        s_distrito='@lang('home.cf_distrito')<br>';
                        }else{
                            
                   document.getElementById("distrito").style.borderColor = "green";
                        document.getElementById("distrito").style.backgroundColor = "#E4FFEF";
                        document.getElementById("distrito").style.boxShadow = "0 0 0px 0px";
                        $("#lblDistrito").hide();
                        $("#lblDistrito2").html("@lang('home.cf_distrito2')");
                        $('#lblDistrito2').fadeOut();
                        
                    }
                 
                }

//-------------------------------------- 
function check_telefono(){
                    var telefono_length = $("#telefono").val().length;
                    
                   
                        if(telefono_length<5){
                            if(telefono_length==0){
                                //$("#greencheck").hide();
                       $("#lblTelefono2").hide();
                       $("#lblTelefono").fadeIn();
                   $("#lblTelefono").html("@lang('home.cf_telefono')");
                   
                   document.getElementById("telefono").style.borderColor = "red";
                  document.getElementById("telefono").style.backgroundColor = "#FFEBEB";
                  document.getElementById("telefono").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                  telefono=true;
                   i_telefono++;
                   s_telefono='@lang('home.cf_telefono')<br>';
     
               }else{//^[0-9-+s()]*$
                if(/^[0-9]+$/.test($("#telefono").val())){
                       // if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                        //$("#greencheck").hide();
                            $("#lblTelefono2").hide();
                        
                        $("#lblTelefono").fadeIn();
                        $("#lblTelefono").html("@lang('home.cf_telefono2')");//html("@lang('home.mensaje_contactenos')");
                        
                        document.getElementById("telefono").style.borderColor = "red";
                       document.getElementById("telefono").style.backgroundColor = "#FFEBEB";
                       document.getElementById("telefono").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       telefono=true;
                        i_telefono++;
                        s_telefono='@lang('home.cf_telefono2')<br>';
                        }else{
                            //$("#greencheck").hide();
                            $("#lblTelefono2").hide();
                            $("#lblTelefono").fadeIn();
                        $("#lblTelefono").html("@lang('home.cf_telefono3')");
                        
                        document.getElementById("telefono").style.borderColor = "red";
                       document.getElementById("telefono").style.backgroundColor = "#FFEBEB";
                       document.getElementById("telefono").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       telefono=true;
                        i_telefono++;
                        s_telefono='@lang('home.cf_telefono3')<br>';
                        }
                        //i_name++;
                      }
                        
                    }
                    else if(telefono_length>12){
                        //$("#greencheck").hide();
                        $("#lblTelefono2").hide();
                        
                        $("#lblTelefono").fadeIn();

                        if(/^[0-9]+$/.test($("#telefono").val())){
                            
                        
                        $("#lblTelefono").html("@lang('home.cf_telefono4')");//html("@lang('home.mensaje_contactenos')");
                        s_telefono='@lang('home.cf_telefono4')<br>';
                        }else{
                            $("#lblTelefono").html("@lang('home.cf_telefono5')");
                            s_telefono='@lang('home.cf_telefono5')<br>';
                        }

                        document.getElementById("telefono").style.borderColor = "red";
                       document.getElementById("telefono").style.backgroundColor = "#FFEBEB";
                       document.getElementById("telefono").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       telefono=true;
                    i_telefono++;
                    
                    }

                    else{
                        if(/^[0-9]+$/.test($("#telefono").val())){
                            //if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                            document.getElementById("telefono").style.borderColor = "green";
                        document.getElementById("telefono").style.backgroundColor = "#E4FFEF";
                        document.getElementById("telefono").style.boxShadow = "0 0 1.5px 2.9px #75DA8D";
                        $("#lblTelefono").hide();
                        $("#lblTelefono2").html("@lang('home.cf_telefono6')");
                        $('#lblTelefono2').fadeIn();
                        //$('#greencheck').fadeIn();
                        }else{
                            //$("#greencheck").hide();
                            $("#lblTelefono2").hide();
                        
                        $("#lblTelefono").fadeIn();
                        $("#lblTelefono").html("@lang('home.cf_telefono7')");
                        document.getElementById("telefono").style.borderColor = "red";
                       document.getElementById("telefono").style.backgroundColor = "#FFEBEB";
                       document.getElementById("telefono").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       telefono=true;
                        i_telefono++;
                        s_telefono='@lang('home.cf_telefono7')<br>';
                        }
                        
                    }
                    //i_name=1;
                }

                function check_telefono2(){
                    var telefono_length = $("#telefono").val().length;
                    
                   
                        if(telefono_length<5){
                            if(telefono_length==0){
                                
                       $("#lblTelefono2").hide();
                       $("#lblTelefono").fadeIn();
                   $("#lblTelefono").html("@lang('home.cf_telefono')");
                   
                   document.getElementById("telefono").style.borderColor = "red";
                  document.getElementById("telefono").style.backgroundColor = "#FFEBEB";
                  document.getElementById("telefono").style.boxShadow = "0 0 0px 0px";
                  telefono=true;
                   i_telefono++;
                   s_telefono='@lang('home.cf_telefono')<br>';
     
               }else{//^[0-9-+s()]*$
                if(/^[0-9]+$/.test($("#telefono").val())){
                       
                            $("#lblTelefono2").hide();
                        
                        $("#lblTelefono").fadeIn();
                        $("#lblTelefono").html("@lang('home.cf_telefono2')");//html("@lang('home.mensaje_contactenos')");
                        
                        document.getElementById("telefono").style.borderColor = "red";
                       document.getElementById("telefono").style.backgroundColor = "#FFEBEB";
                       document.getElementById("telefono").style.boxShadow = "0 0 0px 0px";
                       telefono=true;
                        i_telefono++;
                        s_telefono='@lang('home.cf_telefono2')<br>';
                        }else{
                            //$("#greencheck").hide();
                            $("#lblTelefono2").hide();
                            $("#lblTelefono").fadeIn();
                        $("#lblTelefono").html("@lang('home.cf_telefono3')");
                        
                        document.getElementById("telefono").style.borderColor = "red";
                       document.getElementById("telefono").style.backgroundColor = "#FFEBEB";
                       document.getElementById("telefono").style.boxShadow = "0 0 0px 0px";
                       telefono=true;
                        i_telefono++;
                        s_telefono='@lang('home.cf_telefono3')<br>';
                        }
                        //i_name++;
                      }
                        
                    }
                    else if(telefono_length>12){
                        //$("#greencheck").hide();
                        $("#lblTelefono2").hide();
                        
                        $("#lblTelefono").fadeIn();

                        if(/^[0-9]+$/.test($("#telefono").val())){
                            
                        
                        $("#lblTelefono").html("@lang('home.cf_telefono4')");//html("@lang('home.mensaje_contactenos')");
                        s_telefono='@lang('home.cf_telefono4')<br>';
                        }else{
                            $("#lblTelefono").html("@lang('home.cf_telefono5')");
                            s_telefono='@lang('home.cf_telefono5')<br>';
                        }

                        document.getElementById("telefono").style.borderColor = "red";
                       document.getElementById("telefono").style.backgroundColor = "#FFEBEB";
                       document.getElementById("telefono").style.boxShadow = "0 0 0px 0px";
                       telefono=true;
                    i_telefono++;
                    
                    }

                    else{
                        if(/^[0-9]+$/.test($("#telefono").val())){
                            //if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                            document.getElementById("telefono").style.borderColor = "green";
                        document.getElementById("telefono").style.backgroundColor = "#E4FFEF";
                        document.getElementById("telefono").style.boxShadow = "0 0 0px 0px";
                        $("#lblTelefono").hide();
                        $("#lblTelefono2").html("@lang('home.cf_telefono6')");
                        $('#lblTelefono2').fadeOut();
                        //$('#greencheck').fadeIn();
                        }else{
                            //$("#greencheck").hide();
                            $("#lblTelefono2").hide();
                        
                        $("#lblTelefono").fadeIn();
                        $("#lblTelefono").html("@lang('home.cf_telefono7')");
                        document.getElementById("telefono").style.borderColor = "red";
                       document.getElementById("telefono").style.backgroundColor = "#FFEBEB";
                       document.getElementById("telefono").style.boxShadow = "0 0 0px 0px";
                       telefono=true;
                        i_telefono++;
                        s_telefono='@lang('home.cf_telefono7')<br>';
                        }
                        
                    }
                    //i_name=1;
                }


//----------------------------------------------------------------------------

function check_t_documento(){
                    var d= document.getElementById("t_documento");

                    if(d.selectedIndex==0){
                        $("#lblT_documento2").hide();
                        $("#lblT_documento").fadeIn();
                   $("#lblT_documento").html("@lang('home.cf_t_documento')");
                   document.getElementById("t_documento").style.borderColor = "red";
                       document.getElementById("t_documento").style.backgroundColor = "#FFEBEB";
                       document.getElementById("t_documento").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       t_documento=true;
                        i_t_documento++;
                        s_t_documento='@lang('home.cf_t_documento')<br>';
                        }else{
                            
                   document.getElementById("t_documento").style.borderColor = "green";
                        document.getElementById("t_documento").style.backgroundColor = "#E4FFEF";
                        document.getElementById("t_documento").style.boxShadow = "0 0 1.5px 2.9px #75DA8D";
                        $("#lblT_documento").hide();
                        $("#lblT_documento2").html("@lang('home.cf_t_documento2')");
                        $('#lblT_documento2').fadeIn();
                        
                    }
                 
                }

                function check_t_documento2(){
                    var d= document.getElementById("t_documento");

                    if(d.selectedIndex==0){
                        $("#lblT_documento2").hide();
                        $("#lblT_documento").fadeIn();
                   $("#lblT_documento").html("@lang('home.cf_t_documento')");
                   document.getElementById("t_documento").style.borderColor = "red";
                       document.getElementById("t_documento").style.backgroundColor = "#FFEBEB";
                       document.getElementById("t_documento").style.boxShadow = "0 0 0px 0px";
                       t_documento=true;
                        i_t_documento++;
                        s_t_documento='@lang('home.cf_t_documento')<br>';
                        }else{
                            
                   document.getElementById("t_documento").style.borderColor = "green";
                        document.getElementById("t_documento").style.backgroundColor = "#E4FFEF";
                        document.getElementById("t_documento").style.boxShadow = "0 0 0px 0px";
                        $("#lblT_documento").hide();
                        $("#lblT_documento2").html("@lang('home.cf_t_documento2')");
                        $('#lblT_documento2').fadeOut();
                        
                    }
                 
                }

//--------------------------------------------------------------------------------------------------------------------------------------

function check_n_documento(){
                    var n_documento_length = $("#n_documento").val().length;
                    
                   
                        if(n_documento_length<5){
                            if(n_documento_length==0){
                                //$("#greencheck").hide();
                       $("#lblN_documento2").hide();
                       $("#lblN_documento").fadeIn();
                   $("#lblN_documento").html("@lang('home.cf_n_documento')");
                   
                   document.getElementById("n_documento").style.borderColor = "red";
                  document.getElementById("n_documento").style.backgroundColor = "#FFEBEB";
                  document.getElementById("n_documento").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                  n_documento=true;
                   i_n_documento++;
                   s_n_documento='@lang('home.cf_n_documento')<br>';
     
               }else{//^[0-9-+s()]*$
                if(/^[0-9]+$/.test($("#n_documento").val())){
                       // if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                        //$("#greencheck").hide();
                            $("#lblN_documento2").hide();
                        
                        $("#lblN_documento").fadeIn();
                        $("#lblN_documento").html("@lang('home.cf_n_documento2')");//html("@lang('home.mensaje_contactenos')");
                        
                        document.getElementById("n_documento").style.borderColor = "red";
                       document.getElementById("n_documento").style.backgroundColor = "#FFEBEB";
                       document.getElementById("n_documento").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       n_documento=true;
                        i_n_documento++;
                        s_n_documento='@lang('home.cf_n_documento2')<br>';
                        }else{
                            //$("#greencheck").hide();
                            $("#lblN_documento2").hide();
                            $("#lblN_documento").fadeIn();
                        $("#lblN_documento").html("@lang('home.cf_n_documento3')");
                        
                        document.getElementById("n_documento").style.borderColor = "red";
                       document.getElementById("n_documento").style.backgroundColor = "#FFEBEB";
                       document.getElementById("n_documento").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       n_documento=true;
                        i_n_documento++;
                        s_n_documento='@lang('home.cf_n_documento3')<br>';
                        }
                        //i_name++;
                      }
                        
                    }
                    else if(n_documento_length>12){
                        //$("#greencheck").hide();
                        $("#lblN_documento2").hide();
                        
                        $("#lblN_documento").fadeIn();

                        if(/^[0-9]+$/.test($("#n_documento").val())){
                            
                        
                        $("#lblN_documento").html("@lang('home.cf_n_documento4')");//html("@lang('home.mensaje_contactenos')");
                        s_n_documento='@lang('home.cf_n_documento4')<br>';
                        }else{
                            $("#lblN_documento").html("@lang('home.cf_n_documento5')");
                            s_n_documento='@lang('home.cf_n_documento5')<br>';
                        }

                        document.getElementById("n_documento").style.borderColor = "red";
                       document.getElementById("n_documento").style.backgroundColor = "#FFEBEB";
                       document.getElementById("n_documento").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       n_documento=true;
                    i_n_documento++;
                    
                    }

                    else{
                        if(/^[0-9]+$/.test($("#n_documento").val())){
                            //if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                            document.getElementById("n_documento").style.borderColor = "green";
                        document.getElementById("n_documento").style.backgroundColor = "#E4FFEF";
                        document.getElementById("n_documento").style.boxShadow = "0 0 1.5px 2.9px #75DA8D";
                        $("#lblN_documento").hide();
                        $("#lblN_documento2").html("@lang('home.cf_n_documento6')");
                        $('#lblN_documento2').fadeIn();
                        //$('#greencheck').fadeIn();
                        }else{
                            //$("#greencheck").hide();
                            $("#lblN_documento2").hide();
                        
                        $("#lblN_documento").fadeIn();
                        $("#lblN_documento").html("@lang('home.cf_n_documento7')");
                        document.getElementById("n_documento").style.borderColor = "red";
                       document.getElementById("n_documento").style.backgroundColor = "#FFEBEB";
                       document.getElementById("n_documento").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                       n_documento=true;
                        i_n_documento++;
                        s_n_documento='@lang('home.cf_n_documento7')<br>';
                        }
                        
                    }
                    //i_name=1;
                }

                function check_n_documento2(){
                    var n_documento_length = $("#n_documento").val().length;
                    
                   
                        if(n_documento_length<5){
                            if(n_documento_length==0){
                                //$("#greencheck").hide();
                       $("#lblN_documento2").hide();
                       $("#lblN_documento").fadeIn();
                   $("#lblN_documento").html("@lang('home.cf_n_documento')");
                   
                   document.getElementById("n_documento").style.borderColor = "red";
                  document.getElementById("n_documento").style.backgroundColor = "#FFEBEB";
                  document.getElementById("n_documento").style.boxShadow = "0 0 0px 0px";
                  n_documento=true;
                   i_n_documento++;
                   s_n_documento='@lang('home.cf_n_documento')<br>';
     
               }else{//^[0-9-+s()]*$
                if(/^[0-9]+$/.test($("#n_documento").val())){
                       // if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                        //$("#greencheck").hide();
                            $("#lblN_documento2").hide();
                        
                        $("#lblN_documento").fadeIn();
                        $("#lblN_documento").html("@lang('home.cf_n_documento2')");//html("@lang('home.mensaje_contactenos')");
                        
                        document.getElementById("n_documento").style.borderColor = "red";
                       document.getElementById("n_documento").style.backgroundColor = "#FFEBEB";
                       document.getElementById("n_documento").style.boxShadow = "0 0 0px 0px";
                       n_documento=true;
                        i_n_documento++;
                        s_n_documento='@lang('home.cf_n_documento2')<br>';
                        }else{
                            //$("#greencheck").hide();
                            $("#lblN_documento2").hide();
                            $("#lblN_documento").fadeIn();
                        $("#lblN_documento").html("@lang('home.cf_n_documento3')");
                        
                        document.getElementById("n_documento").style.borderColor = "red";
                       document.getElementById("n_documento").style.backgroundColor = "#FFEBEB";
                       document.getElementById("n_documento").style.boxShadow = "0 0 0px 0px";
                       n_documento=true;
                        i_n_documento++;
                        s_n_documento='@lang('home.cf_n_documento3')<br>';
                        }
                        //i_name++;
                      }
                        
                    }
                    else if(n_documento_length>12){
                        //$("#greencheck").hide();
                        $("#lblN_documento2").hide();
                        
                        $("#lblN_documento").fadeIn();

                        if(/^[0-9]+$/.test($("#n_documento").val())){
                            
                        
                        $("#lblN_documento").html("@lang('home.cf_n_documento4')");//html("@lang('home.mensaje_contactenos')");
                        s_n_documento='@lang('home.cf_n_documento4')<br>';
                        }else{
                            $("#lblN_documento").html("@lang('home.cf_n_documento5')");
                            s_n_documento='@lang('home.cf_n_documento5')<br>';
                        }

                        document.getElementById("n_documento").style.borderColor = "red";
                       document.getElementById("n_documento").style.backgroundColor = "#FFEBEB";
                       document.getElementById("n_documento").style.boxShadow = "0 0 0px 0px";
                       n_documento=true;
                    i_n_documento++;
                    
                    }

                    else{
                        if(/^[0-9]+$/.test($("#n_documento").val())){
                            //if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                            document.getElementById("n_documento").style.borderColor = "green";
                        document.getElementById("n_documento").style.backgroundColor = "#E4FFEF";
                        document.getElementById("n_documento").style.boxShadow = "0 0 0px 0px";
                        $("#lblN_documento").hide();
                        $("#lblN_documento2").html("@lang('home.cf_n_documento6')");
                        $('#lblN_documento2').fadeOut();
                        //$('#greencheck').fadeIn();
                        }else{
                            //$("#greencheck").hide();
                            $("#lblN_documento2").hide();
                        
                        $("#lblN_documento").fadeIn();
                        $("#lblN_documento").html("@lang('home.cf_n_documento7')");
                        document.getElementById("n_documento").style.borderColor = "red";
                       document.getElementById("n_documento").style.backgroundColor = "#FFEBEB";
                       document.getElementById("n_documento").style.boxShadow = "0 0 0px 0px";
                       n_documento=true;
                        i_n_documento++;
                        s_n_documento='@lang('home.cf_n_documento7')<br>';
                        }
                        
                    }
                    //i_name=1;
                }
//-----------------------------------------------------------------

function check_email(){

var pattern=new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
var email_length = $("#email").val().length;

if(email_length<=320){//320
if(email_length==0){
            //$("#greencheck").hide();
   $("#lblEmail2").hide();
   $("#lblEmail").fadeIn();
$("#lblEmail").html("@lang('home.cf_email')");
document.getElementById("email").style.borderColor = "red";
document.getElementById("email").style.backgroundColor = "#FFEBEB";
document.getElementById("email").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
email=true;
i_email++;
s_email='@lang('home.cf_email')<br>';
}else{
if(pattern.test($("#email").val())){
//$("#greencheck").hide();
document.getElementById("email").style.borderColor = "green";
    document.getElementById("email").style.backgroundColor = "#E4FFEF";
    document.getElementById("email").style.boxShadow = "0 0 1.5px 2.9px #75DA8D";
        $("#lblEmail").hide();
        $("#lblEmail2").html("@lang('home.cf_email2')");
        $('#lblEmail2').fadeIn();
        //$('#greencheck').fadeIn();
}else{
$("#lblEmail2").hide();
    $("#lblEmail").fadeIn();
    $("#lblEmail").html("@lang('home.cf_email3')");//html("@lang('home.mensaje_contactenos')");
    document.getElementById("email").style.borderColor = "red";
   document.getElementById("email").style.backgroundColor = "#FFEBEB";
   document.getElementById("email").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
    email=true;
    i_email++;
    s_email='@lang('home.cf_email3')<br>';
}
}
}else{
    $("#lblEmail2").hide();
    $("#lblEmail").fadeIn();
    if(pattern.test($("#email").val())){ 
    //----------------------------------------------------------
        $("#lblEmail").html("@lang('home.cf_email4')");//html("@lang('home.mensaje_contactenos')");
        s_email='@lang('home.cf_email4')<br>';
        }else{
            $("#lblEmail").html("@lang('home.cf_email5')");
        s_email='@lang('home.cf_email5')<br>';
        }

        document.getElementById("email").style.borderColor = "red";
       document.getElementById("email").style.backgroundColor = "#FFEBEB";
       document.getElementById("email").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
        email=true;
        i_email++;
   
}



}

function check_email2(){

var pattern=new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
var email_length = $("#email").val().length;

if(email_length<=320){
if(email_length==0){
//$("#greencheck").hide();
$("#lblEmail2").hide();
$("#lblEmail").fadeIn();
$("#lblEmail").html("@lang('home.cf_email')");

document.getElementById("email").style.borderColor = "red";
document.getElementById("email").style.backgroundColor = "#FFEBEB";
document.getElementById("email").style.boxShadow = "0 0 0px 0px";
email=true;
i_email++;
s_email='@lang('home.cf_email')<br>';

}else{
if(pattern.test($("#email").val())){
//$("#greencheck").hide();
document.getElementById("email").style.borderColor = "green";
document.getElementById("email").style.backgroundColor = "#E4FFEF";
document.getElementById("email").style.boxShadow = "0 0 0px 0px";

$("#lblEmail").hide();
$("#lblEmail2").html("@lang('home.cf_email2')");
$('#lblEmail2').fadeOut();
//$('#greencheck').fadeIn();
}else{ 
$("#lblEmail2").hide();
$("#lblEmail").fadeIn();
$("#lblEmail").html("@lang('home.cf_email3')");//html("@lang('home.mensaje_contactenos')");

document.getElementById("email").style.borderColor = "red";
document.getElementById("email").style.backgroundColor = "#FFEBEB";
document.getElementById("email").style.boxShadow = "0 0 0px 0px";
email=true;
i_email++;
s_email='@lang('home.cf_email3')<br>';
}



}
}else{
$("#lblEmail2").hide();

$("#lblEmail").fadeIn();

if(pattern.test($("#email").val())){

//----------------------------------------------------------
$("#lblEmail").html("@lang('home.cf_email4')");//html("@lang('home.mensaje_contactenos')");
s_email='@lang('home.cf_email4')<br>';
}else{
$("#lblEmail").html("@lang('home.cf_email5')");
s_email='@lang('home.cf_email5')<br>';
}

document.getElementById("email").style.borderColor = "red";
document.getElementById("email").style.backgroundColor = "#FFEBEB";
document.getElementById("email").style.boxShadow = "0 0 0px 0px";
email=true;
i_email++;
}



}

 
//------------------------------------------------------
function check_message(){
                    var message_length = $("#message").val().length;
                        if(message_length<5){
                            if(message_length==0){
                                //$("#greencheck").hide();
                       $("#lblMessage2").hide();
                       $("#lblMessage").fadeIn();
                   $("#lblMessage").html("@lang('home.cf_message')");
                   
                   document.getElementById("message").style.borderColor = "red";
                  document.getElementById("message").style.backgroundColor = "#FFEBEB";
                  document.getElementById("message").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                   message=true;
                   i_message++;
                   s_message='@lang('home.cf_message')<br>';
               }else{
                            $("#lblMessage2").hide();
                        
                        $("#lblMessage").fadeIn();
                        $("#lblMessage").html("@lang('home.cf_message2')");//html("@lang('home.mensaje_contactenos')");
                        
                        document.getElementById("message").style.borderColor = "red";
                       document.getElementById("message").style.backgroundColor = "#FFEBEB";
                       document.getElementById("message").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        message=true;
                        i_message++;
                        s_message='@lang('home.cf_message2')<br>';
                      }   
                    }
                    else if(message_length>320){
                        //$("#greencheck").hide();
                        $("#lblMessage2").hide();
                        
                        $("#lblMessage").fadeIn();

                        $("#lblMessage").html("@lang('home.cf_message3')");//html("@lang('home.mensaje_contactenos')");
                        
                        document.getElementById("message").style.borderColor = "red";
                       document.getElementById("message").style.backgroundColor = "#FFEBEB";
                       document.getElementById("message").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        message=true;
                        i_message++;
                        s_message='@lang('home.cf_message3')<br>';
                    }
                    else{
                         document.getElementById("message").style.borderColor = "green";
                        document.getElementById("message").style.backgroundColor = "#E4FFEF";
                        document.getElementById("message").style.boxShadow = "0 0 1.5px 2.9px #75DA8D";
                        $("#lblMessage").hide();
                        $("#lblMessage2").html("@lang('home.cf_message4')");
                        $('#lblMessage2').fadeIn();
                    }
                }
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                function check_message2(){
                    var message_length = $("#message").val().length;
                    
                    
                if(message_length<5){


                    if(message_length==0){
                        //$("#greencheck").hide();
                       $("#lblMessage2").hide();
                       $("#lblMessage").fadeIn();
                   $("#lblMessage").html("@lang('home.cf_message')");
                   
                   document.getElementById("message").style.borderColor = "red";
                  document.getElementById("message").style.backgroundColor = "#FFEBEB";
                  document.getElementById("message").style.boxShadow = "0 0 0px 0px";
                   message=true;
                   i_message++;
                   s_message='@lang('home.cf_message')<br>';
               }else{

                        
                            //$("#greencheck").hide();
                            $("#lblMessage2").hide();
                        
                        $("#lblMessage").fadeIn();
                        $("#lblMessage").html("@lang('home.cf_message2')");
                        
                        document.getElementById("message").style.borderColor = "red";
                       document.getElementById("message").style.backgroundColor = "#FFEBEB";
                       document.getElementById("message").style.boxShadow = "0 0 0px 0px";
                        message=true;
                        i_message++;
                        s_message='@lang('home.cf_message2')<br>';
                    }
                        
                    }
                    
                    else if(message_length>320){
                        //$("#greencheck").hide();
                        $("#lblMessage2").hide();
                        
                        $("#lblMessage").fadeIn();

                        
                        
                        
                        $("#lblMessage").html("@lang('home.cf_message3')");
                        

                        document.getElementById("message").style.borderColor = "red";
                       document.getElementById("message").style.backgroundColor = "#FFEBEB";
                       document.getElementById("message").style.boxShadow = "0 0 0px 0px";
                        message=true;
                        i_message++;
                        s_message='@lang('home.cf_message3')<br>';
                    }

                    else{
                       
                            document.getElementById("message").style.borderColor = "green";
                        document.getElementById("message").style.backgroundColor = "#E4FFEF";
                        document.getElementById("message").style.boxShadow = "0 0 0px 0px";
                        $("#lblMessage").hide();
                        $("#lblMessage2").html("@lang('home.cf_message4')");
                        $('#lblMessage2').fadeOut();
                        //$('#greencheck').fadeIn();
                        
                       
                        
                    }
                   
                }


       
//-----------------------------------------------------------------14-2-2020
//-----------------------------------------------------------------14-2-2020
  
       

document.addEventListener("click", (evt) => {
                
                const sug = document.getElementById("sugerencia");
                const que = document.getElementById("queja");
                const rec = document.getElementById("reclamo");
    
               

    let targetElement = evt.target; 

    do {
        if (targetElement == sug || targetElement == que || targetElement == rec) {

            var value=$("input:radio[name=t_reclamo]:checked").val();
    
    $("#lblD_reclamacion").hide();
    $("#lblD_reclamacion2").fadeIn();
    $("#lblD_reclamacion2").html("@lang('home.cf_t_reclamo2'): "+value);
          
            return;
        }
        
        targetElement = targetElement.parentNode;
    } while (targetElement);

    if ($("#reclamo").is(':checked') || $("#queja").is(':checked') || $("#sugerencia").is(':checked'))
    {
     
    $("#lblD_reclamacion2").fadeOut();
    
    }else{   
    
}
});



$("#submit_form").submit(function(e){

//var n=i_name+i_email+i_message;
            name=false;
            apellidop=false;
            apellidom=false;
            distrito=false;
            telefono=false;
            t_documento=false;
            n_documento=false;
            email=false;
            t_reclamo=false;
            message=false;

            i_name=0;
            i_apellidop=0;
            i_apellidom=0;
            i_distrito=0;
            i_telefono=0;
            i_t_documento=0;
            i_n_documento=0;
            i_email=0;
            i_t_reclamo=0;
            i_message=0;

            s_name='';
            s_apellidop='';
            s_apellidom='';
            s_distrito='';
            s_telefono='';
            s_t_documento='';
            s_n_documento='';
            s_email='';
            s_t_reclamo='';
            s_message='';

    check_name2();
    check_apellidop2();
    check_apellidom2();
    check_distrito2();
    check_telefono2();
    check_t_documento2();
    check_n_documento2();
    check_message2();
    
    check_email2();
    



    if ($("#reclamo").is(':checked') || $("#queja").is(':checked') || $("#sugerencia").is(':checked'))
    {
     
    $("#lblD_reclamacion").hide();
    //$("#lblD_reclamacion").html("si");
    
    }else{   
   
    $("#lblD_reclamacion").fadeIn();
    $("#lblD_reclamacion").html("@lang('home.cf_t_reclamo')");
    t_reclamo=true;
                        i_t_reclamo++;
                        s_t_reclamo='@lang('home.cf_t_reclamo')<br>';
}

    

    if(name==false && apellidop==false && apellidom==false && distrito==false && telefono==false && 
    t_documento==false && n_documento==false && email==false && message==false && 
    t_reclamo==false){
        //-----------------------------------------------------------------------------------
        if($("#ch1").is(':checked')){ 

       // e.preventDefault();
        //14-2-2020
        var route=$('#submit-form').data('route');
                        var submit_form=$(this);
        //14-2-2020

        var value3=$("input:radio[name=t_reclamo]:checked").val();
        
        $.ajax({
           // url:"{{route('reclamaciones.send') }}",
            type:"POST",
           url:route,
            data: submit_form.serialize(),
            success:function(Response){

                Swal.fire({
                    type: 'success',
title:'@lang('home.cf_bresponse')',
html: '<i class="fas fa-check-circle" style="color:green"></i> @lang('home.cf_bresponse1') <i class="fas fa-check-circle"  style="color:green"></i>'+
'<br><br><div class="alert alert-success" role="alert">'
+'@lang('home.cf_bresponse11')'+'<br>'
+'@lang('home.cf_bresponse2'): '+$('#name').val()+'<br>'
+'@lang('home.cf_bresponse4'): '+$('#apellidop').val()+'<br>'
+'@lang('home.cf_bresponse5'): '+$('#apellidom').val()+'<br>'
+'@lang('home.cf_bresponse6'): '+$('#distrito').val()+'<br>'
+'@lang('home.cf_bresponse7'): '+$('#telefono').val()+'<br>'
+'@lang('home.cf_bresponse8'): '+$('#t_documento').val()+'<br>'
+'@lang('home.cf_bresponse9'): '+$('#n_documento').val()+'<br>'
+'@lang('home.cf_bresponse3'): '+$('#email').val()+'<br>'
+'@lang('home.cf_bresponse10'): '+value3+'</div>',
animation: false,
customClass: {
popup: 'animated tada'
}
})
            }
        });
        e.preventDefault();

        }else{
           // e.preventDefault();
             //14-2-2020
             var route=$('#submit-form').data('route');
                        var submit_form=$(this);
        //14-2-2020
        var value3=$("input:radio[name=t_reclamo]:checked").val();
        
        $.ajax({
           // url:"{{route('reclamaciones.send') }}",
            type:"POST",
            url:route,
            //data:{name:name,email:email,message:message},
            data: submit_form.serialize(),
            success:function(Response){

                Swal.fire({
                    type: 'success',
/*title: '@lang('home.boton_contactenos')'+$('#name').val(),*/
title:'@lang('home.cf_bresponse')',
html: '<i class="fas fa-check-circle" style="color:green"></i> @lang('home.cf_bresponse1') <i class="fas fa-check-circle"  style="color:green"></i>'+
'<br><br><div class="alert alert-success" role="alert">'
+'@lang('home.cf_bresponse12')'+'<br>'
+'@lang('home.cf_bresponse2'): '+$('#name').val()+'<br>'
+'@lang('home.cf_bresponse4'): '+$('#apellidop').val()+'<br>'
+'@lang('home.cf_bresponse5'): '+$('#apellidom').val()+'<br>'
+'@lang('home.cf_bresponse6'): '+$('#distrito').val()+'<br>'
+'@lang('home.cf_bresponse7'): '+$('#telefono').val()+'<br>'
+'@lang('home.cf_bresponse8'): '+$('#t_documento').val()+'<br>'
+'@lang('home.cf_bresponse9'): '+$('#n_documento').val()+'<br>'
+'@lang('home.cf_bresponse3'): '+$('#email').val()+'<br>'
//+'@lang('home.cf_bresponse10'): '+$('#t_reclamo').val()+'</div>',
+'@lang('home.cf_bresponse10'): '+value3+'</div>',
animation: false,
customClass: {
popup: 'animated tada'
}
})
            }
        });
        e.preventDefault();

        }
        //------------------------------------------------------------------------------------
    }else{
        var n=i_name+i_apellidop+i_apellidom+i_distrito+i_telefono+i_t_documento+i_n_documento+i_email+i_t_reclamo+i_message;
        var st=s_name+s_apellidop+s_apellidom+s_distrito+s_telefono+s_t_documento+s_n_documento+s_email+s_t_reclamo+s_message;
        if(n==1){  
        Swal.fire({
            
type: 'error',
title: '@lang('home.cf_tresponse')',
/*text: 'Se ha encontrado '+n+' error'+' /n'+st,*/
html: '<i class="fas fa-exclamation-triangle" style="color:red"></i> @lang('home.cf_tresponse1') '+n+' error <i class="fas fa-exclamation-triangle"  style="color:red"></i>'+
'<br><br><div class="alert alert-danger" role="alert">'+st+'</div>',

})}else{

Swal.fire({
            
            type: 'error',
            title: '@lang('home.cf_tresponse')',
            html: '<i class="fas fa-exclamation-triangle" style="color:red"></i> @lang('home.cf_tresponse2') '+n+' @lang('home.cf_tresponse3') <i class="fas fa-exclamation-triangle"  style="color:red"></i>'+
            '<br><br><div class="alert alert-danger" role="alert">'+st+'</div>',
            
          })

}
return false;
        
        
    }

   
});




//-----------------------------------------------------------------14-2-2020
//-----------------------------------------------------------------14-2-2020


        });
</script>



          
</div>


@endsection