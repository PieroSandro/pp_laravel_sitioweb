@extends('layouts.master2_2')
@section('title','Videos')

@section('content')

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<link rel="stylesheet" href="{{url('/css/animate.css')}}">


<!--------------ANIMATE ON SCROLL------------------------->
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        
        <script>
        AOS.init({
          duration:3000,
        });
        </script>
        <!------------------------------------------->

<style>
.formpar{
    
    vertical-align: middle;
  border-bottom: 35px solid rgba(228, 20, 20, 0.931);
  border-left: 20px solid transparent;
  height: 0;
  
  box-shadow: 5px 0px white, 10px 0px rgba(26, 26, 161, 0.993), 15px 0px white, 20px 0px rgba(26, 26, 161, 0.993), 25px 0px white, 30px 0px rgba(26, 26, 161, 0.993);
  transform: skew(30deg);
  

}
</style>






<label id="qs"></label>

<br>
<br>





<div data-aos="fade-right" style="padding-top: 40px;
  padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">
<div class="formpar" id="fpr"><h3><div style="transform: skewX(-45deg); color: white; font-family: 'Montserrat', sans-serif;">Videos</div></h3></div>


<div style="padding-top: 40px;
  padding-right: 0px;
  padding-bottom: 40px;
  padding-left: 0px;">
  
<div align="center">

  <?php
  $counter = 1;
  
    ?>
<!--------------------MARCO-------------------------------------------->
<!--<iframe width="320" height="240" src="https://www.youtube.com/embed/CDV2AwOeeis" 
    frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/CDV2AwOeeis" 
    frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        
        <br/>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/CDV2AwOeeis" 
    frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/CDV2AwOeeis" 
    frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        <br/>
      
	   <iframe width="320" height="240" src="https://www.youtube.com/embed/CDV2AwOeeis" 
    frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            <iframe width="320" height="240" src="https://embed.ted.com/talks/lera_boroditsky_how_language_shapes_the_way_we_think" 
            frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      
            <br/>
       
	   <iframe width="320" height="240" src="https://www.youtube.com/embed/CDV2AwOeeis" 
    frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            <iframe width="320" height="240" src="https://embed.ted.com/talks/lera_boroditsky_how_language_shapes_the_way_we_think" 
            frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>-->


            @if(count($videos)>0)
            @foreach($videos as $video)
             
            
            
            @if((time() - strtotime($video->created_at)) <= 86400)
            
           
            @else
            
            @endif

            <?php
$bodyevent='';
            $bodyevent=$video->vid;
            
            //$bodyevent1 = html_entity_decode($bodyevent);
            $bodyevent1=str_replace("watch?v=","embed/",$bodyevent);
if ($counter % 2 == 1){ 
      ?>
<iframe width="320" height="240" src="{{$bodyevent1}}" 
frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
<?php
}else{?>
 <iframe width="320" height="240" src="{{$bodyevent1}}" 
 frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
     
     <br/>


<?php }
?>

<?php
                        
$counter++;

?>
@endforeach
<br>
<br>
{{$videos->links('vendor.pagination.bootstrap-4')}}
@else
<p>@lang('home.bl_nopost')</p>
@endif
            <!--------------------MARCO-------------------------------------------->

</div>
</div>






</div>
<!-----------SCRIPT------------------------------->
<script>
    $(document).ready(function() {
    function checkWidth() {
      
        var windowSize = $(window).width();

        if (windowSize <= 700) {
          $("#math").html('jite');
        }
        else{
          $("#math").html('manu');
        }
    }

   
    checkWidth();
    
    $(window).resize(checkWidth);
});
</script>
    
    
<script>
    $( document ).ready(function() {
      function Scrolldown() {
     
     document.getElementById("qs").scrollIntoView();
}

window.onload = Scrolldown;
    });

    $( window ).on( "load", function() {
      function Scrolldown() {
     
     document.getElementById("qs").scrollIntoView();
}

window.onload = Scrolldown;
    });
 
   
    </script>







<script>
          $(function(){
            
                $(window).resize(function(){
                    winSize();
               });

               function winSize(){
               
        if ($(window).width() < 768) {
          document.getElementById("fpr").style.width = "100%";
          
        }
    
    else{
      if ($(window).width() < 992) {
      document.getElementById("fpr").style.width = "100%";
     
      }else{
        if($(window).width() < 1200){
          document.getElementById("fpr").style.width = "50%";
          
        }else{
          document.getElementById("fpr").style.width = "50%";
         
        }
      }
    }
          }
           

               $(window).show(function(){
                    win2();
               });

               function win2(){
               
        if ($(window).width() < 768) {
          document.getElementById("fpr").style.width = "100%";
          
        }
    
    else{
      if ($(window).width() < 992) {
      document.getElementById("fpr").style.width = "100%";
      
      }else{
        if($(window).width() < 1200){
          document.getElementById("fpr").style.width = "50%";
          
        }else{
          document.getElementById("fpr").style.width = "50%";
          
        }
      }
    }
          }
          });
        </script>
@endsection