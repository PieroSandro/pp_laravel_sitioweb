@extends('layouts.master2')
@section('title')
@lang('home.ie_index')

@endsection
@section('content')


<!--------------ANIMATE ON SCROLL------------------------->
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        
        <script>
        AOS.init({
          duration:3000,
        });
        </script>
        <!------------------------------------------->

<style>
.formpar{
   
   vertical-align: middle;
   
 width: 86%;
 box-shadow: 5px 0px white, 10px 0px rgba(26, 26, 161, 0.993), 15px 0px white, 
 20px 0px rgba(26, 26, 161, 0.993), 25px 0px white, 30px 0px rgba(26, 26, 161, 0.993);
 transform: skew(30deg);
}
</style>


<label id="qs4">
<br>
<br>

<div class="text">
<div data-aos="fade-right" style="padding-top: 40px;
  padding-right: 40px;
  padding-bottom: 0px;
  padding-left: 40px;">

        
        
        
</div>
        <div style="
  padding-right: 14px;
  
  padding-left: 14px;">
    <div class="formpar" id="fpr"><h3><div style="transform: skewX(-45deg); color: white; font-family: 'Montserrat', sans-serif;">@lang('home.ie_index')</div></h3></div>
</div>
<br>        

<div style="padding-top: 0px;
  padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">




       <!-- @lang('home.ie_content')
        <br><br>
        <center>
  	<img src="images/Engligh_class.jpg" id="img1"/></center>-->


<!------------------------------------------------------------------------------------>

<?php

$locale=App::getLocale();
          

            if(App::isLocale('en')){
              ?>
         
        
         @if(count($mainpages)>0)
            @foreach($mainpages as $mainpage)
            <div id="txt4" style="text-align: justify; font-family: Montserrat, sans-serif; width:100%;">

            <p style="text-align: justify; font-family: Montserrat, sans-serif;">{!!$mainpage->body_en!!}</p>
            </div>
            <br><br>
            <center>
            <img src="/intranet/storage/app/public/images/{{$mainpage->foto_en}}" id="img1"/></center>           
            @endforeach

            {{$mainpages->links()}}
            @else
              @endif
                <?php    
                        
          }else{
            ?>
             
             @if(count($mainpages)>0)
            @foreach($mainpages as $mainpage)
            <div id="txt4" style="text-align: justify; font-family: Montserrat, sans-serif; width:100%;">

            <p style="text-align: justify; font-family: Montserrat, sans-serif;">{!!$mainpage->body_es!!}</p>
            </div>
           <br><br>
           <center>
           <img src="/intranet/storage/app/public/images/{{$mainpage->foto_es}}" id="img1"/></center>         
            @endforeach

            {{$mainpages->links()}}
            @else
              @endif
         
              <?php
            }
?>
<!------------------------------------------------------------------------------------>



        </div>
</div>
<br>

</label>



<!------------------------------------------------->

<script>
    $( document ).ready(function() {
      function Scrolldown() {
     //window.scroll(0,562); 
     document.getElementById("qs4").scrollIntoView();
}

window.onload = Scrolldown;
    });

    $( window ).on( "load", function() {
      function Scrolldown() {
     //window.scroll(0,562); 
     document.getElementById("qs4").scrollIntoView();
}

window.onload = Scrolldown;
    });
 
    /*$( window ).on( "load", function() {
        console.log( "window loaded" );
    });*/
    </script>








<?php
           
            $locale=App::getLocale();

            if(App::isLocale('en')){
              ?>


<script>






          $(function(){
            
                $(window).resize(function(){
                    winSize();
               });


               //border-bottom: 80px solid rgba(228, 20, 20, 0.931);
 //border-left: 46px solid transparent;
 //height: 80px;
 function winSize(){
               
               if ($(window).width() < 520) {
                 //document.getElementById("fpr").style.width = "100%";
                 document.getElementById("fpr").style.marginLeft = "10px";
               document.getElementById("fpr").style.marginRight = "10px";
                 document.getElementById("fpr").style.borderBottom = "80px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "46px solid transparent";
                 document.getElementById("fpr").style.height = "80px";
               
               }
           
           else{
             if ($(window).width() < 751) {
               document.getElementById("fpr").style.marginLeft = "23px";
               document.getElementById("fpr").style.marginRight = "23px";
               document.getElementById("fpr").style.borderBottom = "35px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "20px solid transparent";
                 document.getElementById("fpr").style.height = "0px";
             //document.getElementById("fpr").style.width = "100%";
            
             }else{
               if($(window).width() < 845){
                 document.getElementById("fpr").style.marginLeft = "0px";
               document.getElementById("fpr").style.marginRight = "0px";
                 document.getElementById("fpr").style.borderBottom = "115px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "66px solid transparent";
                 document.getElementById("fpr").style.height = "115px";
                 //document.getElementById("fpr").style.width = "50%";
                 
               }else{
                 if($(window).width() < 975){  
                   document.getElementById("fpr").style.marginLeft = "10px";
               document.getElementById("fpr").style.marginRight = "10px";
                 document.getElementById("fpr").style.borderBottom = "80px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "46px solid transparent";
                 document.getElementById("fpr").style.height = "80px";
                 //document.getElementById("fpr").style.width = "50%";
                 }else{
                   document.getElementById("fpr").style.marginLeft = "23px";
                 document.getElementById("fpr").style.marginRight = "23px";
                 document.getElementById("fpr").style.borderBottom = "35px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "20px solid transparent";
                 document.getElementById("fpr").style.height = "0";
                 }
               }
             }
           }
                 }
           

               $(window).show(function(){
                    win2();
               });

               function win2(){
               
               if ($(window).width() < 520) {
                 //document.getElementById("fpr").style.width = "100%";
                 document.getElementById("fpr").style.marginLeft = "10px";
               document.getElementById("fpr").style.marginRight = "10px";
                 document.getElementById("fpr").style.borderBottom = "80px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "46px solid transparent";
                 document.getElementById("fpr").style.height = "80px";
               
               }
           
           else{
             if ($(window).width() < 751) {
               document.getElementById("fpr").style.marginLeft = "23px";
               document.getElementById("fpr").style.marginRight = "23px";
               document.getElementById("fpr").style.borderBottom = "35px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "20px solid transparent";
                 document.getElementById("fpr").style.height = "0px";
             //document.getElementById("fpr").style.width = "100%";
            
             }else{
               if($(window).width() < 845){
                 document.getElementById("fpr").style.marginLeft = "0px";
               document.getElementById("fpr").style.marginRight = "0px";
                 document.getElementById("fpr").style.borderBottom = "115px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "66px solid transparent";
                 document.getElementById("fpr").style.height = "115px";
                 //document.getElementById("fpr").style.width = "50%";
                 
               }else{
                 if($(window).width() < 975){  
                   document.getElementById("fpr").style.marginLeft = "10px";
               document.getElementById("fpr").style.marginRight = "10px";
                 document.getElementById("fpr").style.borderBottom = "80px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "46px solid transparent";
                 document.getElementById("fpr").style.height = "80px";
                 //document.getElementById("fpr").style.width = "50%";
                 }else{
                   document.getElementById("fpr").style.marginLeft = "23px";
                 document.getElementById("fpr").style.marginRight = "23px";
                 document.getElementById("fpr").style.borderBottom = "35px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "20px solid transparent";
                 document.getElementById("fpr").style.height = "0";
                 }
               }
             }
           }
                 }
          });
        </script>


<?php    
                        
          }else{?>

<script>
          $(function(){
            
                $(window).resize(function(){
                    winSize();
               });


               //border-bottom: 80px solid rgba(228, 20, 20, 0.931);
 //border-left: 46px solid transparent;
 //height: 80px;
 function winSize(){
               
               if ($(window).width() < 520) {
                 //document.getElementById("fpr").style.width = "100%";
                 document.getElementById("fpr").style.marginLeft = "10px";
               document.getElementById("fpr").style.marginRight = "10px";
                 document.getElementById("fpr").style.borderBottom = "80px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "46px solid transparent";
                 document.getElementById("fpr").style.height = "80px";
               
               }
           
           else{
             if ($(window).width() < 751) {
               document.getElementById("fpr").style.marginLeft = "23px";
               document.getElementById("fpr").style.marginRight = "23px";
               document.getElementById("fpr").style.borderBottom = "35px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "20px solid transparent";
                 document.getElementById("fpr").style.height = "0px";
             //document.getElementById("fpr").style.width = "100%";
            
             }else{
               if($(window).width() < 765){
                 document.getElementById("fpr").style.marginLeft = "0px";
               document.getElementById("fpr").style.marginRight = "0px";
                 document.getElementById("fpr").style.borderBottom = "115px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "66px solid transparent";
                 document.getElementById("fpr").style.height = "115px";
                 //document.getElementById("fpr").style.width = "50%";
                 
               }else{
                 if($(window).width() < 975){  
                   document.getElementById("fpr").style.marginLeft = "10px";
               document.getElementById("fpr").style.marginRight = "10px";
                 document.getElementById("fpr").style.borderBottom = "80px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "46px solid transparent";
                 document.getElementById("fpr").style.height = "80px";
                 //document.getElementById("fpr").style.width = "50%";
                 }else{
                   document.getElementById("fpr").style.marginLeft = "23px";
                 document.getElementById("fpr").style.marginRight = "23px";
                 document.getElementById("fpr").style.borderBottom = "35px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "20px solid transparent";
                 document.getElementById("fpr").style.height = "0";
                 }
               }
             }
           }
                 }
           

               $(window).show(function(){
                    win2();
               });

               function win2(){
               
               if ($(window).width() < 520) {
                 //document.getElementById("fpr").style.width = "100%";
                 document.getElementById("fpr").style.marginLeft = "10px";
               document.getElementById("fpr").style.marginRight = "10px";
                 document.getElementById("fpr").style.borderBottom = "80px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "46px solid transparent";
                 document.getElementById("fpr").style.height = "80px";
               
               }
           
           else{
             if ($(window).width() < 751) {
               document.getElementById("fpr").style.marginLeft = "23px";
               document.getElementById("fpr").style.marginRight = "23px";
               document.getElementById("fpr").style.borderBottom = "35px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "20px solid transparent";
                 document.getElementById("fpr").style.height = "0px";
             //document.getElementById("fpr").style.width = "100%";
            
             }else{
               if($(window).width() < 765){
                 document.getElementById("fpr").style.marginLeft = "0px";
               document.getElementById("fpr").style.marginRight = "0px";
                 document.getElementById("fpr").style.borderBottom = "115px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "66px solid transparent";
                 document.getElementById("fpr").style.height = "115px";
                 //document.getElementById("fpr").style.width = "50%";
                 
               }else{
                 if($(window).width() < 975){  
                   document.getElementById("fpr").style.marginLeft = "10px";
               document.getElementById("fpr").style.marginRight = "10px";
                 document.getElementById("fpr").style.borderBottom = "80px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "46px solid transparent";
                 document.getElementById("fpr").style.height = "80px";
                 //document.getElementById("fpr").style.width = "50%";
                 }else{
                   document.getElementById("fpr").style.marginLeft = "23px";
                 document.getElementById("fpr").style.marginRight = "23px";
                 document.getElementById("fpr").style.borderBottom = "35px solid rgba(228, 20, 20, 0.931)";
                 document.getElementById("fpr").style.borderLeft = "20px solid transparent";
                 document.getElementById("fpr").style.height = "0";
                 }
               }
             }
           }
                 }
          });
        </script>
<?php
            }

          ?>

@endsection