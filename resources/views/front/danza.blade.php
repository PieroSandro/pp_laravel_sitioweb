@extends('layouts.actividadescul')
@section('titleblog')
@lang('home.bl_dance')

@endsection

<!-- ------------------------------------------------------------------------>

@section('carusel')

<style>
#bt1car:before{
	
	content:'@lang('home.ver_slider')';
	align-content: center;
	font-size:11.1px;/*15px */
	position:absolute;
	top:50%;
	left:50%;
	transition:all 0.3s;
	transform:translate(-50%,-50%);
}
</style>
     <ul id="sb-slider" class="sb-slider" style="max-height:650px; width: 100%; margin: auto;">
           
           
           <?php
               use App\Banner;
               $banners=Banner::all();
               $counter1=1;
               foreach($banners as $banner){
                   ?>
 
 
 
 <li style="max-height:650px; width:100%;">
                         <a href="{{$banner->link}}" target="_blank"><img class="d-block w-100" src="/intranet/storage/app/public/images/{{$banner->foto}}" alt="image_{{$counter1}}"/></a>
             
             
             <div class="sb-description" style="text-align:center; width:40px; height:40px; margin:0 auto;">
                             <h3 style="text-align:center; font-family: 'Montserrat', sans-serif;">{{$counter1}}</h3>
             </div>
               
           </li>
 
 
 
 <?php
 $counter1++;
               }
 
 
 
 
 
 
             ?>
 
 
 
 
 
 
           <div id="nav-arrows" class="nav-arrows">
                     <a href="#">Next</a>
                     <a href="#">Previous</a>
                 </div>
         </ul>
              @endsection


<!----------------------------------------------------------------------------->

@section('content')

<!-------------------------POPOVER---------------------------------->

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<!----------------------POPOVER------------------------------------->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<link rel="stylesheet" href="{{url('/css/animate.css')}}">


<!--------------ANIMATE ON SCROLL------------------------->
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        
        <script>
        AOS.init({
          duration:3000,
        });
        </script>
        <!------------------------------------------->
<br>
<!-- 29_01-->
<label id="qs"></label>

<br>
<br>
<!-- 29_01-->

<div class="text">
<div data-aos="fade-right" style="font-family: Montserrat, sans-serif; padding-top: 40px;
  padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">

<!--<h3>@lang('home.bl_dance')</h3>-->
<h1 style="color: black; font-family: Montserrat, sans-serif; width:100%;">@lang('home.bl_dance')</h1>

<!-----------------------------------29_01------------------------------------------------->

<?php

$locale=App::getLocale();
          

            if(App::isLocale('en')){
              ?>
         
        
         @if(count($mainpages)>0)
            @foreach($mainpages as $mainpage)
            <div id="txt4" style="text-align: justify; font-family: Montserrat, sans-serif; width:100%;">

            <p style="text-align: justify; font-family: Montserrat, sans-serif;">{!!$mainpage->body_en!!}</p>
            </div>
            <br><br>
            <center>
            <img src="/intranet/storage/app/public/images/{{$mainpage->foto_en}}" id="img1"/></center>           
            @endforeach

            {{$mainpages->links()}}
            @else
              @endif
                <?php    
                        
          }else{
            ?>
             
             @if(count($mainpages)>0)
            @foreach($mainpages as $mainpage)
            <div id="txt4" style="text-align: justify; font-family: Montserrat, sans-serif; width:100%;">

            <p style="text-align: justify; font-family: Montserrat, sans-serif;">{!!$mainpage->body_es!!}</p>
          </div>
           <br><br>
           <center>
           <img src="/intranet/storage/app/public/images/{{$mainpage->foto_es}}" id="img1"/></center>         
            @endforeach

            {{$mainpages->links()}}
            @else
              @endif
         
              <?php
            }
?>
<!------------------------------------------------------------------------------------>
</div>
</div>
 
<script>
    $( document ).ready(function() {
      function Scrolldown() {
     
     document.getElementById("qs").scrollIntoView();
}

window.onload = Scrolldown;
    });

    $( window ).on( "load", function() {
      function Scrolldown() {
     
     document.getElementById("qs").scrollIntoView();
}

window.onload = Scrolldown;
    });
 
   
    </script>
@endsection

<!-------------------------------------------------------------->

@section('content2')

<?php

$postcomentados=array();
$postcantidad=array();
$linkcomentados=array();

$py=array();
$arr='';
$ct=0;

use App\Post;
      $posts=Post::all();
//use App\Post;
//$posts=Post::all();
foreach($posts as $post){

$titulopost='';
$linkpost='';
$fpo='';
$counter = 0;
foreach($post->comentarios as $comentario){
    $counter++;
    } //$titulopost=$post->titulo;

    $pbody=$post->body;
    //----------------------------------------------------------------------------
    
$extract=substr(htmlspecialchars(trim(strip_tags($pbody))), 0, 150);



$pbody1=htmlspecialchars(trim(strip_tags($pbody)));
//$extract=mb_strimwidth($bodypost, 1, 90, "...");
if(strlen($pbody)>151){

  $locale=App::getLocale();
//echo $extract, PHP_EOL;
if(App::isLocale('en')){
$pbo=htmlspecialchars_decode($extract, ENT_NOQUOTES).'... <small><a href="/intranet/public/blog/home/'.$post->id.'" style="text-decoration: none; color: blue;">
Read more</a></small>';
}else{
  $pbo=htmlspecialchars_decode($extract, ENT_NOQUOTES).'... <small><a href="/intranet/public/blog/home/'.$post->id.'" style="text-decoration: none; color: blue;">
Leer más</a></small>';
}
/*echo "... ";?>
<small><a href="/intranet/public/blog/home/{{$post->id}}" style="text-decoration: none; color: blue;">
@lang('home.bl_lmas')</a></small>
<?php*/
}
else{
 $pbo=htmlspecialchars_decode($pbody1, ENT_NOQUOTES);
}

//-------------------------------------------------------------
$fpost=$post->foto;
$fpo='';
if($fpost=='noimage.jpg'){  
$fpo='';
}else{



$fpo='/intranet/storage/app/public/images/{{$post->foto}}';

}

//$fpo="<img src='/intranet/storage/app/public/images/".$post->foto."' />";

$tpost="<div style='color:blue'>".$post->titulo."</div>";

    //-----------------------------------------------------------------------------
    $linkpost="
    <style>
   .popover {
    
    color: black;
    text-align:justify;
    background-color: white;
  }
  
  .popover-title {
    background-color: coral; 
    color: coral; 
    font-size: 40px;
    text-align:center;
  }
 
  .popover-content {
    background-color: green;
    color: blue;
    padding: 25px;
  }
  
  .arrow {
    border-right-color: red !important;
  }
   
   
   </style>
    <a href='/intranet/public/blog/home/".$post->id."'
    title='".$post->titulo."' data-by='".$pbo."' data-img='/intranet/storage/app/public/images/".$post->foto."' class='popi' data-toggle='popover' data-placement='bottom' style='text-decoration: none; color: blue; font-family: Montserrat, sans-serif;'>".$post->titulo."</a>

   ";?>
   
<?php
    $linkcomentados[]=$linkpost;
    //$postcomentados[]=$titulopost;
    $postcantidad[]=$counter;

$ct++;
}



for($i=0;$i<$ct;$i++){   
$py[$linkcomentados[$i]]=$postcantidad[$i];

}

arsort($py);?>

<div style="text-align:center;">
<div class="plustar1" id="plustar2"><div style="margin:10px; padding-left:10px; padding-right:10px; padding-top:2px; text-align:center; float: center; color:white; font-family: 'Montserrat', sans-serif;"><strong><i class="fas fa-star plustar" style="color:yellow"></i>  @lang('home.bl_gencoment')  <i class="fas fa-star plustar" style="color:yellow"></i></strong></div></div>
</div>
<!--@lang('home.bl_gencoment')-->


<?php
echo "<br>";
echo "<br>";
$i=0; 
foreach($py as $x => $x_value) {?>
  <label style="font-family: Montserrat, sans-serif;">
  <b>
	@lang('home.bl_title')
  </b>
	</label>
	<?php
	
    echo $x."&nbsp<label style='font-family: Montserrat, sans-serif;'><b>|</b></label>&nbsp"?><label style="font-family: Montserrat, sans-serif;"><b>@lang('home.bl_comments') </b></label>&nbsp<?php 
    echo "<label style='font-family: Montserrat, sans-serif;'>";
    echo $x_value;
    echo "</label>";
		echo "<br>";
		 
		if($i==9){
			break;
		}
	
		else{
		$i++;}
  }
  
  if($i==0){?>
    @lang('home.bl_nopost')
    <?php }


//--------------------------------
/*$age = array("Ben"=>"37", "Peter"=>"35", "Joe"=>"43");
print_r($age);
arsort($age);

foreach($age as $x => $x_value) {
    echo "Key=" . $x . ", Value=" . $x_value;
    echo "<br>";
}*/
//------------------------------------------------


$counter1 = 0;
$counter2= 0;
foreach($posts as $post){
    $counter1++;
      if((time() - strtotime($post->created_at)) <= 86400)
      {
          $counter2++;
      }
		}

$counter1;
$counter2;



?>

<script>

   $('.popi').popover({
    delay: 1150,
          //trigger: 'focus',
		  trigger: 'hover',
          html: true,
          content: function () {

            if($(this).data('img')=='/intranet/storage/app/public/images/noimage.jpg')
            {
            return $(this).data("by");
            }else{
              
				return $(this).data("by")+'<br><br><img class="img-fluid" src="'+$(this).data('img') + '" />';
            }
          },
          title: 'Toolbox'
    });   
 
 </script>
 <script>



          $(function(){

            
            
            $("#NumPosts").html("Total: <?php echo $counter1 ?>");

            var c2 = "<?php echo $counter2 ?>";
            if(c2>0){   
            $("#NewPosts").html(" @lang('home.bl_newpost2') <i class='fas fa-file comicon2' style='color:white'></i> : <?php echo $counter2 ?>");
            }else{
              $("#NewPosts").hide();
            }

            $("#dance_id2").hide();

            $("#danza_id").mouseover(function(){
              $("#dance_id1").hide();
              $("#dance_id2").show();
                });

            $("#danza_id").mouseout(function(){
              $("#dance_id2").hide();
              $("#dance_id1").show();
                });
            
              
          
          });
        </script>
@endsection