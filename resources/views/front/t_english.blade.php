@extends('layouts.master2')
@section('title')
@lang('home.testenglish_menu')
@endsection
@section('content')

<link rel="stylesheet" href="{{url('/css/animate.css')}}">


<!--------------ANIMATE ON SCROLL------------------------->
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        
        <script>
        AOS.init({
          duration:3000,
        });
        </script>
        <!------------------------------------------->

<style>
.thecard{
	
	width:250px;
	height:320px;
	transform-style:preserve-3d;
	transition:all 0.5s ease;
	
}

.thecard:hover{
	transform:rotateY(180deg);
}

.tfront{
	position:absolute;
	width:250px;
	height:320px;
	backface-visibility:hidden;
	background: url('images/cardf1.jpg')no-repeat;
	color:#333;
	text-align:center;
	border-radius:20px;
}

#text1{
	padding-top: 50%;
	align-items:center;/* 4s*/
	/*-webkit-animation:spin 14s linear infinite;
    -moz-animation:spin 14s linear infinite;
    animation:spin 14s linear infinite;*/
    /*-webkit-animation-delay:2s;*/


}


.tback{
	position:absolute;
	width:250px;
	height:320px;
	backface-visibility:hidden;
	
	background:url('images/cardf2.jpg')no-repeat;
	color:#333;
	text-align:center;
	border-radius:20px;
	transform:rotateY(180deg);
}

.formpar{
    
    vertical-align: middle;
  border-bottom: 35px solid rgba(228, 20, 20, 0.931);
  border-left: 20px solid transparent;
  height: 0;
  
  box-shadow: 5px 0px white, 10px 0px rgba(26, 26, 161, 0.993), 15px 0px white, 20px 0px rgba(26, 26, 161, 0.993), 25px 0px white, 30px 0px rgba(26, 26, 161, 0.993);
  transform: skew(30deg);
  

}
</style>


<label id="qs6">

<br>
<br>

<div class="text">
<div data-aos="fade-right" style="padding-top: 40px;
  padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">





  
  <!------------------------------------------------------------------------------------>

  <?php

$locale=App::getLocale();
          

            if(App::isLocale('en')){
              ?>
         
        
         @if(count($mainpages)>0)
            @foreach($mainpages as $mainpage)
           
            <center>
            <img src="/intranet/storage/app/public/images/{{$mainpage->foto_en}}" id="img1"/></center>           
            @endforeach

            {{$mainpages->links()}}
            @else
              @endif
                <?php    
                        
          }else{
            ?>
             
             @if(count($mainpages)>0)
            @foreach($mainpages as $mainpage)
           
           <center>
           <img src="/intranet/storage/app/public/images/{{$mainpage->foto_es}}" id="img1"/></center>         
            @endforeach

            {{$mainpages->links()}}
            @else
              @endif
         
              <?php
            }
?>
<!------------------------------------------------------------------------------------>













  <!--
	<center>
  	<img src="images/Engligh_class.jpg" class="imgCenter rounded-circle" id="img1"/></center>
-->




<br>
<br>
<div class="formpar" id="fpr"><h3><div style="transform: skewX(-45deg); color: white; font-family: 'Montserrat', sans-serif;">@lang('home.testenglish_menu')</div></h3></div>
        <!--<h3>@lang('home.testenglish_menu')</h3>--><br>
       <!--------------------------------------------------------- @lang('home.tenglish_cont')-->
		
<!------------------------------------------------------------------------------------>

<?php

$locale=App::getLocale();
          

            if(App::isLocale('en')){
              ?>
         
        
         @if(count($mainpages)>0)
            @foreach($mainpages as $mainpage)
            <div id="txt4" style="text-align: justify; font-family: Montserrat, sans-serif; width:100%;">
            <p style="text-align: justify; font-family: Montserrat, sans-serif;">{!!$mainpage->body_en!!}</p>
            </div>
            <br><br>
            @endforeach

            {{$mainpages->links()}}
            @else
              @endif
                <?php    
                        
          }else{
            ?>
             
             @if(count($mainpages)>0)
            @foreach($mainpages as $mainpage)
            <div id="txt4" style="text-align: justify; font-family: Montserrat, sans-serif; width:100%;">
            <p style="text-align: justify; font-family: Montserrat, sans-serif;">{!!$mainpage->body_es!!}</p>
           </div>
           <br><br>
            @endforeach

            {{$mainpages->links()}}
            @else
              @endif
         
              <?php
            }
?>
<!------------------------------------------------------------------------------------>

		<br>
		<br>
		<center>
		<!--<div class="thecard">

			<div class="tfront">
				<div id="text1">
				<p>Nivel básico! Comienza a jugar</p>
				</div>
			</div>

			<div class="tback">back of card
				<a class="btn animated fadeInLeft" href="https://elcomercio.pe/" id="bt1car" target="_blank" role="button">
                                
                               </a>

			</div>

		</div>-->
		<br>
		


		<a class="btn btn-primary" href="{{ url('/t_english/basico') }}" style="font-family: Montserrat, sans-serif; border:none; background-color:rgba(228, 20, 20, 0.931);" target="_blank" role="button">@lang('home.te_basico')</a>
		<br><br>
		<a class="btn btn-primary" href="{{ url('/t_english/intermedio') }}" style="font-family: Montserrat, sans-serif; border:none; background-color:rgba(228, 20, 20, 0.931);" target="_blank" role="button">@lang('home.te_intermedio')</a>
		<br><br>
		<a class="btn btn-primary" href="{{ url('/t_english/intermedio_2') }}" style="font-family: Montserrat, sans-serif; border:none; background-color:rgba(228, 20, 20, 0.931);" target="_blank" role="button">@lang('home.te_intermedio2')</a><br><br>


		
	</div>
	</div>
	</center>
		<br>
		<br>
		
</label>

<script>
    $( document ).ready(function() {
      function Scrolldown() {
     //window.scroll(0,562); 
     document.getElementById("qs6").scrollIntoView();
}

window.onload = Scrolldown;
    });

    $( window ).on( "load", function() {
      function Scrolldown() {
     
     document.getElementById("qs6").scrollIntoView();
}

window.onload = Scrolldown;
    });
 
   
    </script>
		<script>
          $(function(){
            
                $(window).resize(function(){
                    winSize();
               });

               function winSize(){
               
        if ($(window).width() < 768) {
          document.getElementById("fpr").style.width = "100%";
          
        }
    
    else{
      if ($(window).width() < 992) {
      document.getElementById("fpr").style.width = "100%";
     
      }else{
        if($(window).width() < 1200){
          document.getElementById("fpr").style.width = "50%";
          
        }else{
          document.getElementById("fpr").style.width = "50%";
         
        }
      }
    }
          }
           

               $(window).show(function(){
                    win2();
               });

               function win2(){
               
        if ($(window).width() < 768) {
          document.getElementById("fpr").style.width = "100%";
          
        }
    
    else{
      if ($(window).width() < 992) {
      document.getElementById("fpr").style.width = "100%";
      
      }else{
        if($(window).width() < 1200){
          document.getElementById("fpr").style.width = "50%";
          
        }else{
          document.getElementById("fpr").style.width = "50%";
          
        }
      }
    }
          }
          });
        </script>
@endsection