@extends('layouts.master2_2')
@section('title')
@lang('home.revistas_menu')
@endsection
@section('content')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<link rel="stylesheet" href="{{url('/css/animate.css')}}">


<!--------------ANIMATE ON SCROLL------------------------->
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        
        <script>
        AOS.init({
          duration:3000,
        });
        </script>
        <!------------------------------------------->

<style>
@media screen and (max-width: 575px){
  
  .cnb{
	
	order:1;
  }

  .cna{
	order:2;
  }
  
}

@media screen and (min-width: 768px) and (max-width: 991px){
  
  .cnb{
	
	order:1;
  }

  .cna{
	order:2;
  }
  
}


.thecard{
	
	width:250px;
	height:320px;
	transform-style:preserve-3d;
	transition:all 0.5s ease;
	
}

.thecard:hover{
	transform:rotateY(180deg);
}

.tfront{
	position:absolute;
	width:250px;
	height:320px;
	backface-visibility:hidden;
	background: url('images/cardf1.jpg')no-repeat;
	color:#333;
	text-align:center;
	border-radius:20px;
}

#text1{
	padding-top: 50%;
	align-items:center;/* 4s*/



}


.tback{
	position:absolute;
	width:250px;
	height:320px;
	backface-visibility:hidden;
	
	background:url('images/cardf2.jpg')no-repeat;
	color:#333;
	text-align:center;
	border-radius:20px;
	transform:rotateY(180deg);
}

.formpar{
    
    vertical-align: middle;
  border-bottom: 35px solid rgba(228, 20, 20, 0.931);
  border-left: 20px solid transparent;
  height: 0;
  
  box-shadow: 5px 0px white, 10px 0px rgba(26, 26, 161, 0.993), 15px 0px white, 20px 0px rgba(26, 26, 161, 0.993), 25px 0px white, 30px 0px rgba(26, 26, 161, 0.993);
  transform: skew(30deg);
  

}

/*19_12---------------------------------------------------------------------------------------- */
.conew1{
	/*border: none;
	outline: none;*/
	background: rgba(4, 10, 100, 0.808);
	min-width: 120px;
	min-height: 35px;
	border-radius: 20px;
	/*cursor: pointer;
	margin: 10px;*/
}


@-webkit-keyframes conew1 {
  to {
    -webkit-transform: scale(1.03);
    transform: scale(1.03);/* 1.1*/
  }
}
@keyframes conew1 {
  to {
    -webkit-transform: scale(1.03);
    transform: scale(1.03);
  }
}

#NumPosts{
  /*background: rgba(4, 10, 100, 0.808);*/
  background: rgba(228, 20, 20, 0.931);
	min-width: 120px;
	min-height: 35px;
	border-radius: 20px;

}

.NewPosts2{
  background: rgba(4, 10, 100, 0.808);
	min-width: 120px;
	min-height: 35px;
	border-radius: 20px;
}



@-webkit-keyframes NewPosts2 {
  to {
    -webkit-transform: scale(1.03);
    transform: scale(1.03);/* 1.1*/
  }
}
@keyframes NewPosts2 {
  to {
    -webkit-transform: scale(1.03);
    transform: scale(1.03);
  }
}

.NewPosts2 {
  
  -webkit-transform: perspective(1px) translateZ(0);
  transform: perspective(1px) translateZ(0);
  box-shadow: 0 0 1px rgba(0, 0, 0, 0);

  -webkit-animation-name: NewPosts2;
  animation-name: NewPosts2;
  -webkit-animation-duration: 0.3s;
  animation-duration: 0.3s;
  -webkit-animation-timing-function: linear;
  animation-timing-function: linear;
  -webkit-animation-iteration-count: infinite;
  animation-iteration-count: infinite;
  -webkit-animation-direction: alternate;
  animation-direction: alternate;
}

.conew1 {
  display: inline-block;
  vertical-align: middle;
  -webkit-transform: perspective(1px) translateZ(0);
  transform: perspective(1px) translateZ(0);
  box-shadow: 0 0 1px rgba(0, 0, 0, 0);

  -webkit-animation-name: conew1;
  animation-name: conew1;
  -webkit-animation-duration: 0.3s;
  animation-duration: 0.3s;
  -webkit-animation-timing-function: linear;
  animation-timing-function: linear;
  -webkit-animation-iteration-count: infinite;
  animation-iteration-count: infinite;
  -webkit-animation-direction: alternate;
  animation-direction: alternate;
}

</style>

<label id="qs"></label>

<br>
<br>

<div class="text">
<div data-aos="fade-right" style="padding-top: 40px;
  padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">

    <div class="formpar" id="fpr"
    ><h3><div style="transform: skewX(-45deg); color: white; font-family: 'Montserrat', sans-serif;">@lang('home.revistas_menu')
</div></h3></div>
     <!--CURSOS ONLINE-->
     <br>

     <?php
      $counter = 1;
      //$result = $counter%2;
        ?>
  @if(count($revistas)>0)
@foreach($revistas as $revista)
	<div class="card">
    <div style="padding-top: 20px;padding-right: 20px;
  padding-bottom: 20px;
  padding-left: 20px;">


@if((time() - strtotime($revista->created_at)) <= 86400)

<div style="padding-top: 0px;
padding-bottom: 20px;">
<div class="conew1"><div style="padding-left:10px; padding-right:10px; padding-top:5px; text-align:center; float: center; color:white; font-family: 'Montserrat', sans-serif;"><strong>@lang('home.bl_newcourse') <i class="fas fa-user-edit comicon2" style="color:white"></i></strong></div></div>
</div>
@else

@endif

<h3 style="text-align: center; font-family: 'Montserrat', sans-serif;">
<a href="{{$revista->link}}" target="_blank" 
onMouseOver="this.style.cssText='color: rgba(228, 20, 20, 0.931); text-decoration:none;'" 
onMouseOut="this.style.cssText='color: rgb(11, 11, 132, 0.993)'" 
style="text-decoration: none; color: rgb(11, 11, 132, 0.993);">{{$revista->titulo}}</a></h3>
<br>
		<?php

if ($counter % 2 == 1){ 
      ?>
      

        
        
        <div class="row">
        <div class="col-12 col-sm-6 col-md-12 col-lg-6">
       





        <div id="txt4" style="text-align: justify; font-family: Montserrat, sans-serif; width:100%;">

              <p style="text-align:justify; font-family: 'Montserrat', sans-serif;">


              <?php
$bodyvcurso='';
$bodyvcurso=$revista->body;

$bodyvcurso1 = html_entity_decode($bodyvcurso);




echo $bodyvcurso1;

?>


</p>
      </div>  
        </div>
        
        <div class="col-12 col-sm-6 col-md-12 col-lg-6">
        <center>
        <div>
         <style type="text/css">
           #content1, #content2 {
          width: 100%;
          height: 100%;
          margin: 0;
          padding: 0;
      }
      iframe{
          position: relative;

          /* */
          width: 100%;
          
          margin: 0;
          padding: 0;
      }
        </style>
        
        <div id="content1">
        <div id="content2">
        <iframe src="{{$revista->link}}" frameborder="0" height="350px" allowfullscreen></iframe>
        </div>
        </div>
</div>
</center>
        </div>
       
        </div><?php
}else{?>
<!-------------------------------------------------------------------------------->
<div class="row">
<div class="col-12 col-sm-6 col-md-12 col-lg-6 cna">
        <center>
        <div>
        <style type="text/css">
           #content1, #content2 {
          width: 100%;
          height: 100%;
          margin: 0;
          padding: 0;
      }
      iframe{
          position: relative;

          /* */
          width: 100%;
          
          margin: 0;
          padding: 0;
      }

      
        </style>
        
        <div id="content1">
        <div id="content2">
        <iframe src="{{$revista->link}}" frameborder="0" height="350px" allowfullscreen></iframe>
        </div>
        </div>
</div>
</center>
        </div>

        <div class="col-12 col-sm-6 col-md-12 col-lg-6 cnb">
        <div id="txt4" style="text-align: justify; font-family: Montserrat, sans-serif; width:100%;">

        <p style="text-align:justify; font-family: 'Montserrat', sans-serif;">


<?php
$bodyvcurso='';
$bodyvcurso=$revista->body;

$bodyvcurso1 = html_entity_decode($bodyvcurso);




echo $bodyvcurso1;

?>


</p>
       </div> 
        </div>
        
        
       
        </div>




<?php }
?>
        
        
<br>
        
      </div>  
	</div>
	<br>
  <?php
                        
                        $counter++;
                      
                      ?>
@endforeach
	{{$revistas->links('vendor.pagination.bootstrap-4')}}
@else
<p>@lang('home.bl_nopost')</p>
@endif
<?php
                        
                        
                      
                      ?>
</div>
</div>
		<br>
    
    <!--<label id="math"></label>-->
<!---------------PODRIA BORRARSE EL PRIMER SCRIPT DE ABAJO-------------------------------->
<script>
    $(document).ready(function() {
    function checkWidth() {
      
        var windowSize = $(window).width();

        if (windowSize <= 700) {
          $("#math").html('jite');
        }
        else{
          $("#math").html('manu');
        }
    }

   
    checkWidth();
    
    $(window).resize(checkWidth);
});
</script>
    
    
<script>
    $( document ).ready(function() {
      function Scrolldown() {
     
     document.getElementById("qs").scrollIntoView();
}

window.onload = Scrolldown;
    });

    $( window ).on( "load", function() {
      function Scrolldown() {
     
     document.getElementById("qs").scrollIntoView();
}

window.onload = Scrolldown;
    });
 
   
    </script>
	 <script>
          $(function(){
            
                $(window).resize(function(){
                    winSize();
               });

               function winSize(){
               
        if ($(window).width() < 768) {
          document.getElementById("fpr").style.width = "100%";
          
        }
    
    else{
      if ($(window).width() < 992) {
      document.getElementById("fpr").style.width = "100%";
     
      }else{
        if($(window).width() < 1200){
          document.getElementById("fpr").style.width = "50%";
          
        }else{
          document.getElementById("fpr").style.width = "50%";
         
        }
      }
    }
          }
           

               $(window).show(function(){
                    win2();
               });

               function win2(){
               
        if ($(window).width() < 768) {
          document.getElementById("fpr").style.width = "100%";
          
        }
    
    else{
      if ($(window).width() < 992) {
      document.getElementById("fpr").style.width = "100%";
      
      }else{
        if($(window).width() < 1200){
          document.getElementById("fpr").style.width = "50%";
          
        }else{
          document.getElementById("fpr").style.width = "50%";
          
        }
      }
    }
          }
          });
        </script>  
@endsection