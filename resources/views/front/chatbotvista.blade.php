@extends('layouts.master2')
@section('title')
@lang('home.contactenos_menu')
@endsection
@section('content')

<style>


.swal2-modal .swal2-styled{
    
    background-color: rgba(4, 10, 100, 0.808);
}
</style>
<div class="row">
    <div class="col-12 col-sm-8 col-md-12 col-lg-8 col-xl-8 offset-sm-2 offset-md-0 offset-lg-2 offset-xl-2">
        <br>
        <h1><div style="text-align: center; font-family: 'Montserrat', sans-serif;">@lang('home.contactenos_menu')</div></h1>
<br>
        
       
        <form class="sf" id="submit_form" action="{{route('contactenos.send')}}" method="POST" enctype="multipart/form-data">
            @csrf
            @if(session('success'))
                

            @endif
            <!-- 6_2_2020-->
            <p style="text-align:right;" id="demo_a"></p>
            <br>
            <p style="text-align:left;" id="demo"></p>
            <br>
            <label id="textadded"> 
            </label>
                       <!-- 6_2_2020-->

<div class="form-group">

<label for="message" class="mt-3">@lang('home.mensaje_contactenos')</label>

<textarea class="form-control" cols="30" rows="10" name="message" id="message" style="box-shadow: 0 0 0px;"></textarea>
            
                  <br>
            
                  <label id="lblMessage" style="color:red"></label>
                  <label id="lblMessage2" style="color:green"></label>
        </div>
            
        <div style="text-align: center;">
            <input type="submit" class="btn btn-primary center-block" style="background-color:rgba(4, 10, 100, 0.808); border:none;" name="submit" id="submit" value="@lang('home.boton_contactenos')">
        </div>
            <span id="error_message" class="text-danger"></span>
            <span id="success_message" class="text-success"></span>
        
        
        </form>
     
       <br>
        <br>
        

    </div>


    <script>
            $(function(){
                
                $("#lblMessage").hide();
                var message=false;
                var i_message=0;
              
                var count=0;
                
                var a_message='';
              
                var s_message='';

//-------------------------------------------------
              

                $("#message").focusout(function(){
                    check_message2();
                });

              
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                function check_message2(){

                    var count_2=0;
                    var message_contem=$("#message").val();
                  
                    var message_contem_lc=message_contem.toLowerCase();
                    <?php
              use App\Chatbot;
            
              $chatbots=Chatbot::all();

              foreach($chatbots as $chatbot){
                  ?>
                    if(message_contem_lc.includes("<?php echo $chatbot->palabraclave; ?>")){
                        message=true;
                   i_message++;
                   //7-2-2020
                   a_message=message_contem;
                   count++;
                   count_2++;
                   //7-2-2020
                   s_message="<?php echo $chatbot->respuesta; ?>";

                    }else{
                       
                    }
               
                    <?php

              }

            ?>

              if(count_2==0){
                   message=true;
                   i_message++;
                   a_message=message_contem;
                   count++;
                   s_message="Contáctese con nosotros al correo pronto!!";

              
              }



                }


               

            $("#submit_form").submit(function(e){

              
                    message=false;
                  
                    i_message=0;
                      //new 7_2_2020------
                 a_message='';
                //new 7-2-2020------
                    s_message='';

                  
                    check_message2();

                    

                    if(message==false){
                        e.preventDefault();

                        
                        $.ajax({
                            url:"{{route('contactenos.send')}}",
                            type:"post",
                            //data:{name:name,email:email,message:message},
                            data: $('form.sf').serialize(),
                            success:function(data){
                               // console.log('ok');
                                Swal.fire({
                                    type: 'success',
  
  title:'@lang('home.cf_bresponse')',
  html: '<i class="fas fa-check-circle" style="color:green"></i> @lang('home.cf_bresponse1') <i class="fas fa-check-circle"  style="color:green"></i>'+
    '<br><br><div class="alert alert-success" role="alert">'+'@lang('home.cf_bresponse2'): '+$('#name').val()+'<br>'+'@lang('home.cf_bresponse3'): '+$('#email').val()+'</div>',
  animation: false,
  customClass: {
    popup: 'animated tada'
  }
})
                            }
                        });
                    }else{
                        var n=i_message;
                        //--7_2_2020
                        var at=a_message;
                        //--7_2_2020
                        var st=s_message;
                        if(count==1){  
                            //7_2_2020
                            $("#demo_a").html(at);
                            //7_2_2020
                            $("#demo").html(st);

                          
}else{
    //at
    //st
    $('#textadded').append('<p style="text-align:right;">'+at+
            '</p><br>'+
            '<p style="text-align:left;">'+st+
            '</p><br>');


}
document.getElementById("message").value="";     
$("#message").focus();
return false;
                    }

                   
                });

               

        });
           
				
				
			</script>
    

</div>

<script>

  


</script>
<script>
$(function(){
    document.getElementsByClassName("swal2-confirm swal2-styled").setAttribute("id", "jojo");

});
</script>
@endsection