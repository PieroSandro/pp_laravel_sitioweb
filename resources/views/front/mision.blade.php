@extends('layouts.master2')
@section('title')
@lang('home.mision_index')

@endsection




@section('content')
<!--------------ANIMATE ON SCROLL------------------------->
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        
        <script>
        AOS.init({
          duration:3000,
        });
        </script>
        <!------------------------------------------->
<style>
#img33
{
	
	width:70%;
	/*max-width:600px;
	min-width:300px;*/
	border-radius: 20%;
	
	
}

.formpar{
    
    vertical-align: middle;
  border-bottom: 35px solid rgba(228, 20, 20, 0.931);
  border-left: 20px solid transparent;
  height: 0;
  
  box-shadow: 5px 0px white, 10px 0px rgba(26, 26, 161, 0.993), 15px 0px white, 20px 0px rgba(26, 26, 161, 0.993), 25px 0px white, 30px 0px rgba(26, 26, 161, 0.993);
  transform: skew(30deg);
 
}
</style>

<label id="qs2">
<br>
<br>

<div class="text">
<div data-aos="fade-right" style="padding-top: 40px;
  padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">

      <div class="formpar" id="fpr"><h3><div style="transform: skewX(-45deg); color: white; font-family: 'Montserrat', sans-serif;">@lang('home.mision_index')</div></h3></div>
     <br>
     <!-- @lang('home.mision_content')
      <br><br>

      <center>
  	<img src="images/Engligh_class.jpg" id="img1"/></center>-->
    <!------------------------------------------------------------------------------------>

    <?php

$locale=App::getLocale();
          

            if(App::isLocale('en')){
              ?>
         
        
         @if(count($mainpages)>0)
            @foreach($mainpages as $mainpage)
            <div id="txt4" style="text-align: justify; font-family: Montserrat, sans-serif; width:100%;">

            <p style="text-align: justify; font-family: Montserrat, sans-serif;">{!!$mainpage->body_en!!}</p>
            </div>
            <br><br>
            <center>
            <img src="/intranet/storage/app/public/images/{{$mainpage->foto_en}}" id="img1"/></center>           
            @endforeach

            {{$mainpages->links()}}
            @else
              @endif
                <?php    
                        
          }else{
            ?>
             
             @if(count($mainpages)>0)
            @foreach($mainpages as $mainpage)
            <div id="txt4" style="text-align: justify; font-family: Montserrat, sans-serif; width:100%;">

            <p style="text-align: justify; font-family: Montserrat, sans-serif;">{!!$mainpage->body_es!!}</p>
            </div>
           <br><br>
           <center>
           <img src="/intranet/storage/app/public/images/{{$mainpage->foto_es}}" id="img1"/></center>         
            @endforeach

            {{$mainpages->links()}}
            @else
              @endif
         
              <?php
            }
?>
<!------------------------------------------------------------------------------------>
</div>
	</div>	
        
     <br>   
        
     </label> 
        
        
        
        
        <div class="swiper-container">
                    <div class="swiper-wrapper">
                      
                      <div class="swiper-slide">
                            <div class="imgBx">
                                <img src="images/en1.jpg">
                            </div>
                            <div class="details">
                                <h3 style="font-family: 'Montserrat', sans-serif;">John Doe<br><span>Web Designer</span></h3>
                            </div>
                      </div>
                      <div class="swiper-slide">
                            <div class="imgBx">
                                    <img src="images/en2.jpg">
                                </div>
                                <div class="details">
                                    <h3 style="font-family: 'Montserrat', sans-serif;">John Doe<br><span>Web Designer</span></h3>
                            </div>
                      </div>
                      <div class="swiper-slide">
                            <div class="imgBx">
                                    <img src="images/en3.jpg">
                                </div>
                                <div class="details">
                                    <h3 style="font-family: 'Montserrat', sans-serif;">John Doe<br><span>Web Designer</span></h3>
                                </div>
                      </div>
                      <div class="swiper-slide">
                            <div class="imgBx">
                                    <img src="images/en4.jpg">
                                </div>
                                <div class="details">
                                    <h3 style="font-family: 'Montserrat', sans-serif;">John Doe<br><span>Web Designer</span></h3>
                                </div>
                      </div>
                      <div class="swiper-slide">
                            <div class="imgBx">
                                    <img src="images/en5.jpg">
                                </div>
                                <div class="details">
                                    <h3 style="font-family: 'Montserrat', sans-serif;">John Doe<br><span>Web Designer</span></h3>
                                </div>
                      </div>    
                    </div>
                    </div>
                    
                    <br>
                    <br>

                    
                    <!-------------------------------------------------->


                     

                    <script>
    $( document ).ready(function() {
      function Scrolldown() {
     //window.scroll(0,562); 
     document.getElementById("qs2").scrollIntoView();
}

window.onload = Scrolldown;
    });

    $( window ).on( "load", function() {
      function Scrolldown() {
     //window.scroll(0,562); 
     document.getElementById("qs2").scrollIntoView();
}

window.onload = Scrolldown;
    });
 
    /*$( window ).on( "load", function() {
        console.log( "window loaded" );
    });*/
    </script>

                    <script>
          $(function(){
            
                $(window).resize(function(){
                    winSize();
               });

               function winSize(){
               
        if ($(window).width() < 768) {
          document.getElementById("fpr").style.width = "100%";
          
        }
    
    else{
      if ($(window).width() < 992) {
      document.getElementById("fpr").style.width = "100%";
     
      }else{
        if($(window).width() < 1200){
          document.getElementById("fpr").style.width = "50%";
          
        }else{
          document.getElementById("fpr").style.width = "50%";
         
        }
      }
    }
          }
           

               $(window).show(function(){
                    win2();
               });

               function win2(){
               
        if ($(window).width() < 768) {
          document.getElementById("fpr").style.width = "100%";
          
        }
    
    else{
      if ($(window).width() < 992) {
      document.getElementById("fpr").style.width = "100%";
      
      }else{
        if($(window).width() < 1200){
          document.getElementById("fpr").style.width = "50%";
          
        }else{
          document.getElementById("fpr").style.width = "50%";
          
        }
      }
    }
          }
          });
        </script>  

@endsection