@extends('layouts.mainblog')
@section('titleblog')

@lang('home.createbanners')

@endsection


<!---->
@section('carusel')

<!--------12_01_2020-------->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<link rel="stylesheet" href="{{url('/css/animate.css')}}">







<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        
        <script>
        AOS.init({
          duration:3000,
        });
        </script>





<!------------------->

@endsection



@section('content')
<style>


.swal2-modal .swal2-styled{
  background-color: rgba(11, 11, 132, 0.993);
    font-family: 'Montserrat', sans-serif;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js">
</script>
<div class="text">
<div style="padding-top: 40px;
  padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<a href="{{route('backbannerhome')}}" class="btn" style="color: white; border: rgba(228, 20, 20, 0.931); background-color:rgba(228, 20, 20, 0.931) ;font-family: Montserrat, sans-serif;"><i class="fas fa-angle-left"></i> @lang('home.gobanners')</a>
<br><br>
<h1 style="color: black; font-family: Montserrat, sans-serif; width:100%;">@lang('home.createbanners')</h1>
<br>
{!! Form::open(['action'=> 'HomeController@storebanner', 'method'=> 'POST','enctype'=>'multipart/form-data']) !!}

@csrf

@if(session('success'))
 
<?php
           
           $locale=App::getLocale();

           if(App::isLocale('en')){
             ?>
        
        <script>

Swal.fire({
 
 html: '<i class="fas fa-info-circle" style="color:rgba(11, 11, 132, 0.993); font-size:150px;"></i>'+
 '<br><br><br><div style="font-family: Montserrat, sans-serif;">{!!session('success')!!}</div>',
 
 showConfirmButton: true,
 })   
          
</script>
               <?php    
                       
         }else{?>
            <script>

Swal.fire({
 
 html: '<i class="fas fa-info-circle" style="color:rgba(11, 11, 132, 0.993); font-size:150px;"></i>'+
 '<br><br><br><div style="font-family: Montserrat, sans-serif;">{!!session('success')!!}</div>',
 
 showConfirmButton: true,
 })   
          
</script>
           
           <?php
           }

         ?>

            @endif

    <span id="result"></span>
            
        <br>

         <!-----------------------CREATEVCURSOFORM---------------------------------------------------------------------------------->
         <div id="ggg">
       
       <div id="row1" class="card">
       <div style="padding-top: 20px;padding-right: 20px;
 padding-bottom: 20px;
 padding-left: 20px;">
       
       <div class="form-group" style="font-family: Montserrat;">
       <label for="foto" style="font-family: Montserrat;" class="mt-3">@lang('home.dt_foto')</label>
           <div style="text-align:center;">
       <input type="file" id="foto" accept="image/*" name="foto[1]" value="" style="width: 100%; text-align:center;"><!--id="tit"  -->
       </div>
       <br><br>
       <div style="text-align:center;"><img src="" id="image-field" width="250" height="250"></div>
       <br>
       </div>
       
       <div class="form-group" style="font-family: Montserrat;">
       <label for="link" style="font-family: Montserrat;" class="mt-3">@lang('home.linkaudios')</label>
       <!-- 14_01_2020--><input class="form-control" placeholder="" id="link" name="link[1]" type="text" value=""><br>
       
       </div>
       </div>
       </div>
       </div>
       <!--------------------------------------------------------------------------------------------------------------------->

       <br>
        <label class="col text-center">
        <a class="btn" id="addRow" style="color: white; border: rgba(11, 11, 132, 0.993); background-color:rgba(11, 11, 132, 0.993) ;font-family: Montserrat, sans-serif;"><i class="fas fa-plus-circle"></i> @lang('home.addrecord')</a>
        </label>
        &nbsp
        <label class="col text-center">
        <button type="submit" name="save" id="save" class="btn" style="color: white; border: rgba(11, 11, 132, 0.993); background-color:rgba(11, 11, 132, 0.993) ;font-family: Montserrat, sans-serif;">
<i class="fas fa-save"></i> @lang('home.saverecord')
</button>        
</label>
        {!! Form::close() !!}
    </div>
    </div>



<!-------------------------------------------------------------------------------------------->
<!-------------------------------------------------------------------------------------------->
<script>
$(function(){
  $('#link').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

//-----------------------------------------------------------
    //$(this).find('option:hover').css('background-color', 'red');
    //$(this).find('option:selected').css('background-color', 'red');
   
});


$('#link').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});
});
</script>
    <script>
    $( document ).ready(function() {
    var count=1;

    $('#addRow').on('click',function(){
    count++;
    neobanner(count);
});
    
    function neobanner(i)
    {
        var tr= '<div id="row'+i+'" class="card" style="margin-top: 20px;"><div style="padding-top: 20px;padding-right: 20px;padding-bottom: 20px;padding-left: 20px;"><div class="form-group" style="font-family: Montserrat;"><label for="foto" style="font-family: Montserrat;" class="mt-3">@lang('home.dt_foto')</label>'+        
'<div style="text-align:center;"><input id="foto'+i+'" accept="image/*" type="file" name="foto[' + i +']" value="" style="width: 100%; text-align:center;"></div><br><br>'+
                        '<div style="text-align:center;"><img src="" id="image-field'+i+'" width="250" height="250"></div>'+
                    
                    '<br>'+

'</div>'+

'<div class="form-group" style="font-family: Montserrat;">'+

        '<label for="link" class="mt-3">@lang('home.linkaudios')</label>'+

        '<input class="form-control" placeholder="" id="link'+ i +'" name="link[' + i +']" type="text" value="">'+

                   '<br>'+
           

        '</div>'
        ;



        tr+='<label class="col text-center"><a class="btn deleteRow" id="'+i+'" style="color: white; border: rgba(228, 20, 20, 0.931); background-color:rgba(228, 20, 20, 0.931) ;font-family: Montserrat, sans-serif;"><i class="fas fa-minus-circle"></i> @lang('home.deleterecord')</a></label><br></div></div>';


$('#ggg').append(tr);


$('#link'+i+'').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

//-----------------------------------------------------------
    //$(this).find('option:hover').css('background-color', 'red');
    //$(this).find('option:selected').css('background-color', 'red');
   
});


$('#link'+i+'').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});
//-------------------------script_foto_append----------------------------------------------------
var imgVal2 = $('#foto'+i+'').val(); 
        if(imgVal2=='') 
        { 
          
          $('#image-field'+i+'').attr('src','../images/nia.png').fadeIn();
        }

       

$('#foto'+i+'').change(function(){
  
  const file2 = this.files[0];
const  fileType = file2['type'];
const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
if (validImageTypes.includes(fileType)) {
    //alert('NOES');
    var file = document.getElementById('foto'+i+'').files[0];

  var readImg = new FileReader();
  
  readImg.readAsDataURL(file);
  
  readImg.onload = function(e) {
    var span = document.createElement('span');
    $('#image-field'+i+'').attr('src',e.target.result).fadeIn();
    
    }
   
}else{
  Swal.fire({
			
    html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
			'<br><br><h1 style="color: black; font-family: Montserrat, sans-serif;">@lang('home.warning_adver')</h1><br><div style="font-family: Montserrat, sans-serif;">@lang('home.warning_adver_2')</div>',
			
			showConfirmButton: false,
			timer:2000
		  })
  $('#image-field'+i+'').attr('src','../images/nia.png').fadeIn();
}
       
  
    
  })

  $('#foto'+i+'').show(function(){
    const file2 = this.files[0];
const  fileType = file2['type'];
const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
if (validImageTypes.includes(fileType)) {
  
    var file = document.getElementById('foto'+i+'').files[0];

  var readImg = new FileReader();
  
  readImg.readAsDataURL(file);
  
  readImg.onload = function(e) {
    var span = document.createElement('span');
    $('#image-field'+i+'').attr('src',e.target.result).fadeIn();
    
    }
}else{
  Swal.fire({
    html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
			'<br><br><h1 style="color: black; font-family: Montserrat, sans-serif;">@lang('home.warning_adver')</h1><br><div style="font-family: Montserrat, sans-serif;">@lang('home.warning_adver_2')</div>',
			
			showConfirmButton: false,
			timer:2000
		  })
  $('#image-field'+i+'').attr('src','../images/nia.png').fadeIn();
}
  
    
  })
//-------------------------script_foto_append----------------------------------------------------
    }
   
    $(document).on('click','.deleteRow',function(){
    var button_id=$(this).attr("id");
    $('#row'+button_id+'').remove();
});
    });
    </script>

    
<!--SCRIPT PARA LA FOTO----------------------------------------------------------------------------------->
    <script>
     $(document).ready(function(){

      var imgVal = $('#foto').val(); 
        if(imgVal=='') 
        { 
          
          $('#image-field').attr('src','../images/nia.png').fadeIn();
        }

       

$("#foto").change(function(){
  
  const file2 = this.files[0];
const  fileType = file2['type'];
const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
if (validImageTypes.includes(fileType)) {
    //alert('NOES');
    var file = document.getElementById("foto").files[0];

  var readImg = new FileReader();
  
  readImg.readAsDataURL(file);
  
  readImg.onload = function(e) {
    var span = document.createElement('span');
    $('#image-field').attr('src',e.target.result).fadeIn();
    
    }
   
}else{
  Swal.fire({
			
    html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
			'<br><br><h1 style="color: black; font-family: Montserrat, sans-serif;">@lang('home.warning_adver')</h1><br><div style="font-family: Montserrat, sans-serif;">@lang('home.warning_adver_2')</div>',
			
			showConfirmButton: false,
			timer:2000
		  })
  $('#image-field').attr('src','../images/nia.png').fadeIn();
}
       
  
    
  })

  $("#foto").show(function(){
    const file2 = this.files[0];
const  fileType = file2['type'];
const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
if (validImageTypes.includes(fileType)) {
  
    var file = document.getElementById("foto").files[0];

  var readImg = new FileReader();
  
  readImg.readAsDataURL(file);
  
  readImg.onload = function(e) {
    var span = document.createElement('span');
    $('#image-field').attr('src',e.target.result).fadeIn();
    
    }
}else{
  Swal.fire({
			
		html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
			'<br><br><h1 style="color: black; font-family: Montserrat, sans-serif;">@lang('home.warning_adver')</h1><br><div style="font-family: Montserrat, sans-serif;">@lang('home.warning_adver_2')</div>',
			
			showConfirmButton: false,
			timer:2000
		  })
  $('#image-field').attr('src','../images/nia.png').fadeIn();
}
  
    
  })
        
  
})
</script>
<!--SCRIPT PARA LA FOTO------------------------------------------------------------------->
<!--
<script>
    $( document ).ready(function() {
    var count=1;

    $('#addRow').on('click',function(){
    count++;
    neobanner(count);
});
    
    function neobanner(i)
    {
        var tr= '<div id="row'+i+'" class="card" style="margin-top: 20px;"><div style="padding-top: 20px;padding-right: 20px;padding-bottom: 20px;padding-left: 20px;"><div class="form-group"><label for="foto" class="mt-3">@lang('home.bl_foto')</label>'+        
'<input id="foto'+i+'" accept="image/*" type="file" name="foto[' + i +']" value="" style="width: 100%;"><br><br>'+
                        '<div style="text-align:center;"><img src="" id="image-field'+i+'" width="250" height="250"></div>'+
                    
                    '<br>'+

'</div>'+

'<div class="form-group">'+

        '<label for="link" class="mt-3">Link</label>'+

        '<input class="form-control" placeholder="" name="link[' + i +']" type="text" value="">'+

                   '<br>'+
           

        '</div>'
        ;



tr+='<label class="col text-center"><a class="btn btn-danger deleteRow" id="'+i+'">Delete Row</a></label><br></div></div>';


$('#ggg').append(tr);


    }
   
    $(document).on('click','.deleteRow',function(){
    var button_id=$(this).attr("id");
    $('#row'+button_id+'').remove();
});
    });
    </script>-->

    
<!--SCRIPT PARA LA FOTO----------------------------------------------------------------------------------->
    <!--<script>
     $(document).ready(function(){

      var imgVal = $('#foto'i'').val(); 
        if(imgVal=='') 
        { 
          
          $('#image-field'i'').attr('src','../images/nia.png').fadeIn();
        }

       

$('#foto'i'').change(function(){
  
  const file2 = this.files[0];
const  fileType = file2['type'];
const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
if (validImageTypes.includes(fileType)) {
    //alert('NOES');
    var file = document.getElementById('foto'i'').files[0];

  var readImg = new FileReader();
  
  readImg.readAsDataURL(file);
  
  readImg.onload = function(e) {
    var span = document.createElement('span');
    $('#image-field'i'').attr('src',e.target.result).fadeIn();
    
    }
   
}else{
  Swal.fire({
			
			html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
			'<br><br><h1>Warning!</h1><br>Please select an image with format .gif, .jpeg, .png!',
			
			showConfirmButton: false,
			timer:1800
		  })
  $('#image-field'i'').attr('src','../images/nia.png').fadeIn();
}
       
  
    
  })

  $('#foto'i'').show(function(){
    const file2 = this.files[0];
const  fileType = file2['type'];
const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
if (validImageTypes.includes(fileType)) {
  
    var file = document.getElementById('foto'i'').files[0];

  var readImg = new FileReader();
  
  readImg.readAsDataURL(file);
  
  readImg.onload = function(e) {
    var span = document.createElement('span');
    $('#image-field'i'').attr('src',e.target.result).fadeIn();
    
    }
}else{
  Swal.fire({
			
			html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
			'<br><br><h1>Warning!</h1><br>Please select an image with format .gif, .jpeg, .png!',
			
			showConfirmButton: false,
			timer:1800
		  })
  $('#image-field'i'').attr('src','../images/nia.png').fadeIn();
}
  
    
  })
        
  
})
</script>-->

<!---------------------------------------------------------------------------------------->
    @endsection



@section('content2')

<?php

$postcomentados=array();
$postcantidad=array();
$py=array();
$arr='';
$ct=0;

use App\Post;
$posts=Post::all();
foreach($posts as $post){

$titulopost='';
$counter = 0;
foreach($post->comentarios as $comentario){
    $counter++;
    } $titulopost=$post->titulo;
    
    
    $postcomentados[]=$titulopost;
    $postcantidad[]=$counter;

$ct++;
}



for($i=0;$i<$ct;$i++){   
$py[$postcomentados[$i]]=$postcantidad[$i];

}

arsort($py);?>
@lang('home.bl_gencoment')


<?php
echo "<br>";
echo "<br>";
$i=0; 
foreach($py as $x => $x_value) {?>

	@lang('home.bl_title')
	
	<?php
	
		echo $x." | "?>@lang('home.bl_comments') <?php echo $x_value;
		echo "<br>";
		 
		if($i==9){
			break;
		}
	
		else{
		$i++;}
  }
  
  if($i==0){?>
    @lang('home.bl_nopost')
    <?php }



?>
@endsection

@section('js')
<script>
        CKEDITOR.replace( 'gui' );
    </script>
@endsection