@extends('layouts.mainblog')
@section('titleblog')

@lang('home.bl_createpost')

@endsection


<!---->
@section('carusel')
@endsection



@section('content')
<div class="text">
<div style="padding-top: 40px;
  padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<h1>@lang('home.bl_createpost')</h1>
<br>
    {!! Form::open(['action'=> 'HomeController@store', 'method'=> 'POST','enctype'=>'multipart/form-data']) !!}
       @csrf
            @if(session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>

            @endif
       
        <div class="form-group">
        <label for="categoria" class="mt-3">@lang('home.bl_bcategoria')</label>

            <!--{{Form::label('categoria','Categoría')}}-->
            <select class="form-control input-sm" name="categoria_id">
                <option value="">@lang('home.bl_bcategoria2')</option>
                @foreach($categorias as $categoria)

                    <option value="{{$categoria['id']}}">{{$categoria['nombre']}}</option>
                @endforeach
            </select>
                               <br>
            @if($errors->has('categoria_id'))
            @foreach($errors->get('categoria_id') as $error)

            <div class="alert alert-danger">
            @lang('home.bl_bcategoria3')
            </div>
            @endforeach
            @endif

        </div>

        <div class="form-group">

        <label for="titulo" class="mt-3">@lang('home.bl_bgtitulo')</label>

            <!--{{Form::label('titulo','Titulo')}}-->
            {{Form::text('titulo','',['class'=>'form-control','placeholder'=>''])}}

                   <br>
            @if($errors->has('titulo'))
            @foreach($errors->get('titulo') as $error)

            <div class="alert alert-danger">
            @lang('home.bl_bgtitulo2')
            </div>
            @endforeach
            @endif

        </div>

        <div class="form-group">

        <label for="body" class="mt-3">@lang('home.bl_bgbody')</label>

            <!-- {{Form::label('body','Body')}}-->
            {{Form::textarea('body','',['name'=>'body','class'=>'form-control','placeholder'=>'Body Text'])}}
            <!--<input type="textarea" placeholder="@lang('home.bl_bgbody2')" id="body" name="body" class="form-control">-->
                  <br>
            @if($errors->has('body'))
            @foreach($errors->get('body') as $error)

            <div class="alert alert-danger">
            @lang('home.bl_bgbody3')
            </div>
            @endforeach
            @endif

        </div>
        <div class="form-group">

        <label for="foto" class="mt-3">@lang('home.bl_blfoto')</label>        
            <!--{{Form::label('foto','Foto (opcional)')}}-->
            <div class="col-md-10">

                                <input id="foto" type="file" name="foto" value="" style="width: 100%;">

                                @if ($errors->has('foto'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('foto') }}</strong>
                                </span>
                                @endif
                            </div>
                            <br>

        </div>






        <br>
        <!--{{Form::submit('Crear Post',['class'=>'btn btn-primary'])}}-->
        <input type="submit" class="btn btn-success" value="@lang('home.bl_createpost')">
    {!! Form::close() !!}
    </div>
    </div>

    @endsection



@section('content2')

<?php

$postcomentados=array();
$postcantidad=array();
$py=array();
$arr='';
$ct=0;

use App\Post;
$posts=Post::all();
foreach($posts as $post){

$titulopost='';
$counter = 0;
foreach($post->comentarios as $comentario){
    $counter++;
    } $titulopost=$post->titulo;
    
    
    $postcomentados[]=$titulopost;
    $postcantidad[]=$counter;

$ct++;
}



for($i=0;$i<$ct;$i++){   
$py[$postcomentados[$i]]=$postcantidad[$i];

}

arsort($py);?>
@lang('home.bl_gencoment')


<?php
echo "<br>";
echo "<br>";
$i=0; 
foreach($py as $x => $x_value) {?>

	@lang('home.bl_title')
	
	<?php
	
		echo $x." | "?>@lang('home.bl_comments') <?php echo $x_value;
		echo "<br>";
		 
		if($i==9){
			break;
		}
	
		else{
		$i++;}
  }
  
  if($i==0){?>
    @lang('home.bl_nopost')
    <?php }


//--------------------------------
/*$age = array("Ben"=>"37", "Peter"=>"35", "Joe"=>"43");
print_r($age);
arsort($age);

foreach($age as $x => $x_value) {
    echo "Key=" . $x . ", Value=" . $x_value;
    echo "<br>";
}*/
//------------------------------------------------
?>
@endsection

@section('js')
<script>
        CKEDITOR.replace( 'body' );
    </script>
@endsection