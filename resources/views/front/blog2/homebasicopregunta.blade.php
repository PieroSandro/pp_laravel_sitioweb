@extends('layouts.mainblog')
@section('titleblog')



@lang('home.namebasicopreguntas')

@endsection
@section('carusel')


<!--------12_01_2020-------->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<link rel="stylesheet" href="{{url('/css/animate.css')}}">







<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        
        <script>
        AOS.init({
          duration:3000,
        });
        </script>





<!------------------->

<script>
$(document).ready(function(){
  $("option").hover(function(){
    $(this).css("background-color", "green");
  }, function(){
    $(this).css("background-color", "white");
    });
});
</script>
<style>


/*--11-1-2020 */
#basicopregunta-table_length{
  font-family: 'Montserrat', sans-serif;
}

#basicopregunta-table_filter{
  font-family: 'Montserrat', sans-serif;
}


#basicopregunta-table_info{
  font-family: 'Montserrat', sans-serif;
}

.dataTables_empty{
  font-family: 'Montserrat', sans-serif;
}

/*--- */


select option {
  
  background: white;
  color: black;
  
}

option:hover{
  background-color: yellow;
}

@media screen and (max-width: 575px){
  #basicopregunta-table_paginate{
 
 margin-left: 61%;
 margin-right: 50%;
}
  .dataTables_length{
	
	order:1;
  }

  .dataTables_filter{
	order:2;
  }
  
}

@media screen and (min-width: 576px) and (max-width: 767px){
  #basicopregunta-table_paginate{
 
 margin-left: 56%;
 margin-right: 50%;
}
}

@media screen and (min-width: 992px){
  #basicopregunta-table_paginate{
 
 margin-left: 56%;
 margin-right: 50%;
}
}

@media screen and (min-width: 768px) and (max-width: 991px){
  #basicopregunta-table_paginate{
 
 margin-left: 61%;
 margin-right: 50%;
}
  #insearch{
    align: center;
  }

  #basicopregunta-table_length{
    
   width: 50%;
  margin-left: auto;
  margin-right: auto;
  }

  #basicopregunta-table_filter{
   
   width: 50%;
  margin-left: auto;
  margin-right: auto;

  
  }



  .datatables_length{
   
    float: center;
           text-align: center; 
  }

  .datatables_filter{
    float: center;
           text-align: center; 
  }
  
}



.odd *:before{
  background-color: rgb(11, 11, 132, 0.993) !important;
  margin-top:4px;
}



.even *:before{
  background-color: rgb(11, 11, 132, 0.993) !important;
  margin-top:4px;
}



.parent *:before{
  background-color: rgba(228, 20, 20, 0.931) !important;
  margin-top:4px;
}

.panel-default{
  border-radius:10%;
}

*:focus {
    outline: none;
}

*:focus .page-link{
    outline: none;
}
.custom-select {
  
  font-family: 'Montserrat', sans-serif;
}

.custom-select *{
  
  font-family: 'Montserrat', sans-serif;
}
.page-link:disabled{
  background-color: green !important;
  color:green !important;
  outline: none;
}

.page-item.active .page-link {
    background-color: rgba(228, 20, 20, 0.931) !important;
    color: white !important;
    font-weight:bold;
    border-color: white !important;
    outline: none;
}

#event-table_next:active .page-link {
    background-color: rgba(228, 20, 20, 0.931) !important;
    font-family: 'Montserrat', sans-serif;
    color: white !important;
    font-weight:bold;
    border-color: white !important;
    outline: none;
}

#event-table_previous:active .page-link {
    background-color: rgba(228, 20, 20, 0.931) !important;
    font-family: 'Montserrat', sans-serif;
    color: white !important;
    font-weight:bold;
    border-color: white !important;
    outline: none;
}

.dt-button.xselected {
    color:blue;
    border: 1px blue solid;
}
.page-link{
  font-family: 'Montserrat', sans-serif;
  background-color: white !important;
  color: rgb(11, 11, 132, 0.993) !important;
  outline: none;
}

.page-item.active .page-link.active{
  color: white !important;
  font-family: 'Montserrat', sans-serif;
  border-color: white !important;
  outline: none;
}
/* rgba(228, 20, 20, 0.931) !important*/
.page-link:hover{
  background-color: rgb(11, 11, 132, 0.993) !important;
  font-family: 'Montserrat', sans-serif;
color: white !important;

border-color: white !important;
outline: none;
}

#bt1car:before{
	
	content:'@lang('home.ver_slider')';
	align-content: center;
	font-size:12.4px;/*15px */
	position:absolute;
	top:50%;
	left:50%;
	transition:all 0.3s;
	transform:translate(-50%,-50%);
}
</style>
   
              @endsection
              @section('content')
              <style>

                   
.swal2-modal .swal2-styled{
    
    background-color: rgba(11, 11, 132, 0.993);
    font-family: 'Montserrat', sans-serif;
}
                  .custom-select:focus{
  background-color:red;
}
              </style>
              <label id="qs"></label>
<br>
<br>    
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">

<link rel="stylesheet" href="{{url('/css/dataTables.bootstrap4.min.css')}}">


  
<h1 style="font-family: Montserrat, sans-serif; width:100%;">@lang('home.namebasicopreguntas')</h1>

<?php 
                  
                  $counter = 0;
                      ?>

<div id="app">
  
</div>
<div style="padding-top: 40px;padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">
  <div class="panel panel-default">


    <form action="{{url('/index/basicopreguntas')}}" method="post">
    {!! csrf_field() !!}
    <!---------------------------------------------------------------->
    @if(session('success'))
    <?php $locale=App::getLocale();
if(App::isLocale('en')){ ?>
<script>

Swal.fire({
  
  html: '<i class="fas fa-info-circle" style="color:rgba(11, 11, 132, 0.993); font-size:150px;"></i>'+
  '<br><br><br><div style="font-family: Montserrat, sans-serif;">{!!session('success')!!}</div>',

  showConfirmButton: true,
  //timer:3500
  })
               
</script>
<?php
}else{ ?>
<script>

Swal.fire({

html: '<i class="fas fa-info-circle" style="color:rgba(11, 11, 132, 0.993); font-size:150px;"></i>'+
'<br><br><br><div style="font-family: Montserrat, sans-serif;">{!!session('success')!!}</div>',

showConfirmButton: true,
//timer:3500
})
           
</script> <?php } ?>
            @endif
            <!-------------------------------------------------------------->
    <table id="basicopregunta-table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
        <thead>
            <tr>
            <th width="10%" style="border: 1px solid rgba(11, 11, 132, 0.993); background-color: rgba(11, 11, 132, 0.993); color: white; text-align: center; font-family: Montserrat, sans-serif;">N°</th>
            <th width="30%" style="border: 1px solid rgba(228, 20, 20, 0.931); background-color: rgba(228, 20, 20, 0.931);color: white; text-align: center; font-family: Montserrat, sans-serif;">@lang('home.namequestion')</th>
                   
                   <!------------------------------------------------------------------>
                   <!------------------------------------------------------------------>
                   
                   <th width="10%" style="border: 1px solid rgba(11, 11, 132, 0.993); background-color: rgba(11, 11, 132, 0.993);color: white; text-align: center; font-family: Montserrat, sans-serif;">@lang('home.nameoption1')</th>
                   <th width="10%" style="border: 1px solid rgba(228, 20, 20, 0.931); background-color: rgba(228, 20, 20, 0.931);color: white; text-align: center; font-family: Montserrat, sans-serif;">@lang('home.nameoption2')</th>
                   <th width="10%" style="border: 1px solid rgba(11, 11, 132, 0.993); background-color: rgba(11, 11, 132, 0.993);color: white; text-align: center; font-family: Montserrat, sans-serif;">@lang('home.nameoption3')</th>
                   <th width="10%" style="border: 1px solid rgba(228, 20, 20, 0.931); background-color: rgba(228, 20, 20, 0.931);color: white; text-align: center; font-family: Montserrat, sans-serif;">@lang('home.nameoption4')</th>
                   <th width="6%" style="border: 1px solid rgba(11, 11, 132, 0.993); background-color: rgba(11, 11, 132, 0.993);color: white; text-align: center; font-family: Montserrat, sans-serif;">@lang('home.nameanswer')</th>
          <th 
                    style="border: 1px solid rgba(228, 20, 20, 0.931); background-color: rgba(228, 20, 20, 0.931);color: white; text-align: center; font-family: Montserrat, sans-serif;">@lang('home.dt_accion')</th>
                    
            </tr>
        </thead>
        <tbody>
        @if(count($basicopreguntas)>0)
        
            @foreach($basicopreguntas as $basicopregunta)
           <?php
            $bodyvcurso='';
            $bodyvcurso=$basicopregunta->question;
           
            $bodyvcurso1 = html_entity_decode($bodyvcurso);
            ?>
            <tr>
            <td width="10%" 
            style="text-align:center; font-family: Montserrat, sans-serif;"> 
            <?php $counter++; echo $counter ?> <label name="<?php $counter ?>">
            </label><input type="checkbox" name="delid[]" class="cbx1" value="{{$basicopregunta->id}}"></td>


                
           
       <td class="justified-cell" width="30%" 
                style="font-family: Montserrat, sans-serif; text-align:justify; white-space: pre-wrap; white-space: -moz-pre-wrap; white-space: -pre-wrap; white-space: -o-pre-wrap; word-wrap: break-word;"><p style="padding-right: 1px;

    padding-left: 1px">{{$basicopregunta->question}}</p></td>        
<!--------------------------------------------------------->
<!--------------------------------------------------------->
<td class="justified-cell" width="10%" 
                style="font-family: Montserrat, sans-serif; text-align:justify; white-space: pre-wrap; white-space: -moz-pre-wrap; white-space: -pre-wrap; white-space: -o-pre-wrap; word-wrap: break-word;"><p style="padding-right: 1px;

    padding-left: 1px">{{$basicopregunta->option1}}</p></td>          
<td class="justified-cell" width="10%" 
                style="font-family: Montserrat, sans-serif; text-align:justify; white-space: pre-wrap; white-space: -moz-pre-wrap; white-space: -pre-wrap; white-space: -o-pre-wrap; word-wrap: break-word;"><p style="padding-right: 1px;

    padding-left: 1px">{{$basicopregunta->option2}}</p></td>
<td class="justified-cell" width="10%" 
                style="font-family: Montserrat, sans-serif; text-align:justify; white-space: pre-wrap; white-space: -moz-pre-wrap; white-space: -pre-wrap; white-space: -o-pre-wrap; word-wrap: break-word;"><p style="padding-right: 1px;

    padding-left: 1px">{{$basicopregunta->option3}}</p></td>
<td class="justified-cell" width="10%" 
                style="font-family: Montserrat, sans-serif; text-align:justify; white-space: pre-wrap; white-space: -moz-pre-wrap; white-space: -pre-wrap; white-space: -o-pre-wrap; word-wrap: break-word;"><p style="padding-right: 1px;

    padding-left: 1px">{{$basicopregunta->option4}}</p></td>
<td class="justified-cell" width="6%" 
                style="font-family: Montserrat, sans-serif; text-align:justify; white-space: pre-wrap; white-space: -moz-pre-wrap; white-space: -pre-wrap; white-space: -o-pre-wrap; word-wrap: break-word;"><p style="padding-right: 1px;

    padding-left: 1px">{{$basicopregunta->answer}}</p></td>             
<!--------------------------------------------------------->               
           
                
  <td style="font-family: Montserrat, sans-serif; text-align:center; vertical-align:middle; width:1%;
    white-space:nowrap;">
               


                
              
                <a class="btn btn-xs btn-primary" data-idcount="{{$counter}}" data-vcid="{{$basicopregunta->id}}" 
                data-question="{{$basicopregunta->question}}" data-option1="{{$basicopregunta->option1}}" 
                data-option2="{{$basicopregunta->option2}}" data-option3="{{$basicopregunta->option3}}" 
                data-option4="{{$basicopregunta->option4}}"
                data-answer="{{$basicopregunta->answer}}"
                data-toggle="modal" data-target="#modal-edit" 
                style="color:white; border: none; background-color:rgba(4, 10, 100, 0.808);"
                title="Editar"><i class="fas fa-pen"></i> @lang('home.bl_edit4')</a>
                &nbsp
                <a class="btn btn-xs btn-danger" data-idcount2="{{$counter}}" data-catid="{{$basicopregunta->id}}" 
                data-question="{{$basicopregunta->question}}"
                data-toggle="modal" data-target="#delete" style="color:white; border: none;"
                title="Borrar"><i class="fas fa-trash"></i> @lang('home.bl_borrar')</a>
               
               <input type="hidden" name="ccc[]" value="{{$counter}}">
                </td>
            </tr>





           
            @endforeach
            
            
           
            
        
        
       
        
        
@else

@endif
        
</tbody>

 

    </table>


        <input type="checkbox" id="checktodo" style="border-radius:3px;">
        <button type="submit" class="btn btn-xs btn-danger" 
    style="font-family: Montserrat, sans-serif; border: none; background-color:rgba(228, 20, 20, 0.931);">
        <i class="fas fa-trash"></i> @lang('home.deleteaudiosmore')</button>
        <!--<a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deletemore" style="color:white; border: none; background-color:rgba(228, 20, 20, 0.931);"
                title="">Delete selected</a>-->

               <!-- <a class="btn btn-xs btn-danger" type="submit" style="color:white; border: none; background-color:rgba(228, 20, 20, 0.931);"
                title="">Delete selected</a>-->
                <!--<button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deletemore" style="border: none; background-color:rgba(228, 20, 20, 0.931);">Delete selected</button>
        -->
    </form>
</div>
    </div>





<!------------------MODALS---------------------------->
<!------------------MODALS---------------------------->
<!------------------MODALS---------------------------->
<!------------------MODALS---------------------------->  

    <div class="modal modal-danger fade" tabindex="-1" id="delete" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <a href="#" class="navbar-brand"><img src="{{url('/images/CCURRlogo.png')}}" style="width:65px;"></a>
                          
                <h4 class="modal-title lang" style="font-family: Montserrat, sans-serif; margin:0 auto;" key="news">@lang('home.bl_deleteconf')</h4>
                      
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                
                            
                </div>
               
                <form id="sfdel" class="sfdel2" action="{{route('destroybasicopregunta','test')}}" method="post">
                {{method_field('delete')}}
                {{csrf_field()}}
                <div class="modal-body">
                <p class="text-center" style="font-family: Montserrat, sans-serif;">
                @lang('home.audio_qdelete_1') <label id="btitulo"></label>?
                        </p>
                        <input type="hidden" name="id" id="cat_id" value="">
                        <input type="hidden" name="idc2" id="idcount2" value=""> 
                </div>		
                <div class="modal-footer text-center" style="text-align:center; vertical-align:middle;">
                <fieldset class="align-center" 
            style="display: block;
    margin: auto;
    text-align: center;">
                <button type="button" style="color: white; border: rgba(11, 11, 132, 0.993); background-color:rgba(11, 11, 132, 0.993); font-family: Montserrat, sans-serif;" class="btn" data-dismiss="modal"><i class="fas fa-times"></i> @lang('home.audio_no')</button>
                <button type="submit" style="border: none; font-family: Montserrat, sans-serif;" class="btn btn-danger" id="submitdel"><i class="fas fa-trash"></i> @lang('home.audio_yes')</button>
                </fieldset>
                
                </div>
               
                </form>
                </div>
                </div>
                </div>

<!----------------------------------------------------->
<div class="modal fade" id="gardenImage" tabindex="-1" role="dialog" aria-labelledby="gardenImageLabel">
    <div class="modal-dialog modal-lg" style="width:100%; height:100%;" role="document">
        <div class="modal-content" style="height:90%;">
            <div class="modal-body" style="height:90%;">
              
            <iframe src="" frameborder="0" id="myImage" width="100%" height="100%"></iframe>
            </div>
            <div class="modal-footer text-center" style="text-align:center; vertical-align:middle;">
            <fieldset class="align-center" 
            style="display: block;
    margin: auto;
    text-align: center;">
 <button type="button" style="color: white; border: rgba(228, 20, 20, 0.931); background-color:rgba(228, 20, 20, 0.931) ; float:center; font-family: Montserrat, sans-serif;" class="btn center-block" data-dismiss="modal">
                <i class="fas fa-times"></i> @lang('home.times_close')</button>                </fieldset>
    </div>
        </div>
    </div>
</div>
<!------------modaldeletemore---------------------------->
<div class="modal modal-danger fade" id="deletemore" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <a href="#" class="navbar-brand"><img src="{{url('/images/CCURRlogo.png')}}" style="width:65px;"></a>
                          
                        <h4 class="modal-title lang" style="margin:0 auto;" key="news">@lang('home.bl_deleteconf')</h4>
                      
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                
                            
                </div>
               
                <form action="{{route('destroybasicopreguntamore','test')}}" method="post">
                {{method_field('delete')}}
                {{csrf_field()}}
                <div class="modal-body">
                        <p class="text-center">
                        @lang('home.bl_modalq')
                        </p>
                       
                </div>		
                <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">@lang('home.bl_modalcancel')</button>
                <button type="submit" class="btn btn-danger">@lang('home.bl_modaldelete')</button>
                </div>
               
                </form>
                </div>
                </div>
                </div>

<!---------------------modaldeletemore---------------------------------->

<!-- EDIT FORM------------------->


 <!-----------------EDITFORM-------------------------------------------------------->

 <div class="modal modal-danger fade" aria-hidden="true" tabindex="-1" id="modal-edit" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                
                
                <div class="modal-header">
                <a href="#" class="navbar-brand"><img src="{{url('/images/CCURRlogo.png')}}" style="width:65px;"></a>
                          
                <h4 class="modal-title lang" style="margin:0 auto; font-family: 'Montserrat', sans-serif;" 
                        key="news">@lang('home.soloedit')</h4>  





                            <button type="button" class="close" data-dismiss="modal" 
                            aria-hidden="true">&times;</button>
                
                            
                </div>
               
                <form id="editForm" action="{{route('updatebasicopregunta','test')}}" method="post">
                
               
                {{method_field('put')}}<!--patch -->
                {{csrf_field()}}
                <div class="modal-body">
                <input type="hidden" name="id" id="vc_id" value="">       
                <input type="hidden" name="idc" id="idcount" value=""> 

               

        <div class="form-group" style="font-family: 'Montserrat', sans-serif;">
        <label for="question" class="mt-3">@lang('home.namequestion')</label>

            
        <textarea class="form-control" cols="30" rows="10" name="question" id="question"></textarea>

        </div>
  
       <!-- <div class="form-group" style="font-family: 'Montserrat', sans-serif;">
        <label for="titulo" class="mt-3">@lang('home.bl_titulo')</label>

           
            <input type="text" class="form-control" name="titulo" id="titulo">
        </div>-->
        <div class="form-group" style="font-family: 'Montserrat', sans-serif;">
        <label for="option1" class="mt-3">@lang('home.nameoption1')</label>

           
            <input type="text" class="form-control" name="option1" id="option1">
        </div>

        <div class="form-group" style="font-family: 'Montserrat', sans-serif;">
        <label for="option2" class="mt-3">@lang('home.nameoption2')</label>

           
            <input type="text" class="form-control" name="option2" id="option2">
        </div>

        <div class="form-group" style="font-family: 'Montserrat', sans-serif;">
        <label for="option3" class="mt-3">@lang('home.nameoption3')</label>

           
            <input type="text" class="form-control" name="option3" id="option3">
        </div>

        <div class="form-group" style="font-family: 'Montserrat', sans-serif;">
        <label for="option4" class="mt-3">@lang('home.nameoption4')</label>

           
            <input type="text" class="form-control" name="option4" id="option4">
        </div>

        <div class="form-group" style="font-family: 'Montserrat', sans-serif;">
        <label for="answer" class="mt-3">@lang('home.nameanswer')</label>

           
            <input type="text" class="form-control" name="answer" id="answer">
       
    <!------------------------------------------------->
        
       <!------------------------------framevideo-------------------------------------------------------->
        <!--SCRIPT PARA LA FOTO----------------------------------------------------------------------------------->
    <script>
     $(document).ready(function(){

      var imgVal = $('#link').val();
     
        if(imgVal=='') 
        { 
          
          $('#vid-field').attr('src','../images/nva.jpg').fadeIn();
        }

$("#link").change(function(){
  
var imgVal=$("#link").val();
if(imgVal!==''){
  
$('#vid-field').attr('src',imgVal).fadeIn();
}else{
  $('#vid-field').attr('src','../images/nva.jpg').fadeIn();
}
  })

  $("#link").show(function(){
   
var imgVal=$("#link").val();
if(imgVal!==''){ 
 
$('#vid-field').attr('src',imgVal).fadeIn();
}else{
  $('#vid-field').attr('src','../images/nva.jpg').fadeIn();
}
  
    
  })

  $("#link").keypress(function(){
    var imgVal=$("#link").val();
if(imgVal!==''){
 
$('#vid-field').attr('src',imgVal).fadeIn();
}else{
  $('#vid-field').attr('src','../images/nva.jpg').fadeIn();
}
                });

                $("#link").keydown(function(){
                  var imgVal=$("#link").val();
if(imgVal!==''){
  
$('#vid-field').attr('src',imgVal).fadeIn();
}else{
  $('#vid-field').attr('src','../images/nva.jpg').fadeIn();
}
                });

                $("#link").keyup(function(){
                  var imgVal=$("#link").val();
if(imgVal!==''){   
  
$('#vid-field').attr('src',imgVal).fadeIn();
}else{
  $('#vid-field').attr('src','../images/nva.jpg').fadeIn();
}
                });

                $("#link").focusin(function(){
                  var imgVal=$("#link").val();
if(imgVal!==''){
 
$('#vid-field').attr('src',imgVal).fadeIn();
}else{
  $('#vid-field').attr('src','../images/nva.jpg').fadeIn();
}
                });
        
                $('#question').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

//-----------------------------------------------------------
    //$(this).find('option:hover').css('background-color', 'red');
    //$(this).find('option:selected').css('background-color', 'red');
   
});


$('#question').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});

$('#option1').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

});


$('#option1').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});


$('#option2').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

});


$('#option2').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});


$('#option3').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

});


$('#option3').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});


$('#option4').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

});


$('#option4').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});


$('#answer').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

});


$('#answer').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});

})
</script>
<!--SCRIPT PARA LA FOTO--------------------------------------------------->

        </div>
                
                </div>

                <div class="modal-footer text-center" style="text-align:center; vertical-align:middle;">
                <fieldset class="align-center" 
            style="display: block;
    margin: auto;
    text-align: center;">
                <button type="button" style="color: white; border: rgba(228, 20, 20, 0.931); background-color:rgba(228, 20, 20, 0.931); font-family: Montserrat, sans-serif;" class="btn" data-dismiss="modal"><i class="fas fa-times"></i> @lang('home.audio_no')</button>
                <button type="submit" style="color: white; border: rgba(11, 11, 132, 0.993); background-color:rgba(11, 11, 132, 0.993); font-family: Montserrat, sans-serif;" class="btn" id="submitdel"><i class="fas fa-check"></i> @lang('home.audio_yes')</button>
                </fieldset>
                
                </div>
                </form>
                </div>
                </div>
                </div>

<!--------------------------------EDIT FORM------------------------------------------>
<script>
$(document).on("click", ".openImageDialog", function () {
    var myImageId = $(this).data('id');
    $("#gardenImage .modal-body #myImage").attr("src", myImageId);
    $("#gardenImage").modal('show');
});
</script>



<script>
                function Scrolldown() {
    
     document.getElementById("qs").scrollIntoView();
}

window.onload = Scrolldown;
</script>
            

<!-------------------------------------------------------------->

                




    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>

<script>

   

    

        $('#basicopregunta-table').DataTable({
          "language": {
            "emptyTable":			"@lang('home.emptyTable')",
				"info":		   		"@lang('home.info')",
				"infoEmpty":			"@lang('home.infoEmpty')",
				"infoFiltered":			"@lang('home.infoFiltered')",
				"infoPostFix":			"@lang('home.infoPostFix')",
				"lengthMenu":			"@lang('home.lengthMenu')",
				"loadingRecords":		"@lang('home.loadingRecords')",
				"processing":			"@lang('home.processing')",
				"search":			"",
				"searchPlaceholder":		"@lang('home.searchPlaceholder')",//"@lang('home.ver_slider')"
				"zeroRecords":			"@lang('home.zeroRecords')",
				"paginate": {
					"first":			"@lang('home.first')",
					"last":				"@lang('home.last')",
					"next":				"›",
					"previous":			"‹"
				},
				"aria": {
					"sortAscending":	"@lang('home.sortAscending')",
					"sortDescending":	"@lang('home.sortDescending')",
				}
			},

      //responsive: true;
        columns:[
          {data:'nro',name:'nro'},
            {data:'question',name:'question'},
            {data:'option1',name:'option1'},
            {data:'option2',name:'option2'},
            {data:'option3',name:'option3'},
            {data:'option4',name:'option4'},
            {data:'answer',name:'answer'},


            
            {data:'action',name:'action',orderable:false,searchable:false}
            ]
    });

    $.fn.dataTable.ext.errMode = 'throw';

    
    

</script>



<script type="text/javascript" src="{{asset('js/app.js')}}"></script>

<script>
$(document).ready(function(){
  $("select").focusin(function(){
    $("select").css("border-color", "rgb(11, 11, 132, 0.993)");
    $("select").css("box-shadow", "0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)");
  });
  $("select").focusout(function(){
    $("select").css("border-color", "#ccc");
    $("select").css("box-shadow", "0 0 0px");
  });
});
</script>
<script>

$(function(){
  $("option").attr("style","font-family: Montserrat;");

  document.getElementsByTagName("input")[1].setAttribute("id", "insearch");
  $("#insearch").focusin(function(){
    document.getElementById("insearch").style.borderColor = "rgb(11, 11, 132, 0.993)";
                  
                  document.getElementById("insearch").style.boxShadow = "0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)";
                 
                });
                $("#insearch").focusout(function(){
    document.getElementById("insearch").style.borderColor = "#ccc";
                  
                  document.getElementById("insearch").style.boxShadow = "0 0 0px";
                 
                });
    
                
                document.getElementsByClassName("col-sm-12")[0].setAttribute("class", "col-xs-12 col-sm-6 col-md-12 col-lg-6");
                document.getElementsByClassName("col-sm-12")[0].setAttribute("class", "col-xs-12 col-sm-6 col-md-12 col-lg-6");
                document.getElementsByClassName("col-md-5")[0].setAttribute("class", "col-12");
                document.getElementsByClassName("col-md-7")[0].setAttribute("class", "col-12");
                //document.getElementsByClassName("col-12")[1].style.textAlign = "center";
                document.getElementsByClassName("col-12")[2].style.textAlign = "center";

                document.getElementsByClassName("pagination").setAttribute("role", "navigation");
                //document.getElementsByClassName("pagination").setAttribute("class", "pagination justify-content-center");
                //document.getElementById("vcurso-table_paginate").style.Align = "center";
                document.getElementsByTagName("select")[0].setAttribute("id", "selnum");

                $("#selnum").focusin(function(){
    document.getElementById("selnum").style.borderColor = "rgb(11, 11, 132, 0.993)";
                  
                  document.getElementById("selnum").style.boxShadow = "0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)";
                 
                });
                $("#selnum").focusout(function(){
    document.getElementById("selnum").style.borderColor = "#ccc";
                  
                  document.getElementById("selnum").style.boxShadow = "0 0 0px";
                 
                });

                document.getElementsByTagName("option")[0].setAttribute("id", "diez");
                document.getElementsByTagName("option").style.fontFamily="Monserrat,sans-serif";


               // document.getElementsByClassName("select-checkbox").style.boxShadow = "2 2 2px";
});

</script>

<script>

$('#delete').on('show.bs.modal',function(event){
  var button=$(event.relatedTarget)
  var idcount2=button.data('idcount2')
  var cat_id=button.data('catid')
  var modal=$(this)

 //print link title into modal

 var btitulo = button.data("titulo")
        //$(this).find(".modal-title").text(blink);
        modal.find(".modal-body #btitulo").text(idcount2);
//print link title into modal

  modal.find('.modal-body #cat_id').val(cat_id);
  modal.find(".modal-body #idcount2").val(idcount2);
})
</script>
<script>
 
  $("#checktodo").change(function(){
      $(".cbx1").prop("checked",$(this).prop("checked"))
    });
    $(".cbx1").change(function(){
      if($(this).prop("checked")==false){
        $("#checktodo").prop("checked",false)
      }
      if($(".cbx1:checked").length==$(".cbx1").length){
        $("#checktodo").prop("checked",true)
      }
    })
</script>






<script>
      
      $('#modal-edit').on('show.bs.modal',function(event){

        /*$("#messageModal").on('shown.bs.modal', function() {
     CKEDITOR.replace('message', {
       height: '400px',
       width: '100%'
     });
   })*/
   //CKEDITOR.replace('body');
  var button=$(event.relatedTarget)
  var idcount=button.data('idcount')


  /*
  var titulo=button.data('titulo')
  var body=button.data('body')
  var link=button.data('link')*/
  var question=button.data('question')
  var option1=button.data('option1')
  var option2=button.data('option2')
  var option3=button.data('option3')
  var option4=button.data('option4')
  var answer=button.data('answer')
  var vcid=button.data('vcid')
  
  var modal=$(this)

 
       /* modal.find(".modal-body #titulo").val(titulo);
        modal.find(".modal-body #body").val(body);
        modal.find(".modal-body #link").val(link);*/
        modal.find(".modal-body #question").val(question);
        modal.find(".modal-body #option1").val(option1);
        modal.find(".modal-body #option2").val(option2);
        modal.find(".modal-body #option3").val(option3);
        modal.find(".modal-body #option4").val(option4);
        modal.find(".modal-body #answer").val(answer);

        modal.find(".modal-body #vc_id").val(vcid);
        modal.find(".modal-body #idcount").val(idcount);


      //---------------------------------------------------
     // modal.find(".modal-body #bodytxt2").val(idcount);

}) 
</script>

@endsection

@section('content2')


@endsection














