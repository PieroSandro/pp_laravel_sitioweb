@extends('layouts.mainblog')
@section('titleblog')

@lang('home.bl_conta')

@endsection


@section('carusel')


<ul id="sb-slider" class="sb-slider" style="max-height:650px; width: 100%; margin: auto;">
           
           
           <?php
               use App\Banner;
               $banners=Banner::all();
               $counter1=1;
               foreach($banners as $banner){
                   ?>
 
 
 
 <li style="max-height:650px; width:100%;">
                         <a href="{{$banner->link}}" target="_blank"><img class="d-block w-100" src="/intranet/storage/app/public/images/{{$banner->foto}}" alt="image_{{$counter1}}"/></a>
             
             
             <div class="sb-description" style="text-align:center; width:40px; height:40px; margin:0 auto;">
                             <h3 style="text-align:center; font-family: 'Montserrat', sans-serif;">{{$counter1}}</h3>
             </div>
               
           </li>
 
 
 
 <?php
 $counter1++;
               }
 
 
 
 
 
 
             ?>
 
 
 
 
 
 
           <div id="nav-arrows" class="nav-arrows">
                     <a href="#">Next</a>
                     <a href="#">Previous</a>
                 </div>
         </ul>
              @endsection
@section('content')


<!-------------------------POPOVER---------------------------------->

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<!----------------------POPOVER------------------------------------->



<!--------12_01_2020-------->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<link rel="stylesheet" href="{{url('/css/animate.css')}}">







<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        
        <script>
        AOS.init({
          duration:3000,
        });
        </script>



<style>





/* ----------------------------------------------------------------------------------*/
.conew1{
	/*border: none;
	outline: none;*/
	background: rgba(4, 10, 100, 0.808);
	min-width: 120px;
	min-height: 35px;
	border-radius: 20px;
	/*cursor: pointer;
	margin: 10px;*/
}

@-webkit-keyframes conew1 {
  to {
    -webkit-transform: scale(1.03);
    transform: scale(1.03);/* 1.1*/
  }
}
@keyframes conew1 {
  to {
    -webkit-transform: scale(1.03);
    transform: scale(1.03);
  }
}

#NumPosts{
  /*background: rgba(4, 10, 100, 0.808);*/
  background: rgba(228, 20, 20, 0.931);
	min-width: 120px;
	min-height: 35px;
	border-radius: 20px;

}

.NewPosts2{
  background: rgba(4, 10, 100, 0.808);
	min-width: 120px;
	min-height: 35px;
	border-radius: 20px;
}

@-webkit-keyframes NewPosts2 {
  to {
    -webkit-transform: scale(1.03);
    transform: scale(1.03);/* 1.1*/
  }
}
@keyframes NewPosts2 {
  to {
    -webkit-transform: scale(1.03);
    transform: scale(1.03);
  }
}

.NewPosts2 {
  
  -webkit-transform: perspective(1px) translateZ(0);
  transform: perspective(1px) translateZ(0);
  box-shadow: 0 0 1px rgba(0, 0, 0, 0);

  -webkit-animation-name: NewPosts2;
  animation-name: NewPosts2;
  -webkit-animation-duration: 0.3s;
  animation-duration: 0.3s;
  -webkit-animation-timing-function: linear;
  animation-timing-function: linear;
  -webkit-animation-iteration-count: infinite;
  animation-iteration-count: infinite;
  -webkit-animation-direction: alternate;
  animation-direction: alternate;
}

.conew1 {
  display: inline-block;
  vertical-align: middle;
  -webkit-transform: perspective(1px) translateZ(0);
  transform: perspective(1px) translateZ(0);
  box-shadow: 0 0 1px rgba(0, 0, 0, 0);

  -webkit-animation-name: conew1;
  animation-name: conew1;
  -webkit-animation-duration: 0.3s;
  animation-duration: 0.3s;
  -webkit-animation-timing-function: linear;
  animation-timing-function: linear;
  -webkit-animation-iteration-count: infinite;
  animation-iteration-count: infinite;
  -webkit-animation-direction: alternate;
  animation-direction: alternate;
}



/* ------------------------------------------------------------------------------------------------------------*/
#bt1car:before{
	
	content:'@lang('home.ver_slider')';
  align-content: center;
  font-family: 'Montserrat', sans-serif;
	font-size:11.1px;/*15px */
	position:absolute;
	top:50%;
	left:50%;
	transition:all 0.3s;
	transform:translate(-50%,-50%);
}

#bt1car2:before{
	
	content:'@lang('home.ver_slider')';
  align-content: center;
  font-family: 'Montserrat', sans-serif;
	font-size:11.1px;/*15px *//*12.4px; */
	position:absolute;
	top:50%;
	left:50%;
	transition:all 0.3s;
	transform:translate(-50%,-50%);
}

#bt1car3:before{
	
	content:'@lang('home.ver_slider')';
  align-content: center;
  font-family: 'Montserrat', sans-serif;
	font-size:11.1px;/*15px *//*12.4px; */
	position:absolute;
	top:50%;
	left:50%;
	transition:all 0.3s;
	transform:translate(-50%,-50%);
}
</style>



<!------------------->
<div class="text" style="font-family: 'Montserrat', sans-serif;">
<div style="padding-top: 40px;
  padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">
  <h1 style="color: black; font-family: 'Montserrat', sans-serif;">@lang('home.bl_conta') <b><p><label style="padding-left:10px; padding-right:10px; padding-top:8px; text-align:center; float: center; color:white; font-size:16px; font-family: 'Montserrat', sans-serif;" id="NumPosts"></label> <label style="padding-left:10px; padding-right:10px; padding-top:8px; text-align:center; float: center; color:white; font-size:16px; font-family: 'Montserrat', sans-serif;" id="NewPosts" class="NewPosts2"></label></p></b></h1>

<!--<h1>@lang('home.bl_conta')</h1>-->
<br>



@if(session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
                @elseif(session('danger'))
                <div class="alert alert-danger">
                    {{session('danger')}}
                </div>
            @endif
@csrf
@if(count($posts)>0)
@foreach($posts as $post) 

	<div class="card">
  <div style="padding-top: 20px;padding-right: 20px;
  padding-bottom: 20px;
  padding-left: 20px;">
@if((time() - strtotime($post->created_at)) <= 86400)

<div style="padding-top: 0px;
padding-bottom: 20px;">
<div class="conew1"><div style="padding-left:10px; padding-right:10px; padding-top:5px; text-align:center; float: center; color:white; font-family: 'Montserrat', sans-serif;"><strong>@lang('home.bl_newpost') <i class="fas fa-file comicon2" style="color:white"></i></strong></div></div>
</div>
@else

@endif
		
<h3 style="font-family: 'Montserrat', sans-serif;"><a href="/intranet/public/blog/home/{{$post->id}}" onMouseOver="this.style.cssText='color: rgba(228, 20, 20, 0.931); text-decoration:none;'" onMouseOut="this.style.cssText='color: rgb(11, 11, 132, 0.993)'" style="text-decoration: none; color: rgb(11, 11, 132, 0.993);">{{$post->titulo}}</a></h3>
    <br>
             
    @if($post->foto=='noimage.jpg')
        
        


              
              
        <h5 style="text-align:justify; font-family: 'Montserrat', sans-serif; font-style: normal;">


  <label style="color:black;">      <?php
$bodypost='';
$bodypost=$post->body;
$extract=substr(htmlspecialchars(trim(strip_tags($bodypost))), 0, 150);



$bodypost1=htmlspecialchars(trim(strip_tags($bodypost)));
//$extract=mb_strimwidth($bodypost, 1, 90, "...");
if(strlen($bodypost)>151){

//echo $extract, PHP_EOL;
echo htmlspecialchars_decode($extract, ENT_NOQUOTES);
echo "... ";?> 
<small><a href="/intranet/public/blog/home/{{$post->id}}" style="text-decoration: none; color: blue;">
@lang('home.bl_lmas')</a></small>
<?php
}
else{
echo htmlspecialchars_decode($bodypost1, ENT_NOQUOTES);
}
?>
</label>

</h5>

  @else
  
  <div class="row">
  <div class="col-12 col-sm-6 col-md-12 col-lg-6">
  <!--<h3><a href="/intranet/public/blog/home/{{$post->id}}" style="text-decoration: none;"> {{$post->titulo}}</a></h3>
        
        <br>-->
        <h5 style="text-align:justify; font-family: 'Montserrat', sans-serif; font-style: normal;">
        <label style="color:black;"> 

        <?php
$bodypost='';
$bodypost=$post->body;
$extract=substr(htmlspecialchars(trim(strip_tags($bodypost))), 0, 150);



$bodypost1=htmlspecialchars(trim(strip_tags($bodypost)));
//$extract=mb_strimwidth($bodypost, 1, 90, "...");
if(strlen($bodypost)>151){

//echo $extract, PHP_EOL;
echo htmlspecialchars_decode($extract, ENT_NOQUOTES);
echo "... ";?>
<small><a href="/intranet/public/blog/home/{{$post->id}}" style="text-decoration: none; color: blue;">
@lang('home.bl_lmas')</a></small>
<?php
}
else{
echo htmlspecialchars_decode($bodypost1, ENT_NOQUOTES);
}
?>

</label>
</h5>
  <!--<p class="tc">Text on the left.</p>-->
  </div>
  
  <div class="col-12 col-sm-6 col-md-12 col-lg-6">
  <center>
  <div>
<img src="/intranet/storage/app/public/images/{{$post->foto}}" style="margin-bottom:5%; margin-top:5%; max-width:95%; max-height:95%;">
</div>
</center>
  </div>
  <!--<p class="alignleft">Text on the left.</p>
  <p class="alignright">Text on the right.</p>-->
  </div>
  
  
  <!--$fpo='/intranet/storage/app/public/images/{{$post->foto}}';-->
  @endif
<br>
    
    <small>@lang('home.bl_fcreacion'): {{$post->created_at}}</small><br>
        
        <small>@lang('home.bl_factualizacion'): {{$post->updated_at}}</small>
      </div>
	</div>
	<br>


@endforeach
	{{$posts->links('vendor.pagination.bootstrap-4')}}
@else
<p>@lang('home.bl_nopost2')</p>
@endif  
</div>
</div>
@endsection

@section('content2')

<?php

$postcomentados=array();
$postcantidad=array();
$linkcomentados=array();
$py=array();
$arr='';
$ct=0;
//-------------------------------------------------
//----------------------------@lang('home.bl_contacoment')--------------------
//------------------------------------------------

use App\Post;
$posts=Post::all();
foreach($posts as $post){
if ($post->categoria_id == 3){  

  $titulopost='';
  $linkpost='';
  $fpo='';
  $counter = 0;
  foreach($post->comentarios as $comentario){
      $counter++;
      } //$titulopost=$post->titulo;
  
      $pbody=$post->body;
      //----------------------------------------------------------------------------
      
  $extract=substr(htmlspecialchars(trim(strip_tags($pbody))), 0, 150);
  
  
  
  $pbody1=htmlspecialchars(trim(strip_tags($pbody)));
  //$extract=mb_strimwidth($bodypost, 1, 90, "...");
  if(strlen($pbody)>151){
  
    $locale=App::getLocale();
  //echo $extract, PHP_EOL;
  if(App::isLocale('en')){
  $pbo=htmlspecialchars_decode($extract, ENT_NOQUOTES).'... <small><a href="/intranet/public/blog/home/'.$post->id.'" style="text-decoration: none; color: blue;">
  Read more</a></small>';
  }else{
    $pbo=htmlspecialchars_decode($extract, ENT_NOQUOTES).'... <small><a href="/intranet/public/blog/home/'.$post->id.'" style="text-decoration: none; color: blue;">
  Leer más</a></small>';
  }
  /*echo "... ";?>
  <small><a href="/intranet/public/blog/home/{{$post->id}}" style="text-decoration: none; color: blue;">
  @lang('home.bl_lmas')</a></small>
  <?php*/
  }
  else{
   $pbo=htmlspecialchars_decode($pbody1, ENT_NOQUOTES);
  }
  
  //-------------------------------------------------------------
  $fpost=$post->foto;
  $fpo='';
  if($fpost=='noimage.jpg'){  
  $fpo='';
  }else{
  
  
  
  $fpo='/intranet/storage/app/public/images/{{$post->foto}}';
  
  }
  
  //$fpo="<img src='/intranet/storage/app/public/images/".$post->foto."' />";
  
  $tpost="<div style='color:blue'>".$post->titulo."</div>";
  
      //-----------------------------------------------------------------------------
      $linkpost="
      <style>
     .popover {
      
      color: black;
      text-align:justify;
      background-color: white;
    }
    
    .popover-title {
      background-color: coral; 
      color: coral; 
      font-size: 40px;
      text-align:center;
    }
   
    .popover-content {
      background-color: green;
      color: blue;
      padding: 25px;
    }
    
    .arrow {
      border-right-color: red !important;
    }
     
     
     </style>
      <a href='/intranet/public/blog/home/".$post->id."'
      title='".$post->titulo."' data-by='".$pbo."' data-img='/intranet/storage/app/public/images/".$post->foto."' class='popi' data-toggle='popover' data-placement='bottom' style='text-decoration: none; color: blue; font-family: Montserrat, sans-serif;'>".$post->titulo."</a>
  
     ";?>
     
  <?php
      $linkcomentados[]=$linkpost;
      //$postcomentados[]=$titulopost;
      $postcantidad[]=$counter;
  
  $ct++;
}
}


for($i=0;$i<$ct;$i++){   
$py[$linkcomentados[$i]]=$postcantidad[$i];

}

arsort($py);?>
<div style="text-align:center;">
<div class="plustar1" id="plustar2"><div style="margin:10px; padding-left:10px; padding-right:10px; padding-top:2px; text-align:center; float: center; color:white; font-family: 'Montserrat', sans-serif;"><strong><i class="fas fa-star plustar" style="color:yellow"></i>  @lang('home.bl_contacoment')  <i class="fas fa-star plustar" style="color:yellow"></i></strong></div></div>
</div>





<!-- ---------------------------------
---------------------------------------->
<?php
echo "<br>";
echo "<br>";
$i=0; 
foreach($py as $x => $x_value) {?>

<label style="font-family: Montserrat, sans-serif;">
  <b>
	@lang('home.bl_title')
  </b>
	</label>

<?php

echo $x."&nbsp<label style='font-family: Montserrat, sans-serif;'><b>|</b></label>&nbsp"?><label style="font-family: Montserrat, sans-serif;"><b>@lang('home.bl_comments') </b></label>&nbsp<?php 
echo "<label style='font-family: Montserrat, sans-serif;'>";
echo $x_value;
echo "</label>";
echo "<br>";
     
    if($i==9){
    	break;
    }

    else{
    $i++;}
}

if($i==0){?>
  @lang('home.bl_nopost2')
  <?php }

$counter1 = 0;
$counter2= 0;
foreach($posts as $post){
  if ($post->categoria_id == 3){  
    $counter1++;
      if((time() - strtotime($post->created_at)) <= 86400)
      {
          $counter2++;
      }
    }
		}

$counter1;
$counter2;

?>

<script>

   $('.popi').popover({
    delay: 1150,
          //trigger: 'focus',
		  trigger: 'hover',
          html: true,
          content: function () {

            if($(this).data('img')=='/intranet/storage/app/public/images/noimage.jpg')
            {
            return $(this).data("by");
            }else{
              
				return $(this).data("by")+'<br><br><img class="img-fluid" src="'+$(this).data('img') + '" />';
            }
          },
          title: 'Toolbox'
    });   
 
 </script>
<script>
          $(function(){

            $("#NumPosts").html("Total: <?php echo $counter1 ?>");

            var c2 = "<?php echo $counter2 ?>";
            if(c2>0){   
            $("#NewPosts").html(" @lang('home.bl_newpost2') <i class='fas fa-file comicon2' style='color:white'></i> : <?php echo $counter2 ?>");
            }else{
              $("#NewPosts").hide();
            }

            $("#dance_id2").hide();

            $("#danza_id").mouseover(function(){
              $("#dance_id1").hide();
              $("#dance_id2").show();
                });

            $("#danza_id").mouseout(function(){
              $("#dance_id2").hide();
              $("#dance_id1").show();
                });
            
          });
        </script>
@endsection