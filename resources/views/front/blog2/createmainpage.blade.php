@extends('layouts.mainblog')
@section('titleblog')

@lang('home.bl_banner1')

@endsection


<!---->
@section('carusel')
@endsection



@section('content')
<style>


.swal2-modal .swal2-styled{
    
    background-color: rgba(4, 10, 100, 0.808);
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js">
</script>
<div class="text">
<div style="padding-top: 40px;
  padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<a href="{{route('backbannerhome')}}" class="btn btn-success"><i class="fas fa-angle-left"></i> @lang('home.bl_back')</a>
<br><br>
<h1>@lang('home.bl_banner1')</h1>
<br>
{!! Form::open(['action'=> 'HomeController@storemainpage', 'method'=> 'POST','enctype'=>'multipart/form-data']) !!}

@csrf

@if(session('success'))
 
<script>

Swal.fire({
  
  html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
  '<br><br><h1>Warning!</h1><br> {!!session('success')!!}',
  
  showConfirmButton: true,
  
  })   
           
</script>

            @endif

    <span id="result"></span>
            
        <br>

         <!-----------------------CREATEVCURSOFORM---------------------------------------------------------------------------------->
         <div id="ggg">
       
       <div class="card">
       <div style="padding-top: 20px;padding-right: 20px;
 padding-bottom: 20px;
 padding-left: 20px;">

<div class="form-group">
    <label for="body_es" class="mt-3">@lang('home.bl_link')</label>
    <textarea class="form-control" cols="30" rows="10" name="body_es" id="body_es"></textarea>

    <!--<input class="form-control" placeholder="" name="link[1]" type="text" value=""><br>-->
    
    </div>

       <div class="form-group">
           <label for="foto_es" class="mt-3">@lang('home.bl_title')</label>
       <input type="file" id="foto_es" accept="image/*" name="foto_es" value="" style="width: 100%;"><!--id="tit"  -->
       <br><br>
       <div style="text-align:center;"><img src="" id="image-field_es" width="250" height="250"></div>
       <br>
       </div>
       
       
       </div>
       </div>










       <div class="card">
        <div style="padding-top: 20px;padding-right: 20px;
  padding-bottom: 20px;
  padding-left: 20px;">
 
 <div class="form-group">
     <label for="body_en" class="mt-3">@lang('home.bl_link')</label>
     <textarea class="form-control" cols="30" rows="10" name="body_en" id="body_en"></textarea>
 
     <!--<input class="form-control" placeholder="" name="link[1]" type="text" value=""><br>-->
     
     </div>
 
        <div class="form-group">
            <label for="foto_en" class="mt-3">@lang('home.bl_title')</label>
        <input type="file" id="foto_en" accept="image/*" name="foto_en" value="" style="width: 100%;"><!--id="tit"  -->
        <br><br>
        <div style="text-align:center;"><img src="" id="image-field_en" width="250" height="250"></div>
        <br>
        </div>
        
        
        </div>
        </div>
       </div>
       <!--------------------------------------------------------------------------------------------------------------------->

       <br>
        <!--<label class="col text-center">
        <a class="btn btn-danger" id="addRow">Add Row</a>
        </label>-->
        &nbsp

       
        <label class="col text-center">
        <input type="submit" name="save" id="save" class="btn btn-success" value="@lang('home.bl_banner1')">
        </label>
        {!! Form::close() !!}
    </div>
    </div>



<!-------------------------------------------------------------------------------------------->
<!-------------------------------------------------------------------------------------------->


    
<!--SCRIPT PARA LA FOTO----------------------------------------------------------------------------------->
    <script>
     $(document).ready(function(){

      var imgVal = $('#foto_es').val(); 
        if(imgVal=='') 
        { 
          
          $('#image-field_es').attr('src','../images/nia.png').fadeIn();
        }

       

$("#foto_es").change(function(){
  
  const file2 = this.files[0];
const  fileType = file2['type'];
const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
if (validImageTypes.includes(fileType)) {
    //alert('NOES');
    var file = document.getElementById("foto_es").files[0];

  var readImg = new FileReader();
  
  readImg.readAsDataURL(file);
  
  readImg.onload = function(e) {
    var span = document.createElement('span');
    $('#image-field_es').attr('src',e.target.result).fadeIn();
    
    }
   
}else{
  Swal.fire({
			
			html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
			'<br><br><h1>Warning!</h1><br>Please select an image with format .gif, .jpeg, .png!',
			
			showConfirmButton: false,
			timer:1800
		  })
  $('#image-field_es').attr('src','../images/nia.png').fadeIn();
}
       
  
    
  })

  $("#foto_es").show(function(){
    const file2 = this.files[0];
const  fileType = file2['type'];
const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
if (validImageTypes.includes(fileType)) {
  
    var file = document.getElementById("foto_es").files[0];

  var readImg = new FileReader();
  
  readImg.readAsDataURL(file);
  
  readImg.onload = function(e) {
    var span = document.createElement('span');
    $('#image-field_es').attr('src',e.target.result).fadeIn();
    
    }
}else{
  Swal.fire({
			
			html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
			'<br><br><h1>Warning!</h1><br>Please select an image with format .gif, .jpeg, .png!',
			
			showConfirmButton: false,
			timer:1800
		  })
  $('#image-field_es').attr('src','../images/nia.png').fadeIn();
}
  
    
  })
        
  
})
</script>
<!-- foto ingles-->
<script>
    $(document).ready(function(){

     var imgVal = $('#foto_en').val(); 
       if(imgVal=='') 
       { 
         
         $('#image-field_en').attr('src','../images/nia.png').fadeIn();
       }

      

$("#foto_en").change(function(){
 
 const file2 = this.files[0];
const  fileType = file2['type'];
const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
if (validImageTypes.includes(fileType)) {
   //alert('NOES');
   var file = document.getElementById("foto_en").files[0];

 var readImg = new FileReader();
 
 readImg.readAsDataURL(file);
 
 readImg.onload = function(e) {
   var span = document.createElement('span');
   $('#image-field_en').attr('src',e.target.result).fadeIn();
   
   }
  
}else{
 Swal.fire({
           
           html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
           '<br><br><h1>Warning!</h1><br>Please select an image with format .gif, .jpeg, .png!',
           
           showConfirmButton: false,
           timer:1800
         })
 $('#image-field_en').attr('src','../images/nia.png').fadeIn();
}
      
 
   
 })

 $("#foto_en").show(function(){
   const file2 = this.files[0];
const  fileType = file2['type'];
const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
if (validImageTypes.includes(fileType)) {
 
   var file = document.getElementById("foto_en").files[0];

 var readImg = new FileReader();
 
 readImg.readAsDataURL(file);
 
 readImg.onload = function(e) {
   var span = document.createElement('span');
   $('#image-field_en').attr('src',e.target.result).fadeIn();
   
   }
}else{
 Swal.fire({
           
           html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
           '<br><br><h1>Warning!</h1><br>Please select an image with format .gif, .jpeg, .png!',
           
           showConfirmButton: false,
           timer:1800
         })
 $('#image-field_en').attr('src','../images/nia.png').fadeIn();
}
 
   
 })
       
 
})
</script>
<!--SCRIPT PARA LA FOTO------------------------------------------------------------------->


    
<!--SCRIPT PARA LA FOTO----------------------------------------------------------------------------------->
   

<!---------------------------------------------------------------------------------------->
    @endsection



@section('content2')

<?php

$postcomentados=array();
$postcantidad=array();
$py=array();
$arr='';
$ct=0;

use App\Post;
$posts=Post::all();
foreach($posts as $post){

$titulopost='';
$counter = 0;
foreach($post->comentarios as $comentario){
    $counter++;
    } $titulopost=$post->titulo;
    
    
    $postcomentados[]=$titulopost;
    $postcantidad[]=$counter;

$ct++;
}



for($i=0;$i<$ct;$i++){   
$py[$postcomentados[$i]]=$postcantidad[$i];

}

arsort($py);?>
@lang('home.bl_gencoment')


<?php
echo "<br>";
echo "<br>";
$i=0; 
foreach($py as $x => $x_value) {?>

	@lang('home.bl_title')
	
	<?php
	
		echo $x." | "?>@lang('home.bl_comments') <?php echo $x_value;
		echo "<br>";
		 
		if($i==9){
			break;
		}
	
		else{
		$i++;}
  }
  
  if($i==0){?>
    @lang('home.bl_nopost')
    <?php }



?>
@endsection

@section('js')
<script>
        CKEDITOR.replace( 'body_es' );
    </script>
    <script>
        CKEDITOR.replace( 'body_en' );
    </script>
@endsection