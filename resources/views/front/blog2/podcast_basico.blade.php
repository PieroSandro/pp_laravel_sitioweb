@extends('layouts.actividadescul')
@section('titleblog')
@lang('home.bl_podcastbasico')

@endsection

<!-- ------------------------------------------------------------------------>

@section('carusel')

<style>
#bt1car:before{
	
	content:'@lang('home.ver_slider')';
	align-content: center;
	font-size:12.4px;/*15px */
	position:absolute;
	top:50%;
	left:50%;
	transition:all 0.3s;
	transform:translate(-50%,-50%);
}

.c1{
  display:inline-block;

  margin:20px;

}
</style>
   <ul id="sb-slider" class="sb-slider" style="max-height:650px; width: 100%; margin: auto;">
           
           
           <?php
               use App\Banner;
               $banners=Banner::all();
               $counter1=1;
               foreach($banners as $banner){
                   ?>
 
 
 
 <li style="max-height:650px; width:100%;">
                         <a href="{{$banner->link}}" target="_blank"><img class="d-block w-100" src="/intranet/storage/app/public/images/{{$banner->foto}}" alt="image_{{$counter1}}"/></a>
             
             
             <div class="sb-description" style="text-align:center; width:40px; height:40px; margin:0 auto;">
                             <h3 style="text-align:center; font-family: 'Montserrat', sans-serif;">{{$counter1}}</h3>
             </div>
               
           </li>
 
 
 
 <?php
 $counter1++;
               }
 
 
 
 
 
 
             ?>
 
 
 
 
 
 
           <div id="nav-arrows" class="nav-arrows">
                     <a href="#">Next</a>
                     <a href="#">Previous</a>
                 </div>
         </ul>
              @endsection


<!----------------------------------------------------------------------------->

@section('content')

<!-------------------------POPOVER---------------------------------->

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<!----------------------POPOVER------------------------------------->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<link rel="stylesheet" href="{{url('/css/animate.css')}}">


<!--------------ANIMATE ON SCROLL------------------------->
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        
        <script>
        AOS.init({
          duration:3000,
        });
        </script>
        <!------------------------------------------->
<br>
<!-- 29_01-->
<label id="qs"></label>

<br>
<br>
<!-- 29_01-->



















<div data-aos="fade-right" align="center" style="font-family: Montserrat, sans-serif;">
<!--<h3>@lang('home.bl_podcastbasico')</h3>-->

<h1 style="color: black; font-family: Montserrat, sans-serif; width:100%;">
@lang('home.bl_podcastbasico')</h1>
<br/>
        
        
  













<!--<div class="pcast-player">
  <div class="pcast-player-controls">
    <button class="pcast-play"><i class="fa fa-play"></i><span>Play</span></button>
    <button class="pcast-pause"><i class="fa fa-pause"></i><span>Pause</span></button>
    <button class="pcast-rewind"><i class="fa fa-fast-backward"></i><span>Rewind</span></button>
    <span class="pcast-currenttime pcast-time">00:00</span>
    <progress class="pcast-progress" value="0"></progress>
    <span class="pcast-duration pcast-time">00:00</span>
    <button class="pcast-speed">1x</button>
    <button class="pcast-mute"><i class="fa fa-volume-up"></i><span>Mute/Unmute</span></button>
  </div>
  <audio src="https://traffic.libsyn.com/podcastsinenglish/animals.mp3"></audio><a class="pcast-download" href="https://traffic.libsyn.com/podcastsinenglish/animals.mp3" download>Download MP3</a>
</div>-->











<!------------------------------------------------------------------------->
@if(count($audios)>0)
            @foreach($audios as $audio)
            @if((time() - strtotime($audio->created_at)) <= 86400)
      
            @else
            
            @endif

            <div class="c1">
<audio id="player" controls style="border-radius: 10px;">
    <source src="{{$audio->link}}" type="audio/mp3" />
    
</audio><br><a class="pcast-download" target="blank" 

onMouseOver="this.style.cssText='color: rgba(228, 20, 20, 0.931); text-decoration:none;'" 
onMouseOut="this.style.cssText='color: rgb(11, 11, 132, 0.993)'" 
style="text-decoration: none; color: rgb(11, 11, 132, 0.993);" 
href="{{$audio->link}}" 
download>@lang('home.modal_descargar') MP3</a>
<br></div>
          
@endforeach
<br>
<br>
{{$audios->links('vendor.pagination.bootstrap-4')}}
@else
<p>@lang('home.bl_nopost')</p>
@endif

<!----------------------------------------------->



<!--
<div class="c1">
<audio id="player" controls style="border-radius: 10px;">
    <source id="audio-field" src="https://traffic.libsyn.com/podcastsinenglish/filmlife.mp3" 
    type="audio/mp3" />
    
</audio><br>
<a class="pcast-download" target="blank"
onMouseOver="this.style.cssText='color: rgba(228, 20, 20, 0.931); text-decoration:none;'" 
onMouseOut="this.style.cssText='color: rgb(11, 11, 132, 0.993)'" 
style="text-decoration: none; color: rgb(11, 11, 132, 0.993);" 
href="https://traffic.libsyn.com/podcastsinenglish/filmlife.mp3" 
download>@lang('home.modal_descargar') MP3</a>
<br></div>

<div class="c1">
<audio id="player" controls style="border-radius: 10px;">
    <source src="{{url('/images/FEY-NI_TU_NI_NADIE.mp3')}}" 
    type="audio/mp3" />
   
</audio><br><a class="pcast-download" target="blank"
onMouseOver="this.style.cssText='color: rgba(228, 20, 20, 0.931); text-decoration:none;'" 
onMouseOut="this.style.cssText='color: rgb(11, 11, 132, 0.993)'" 
style="text-decoration: none; color: rgb(11, 11, 132, 0.993);" 
href="{{url('/images/FEY-NI_TU_NI_NADIE.mp3')}}" 
download>@lang('home.modal_descargar') MP3</a>
<br></div>-->






<!----------------------------------------------------------->
<!--
<div class="c1">
<audio id="player" controls style="border-radius: 10px;">
    <source src="https://public.boxcloud.com/api/2.0/internal_files/43756094385/versions/44251836529/representations/mp3/content/?access_token=1!VHiFzZ_H22jPdKdwFYYNdG_KzeL6xvYjiMpjGJp2in4DX8sb-A8B2Y-za2nkAVeBK0lKyIYLSDJs6x5oeFG_fMEoQ8cotIUvTb1_jzQQf30sASW1YuLi3NtZwF0ALZbdMyP1YycuWaR8hqZ4l1X3vQraMlZsIl-hXcnWkketI48eyF6GWuYFZEchA40L2wxPAjoAudvAGu0_8auKY6LTxnv6NMqaihlkV57KuHFL5W0lPezWe_1KQthBTk_crw5WhBAUjYjXZGsqWi_Fic8-Q9wjwM7LuNiuYOfEtDJvEmDP3aoaVQq67NOmOGu7VJzRZsHEMjvYoHEKJCfYtsTdtcwYZBAiB9QlYtWonDt4TM9HvuoeBcW5yl6JRHxgFoURKyB1gRFVjM5o9Lep7IevL5LYQxX9j26ebdzzVDnwOhWSkXAq8xD7yDkQ2PWC0nXZ-RHrUaICsgotThOAH5POsjg3qVz12PtWDNIymfwOCf6MF7YnG2tkpOqTb4-GFtQx1JIfI4yRZbqlq_pvrlCX_aJ-ZtLVX5e3Ya4La1mpH_-p1Y2rA5a75nRfJIAz6A..&box_client_name=box-content-preview&box_client_version=2.26.0" type="audio/mp3" />
    
</audio><br><a class="pcast-download" target="blank" href="https://content.production.cdn.art19.com/validation=1561073380,f4180ba0-0696-51c0-9534-7bb8c1bd29fa,WZzn9Bo2gFnoOXh6T2AW822BTTg/episodes/07c25acc-c435-417d-bb81-f872721ba691/06d920f7ffdac9f478b5077cbe52ae0337752339e18657747ba06163b7157d4df8976a34e8cfb21cae62220cd6ebc04beee5a4e4a0ea060bc7dd1ce65acbd3ec/Arena%20Rerun%20-%20Complete.mp3" download>Download MP3</a>
<br></div>

<div class="c1">
<audio id="player" controls style="border-radius: 10px;">
    <source src="http://feedproxy.google.com/~r/kqedforum/~5/xsL8qr1jJYM/Forum20190531bb.mp3" type="audio/mp3" />
</audio><br><a class="pcast-download" target="blank" href="http://feedproxy.google.com/~r/kqedforum/~5/xsL8qr1jJYM/Forum20190531bb.mp3" download>Download MP3</a>
<br></div>-->



<!------------------------------------------------------------>
<!--
<div class="c1">
<audio id="player" controls style="border-radius: 10px;">
    <source src="http://podcastapp.ehubsoft.net/?m=isearch#" type="audio/mp3" />
    
</audio><br><a class="pcast-download" target="blank" href="http://podcastapp.ehubsoft.net/?m=isearch#" download>Download MP3</a>
<br></div>

<div class="c1">
<audio id="player" controls style="border-radius: 10px;">
    <source src="" type="audio/mp3" />
</audio><br><a class="pcast-download" target="blank" href="https://serve.castfire.com/audio/3584615/3584615_2019-01-10-174615.mp3" download>Download MP3</a>
<br></div>-->


<!-------------------7-------7--------7-----------7---------audio------------>

<!--
<div class="c1">
<audio id="player" controls style="border-radius: 10px;">
    <source src="" type="audio/mp3" />
  
</audio><br><a class="pcast-download" target="blank" href="https://serve.castfire.com/audio/3584615/3584615_2019-01-10-174615.mp3" download>Download MP3</a>
<br></div>-->

</div>
<script>
    $( document ).ready(function() {
      function Scrolldown() {
     
     document.getElementById("qs").scrollIntoView();
}

window.onload = Scrolldown;
    });

    $( window ).on( "load", function() {
      function Scrolldown() {
     
     document.getElementById("qs").scrollIntoView();
}

window.onload = Scrolldown;
    });
 
   
    </script>
@endsection

<!-------------------------------------------------------------->

@section('content2')

<?php

$postcomentados=array();
$postcantidad=array();
$linkcomentados=array();

$py=array();
$arr='';
$ct=0;

use App\Post;
      $posts=Post::all();
//use App\Post;
//$posts=Post::all();
foreach($posts as $post){

$titulopost='';
$linkpost='';
$fpo='';
$counter = 0;
foreach($post->comentarios as $comentario){
    $counter++;
    } //$titulopost=$post->titulo;

    $pbody=$post->body;
    //----------------------------------------------------------------------------
    
$extract=substr(htmlspecialchars(trim(strip_tags($pbody))), 0, 150);



$pbody1=htmlspecialchars(trim(strip_tags($pbody)));
//$extract=mb_strimwidth($bodypost, 1, 90, "...");
if(strlen($pbody)>151){

  $locale=App::getLocale();
//echo $extract, PHP_EOL;
if(App::isLocale('en')){
$pbo=htmlspecialchars_decode($extract, ENT_NOQUOTES).'... <small><a href="/intranet/public/blog/home/'.$post->id.'" style="text-decoration: none; color: blue;">
Read more</a></small>';
}else{
  $pbo=htmlspecialchars_decode($extract, ENT_NOQUOTES).'... <small><a href="/intranet/public/blog/home/'.$post->id.'" style="text-decoration: none; color: blue;">
Leer más</a></small>';
}
/*echo "... ";?>
<small><a href="/intranet/public/blog/home/{{$post->id}}" style="text-decoration: none; color: blue;">
@lang('home.bl_lmas')</a></small>
<?php*/
}
else{
 $pbo=htmlspecialchars_decode($pbody1, ENT_NOQUOTES);
}

//-------------------------------------------------------------
$fpost=$post->foto;
$fpo='';
if($fpost=='noimage.jpg'){  
$fpo='';
}else{



$fpo='/intranet/storage/app/public/images/{{$post->foto}}';

}

//$fpo="<img src='/intranet/storage/app/public/images/".$post->foto."' />";

$tpost="<div style='color:blue'>".$post->titulo."</div>";

    //-----------------------------------------------------------------------------
    $linkpost="
    <style>
   .popover {
    
    color: black;
    text-align:justify;
    background-color: white;
  }
  
  .popover-title {
    background-color: coral; 
    color: coral; 
    font-size: 40px;
    text-align:center;
  }
 
  .popover-content {
    background-color: green;
    color: blue;
    padding: 25px;
  }
  
  .arrow {
    border-right-color: red !important;
  }
   
   
   </style>
    <a href='/intranet/public/blog/home/".$post->id."'
    title='".$post->titulo."' data-by='".$pbo."' data-img='/intranet/storage/app/public/images/".$post->foto."' class='popi' data-toggle='popover' data-placement='bottom' style='text-decoration: none; color: blue; font-family: Montserrat, sans-serif;'>".$post->titulo."</a>

   ";?>
   
<?php
    $linkcomentados[]=$linkpost;
    //$postcomentados[]=$titulopost;
    $postcantidad[]=$counter;

$ct++;
}



for($i=0;$i<$ct;$i++){   
$py[$linkcomentados[$i]]=$postcantidad[$i];

}

arsort($py);?>

<div style="text-align:center;">
<div class="plustar1" id="plustar2"><div style="margin:10px; padding-left:10px; padding-right:10px; padding-top:2px; text-align:center; float: center; color:white; font-family: 'Montserrat', sans-serif;"><strong><i class="fas fa-star plustar" style="color:yellow"></i>  @lang('home.bl_gencoment')  <i class="fas fa-star plustar" style="color:yellow"></i></strong></div></div>
</div>
<!--@lang('home.bl_gencoment')-->


<?php
echo "<br>";
echo "<br>";
$i=0; 
foreach($py as $x => $x_value) {?>
  <label style="font-family: Montserrat, sans-serif;">
  <b>
	@lang('home.bl_title')
  </b>
	</label>
	<?php
	
    echo $x."&nbsp<label style='font-family: Montserrat, sans-serif;'><b>|</b></label>&nbsp"?><label style="font-family: Montserrat, sans-serif;"><b>@lang('home.bl_comments') </b></label>&nbsp<?php 
    echo "<label style='font-family: Montserrat, sans-serif;'>";
    echo $x_value;
    echo "</label>";
		echo "<br>";
		 
		if($i==9){
			break;
		}
	
		else{
		$i++;}
  }
  
  if($i==0){?>
    @lang('home.bl_nopost')
    <?php }


//--------------------------------
/*$age = array("Ben"=>"37", "Peter"=>"35", "Joe"=>"43");
print_r($age);
arsort($age);

foreach($age as $x => $x_value) {
    echo "Key=" . $x . ", Value=" . $x_value;
    echo "<br>";
}*/
//------------------------------------------------


$counter1 = 0;
$counter2= 0;
foreach($posts as $post){
    $counter1++;
      if((time() - strtotime($post->created_at)) <= 86400)
      {
          $counter2++;
      }
		}

$counter1;
$counter2;



?>

<script>

   $('.popi').popover({
    delay: 1150,
          //trigger: 'focus',
		  trigger: 'hover',
          html: true,
          content: function () {

            if($(this).data('img')=='/intranet/storage/app/public/images/noimage.jpg')
            {
            return $(this).data("by");
            }else{
              
				return $(this).data("by")+'<br><br><img class="img-fluid" src="'+$(this).data('img') + '" />';
            }
          },
          title: 'Toolbox'
    });   
 
 </script>
 <script>



          $(function(){

            
            
            $("#NumPosts").html("Total: <?php echo $counter1 ?>");

            var c2 = "<?php echo $counter2 ?>";
            if(c2>0){   
            $("#NewPosts").html(" @lang('home.bl_newpost2') <i class='fas fa-file comicon2' style='color:white'></i> : <?php echo $counter2 ?>");
            }else{
              $("#NewPosts").hide();
            }

            $("#dance_id2").hide();

            $("#danza_id").mouseover(function(){
              $("#dance_id1").hide();
              $("#dance_id2").show();
                });

            $("#danza_id").mouseout(function(){
              $("#dance_id2").hide();
              $("#dance_id1").show();
                });
            
              
          
          });
        </script>
@endsection