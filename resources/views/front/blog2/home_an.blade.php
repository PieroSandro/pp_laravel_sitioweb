@extends('layouts.mainblog')
@section('titleblog')



@lang('home.bl_listan')

@endsection
@section('carusel')

<style>
#bt1car:before{
	
	content:'@lang('home.ver_slider')';
	align-content: center;
	font-size:12.4px;/*15px */
	position:absolute;
	top:50%;
	left:50%;
	transition:all 0.3s;
	transform:translate(-50%,-50%);
}
</style>
   <div class="carousel slide" id="MagicCarousel" data-ride="carousel">
                <ol id="myCarousel-indicators" class="carousel-indicators">
                  <li class="active" data-slide-to="0" data-target="#MagicCarousel"></li>
                  <li data-slide-to="1" data-target="#MagicCarousel"></li>
                  <li data-slide-to="2" data-target="#MagicCarousel"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                      <div class="carousel-item active">
                          
                          <img class="d-block w-100" src="{{url('/images/banner-de-iniciacion-1.png')}}" alt="First slide">
                          <div class="carousel-caption">
                          
                               <a class="btn animated shake" href="https://elcomercio.pe/" id="bt1car" target="_blank" role="button">
                                
                               </a>

                          </div>  
                      </div>
                      <div class="carousel-item">
                          <img class="d-block w-100" src="{{url('/images/BANER-DE-INICIACION-2.png')}}" alt="Second slide">
                          <div class="carousel-caption">
                          
                                  <a class="btn animated shake" href="https://www.goltelevision.com/" id="bt1car" target="_blank" role="button">
                                
                               </a>
                          </div>  
                      </div>
                      <div class="carousel-item">
                          <img class="d-block w-100" src="{{url('/images/baner-de-iniciacion-3.png')}}" alt="Third slide">
                          
                          <div class="carousel-caption">
                          <a class="btn animated shake" href="https://elcomercio.pe/" id="bt1car" target="_blank" role="button">
                                
                               </a> 
                                  
                          </div>  
                      </div>
                </div>
                <a class="carousel-control-prev" href="#MagicCarousel"
            data-slide="prev" role="button">
              <span class="carousel-control-prev-icon"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#MagicCarousel"
            data-slide="next" role="button">
              <span class="carousel-control-next-icon"></span>
              <span class="sr-only">Next</span>
            </a>
              </div>
              @endsection
@section('content')
<style>
#txt4{
    /*max-height: 410px;
    max-width:350px;
    min-width:50%;*/
    overflow: auto;
    width:100%;
}
</style>
<div class="text">
<div style="padding-top: 40px;
  padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">
  
<h1>@lang('home.bl_an2')</h1>
@if(session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
                @elseif(session('danger'))
                <div class="alert alert-danger">
                    {{session('danger')}}
                </div>
            @endif
@csrf
            
@if(count($anuncios)>0)
@foreach($anuncios as $anuncio)
	<div class="card">


		<div style="padding-top: 20px;padding-right: 20px;
  padding-bottom: 20px;
  padding-left: 20px;">
		<h3><a href="/intranet/public/index/anuncios/{{$anuncio->id}}" style="text-decoration: none;"> {{$anuncio->titulo}}</a></h3>
        <div id="txt4">
        <h4>{!!$anuncio->body!!}</h4>
</div>
        <h4> @lang('home.bl_vanuncio'): {{$anuncio->fecha_vence}}</h4>
		<small> @lang('home.bl_fcreacion'): {{$anuncio->created_at}}</small>
        <small>| @lang('home.bl_factualizacion'): {{$anuncio->updated_at}}</small>
            </div>

        
	</div>
	<br>
@endforeach
	{{$anuncios->links()}}
@else
<p>@lang('home.bl_an3')</p>
@endif
  </div>
</div>
@endsection

@section('content2')


@endsection