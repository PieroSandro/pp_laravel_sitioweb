@extends('layouts.mainblog')
@section('titleblog')

@lang('home.bl_banner1')

@endsection

@section('carusel')
@endsection



@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js">
</script>
<div class="text">
<div style="padding-top: 40px;
  padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<h1>Create banner</h1>
<br>
<!-----------------------------------------------FORMULARIO: OPEN--------------------------------------------------------------------->
    {!! Form::open(['action'=> 'HomeController@storebanner', 'method'=> 'POST','enctype'=>'multipart/form-data']) !!}
       @csrf
            @if(session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>

            @endif
                
        <?php $filefoto=''; ?>
        
        <div class="form-group">

        <label for="foto" class="mt-3">@lang('home.bl_foto')</label>        
       
            <div class="col-md-10">
             
                                <input id="foto" accept='image/*' type="file" name="foto" value="" style="width: 100%;">
                                <br><br>
                                <div><img src="" id="image-field" width="200" height="250"></div>
                                @if ($errors->has('foto'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('foto') }}</strong>
                                </span>
                                @endif
                            </div>
                            <br>
        </div>
        <div class="form-group">

                <label for="link" class="mt-3">@lang('home.bl_link')</label>
        
                    {{Form::text('link','',['class'=>'form-control','placeholder'=>''])}}
        
                           <br>
                    @if($errors->has('link'))
                    @foreach($errors->get('link') as $error)
        
                    <div class="alert alert-danger">
                    Error
                    </div>
                    @endforeach
                    @endif
        
                </div>          
        <br>
       
        <input type="submit" class="btn btn-success" value="@lang('home.bl_banner1')">
    {!! Form::close() !!}
    <!-----------------------------------------------FORMULARIO: CLOSE--------------------------------------------------------------------->
    </div>
    </div>




<script>
     $(document).ready(function(){

      var imgVal = $('#foto').val(); 
        if(imgVal=='') 
        { 
          //alert('niko');
          $('#image-field').attr('src','../images/nia.png').fadeIn();
        }

       

$("#foto").change(function(){
  //---------------------------------------
  const file2 = this.files[0];
const  fileType = file2['type'];
const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
if (validImageTypes.includes(fileType)) {
    //alert('NOES');
    var file = document.getElementById("foto").files[0];

  var readImg = new FileReader();
  
  readImg.readAsDataURL(file);
  
  readImg.onload = function(e) {
    var span = document.createElement('span');
    $('#image-field').attr('src',e.target.result).fadeIn();
    
    }
    //document.getElementById("image-field").src = "../images/nia.png";
}else{
  Swal.fire({
			
			html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
			'<br><br><h1>Warning!</h1><br>Please select an image with format .gif, .jpeg, .png!',
			
			showConfirmButton: false,
			timer:1800
		  })
  $('#image-field').attr('src','../images/nia.png').fadeIn();
}
        //----------------------------
  
    
  })

  $("#foto").show(function(){
    const file2 = this.files[0];
const  fileType = file2['type'];
const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
if (validImageTypes.includes(fileType)) {
    // invalid file type code goes here.
    //alert('NOES');
    //document.getElementById("image-field").src = "../images/nia.png";
    var file = document.getElementById("foto").files[0];

  var readImg = new FileReader();
  
  readImg.readAsDataURL(file);
  
  readImg.onload = function(e) {
    var span = document.createElement('span');
    $('#image-field').attr('src',e.target.result).fadeIn();
    
    }
}else{
  Swal.fire({
			
			html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
			'<br><br><h1>Warning!</h1><br>Please select an image with format .gif, .jpeg, .png!',
			
			showConfirmButton: false,
			timer:1800
		  })
  $('#image-field').attr('src','../images/nia.png').fadeIn();
}
  
    
  })
        
  
})
</script>
  <!---------------------------------------------------------------------------------->  
    @endsection



@section('content2')

<?php

$postcomentados=array();
$postcantidad=array();
$py=array();
$arr='';
$ct=0;

use App\Post;
$posts=Post::all();
foreach($posts as $post){

$titulopost='';
$counter = 0;
foreach($post->comentarios as $comentario){
    $counter++;
    } $titulopost=$post->titulo;
    
    
    $postcomentados[]=$titulopost;
    $postcantidad[]=$counter;

$ct++;
}



for($i=0;$i<$ct;$i++){   
$py[$postcomentados[$i]]=$postcantidad[$i];

}

arsort($py);?>
@lang('home.bl_gencoment')


<?php
echo "<br>";
echo "<br>";
$i=0; 
foreach($py as $x => $x_value) {?>

	@lang('home.bl_title')
	
	<?php
	
		echo $x." | "?>@lang('home.bl_comments') <?php echo $x_value;
		echo "<br>";
		 
		if($i==9){
			break;
		}
	
		else{
		$i++;}
  }
  
  if($i==0){?>
    @lang('home.bl_nopost')
    <?php }


//--------------------------------
/*$age = array("Ben"=>"37", "Peter"=>"35", "Joe"=>"43");
print_r($age);
arsort($age);

foreach($age as $x => $x_value) {
    echo "Key=" . $x . ", Value=" . $x_value;
    echo "<br>";
}*/
//------------------------------------------------
?>


@endsection

@section('js')
<script>
        CKEDITOR.replace( 'body' );
    </script>
@endsection