@extends('layouts.mainblog')
@section('titleblog','Post: '.$post->titulo)





@section('carusel')


              @endsection




@section('content')

<!-------------------------POPOVER---------------------------------->

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<!----------------------POPOVER------------------------------------->



<!--------12_01_2020-------->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<link rel="stylesheet" href="{{url('/css/animate.css')}}">







<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        
        <script>
        AOS.init({
          duration:3000,
        });
        </script>











<style>

.swal2-title{
   
    font-family: 'Montserrat', sans-serif;
}


#swal2-content{
   
   font-family: 'Montserrat', sans-serif;
}

.swal2-modal .swal2-styled{
    
    background-color: rgba(11, 11, 132, 0.993);
    font-family: 'Montserrat', sans-serif;
}
#txt4{
    /*max-height: 410px;
    max-width:350px;
    min-width:50%;*/
    text-align:justify;
    overflow: auto;
    width:100%;
}

.conew1{
	/*border: none;
	outline: none;*/
	background: rgba(4, 10, 100, 0.808);
	min-width: 120px;
	min-height: 35px;
	border-radius: 20px;
	/*cursor: pointer;
	margin: 10px;*/
}

@-webkit-keyframes conew1 {
  to {
    -webkit-transform: scale(1.03);
    transform: scale(1.03);/* 1.1*/
  }
}
@keyframes conew1 {
  to {
    -webkit-transform: scale(1.03);
    transform: scale(1.03);
  }
}
.conew1 {
  display: inline-block;
  vertical-align: middle;
  -webkit-transform: perspective(1px) translateZ(0);
  transform: perspective(1px) translateZ(0);
  box-shadow: 0 0 1px rgba(0, 0, 0, 0);

  -webkit-animation-name: conew1;
  animation-name: conew1;
  -webkit-animation-duration: 0.3s;
  animation-duration: 0.3s;
  -webkit-animation-timing-function: linear;
  animation-timing-function: linear;
  -webkit-animation-iteration-count: infinite;
  animation-iteration-count: infinite;
  -webkit-animation-direction: alternate;
  animation-direction: alternate;
}

/*
.conew1:hover, .hvr-pulse-grow:focus, .hvr-pulse-grow:active {
  -webkit-animation-name: conew1;
  animation-name: conew1;
  -webkit-animation-duration: 0.3s;
  animation-duration: 0.3s;
  -webkit-animation-timing-function: linear;
  animation-timing-function: linear;
  -webkit-animation-iteration-count: infinite;
  animation-iteration-count: infinite;
  -webkit-animation-direction: alternate;
  animation-direction: alternate;
}*/
</style>
<div class="text" style="font-family: 'Montserrat', sans-serif;">
<div style="padding-top: 40px;
  padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">
<a href="/intranet/public/blog/home" class="btn" 
style="color: white; border: rgba(228, 20, 20, 0.931); background-color:rgba(228, 20, 20, 0.931) ;font-family: Montserrat, sans-serif;"><i class="fas fa-angle-left"></i> @lang('home.bl_back')</a>
<br><br>

<!--<h1>{{$post->titulo}}</h1>-->
<h1 style="color: black; font-family: Montserrat, sans-serif; width:100%;">{{$post->titulo}}</h1>

<br>
<div>
<div id="txt4">
<h5 style="color: black; font-style: normal; text-align: justify; font-family: Montserrat, sans-serif;">{!!$post->body!!}</h5>
</div>
<br>






<?php
$fotopost='';
$fotopost=$post->foto;

if($fotopost=='noimage.jpg'){  
?> <?php
}else{?>




<div style="text-align:center;">
<img src="/intranet/storage/app/public/images/{{$post->foto}}" style="width:90%; float:center;">
</div>
<?php
}
?>
</div>
<hr>
<small>@lang('home.bl_fcreacion'): {{$post->created_at}}</small><br>
        <small>@lang('home.bl_factualizacion'): {{$post->updated_at}}</small>  
<hr>
<div class="sharethis-inline-share-buttons"></div>

@guest

@else
<a href="/intranet/public/blog/home/{{$post->id}}/edit" class="btn btn-primary"><i class="fas fa-pen"></i> @lang('home.bl_edit4')</a>



<button data-toggle="modal" data-target="#delete" class="btn btn-danger">
<i class="fas fa-trash"></i> @lang('home.bl_borrar')
</button>

{!!Form::close()!!}
@endguest

<br>
<br>
@if(session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>

            @endif
<br>
<div style="padding-left: 20px;">
<strong>@lang('home.bl_totcomentarios'):</strong>
<?php
$counter = 0;
foreach($post->comentarios as $comentario){
		$counter++;
		}

$counter;

echo $counter;
?>
</div>
<br>
<br>
<div class="row" >

	
	<div class="col-12 col-md-offset-2" id="mostrar">
        <!---------------------->
    <!---------------------->
    
    <!---------------------->
    <!---------------------->
		@foreach($post->comentarios as $comentario)
		<div class="card"  style="padding-top: 20px;padding-right: 20px;
  padding-bottom: 20px;
  padding-left: 20px;">
            <!------------------------------------------------------------>
        
            @if((time() - strtotime($comentario->created_at)) <= 86400)

            <div style="padding-top: 0px;
  padding-bottom: 20px;">
            <div class="conew1" id="conew2"><div style="padding-left:10px; padding-right:10px; padding-top:5px; text-align:center; float: center; color:white; font-family: 'Montserrat', sans-serif;"><strong>@lang('home.bl_newcom') <i class="fas fa-comment-dots comicon" style="color:white"></i></strong></div></div>
            </div>
            @else

        @endif
        <!------------------------------------------------------------>
		
			<p><strong>@lang('home.bl_bname'):</strong> {{$comentario->nick}}</p><br>
			
			@guest
			<p><strong>@lang('home.bl_coment'):</strong> {{$comentario->contenido}}</p><br>
			<p><small>@lang('home.bl_fcreacion'): {{$comentario->created_at}}</small></p>
			


			
@else
<p><strong>@lang('home.bl_coment'):</strong> {{$comentario->contenido}}</p><br>
<p><strong>@lang('home.bl_correo'):</strong> {{$comentario->correo}}</p><br>
<p><small>@lang('home.bl_fcreacion'): {{$comentario->created_at}}</small></p>
<br>
{!!Form::open(['action'=>['ComentarioController@destroy',$post->id,$comentario->id],'method'=>'POST','class'=>'float-right'])!!}
@csrf
{{Form::hidden('_method','DELETE')}}

<button type="submit" class="btn btn-danger">
<i class="fas fa-trash"></i> @lang('home.bl_borrar')
</button>
<br><br>
{!!Form::close()!!}
<br><br>
@endguest

@foreach($comentario->respuestas as $respuesta)
<br>
<div class="card"  style="padding-top: 20px;padding-right: 20px;
  padding-bottom: 20px;
  padding-left: 20px;">
		


        <p><strong>@lang('home.bl_bname'):</strong> Vilma Juárez</p><br>

		<p><strong>@lang('home.bl_coment'):</strong> {{$respuesta->contenido}}</p><br>
		<p><small>@lang('home.bl_fcreacion'): {{$respuesta->created_at}}</small></p>
		
		@guest
			
	
@else
<br>
{!!Form::open(['action'=>['RespuestaController@destroy',$post->id,$comentario->id,$respuesta->id],'method'=>'POST','class'=>'float-right'])!!}
@csrf
{{Form::hidden('_method','DELETE')}}

<input type="submit" class="btn btn-danger" value="@lang('home.bl_borrar')">
<!--{{Form::submit('Delete',['class'=>'btn btn-danger'])}}-->
{!!Form::close()!!}

@endguest
	</div>
	<p style="margin: 10px 0;"> </p>
	@endforeach
	<br><!-- br-->
@guest

@else
	<div class="row">
	<div id="comment-form" class="col-12 col-md-offset-2" style="margin-top:0px;">
		{!!Form::open(['action'=>['ComentarioController@store',$post->id,$comentario->id],'method'=>'POST','enctype'=>'multipart/form-data'])!!}
		@csrf
		

			<div class="row">


				<div class="col-lg-12">
										
						
						<label for="contenido" class="mt-3">@lang('home.bl_responder')</label>

						{{Form::textarea('contenido',null,['class'=>'form-control','rows'=>'5']) }}
						
						

						
					<br>
				</div>

				
				<input type="submit" class="btn btn-success" style="margin-left:15px;" value="@lang('home.bl_arespuesta')">
        
				{!!Form::close()!!}
			</div>		
		
	</div>
</div>
@endguest
		</div>
		<br>








        @endforeach


        <!--------------------------------------------------->
        <!--------------------------------------------------->
        

<!--------------------------------------------------->
<!--------------------------------------------------->
	</div>

</div>
<div class="row">
	<div id="comment-form" class="col-lg-12" style="margin-top:50px;">
		<!--{!!Form::open(['route'=>['storecoment',$post->id],'method'=>'POST','enctype'=>'multipart/form-data'])!!}-->


        <form class="sf1" id="submit_form1" action="{{route('storecoment',[$post->id])}}" method="POST" enctype="multipart/form-data">
        
		<!--<form class="sf1" id="submit_form1" method="POST">-->
        
		@csrf
		
            
			<div class="row">
			
				<div class="col-xs-12 col-sm-6 col-md-12 col-lg-6">
						
						<label for="nick" class="mt-3">@lang('home.bl_bname2')</label>
						<!--{{Form::text('nick',null,['class'=>'form-control']) }}-->
						<input type="text" class="form-control" name="nick" id="nick" style="box-shadow: 0 0 0px;">
            

						<br>
            
			<label id="lblName" style="color:red"></label>
            <label id="lblName2" style="color:green"></label>
				</div>




				<div class="col-xs-12 col-sm-6 col-md-12 col-lg-6">
						
						<label for="correo" class="mt-3">@lang('home.bl_bcorreo2')</label>
						<input type="text" class="form-control" name="correo" id="correo" style="box-shadow: 0 0 0px;">
            
						<br>
            
			<label id="lblEmail" style="color:red"></label>
            <label id="lblEmail2" style="color:green"></label>
        
				</div>



				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										
						<label for="contenido" class="mt-3">@lang('home.bl_bconte2')</label>
						<!--{{Form::textarea('contenido',null,['class'=>'form-control','rows'=>'5']) }}-->
						<textarea class="form-control" cols="30" rows="5" name="contenido" id="contenido" style="box-shadow: 0 0 0px;"></textarea>

						<br>

			
			<label id="lblMessage" style="color:red"></label>
                  <label id="lblMessage2" style="color:green"></label>
                        <br><br>
						</div>
						
						<br>
						<br>
						<br>
						&nbsp&nbsp&nbsp
                        <!--
<input type="submit" class="btn btn-success" name="submit" id="submit" 
value="@lang('home.bl_addcoment')">-->

<label class="col text-center">



        <!--<input type="submit" name="save" id="save" class="btn btn-success" value="@lang('home.bl_banner1')">
        
        -->
        <button type="submit" name="submit" id="submit" class="btn" 
        style="color: white; border: rgba(11, 11, 132, 0.993); background-color:rgba(11, 11, 132, 0.993) ;font-family: Montserrat, sans-serif;">
<i class="fas fa-pen"></i> @lang('home.bl_addcoment')
</button>
        </label>

						<!--{!!Form::close()!!}-->
						<span id="error_message" class="text-danger"></span>
            <span id="success_message" class="text-success"></span>
        
        
        </form>
			</div>		
		
	</div>
</div>
<br>
</div>
</div>

<div class="modal modal-danger fade" id="delete" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<a href="#" class="navbar-brand"><img src="{{url('/images/CCURRlogo.png')}}" style="width:65px;"></a>
          
        <h4 class="modal-title lang" style="margin:0 auto;" key="news">@lang('home.bl_deleteconf')</h4>
      
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

            
</div>
{!!Form::open(['route'=>['destroy',$post->id],'method'=>'POST','class'=>'float-right'])!!}
@csrf
{{Form::hidden('_method','DELETE')}}

<div class="modal-body">
		<p class="text-center">
		@lang('home.bl_modalq') <?php echo "'"?>{{$post->titulo}}<?php echo "'?"?>
		</p>
		
</div>		
<div class="modal-footer">
<button type="button" class="btn btn-success" data-dismiss="modal">@lang('home.bl_modalcancel')</button>
<button type="submit" class="btn btn-danger">@lang('home.bl_modaldelete')</button>
</div>
{!!Form::close()!!}
</div>
</div>
</div>

<!-------------------------------->
<script>
	$(function(){

		$("#lblName").hide();
        $("#lblEmail").hide();
        $("#lblMessage").hide();

				var name=false;
                var email=false;
                var message=false;
                var i_name=0;
                var i_email=0;
                var i_message=0;
                var s_name='';
                var s_email='';
				var s_message='';
				
				$("#nick").keypress(function(){
                    check_name();
                });

                $("#nick").keydown(function(){
                    check_name();
                });

                $("#nick").keyup(function(){
                    check_name();
                });

                $("#nick").focusin(function(){
                    check_name();
                });

               
                

                $("#nick").focusout(function(){
                    check_name2();
                });
//------------------------------------------------------
                $("#correo").keypress(function(){
                    check_email();
                });

                $("#correo").keydown(function(){
                    check_email();
                });

                $("#correo").keyup(function(){
                    check_email();
                });

                $("#correo").focusin(function(){
                    check_email();
                });

                $("#correo").focusout(function(){
                    check_email2();
                });
//-------------------------------------------------
                $("#contenido").keypress(function(){
                    check_message();
                });

                $("#contenido").keydown(function(){
                    check_message();
                });

                $("#contenido").keyup(function(){
                    check_message();
                });

                $("#contenido").focusin(function(){
                    check_message();
                });

                $("#contenido").focusout(function(){
                    check_message2();
                });		

				//----------------------------------------------------------------
				function check_name(){
                    var name_length = $("#nick").val().length;
                    
                   
                        if(name_length<5){
                            if(name_length==0){
                                $("#greencheck").hide();
                       $("#lblName2").hide();
                       $("#lblName").fadeIn();
                   $("#lblName").html("@lang('home.cf_nombre')");
                   
                   document.getElementById("nick").style.borderColor = "red";
                  document.getElementById("nick").style.backgroundColor = "#FFEBEB";
                  document.getElementById("nick").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                   name=true;
                   i_name++;
                   s_name='@lang('home.cf_nombre')<br>';
     
               }else{
                if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#nick").val())){
                       // if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                        $("#greencheck").hide();
                            $("#lblName2").hide();
                        
                        $("#lblName").fadeIn();
                        $("#lblName").html("@lang('home.cf_nombre2')");//html("@lang('home.mensaje_contactenos')");
                        
                        document.getElementById("nick").style.borderColor = "red";
                       document.getElementById("nick").style.backgroundColor = "#FFEBEB";
                       document.getElementById("nick").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        name=true;
                        i_name++;
                        s_name='@lang('home.cf_nombre2')<br>';
                        }else{
                            $("#greencheck").hide();
                            $("#lblName2").hide();
                            $("#lblName").fadeIn();
                        $("#lblName").html("@lang('home.cf_nombre3')");
                        
                        document.getElementById("nick").style.borderColor = "red";
                       document.getElementById("nick").style.backgroundColor = "#FFEBEB";
                       document.getElementById("nick").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        name=true;
                        i_name++;
                        s_name='@lang('home.cf_nombre3')<br>';
                        }
                        //i_name++;
                      }
                        
                    }
                    else if(name_length>45){
                        $("#greencheck").hide();
                        $("#lblName2").hide();
                        
                        $("#lblName").fadeIn();

                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#nick").val())){
                            
                        
                        $("#lblName").html("@lang('home.cf_nombre4')");//html("@lang('home.mensaje_contactenos')");
                        s_name='@lang('home.cf_nombre4')<br>';
                        }else{
                            $("#lblName").html("@lang('home.cf_nombre5')");
                            s_name='@lang('home.cf_nombre5')<br>';
                        }

                        document.getElementById("nick").style.borderColor = "red";
                       document.getElementById("nick").style.backgroundColor = "#FFEBEB";
                       document.getElementById("nick").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        name=true;
                    i_name++;
                    
                    }

                    else{
                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#nick").val())){
                            //if((/^[a-zA-Z]+$/.test($("#name").val())) || (/^ +$/.test($("#name").val()))){
                            document.getElementById("nick").style.borderColor = "green";
                        document.getElementById("nick").style.backgroundColor = "#E4FFEF";
                        document.getElementById("nick").style.boxShadow = "0 0 1.5px 2.9px #75DA8D";
                        $("#lblName").hide();
                        $("#lblName2").html("@lang('home.cf_nombre6')");
                        $('#lblName2').fadeIn();
                        $('#greencheck').fadeIn();
                        }else{
                            $("#greencheck").hide();
                            $("#lblName2").hide();
                        
                        $("#lblName").fadeIn();
                        $("#lblName").html("@lang('home.cf_nombre7')");
                        document.getElementById("nick").style.borderColor = "red";
                       document.getElementById("nick").style.backgroundColor = "#FFEBEB";
                       document.getElementById("nick").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        name=true;
                        i_name++;
                        s_name='@lang('home.cf_nombre7')<br>';
                        }
                        
                    }
                    //i_name=1;
                }

                function check_name2(){
                    var name_length = $("#nick").val().length;
                    
                    
                if(name_length<5){


                    if(name_length==0){
                        $("#greencheck").hide();
                       $("#lblName2").hide();
                       $("#lblName").fadeIn();
                   $("#lblName").html("@lang('home.cf_nombre')");
                   
                   document.getElementById("nick").style.borderColor = "red";
                  document.getElementById("nick").style.backgroundColor = "#FFEBEB";
                  document.getElementById("nick").style.boxShadow = "0 0 0px 0px";
                   name=true;
                   i_name++;
                   s_name='@lang('home.cf_nombre')<br>';
               }else{

                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#nick").val())){
                            $("#greencheck").hide();
                            $("#lblName2").hide();
                        
                        $("#lblName").fadeIn();
                        $("#lblName").html("@lang('home.cf_nombre2')");
                        
                        document.getElementById("nick").style.borderColor = "red";
                       document.getElementById("nick").style.backgroundColor = "#FFEBEB";
                       document.getElementById("nick").style.boxShadow = "0 0 0px 0px";
                        name=true;
                        i_name++;
                        s_name='@lang('home.cf_nombre2')<br>';
                        }else{
                            $("#greencheck").hide();
                            $("#lblName2").hide();
                            $("#lblName").fadeIn();
                        $("#lblName").html("@lang('home.cf_nombre3')");
                        
                        document.getElementById("nick").style.borderColor = "red";
                       document.getElementById("nick").style.backgroundColor = "#FFEBEB";
                       document.getElementById("nick").style.boxShadow = "0 0 0px 0px";
                        name=true;
                        i_name++;
                        s_name='@lang('home.cf_nombre3')<br>';
                        }
                    }
                        
                    }
                    
                    
                    
                    else if(name_length>45){
                        $("#greencheck").hide();
                        $("#lblName2").hide();
                        
                        $("#lblName").fadeIn();

                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#nick").val())){
                        
                        
                        $("#lblName").html("@lang('home.cf_nombre4')");
                        s_name='@lang('home.cf_nombre4')<br>';
                        }else{
                            $("#lblName").html("@lang('home.cf_nombre5')");
                        s_name='@lang('home.cf_nombre5')<br>';
                        }

                        document.getElementById("nick").style.borderColor = "red";
                       document.getElementById("nick").style.backgroundColor = "#FFEBEB";
                       document.getElementById("nick").style.boxShadow = "0 0 0px 0px";
                        name=true;
                        i_name++;
                    }

                    else{
                        if(/^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$/.test($("#nick").val())){
                            document.getElementById("nick").style.borderColor = "green";
                        document.getElementById("nick").style.backgroundColor = "#E4FFEF";
                        document.getElementById("nick").style.boxShadow = "0 0 0px 0px";
                        $("#lblName").hide();
                        $("#lblName2").html("@lang('home.cf_nombre6')");
                        $('#lblName2').fadeOut();
                        $('#greencheck').fadeIn();
                        
                        }else{
                            $("#greencheck").hide();
                            $("#lblName2").hide();
                        
                        $("#lblName").fadeIn();
                        $("#lblName").html("@lang('home.cf_nombre7')");
                        document.getElementById("nick").style.borderColor = "red";
                       document.getElementById("nick").style.backgroundColor = "#FFEBEB";
                       document.getElementById("nick").style.boxShadow = "0 0 0px 0px";
                        name=true;
                        i_name++;
                        s_name='@lang('home.cf_nombre7')<br>';
                        }
                        
                    }
                   
				}
				

				function check_email(){

var pattern=new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
var email_length = $("#correo").val().length;

if(email_length<=320){//320
if(email_length==0){
			//$("#greencheck").hide();
   $("#lblEmail2").hide();
   $("#lblEmail").fadeIn();
$("#lblEmail").html("@lang('home.cf_email')");
document.getElementById("correo").style.borderColor = "red";
document.getElementById("correo").style.backgroundColor = "#FFEBEB";
document.getElementById("correo").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
email=true;
i_email++;
s_email='@lang('home.cf_email')<br>';
}else{
if(pattern.test($("#correo").val())){
//$("#greencheck").hide();
document.getElementById("correo").style.borderColor = "green";
	document.getElementById("correo").style.backgroundColor = "#E4FFEF";
	document.getElementById("correo").style.boxShadow = "0 0 1.5px 2.9px #75DA8D";
		$("#lblEmail").hide();
		$("#lblEmail2").html("@lang('home.cf_email2')");
		$('#lblEmail2').fadeIn();
		//$('#greencheck').fadeIn();
}else{
$("#lblEmail2").hide();
	$("#lblEmail").fadeIn();
	$("#lblEmail").html("@lang('home.cf_email3')");//html("@lang('home.mensaje_contactenos')");
	document.getElementById("correo").style.borderColor = "red";
   document.getElementById("correo").style.backgroundColor = "#FFEBEB";
   document.getElementById("correo").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
	email=true;
	i_email++;
	s_email='@lang('home.cf_email3')<br>';
}
}
}else{
	$("#lblEmail2").hide();
	$("#lblEmail").fadeIn();
	if(pattern.test($("#correo").val())){ 
	//----------------------------------------------------------
		$("#lblEmail").html("@lang('home.cf_email4')");//html("@lang('home.mensaje_contactenos')");
		s_email='@lang('home.cf_email4')<br>';
		}else{
			$("#lblEmail").html("@lang('home.cf_email5')");
		s_email='@lang('home.cf_email5')<br>';
		}

		document.getElementById("correo").style.borderColor = "red";
	   document.getElementById("correo").style.backgroundColor = "#FFEBEB";
	   document.getElementById("correo").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
		email=true;
		i_email++;
   
}



}

function check_email2(){

var pattern=new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
var email_length = $("#correo").val().length;

if(email_length<=320){
if(email_length==0){
//$("#greencheck").hide();
$("#lblEmail2").hide();
$("#lblEmail").fadeIn();
$("#lblEmail").html("@lang('home.cf_email')");

document.getElementById("correo").style.borderColor = "red";
document.getElementById("correo").style.backgroundColor = "#FFEBEB";
document.getElementById("correo").style.boxShadow = "0 0 0px 0px";
email=true;
i_email++;
s_email='@lang('home.cf_email')<br>';

}else{
if(pattern.test($("#correo").val())){
//$("#greencheck").hide();
document.getElementById("correo").style.borderColor = "green";
document.getElementById("correo").style.backgroundColor = "#E4FFEF";
document.getElementById("correo").style.boxShadow = "0 0 0px 0px";

$("#lblEmail").hide();
$("#lblEmail2").html("@lang('home.cf_email2')");
$('#lblEmail2').fadeOut();
//$('#greencheck').fadeIn();
}else{ 
$("#lblEmail2").hide();
$("#lblEmail").fadeIn();
$("#lblEmail").html("@lang('home.cf_email3')");//html("@lang('home.mensaje_contactenos')");

document.getElementById("correo").style.borderColor = "red";
document.getElementById("correo").style.backgroundColor = "#FFEBEB";
document.getElementById("correo").style.boxShadow = "0 0 0px 0px";
email=true;
i_email++;
s_email='@lang('home.cf_email3')<br>';
}



}
}else{
$("#lblEmail2").hide();

$("#lblEmail").fadeIn();

if(pattern.test($("#correo").val())){

//----------------------------------------------------------
$("#lblEmail").html("@lang('home.cf_email4')");//html("@lang('home.mensaje_contactenos')");
s_email='@lang('home.cf_email4')<br>';
}else{
$("#lblEmail").html("@lang('home.cf_email5')");
s_email='@lang('home.cf_email5')<br>';
}

document.getElementById("correo").style.borderColor = "red";
document.getElementById("correo").style.backgroundColor = "#FFEBEB";
document.getElementById("correo").style.boxShadow = "0 0 0px 0px";
email=true;
i_email++;
}



}


function check_message(){
                    var message_length = $("#contenido").val().length;
                        if(message_length<5){
                            if(message_length==0){
                                //$("#greencheck").hide();
                       $("#lblMessage2").hide();
                       $("#lblMessage").fadeIn();
                   $("#lblMessage").html("@lang('home.cf_message')");
                   
                   document.getElementById("contenido").style.borderColor = "red";
                  document.getElementById("contenido").style.backgroundColor = "#FFEBEB";
                  document.getElementById("contenido").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                   message=true;
                   i_message++;
                   s_message='@lang('home.cf_message')<br>';
               }else{

                $("#lblMessage2").hide();
                        
                        $("#lblMessage").fadeIn();
                        $("#lblMessage").html("@lang('home.cf_message2')");
                        


                        document.getElementById("contenido").style.borderColor = "red";
                       document.getElementById("contenido").style.backgroundColor = "#FFEBEB";
                       document.getElementById("contenido").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        message=true;
                        i_message++;
                        s_message='@lang('home.cf_message2')<br>';
                  

                      }   
                    }
                    else if(message_length>320){
                        //$("#greencheck").hide();
                        $("#lblMessage2").hide();
                        
                        $("#lblMessage").fadeIn();

                        $("#lblMessage").html("@lang('home.cf_message3')");//html("@lang('home.mensaje_contactenos')");
                        
                        document.getElementById("contenido").style.borderColor = "red";
                       document.getElementById("contenido").style.backgroundColor = "#FFEBEB";
                       document.getElementById("contenido").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        message=true;
                        i_message++;
                        s_message='@lang('home.cf_message3')<br>';
                    }
                    else{

                        var n=document.getElementById("contenido").value;
                        var n2=n.toLowerCase();

                        var lisuras = ["mierda","carajo","puta","rechucha","imbecil",
        "verga","huevada","mrd","ctm","rctm","ptm","csm","pendejo","conchatumare","mamada",
    "perro","chupapinga","cmamo","hdp","motherfuck","fuck","shit"];


    var i;
    var cont=0;
for (i = 0; i < lisuras.length; i++) {
  if(n2.includes(lisuras[i])==true){
        cont++;
  };
} 

  
                            if(cont>0){  

                            $("#lblMessage2").hide();
                        
                        $("#lblMessage").fadeIn();

                        $("#lblMessage").html("@lang('home.cf_message5')");
                        
                        document.getElementById("contenido").style.borderColor = "red";
                       document.getElementById("contenido").style.backgroundColor = "#FFEBEB";
                       document.getElementById("contenido").style.boxShadow = "0 0 1.5px 2.9px #FD9F9F";
                        message=true;
                        i_message++;
                        s_message='@lang('home.cf_message5')<br>';

                        }else{
                            document.getElementById("contenido").style.borderColor = "green";
                        document.getElementById("contenido").style.backgroundColor = "#E4FFEF";
                        document.getElementById("contenido").style.boxShadow = "0 0 1.5px 2.9px #75DA8D";
                        $("#lblMessage").hide();
                        $("#lblMessage2").html("@lang('home.cf_message4')");
                        $('#lblMessage2').fadeIn();

                        }

                    }
                }
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                function check_message2(){
                    var message_length = $("#contenido").val().length;
                    
                    
                if(message_length<5){


                    if(message_length==0){
                        //$("#greencheck").hide();
                       $("#lblMessage2").hide();
                       $("#lblMessage").fadeIn();
                   $("#lblMessage").html("@lang('home.cf_message')");
                   
                   document.getElementById("contenido").style.borderColor = "red";
                  document.getElementById("contenido").style.backgroundColor = "#FFEBEB";
                  document.getElementById("contenido").style.boxShadow = "0 0 0px 0px";
                   message=true;
                   i_message++;
                   s_message='@lang('home.cf_message')<br>';
               }else{

                        
                            //$("#greencheck").hide();
                            $("#lblMessage2").hide();
                        
                        $("#lblMessage").fadeIn();
                        $("#lblMessage").html("@lang('home.cf_message2')");
                        
                        document.getElementById("contenido").style.borderColor = "red";
                       document.getElementById("contenido").style.backgroundColor = "#FFEBEB";
                       document.getElementById("contenido").style.boxShadow = "0 0 0px 0px";
                        message=true;
                        i_message++;
                        s_message='@lang('home.cf_message2')<br>';
                    }
                        
                    }
                    
                    else if(message_length>320){
                        //$("#greencheck").hide();
                        $("#lblMessage2").hide();
                        
                        $("#lblMessage").fadeIn();

                        
                        
                        
                        $("#lblMessage").html("@lang('home.cf_message3')");
                        

                        document.getElementById("contenido").style.borderColor = "red";
                       document.getElementById("contenido").style.backgroundColor = "#FFEBEB";
                       document.getElementById("contenido").style.boxShadow = "0 0 0px 0px";
                        message=true;
                        i_message++;
                        s_message='@lang('home.cf_message3')<br>';
                    }

                    else{
                       
                        var n=document.getElementById("contenido").value;
                        var n2=n.toLowerCase();

                        var lisuras = ["mierda","carajo","puta","rechucha","imbecil",
        "verga","huevada","mrd","ctm","rctm","ptm","csm","pendejo","conchatumare","mamada",
    "perro","chupapinga","cmamo","hdp","motherfuck","fuck","shit"];


    var i;
    var cont=0;
for (i = 0; i < lisuras.length; i++) {
  if(n2.includes(lisuras[i])==true){
        cont++;
  };
} 

  
                            if(cont>0){  

                            $("#lblMessage2").hide();
                        
                        $("#lblMessage").fadeIn();

                        $("#lblMessage").html("@lang('home.cf_message5')");
                        
                        document.getElementById("contenido").style.borderColor = "red";
                       document.getElementById("contenido").style.backgroundColor = "#FFEBEB";
                       document.getElementById("contenido").style.boxShadow = "0 0 0px 0px";
                        message=true;
                        i_message++;
                        s_message='@lang('home.cf_message5')<br>';

                        }else{
                            document.getElementById("contenido").style.borderColor = "green";
                        document.getElementById("contenido").style.backgroundColor = "#E4FFEF";
                        document.getElementById("contenido").style.boxShadow = "0 0 0px 0px";
                        $("#lblMessage").hide();
                        $("#lblMessage2").html("@lang('home.cf_message4')");
                        $('#lblMessage2').fadeOut();

                        }
                        
                       
                        
                    }
                   
                }

				$("#submit_form1").submit(function(){


name=false;
email=false;
message=false;
i_name=0;
i_email=0;
i_message=0;
s_name='';
s_email='';
s_message='';

check_name2();

check_email2();
check_message2();

if(name==true || email==true || message==true){
    
    //}else{
    var n=i_name+i_email+i_message;
    var st=s_name+s_email+s_message;
    if(n==1){  
    Swal.fire({
        
type: 'error',
title: '@lang('home.cf_tresponse')',

html: '<i class="fas fa-exclamation-triangle" style="font-family: Montserrat, sans-serif; color:red"></i> @lang('home.cf_tresponse1') '+n+' error <i class="fas fa-exclamation-triangle"  style="color:red"></i>'+
'<br><br><div class="alert alert-danger" style="font-family: Montserrat, sans-serif;" role="alert">'+st+'</div>',

})}else{

Swal.fire({
        
        type: 'error',
        title: '@lang('home.cf_tresponse')',
        html: '<i class="fas fa-exclamation-triangle" style="color:red"></i> @lang('home.cf_tresponse2') '+
        n+' @lang('home.cf_tresponse3') <i class="fas fa-exclamation-triangle"  style="font-family: Montserrat, sans-serif; color:red"></i>'+
        '<br><br><div class="alert alert-danger" style="font-family: Montserrat, sans-serif;" role="alert">'+st+'</div>',
        
      })

}
return false;
    
    
}


});

				
	});
</script>

@endsection

@section('content2')

<?php

$postcomentados=array();
$postcantidad=array();
$py=array();
$arr='';
$ct=0;

use App\Post;
      $posts=Post::all();
//use App\Post;
//$posts=Post::all();
foreach($posts as $post){

$titulopost='';
$linkpost='';
$fpo='';
$counter = 0;
foreach($post->comentarios as $comentario){
    $counter++;
    } //$titulopost=$post->titulo;

    $pbody=$post->body;
    //----------------------------------------------------------------------------
    
$extract=substr(htmlspecialchars(trim(strip_tags($pbody))), 0, 150);



$pbody1=htmlspecialchars(trim(strip_tags($pbody)));
//$extract=mb_strimwidth($bodypost, 1, 90, "...");
if(strlen($pbody)>151){

  $locale=App::getLocale();
//echo $extract, PHP_EOL;
if(App::isLocale('en')){
$pbo=htmlspecialchars_decode($extract, ENT_NOQUOTES).'... <small><a href="/intranet/public/blog/home/'.$post->id.'" style="text-decoration: none; color: blue;">
Read more</a></small>';
}else{
  $pbo=htmlspecialchars_decode($extract, ENT_NOQUOTES).'... <small><a href="/intranet/public/blog/home/'.$post->id.'" style="text-decoration: none; color: blue;">
Leer más</a></small>';
}
/*echo "... ";?>
<small><a href="/intranet/public/blog/home/{{$post->id}}" style="text-decoration: none; color: blue;">
@lang('home.bl_lmas')</a></small>
<?php*/
}
else{
 $pbo=htmlspecialchars_decode($pbody1, ENT_NOQUOTES);
}

//-------------------------------------------------------------
$fpost=$post->foto;
$fpo='';
if($fpost=='noimage.jpg'){  
$fpo='';
}else{



$fpo='/intranet/storage/app/public/images/{{$post->foto}}';

}

//$fpo="<img src='/intranet/storage/app/public/images/".$post->foto."' />";

$tpost="<div style='color:blue'>".$post->titulo."</div>";

    //-----------------------------------------------------------------------------
    $linkpost="
    <style>
   .popover {
    
    color: black;
    text-align:justify;
    background-color: white;
  }
  
  .popover-title {
    background-color: coral; 
    color: coral; 
    font-size: 40px;
    text-align:center;
  }
 
  .popover-content {
    background-color: green;
    color: blue;
    padding: 25px;
  }
  
  .arrow {
    border-right-color: red !important;
  }
   
   
   </style>
    <a href='/intranet/public/blog/home/".$post->id."'
    title='".$post->titulo."' data-by='".$pbo."' data-img='/intranet/storage/app/public/images/".$post->foto."' class='popi' data-toggle='popover' data-placement='bottom' style='text-decoration: none; color: blue; font-family: Montserrat, sans-serif;'>".$post->titulo."</a>

   ";?>
   
<?php
    $linkcomentados[]=$linkpost;
    //$postcomentados[]=$titulopost;
    $postcantidad[]=$counter;

$ct++;
}



for($i=0;$i<$ct;$i++){   
$py[$linkcomentados[$i]]=$postcantidad[$i];

}

arsort($py);?>

<div style="text-align:center;">
<div class="plustar1" id="plustar2"><div style="margin:10px; padding-left:10px; padding-right:10px; padding-top:2px; text-align:center; float: center; color:white; font-family: 'Montserrat', sans-serif;"><strong><i class="fas fa-star plustar" style="color:yellow"></i>  @lang('home.bl_gencoment')  <i class="fas fa-star plustar" style="color:yellow"></i></strong></div></div>
</div>
<!--@lang('home.bl_gencoment')-->


<?php
echo "<br>";
echo "<br>";
$i=0; 
foreach($py as $x => $x_value) {?>
  <label style="font-family: Montserrat, sans-serif;">
  <b>
	@lang('home.bl_title')
  </b>
	</label>
	<?php
	
    echo $x."&nbsp<label style='font-family: Montserrat, sans-serif;'><b>|</b></label>&nbsp"?><label style="font-family: Montserrat, sans-serif;"><b>@lang('home.bl_comments') </b></label>&nbsp<?php 
    echo "<label style='font-family: Montserrat, sans-serif;'>";
    echo $x_value;
    echo "</label>";
		echo "<br>";
		 
		if($i==9){
			break;
		}
	
		else{
		$i++;}
  }
  
  if($i==0){?>
    @lang('home.bl_nopost')
    <?php }


//--------------------------------
/*$age = array("Ben"=>"37", "Peter"=>"35", "Joe"=>"43");
print_r($age);
arsort($age);

foreach($age as $x => $x_value) {
    echo "Key=" . $x . ", Value=" . $x_value;
    echo "<br>";
}*/
//------------------------------------------------


$counter1 = 0;
$counter2= 0;
foreach($posts as $post){
    $counter1++;
      if((time() - strtotime($post->created_at)) <= 86400)
      {
          $counter2++;
      }
		}

$counter1;
$counter2;



?>

<script>

   $('.popi').popover({
    delay: 1150,
          //trigger: 'focus',
		  trigger: 'hover',
          html: true,
          content: function () {

            if($(this).data('img')=='/intranet/storage/app/public/images/noimage.jpg')
            {
            return $(this).data("by");
            }else{
              
				return $(this).data("by")+'<br><br><img class="img-fluid" src="'+$(this).data('img') + '" />';
            }
          },
          title: 'Toolbox'
    });   
 
 </script>
 <script>



          $(function(){

            
            
            $("#NumPosts").html("Total: <?php echo $counter1 ?>");

            var c2 = "<?php echo $counter2 ?>";
            if(c2>0){   
            $("#NewPosts").html(" @lang('home.bl_newpost2') <i class='fas fa-file comicon2' style='color:white'></i> : <?php echo $counter2 ?>");
            }else{
              $("#NewPosts").hide();
            }

            $("#dance_id2").hide();

            $("#danza_id").mouseover(function(){
              $("#dance_id1").hide();
              $("#dance_id2").show();
                });

            $("#danza_id").mouseout(function(){
              $("#dance_id2").hide();
              $("#dance_id1").show();
                });
            
              
          
          });
        </script>
@endsection