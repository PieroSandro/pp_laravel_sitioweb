@extends('layouts.mainblog')
@section('titleblog')


@lang('home.editmainpages')

@endsection

@section('carusel')

<!--------11_01_2020-------->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<link rel="stylesheet" href="{{url('/css/animate.css')}}">







<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        
        <script>
        AOS.init({
          duration:3000,
        });
        </script>





<!------------------->

              @endsection



@section('content')
<div class="text">
<div style="padding-top: 40px;
  padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">
  <!--<a href="/intranet/public/index/anuncios/$anuncio->id}}" class="btn btn-success"><i class="fas fa-angle-left"></i> @lang('home.bl_back')</a>-->
  <a href="{{route('backmain2pagehome')}}" class="btn" 
  style="color: white; border: rgba(228, 20, 20, 0.931); background-color:rgba(228, 20, 20, 0.931) ;font-family: Montserrat, sans-serif;">
  <i class="fas fa-angle-left"></i> @lang('home.bl_back')</a>
  <br><br>
  <h1 style="color: black; font-family: Montserrat, sans-serif; width:100%;">@lang('home.editmainpages')</h1>
<!-----------------------------------------------FORMULARIO: OPEN--------------------------------------------------------------------->
{!! Form::open(['action'=> ['Main2pageController@updatemain2page',$main2page->id], 'method'=> 'PUT','enctype'=>'multipart/form-data']) !!}
        {{Form::text('id',$main2page->id,['class'=>'form-control','hidden'])}}
        @csrf
            @if(session('success'))
               
                <script>

    Swal.fire({
			
			html: '<br><br><i class="fas fa-info-circle" style="color: rgba(11, 11, 132, 0.993); font-size:150px;"></i>'+
			'<br><br><br><div style="font-family: Montserrat, sans-serif;">{{session('success')}}</div><br><br>',
			
			showConfirmButton: false,//false
			timer:1500
      //timer 1500
		  })
		  
		
               
              
               
</script>

            @endif
        <br>

<!------------------------------------------------------------------------------------------------->
<div class="form-group" style="font-family: Montserrat;">
        <label for="body_es" style="font-family: Montserrat;" class="mt-3">@lang('home.dt_contenido_ES')</label>

            
 {{Form::textarea('body_es',$main2page->body_es,['name'=>'body_es','class'=>'form-control','placeholder'=>'Body Text'])}}
        </div>



                  <!---------------------------------------------------------------------------------------->

        <div class="form-group">

        <label for="foto_es" style="font-family: Montserrat;" class="mt-3">@lang('home.dt_foto_ES')</label>
               <br>
                    <!--{{Form::file('foto')}}-->
                    <div style="float: center; font-family: Montserrat, sans-serif;"><!--float:center; -->
                    
                    <input id="foto_es" accept='image/*' type="file" name="foto_es" 
                    value="{{$main2page->foto_es}}" 
                    style="font-family: Montserrat, sans-serif; width: 100%; text-align: center;"><!--text-align: center; -->
                    
                    </div>
                    <br><br>
                                <div style="text-align:center;">
                                <img src="/intranet/storage/app/public/images/{{$main2page->foto_es}}" id="image-field_es" width="250" height="250"><!-- style="width: 90%;"-->
                                </div>
           
                   </div>
            
        <!------------------------------------------------------------------------------>
        <!------------------------------------------------------------------------------->

<!------------------------------------------------------------------------------------------------->
<div class="form-group" style="font-family: Montserrat;">
        <label for="body_en" style="font-family: Montserrat;" class="mt-3">@lang('home.dt_contenido_EN')</label>      
 {{Form::textarea('body_en',$main2page->body_en,['name'=>'body_en','class'=>'form-control','placeholder'=>'Body Text'])}}
        </div>
                  <!---------------------------------------------------------------------------------------->

        <div class="form-group">
        <label for="foto_en" style="font-family: Montserrat;" class="mt-3">@lang('home.dt_foto_EN')</label>
               <br>
                    <!--{{Form::file('foto')}}-->
                    <div style="float: center; font-family: Montserrat, sans-serif;"><!--float:center; -->
                    <input id="foto_en" accept='image/*' type="file" name="foto_en" 
                    value="{{$main2page->foto_en}}" 
                    style="font-family: Montserrat, sans-serif; width: 100%; text-align: center;"><!--text-align: center; -->
                    
                    </div>
                    <br><br>
                                <div style="text-align:center;">
                                <img src="/intranet/storage/app/public/images/{{$main2page->foto_en}}" id="image-field_en" width="250" height="250"><!-- style="width: 90%;"-->
                                </div>
           
                   </div>
            
        <!------------------------------------------------------------------------------>
<!---16-2-2020-->
<br>
<div class="form-group" style="float: center; font-family: Montserrat, sans-serif;">
            <div class="checkbox" style="float: center; text-align: center;">
                <label for="si_primero_foto">
                    
                
                <input type="checkbox" onclick="cr4()" id="ch1" name="si_primero_foto" 
                value="{{$main2page->si_primero_foto}}" <?php if($main2page->si_primero_foto=="si_primero_foto"){ echo " checked "; } else{ echo " unchecked "; }?>>
                @lang('home.checkphoto')
                    </label>
                    <!--<label></label>-->
                    <input type="hidden" class="form-control" placeholder="" 
       name="cuadrocheck" id="cuadrocheck" value="<?php if($main2page->si_primero_foto=="si_primero_foto"){ echo "si_primero_foto"; } else{ echo ""; }?>">

            </div>
           
            </div>
            <br>
            <script>


                    function cr4() {

//$("#chatbotboard").animate({width: 'toggle'}, "slow");
//if($("#ch1").is(':checked')){
  //  document.getElementById("ch1").checked = false;

//}else{
    //document.getElementById("ch1").checked = true;
//}
var checkBox = document.getElementById("ch1");
  //var text = document.getElementById("text");
  if (checkBox.checked == true){
    //text.style.display = "block";
    //document.getElementById("ch1").checked = true;
    //$('#che').html('ja');
    //$('#ch1').removeAttribute("unchecked");
    //$('#ch1').setAttribute("checked", "checked");
    $("#cuadrocheck").val('si_primero_foto');

  } else {
     //text.style.display = "none";
     //document.getElementById("ch1").checked = false;
     

// Uncheck
//$('#che').html('no');
//$('#ch1').removeAttribute("checked");
//$('#ch1').setAttribute("unchecked", "unchecked");
$("#cuadrocheck").val('');
  }
}
            </script>
        <!---16-2-2020-->

        
        
        {{Form::hidden('_method','PUT')}}
        <div style="text-align: center;">
        <button type="submit" class="btn" style="color: white; border: rgba(11, 11, 132, 0.993); background-color:rgba(11, 11, 132, 0.993) ;font-family: Montserrat, sans-serif;">
<i class="fas fa-save"></i> @lang('home.bl_change')
</button>
      </div>      {!! Form::close() !!}
<!-----------------------------------------------FORMULARIO: CLOSE--------------------------------------------------------------------->

</div>
    </div>

    <script>
$(document).ready(function(){
    $("#foto_es").change(function(){
  const file2_es = this.files[0];
const  fileType_es = file2_es['type'];
const validImageTypes_es = ['image/gif', 'image/jpeg', 'image/png'];
if (validImageTypes_es.includes(fileType_es)) {
    var file_es = document.getElementById("foto_es").files[0];
  var readImg_es = new FileReader();
  readImg_es.readAsDataURL(file_es);
  readImg_es.onload = function(e) {
    var span_es = document.createElement('span');
    $('#image-field_es').attr('src',e.target.result).fadeIn();
    }
}else{
  Swal.fire({	
		html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
			'<br><br><h1 style="color: black; font-family: Montserrat, sans-serif;">@lang('home.warning_adver')</h1><br><div style="font-family: Montserrat, sans-serif;">@lang('home.warning_adver_2')</div>',
	showConfirmButton: false,
			timer:2000
		  })
  $('#image-field_es').attr('src','../images/nia.png').fadeIn();
} 
  })

  $("#foto_es").show(function(){
    const file2_es = this.files[0];
const  fileType_es = file2_es['type'];
const validImageTypes_es = ['image/gif', 'image/jpeg', 'image/png'];
if (validImageTypes_es.includes(fileType_es)) {
    var file_es = document.getElementById("foto_es").files[0];
  var readImg_es = new FileReader();
  readImg_es.readAsDataURL(file_es);
  readImg_es.onload = function(e) {
    var span_es = document.createElement('span');
    $('#image-field_es').attr('src',e.target.result).fadeIn();
    }
}else{
  Swal.fire({	
		html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
			'<br><br><h1 style="color: black; font-family: Montserrat, sans-serif;">@lang('home.warning_adver')</h1><br><div style="font-family: Montserrat, sans-serif;">@lang('home.warning_adver_2')</div>',
	showConfirmButton: false,
			timer:2000
		  })
  $('#image-field_es').attr('src','../images/nia.png').fadeIn();
}
  
    
  })




  $("#foto_en").change(function(){
  const file2_en = this.files[0];
const  fileType_en = file2_es['type'];
const validImageTypes_en = ['image/gif', 'image/jpeg', 'image/png'];
if (validImageTypes_en.includes(fileType_en)) {
    var file_en = document.getElementById("foto_en").files[0];
  var readImg_en = new FileReader();
  readImg_en.readAsDataURL(file_es);
  readImg_en.onload = function(e) {
    var span_en = document.createElement('span');
    $('#image-field_en').attr('src',e.target.result).fadeIn();
    }
}else{
  Swal.fire({	
		html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
			'<br><br><h1 style="color: black; font-family: Montserrat, sans-serif;">@lang('home.warning_adver')</h1><br><div style="font-family: Montserrat, sans-serif;">@lang('home.warning_adver_2')</div>',
	showConfirmButton: false,
			timer:2000
		  })
  $('#image-field_en').attr('src','../images/nia.png').fadeIn();
} 
  })

  $("#foto_en").show(function(){
    const file2_en = this.files[0];
const  fileType_en = file2_en['type'];
const validImageTypes_en = ['image/gif', 'image/jpeg', 'image/png'];
if (validImageTypes_en.includes(fileType_en)) {
    var file_en = document.getElementById("foto_en").files[0];
  var readImg_en = new FileReader();
  readImg_en.readAsDataURL(file_en);
  readImg_en.onload = function(e) {
    var span_en = document.createElement('span');
    $('#image-field_en').attr('src',e.target.result).fadeIn();
    }
}else{
  Swal.fire({	
		html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
			'<br><br><h1 style="color: black; font-family: Montserrat, sans-serif;">@lang('home.warning_adver')</h1><br><div style="font-family: Montserrat, sans-serif;">@lang('home.warning_adver_2')</div>',
	showConfirmButton: false,
			timer:2000
		  })
  $('#image-field_en').attr('src','../images/nia.png').fadeIn();
}
  
    
  })













})
    </script>
@endsection



@section('content2')

<?php

$postcomentados=array();
$postcantidad=array();
$py=array();
$arr='';
$ct=0;

use App\Post;
$posts=Post::all();
foreach($posts as $post){

$titulopost='';
$counter = 0;
foreach($post->comentarios as $comentario){
    $counter++;
    } $titulopost=$post->titulo;
    
    
    $postcomentados[]=$titulopost;
    $postcantidad[]=$counter;

$ct++;
}



for($i=0;$i<$ct;$i++){   
$py[$postcomentados[$i]]=$postcantidad[$i];

}

arsort($py);?>
@lang('home.bl_gencoment')


<?php
echo "<br>";
echo "<br>";
$i=0; 
foreach($py as $x => $x_value) {?>

	@lang('home.bl_title')
	
	<?php
	
		echo $x." | "?>@lang('home.bl_comments') <?php echo $x_value;
		echo "<br>";
		 
		if($i==9){
			break;
		}
	
		else{
		$i++;}
	}

	if($i==0){?>
		@lang('home.bl_nopost')
		<?php }
	


//--------------------------------
/*$age = array("Ben"=>"37", "Peter"=>"35", "Joe"=>"43");
print_r($age);
arsort($age);

foreach($age as $x => $x_value) {
    echo "Key=" . $x . ", Value=" . $x_value;
    echo "<br>";
}*/
//------------------------------------------------
?>
@endsection
        

@section('js')
<script>
        CKEDITOR.replace( 'body_es' );
    </script>

<script>
        CKEDITOR.replace( 'body_en' );
    </script>

<script>
$(function(){
  $('#idtitulo').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

//-----------------------------------------------------------
    //$(this).find('option:hover').css('background-color', 'red');
    //$(this).find('option:selected').css('background-color', 'red');
   
});


$('#idtitulo').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});


$('#idlink').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

//-----------------------------------------------------------
    //$(this).find('option:hover').css('background-color', 'red');
    //$(this).find('option:selected').css('background-color', 'red');
   
});


$('#idlink').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});
});
</script>
@endsection