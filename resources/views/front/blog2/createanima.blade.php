@extends('layouts.mainblog')
@section('titleblog')

@lang('home.bl_createpost')

@endsection


<!---->
@section('carusel')
@endsection



@section('content')
<div class="text">
<div style="padding-top: 40px;
  padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<h1>@lang('home.bl_createpost')</h1>
<br>
    {!! Form::open(['action'=> 'HomeController@storeanima', 'method'=> 'POST','enctype'=>'multipart/form-data']) !!}
       @csrf
            @if(session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>

            @endif
       
        <div class="form-group">
        <label for="efecto" class="mt-3">@lang('home.bl_bcategoria')</label>
            <select class="form-control input-sm" name="efecto_id">
                <option value="">@lang('home.bl_bcategoria2')</option>
                @foreach($efectos as $efecto)

                    <option value="{{$efecto['id']}}">{{$efecto['nombre']}}</option>
                @endforeach
            </select>
                               <br>
            @if($errors->has('efecto_id'))
            @foreach($errors->get('efecto_id') as $error)

            <div class="alert alert-danger">
            @lang('home.bl_bcategoria3')
            </div>
            @endforeach
            @endif

        </div>



        
        <div class="form-group">

        

            
            {{Form::hidden('titulo','hola',['class'=>'form-control','placeholder'=>''])}}

                   <br>
            @if($errors->has('titulo'))
            @foreach($errors->get('titulo') as $error)

            <div class="alert alert-danger">
            @lang('home.bl_bgtitulo2')
            </div>
            @endforeach
            @endif

        </div>




        <br>
        <!--{{Form::submit('Crear Post',['class'=>'btn btn-primary'])}}-->
        <input type="submit" class="btn btn-success" value="@lang('home.bl_createpost')">
    {!! Form::close() !!}
    </div>
    </div>

    @endsection



@section('content2')

<?php

$postcomentados=array();
$postcantidad=array();
$py=array();
$arr='';
$ct=0;

use App\Post;
$posts=Post::all();
foreach($posts as $post){

$titulopost='';
$counter = 0;
foreach($post->comentarios as $comentario){
    $counter++;
    } $titulopost=$post->titulo;
    
    
    $postcomentados[]=$titulopost;
    $postcantidad[]=$counter;

$ct++;
}



for($i=0;$i<$ct;$i++){   
$py[$postcomentados[$i]]=$postcantidad[$i];

}

arsort($py);?>
@lang('home.bl_gencoment')


<?php
echo "<br>";
echo "<br>";
$i=0; 
foreach($py as $x => $x_value) {?>

	@lang('home.bl_title')
	
	<?php
	
		echo $x." | "?>@lang('home.bl_comments') <?php echo $x_value;
		echo "<br>";
		 
		if($i==9){
			break;
		}
	
		else{
		$i++;}
  }
  
  if($i==0){?>
    @lang('home.bl_nopost')
    <?php }



?>
@endsection

@section('js')
<script>
        CKEDITOR.replace( 'body' );
    </script>
@endsection