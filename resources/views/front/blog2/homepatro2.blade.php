@extends('layouts.mainblog')
@section('titleblog')



@lang('home.bl_listan')

@endsection
@section('carusel')

<script>
$(document).ready(function(){
  $("option").hover(function(){
    $(this).css("background-color", "yellow");
  }, function(){
    $(this).css("background-color", "white");
    });
});
</script>

<style>

select option {
  
  background: white;
  color: black;
  
}

option:hover{
  background-color: yellow;
}

@media screen and (max-width: 575px){
  #patro-table_paginate{
 
 margin-left: 61%;
 margin-right: 50%;
}
  .dataTables_length{
	
	order:1;
  }

  .dataTables_filter{
	order:2;
  }
  
}

@media screen and (min-width: 576px) and (max-width: 767px){
  #patro-table_paginate{
 
 margin-left: 56%;
 margin-right: 50%;
}
}

@media screen and (min-width: 992px){
  #patro-table_paginate{
 
 margin-left: 56%;
 margin-right: 50%;
}
}

@media screen and (min-width: 768px) and (max-width: 991px){
  #patro-table_paginate{
 
 margin-left: 61%;
 margin-right: 50%;
}
  #insearch{
    align: center;
  }

  #patro-table_length{
    
   width: 50%;
  margin-left: auto;
  margin-right: auto;
  }

  #patro-table_filter{
   
   width: 50%;
  margin-left: auto;
  margin-right: auto;

  
  }



  .datatables_length{
   
    float: center;
           text-align: center; 
  }

  .datatables_filter{
    float: center;
           text-align: center; 
  }
  
}



.odd *:before{
  background-color: rgb(11, 11, 132, 0.993) !important;
  margin-top:4px;
}



.even *:before{
  background-color: rgb(11, 11, 132, 0.993) !important;
  margin-top:4px;
}



.parent *:before{
  background-color: rgba(228, 20, 20, 0.931) !important;
  margin-top:4px;
}

.panel-default{
  border-radius:10%;
}

*:focus {
    outline: none;
}

*:focus .page-link{
    outline: none;
}
.custom-select {
  
  font-family: 'Montserrat', sans-serif;
}

.custom-select *{
  
  font-family: 'Montserrat', sans-serif;
}
.page-link:disabled{
  background-color: green !important;
  color:green !important;
  outline: none;
}

.page-item.active .page-link {
    background-color: rgba(228, 20, 20, 0.931) !important;
    color: white !important;
    font-weight:bold;
    border-color: white !important;
    outline: none;
}

#patro-table_next:active .page-link {
    background-color: rgba(228, 20, 20, 0.931) !important;
    font-family: 'Montserrat', sans-serif;
    color: white !important;
    font-weight:bold;
    border-color: white !important;
    outline: none;
}

#patro-table_previous:active .page-link {
    background-color: rgba(228, 20, 20, 0.931) !important;
    font-family: 'Montserrat', sans-serif;
    color: white !important;
    font-weight:bold;
    border-color: white !important;
    outline: none;
}

.dt-button.xselected {
    color:blue;
    border: 1px blue solid;
}
.page-link{
  font-family: 'Montserrat', sans-serif;
  background-color: white !important;
  color: rgb(11, 11, 132, 0.993) !important;
  outline: none;
}

.page-item.active .page-link.active{
  color: white !important;
  font-family: 'Montserrat', sans-serif;
  border-color: white !important;
  outline: none;
}

.page-link:hover{
  background-color: rgb(11, 11, 132, 0.993) !important;
  font-family: 'Montserrat', sans-serif;
color: white !important;

border-color: white !important;
outline: none;
}

#bt1car:before{
	
	content:'@lang('home.ver_slider')';
	align-content: center;
	font-size:12.4px;
	position:absolute;
	top:50%;
	left:50%;
	transition:all 0.3s;
	transform:translate(-50%,-50%);
}




#appear_image_div{
  width:100%;
  height:100%;
  position:absolute;
  z-index:10;
  opacity:0.7;
  background:#002447;
}
#appear_image{
  display:block;
  margin:auto;
  position:relative;
  z-index:11;
  top:20px;
}
#close_image{
  position:fixed;
  z-index:12;
  top:20px;
  right:20px;
  cursor:pointer;
}
#close_image:hover{
  opacity:0.7;
}
























</style>


              @endsection
@section('content')
<style>
                  .custom-select:focus{
  background-color:red;
}
              </style>

<label id="qs"></label>

<br>
<br>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">

<link rel="stylesheet" href="{{url('/css/dataTables.bootstrap4.min.css')}}">


  
<h1>@lang('home.bl_an2')</h1>

<?php 
                  
                  $counter = 0;
                      ?>











<div id="app">
  
</div>
<div style="padding-top: 40px;padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">
  <div class="panel panel-default">


    <form action="{{url('/index/patros')}}" method="post">
    {!! csrf_field() !!}
    @if(session('success'))
<script>

    Swal.fire({
			
			html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
			'<br><br><h1>Warning!</h1><br> {{session('success')}}',
			
			showConfirmButton: true,
			//timer:3500
		  })
		  
		
               
              
               
</script>
            @endif
<!--------------------------------------------->
<table id="patro-table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">

        <thead>
            <tr>
                    
            <th width="10%" style="border: 1px solid rgba(11, 11, 132, 0.993); background-color: rgba(11, 11, 132, 0.993); color: white; text-align: center;">N°</th>
                    <th  width="10%" style="border: 1px solid rgba(228, 20, 20, 0.931); background-color: rgba(228, 20, 20, 0.931);color: white; text-align: center;">Foto</th>
                    <th  width="20%" style="border: 1px solid rgba(11, 11, 132, 0.993); background-color: rgba(11, 11, 132, 0.993);color: white; text-align: center;">Título</th>
                    <th width="30%" style="border: 1px solid rgba(228, 20, 20, 0.931); background-color: rgba(228, 20, 20, 0.931);color: white; text-align: center;">Contenido</th>
                   <th style="border: 1px solid rgba(11, 11, 132, 0.993); background-color: rgba(11, 11, 132, 0.993);color: white; text-align: center;">Acción</th>
                  
                    
            </tr>
        </thead>
        <tbody>
      
            @foreach($patros as $patro)
            <?php
            $bodyevent='';
            $bodyevent=$patro->body;
            
            $bodyevent1 = html_entity_decode($bodyevent);
            ?>
            <tr>
            <td width="10%" style="text-align:center;"> <?php $counter++; echo $counter ?> <label name="<?php $counter ?>"></label></td>
            <td width="10%" style="text-align:center; vertical-align:middle;"><a href="#gardenImage" data-id="/intranet/storage/app/public/images/{{$patro->foto}}" name="eventimg[]" class="openImageDialog thumbnail" data-toggle="modal"><img class="imgevent" src="/intranet/storage/app/public/images/{{$patro->foto}}" style="width:90%; cursor: pointer;"></a></td>
            <td width="20%" style="text-align:justify; white-space: pre-wrap; white-space: -moz-pre-wrap; white-space: -pre-wrap; white-space: -o-pre-wrap; word-wrap: break-word;">{{$patro->titulo}}</td>
            <td class="justified-cell" width="30%" style="text-align:justify; white-space: pre-wrap; white-space: -moz-pre-wrap; white-space: -pre-wrap; white-space: -o-pre-wrap; word-wrap: break-word;"><p style="padding-right: 1px;

padding-left: 1px"><?php echo $bodyevent1; ?></p></td>
                
                
                
               
                
               
                <td style="text-align:center; vertical-align:middle; width:1%;
    white-space:nowrap;">
               


                <a href="/intranet/public/index/patros/{{$patro->id}}/edit" name="idc" value="{{$counter}}" class="btn btn-primary" style="color:white; border: none; background-color:rgba(4, 10, 100, 0.808);"><i class="fas fa-pen"></i> @lang('home.bl_edit4')</a>
                
               <?php
              $crt=$counter;
               ?>
               <input type="hidden" name="ccc" value="{{$crt}}">
                </td>
            </tr>














            

            @endforeach
            

        </tbody>
    </table>
    </form>
</div>
    </div>







 <!--------------------------------->
<!--------------------------------->

<div class="modal fade" id="gardenImage" tabindex="-1" role="dialog" aria-labelledby="gardenImageLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <img id="myImage" class="img-responsive" src="" alt="" height="100%" width="100%">
            </div>
            <div class="modal-footer text-center" style="text-align:center; vertical-align:middle;">
            <fieldset class="align-center" 
            style="display: block;
    margin: auto;
    text-align: center;">
                <button type="button" style="float:center;" class="btn btn-danger center-block" data-dismiss="modal">@lang('home.bl_modalcancel')</button>
                </fieldset>
    </div>
        </div>
    </div>
</div>




<script>
$(document).on("click", ".openImageDialog", function () {
    var myImageId = $(this).data('id');
    $("#gardenImage .modal-body #myImage").attr("src", myImageId);
    $("#gardenImage").modal('show');
});
</script>

<script>
                function Scrolldown() {
     //window.scroll(0,562); 
     document.getElementById("qs").scrollIntoView();
}

window.onload = Scrolldown;
</script>


<script>
$(document).ready(function(){
  

    $("#foto").change(function(){
  //---------------------------------------
  const file2 = this.files[0];
const  fileType = file2['type'];
const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
if (validImageTypes.includes(fileType)) {
    
    var file = document.getElementById("foto").files[0];

  var readImg = new FileReader();
  
  readImg.readAsDataURL(file);
  
  readImg.onload = function(e) {
    var span = document.createElement('span');
    $('#image-field').attr('src',e.target.result).fadeIn();
    
    }
    
}else{
  Swal.fire({
			
			html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
			'<br><br><h1>Warning!</h1><br>Please select an image with format .gif, .jpeg, .png!',
			
			showConfirmButton: false,
			timer:1800
		  })
  $('#image-field').attr('src','../images/nia.png').fadeIn();
}
        //----------------------------
  
    
  })

  $("#foto").show(function(){
    const file2 = this.files[0];
const  fileType = file2['type'];
const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
if (validImageTypes.includes(fileType)) {
   
    var file = document.getElementById("foto").files[0];

  var readImg = new FileReader();
  
  readImg.readAsDataURL(file);
  
  readImg.onload = function(e) {
    var span = document.createElement('span');
    $('#image-field').attr('src',e.target.result).fadeIn();
    
    }
}else{
  Swal.fire({
			
			html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
			'<br><br><h1>Warning!</h1><br>Please select an image with format .gif, .jpeg, .png!',
			
			showConfirmButton: false,
			timer:1800
		  })
  $('#image-field').attr('src','../images/nia.png').fadeIn();
}
  
    
  })
})
    </script>

                



<!--MODAL TO CREATE-->	

<!------------------------------------------------------------>
<!------------------------------POPOVER------------------------------>


<!---------------------------------POPOVER------------------------------>
<!-------------------------------------------------------------->

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>


<script>

   $(document).ready(function(){ 
        $('#patro-table').DataTable({
            "language": {
				"emptyTable":			"No hay datos disponibles en la tabla.",
				"info":		   		"Del _START_ al _END_ de _TOTAL_ ",
				"infoEmpty":			"Mostrando 0 registros de un total de 0.",
				"infoFiltered":			"(filtrados de un total de _MAX_ registros)",
				"infoPostFix":			"(actualizados)",
				"lengthMenu":			"Mostrar _MENU_ registros",
				"loadingRecords":		"Cargando...",
				"processing":			"Procesando...",
				"search":			"",
				"searchPlaceholder":		"Buscar...",//"@lang('home.ver_slider')"
				"zeroRecords":			"No se han encontrado coincidencias.",
				"paginate": {
					"first":			"Primera",
					"last":				"Última",
					"next":				"›",
					"previous":			"‹"
				},
				"aria": {
					"sortAscending":	"Ordenación ascendente",
					"sortDescending":	"Ordenación descendente"
				}
			},



        columns:[
            
            {data:'nro',name:'nro'},
            {data:'foto',name:'foto',orderable:false,searchable:false},
            {data:'titulo',name:'titulo'},
            {data:'body',name:'body'},
            
            {data:'action',name:'action',orderable:false,searchable:false}
            ]
    });

    $.fn.dataTable.ext.errMode = 'throw';

   });
    

</script>
<script type="text/javascript" src="{{asset('js/app.js')}}"></script>
<script>

$(function(){
  document.getElementsByTagName("input")[1].setAttribute("id", "insearch");
  $("#insearch").focusin(function(){
    document.getElementById("insearch").style.borderColor = "rgb(11, 11, 132, 0.993)";
                  
                  document.getElementById("insearch").style.boxShadow = "0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)";
                 
                });
                $("#insearch").focusout(function(){
    document.getElementById("insearch").style.borderColor = "#ccc";
                  
                  document.getElementById("insearch").style.boxShadow = "0 0 0px";
                 
                });
    
                
                document.getElementsByClassName("col-sm-12")[0].setAttribute("class", "col-xs-12 col-sm-6 col-md-12 col-lg-6");
                document.getElementsByClassName("col-sm-12")[0].setAttribute("class", "col-xs-12 col-sm-6 col-md-12 col-lg-6");
                document.getElementsByClassName("col-md-5")[0].setAttribute("class", "col-12");
                document.getElementsByClassName("col-md-7")[0].setAttribute("class", "col-12");
                //document.getElementsByClassName("col-12")[1].style.textAlign = "center";
                document.getElementsByClassName("col-12")[2].style.textAlign = "center";

                document.getElementsByClassName("pagination").setAttribute("role", "navigation");
                //document.getElementsByClassName("pagination").setAttribute("class", "pagination text-center");
                
                //document.getElementById("vcurso-table_paginate").style.Align = "center";
                //  document.getElementsByClassName("custom-select custom-select-sm form-control form-control-sm").setAttribute("id", "sellin");
                document.getElementsByTagName("select")[0].setAttribute("id", "selnum");

                $("#selnum").focusin(function(){
    document.getElementById("selnum").style.borderColor = "rgb(11, 11, 132, 0.993)";
                  
                  document.getElementById("selnum").style.boxShadow = "0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)";
                 
                });
                $("#selnum").focusout(function(){
    document.getElementById("selnum").style.borderColor = "#ccc";
                  
                  document.getElementById("selnum").style.boxShadow = "0 0 0px";
                 
                });

                document.getElementsByTagName("option")[0].setAttribute("id", "diez");
                document.getElementsByTagName("option").style.fontFamily="Monserrat,sans-serif";


               // document.getElementsByClassName("select-checkbox").style.boxShadow = "2 2 2px";
});

</script>

<script>

$('#delete').on('show.bs.modal',function(event){
  var button=$(event.relatedTarget)
  var cat_id=button.data('catid')
  var modal=$(this)

 //print link title into modal

 var btitulo = button.data("titulo");
        //$(this).find(".modal-title").text(blink);
        modal.find(".modal-body #btitulo").text(btitulo);
//print link title into modal

  modal.find('.modal-body #cat_id').val(cat_id);

})
</script>
<script>
  /*$(function(){
    $("#checktodo").click(function(){
      $("input[class='cbx1']").attr("checked",this.checked)
    });

    $("input[class='cbx1']").click(function(){
      if($("input[class='cbx1']").length == $("input[class='cbx1']:checked").length){
        $("#checktodo").attr("checked","checked");
      }else{
        $("#checktodo").removeAttr("checked");
      }
    });
  });*/
  $("#checktodo").change(function(){
      $(".cbx1").prop("checked",$(this).prop("checked"))
    });
    $(".cbx1").change(function(){
      if($(this).prop("checked")==false){
        $("#checktodo").prop("checked",false)
      }
      if($(".cbx1:checked").length==$(".cbx1").length){
        $("#checktodo").prop("checked",true)
      }
    })
</script>


<!--<script>
$("#modal-edit").on('shown.bs.modal', function() {
     CKEDITOR.replace('body');
   })



</script>-->

<script>
      
      $('#modal-edit').on('show.bs.modal',function(event){

        

  var button=$(event.relatedTarget)
  var titulo=button.data('titulo')
  var body=button.data('body')
  
  var eid=button.data('eid')
  var foto=button.data('foto')
  var modal=$(this)

 //print link title into modal
        //alert(foto);
 
        //$(this).find(".modal-title").text(blink);
        modal.find(".modal-body #titulo").val(titulo);
        modal.find(".modal-body #body").val(body);
      
        modal.find(".modal-body #foto").val(foto);
        modal.find(".modal-body #e_id").val(eid);
        
       
//print link title into modal

  //modal.find('.modal-body #cat_id').val(cat_id);

}) 
</script>
@endsection

<!--@section('js')
<script>
        CKEDITOR.replace( '.modal-body #body' );
    </script>
@endsection-->

@section('content2')


@endsection



