@extends('layouts.mainblog')
@section('titleblog')

@lang('home.bl_banner1')

@endsection


<!---->
@section('carusel')
@endsection



@section('content')
<div class="text">
<div style="padding-top: 40px;
  padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<h1>@lang('home.bl_banner1')</h1>
<br>
<!-----------------------------------------------FORMULARIO: OPEN--------------------------------------------------------------------->
    {!! Form::open(['action'=> 'HomeController@storemainicono', 'method'=> 'POST','enctype'=>'multipart/form-data']) !!}
       @csrf
            @if(session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>

            @endif
        
        <div class="form-group">

        <label for="foto" class="mt-3">@lang('home.bl_foto')</label>        
            
            <div class="col-md-10">

                                <input id="foto" accept='image/*' type="file" name="foto" value="" style="width: 100%;">

                                @if ($errors->has('foto'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('foto') }}</strong>
                                </span>
                                @endif
                            </div>
                            <br>

        </div>
          

                    
        <br>
        
        <input type="submit" class="btn btn-success" value="@lang('home.bl_banner1')">
    {!! Form::close() !!}
    </div>
    </div>

    @endsection



@section('content2')

<?php

$postcomentados=array();
$postcantidad=array();
$py=array();
$arr='';
$ct=0;

use App\Post;
$posts=Post::all();
foreach($posts as $post){

$titulopost='';
$counter = 0;
foreach($post->comentarios as $comentario){
    $counter++;
    } $titulopost=$post->titulo;
    
    
    $postcomentados[]=$titulopost;
    $postcantidad[]=$counter;

$ct++;
}



for($i=0;$i<$ct;$i++){   
$py[$postcomentados[$i]]=$postcantidad[$i];

}

arsort($py);?>
@lang('home.bl_gencoment')


<?php
echo "<br>";
echo "<br>";
$i=0; 
foreach($py as $x => $x_value) {?>

	@lang('home.bl_title')
	
	<?php
	
		echo $x." | "?>@lang('home.bl_comments') <?php echo $x_value;
		echo "<br>";
		 
		if($i==9){
			break;
		}
	
		else{
		$i++;}
  }
  
  if($i==0){?>
    @lang('home.bl_nopost')
    <?php }


//--------------------------------
/*$age = array("Ben"=>"37", "Peter"=>"35", "Joe"=>"43");
print_r($age);
arsort($age);

foreach($age as $x => $x_value) {
    echo "Key=" . $x . ", Value=" . $x_value;
    echo "<br>";
}*/
//------------------------------------------------
?>
@endsection

@section('js')
<script>
        CKEDITOR.replace( 'gui' );
    </script>
@endsection