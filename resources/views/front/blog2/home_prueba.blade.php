@extends('layouts.mainblogPRUEBA')
@section('titleblog')



@lang('home.bl_home')

@endsection



<!------------------------------------------------------------------------------------>

@section('content')
<!-------------------------POPOVER---------------------------------->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

  
<link rel="stylesheet" href="{{url('/css/animate.css')}}">


<!--------------ANIMATE ON SCROLL------------------------->
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        
        <script>
        AOS.init({
          duration:3000,
        });
        </script>
<!----------------------POPOVER------------------------------------->



<div class="text">
<div style="padding-top: 40px;
  padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">
<h1 style="font-family: 'Montserrat', sans-serif;">Blog <b><p><label style="padding-left:10px; padding-right:10px; padding-top:8px; text-align:center; float: center; color:white; font-size:16px; font-family: 'Montserrat', sans-serif;" id="NumPosts"></label> <label style="padding-left:10px; padding-right:10px; padding-top:8px; text-align:center; float: center; color:white; font-size:16px; font-family: 'Montserrat', sans-serif;" id="NewPosts" class="NewPosts2"></label></p></b></h1>
<br>
@if(session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
                @elseif(session('danger'))
                <div class="alert alert-danger">
                    {{session('danger')}}
                </div>
            @endif
@csrf
@if(count($posts)>0)
@foreach($posts as $post)
	<div class="card">
    <div style="padding-top: 20px;padding-right: 20px;
  padding-bottom: 20px;
  padding-left: 20px;">


@if((time() - strtotime($post->created_at)) <= 86400)

<div style="padding-top: 0px;
padding-bottom: 20px;">
<div class="conew1"><div style="padding-left:10px; padding-right:10px; padding-top:5px; text-align:center; float: center; color:white; font-family: 'Montserrat', sans-serif;"><strong>@lang('home.bl_newpost') <i class="fas fa-file comicon2" style="color:white"></i></strong></div></div>
</div>
@else

@endif

<h3><a href="/intranet/public/blog/home/{{$post->id}}" onMouseOver="this.style.cssText='color: rgba(228, 20, 20, 0.931); text-decoration:none;'" onMouseOut="this.style.cssText='color: rgb(11, 11, 132, 0.993)'" style="text-decoration: none; color: rgb(11, 11, 132, 0.993);">{{$post->titulo}}</a></h3>
<br>
		<?php
      
      //$fpost_a=$post->foto;
      ?>
      @if($post->foto=='noimage.jpg')
        
        


              
              
              <h5 style="text-align:justify;">


              <?php
$bodypost='';
$bodypost=$post->body;
$extract=substr(htmlspecialchars(trim(strip_tags($bodypost))), 0, 150);



$bodypost1=htmlspecialchars(trim(strip_tags($bodypost)));
//$extract=mb_strimwidth($bodypost, 1, 90, "...");
if(strlen($bodypost)>151){

//echo $extract, PHP_EOL;
echo htmlspecialchars_decode($extract, ENT_NOQUOTES);
echo "... ";?>
<small><a href="/intranet/public/blog/home/{{$post->id}}" style="text-decoration: none; color: blue;">
@lang('home.bl_lmas')</a></small>
<?php
}
else{
 echo htmlspecialchars_decode($bodypost1, ENT_NOQUOTES);
}
?>


</h5>

        @else
        
        <div class="row">
        <div class="col-12 col-sm-6 col-md-12 col-lg-6">
        <!--<h3><a href="/intranet/public/blog/home/{{$post->id}}" style="text-decoration: none;"> {{$post->titulo}}</a></h3>
              
              <br>-->
              <h5 style="text-align:justify;">


              <?php
$bodypost='';
$bodypost=$post->body;
$extract=substr(htmlspecialchars(trim(strip_tags($bodypost))), 0, 150);



$bodypost1=htmlspecialchars(trim(strip_tags($bodypost)));
//$extract=mb_strimwidth($bodypost, 1, 90, "...");
if(strlen($bodypost)>151){

//echo $extract, PHP_EOL;
echo htmlspecialchars_decode($extract, ENT_NOQUOTES);
echo "... ";?>
<small><a href="/intranet/public/blog/home/{{$post->id}}" style="text-decoration: none; color: blue;">
@lang('home.bl_lmas')</a></small>
<?php
}
else{
 echo htmlspecialchars_decode($bodypost1, ENT_NOQUOTES);
}
?>


</h5>
        <!--<p class="tc">Text on the left.</p>-->
        </div>
        
        <div class="col-12 col-sm-6 col-md-12 col-lg-6">
        <center>
        <div>
<img src="/intranet/storage/app/public/images/{{$post->foto}}" style="margin-bottom:5%; margin-top:5%; max-width:95%; max-height:95%;">
</div>
</center>
        </div>
        <!--<p class="alignleft">Text on the left.</p>
        <p class="alignright">Text on the right.</p>-->
        </div>
        
        
        <!--$fpo='/intranet/storage/app/public/images/{{$post->foto}}';-->
        @endif
		
<br>
        <small>@lang('home.bl_fcreacion'): {{$post->created_at}}</small><br>
        
        <small>@lang('home.bl_factualizacion'): {{$post->updated_at}}</small>
      </div>  
	</div>
	<br>
@endforeach
	{{$posts->links('vendor.pagination.bootstrap-4')}}
@else
<p>@lang('home.bl_nopost')</p>
@endif
  </div>
  </div>
@endsection

@section('content2')

<?php

$postcomentados=array();
$postcantidad=array();
$linkcomentados=array();

$py=array();
$arr='';
$ct=0;

use App\Post;
      $posts=Post::all();
//use App\Post;
//$posts=Post::all();
foreach($posts as $post){

$titulopost='';
$linkpost='';
$fpo='';
$counter = 0;
foreach($post->comentarios as $comentario){
    $counter++;
    } //$titulopost=$post->titulo;

    $pbody=$post->body;
    //----------------------------------------------------------------------------
    
$extract=substr(htmlspecialchars(trim(strip_tags($pbody))), 0, 150);



$pbody1=htmlspecialchars(trim(strip_tags($pbody)));
//$extract=mb_strimwidth($bodypost, 1, 90, "...");
if(strlen($pbody)>151){

  $locale=App::getLocale();
//echo $extract, PHP_EOL;
if(App::isLocale('en')){
$pbo=htmlspecialchars_decode($extract, ENT_NOQUOTES).'... <small><a href="/intranet/public/blog/home/'.$post->id.'" style="text-decoration: none; color: blue;">
Read more</a></small>';
}else{
  $pbo=htmlspecialchars_decode($extract, ENT_NOQUOTES).'... <small><a href="/intranet/public/blog/home/'.$post->id.'" style="text-decoration: none; color: blue;">
Leer más</a></small>';
}
/*echo "... ";?>
<small><a href="/intranet/public/blog/home/{{$post->id}}" style="text-decoration: none; color: blue;">
@lang('home.bl_lmas')</a></small>
<?php*/
}
else{
 $pbo=htmlspecialchars_decode($pbody1, ENT_NOQUOTES);
}

//-------------------------------------------------------------
$fpost=$post->foto;
$fpo='';
if($fpost=='noimage.jpg'){  
$fpo='';
}else{



$fpo='/intranet/storage/app/public/images/{{$post->foto}}';

}

//$fpo="<img src='/intranet/storage/app/public/images/".$post->foto."' />";

$tpost="<div style='color:blue'>".$post->titulo."</div>";

    //-----------------------------------------------------------------------------
    $linkpost="
    <style>
   .popover {
    
    color: black;
    text-align:justify;
    background-color: white;
  }
  
  .popover-title {
    background-color: coral; 
    color: coral; 
    font-size: 40px;
    text-align:center;
  }
 
  .popover-content {
    background-color: green;
    color: blue;
    padding: 25px;
  }
  
  .arrow {
    border-right-color: red !important;
  }
   
   
   </style>
    <a href='/intranet/public/blog/home/".$post->id."'
    title='".$post->titulo."' data-by='".$pbo."' data-img='/intranet/storage/app/public/images/".$post->foto."' class='popi' data-toggle='popover' data-placement='bottom' style='text-decoration: none; color: blue; font-family: Montserrat, sans-serif;'>".$post->titulo."</a>

   ";?>
   
<?php
    $linkcomentados[]=$linkpost;
    //$postcomentados[]=$titulopost;
    $postcantidad[]=$counter;

$ct++;
}



for($i=0;$i<$ct;$i++){   
$py[$linkcomentados[$i]]=$postcantidad[$i];

}

arsort($py);?>

<div style="text-align:center;">
<div class="plustar1" id="plustar2"><div style="margin:10px; padding-left:10px; padding-right:10px; padding-top:2px; text-align:center; float: center; color:white; font-family: 'Montserrat', sans-serif;"><strong><i class="fas fa-star plustar" style="color:yellow"></i>  @lang('home.bl_gencoment')  <i class="fas fa-star plustar" style="color:yellow"></i></strong></div></div>
</div>
<!--@lang('home.bl_gencoment')-->


<?php
echo "<br>";
echo "<br>";
$i=0; 
foreach($py as $x => $x_value) {?>
  <label style="font-family: Montserrat, sans-serif;">
  <b>
	@lang('home.bl_title')
  </b>
	</label>
	<?php
	
    echo $x."&nbsp<label style='font-family: Montserrat, sans-serif;'><b>|</b></label>&nbsp"?><label style="font-family: Montserrat, sans-serif;"><b>@lang('home.bl_comments') </b></label>&nbsp<?php 
    echo "<label style='font-family: Montserrat, sans-serif;'>";
    echo $x_value;
    echo "</label>";
		echo "<br>";
		 
		if($i==9){
			break;
		}
	
		else{
		$i++;}
  }
  
  if($i==0){?>
    @lang('home.bl_nopost')
    <?php }


//--------------------------------
/*$age = array("Ben"=>"37", "Peter"=>"35", "Joe"=>"43");
print_r($age);
arsort($age);

foreach($age as $x => $x_value) {
    echo "Key=" . $x . ", Value=" . $x_value;
    echo "<br>";
}*/
//------------------------------------------------


$counter1 = 0;
$counter2= 0;
foreach($posts as $post){
    $counter1++;
      if((time() - strtotime($post->created_at)) <= 86400)
      {
          $counter2++;
      }
		}

$counter1;
$counter2;



?>

<script>

   $('.popi').popover({
    delay: 1150,
          //trigger: 'focus',
		  trigger: 'hover',
          html: true,
          content: function () {

            if($(this).data('img')=='/intranet/storage/app/public/images/noimage.jpg')
            {
            return $(this).data("by");
            }else{
              
				return $(this).data("by")+'<br><br><img class="img-fluid" src="'+$(this).data('img') + '" />';
            }
          },
          title: 'Toolbox'
    });   
 
 </script>
 <script>



          $(function(){

            
            
            $("#NumPosts").html("Total: <?php echo $counter1 ?>");

            var c2 = "<?php echo $counter2 ?>";
            if(c2>0){   
            $("#NewPosts").html(" @lang('home.bl_newpost2') <i class='fas fa-file comicon2' style='color:white'></i> : <?php echo $counter2 ?>");
            }else{
              $("#NewPosts").hide();
            }

            $("#dance_id2").hide();

            $("#danza_id").mouseover(function(){
              $("#dance_id1").hide();
              $("#dance_id2").show();
                });

            $("#danza_id").mouseout(function(){
              $("#dance_id2").hide();
              $("#dance_id1").show();
                });
            
              
                $(window).resize(function(){
                    winSize();
               });

               function winSize(){
               
        if ($(window).width() < 568) {
          document.getElementById("bt1car").style.marginTop = "0px";
          document.getElementById("bt1car2").style.marginTop = "0px";
          document.getElementById("bt1car3").style.marginTop = "0px";
        }
    
    else{
      if ($(window).width() < 880) {
      document.getElementById("bt1car").style.marginTop = "50px";
      document.getElementById("bt1car2").style.marginTop = "50px";
      document.getElementById("bt1car3").style.marginTop = "50px";
      }else{
        if($(window).width() < 1200){
          document.getElementById("bt1car").style.marginTop = "120px";
          document.getElementById("bt1car2").style.marginTop = "120px";
          document.getElementById("bt1car3").style.marginTop = "120px";
        }else{
          document.getElementById("bt1car").style.marginTop = "205px";
          document.getElementById("bt1car2").style.marginTop = "205px";
          document.getElementById("bt1car3").style.marginTop = "205px";
        }
      }
    }
          }
           

               $(window).show(function(){
                    win2();
               });

               function win2(){
               
        if ($(window).width() < 568) {
          document.getElementById("bt1car").style.marginTop = "0px";
          document.getElementById("bt1car2").style.marginTop = "0px";
          document.getElementById("bt1car3").style.marginTop = "0px";
        }
    
    else{
      if ($(window).width() < 880) {
      document.getElementById("bt1car").style.marginTop = "50px";
      document.getElementById("bt1car2").style.marginTop = "50px";
      document.getElementById("bt1car3").style.marginTop = "50px";
      }else{
        if($(window).width() < 1200){
          document.getElementById("bt1car").style.marginTop = "120px";
          document.getElementById("bt1car2").style.marginTop = "120px";
          document.getElementById("bt1car3").style.marginTop = "120px";
        }else{
          document.getElementById("bt1car").style.marginTop = "205px";
          document.getElementById("bt1car2").style.marginTop = "205px";
          document.getElementById("bt1car3").style.marginTop = "205px";
        }
      }
    }
          }
          });
        </script>
@endsection