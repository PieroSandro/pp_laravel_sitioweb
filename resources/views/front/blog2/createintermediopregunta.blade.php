@extends('layouts.mainblog')
@section('titleblog')

@lang('home.createintermediopreguntas')

@endsection

@section('carusel')


<!--------12_01_2020-------->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<link rel="stylesheet" href="{{url('/css/animate.css')}}">







<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        
        <script>
        AOS.init({
          duration:3000,
        });
        </script>





<!------------------->


@endsection

@section('content')
<style>


.swal2-modal .swal2-styled{
    background-color: rgba(11, 11, 132, 0.993);
    font-family: 'Montserrat', sans-serif;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js">
</script>
<div class="text">
<div style="padding-top: 40px;
  padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;"><script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<a href="{{route('backintermediopreguntahome')}}" 
class="btn" 
style="color: white; border: rgba(228, 20, 20, 0.931); background-color:rgba(228, 20, 20, 0.931) ;font-family: Montserrat, sans-serif;">
<i class="fas fa-angle-left"></i> @lang('home.gointermediopreguntas')</a>


<br><br>
<h1 style="color: black; font-family: Montserrat, sans-serif; width:100%;">@lang('home.createintermediopreguntas')</h1>
<br>

{!! Form::open(['action'=> 'HomeController@storeintermediopregunta', 'method'=> 'POST','enctype'=>'multipart/form-data']) !!}

@csrf

@if(session('success'))

<?php
           
           $locale=App::getLocale();

           if(App::isLocale('en')){
             ?>
        
        <script>

Swal.fire({
 
 html: '<i class="fas fa-info-circle" style="color:rgba(11, 11, 132, 0.993); font-size:150px;"></i>'+
 '<br><br><br><div style="font-family: Montserrat, sans-serif;">{!!session('success')!!}</div>',
 
 showConfirmButton: true,
 })   
          
</script>
               <?php    
                       
         }else{?>
            <script>

Swal.fire({
 
 html: '<i class="fas fa-info-circle" style="color:rgba(11, 11, 132, 0.993); font-size:150px;"></i>'+
 '<br><br><br><div style="font-family: Montserrat, sans-serif;">{!!session('success')!!}</div>',
 
 showConfirmButton: true,
 })   
          
</script>
           
           <?php
           }

         ?>


            @endif

    <span id="result"></span>
            
        <br>

         <!-----------------------CREATEVCURSOFORM---------------------------------------------------------------------------------->
         <div id="ggg">
       
       <div id="row1" class="card">
       <div style="padding-top: 20px;padding-right: 20px;
 padding-bottom: 20px;
 padding-left: 20px;">
       
       

       <div class="form-group" style="font-family: Montserrat;">
       <label for="body" style="font-family: Montserrat;" class="mt-3">@lang('home.namequestion')</label>
       <textarea name="rows[1][Body]" class="form-control" id="b_body" placeholder="Body Text" cols="50" rows="10"></textarea>
               <br>
       </div>

       <div class="form-group" style="font-family: Montserrat;">
       <label for="option1" style="font-family: Montserrat;" class="mt-3">@lang('home.nameoption1')</label>
       <input type="text" class="form-control" placeholder="" name="rows[1][Option1]" id="tit1" value="">
       <br>
       </div>
       
       <div class="form-group" style="font-family: Montserrat;">
       <label for="option2" style="font-family: Montserrat;" class="mt-3">@lang('home.nameoption2')</label>
       <input type="text" class="form-control" placeholder="" name="rows[1][Option2]" id="tit2" value="">
       <br>
       </div>

       <div class="form-group" style="font-family: Montserrat;">
       <label for="option3" style="font-family: Montserrat;" class="mt-3">@lang('home.nameoption3')</label>
       <input type="text" class="form-control" placeholder="" name="rows[1][Option3]" id="tit3" value="">
       <br>
       </div>

       <div class="form-group" style="font-family: Montserrat;">
       <label for="option4" style="font-family: Montserrat;" class="mt-3">@lang('home.nameoption4')</label>
       <input type="text" class="form-control" placeholder="" name="rows[1][Option4]" id="tit4" value="">
       <br>
       </div>
       


       <div class="form-group" style="font-family: Montserrat;">
       <label for="answer" style="font-family: Montserrat;" class="mt-3">@lang('home.nameanswer')</label>
       <input type="text" class="form-control" placeholder="" name="rows[1][Answer]" id="tit5" value="">
       <br>
       </div>


       </div>
       </div>
       </div>
       <!--------------------------------------------------------------------------------------------------------------------->

       <br>
       <label class="col text-center">
        <a class="btn" id="addRow" style="color: white; border: rgba(11, 11, 132, 0.993); background-color:rgba(11, 11, 132, 0.993) ;font-family: Montserrat, sans-serif;"><i class="fas fa-plus-circle"></i> @lang('home.addrecord')</a>
        </label>
        &nbsp
        <label class="col text-center">
        <button type="submit" name="save" id="save" class="btn" style="color: white; border: rgba(11, 11, 132, 0.993); background-color:rgba(11, 11, 132, 0.993) ;font-family: Montserrat, sans-serif;">
<i class="fas fa-save"></i> @lang('home.saverecord')
</button>        </label>
        {!! Form::close() !!}
    </div>
    </div>



    <script>
$(function(){
 

    $('#b_body').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

});


$('#b_body').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});
 //------------------------------------------------------------------------
$('#tit1').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

});


$('#tit1').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});


$('#tit2').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

});


$('#tit2').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});


$('#tit3').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

});


$('#tit3').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});


$('#tit4').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

});


$('#tit4').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});


$('#tit5').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

});


$('#tit5').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});

});
</script>
<!-------------------------------------------------------------------------------------------->
<!-------------------------------------------------------------------------------------------->

    <script>
    $( document ).ready(function() {
    var count=1;

    $('#addRow').on('click',function(){
    count++;
    neorevista(count);
});
    
    function neorevista(i)
    {
        var tr= '<div id="row'+i+'" class="card" style="margin-top: 20px;"><div style="padding-top: 20px;padding-right: 20px;padding-bottom: 20px; padding-left: 20px;">'+        


'<div class="form-group" style="font-family: Montserrat;">'+
    '<label for="body" class="mt-3">@lang('home.namequestion')</label>'+

    '<textarea name="rows[' + i +'][Body]" class="form-control" id="b_body'+i+'" placeholder="Body Text" cols="50" rows="10"></textarea>'+

               '<br>'+
       

    '</div>'+

    '<div class="form-group" style="font-family: Montserrat;"><label for="option1" style="font-family: Montserrat;" class="mt-3">@lang('home.nameoption1')</label>'+        
'<input class="form-control" placeholder="" name="rows[' + i +'][Option1]" id="tit1'+i+'" type="text" value="">'+
                '<br>'+
   

'</div>'+

'<div class="form-group" style="font-family: Montserrat;"><label for="option2" style="font-family: Montserrat;" class="mt-3">@lang('home.nameoption2')</label>'+        
'<input class="form-control" placeholder="" name="rows[' + i +'][Option2]" id="tit2'+i+'" type="text" value="">'+
                '<br>'+
   

'</div>'+












'<div class="form-group" style="font-family: Montserrat;"><label for="option3" style="font-family: Montserrat;" class="mt-3">@lang('home.nameoption3')</label>'+        
'<input class="form-control" placeholder="" name="rows[' + i +'][Option3]" id="tit3'+i+'" type="text" value="">'+
                '<br>'+
   

'</div>'+


'<div class="form-group" style="font-family: Montserrat;"><label for="option4" style="font-family: Montserrat;" class="mt-3">@lang('home.nameoption4')</label>'+        
'<input class="form-control" placeholder="" name="rows[' + i +'][Option4]" id="tit4'+i+'" type="text" value="">'+
                '<br>'+
   

'</div>'+


'<div class="form-group" style="font-family: Montserrat;"><label for="answer" style="font-family: Montserrat;" class="mt-3">@lang('home.nameanswer')</label>'+        
'<input class="form-control" placeholder="" name="rows[' + i +'][Answer]" id="tit5'+i+'" type="text" value="">'+
                '<br>'+
   

'</div>'
;

tr+='<label class="col text-center"><a class="btn deleteRow" id="'+i+'" style="color: white; border: rgba(228, 20, 20, 0.931); background-color:rgba(228, 20, 20, 0.931) ;font-family: Montserrat, sans-serif;"><i class="fas fa-minus-circle"></i> @lang('home.deleterecord')</a></label><br></div></div>';


$('#ggg').append(tr);
//CKEDITOR.replace( 'rows[' + i +'][Body]' );

                //---------script_foto_append-----------------------------
                $('#link'+i+'').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

//-----------------------------------------------------------
    //$(this).find('option:hover').css('background-color', 'red');
    //$(this).find('option:selected').css('background-color', 'red');
   
});


$('#link'+i+'').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});



//---------------------------------------------------------------


$('#b_body'+i+'').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

   
});


$('#b_body'+i+'').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});



//-------------------------------------------------------------
$('#tit1'+i+'').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

   
});


$('#tit1'+i+'').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});


$('#tit2'+i+'').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

});


$('#tit2'+i+'').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});


$('#tit3'+i+'').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

//-----------------------------------------------------------
    //$(this).find('option:hover').css('background-color', 'red');
    //$(this).find('option:selected').css('background-color', 'red');
   
});


$('#tit3'+i+'').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});


$('#tit4'+i+'').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

//-----------------------------------------------------------
    //$(this).find('option:hover').css('background-color', 'red');
    //$(this).find('option:selected').css('background-color', 'red');
   
});


$('#tit4'+i+'').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});


$('#tit5'+i+'').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

//-----------------------------------------------------------
    //$(this).find('option:hover').css('background-color', 'red');
    //$(this).find('option:selected').css('background-color', 'red');
   
});


$('#tit5'+i+'').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});
                //---------script_foto_append-----------------------------

var imgVal2 = $('#link'+i+'').val(); 
        if(imgVal2=='') 
        { 
          
          $('#link-field'+i+'').attr('src','../images/nia.png').fadeIn();
        }

$('#link'+i+'').change(function(){
  
var imgVal2=$('#link'+i+'').val();
if(imgVal2!==''){
   
$('#link-field'+i+'').attr('src',imgVal2).fadeIn();
}else{
  $('#link-field'+i+'').attr('src','../images/nia.png').fadeIn();
}
  })

  $('#link'+i+'').show(function(){
   
var imgVal2=$('#link'+i+'').val();
if(imgVal2!==''){  
 
$('#link-field'+i+'').attr('src',imgVal2).fadeIn();
}else{
  $('#link-field'+i+'').attr('src','../images/nia.png').fadeIn();
}
  
    
  })

  $('#link'+i+'').keypress(function(){
    var imgVal2=$('#link'+i+'').val();
if(imgVal2!==''){
   
$('#link-field'+i+'').attr('src',imgVal2).fadeIn();
}else{
  $('#link-field'+i+'').attr('src','../images/nia.png').fadeIn();
}
                });

                $('#link'+i+'').keydown(function(){
                  var imgVal2=$('#link'+i+'').val();
if(imgVal2!==''){
 
$('#link-field'+i+'').attr('src',imgVal2).fadeIn();
}else{
  $('#link-field'+i+'').attr('src','../images/nia.png').fadeIn();
}
                });

                $('#link'+i+'').keyup(function(){
                  var imgVal2=$('#link'+i+'').val();
if(imgVal2!==''){
    
$('#link-field'+i+'').attr('src',imgVal2).fadeIn();
}else{
  $('#link-field'+i+'').attr('src','../images/nia.png').fadeIn();
}
                });

                $('#link'+i+'').focusin(function(){
                  var imgVal2=$('#link'+i+'').val();
if(imgVal2!==''){  
  
$('#link-field'+i+'').attr('src',imgVal2).fadeIn();
}else{
  $('#link-field'+i+'').attr('src','../images/nia.png').fadeIn();
}
                });
                //---------script_foto_append-----------------------------
    }
   
    $(document).on('click','.deleteRow',function(){
    var button_id=$(this).attr("id");
    $('#row'+button_id+'').remove();
});



    });
    </script>

    
<!--SCRIPT PARA LA FOTO----------------------------------------------------------------------------------->
    <script>
     $(document).ready(function(){

      var imgVal = $('#link').val();
     
        if(imgVal=='') 
        { 
          
          $('#link-field').attr('src','../images/nva.jpg').fadeIn();
        }

$("#link").change(function(){
  
var imgVal=$("#link").val();
if(imgVal!==''){
$('#link-field').attr('src',imgVal).fadeIn();
}else{
  $('#link-field').attr('src','../images/nva.jpg').fadeIn();
}
  })

  $("#link").show(function(){
   
var imgVal=$("#link").val();
if(imgVal!==''){ 
 
$('#link-field').attr('src',imgVal).fadeIn();
}else{
  $('#link-field').attr('src','../images/nva.jpg').fadeIn();
}
  
    
  })

  $("#link").keypress(function(){
    var imgVal=$("#link").val();
if(imgVal!==''){
  
$('#link-field').attr('src',imgVal).fadeIn();
}else{
  $('#link-field').attr('src','../images/nva.jpg').fadeIn();
}
                });

                $("#link").keydown(function(){
                  var imgVal=$("#link").val();
if(imgVal!==''){
$('#link-field').attr('src',imgVal).fadeIn();
}else{
  $('#link-field').attr('src','../images/nva.jpg').fadeIn();
}
                });

                $("#link").keyup(function(){
                  var imgVal=$("#link").val();
if(imgVal!==''){   
  
$('#link-field').attr('src',imgVal).fadeIn();
}else{
  $('#link-field').attr('src','../images/nva.jpg').fadeIn();
}
                });

                $("#link").focusin(function(){
                  var imgVal=$("#link").val();
if(imgVal!==''){
  
$('#link-field').attr('src',imgVal).fadeIn();
}else{
  $('#link-field').attr('src','../images/nva.jpg').fadeIn();
}
                });
        
  
})
</script>
<!--SCRIPT PARA LA FOTO--------------------------------------------------->
   @endsection



@section('content2')

<?php

$postcomentados=array();
$postcantidad=array();
$py=array();
$arr='';
$ct=0;

use App\Post;
$posts=Post::all();
foreach($posts as $post){

$titulopost='';
$counter = 0;
foreach($post->comentarios as $comentario){
    $counter++;
    } $titulopost=$post->titulo;
    
    
    $postcomentados[]=$titulopost;
    $postcantidad[]=$counter;

$ct++;
}



for($i=0;$i<$ct;$i++){   
$py[$postcomentados[$i]]=$postcantidad[$i];

}

arsort($py);?>
@lang('home.bl_gencoment')


<?php
echo "<br>";
echo "<br>";
$i=0; 
foreach($py as $x => $x_value) {?>

	@lang('home.bl_title')
	
	<?php
	
		echo $x." | "?>@lang('home.bl_comments') <?php echo $x_value;
		echo "<br>";
		 
		if($i==9){
			break;
		}
	
		else{
		$i++;}
  }
  
  if($i==0){?>
    @lang('home.bl_nopost')
    <?php }



?>
@endsection

@section('js')
<script>
       // CKEDITOR.replace( 'rows[1][Body]' );
    </script>
@endsection