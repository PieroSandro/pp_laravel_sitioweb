@extends('layouts.mainblog')
@section('titleblog')



@lang('home.namemainpages')

@endsection
@section('carusel')

<!--------11_01_2020-------->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<link rel="stylesheet" href="{{url('/css/animate.css')}}">







<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        
        <script>
        AOS.init({
          duration:3000,
        });
        </script>
<!------------------->
<script>
$(document).ready(function(){
  $("option").hover(function(){
    $(this).css("background-color", "yellow");
  }, function(){
    $(this).css("background-color", "white");
    });
});
</script>
<style>
input[type="checkbox"] {
  pointer-events: none;
}

</style>
<style>

/*--11-1-2020 */
#mainpage-table_length{
  font-family: 'Montserrat', sans-serif;
}

#mainpage-table_filter{
  font-family: 'Montserrat', sans-serif;
}


#mainpage-table_info{
  font-family: 'Montserrat', sans-serif;
}

.dataTables_empty{
  font-family: 'Montserrat', sans-serif;
}

/*--- */


select option {
  
  background: white;
  color: black;
  
}

option:hover{
  background-color: yellow;
}

@media screen and (max-width: 575px){
  #mainpage-table_paginate{
 
 margin-left: 61%;
 margin-right: 50%;
}
  .dataTables_length{
	
	order:1;
  }

  .dataTables_filter{
	order:2;
  }
  
}

@media screen and (min-width: 576px) and (max-width: 767px){
  #mainpage-table_paginate{
 
 margin-left: 56%;
 margin-right: 50%;
}
}

@media screen and (min-width: 992px){
  #mainpage-table_paginate{
 
 margin-left: 56%;
 margin-right: 50%;
}
}

@media screen and (min-width: 768px) and (max-width: 991px){
  #mainpage-table_paginate{
 
 margin-left: 61%;
 margin-right: 50%;
}
  #insearch{
    align: center;
  }

  #mainpage-table_length{
    
   width: 50%;
  margin-left: auto;
  margin-right: auto;
  }

  #mainpage-table_filter{
   
   width: 50%;
  margin-left: auto;
  margin-right: auto;

  
  }



  .datatables_length{
   
    float: center;
           text-align: center; 
  }

  .datatables_filter{
    float: center;
           text-align: center; 
  }
  
}



.odd *:before{
  background-color: rgb(11, 11, 132, 0.993) !important;
  margin-top:4px;
}



.even *:before{
  background-color: rgb(11, 11, 132, 0.993) !important;
  margin-top:4px;
}



.parent *:before{
  background-color: rgba(228, 20, 20, 0.931) !important;
  margin-top:4px;
}

.panel-default{
  border-radius:10%;
}

*:focus {
    outline: none;
}

*:focus .page-link{
    outline: none;
}
.custom-select {
  
  font-family: 'Montserrat', sans-serif;
}

.custom-select *{
  
  font-family: 'Montserrat', sans-serif;
}
.page-link:disabled{
  background-color: green !important;
  color:green !important;
  outline: none;
}

.page-item.active .page-link {
    background-color: rgba(228, 20, 20, 0.931) !important;
    color: white !important;
    font-weight:bold;
    border-color: white !important;
    outline: none;
}

#mainpage-table_next:active .page-link {
    background-color: rgba(228, 20, 20, 0.931) !important;
    font-family: 'Montserrat', sans-serif;
    color: white !important;
    font-weight:bold;
    border-color: white !important;
    outline: none;
}

#mainpage-table_previous:active .page-link {
    background-color: rgba(228, 20, 20, 0.931) !important;
    font-family: 'Montserrat', sans-serif;
    color: white !important;
    font-weight:bold;
    border-color: white !important;
    outline: none;
}

.dt-button.xselected {
    color:blue;
    border: 1px blue solid;
}
.page-link{
  font-family: 'Montserrat', sans-serif;
  background-color: white !important;
  color: rgb(11, 11, 132, 0.993) !important;
  outline: none;
}

.page-item.active .page-link.active{
  color: white !important;
  font-family: 'Montserrat', sans-serif;
  border-color: white !important;
  outline: none;
}

.page-link:hover{
  background-color: rgb(11, 11, 132, 0.993) !important;
  font-family: 'Montserrat', sans-serif;
color: white !important;

border-color: white !important;
outline: none;
}

#bt1car:before{
	
	content:'@lang('home.ver_slider')';
	align-content: center;
	font-size:12.4px;
	position:absolute;
	top:50%;
	left:50%;
	transition:all 0.3s;
	transform:translate(-50%,-50%);
}




#appear_image_div{
  width:100%;
  height:100%;
  position:absolute;
  z-index:10;
  opacity:0.7;
  background:#002447;
}
#appear_image{
  display:block;
  margin:auto;
  position:relative;
  z-index:11;
  top:20px;
}
#close_image{
  position:fixed;
  z-index:12;
  top:20px;
  right:20px;
  cursor:pointer;
}
#close_image:hover{
  opacity:0.7;
}
</style>
   
              @endsection
              @section('content')
              <style>
                  .custom-select:focus{
  background-color:red;
}
              </style>
              <label id="qs"></label>
<br>
<br>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">

<link rel="stylesheet" href="{{url('/css/dataTables.bootstrap4.min.css')}}">


  
<h1 style="font-family: Montserrat, sans-serif; width:100%;">@lang('home.namemainpages')</h1>

<?php 
                  
                  $counter = 0;
                      ?>

<div id="app">
  
</div>
<div style="padding-top: 40px;padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">
  <div class="panel panel-default">


    <form action="{{url('/index/main2pages')}}" method="post">
    {!! csrf_field() !!}
    @if(session('success'))
<script>

    Swal.fire({
			
			html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
			'<br><br><h1>Warning!</h1><br> {{session('success')}}',
			
			showConfirmButton: true,
			//timer:3500
		  })
		  
		
               
              
               
</script>
            @endif
    <table id="mainpage-table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
        <thead>
            <tr>
            
                    <th width="10%" style="border: 1px solid rgba(11, 11, 132, 0.993); background-color: rgba(11, 11, 132, 0.993); color: white; text-align: center; font-family: Montserrat, sans-serif;">N°</th>
                    <th width="30%" style="border: 1px solid rgba(228, 20, 20, 0.931); background-color: rgba(228, 20, 20, 0.931);color: white; text-align: center; font-family: Montserrat, sans-serif;">@lang('home.dt_contenido_ES')</th>
                    <th  width="10%" style="border: 1px solid rgba(11, 11, 132, 0.993); background-color: rgba(11, 11, 132, 0.993);color: white; text-align: center; font-family: Montserrat, sans-serif;">@lang('home.dt_foto_ES')</th>
                    <th width="30%" style="border: 1px solid rgba(228, 20, 20, 0.931); background-color: rgba(228, 20, 20, 0.931);color: white; text-align: center; font-family: Montserrat, sans-serif;">@lang('home.dt_contenido_EN')</th>
                    <th  width="10%" style="border: 1px solid rgba(11, 11, 132, 0.993); background-color: rgba(11, 11, 132, 0.993);color: white; text-align: center; font-family: Montserrat, sans-serif;">@lang('home.dt_foto_EN')</th>
                   
                   <!--16-2-2020-->
                   <th  width="10%" style="border: 1px solid rgba(228, 20, 20, 0.931); background-color: rgba(228, 20, 20, 0.931);color: white; text-align: center; font-family: Montserrat, sans-serif;">@lang('home.firstphoto')</th>

                   <!--16-2-2020-->
                   
                    <th style="border: 1px solid rgba(11, 11, 132, 0.993); background-color: rgba(11, 11, 132, 0.993);color: white; text-align: center; font-family: Montserrat, sans-serif;">@lang('home.dt_accion')</th>
                    
            </tr>
        </thead>






        <tbody>

        
        @if(count($main2pages)>0)
            
            @foreach($main2pages as $main2page)
           <?php
            $bodyevent_ES='';
            $bodyevent_ES=$main2page->body_es;
            
            $bodyevent1_ES = html_entity_decode($bodyevent_ES);

            $bodyevent_EN='';
            $bodyevent_EN=$main2page->body_en;
            
            $bodyevent1_EN = html_entity_decode($bodyevent_EN);
            ?>
            <tr>
                
      <td width="10%" style="text-align:center; font-family: Montserrat, sans-serif;"> <?php $counter++; echo $counter ?> <label name="<?php $counter ?>"></label></td>
      <!-------------------------------------->
     <td class="justified-cell" width="30%" 
        style="font-family: Montserrat, sans-serif; text-align:justify; white-space: pre-wrap; white-space: -moz-pre-wrap; white-space: -pre-wrap; white-space: -o-pre-wrap; word-wrap: break-word;"><p style="padding-right: 1px;

    padding-left: 1px"><?php echo $bodyevent1_ES; ?></p></td>

     <td width="10%" style="text-align:center; vertical-align:middle;">
    <a href="#gardenImage_ES" data-id="/intranet/storage/app/public/images/{{$main2page->foto_es}}" 
 name="eventimg_ES[]" class="openImageDialog_ES thumbnail" data-toggle="modal">
<img class="imgevent_ES" src="/intranet/storage/app/public/images/{{$main2page->foto_es}}" 
 style="width:90%; cursor: pointer;"></a></td>


 <td class="justified-cell" width="30%" 
        style="font-family: Montserrat, sans-serif; text-align:justify; white-space: pre-wrap; white-space: -moz-pre-wrap; white-space: -pre-wrap; white-space: -o-pre-wrap; word-wrap: break-word;"><p style="padding-right: 1px;

    padding-left: 1px"><?php echo $bodyevent1_EN; ?></p></td>

     <td width="10%" style="text-align:center; vertical-align:middle;">
    <a href="#gardenImage_EN" data-id="/intranet/storage/app/public/images/{{$main2page->foto_en}}" 
 name="eventimg_EN[]" class="openImageDialog_EN thumbnail" data-toggle="modal">
<img class="imgevent_EN" src="/intranet/storage/app/public/images/{{$main2page->foto_en}}" 
 style="width:90%; cursor: pointer;"></a></td>
<!-----16-2-2020--------------------------------->
<!-----16-2-2020--------------------------------->

@if($main2page->si_primero_foto=='si_primero_foto')

<td width="30%" style="text-align:center; font-family: Montserrat, sans-serif; vertical-align:middle;"><p style="padding-right: 1px;

    padding-left: 1px"><input type="checkbox" style="text-align:center;" id="ch1" name="si_primero_foto" value="si_primero_foto" checked></p></td>
@else

<td width="30%" style="text-align:center; font-family: Montserrat, sans-serif; vertical-align:middle;"><p style="padding-right: 1px;

    padding-left: 1px"><input type="checkbox" style="text-align:center;" id="ch1" name="si_primero_foto" value="si_primero_foto"></p></td>


@endif

<!-----16-2-2020-------------------------------->
<!-----16-2-2020--------------------------------->

      <!-------------------------------------->


                
    <td style="font-family: Montserrat, sans-serif; text-align:center; vertical-align:middle; width:1%;
    white-space:nowrap;">
               


                <a href="/intranet/public/index/main2pages/{{$main2page->id}}/edit" 
                name="idc" value="{{$counter}}" 
                class="btn btn-primary" 
                style="color:white; border: none; background-color:rgba(4, 10, 100, 0.808);">
                <i class="fas fa-pen"></i> @lang('home.bl_edit4')</a>
                
               <?php
              $crt=$counter;
               ?>
               <input type="hidden" name="ccc" value="{{$crt}}">
                </td>
            </tr>





            @endforeach
            
            
           
            
        
        
       
        
@else

@endif
        
</tbody>

 

    </table>


        <!--<input type="checkbox" id="checktodo" style="border-radius:3px;">
        <button type="submit" class="btn btn-xs btn-danger" style="border: none; background-color:rgba(228, 20, 20, 0.931);">Borrar seleccionado(s)</button>-->
        <!--</td>
        </tr>-->
    
    </form>
</div>
    </div>


<!----------ALIGN-------------> 



<!--------------------------------->
<!--------------------------------->

    <div class="modal modal-danger fade" tabindex="-1" id="delete" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                
                
                <div class="modal-header">
                <a href="#" class="navbar-brand"><img src="{{url('/images/CCURRlogo.png')}}" style="width:65px;"></a>
                          
                        <h4 class="modal-title lang" style="margin:0 auto;" key="news">@lang('home.bl_deleteconf')</h4>
                      
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                
                            
                </div>
               
                <form id="sfdel" class="sfdel2" action="{{route('destroyevent','test')}}" method="post">
                {{method_field('delete')}}
                {{csrf_field()}}
                <div class="modal-body">
                        <p class="text-center">
                        @lang('home.bl_modalq') <label id="btitulo"></label>
                        </p>
                        <input type="hidden" name="id" id="cat_id" value="">
                </div>

                <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">@lang('home.bl_modalcancel')</button>
                <button type="submit" class="btn btn-danger" id="submitdel">@lang('home.bl_modaldelete')</button>
                </div>
                </form>


                </div>
                </div>
                </div>


<!-------------------------------------------------------->
<!-------------------------------------------------------->
<div class="modal fade" id="gardenImage_ES" tabindex="-1" role="dialog" aria-labelledby="gardenImageLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <img id="myImage_ES" class="img-responsive" src="" alt="" height="100%" width="100%">
            </div>
            <div class="modal-footer text-center" style="text-align:center; vertical-align:middle;">
            <fieldset class="align-center" 
            style="display: block;
    margin: auto;
    text-align: center;">
                <button type="button" style="color: white; border: rgba(228, 20, 20, 0.931); background-color:rgba(228, 20, 20, 0.931) ; float:center; font-family: Montserrat, sans-serif;" class="btn center-block" data-dismiss="modal">
                <i class="fas fa-times"></i> @lang('home.times_close')</button>
                </fieldset>
    </div>
        </div>
    </div>
</div>



<div class="modal fade" id="gardenImage_EN" tabindex="-1" role="dialog" aria-labelledby="gardenImageLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <img id="myImage_EN" class="img-responsive" src="" alt="" height="100%" width="100%">
            </div>
            <div class="modal-footer text-center" style="text-align:center; vertical-align:middle;">
            <fieldset class="align-center" 
            style="display: block;
    margin: auto;
    text-align: center;">
                <button type="button" style="color: white; border: rgba(228, 20, 20, 0.931); background-color:rgba(228, 20, 20, 0.931) ; float:center; font-family: Montserrat, sans-serif;" class="btn center-block" data-dismiss="modal">
                <i class="fas fa-times"></i> @lang('home.times_close')</button>
                </fieldset>
    </div>
        </div>
    </div>
</div>
<!------------------------------------------------>
<!------------------------------------------------->
<script>
$(document).on("click", ".openImageDialog_ES", function () {
    var myImageId = $(this).data('id');
    $("#gardenImage_ES .modal-body #myImage_ES").attr("src", myImageId);
    $("#gardenImage_ES").modal('show');
});
</script>
<script>
$(document).on("click", ".openImageDialog_EN", function () {
    var myImageId = $(this).data('id');
    $("#gardenImage_EN .modal-body #myImage_EN").attr("src", myImageId);
    $("#gardenImage_EN").modal('show');
});
</script>

<script>
                function Scrolldown() {
     //window.scroll(0,562); 
     document.getElementById("qs").scrollIntoView();
}

window.onload = Scrolldown;
</script>


<script>
$(document).ready(function(){
  

    $("#foto").change(function(){
  //---------------------------------------
  const file2 = this.files[0];
const  fileType = file2['type'];
const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
if (validImageTypes.includes(fileType)) {
    
    var file = document.getElementById("foto").files[0];

  var readImg = new FileReader();
  
  readImg.readAsDataURL(file);
  
  readImg.onload = function(e) {
    var span = document.createElement('span');
    $('#image-field').attr('src',e.target.result).fadeIn();
    
    }
    
}else{
  Swal.fire({
			
			html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
			'<br><br><h1>Warning!</h1><br>Please select an image with format .gif, .jpeg, .png!',
			
			showConfirmButton: false,
			timer:1800
		  })
  $('#image-field').attr('src','../images/nia.png').fadeIn();
}
        //----------------------------
  
    
  })

  $("#foto").show(function(){
    const file2 = this.files[0];
const  fileType = file2['type'];
const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
if (validImageTypes.includes(fileType)) {
   
    var file = document.getElementById("foto").files[0];

  var readImg = new FileReader();
  
  readImg.readAsDataURL(file);
  
  readImg.onload = function(e) {
    var span = document.createElement('span');
    $('#image-field').attr('src',e.target.result).fadeIn();
    
    }
}else{
  Swal.fire({
			
			html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
			'<br><br><h1>Warning!</h1><br>Please select an image with format .gif, .jpeg, .png!',
			
			showConfirmButton: false,
			timer:1800
		  })
  $('#image-field').attr('src','../images/nia.png').fadeIn();
}
  
    
  })
})
    </script>

                



<!---------------------------------------------------------->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>






<!--<script>
$(document).ready(function() {
    $('#event-table').DataTable( {
        "pagingType": "full_numbers"
    } );
} );

</script>-->
<script>

   

    

        $('#mainpage-table').DataTable({
          "language": {
            "emptyTable":			"@lang('home.emptyTable')",
				"info":		   		"@lang('home.info')",
				"infoEmpty":			"@lang('home.infoEmpty')",
				"infoFiltered":			"@lang('home.infoFiltered')",
				"infoPostFix":			"@lang('home.infoPostFix')",
				"lengthMenu":			"@lang('home.lengthMenu')",
				"loadingRecords":		"@lang('home.loadingRecords')",
				"processing":			"@lang('home.processing')",
				"search":			"",
				"searchPlaceholder":		"@lang('home.searchPlaceholder')",//"@lang('home.ver_slider')"
				"zeroRecords":			"@lang('home.zeroRecords')",
				"paginate": {
					"first":			"@lang('home.first')",
					"last":				"@lang('home.last')",
					"next":				"›",
					"previous":			"‹"
				},
				"aria": {
					"sortAscending":	"@lang('home.sortAscending')",
					"sortDescending":	"@lang('home.sortDescending')",
				}
			},

      //responsive: true;
        columns:[
          
            {data:'nro',name:'nro'},
            {data:'body_ES',name:'body_ES'},
            {data:'foto_ES',name:'foto_ES',orderable:false,searchable:false},
            
            {data:'body_EN',name:'body_EN'},
            {data:'foto_EN',name:'foto_EN',orderable:false,searchable:false},
            {data:'si_primero_foto',name:'si_primero_foto',orderable:false,searchable:false},
            {data:'action',name:'action',orderable:false,searchable:false}
            ]
    });

    $.fn.dataTable.ext.errMode = 'throw';

    
    

</script>

<script type="text/javascript" src="{{asset('js/app.js')}}"></script>

<script>

$(function(){
  
  $('select').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

//-----------------------------------------------------------
    //$(this).find('option:hover').css('background-color', 'red');
    //$(this).find('option:selected').css('background-color', 'red');
   
});

$('select').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});

//-------------------------------------------------
$('option').mouseover(function () {
    $(this).css('color', 'green');
   // $(this).css('box-shadow', '0 0 0px');
});

  document.getElementsByTagName("input")[1].setAttribute("id", "insearch");
  $("#insearch").focusin(function(){
    document.getElementById("insearch").style.borderColor = "rgb(11, 11, 132, 0.993)";
                  
                  document.getElementById("insearch").style.boxShadow = "0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)";
                 
                });
                $("#insearch").focusout(function(){
    document.getElementById("insearch").style.borderColor = "#ccc";
                  
                  document.getElementById("insearch").style.boxShadow = "0 0 0px";
                 
                });
    
                
                document.getElementsByClassName("col-sm-12")[0].setAttribute("class", "col-xs-12 col-sm-6 col-md-12 col-lg-6");
                document.getElementsByClassName("col-sm-12")[0].setAttribute("class", "col-xs-12 col-sm-6 col-md-12 col-lg-6");
                document.getElementsByClassName("col-md-5")[0].setAttribute("class", "col-12");
                document.getElementsByClassName("col-md-7")[0].setAttribute("class", "col-12");
                //document.getElementsByClassName("col-12")[1].style.textAlign = "center";
                document.getElementsByClassName("col-12")[2].style.textAlign = "center";

                document.getElementsByClassName("pagination").setAttribute("role", "navigation");
                //document.getElementsByClassName("pagination").setAttribute("class", "pagination text-center");
                
                //document.getElementById("vcurso-table_paginate").style.Align = "center";
                //  document.getElementsByClassName("custom-select custom-select-sm form-control form-control-sm").setAttribute("id", "sellin");
                document.getElementsByTagName("select")[0].setAttribute("id", "selnum");

                $("#selnum").focusin(function(){
    document.getElementById("selnum").style.borderColor = "rgb(11, 11, 132, 0.993)";
                  
                  document.getElementById("selnum").style.boxShadow = "0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)";
                 
                });
                $("#selnum").focusout(function(){
    document.getElementById("selnum").style.borderColor = "#ccc";
                  
                  document.getElementById("selnum").style.boxShadow = "0 0 0px";
                 
                });

                document.getElementsByTagName("option")[0].setAttribute("id", "diez");
                document.getElementsByTagName("option").style.fontFamily="Monserrat,sans-serif";


               // document.getElementsByClassName("select-checkbox").style.boxShadow = "2 2 2px";
});

</script>

<script>

$('#delete').on('show.bs.modal',function(event){
  var button=$(event.relatedTarget)
  var cat_id=button.data('catid')
  var modal=$(this)

 //print link title into modal

 var btitulo = button.data("titulo");
        //$(this).find(".modal-title").text(blink);
        modal.find(".modal-body #btitulo").text(btitulo);
//print link title into modal

  modal.find('.modal-body #cat_id').val(cat_id);

})
</script>
<script>
  /*$(function(){
    $("#checktodo").click(function(){
      $("input[class='cbx1']").attr("checked",this.checked)
    });

    $("input[class='cbx1']").click(function(){
      if($("input[class='cbx1']").length == $("input[class='cbx1']:checked").length){
        $("#checktodo").attr("checked","checked");
      }else{
        $("#checktodo").removeAttr("checked");
      }
    });
  });*/
  $("#checktodo").change(function(){
      $(".cbx1").prop("checked",$(this).prop("checked"))
    });
    $(".cbx1").change(function(){
      if($(this).prop("checked")==false){
        $("#checktodo").prop("checked",false)
      }
      if($(".cbx1:checked").length==$(".cbx1").length){
        $("#checktodo").prop("checked",true)
      }
    })
</script>


<!--<script>
$("#modal-edit").on('shown.bs.modal', function() {
     CKEDITOR.replace('body');
   })



</script>-->

<script>
      
      $('#modal-edit').on('show.bs.modal',function(event){

        

  var button=$(event.relatedTarget)
  var titulo=button.data('titulo')
  var body=button.data('body')
  var link=button.data('link')
  var eid=button.data('eid')
  var foto=button.data('foto')
  var modal=$(this)

 //print link title into modal
        //alert(foto);
 
        //$(this).find(".modal-title").text(blink);
        modal.find(".modal-body #titulo").val(titulo);
        modal.find(".modal-body #body").val(body);
        modal.find(".modal-body #link").val(link);
        modal.find(".modal-body #foto").val(foto);
        modal.find(".modal-body #e_id").val(eid);
        
       
//print link title into modal

  //modal.find('.modal-body #cat_id').val(cat_id);

}) 
</script>
@endsection

<!--@section('js')
<script>
        CKEDITOR.replace( '.modal-body #body' );
    </script>
@endsection-->

@section('content2')


@endsection














