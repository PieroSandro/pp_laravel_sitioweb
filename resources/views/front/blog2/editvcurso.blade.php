@extends('layouts.mainblog')
@section('titleblog')


<!--@lang('home.bl_editpost')<php echo $post->titulo?>-->


@endsection

@section('carusel')


              @endsection
@section('content')
<div class="text">
<div style="padding-top: 40px;
  padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">
<a href="{{route('backvcursohome')}}" class="btn btn-success"><i class="fas fa-angle-left"></i> @lang('home.bl_back')</a>
<br><br>
<h1>Editar curso online</h1>
    {!! Form::open(['action'=> ['VcursoController@updatevcurso',$vcurso->id], 'method'=> 'PUT','enctype'=>'multipart/form-data']) !!}
        
            {{Form::text('id',$vcurso->id,['class'=>'form-control','hidden'])}}
            @csrf
            @if(session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>

            @endif
        <br>
        <div class="form-group">
            <label for="titulo" class="mt-3">@lang('home.bl_titulo')</label>

            <!--{{Form::label('titulo','Titulo')}}-->
            {{Form::text('titulo',$vcurso->titulo,['class'=>'form-control','placeholder'=>'Titulo'])}}
        </div>
        <br>
        <div class="form-group">
            <label for="body" class="mt-3">@lang('home.bl_body')</label>

            <!--{{Form::label('body','Body')}}-->
            {{Form::textarea('body',$vcurso->body,['name'=>'body','class'=>'form-control','placeholder'=>'Body Text'])}}
        </div>

        <br>
        <div class="form-group">
        <label for="link" class="mt-3">Link</label>
       <br>
       {{Form::text('link',$vcurso->link,['class'=>'form-control','placeholder'=>'Link'])}}
           </div>  
           <br>
        {{Form::hidden('_method','PUT')}}
        <input type="submit" class="btn btn-success" value="@lang('home.bl_change')">
        {!! Form::close() !!}
    </div>
    </div>
@endsection

@section('js')
<!--<script src="{{url('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>-->

<script>
        CKEDITOR.replace( 'body' );
    </script>
@endsection

@section('content2')

<?php

$postcomentados=array();
$postcantidad=array();
$py=array();
$arr='';
$ct=0;

use App\Post;
$posts=Post::all();
foreach($posts as $post){

$titulopost='';
$counter = 0;
foreach($post->comentarios as $comentario){
    $counter++;
    } $titulopost=$post->titulo;
    
    
    $postcomentados[]=$titulopost;
    $postcantidad[]=$counter;

$ct++;
}



for($i=0;$i<$ct;$i++){   
$py[$postcomentados[$i]]=$postcantidad[$i];

}

arsort($py);?>
@lang('home.bl_gencoment')


<?php
echo "<br>";
echo "<br>";
$i=0; 
foreach($py as $x => $x_value) {?>

	@lang('home.bl_title')
	
	<?php
	
		echo $x." | "?>@lang('home.bl_comments') <?php echo $x_value;
		echo "<br>";
		 
		if($i==9){
			break;
		}
	
		else{
		$i++;}
	}

	if($i==0){?>
		@lang('home.bl_nopost')
		<?php }
	


//--------------------------------
/*$age = array("Ben"=>"37", "Peter"=>"35", "Joe"=>"43");
print_r($age);
arsort($age);

foreach($age as $x => $x_value) {
    echo "Key=" . $x . ", Value=" . $x_value;
    echo "<br>";
}*/
//------------------------------------------------
?>
@endsection