@extends('layouts.mainblog')
@section('titleblog')


@lang('home.bl_shanuncio')<?php echo $anuncio->titulo?>


@endsection

@section('carusel')


              @endsection
@section('content')
<style>
#txt4{
    /*max-height: 410px;
    max-width:350px;
    min-width:50%;*/
    overflow: auto;
    width:100%;
}
</style>
<div class="text">
<div style="padding-top: 40px;
  padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">
<a href="{{route('backanunciohome')}}" class="btn btn-success"><i class="fas fa-angle-left"></i> @lang('home.bl_back')</a>
<br><br>
<h1>{{$anuncio->titulo}}</h1>
<br>
<div id="txt4">
<h3>{!!$anuncio->body!!}</h3>
<br>
<img src="/intranet/storage/app/public/images/{{$anuncio->foto}}"><!-- style="width:100%"-->
</div>
<br>
<h4>@lang('home.bl_vanuncio') {{$anuncio->fecha_vence}}</h4>
<hr>
<small>@lang('home.bl_fcreacion'): {{$anuncio->created_at}}</small><br>
        <small>@lang('home.bl_factualizacion'): {{$anuncio->updated_at}}</small>  
<hr>
<div class="sharethis-inline-share-buttons"></div>

@guest

@else
<a href="/intranet/public/index/anuncios/{{$anuncio->id}}/edit" class="btn btn-primary"><i class="fas fa-pen"></i> @lang('home.bl_edit4')</a>

<button data-toggle="modal" data-target="#delete2" class="btn btn-danger">
<i class="fas fa-trash"></i> @lang('home.bl_borrar')
</button>
<!--{!!Form::open(['route'=>['destroy_an',$anuncio->id],'method'=>'POST','class'=>'float-right'])!!}





{{Form::hidden('_method','DELETE')}}
{{Form::submit('Delete',['class'=>'btn btn-danger'])}}-->
{!!Form::close()!!}
@endguest

		
	
</div>
</div>
<div class="modal modal-danger fade" id="delete2" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<a href="#" class="navbar-brand"><img src="{{url('/images/CCURRlogo.png')}}" style="width:65px;"></a>
          
        <h4 class="modal-title lang" style="margin:0 auto;" key="news">@lang('home.bl_deleteconf')</h4>
      
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

            
</div>
{!!Form::open(['route'=>['destroy_an',$anuncio->id],'method'=>'POST','class'=>'float-right'])!!}
@csrf
{{Form::hidden('_method','DELETE')}}

<div class="modal-body">
		<p class="text-center">
		@lang('home.bl_modalq2') <?php echo "'"?>{{$anuncio->titulo}}<?php echo "'?"?>
		</p>
		
</div>		
<div class="modal-footer">
<button type="button" class="btn btn-success" data-dismiss="modal">@lang('home.bl_modalcancel')</button>
<button type="submit" class="btn btn-danger">@lang('home.bl_modaldelete')</button>
</div>
{!!Form::close()!!}
</div>
</div>
</div>
@endsection

@section('content2')

<?php

$postcomentados=array();
$postcantidad=array();
$py=array();
$arr='';
$ct=0;

use App\Post;
$posts=Post::all();
foreach($posts as $post){

$titulopost='';
$counter = 0;
foreach($post->comentarios as $comentario){
    $counter++;
    } $titulopost=$post->titulo;
    
    
    $postcomentados[]=$titulopost;
    $postcantidad[]=$counter;

$ct++;
}



for($i=0;$i<$ct;$i++){   
$py[$postcomentados[$i]]=$postcantidad[$i];

}

arsort($py);?>
@lang('home.bl_gencoment')


<?php
echo "<br>";
echo "<br>";
$i=0; 
foreach($py as $x => $x_value) {?>

	@lang('home.bl_title')
	
	<?php
	
		echo $x." | "?>@lang('home.bl_comments') <?php echo $x_value;
		echo "<br>";
		 
		if($i==9){
			break;
		}
	
		else{
		$i++;}
	}

	if($i==0){?>
		@lang('home.bl_nopost')
		<?php }
	


//--------------------------------
/*$age = array("Ben"=>"37", "Peter"=>"35", "Joe"=>"43");
print_r($age);
arsort($age);

foreach($age as $x => $x_value) {
    echo "Key=" . $x . ", Value=" . $x_value;
    echo "<br>";
}*/
//------------------------------------------------
?>
@endsection