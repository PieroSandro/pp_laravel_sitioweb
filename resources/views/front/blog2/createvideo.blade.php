@extends('layouts.mainblog')
@section('titleblog')

@lang('home.createvideos')

@endsection



@section('carusel')


<!--------12_01_2020-------->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<link rel="stylesheet" href="{{url('/css/animate.css')}}">







<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        
        <script>
        AOS.init({
          duration:3000,
        });
        </script>





<!------------------->

@endsection



@section('content')
<style>


.swal2-modal .swal2-styled{
    
  background-color: rgba(11, 11, 132, 0.993);
    font-family: 'Montserrat', sans-serif;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js">
</script>
<div class="text">
<div style="padding-top: 40px;
  padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<a href="{{route('backvideohome')}}" class="btn" 
style="color: white; border: rgba(228, 20, 20, 0.931); background-color:rgba(228, 20, 20, 0.931) ;font-family: Montserrat, sans-serif;">
<i class="fas fa-angle-left"></i> @lang('home.govideos')</a>

<!--
<a href="{{route('backbannerhome')}}" class="btn btn-success"><i class="fas fa-angle-left"></i> @lang('home.bl_back')</a>-->
<br><br>
<h1 style="color: black; font-family: Montserrat, sans-serif; width:100%;">@lang('home.createvideos')</h1>
<br>
{!! Form::open(['action'=> 'HomeController@storevideo', 'method'=> 'POST','enctype'=>'multipart/form-data']) !!}

@csrf

@if(session('success'))
 
<?php
           
           $locale=App::getLocale();

           if(App::isLocale('en')){
             ?>
        
        <script>

Swal.fire({
 
 html: '<i class="fas fa-info-circle" style="color:rgba(11, 11, 132, 0.993); font-size:150px;"></i>'+
 '<br><br><br><div style="font-family: Montserrat, sans-serif;">{!!session('success')!!}</div>',
 
 showConfirmButton: true,
 })   
          
</script>
               <?php    
                       
         }else{?>
            <script>

Swal.fire({
 
 html: '<i class="fas fa-info-circle" style="color:rgba(11, 11, 132, 0.993); font-size:150px;"></i>'+
 '<br><br><br><div style="font-family: Montserrat, sans-serif;">{!!session('success')!!}</div>',
 
 showConfirmButton: true,
 })   
          
</script>
           
           <?php
           }

         ?>

            @endif

    <span id="result"></span>
            
        <br>

         <!-----------------------CREATEVCURSOFORM---------------------------------------------------------------------------------->
         <div id="ggg">
       
       <div id="row1" class="card">
       <div style="padding-top: 20px;padding-right: 20px;
 padding-bottom: 20px;
 padding-left: 20px;">
       
       <div class="form-group" style="font-family: Montserrat;">       
       <label for="vid" style="font-family: Montserrat;" class="mt-3">Video</label>
       <!--<input type="file" id="foto" accept="image/*" name="foto[1]" value="" style="width: 100%;">-->
       <input class="form-control" placeholder="" id="vid" name="vid[1]" type="text" value="">
       <br><br>
       <div style="text-align:center;">
    <iframe width="320" height="240" src="" id="vid-field"
    frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
       </div>
       <br>
       </div>
       
       <div class="form-group">
       <label for="link" style="font-family: Montserrat;" 
       class="mt-3">@lang('home.dt_contenido')</label>




      <!-- <input class="form-control" placeholder="" name="link[1]" type="text" value="">-->
      <textarea name="link[1]" class="form-control" 
      placeholder="Body Text" cols="50" rows="10"></textarea>

       <br>
       
       </div>




       </div>
       </div>
       </div>
       <!--------------------------------------------------------------------------------------------------------------------->

       <br>
        <label class="col text-center">
        <a class="btn" id="addRow" style="color: white; border: rgba(11, 11, 132, 0.993); background-color:rgba(11, 11, 132, 0.993) ;font-family: Montserrat, sans-serif;"><i class="fas fa-plus-circle"></i> @lang('home.addrecord')</a>
        </label>
        &nbsp
        <label class="col text-center">
        <button type="submit" name="save" id="save" class="btn" style="color: white; border: rgba(11, 11, 132, 0.993); background-color:rgba(11, 11, 132, 0.993) ;font-family: Montserrat, sans-serif;">
<i class="fas fa-save"></i> @lang('home.saverecord')
</button>
        </label>
        {!! Form::close() !!}
    </div>
    </div>


    <script>
$(function(){
  $('#vid').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

//-----------------------------------------------------------
    //$(this).find('option:hover').css('background-color', 'red');
    //$(this).find('option:selected').css('background-color', 'red');
   
});


$('#vid').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});
});
</script>
<!-------------------------------------------------------------------------------------------->
<!-------------------------------------------------------------------------------------------->

    <script>
    $( document ).ready(function() {
    var count=1;

    $('#addRow').on('click',function(){
    count++;
    neobanner(count);
});
    
    function neobanner(i)
    {
        var tr= '<div id="row'+i+'" class="card" style="margin-top: 20px;"><div style="padding-top: 20px;padding-right: 20px;padding-bottom: 20px;padding-left: 20px;"><div class="form-group" style="font-family: Montserrat;"><label for="vid" style="font-family: Montserrat;" class="mt-3">Video</label>'+        
//'<input id="foto'+i+'" accept="image/*" type="file" name="foto[' + i +']" value="" style="width: 100%;">'+
'<input class="form-control" placeholder="" id="vid'+i+'" name="vid[' + i +']"  type="text" value=""><br><br>'+
                        '<div style="text-align:center;"><iframe width="320" height="240" src="" id="vid-field'+i+'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>'+
                    
                    '<br>'+

'</div>'+

'<div class="form-group" style="font-family: Montserrat;">'+

        '<label for="link" class="mt-3">@lang('home.dt_contenido')</label>'+

        '<textarea name="link[' + i +']" class="form-control" placeholder="Body Text" cols="50" rows="10"></textarea>'+

                   '<br>'+
           

        '</div>'
        ;



tr+='<label class="col text-center"><a class="btn deleteRow" id="'+i+'" style="color: white; border: rgba(228, 20, 20, 0.931); background-color:rgba(228, 20, 20, 0.931) ;font-family: Montserrat, sans-serif;"><i class="fas fa-minus-circle"></i> @lang('home.deleterecord')</a></label><br></div></div>';


$('#ggg').append(tr);
                //---------script_foto_append-----------------------------
                CKEDITOR.replace( 'link[' + i +']' );

//---------script_foto_append-----------------------------
//<a class="btn" id="addRow" style="color: white; border: rgba(11, 11, 132, 0.993); background-color:rgba(11, 11, 132, 0.993) ;font-family: Montserrat, sans-serif;"><i class="fas fa-plus-circle"></i> @lang('home.addrecord')</a>

$('#vid'+i+'').focusin(function () {
$(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
$(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

//-----------------------------------------------------------
//$(this).find('option:hover').css('background-color', 'red');
//$(this).find('option:selected').css('background-color', 'red');

});


$('#vid'+i+'').focusout(function () {
$(this).css('border-color', '#ccc');
$(this).css('box-shadow', '0 0 0px');

});









var imgVal2 = $('#vid'+i+'').val(); 
        if(imgVal2=='') 
        { 
          
          $('#vid-field'+i+'').attr('src','../images/nia.png').fadeIn();
        }

$('#vid'+i+'').change(function(){
  
var imgVal2=$('#vid'+i+'').val();
if(imgVal2!==''){
  var imgValue2 = imgVal2.replace("watch?v=", "embed/");   
$('#vid-field'+i+'').attr('src',imgValue2).fadeIn();
}else{
  $('#vid-field'+i+'').attr('src','../images/nia.png').fadeIn();
}
  })

  $('#vid'+i+'').show(function(){
   
var imgVal2=$('#vid'+i+'').val();
if(imgVal2!==''){  
  var imgValue2 = imgVal2.replace("watch?v=", "embed/"); 
$('#vid-field'+i+'').attr('src',imgValue2).fadeIn();
}else{
  $('#vid-field'+i+'').attr('src','../images/nia.png').fadeIn();
}
  
    
  })

  $('#vid'+i+'').keypress(function(){
    var imgVal2=$('#vid'+i+'').val();
if(imgVal2!==''){
  var imgValue2 = imgVal2.replace("watch?v=", "embed/");   
$('#vid-field'+i+'').attr('src',imgValue2).fadeIn();
}else{
  $('#vid-field'+i+'').attr('src','../images/nia.png').fadeIn();
}
                });

                $('#vid'+i+'').keydown(function(){
                  var imgVal2=$('#vid'+i+'').val();
if(imgVal2!==''){
  var imgValue2 = imgVal2.replace("watch?v=", "embed/");   
$('#vid-field'+i+'').attr('src',imgValue2).fadeIn();
}else{
  $('#vid-field'+i+'').attr('src','../images/nia.png').fadeIn();
}
                });

                $('#vid'+i+'').keyup(function(){
                  var imgVal2=$('#vid'+i+'').val();
if(imgVal2!==''){
  var imgValue2 = imgVal2.replace("watch?v=", "embed/");   
$('#vid-field'+i+'').attr('src',imgValue2).fadeIn();
}else{
  $('#vid-field'+i+'').attr('src','../images/nia.png').fadeIn();
}
                });

                $('#vid'+i+'').focusin(function(){
                  var imgVal2=$('#vid'+i+'').val();
if(imgVal2!==''){  
  var imgValue2 = imgVal2.replace("watch?v=", "embed/");  
$('#vid-field'+i+'').attr('src',imgValue2).fadeIn();
}else{
  $('#vid-field'+i+'').attr('src','../images/nia.png').fadeIn();
}
                });
                //---------script_foto_append-----------------------------
    }
   
    $(document).on('click','.deleteRow',function(){
    var button_id=$(this).attr("id");
    $('#row'+button_id+'').remove();
});



    });
    </script>

    
<!--SCRIPT PARA LA FOTO----------------------------------------------------------------------------------->
    <script>
     $(document).ready(function(){

      var imgVal = $('#vid').val();
     
        if(imgVal=='') 
        { 
          
          $('#vid-field').attr('src','../images/nva.jpg').fadeIn();
        }

$("#vid").change(function(){
  
var imgVal=$("#vid").val();
if(imgVal!==''){
  var imgValue = imgVal.replace("watch?v=", "embed/");
$('#vid-field').attr('src',imgValue).fadeIn();
}else{
  $('#vid-field').attr('src','../images/nva.jpg').fadeIn();
}
  })

  $("#vid").show(function(){
   
var imgVal=$("#vid").val();
if(imgVal!==''){ 
  var imgValue = imgVal.replace("watch?v=", "embed/");
$('#vid-field').attr('src',imgValue).fadeIn();
}else{
  $('#vid-field').attr('src','../images/nva.jpg').fadeIn();
}
  
    
  })

  $("#vid").keypress(function(){
    var imgVal=$("#vid").val();
if(imgVal!==''){
  var imgValue = imgVal.replace("watch?v=", "embed/");
$('#vid-field').attr('src',imgValue).fadeIn();
}else{
  $('#vid-field').attr('src','../images/nva.jpg').fadeIn();
}
                });

                $("#vid").keydown(function(){
                  var imgVal=$("#vid").val();
if(imgVal!==''){
  var imgValue = imgVal.replace("watch?v=", "embed/");
$('#vid-field').attr('src',imgValue).fadeIn();
}else{
  $('#vid-field').attr('src','../images/nva.jpg').fadeIn();
}
                });

                $("#vid").keyup(function(){
                  var imgVal=$("#vid").val();
if(imgVal!==''){   
  var imgValue = imgVal.replace("watch?v=", "embed/");
$('#vid-field').attr('src',imgValue).fadeIn();
}else{
  $('#vid-field').attr('src','../images/nva.jpg').fadeIn();
}
                });

                $("#vid").focusin(function(){
                  var imgVal=$("#vid").val();
if(imgVal!==''){
  var imgValue = imgVal.replace("watch?v=", "embed/");  
$('#vid-field').attr('src',imgValue).fadeIn();
}else{
  $('#vid-field').attr('src','../images/nva.jpg').fadeIn();
}
                });
        
  
})
</script>
<!--SCRIPT PARA LA FOTO--------------------------------------------------->
   @endsection



@section('content2')

<?php

$postcomentados=array();
$postcantidad=array();
$py=array();
$arr='';
$ct=0;

use App\Post;
$posts=Post::all();
foreach($posts as $post){

$titulopost='';
$counter = 0;
foreach($post->comentarios as $comentario){
    $counter++;
    } $titulopost=$post->titulo;
    
    
    $postcomentados[]=$titulopost;
    $postcantidad[]=$counter;

$ct++;
}



for($i=0;$i<$ct;$i++){   
$py[$postcomentados[$i]]=$postcantidad[$i];

}

arsort($py);?>
@lang('home.bl_gencoment')


<?php
echo "<br>";
echo "<br>";
$i=0; 
foreach($py as $x => $x_value) {?>

	@lang('home.bl_title')
	
	<?php
	
		echo $x." | "?>@lang('home.bl_comments') <?php echo $x_value;
		echo "<br>";
		 
		if($i==9){
			break;
		}
	
		else{
		$i++;}
  }
  
  if($i==0){?>
    @lang('home.bl_nopost')
    <?php }



?>
@endsection

@section('js')
<script>
        CKEDITOR.replace( 'link[1]' );
    </script>
@endsection