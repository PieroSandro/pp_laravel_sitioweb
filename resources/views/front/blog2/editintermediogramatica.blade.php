@extends('layouts.mainblog')
@section('titleblog')


@lang('home.editintermediogramaticas')

@endsection

@section('carusel')


<!--------11_01_2020-------->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<link rel="stylesheet" href="{{url('/css/animate.css')}}">







<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        
        <script>
        AOS.init({
          duration:3000,
        });
        </script>





<!------------------->
              @endsection



@section('content')
<style>


.swal2-modal .swal2-styled{
    
    background-color: rgba(4, 10, 100, 0.808);
}
</style>

<div class="text">
<div style="padding-top: 40px;
  padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">
  <!--<a href="/intranet/public/index/anuncios/$anuncio->id}}" class="btn btn-success"><i class="fas fa-angle-left"></i> @lang('home.bl_back')</a>-->
  <a href="{{route('backintermediogramaticahome')}}" class="btn" style="color: white; border: rgba(228, 20, 20, 0.931); background-color:rgba(228, 20, 20, 0.931) ;font-family: Montserrat, sans-serif;"><i class="fas fa-angle-left"></i> @lang('home.bl_back')</a>
  <br><br>
  <h1 style="color: black; font-family: Montserrat, sans-serif; width:100%;">@lang('home.editintermediogramaticas')</h1>
<!-----------------------------------------------FORMULARIO: OPEN--------------------------------------------------------------------->
{!! Form::open(['action'=> ['IntermediogramaticaController@updateintermediogramatica',$intermediogramatica->id], 'method'=> 'PUT','enctype'=>'multipart/form-data']) !!}
        {{Form::text('id',$intermediogramatica->id,['class'=>'form-control','hidden'])}}
        @csrf
            @if(session('success'))
            <script>

Swal.fire({
  
  html: '<br><br><i class="fas fa-info-circle" style="color: rgba(11, 11, 132, 0.993); font-size:150px;"></i>'+
  '<br><br><br><div style="font-family: Montserrat, sans-serif;">{{session('success')}}</div><br><br>',
  
  showConfirmButton: false,//false
  timer:1500
  //timer 1500
  })
  

           
          
           
</script>

            @endif
        
            <span id="result"></span>
        <br>
        <!------>

        <div id="ggg">
       
       <div id="row1" class="card">
       <div style="padding-top: 20px;padding-right: 20px;
 padding-bottom: 20px;
 padding-left: 20px;">
        <div class="form-group">
        <label for="foto" style="font-family: Montserrat;" class="mt-3">@lang('home.dt_foto')</label>
               <br>
                    
                    <div class="align-center" 
            style="display: block;
    margin: auto;
    text-align: center; float: center; font-family: Montserrat, sans-serif;">
                    <input id="foto" accept='image/*' 
                    type="file" name="foto" value="{{$intermediogramatica->foto}}" 
                    style="font-family: Montserrat, sans-serif; width: 100%; text-align: center;">
                   
                    </div>
                    <br><br>
                                <div style="text-align:center;">
                                <img src="/intranet/storage/app/public/images/{{$intermediogramatica->foto}}" id="image-field" width="250" height="250"></div>
           
                   </div>
            
        <div class="form-group" style="font-family: Montserrat;">
        <label for="link" style="font-family: Montserrat;" class="mt-3">@lang('home.linkaudios')</label>

            {{Form::text('link',$intermediogramatica->link,['id'=>'idlink','class'=>'form-control','placeholder'=>'Link'])}}
        </div>




        <!--1-02-->
        <div class="form-group" style="font-family: Montserrat;">
            <label for="body" class="mt-3">@lang('home.bl_body')</label>

            <!--{{Form::label('body','Body')}}-->
            {{Form::textarea('body',$intermediogramatica->body,['id'=>'idbody','name'=>'body','class'=>'form-control','placeholder'=>'Body Text'])}}
        </div>
        <!--1-02-->
        </div>
        </div>
        </div>
         <!----->
           
        {{Form::hidden('_method','PUT')}}
        <br>
        <label class="col text-center">
        <button type="submit" class="btn" style="color: white; border: rgba(11, 11, 132, 0.993); background-color:rgba(11, 11, 132, 0.993) ;font-family: Montserrat, sans-serif;">
<i class="fas fa-save"></i> @lang('home.bl_change')
</button>        </label>
    {!! Form::close() !!}
<!-----------------------------------------------FORMULARIO: CLOSE--------------------------------------------------------------------->

</div>
    </div>

    <script>
$(document).ready(function(){


    $("#foto").change(function(){
  //---------------------------------------
  const file2 = this.files[0];
const  fileType = file2['type'];
const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
if (validImageTypes.includes(fileType)) {
    //alert('NOES');
    var file = document.getElementById("foto").files[0];

  var readImg = new FileReader();
  
  readImg.readAsDataURL(file);
  
  readImg.onload = function(e) {
    var span = document.createElement('span');
    $('#image-field').attr('src',e.target.result).fadeIn();
    
    }
    //document.getElementById("image-field").src = "../images/nia.png";
}else{
  Swal.fire({
			
    html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
			'<br><br><h1 style="color: black; font-family: Montserrat, sans-serif;">@lang('home.warning_adver')</h1><br><div style="font-family: Montserrat, sans-serif;">@lang('home.warning_adver_2')</div>',
			
			showConfirmButton: false,
			timer:2000
		  })
  $('#image-field').attr('src','../images/nia.png').fadeIn();
}
        //----------------------------
  
    
  })

  $("#foto").show(function(){
    const file2 = this.files[0];
const  fileType = file2['type'];
const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
if (validImageTypes.includes(fileType)) {
    // invalid file type code goes here.
    //alert('NOES');
    //document.getElementById("image-field").src = "../images/nia.png";
    var file = document.getElementById("foto").files[0];

  var readImg = new FileReader();
  
  readImg.readAsDataURL(file);
  
  readImg.onload = function(e) {
    var span = document.createElement('span');
    $('#image-field').attr('src',e.target.result).fadeIn();
    
    }
}else{
  Swal.fire({
			
    html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
			'<br><br><h1 style="color: black; font-family: Montserrat, sans-serif;">@lang('home.warning_adver')</h1><br><div style="font-family: Montserrat, sans-serif;">@lang('home.warning_adver_2')</div>',
			
			showConfirmButton: false,
			timer:2000
		  })
  $('#image-field').attr('src','../images/nia.png').fadeIn();
}
  
    
  })
})
    </script>
@endsection



@section('content2')

<?php

$postcomentados=array();
$postcantidad=array();
$py=array();
$arr='';
$ct=0;

use App\Post;
$posts=Post::all();
foreach($posts as $post){

$titulopost='';
$counter = 0;
foreach($post->comentarios as $comentario){
    $counter++;
    } $titulopost=$post->titulo;
    
    
    $postcomentados[]=$titulopost;
    $postcantidad[]=$counter;

$ct++;
}



for($i=0;$i<$ct;$i++){   
$py[$postcomentados[$i]]=$postcantidad[$i];

}

arsort($py);?>
@lang('home.bl_gencoment')


<?php
echo "<br>";
echo "<br>";
$i=0; 
foreach($py as $x => $x_value) {?>

	@lang('home.bl_title')
	
	<?php
	
		echo $x." | "?>@lang('home.bl_comments') <?php echo $x_value;
		echo "<br>";
		 
		if($i==9){
			break;
		}
	
		else{
		$i++;}
	}

	if($i==0){?>
		@lang('home.bl_nopost')
		<?php }
	


//--------------------------------
/*$age = array("Ben"=>"37", "Peter"=>"35", "Joe"=>"43");
print_r($age);
arsort($age);

foreach($age as $x => $x_value) {
    echo "Key=" . $x . ", Value=" . $x_value;
    echo "<br>";
}*/
//------------------------------------------------
?>
@endsection
@section('js')
<!--<script>
        CKEDITOR.replace( 'body' );
    </script>-->
    <script>
$(function(){
  $('#idlink').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

   
});


$('#idlink').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});



$('#idbody').focusin(function () {
    $(this).css('border-color', 'rgb(11, 11, 132, 0.993)');
    $(this).css('box-shadow', '0 0 1.5px 2.9px rgba(124, 160, 250, 0.993)');

   
});


$('#idbody').focusout(function () {
    $(this).css('border-color', '#ccc');
    $(this).css('box-shadow', '0 0 0px');
   
});
});
</script>
@endsection