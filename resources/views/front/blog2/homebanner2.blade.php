@extends('layouts.mainblog')
@section('titleblog')



@lang('home.bl_listan')

@endsection
@section('carusel')

<style>
#bt1car:before{
	
	content:'@lang('home.ver_slider')';
	align-content: center;
	font-size:12.4px;/*15px */
	position:absolute;
	top:50%;
	left:50%;
	transition:all 0.3s;
	transform:translate(-50%,-50%);
}
</style>
   <div class="carousel slide" id="MagicCarousel" data-ride="carousel">
                <ol id="myCarousel-indicators" class="carousel-indicators">
                  <li class="active" data-slide-to="0" data-target="#MagicCarousel"></li>
                  <li data-slide-to="1" data-target="#MagicCarousel"></li>
                  <li data-slide-to="2" data-target="#MagicCarousel"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                      <div class="carousel-item active">
                          
                          <img class="d-block w-100" src="{{url('/images/banner-de-iniciacion-1.png')}}" alt="First slide">
                          <div class="carousel-caption">
                          
                               <a class="btn animated shake" href="https://elcomercio.pe/" id="bt1car" target="_blank" role="button">
                                
                               </a>

                          </div>  
                      </div>
                      <div class="carousel-item">
                          <img class="d-block w-100" src="{{url('/images/BANER-DE-INICIACION-2.png')}}" alt="Second slide">
                          <div class="carousel-caption">
                          
                                  <a class="btn animated shake" href="https://www.goltelevision.com/" id="bt1car" target="_blank" role="button">
                                
                               </a>
                          </div>  
                      </div>
                      <div class="carousel-item">
                          <img class="d-block w-100" src="{{url('/images/baner-de-iniciacion-3.png')}}" alt="Third slide">
                          
                          <div class="carousel-caption">
                          <a class="btn animated shake" href="https://elcomercio.pe/" id="bt1car" target="_blank" role="button">
                                
                               </a> 
                                  
                          </div>  
                      </div>
                </div>
                <a class="carousel-control-prev" href="#MagicCarousel"
            data-slide="prev" role="button">
              <span class="carousel-control-prev-icon"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#MagicCarousel"
            data-slide="next" role="button">
              <span class="carousel-control-next-icon"></span>
              <span class="sr-only">Next</span>
            </a>
              </div>
              @endsection
@section('content')
<!--------------ANIMATE ON SCROLL------------------------->
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        
        <script>
        AOS.init({
          duration:3000,
        });
        </script>
        <!------------------------------------------->

<style>
#txt4{
    /*max-height: 410px;
    max-width:350px;
    min-width:50%;*/
    overflow: auto;
    width:100%;
}
</style>






<label id="qs">
<br>
<br>
<div class="text">
<div data-aos="fade-right" style="padding-top: 40px;
  padding-right: 40px;
  padding-bottom: 40px;
  padding-left: 40px;">
  
<h1>@lang('home.bl_an2')</h1>
@if(session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
                
            @endif
@csrf
<?php

$counter = 0;
?>
@if(count($banners)>0)
@foreach($banners as $banner)
	<div class="card">


		<div style="padding-top: 20px;padding-right: 20px;
  padding-bottom: 20px;
  padding-left: 20px;">
		<!--<h3><a href="/intranet/public/index/anuncios/$anuncio->id}}" style="text-decoration: none;">$anuncio->titulo}}</a></h3>-->
        <div id="txt4">
                <?php $counter++;
                echo $counter ?>
        <!--<h3>$banner->id}}</h3>-->
        
</div>
<h4> Imagen: </h4>
<div><img src="/intranet/storage/app/public/images/{{$banner->foto}}" style="width:90%;"></div>
      
        <h4> Link: <a href="{{$banner->link}}">{{$banner->link}}</a></h4>

          <a href="/intranet/public/index/banners/{{$banner->id}}/edit" class="btn btn-primary"><i class="fas fa-pen"></i> @lang('home.bl_edit4')</a>
        <button data-catid="{{$banner->id}}" data-link="{{$banner->link}}" data-toggle="modal" data-target="#delete" class="btn btn-danger">
                <i class="fas fa-trash"></i> @lang('home.bl_borrar')
                </button>
                <br>
		<small> @lang('home.bl_fcreacion'): {{$banner->created_at}}</small>
        <small>| @lang('home.bl_factualizacion'): {{$banner->updated_at}}</small>




        
            </div>


     
        
    </div>
    

    
	<br>
@endforeach
	{{$banners->links()}}
@else
<p>@lang('home.bl_an3')</p>
@endif
  </div>
</div>




















<div class="modal modal-danger fade" tabindex="-1" id="delete" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <a href="#" class="navbar-brand"><img src="{{url('/images/CCURRlogo.png')}}" style="width:65px;"></a>
                          
                        <h4 class="modal-title lang" style="margin:0 auto;" key="news">@lang('home.bl_deleteconf')</h4>
                      
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                
                            
                </div>
                <!--{!!Form::open(['route'=>['destroybanner','test'],'method'=>'POST','class'=>'float-right'])!!}
                
                {{Form::hidden('_method','DELETE')}}
                @csrf-->
                <form id="sfdel" class="sfdel2" action="{{route('destroybanner','test')}}" method="post">
                {{method_field('delete')}}
                {{csrf_field()}}
                <div class="modal-body">
                        <p class="text-center">
                        @lang('home.bl_modalq') <label id="blink"></label>
                        </p>
                        <input type="hidden" name="id" id="cat_id" value="">
                </div>		
                <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">@lang('home.bl_modalcancel')</button>
                <button type="submit" class="btn btn-danger" id="submitdel">@lang('home.bl_modaldelete')</button>
                </div>
                <!--{!!Form::close()!!}-->
                </form>
                </div>
                </div>
                </div>

</label>
                <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
<script>

$('#delete').on('show.bs.modal',function(event){
  var button=$(event.relatedTarget)
  var cat_id=button.data('catid')
  var modal=$(this)

 //print link title into modal

 var blink = button.data("link");
        //$(this).find(".modal-title").text(blink);
        modal.find(".modal-body #blink").text(blink);
//print link title into modal

  modal.find('.modal-body #cat_id').val(cat_id);

})







function Scrolldown() {
     //window.scroll(0,562); 
     document.getElementById("qs").scrollIntoView();
}

window.onload = Scrolldown;
</script>

<!--<script>
  $(function(){
    $("#sfdel").submit(function(e){
        e.preventDefault();

        
        


                Swal.fire({
                    type: 'success',

title:'@lang('home.cf_bresponse')',
html: '<i class="fas fa-check-circle" style="color:green"></i> @lang('home.cf_bresponse1') <i class="fas fa-check-circle"  style="color:green"></i>'+
'<br><br><div class="alert alert-success" role="alert">'+'@lang('home.cf_bresponse2'): '+'</div>',
animation: false,
customClass: {
popup: 'animated tada'
}

})
            
       
    

   
});
  });
</script>-->
@endsection

@section('content2')


@endsection