<h3>Tienes un mensaje de reclamaciones</h3>
<br>
Menor de edad: {{$si_menor_edad}}<br>
Nombre: {{$name}}<br>
Apellido paterno: {{$apellidop}}<br>
Apellido materno: {{$apellidom}}<br>
Distrito: {{$distrito}}<br>
Teléfono: {{$telefono}}<br>
Tipo de documento: {{$t_documento}}<br>
Nro. de documento: {{$n_documento}}<br>
Tipo de reclamo: {{$t_reclamo}}<br>
<div>
    Mensaje: {{$bodymessage}}
</div>

<p>sent via {{$email}}</p>