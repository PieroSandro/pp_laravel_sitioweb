<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link rel="shortcut icon" type="image/x-icon" href="{{url('/images/ESTRELLA.png')}}"/>
        <title>@yield('title')</title>
        <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet"> 
        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
        
      <!--------------MODALALERT----------------->
      <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
      <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
      
      <link rel="stylesheet" type="text/css" href="{{url('/css/sweetalert2.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('/css/sweetalert2.min.css')}}">
        <script type="text/javascript" src="{{url('/js/sweetalert2.js')}}"></script>
        <script type="text/javascript" src="{{url('/js/sweetalert2.min.js')}}"></script>
        
       

        <!-------------------AJAXXXX------------------------------------------------->



<!-------------------------------------------------------------------->
<!--------------ANIMATE ON SCROLL------------------------->

        <!------------------------------------------->
<!-----------MENUES SECUNDARIOS-------------------------------->


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
        <link rel="stylesheet" href="{{url('/css/bootnavbar.css')}}">
	<script src="{{url('/js/bootnavbar.js')}}" ></script>


      <!-------------------------------------------->



        <link rel="stylesheet" href="{{url('/css/animate.css')}}">
       <link rel="stylesheet" type="text/css" href="{{url('/css/swiper.min.css')}}">
       <link rel="stylesheet" href="{{url('/font.css')}}">
       <link rel='stylesheet' type='text/css' href="{{url('/css/stylesheet.css')}}">
        <link rel='stylesheet' href="https://fonts.googleapis.com/css?family=Coolvetica">
        <link rel='stylesheet' type='text/css' href="{{url('/css/font.css')}}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <script type='text/javascript' src="{{url('/js/jquery-2.1.3.min.js')}}"></script>
        <script type='text/javascript' src="{{url('/js/jQuery-2.1.4.min.js')}}"></script>
        <script type='text/javascript' src="{{url('/js/jquery-3.3.1.min.js')}}"></script>
        <script type='text/javascript' src="{{url('/js/jquery-1.11.3.min.js')}}"></script>
        <script defer src="{{url('/js/all.js')}}" integrity="sha384-kW+oWsYx3YpxvjtZjFXqazFpA7UP/MbiY4jvs+RWZo2+N94PFZ36T6TFkc9O3qoB" crossorigin="anonymous"></script>
        <script src="{{url('/js/arriba.js')}}"></script>
        <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5c87d103200ac0001700be5e&product=inline-share-buttons' async='async'></script>
        <script src='https://code.jquery.com/jquery-3.3.1.js'></script>
        
        <script type="text/javascript">
          var arrLang={
              'en':{
                'home':'Home',
                'about':'About Us',
                'news':'News'
              },
              'es':{
                'home':'Inicio',
                'about':'Quienes Somos',
                'news':'Novedades'
              }
          };

          $(function(){
            $('.translate').click(function(){
                var lang=$(this).attr('id');
                $('.lang').each(function(index,element){
                  $(this).text(arrLang[lang][$(this).attr('key')]);
                });
            });
          });
        </script>

<style>



#linkbottom{
margin:0;
padding:0;
}


#linkbottom li{
list-style:none;
display:inline-block;
}


#linkbottom li a{
text-decoration:none;
position:relative;
color:white;
display:block;
overflow:hidden;
transition:0.7s all;


}


.btn:before{

content:'@lang('home.ver_slider')';
align-content: center;
font-family: 'Montserrat', sans-serif;
font-size:11.1px;/*15px *//*12.4px; */
position:absolute;
top:50%;
left:50%;
transition:all 0.3s;
transform:translate(-50%,-50%);
}

.btn{
width:108px;
height:28px;
/*min-width: 90px;
width:30%;*/
background:red;
top:50%;
left:50%;
transform:translate(-50%,-50%);
overflow:hidden;
color:#fff;
}

.btn:hover:before{
top:-30px;
}

.btn:after{
font-family: "Font Awesome 5 Free";
color:white;
content: "\f35a";/*  \f35a*/
position:absolute;
top:calc(200px);
left:50%;
transform:translate(-50%,-50%);
font-size:20px;
transition:all 0.5s;
}

.btn:hover:after{
top:50%;
}
#bt1car2:before{

content:'@lang('home.ver_slider')';
align-content: center;
font-family: 'Montserrat', sans-serif;
font-size:11.1px;/*15px *//*12.4px; */
position:absolute;
top:50%;
left:50%;
transition:all 0.3s;
transform:translate(-50%,-50%);
}

#bt1car3:before{

content:'@lang('home.ver_slider')';
align-content: center;
font-family: 'Montserrat', sans-serif;
font-size:11.1px;/*15px *//*12.4px; */
position:absolute;
top:50%;
left:50%;
transition:all 0.3s;
transform:translate(-50%,-50%);
}
#bt2car:before{


content:'@lang('home.ver_slider')';
align-content: center;
font-family: 'Montserrat', sans-serif;
font-size:10px;
position:absolute;
top:50%;
left:50%;
transition:all 0.3s;
transform:translate(-50%,-50%);
}

.dropdown:hover>.dropdown-menu{
display: block;
}

.insta {
padding-right: 43px;
padding-left: 43px;
}


.change .bar1{
transform: rotate(-45deg) translate(-9px,6px);
}

.change .bar3{
transform: rotate(45deg) translate(-8px,-6px);
}

.change .bar2{
opacity:0;
}

@media screen and (max-width: 567px){

#ft1{
margin-top: 0px;
}

#ft2{
margin-top: 0px;
}

}

@media screen and (min-width: 568px) and (max-width: 879px){

#ft1{
margin-top: 50px;
}

#ft2{
margin-top: 50px;
}

}

@media screen and (min-width: 880px) and (max-width: 1199px){

#ft1{
margin-top: 120px;
}

#ft2{
margin-top: 120px;
}


}

@media screen and (min-width: 1200px){

#ft1{
margin-top: 205px;
}

#ft2{
margin-top: 205px;
}



}


@media screen and (min-width: 768px){

.fg{
margin-top: 87px;
}



}
</style>

<link href="{{url('/css/custom.css')}}" rel="stylesheet" type="text/css">
<link href="{{url('/css/slicebox.css')}}" rel="stylesheet" type="text/css">

<link href="{{url('/css/demo.css')}}" rel="stylesheet" type="text/css">
<script src="{{url('/js/modernizr.custom.46884.js')}}" ></script>
<script src="{{url('/js/jquery.slicebox.min.js')}}" ></script>
<script src="{{url('/js/jquery.slicebox.js')}}" ></script>

<!--
<link rel="stylesheet" type="text/css" href="https://notoyontoy.000webhostapp.com/demos/slice-box/css/slicebox.css" />
<link rel="stylesheet" type="text/css" href="https://notoyontoy.000webhostapp.com/demos/slice-box/css/custom.css" />
<script type="text/javascript" src="https://notoyontoy.000webhostapp.com/demos/slice-box/js/modernizr.custom.46884.js"></script> 
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript" src="https://notoyontoy.000webhostapp.com/demos/slice-box/js/jquery.slicebox.js"></script>-->
<script type="text/javascript">
   $(function() {
      var Page = (function() {
         var $navArrows = $( '#nav-arrows' ).hide(),
            $shadow = $( '#shadow' ).hide(),
            slicebox = $( '#sb-slider' ).slicebox( {
               onReady : function() {
                  $navArrows.show();
                  $shadow.show();
               },
               orientation : 'r',
               cuboidsRandom : true,
               autoplay : true
            } ),
            init = function() {
               initEvents();
            },
            initEvents = function() {
               // add navigation events
               $navArrows.children( ':first' ).on( 'click', function() {
                  slicebox.next();
                  
                  
                  
                  return false;
               } );

               

               $navArrows.children( ':last' ).on( 'click', function() {
                  slicebox.previous();
                  return false;
                  
              } );
            };
            return { init : init };
      })();
      Page.init();
   });
</script>


    </head>
    <body>

        <div class="container-fluid">
            

            


            <div class="row" id="top">

            <nav class="col-12 navbar navbar-expand-xl bg navbar fixed-top" style="background-color:white;"><!--#4682B4--fixed-top <nav class="col-12 navbar navbar-expand-md bg-dark navbar-dark fixed-top" style="background-color:yellow;">-->
  <a href="{{url('/index')}}" id="lglogo" class="navbar-brand"><img src="{{url('/images/CCURRlogo.png')}}" style="width:165px;"></a>
        <!--30 -->



        <!--<div class="order-md-1 offset-1 offset-sm-4 offset-md-4 offset-lg-4 offset-xl-4">-->
        <div class="order-xl-1 offset-3 offset-sm-5 offset-md-7 offset-lg-8 offset-xl-0">
 

            <?php
           
            $locale=App::getLocale();

            if(App::isLocale('en')){
              ?>
         
         
         <!--<div class="mz" style="padding-right:auto;">-->
         <a href="{{ route('change_lang', ['lang' => 'es']) }}"><img src="{{url('/images/spain.jpg')}}" id="esflag" style="width:50px; padding-top:1px;"></a>  
               <!--</div>-->
                <?php    
                        
          }else{?>
             
             <a href="{{ route('change_lang', ['lang' => 'en']) }}"><img src="{{url('/images/usa.jpg')}}" id="usflag" style="width:50px; padding-top:1px;"></a><!-- background-position:right;-->
            <?php
            }

          ?>
           </div>




        <div class="c3" data-toggle="collapse" data-target="#navbarNavAltMarkup" onclick="mf(this)">
    <!--<span class="navbar-toggler-icon"></span>-->
    <!--<div class="c3" onclick="mf(this)"-->
    <div class="bar1"></div>
    <div class="bar2"></div>
    <div class="bar3"></div>
          <!--</div>-->
          </div>
        <div class="navbar-collapse justify-content-xl-center collapse" id="navbarNavAltMarkup">
              <ul class="navbar-nav">
              <li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" style="font-family: 'Montserrat', sans-serif;" data-toggle="dropdown">@lang('home.home_menu')</a><!-- <p class="lang" key="home">Home</p></a>-->
                <div class="dropdown-menu">
                    
                    <a href="{{url('/somos')}}" class="dropdown-item lang" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-users"></i> @lang('home.about_menu')</a>
                    <a href="{{url('/mision')}}" id="mision_id" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><img src="{{url('/images/targetn.png')}}" id="target_id1" style="width:20px;"><img src="{{url('/images/targetb.png')}}" id="target_id2" style="width:20px;"> @lang('home.mision_menu')</a><!-- <i class="fas fa-file-alt"></i> <img src="{{url('/images/sheet1.png')}}" id="target_id" style="width:20px; color:red;">-->
                    <a href="{{url('/vision')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-eye"></i>
 @lang('home.vision_menu')</a>
                    <a href="{{url('/docentes')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-chalkboard-teacher"></i> @lang('home.docentes_menu')</a>
                
                </div></li>

            <li class="nav-item dropdown">
	        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" style="font-family: 'Montserrat', sans-serif;" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          @lang('home.testenglish_menu')</a>
<ul class="dropdown-menu" aria-labelledby="navbarDropdown">

<li class="nav-item dropdown">
<a class="dropdown-item dropdown-toggle" id="xy1" href="#" id="navbarDropdown1" style="font-family: 'Montserrat', sans-serif;" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-edit"></i>
@lang('home.examenes_menu')
</a>
	<ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
	              <li><a class="dropdown-item esc" id="xy2" href="{{url('/suficiencia')}}" style="font-family: 'Montserrat', sans-serif;"><img src="{{url('/images/sheet2n.png')}}" id="sheet2_id1" style="width:18px;"><img src="{{url('/images/sheet2b.png')}}" id="sheet2_id2" style="width:18px;"> @lang('home.exsuficiencia_menu')</a></li>
	              <li><a class="dropdown-item eec" id="xy2" href="#" style="font-family: 'Montserrat', sans-serif;"><img src="{{url('/images/sheet1n.png')}}" id="sheet1_id1" style="width:18px;"><img src="{{url('/images/sheet1b.png')}}" id="sheet1_id2" style="width:18px;"> @lang('home.exextraordinarios_menu')</a></li>
                
	              
	              
	            </ul>
	          </li>
<!--<li></li><a class="dropdown-item" href="#">Something else here</a></li>-->
<a href="{{url('/t_english')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-lightbulb"></i> @lang('home.testenglish_menu')</a>


            </ul>
            </li>




            <!----------------------------------------------------------------------->

            <li class="nav-item"><a href="{{ url('/login') }}" style="font-family: 'Montserrat', sans-serif;" class="nav-link">Intranet</a></li>
            <li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" style="font-family: 'Montserrat', sans-serif;" data-toggle="dropdown">@lang('home.publi_menu')</a>
                <div class="dropdown-menu">
                    <a href="{{url('/revistas')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-book-reader"></i> @lang('home.revistas_menu')</a>
                    <a href="{{ url('/videos') }}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-video"></i> Videos</a>
                    <a href="{{ url('/blog/home') }}" target="_blank" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fab fa-blogger"></i> Blog</a>
                </div>
            </li>
            <li class="nav-item"><a href="{{ url('/cursosonline') }}" class="nav-link" style="font-family: 'Montserrat', sans-serif;">@lang('home.regcursos_menu')</a></li>
            <li class="nav-item"><a href="{{ url('/biblioteca') }}" class="nav-link" style="font-family: 'Montserrat', sans-serif;">@lang('home.biblioteca_menu')</a></li>
            <!--<li class="nav-item"><a href="{{ url('/contactenos') }}" class="nav-link"><i class="fas fa-poll"></i> Encuestas</a></li>-->
            <li class="nav-item"><a href="{{ url('/contactenos') }}" class="nav-link" style="font-family: 'Montserrat', sans-serif;">@lang('home.contactenos_menu')</a></li>

            
            
            
            <!--
            <li class="nav-item"><a href="{{ url('/videos') }}" style="font-family: 'Montserrat', sans-serif;" class="nav-link">Videos</a></li>
            <li class="nav-item"><a href="{{url('/blog/home')}}" style="font-family: 'Montserrat', sans-serif;" class="nav-link" target="_blank">Blog</a></li>
            <li class="nav-item"><a href="{{ url('/login') }}" style="font-family: 'Montserrat', sans-serif;" class="nav-link">Intranet</a></li>
            
            <li class="nav-item"><a href="{{ url('/contactenos') }}" class="nav-link" style="font-family: 'Montserrat', sans-serif;">@lang('home.regcursos_menu')</a></li>
            <li class="nav-item"><a href="{{ url('/contactenos') }}" class="nav-link" style="font-family: 'Montserrat', sans-serif;">@lang('home.biblioteca_menu')</a></li>
            <li class="nav-item"><a href="{{ url('/contactenos') }}" class="nav-link" style="font-family: 'Montserrat', sans-serif;">@lang('home.contactenos_menu')</a></li>
            -->
            <!--<li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="fas fa-sticky-note"></i> Idioma</a>
                <div class="dropdown-menu">-->
                   <!---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->

                   












                   <!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------> 

                    
                <!--</div>
            </li>-->
              </ul>
              </div>
              </nav>

            </div>
            <!--<div class="row" id="ab">
</div>-->


           
<div class="row" id="fig">
           
           <ul id="sb-slider" class="sb-slider" style="max-height:650px; width: 100%; margin: auto;">
           
           
                     <?php
                         use App\Banner;
                         $banners=Banner::all();
                         $counter1=1;
                         foreach($banners as $banner){
                             ?>
           
           
           
           <li style="max-height:650px; width:100%;">
                                   <a href="{{$banner->link}}" target="_blank"><img class="d-block w-100" src="/intranet/storage/app/public/images/{{$banner->foto}}" alt="image_{{$counter1}}"/></a>
                       
                       
                       <div class="sb-description" style="text-align:center; width:40px; height:40px; margin:0 auto;">
                                       <h3 style="text-align:center; font-family: 'Montserrat', sans-serif;">{{$counter1}}</h3>
                       </div>
                         
                     </li>
           
           
           
           <?php
           $counter1++;
                         }
           
           
           
           
           
           
                       ?>
           
           
           
           
           
           
                     <div id="nav-arrows" class="nav-arrows">
                               <a href="#">Next</a>
                               <a href="#">Previous</a>
                           </div>
                   </ul>
                   
           
           
           
           
           
           
                  
           
                           
           
                       </div>

                       <div class="row">
                
                <div class="col-12 col-md-6 col-lg-8 borde3">
                    
                    
                    @yield('content')
                    
                    
                </div>
                
                
                <div class="col-12 col-md-6 col-lg-4 borde4">
                
                <?php
                $counterev = 0;
        
        
                   ?> 
                    <div class="fg" align="center">
                    <div id="sidebar">
                        <ul>
                        <?php 
                         
                          use App\Event;
                              $events=Event::all();
                              foreach($events as $event){
                                
                $bodyevent=$event->body;
                $bodyevent1 = html_entity_decode($bodyevent);
                                /*$counterev++;*/
                                  ?>
                                <li style="background:url(/intranet/storage/app/public/images/{{$event->foto}}); 	background-size:cover; background-position:left;">
                                <div class="content">
                                    <h2 style="font-family: 'Montserrat', sans-serif;">{{$event->titulo}}</h2>
                                    <p style="font-family: 'Montserrat', sans-serif;"><?php echo $bodyevent1; ?></p>
                                    <a class="btn animated shake" style="margin-bottom:10px;" href="{{$event->link}}" id="bt2car" target="_blank" role="button"></a>
                                </div>
                    </li>
                    <style type="text/css">
                            




                            #sidebar ul
                            {
                                
                                top: 0;
                                left: 0;
                                width: 100%;
                                height:100%;/* 100vh*/
                                
                                margin:0;
                                padding:0;
                                
                            }
                            
                            #sidebar ul li
                            {
                                list-style: none;
                                position: relative;
                                width:33.3%;
                                height:100%;
                                border-right: 1px solid #000;
                                float: left;
                                box-sizing: border-box;
                                transition: 0.5s;
                                text-align: center;
                                overflow:hidden;
                            }
                            
                            
                            #sidebar ul:hover li
                            {
                                
                                width:5%;
                            }
                            
                            #sidebar ul li:hover
                            {
                                
                                width:90%;
                            }
                            
                            #sidebar ul li .content
                            {
                                
                                position:absolute;
                                bottom: -100%;
                                left: 0;
                                width: 100%;
                                background: rgba(0,0,0,.7);
                                box-sizing:border-box;
                                color: #fff;
                                opacity: 0;
                            }
                            
                            #sidebar ul li:hover .content
                            {
                                
                                bottom: 0;
                                transition: 0.5s;
                                transition-delay:0.5s;
                                opacity: 1;
                            }
                                                    </style>
                        <?php 
                        
                      }
                ?>
               
                </ul>
                </div><!--<br>-->
            
            
            <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.2';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

jQuery(document).ready(function($) {
  $(window).bind("load resize", function(){  
  setTimeout(function() {
  var container_width = $('#container').width();    
    $('#container').html('<div class="fb-page" ' + 
    'data-href="https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/"' +
    ' data-width="' + container_width + '" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="http://www.facebook.com/IniciativaAutoMat"><a href="http://www.facebook.com/IniciativaAutoMat">Auto*Mat</a></blockquote></div></div>');
    FB.XFBML.parse( );    
  }, 100);  
}); 
});
</script>







<div id="container" style="width:100%; padding-left:">  
       
        <div class="fb-page" data-href="https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/" 
        data-tabs="timeline" data-height="460" data-small-header="false" data-adapt-container-width="true" 
        data-hide-cover="false" data-show-facepile="true">
        <blockquote cite="https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/" 
        class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/"  
        style="font-family: 'Montserrat', sans-serif;">Crosscurricular Module English</a></blockquote></div>

        </div>


<br>

<style type="text/css">
           #content1, #content2 {
          width: 100%;
          height: 100%;
          margin: 0;
          padding: 0;
      }
     blockquote{
          position: relative;

          /* */
          width: 100%;
          
          margin: 0;
          padding: 0;
      }

      
        </style>
<!----------INSTAGRAM--------------->
<div id="content1">
        <div id="content2">
<blockquote class="instagram-media" data-instgrm-permalink="https://www.instagram.com/p/Bu6ziucH5Ev/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="12" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:500px; min-width:316px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/Bu6ziucH5Ev/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> Ver esta publicación en Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/Bu6ziucH5Ev/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">Una publicación compartida de Juventud Globalizada (@juventudglobalizada)</a> el <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2019-03-12T18:46:43+00:00">12 Mar, 2019 a las 11:46 PDT</time></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>
</div>
</div>
<!----------INSTAGRAM--------------->












</div><!---->
         
          <br>
        </div>
        
    </div>
<!----------------------------------------------------------->
<!----------------------------------------------------------->
        <div class="row" id="patroc">
          <div class="col-12">
            <div class="cont" align="center">
              
            
              <div class="mx-auto d-block">
              <?php
              use App\Patro;
              //$patros=Patro::orderBy('id','ASC')->where('id',1)->paginate();
              //$patros=Patro::all();
              $patros=Patro::all()->take(4);


              //$counter1=1;
              foreach($patros as $patro){
                  ?>
<!--------------------------------->
        <div class="box">
        <div class="imgBox">
              <img src="/intranet/storage/app/public/images/{{$patro->foto}}">
          </div>
          <div class="details">
              <div class="cont2">
     <!--         <div id="txt4" style="text-align: center; font-family: Montserrat, sans-serif; width:100%;">-->
                            <h2 style="text-align:center; font-family: 'Montserrat', sans-serif;">{!!$patro->titulo!!}</h2>
                            <div style="text-align:center; font-family: 'Montserrat', sans-serif;">

                            <p style="text-align:center; font-family: 'Montserrat', sans-serif;">{!!$patro->body!!}</p>
                          </div>
<!--</div>-->
</div>        
          </div>
        </div>
<!------------------------------------->
<?php
//$counter1++;
              }

            ?>
               </div> 
              
              
            </div>
            </div>
        </div> 
        <!----------------------------------------------------------------------->
        <!----------------------------------------------------------------------->
        <div class="row" id="bottom">

       
          <div class="col-12 col-sm-6 col-md-2 col-lg-4 order-1 order-sm-1 bordeo">
          <br>
            <p class="text-center" style="color:white; font-family: 'Montserrat', sans-serif;">
              <i class="fas fa-smile"></i><strong> @lang('home.conocenos_menu')</strong></p>
            <p class="text-center" id="tx"><a class="i2" href="{{url('/index')}}"><img id="ongvjg" src="{{url('/images/iconofinal1.png')}}" style="width:100px;"></a></p>
            <p class="text-center" id="tz" style="color:white; font-family: 'Roboto', sans-serif;"
            >
            <strong>Juventud</strong>Globalizada</p>
          <br>
          <ul id="linkbottom">
            <ul>
              
              <center>
              <li><a href="{{ url('/reclamaciones') }}" class="ulink" style="color:white; font-family: 'Montserrat', sans-serif;">@lang('home.reclamaciones_menu')</a></li>
              <br>
              
            </center>
            </ul>
            </ul>

          <br>

          </div>
          <div class="col-12 col-sm-12 col-md-8 col-lg-4 order-md-1 order-lg-1 bordep">
            
          <div id="fi" align="center">
            <br>
            
          <p><iframe id="map1" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4027.294599494253!2d-77.05698433243407!3d-11.963111892243681!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105cfd2b037c99f%3A0xcbc9d5e8896808e5!2sI.S.T.P.+Carlos+Cueto+Fernandini!5e0!3m2!1ses-419!2sus!4v1547516063940" width="85%" height="300px" frameborder="0" style="border:0" allowfullscreen></iframe>

        <!--  width="400" height="300"-->
</p>  







            </div>

            <!-- -->
              
<!--<div id="ft" align="center">
            <br>
            
          <p><iframe id="twitter" src="https://learnenglish.britishcouncil.org/basic-grammar" width="85%" height="300px" frameborder="0" style="border:0" allowfullscreen></iframe>

    
        </p>  







</div>-->

           <!--   -->
           
          </div>
          <div class="col-12 col-sm-6 col-md-2 col-lg-4 order-2 order-sm-2 order-md-2 order-lg-2 bordeq">
            <br>
            <p class="text-center" style="color:white; font-family: 'Montserrat', sans-serif;"><i class="fas fa-phone"></i><strong> @lang('home.contactanos_menu')</strong></p>
            <p class="text-center" style="color:white; font-family: 'Montserrat', sans-serif;">947326744</p>
          <br>
          <div id="fh">
            
              <a data-aos="fade-up" class="btn2" style="margin:2px;" href="">
                    <i class="icon icon-facebook2"></i>
                </a>
              
              <a data-aos="fade-up" class="btn2" style="margin:2px;" href="">
                    <i class="icon icon-youtube"></i>
                </a>
              
              <a data-aos="fade-up" class="btn2" style="margin:2px;" href="">
                    <i class="icon icon-twitter"></i>
                </a>
                
              </div>     
          <br>
          </div>
        </div>
        <div class="row">
        <div class="col-12 col-md-6 col-lg-6 bordea">
            <div class="container text-center text-md-left text-lg-left">
            <div class="tc" style="color:white; font-family: 'Montserrat', sans-serif;">&copy 2019 ONG VJG</div>
            </div>
        
        </div>
        
        <div class="col-12 col-md-6 col-lg-6 bordeb">
            <div class="container text-center text-md-right text-lg-right">
            <div class="tc" style="color:white; font-family: 'Montserrat', sans-serif;">Desarrollado por PR</div>
            </div>
        </div>
        
        </div>
  

        </div>

        <div>
        
       <!---------------------------------->

             <!---------------------------POPOVER----------------------------------------->

             <!--<script type='text/javascript' src="{{url('/js/bootstrap.min.js')}}"></script>-->







 <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>














  
<a class="popi" data-by="@lang('home.rs_right')" data-toggle="popover" style="cursor: pointer; color: rgba(11, 11, 132, 0.993);" onclick="cr1()" id="indi1"><i class="fas fa-caret-right"></i></a>





<script>

$('.popi').popover({
 delay: 200,/*1150 */
       //trigger: 'focus',
   trigger: 'hover',
       html: true,
       content: function () {

         //if($(this).data('img')=='/intranet/storage/app/public/images/noimage.jpg')
         //{
          //$(this).attr("style","font-family: Montserrat;");
         return $(this).data("by");
        // }else{
           
   //	return $(this).data("by")+'<br><br><img class="img-fluid" src="'+$(this).data('img') + '" />';
        // }
       },
       
 });   

</script>









       <!------------------------>
        <i class="fas fa-caret-left" style="cursor: pointer; color: rgba(11, 11, 132, 0.993);" onclick="cr2()" id="indi2"></i>
        </div>
        <!-- .........................................................-->
        
        <!-- .........................................................-->
        <div class="red">
            <ul>
        
            <li><a href=""  title="Facebook" class="icon-facebook2"></a>
                
            </li>
            <li><a href=""  title="Youtube" class="icon-youtube"></a></li>
            <li><a href=""  title="Twitter" class="icon-twitter"></a></li>
            <!--<a class="link" href="" id="btred"><i class="fas fa-caret-right"></i></a>-->
            </ul>

        </div>
        <!-- href="#top"-->
   
   
        <a href="#" class="to-top" title="@lang('home.boton_subir')"><div style="text-align:center;"><i class="fas fa-angle-up nbu" style="float:center; padding-bottom:8px;"></i></div></a>
        <!--<script type="text/javascript" src="{{url('/js/popper.min.js')}}"></script>-->
        <script type="text/javascript" src="{{url('/js/popper-1.12.9.min.js')}}"></script>
        <script type="text/javascript" src="{{url('/js/swiper.min.js')}}"></script>
        <script>
                var swiper = new Swiper('.swiper-container', {
                  effect: 'coverflow',
                  grabCursor: true,
                  centeredSlides: true,
                  slidesPerView: 'auto',
                  coverflowEffect: {
                    rotate: 50,//60
                    stretch: 0,
                    depth: 100,//500
                    modifier: 1,//5
                    slideShadows : true,
                  },
                  pagination: {
                    el: '.swiper-pagination',
                  },
                });
              </script>
              
        <script>
          $(function(){
            $("#target_id2").hide();

            $("#mision_id").mouseover(function(){
              $("#target_id1").hide();
              $("#target_id2").show();
                });

            $("#mision_id").mouseout(function(){
              $("#target_id2").hide();
              $("#target_id1").show();
                });
            
            
            $("#sheet1_id2").hide();
            
            $(".eec").mouseover(function(){
              $("#sheet1_id1").hide();
              $("#sheet1_id2").show();
                });

            $(".eec").mouseout(function(){
              $("#sheet1_id2").hide();
              $("#sheet1_id1").show();
                });

                $("#sheet2_id2").hide();
            
            $(".esc").mouseover(function(){
              $("#sheet2_id1").hide();
              $("#sheet2_id2").show();
                });

            $(".esc").mouseout(function(){
              $("#sheet2_id2").hide();
              $("#sheet2_id1").show();
                });

               


          });
          //<div class="fb-page" data-href="https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/" data-tabs="timeline" data-width="430" data-height="460" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/" style="font-family: 'Montserrat', sans-serif;">Crosscurricular Module English</a></blockquote></div>
        </script>
        <!--<script>
            $(function(){
              
              $(window).resize(function(){
                  if ($(window).width() < 900) {
                document.getElementById("rarata").innerHTML="<div class='fb-page' data-href='https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/' data-tabs='timeline' data-width='400' data-height='460' data-small-header='false' data-adapt-container-width='true' data-hide-cover='false' data-show-facepile='true'><blockquote cite='https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/' class='fb-xfbml-parse-ignore'><a href='https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/' style='font-family: 'Montserrat', sans-serif;'>Crosscurricular Module English</a></blockquote></div>";
                //$("#ratata").html("julio");
                
                }else{
                  document.getElementById("rarata").innerHTML="<div class='fb-page' data-href='https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/' data-tabs='timeline' data-width='400' data-height='150' data-small-header='false' data-adapt-container-width='true' data-hide-cover='false' data-show-facepile='true'><blockquote cite='https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/' class='fb-xfbml-parse-ignore'><a href='https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/' style='font-family: 'Montserrat', sans-serif;'>Crosscurricular Module English</a></blockquote></div>";
                  //$("#ratata").html("agosto");
                
                }
               });

               $(window).show(function(){
                  if ($(window).width() < 900) {
                document.getElementById("rarata").innerHTML="<div class='fb-page' data-href='https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/' data-tabs='timeline' data-width='400' data-height='460' data-small-header='false' data-adapt-container-width='true' data-hide-cover='false' data-show-facepile='true'><blockquote cite='https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/' class='fb-xfbml-parse-ignore'><a href='https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/' style='font-family: 'Montserrat', sans-serif;'>Crosscurricular Module English</a></blockquote></div>";
                //$("#ratata").html("julio");
                }else{
                  document.getElementById("rarata").innerHTML="<div class='fb-page' data-href='https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/' data-tabs='timeline' data-width='400' data-height='150' data-small-header='false' data-adapt-container-width='true' data-hide-cover='false' data-show-facepile='true'><blockquote cite='https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/' class='fb-xfbml-parse-ignore'><a href='https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/' style='font-family: 'Montserrat', sans-serif;'>Crosscurricular Module English</a></blockquote></div>";
                  //$("#ratata").html("agosto");
                }
               });

            }); 
        </script>-->




        <!--<script>
            $(document).ready(function() {
    function checkWidth() {
      
      //$(window).width()=">";
        var windowSize = $(window).width();

        if (windowSize <= 900) {
          document.getElementById("rarata").innerHTML="<div class='fb-page' data-href='https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/' data-tabs='timeline' data-width='400' data-height='460' data-small-header='false' data-adapt-container-width='true' data-hide-cover='false' data-show-facepile='true'><blockquote cite='https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/' class='fb-xfbml-parse-ignore'><a href='https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/' style='font-family: 'Montserrat', sans-serif;'>Crosscurricular Module English</a></blockquote></div>";
               
        }
        else{
          document.getElementById("rarata").innerHTML="<div class='fb-page' data-href='https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/' data-tabs='timeline' data-width='400' data-height='150' data-small-header='false' data-adapt-container-width='true' data-hide-cover='false' data-show-facepile='true'><blockquote cite='https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/' class='fb-xfbml-parse-ignore'><a href='https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/' style='font-family: 'Montserrat', sans-serif;'>Crosscurricular Module English</a></blockquote></div>";
               
        }
    }

    // Execute on load
    checkWidth();
    // Bind event listener
    $(window).resize(checkWidth);
    $(window).show(checkWidth);
            });

        </script>-->
        <script>
$("#indi2").hide();
$('.red').hide();

//if(document.getElementById("ccd1").style.visibility = "hidden"){   
function cr1() {
$("#indi2").show();
$("#indi1").hide();
$('.red').animate({width: 'toggle'}, "slow");
}

function cr2() {
$("#indi1").show();
$("#indi2").hide();
$('.red').animate({width: 'toggle'}, "slow");
}


</script>


        <script>
            function mf(X){
                X.classList.toggle("change");
            }
        </script>





        <script>
function myFunction(x) {
  if (x.matches) { // If media query matches
    $(".c3").show();
  } else {
    $(".c3").hide();
  }
}

var x = window.matchMedia("(max-width: 1199px)")
myFunction(x) // Call listener function at run time
x.addListener(myFunction) // Attach listener function on state changes
</script>
    </body>

</html>
