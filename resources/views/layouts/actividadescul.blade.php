<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link rel="shortcut icon" type="image/x-icon" href="{{url('/images/ESTRELLA.png')}}"/>
        <title >Blog - @yield('titleblog')</title>


       
        <link rel='stylesheet' type='text/css' href="{{url('/css/font.css')}}">
        
      
     
       

        <script src='https://www.google.com/recaptcha/api.js'></script>


<!-- 29_01_2020-->
<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet"> 
        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
        
      <!--------------MODALALERT----------------->
      <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
      <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
      
      <link rel="stylesheet" type="text/css" href="{{url('/css/sweetalert2.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('/css/sweetalert2.min.css')}}">
        <script type="text/javascript" src="{{url('/js/sweetalert2.js')}}"></script>
        <script type="text/javascript" src="{{url('/js/sweetalert2.min.js')}}"></script>
        
       

        <!-------------------AJAXXXX------------------------------------------------->



<!-------------------------------------------------------------------->
<!--------------ANIMATE ON SCROLL------------------------->

        <!------------------------------------------->
<!-----------MENUES SECUNDARIOS-------------------------------->


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
        <link rel="stylesheet" href="{{url('/css/bootnavbar.css')}}">
	<script src="{{url('/js/bootnavbar.js')}}" ></script>


      <!-------------------------------------------->



        <link rel="stylesheet" href="{{url('/css/animate.css')}}">
       <link rel="stylesheet" type="text/css" href="{{url('/css/swiper.min.css')}}">
       <link rel="stylesheet" href="{{url('/font.css')}}">
       <link rel='stylesheet' type='text/css' href="{{url('/css/stylesheet.css')}}">
        <link rel='stylesheet' href="https://fonts.googleapis.com/css?family=Coolvetica">
        <link rel='stylesheet' type='text/css' href="{{url('/css/font.css')}}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <script type='text/javascript' src="{{url('/js/jquery-2.1.3.min.js')}}"></script>
        <script type='text/javascript' src="{{url('/js/jQuery-2.1.4.min.js')}}"></script>
        <script type='text/javascript' src="{{url('/js/jquery-3.3.1.min.js')}}"></script>
        <script type='text/javascript' src="{{url('/js/jquery-1.11.3.min.js')}}"></script>
        <script defer src="{{url('/js/all.js')}}" integrity="sha384-kW+oWsYx3YpxvjtZjFXqazFpA7UP/MbiY4jvs+RWZo2+N94PFZ36T6TFkc9O3qoB" crossorigin="anonymous"></script>
        <script src="{{url('/js/arriba.js')}}"></script>
        <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5c87d103200ac0001700be5e&product=inline-share-buttons' async='async'></script>
        <script src='https://code.jquery.com/jquery-3.3.1.js'></script>
        
        <script type="text/javascript">
          var arrLang={
              'en':{
                'home':'Home',
                'about':'About Us',
                'news':'News'
              },
              'es':{
                'home':'Inicio',
                'about':'Quienes Somos',
                'news':'Novedades'
              }
          };

          $(function(){
            $('.translate').click(function(){
                var lang=$(this).attr('id');
                $('.lang').each(function(index,element){
                  $(this).text(arrLang[lang][$(this).attr('key')]);
                });
            });
          });
        </script>
        <style>
          #linkbottom{
  margin:0;
  padding:0;
}


#linkbottom li{
  list-style:none;
  display:inline-block;
}


#linkbottom li a{
  text-decoration:none;
  position:relative;
  color:white;
  display:block;
  overflow:hidden;
  transition:0.7s all;


}
.thecard{
	
	width:250px;
	height:320px;
	transform-style:preserve-3d;
	transition:all 0.5s ease;
	
}

.thecard:hover{
	transform:rotateY(180deg);
}

.tfront{
	position:absolute;
	width:250px;
	height:320px;
	backface-visibility:hidden;
	background: url('images/cardf1.jpg')no-repeat;
	color:#333;
	text-align:center;
	border-radius:20px;
}

#text1{
	padding-top: 50%;
	align-items:center;


}



.tback{
	position:absolute;
	width:250px;
	height:320px;
	backface-visibility:hidden;
	
	background:url('images/cardf2.jpg')no-repeat;
	color:#333;
	text-align:center;
	border-radius:20px;
	transform:rotateY(180deg);
}

#bt1car:before{
	
	content:'@lang('home.ver_slider')';
  align-content: center;
  font-family: 'Montserrat', sans-serif;
	font-size:11.1px;
	position:absolute;
	top:50%;
	left:50%;
	transition:all 0.3s;
	transform:translate(-50%,-50%);
}
/*15px */ /*12.4px; */
#bt1car2:before{
	
	content:'@lang('home.ver_slider')';
  align-content: center;
  font-family: 'Montserrat', sans-serif;
	font-size:11.1px;/*15px *//*12.4px; */
	position:absolute;
	top:50%;
	left:50%;
	transition:all 0.3s;
	transform:translate(-50%,-50%);
}

#bt1car3:before{
	
	content:'@lang('home.ver_slider')';
  align-content: center;
  font-family: 'Montserrat', sans-serif;
	font-size:11.1px;/*15px *//*12.4px; */
	position:absolute;
	top:50%;
	left:50%;
	transition:all 0.3s;
	transform:translate(-50%,-50%);
}
#bt2car:before{
	
	
	content:'@lang('home.ver_slider')';
  align-content: center;
  font-family: 'Montserrat', sans-serif;
	font-size:10px;
	position:absolute;
	top:50%;
	left:50%;
	transition:all 0.3s;
  transform:translate(-50%,-50%);
  
}

.dropdown:hover>.dropdown-menu{
  display: block;
}


.insta {
  padding-right: 43px;
  padding-left: 43px;
}


.change .bar1{
transform: rotate(-45deg) translate(-9px,6px);
}

.change .bar3{
transform: rotate(45deg) translate(-8px,-6px);
}

.change .bar2{
opacity:0;
}

@media screen and (max-width: 567px){

#ft1{
margin-top: 0px;
}

#ft2{
margin-top: 0px;
}

}

@media screen and (min-width: 568px) and (max-width: 879px){

#ft1{
margin-top: 50px;
}

#ft2{
margin-top: 50px;
}

}

@media screen and (min-width: 880px) and (max-width: 1199px){

#ft1{
margin-top: 120px;
}

#ft2{
margin-top: 120px;
}


}

@media screen and (min-width: 1200px){

#ft1{
margin-top: 205px;
}

#ft2{
margin-top: 205px;
}



}


@media screen and (min-width: 768px){

.fg{
margin-top: 87px;
}



}
        </style>


<link href="{{url('/css/custom.css')}}" rel="stylesheet" type="text/css">
<link href="{{url('/css/slicebox.css')}}" rel="stylesheet" type="text/css">

<link href="{{url('/css/demo.css')}}" rel="stylesheet" type="text/css">
<script src="{{url('/js/modernizr.custom.46884.js')}}" ></script>
<script src="{{url('/js/jquery.slicebox.min.js')}}" ></script>
<script src="{{url('/js/jquery.slicebox.js')}}" ></script>

<!--
<link rel="stylesheet" type="text/css" href="https://notoyontoy.000webhostapp.com/demos/slice-box/css/slicebox.css" />
<link rel="stylesheet" type="text/css" href="https://notoyontoy.000webhostapp.com/demos/slice-box/css/custom.css" />
<script type="text/javascript" src="https://notoyontoy.000webhostapp.com/demos/slice-box/js/modernizr.custom.46884.js"></script> 
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript" src="https://notoyontoy.000webhostapp.com/demos/slice-box/js/jquery.slicebox.js"></script>-->
<script type="text/javascript">
   $(function() {
      var Page = (function() {
         var $navArrows = $( '#nav-arrows' ).hide(),
            $shadow = $( '#shadow' ).hide(),
            slicebox = $( '#sb-slider' ).slicebox( {
               onReady : function() {
                  $navArrows.show();
                  $shadow.show();
               },
               orientation : 'r',
               cuboidsRandom : true,
               autoplay : true
            } ),
            init = function() {
               initEvents();
            },
            initEvents = function() {
               // add navigation events
               $navArrows.children( ':first' ).on( 'click', function() {
                  slicebox.next();
                  
                  
                  
                  return false;
               } );

               

               $navArrows.children( ':last' ).on( 'click', function() {
                  slicebox.previous();
                  return false;
                  
              } );
            };
            return { init : init };
      })();
      Page.init();
   });
</script>



        <!---------------------------------------------------->
        <!----------------------------------------------------->
          <!---------------------------------------------------->
        <!----------------------------------------------------->
        
    </head>
    <body>

        <div class="container-fluid">
            

            <div class="row" id="top">

            <nav class="col-12 navbar navbar-expand-xl bg navbar fixed-top" style="background-color:white;"><!-- id="navbar"|fixed-top -->
  <a href="{{url('/blog/home')}}" id="lglogo" class="navbar-brand"><img src="{{url('/images/CCURRlogo.png')}}" style="width:165px;"></a><!--padding-top:20px; width:150px-->
<!------------------------------------------->




    <div class="order-xl-1 offset-1 offset-sm-4 offset-md-4 offset-lg-5 offset-xl-0">

 

            <?php
           
            $locale=App::getLocale();

            if(App::isLocale('en')){
              ?>
         
         
         <!--<div class="mz" style="padding-right:auto;">-->
         <a href="{{ route('change_lang', ['lang' => 'es']) }}"><img src="{{url('/images/spain.jpg')}}" id="esflag" style="width:50px; padding-top:1px;"></a>  
               <!--</div>-->
                <?php    
                        
          }else{?>
             
             <a href="{{ route('change_lang', ['lang' => 'en']) }}"><img src="{{url('/images/usa.jpg')}}" id="usflag" style="width:50px; padding-top:1px;"></a><!-- background-position:right;-->
            <?php
            }

          ?>
           </div>






    
            <!------------------------------------>
            <div class="c3" data-toggle="collapse" data-target="#navbarNavAltMarkup" onclick="mf(this)">
    <!--<span class="navbar-toggler-icon"></span>-->
    <!--<div class="c3" onclick="mf(this)"-->
    <div class="bar1"></div>
    <div class="bar2"></div>
    <div class="bar3"></div>
          <!--</div>-->
          </div>
        <div class="navbar-collapse justify-content-xl-center collapse" id="navbarNavAltMarkup"><!--<div class="navbar-collapse justify-content-md-center collapse" id="navbarNavAltMarkup"> -->
              <ul class="navbar-nav">
              <li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" style="font-family: 'Montserrat', sans-serif;" data-toggle="dropdown">@lang('home.bl_carrerast')</a>
                <div class="dropdown-menu">
                    
                    <a href="{{url('/blog/administracion')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-tasks"></i> @lang('home.bl_admin')</a>
                    <a href="{{url('/blog/computacion')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-laptop"></i> @lang('home.bl_compu')</a>
                    <a href="{{url('/blog/contabilidad')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-calculator"></i> @lang('home.bl_conta')</a>
                    <a href="{{url('/blog/electronica')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-microchip"></i> @lang('home.bl_elec1')</a>
                    <a href="{{url('/blog/electrotecnia')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="far fa-lightbulb"></i> @lang('home.bl_elec2')</a>
                    <a href="{{url('/blog/laboratorio')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-notes-medical"></i> @lang('home.bl_lacli')</a>
                    <a href="{{url('/blog/mautomotriz')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-wrench"></i> @lang('home.bl_mecau')</a>
                    <a href="{{url('/blog/mproduccion')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-cogs"></i> @lang('home.bl_mepro')</a>
                    <a href="{{url('/blog/metalurgia')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-gem"></i> @lang('home.bl_metal')</a>
                </div></li>  
              <li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" style="font-family: 'Montserrat', sans-serif;" data-toggle="dropdown">@lang('home.bl_acultural')</a>
                <div class="dropdown-menu">
                    
                    <a href="{{url('/blog/danza')}}" id="danza_id" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><img src="{{url('/images/dancen.png')}}" id="dance_id1" style="width:13px;"><img src="{{url('/images/danceb.png')}}" id="dance_id2" style="width:13px;"> @lang('home.bl_danza')</a>
                    <a href="{{url('/blog/musica')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-music"></i> @lang('home.bl_musica')</a>
                    <a href="{{url('/blog/teatro')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-theater-masks"></i> @lang('home.bl_teatro')</a>
                    
                </div></li>  
            <li class="nav-item"><a href="{{ url('/blog/v_estudiantil') }}" class="nav-link" style="font-family: 'Montserrat', sans-serif;">@lang('home.bl_vestudiantil')</a>
            </li>
            <li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" style="font-family: 'Montserrat', sans-serif;">Podcasts</a>
              <div class="dropdown-menu">
                    
                    <a href="{{url('/blog/podcast_basico')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-one"></i> @lang('home.bl_basico')</a>
                    <a href="{{url('/blog/podcast_intermedio')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-two"></i> @lang('home.bl_intermedio')</a>
                    <a href="{{url('/blog/podcast_avanzado')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-three"></i> @lang('home.bl_avanzado')</a>
                    
                </div>
            </li>
            <li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" style="font-family: 'Montserrat', sans-serif;" data-toggle="dropdown">@lang('home.bl_ginglesa')</a>
                <div class="dropdown-menu">
                    
                    <a href="{{url('/blog/basic_grammar')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-one"></i> @lang('home.bl_basico')</a>
                    <a href="{{url('/blog/intermediate_grammar')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-two"></i> @lang('home.bl_intermedio')</a>
                    <a href="{{url('/blog/advanced_grammar')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-three"></i> @lang('home.bl_avanzado')</a>
                    
                </div></li>
            <!-------------------------------XXXXXXXXXXXX-------------------------------------------->
            <!---------------------------------------------------------------------------->

            <!--<li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">@lang('home.bl_ginglesa')</a>
                <div class="dropdown-menu">
                    
                    <a href="{{url('/blog/basic_grammar')}}" class="dropdown-item"><i class="fas fa-dice-one"></i> @lang('home.bl_basico')</a>
                    <a href="{{url('/blog/intermediate_grammar')}}" class="dropdown-item"><i class="fas fa-dice-two"></i> @lang('home.bl_intermedio')</a>
                    <li class="nav-item dropdown"><a href="{{url('/blog/advanced_grammar')}}" class="dropdown-item"><i class="fas fa-dice-three"></i> @lang('home.bl_avanzado')</a>

                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
	              <li><a class="dropdown-item xy2" href="#"><i class="fas fa-dice-one"></i> @lang('home.bl_basico')</a></li>
	              <li><a class="dropdown-item xy2" href="#"><i class="fas fa-dice-two"></i> @lang('home.bl_intermedio')</a></li>
                <li><a class="dropdown-item xy2" href="#"><i class="fas fa-dice-three"></i> @lang('home.bl_avanzado')</a></li>
	              
	            </ul></li>
                </div></li>-->


            <!---------------------------------------------------------------------------->
            <!----------------------------XXXXXXXXXXXXXXXX------------------------------------------------->

            <li class="nav-item dropdown">
	        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" style="font-family: 'Montserrat', sans-serif;" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          @lang('home.bl_itecnico')</a>
<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
<!--<div class="dropdown-divider"></div>-->
<li class="nav-item dropdown">
<a class="dropdown-item dropdown-toggle" id="xy1" href="#" id="navbarDropdown1" style="font-family: 'Montserrat', sans-serif;" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-tasks"></i>
@lang('home.bl_admin')
</a>
	<ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
	              <li><a class="dropdown-item" id="xy2" href="{{url('/blog/administracion_basico')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-one"></i> @lang('home.bl_basico')</a></li>
	              <li><a class="dropdown-item" id="xy2" href="{{url('/blog/administracion_intermedio')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-two"></i> @lang('home.bl_intermedio')</a></li>
                <li><a class="dropdown-item" id="xy2" href="{{url('/blog/administracion_avanzado')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-three"></i> @lang('home.bl_avanzado')</a></li>
	              
	              
	            </ul>
	          </li>
<!--<li></li><a class="dropdown-item" href="#">Something else here</a></li>-->
<li class="nav-item dropdown">
<a class="dropdown-item dropdown-toggle" id="xy1" href="#" style="font-family: 'Montserrat', sans-serif;" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-laptop"></i>
@lang('home.bl_compu')
</a>
	<ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
	              <li><a class="dropdown-item" id="xy2" href="{{url('/blog/computacion_basico')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-one"></i> @lang('home.bl_basico')</a></li>
	              <li><a class="dropdown-item" id="xy2" href="{{url('/blog/computacion_intermedio')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-two"></i> @lang('home.bl_intermedio')</a></li>
                <li><a class="dropdown-item" id="xy2" href="{{url('/blog/computacion_avanzado')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-three"></i> @lang('home.bl_avanzado')</a></li>
	              
	              
	            </ul>
	          </li>
<!---------------------------------------------->
<li class="nav-item dropdown">
<a class="dropdown-item dropdown-toggle" id="xy1" href="#" style="font-family: 'Montserrat', sans-serif;" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-calculator"></i>
@lang('home.bl_conta')
</a>
	<ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
	              <li><a class="dropdown-item" id="xy2" href="{{url('/blog/contabilidad_basico')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-one"></i> @lang('home.bl_basico')</a></li>
	              <li><a class="dropdown-item" id="xy2" href="{{url('/blog/contabilidad_intermedio')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-two"></i> @lang('home.bl_intermedio')</a></li>
                <li><a class="dropdown-item" id="xy2" href="{{url('/blog/contabilidad_avanzado')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-three"></i> @lang('home.bl_avanzado')</a></li>
	              
	              
	            </ul>
	          </li>
            <!------------------------------------------------>
            <li class="nav-item dropdown">
<a class="dropdown-item dropdown-toggle" id="xy1" href="#" style="font-family: 'Montserrat', sans-serif;" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-microchip"></i>
@lang('home.bl_elec1')
</a>
	<ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
	              <li><a class="dropdown-item" id="xy2" href="{{url('/blog/electronica_basico')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-one"></i> @lang('home.bl_basico')</a></li>
	              <li><a class="dropdown-item" id="xy2" href="{{url('/blog/electronica_intermedio')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-two"></i> @lang('home.bl_intermedio')</a></li>
                <li><a class="dropdown-item" id="xy2" href="{{url('/blog/electronica_avanzado')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-three"></i> @lang('home.bl_avanzado')</a></li>
	              
	            </ul>
	          </li>

<!---------------------------------------------------------------------------------->
<li class="nav-item dropdown">
<a class="dropdown-item dropdown-toggle" style="font-family: 'Montserrat', sans-serif;" id="xy1" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-lightbulb"></i>
@lang('home.bl_elec2')
</a>
	<ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
	              <li><a class="dropdown-item" id="xy2" href="{{url('/blog/electrotecnia_basico')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-one"></i> @lang('home.bl_basico')</a></li>
	              <li><a class="dropdown-item" id="xy2" href="{{url('/blog/electrotecnia_intermedio')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-two"></i> @lang('home.bl_intermedio')</a></li>
                <li><a class="dropdown-item" id="xy2" href="{{url('/blog/electrotecnia_avanzado')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-three"></i> @lang('home.bl_avanzado')</a></li>
	              
	            </ul>
	          </li> 
<!------------------------------------------------------------------>
<li class="nav-item dropdown">
<a class="dropdown-item dropdown-toggle" style="font-family: 'Montserrat', sans-serif;" id="xy1" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-notes-medical"></i>
@lang('home.bl_lacli')
</a>
	<ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
	              <li><a class="dropdown-item" id="xy2" href="{{url('/blog/laboratorio_basico')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-one"></i> @lang('home.bl_basico')</a></li>
	              <li><a class="dropdown-item" id="xy2" href="{{url('/blog/laboratorio_intermedio')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-two"></i> @lang('home.bl_intermedio')</a></li>
                <li><a class="dropdown-item" id="xy2" href="{{url('/blog/laboratorio_avanzado')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-three"></i> @lang('home.bl_avanzado')</a></li>
	              
	            </ul>
            </li> 
            
<!----------------------------------------------------------------------->
<li class="nav-item dropdown">
<a class="dropdown-item dropdown-toggle" style="font-family: 'Montserrat', sans-serif;" id="xy1" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-wrench"></i>
@lang('home.bl_mecau')
</a>
	<ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
	              <li><a class="dropdown-item" id="xy2" href="{{url('/blog/mautomotriz_basico')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-one"></i> @lang('home.bl_basico')</a></li>
	              <li><a class="dropdown-item" id="xy2" href="{{url('/blog/mautomotriz_intermedio')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-two"></i> @lang('home.bl_intermedio')</a></li>
                <li><a class="dropdown-item" id="xy2" href="{{url('/blog/mautomotriz_avanzado')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-three"></i> @lang('home.bl_avanzado')</a></li>
	              
	            </ul>
	          </li> 

<!-------------------------------------------------->

<li class="nav-item dropdown">
<a class="dropdown-item dropdown-toggle" style="font-family: 'Montserrat', sans-serif;" id="xy1" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-cogs"></i>
@lang('home.bl_mepro')
</a>
	<ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
	              <li><a class="dropdown-item" id="xy2" href="{{url('/blog/mproduccion_basico')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-one"></i> @lang('home.bl_basico')</a></li>
	              <li><a class="dropdown-item" id="xy2" href="{{url('/blog/mproduccion_intermedio')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-two"></i> @lang('home.bl_intermedio')</a></li>
                <li><a class="dropdown-item" id="xy2" href="{{url('/blog//mproduccion_avanzado')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-three"></i> @lang('home.bl_avanzado')</a></li>
	              
	            </ul>
	          </li> 

<!-------------------------------------------------------------------------------------------->

<li class="nav-item dropdown">
<a class="dropdown-item dropdown-toggle" style="font-family: 'Montserrat', sans-serif;" id="xy1" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-gem"></i>
@lang('home.bl_metal')
</a>
	<ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
	              <li><a class="dropdown-item" id="xy2" href="{{url('/blog/metalurgia_basico')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-one"></i> @lang('home.bl_basico')</a></li>
	              <li><a class="dropdown-item" id="xy2" href="{{url('/blog/metalurgia_intermedio')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-two"></i> @lang('home.bl_intermedio')</a></li>
                <li><a class="dropdown-item" id="xy2" href="{{url('/blog/metalurgia_avanzado')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-three"></i> @lang('home.bl_avanzado')</a></li>
	              
	            </ul>
	          </li> 


<!------------------------------------------------------------------------------->
	        </ul>
	      </li>

            <!---------------------------------------------------------->
            <!----1------------------------------------------------------------------------------------------------------------------------->
            
            <li class="nav-item"><a href="{{ url('/blog/preguntas_frecuentes') }}" style="font-family: 'Montserrat', sans-serif;" class="nav-link">@lang('home.bl_pfrecuentes')</a>
            </li>
            
            
            

            <!--@yield('languages')-->
            
                     
            
            
            
              
              
              



























<!-------------------------------------------->

<!--
<li class="nav-item dropdown">
	        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
Dropdown</a>
<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
<li><a class="dropdown-item" href="#">Action</a></li>
<li><a class="dropdown-item" href="#">Another action</a></li>
<div class="dropdown-divider"></div>
<li></li><a class="dropdown-item" href="#">Something else here</a></li>
<li class="nav-item dropdown">
<a class="dropdown-item dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
Dropdown
</a>
	<ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
	              <li><a class="dropdown-item" href="#">Action</a></li>
	              <li><a class="dropdown-item" href="#">Another action</a></li>
	              <div class="dropdown-divider"></div>
	              <li></li><a class="dropdown-item" href="#">Something else here</a></li>
	              <li class="nav-item dropdown">
	                <a class="dropdown-item dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown"
	                    aria-haspopup="true" aria-expanded="false">
	                  Dropdown
	                </a>
	                <ul class="dropdown-menu" aria-labelledby="navbarDropdown2">
	                  <li><a class="dropdown-item" href="#">Action</a></li>
	                  <li><a class="dropdown-item" href="#">Another action</a></li>
	                  <div class="dropdown-divider"></div>
	                  <li></li><a class="dropdown-item" href="#">Something else here</a></li>
	                </ul>
	              </li>
	            </ul>
	          </li>
	        </ul>
	      </li>-->










<!-------------------------------------------->




































              </ul>
              

              
              </div>
             
              </nav>


              
              
            </div>

            <div class="row" id="fig">
           @yield('carusel')
            </div>





            <div class="row">
                
        <div class="col-12 col-md-6 col-lg-8 borde3">
            
           @yield('content')
            
            
        </div>
        
        
        <div class="col-12 col-md-6 col-lg-4 borde4">

            
            <div class="fg" align="center">

            <br>
            <div class="jumbotron text-center">
              
            
@yield('content2')


              
              
              
              
              
              
              
              
              
              </div>  
            



            <div id="sidebar">
            <ul>
                        <?php 
                         
                          use App\Event;
                              $events=Event::all();
                              foreach($events as $event){
                                
                $bodyevent=$event->body;
                $bodyevent1 = html_entity_decode($bodyevent);
                                /*$counterev++;*/
                                  ?>
                                <li style="background:url(/intranet/storage/app/public/images/{{$event->foto}}); 	background-size:cover; background-position:left;">
                                <div class="content">
                                    <h2 style="font-family: 'Montserrat', sans-serif;">{{$event->titulo}}</h2>
                                    <p style="font-family: 'Montserrat', sans-serif;"><?php echo $bodyevent1; ?></p>
                                    <a class="btn animated shake" style="margin-bottom:10px;" href="{{$event->link}}" id="bt2car" target="_blank" role="button"></a>
                                </div>
                    </li>
                    <style type="text/css">
                            




                            #sidebar ul
                            {
                                
                                top: 0;
                                left: 0;
                                width: 100%;
                                height:100%;/* 100vh*/
                                
                                margin:0;
                                padding:0;
                                
                            }
                            
                            #sidebar ul li
                            {
                                list-style: none;
                                position: relative;
                                width:33.3%;
                                height:100%;
                                border-right: 1px solid #000;
                                float: left;
                                box-sizing: border-box;
                                transition: 0.5s;
                                text-align: center;
                                overflow:hidden;
                            }
                            
                            
                            #sidebar ul:hover li
                            {
                                
                                width:5%;
                            }
                            
                            #sidebar ul li:hover
                            {
                                
                                width:90%;
                            }
                            
                            #sidebar ul li .content
                            {
                                
                                position:absolute;
                                bottom: -100%;
                                left: 0;
                                width: 100%;
                                background: rgba(0,0,0,.7);
                                box-sizing:border-box;
                                color: #fff;
                                opacity: 0;
                            }
                            
                            #sidebar ul li:hover .content
                            {
                                
                                bottom: 0;
                                transition: 0.5s;
                                transition-delay:0.5s;
                                opacity: 1;
                            }
                                                    </style>
                        <?php 
                        
                      }
                ?>
               
                </ul>
            </div><br>
        
          <!----------INSTAGRAM--------------->

          <style type="text/css">
           #content1, #content2 {
          width: 100%;
          height: 100%;
          margin: 0;
          padding: 0;
      }
     blockquote{
          position: relative;

          /* */
          width: 100%;
          
          margin: 0;
          padding: 0;
      }

      
        </style>





<div id="content1">
        <div id="content2">
<blockquote class="instagram-media" data-instgrm-permalink="https://www.instagram.com/p/Bu6ziucH5Ev/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="12" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:500px; min-width:316px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/Bu6ziucH5Ev/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> Ver esta publicación en Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/Bu6ziucH5Ev/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">Una publicación compartida de Juventud Globalizada (@juventudglobalizada)</a> el <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2019-03-12T18:46:43+00:00">12 Mar, 2019 a las 11:46 PDT</time></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>
</div>
</div>
<!----------INSTAGRAM--------------->



   <br></div><!---->
          
        </div>
    </div>
        <div class="row" id="patroc">
        <div class="col-12">
            <div class="cont" align="center">
              
            
              <div class="mx-auto d-block">
              <?php
              use App\Patro;
              //$patros=Patro::orderBy('id','ASC')->where('id',1)->paginate();
              //$patros=Patro::all();
              $patros=Patro::all()->take(4);


              //$counter1=1;
              foreach($patros as $patro){
                  ?>
<!--------------------------------->
        <div class="box">
        <div class="imgBox">
              <img src="/intranet/storage/app/public/images/{{$patro->foto}}">
          </div>
          <div class="details">
              <div class="cont2">
     <!--         <div id="txt4" style="text-align: center; font-family: Montserrat, sans-serif; width:100%;">-->
                            <h2 style="text-align:center; font-family: 'Montserrat', sans-serif;">{!!$patro->titulo!!}</h2>
                            <div style="text-align:center; font-family: 'Montserrat', sans-serif;">

                            <p style="text-align:center; font-family: 'Montserrat', sans-serif;">{!!$patro->body!!}</p>
                          </div>
<!--</div>-->
</div>        
          </div>
        </div>
<!------------------------------------->
<?php
//$counter1++;
              }

            ?>
               </div> 
              
              
            </div>
            </div>
          
        </div> 
        
        <div class="row" id="bottom">

       
        <div class="col-12 col-sm-6 col-md-2 col-lg-4 order-1 order-sm-1 bordeo">
          <br>
            <p class="text-center" style="color:white; font-family: 'Montserrat', sans-serif;">
              <i class="fas fa-smile"></i><strong> @lang('home.conocenos_menu')</strong></p>
            <p class="text-center" id="tx"><a class="i2" href="{{url('/index')}}"><img id="ongvjg" src="{{url('/images/iconofinal1.png')}}" style="width:100px;"></a></p>
            <p class="text-center" id="tz" style="color:white; font-family: 'Roboto', sans-serif;"
            >
            <strong>Juventud</strong>Globalizada</p>
          <br>
          <ul id="linkbottom">
            <ul>
              
              <center>
              <li><a href="{{ url('/reclamaciones') }}" class="ulink" style="color:white; font-family: 'Montserrat', sans-serif;">@lang('home.reclamaciones_menu')</a></li>
              <br>
              
            </center>
            </ul>
            </ul>

          <br>

          </div>
          <div class="col-12 col-sm-12 col-md-8 col-lg-4 order-md-1 order-lg-1 bordep">
            
          <div id="fi" align="center">
            <br>
            
          <p><iframe id="map1" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4027.294599494253!2d-77.05698433243407!3d-11.963111892243681!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105cfd2b037c99f%3A0xcbc9d5e8896808e5!2sI.S.T.P.+Carlos+Cueto+Fernandini!5e0!3m2!1ses-419!2sus!4v1547516063940" width="85%" height="300px" frameborder="0" style="border:0" allowfullscreen></iframe>

        <!--  width="400" height="300"-->
</p>  







            </div>

            <!-- -->
              
<!--<div id="ft" align="center">
            <br>
            
          <p><iframe id="twitter" src="https://learnenglish.britishcouncil.org/basic-grammar" width="85%" height="300px" frameborder="0" style="border:0" allowfullscreen></iframe>

    
        </p>  







</div>-->

           <!--   -->
           
          </div>
          <div class="col-12 col-sm-6 col-md-2 col-lg-4 order-2 order-sm-2 order-md-2 order-lg-2 bordeq">
            <br>
            <p class="text-center" style="color:white; font-family: 'Montserrat', sans-serif;"><i class="fas fa-phone"></i><strong> @lang('home.contactanos_menu')</strong></p>
            <p class="text-center" style="color:white; font-family: 'Montserrat', sans-serif;">947326744</p>
          <br>
          <div id="fh">
            
              <a data-aos="fade-up" class="btn2" style="margin:2px;" href="">
                    <i class="icon icon-facebook2"></i>
                </a>
              
              <a data-aos="fade-up" class="btn2" style="margin:2px;" href="">
                    <i class="icon icon-youtube"></i>
                </a>
              
              <a data-aos="fade-up" class="btn2" style="margin:2px;" href="">
                    <i class="icon icon-twitter"></i>
                </a>
                
              </div>     
          <br>
          </div>
        </div>
        <div class="row">
        <div class="col-12 col-md-6 col-lg-6 bordea">
            <div class="container text-center text-md-left text-lg-left">
            <div class="tc" style="color:white; font-family: 'Montserrat', sans-serif;">&copy 2019 ONG VJG</div>
            </div>
        
        </div>
        
        <div class="col-12 col-md-6 col-lg-6 bordeb">
            <div class="container text-center text-md-right text-lg-right">
            <div class="tc" style="color:white; font-family: 'Montserrat', sans-serif;">Desarrollado por PR</div>
            </div>
        </div>
        
        </div>
  

        </div>

        <div>
        
       <!---------------------------------->

             <!---------------------------POPOVER----------------------------------------->

             <!--<script type='text/javascript' src="{{url('/js/bootstrap.min.js')}}"></script>-->







 <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>














  
<a class="popi" data-by="@lang('home.rs_right')" data-toggle="popover" style="cursor: pointer; color: rgba(11, 11, 132, 0.993);" onclick="cr1()" id="indi1"><i class="fas fa-caret-right"></i></a>





<script>

$('.popi').popover({
 delay: 200,/*1150 */
       //trigger: 'focus',
   trigger: 'hover',
       html: true,
       content: function () {

         //if($(this).data('img')=='/intranet/storage/app/public/images/noimage.jpg')
         //{
          //$(this).attr("style","font-family: Montserrat;");
         return $(this).data("by");
        // }else{
           
   //	return $(this).data("by")+'<br><br><img class="img-fluid" src="'+$(this).data('img') + '" />';
        // }
       },
       
 });   

</script>









       <!------------------------>
        <i class="fas fa-caret-left" style="cursor: pointer; color: rgba(11, 11, 132, 0.993);" onclick="cr2()" id="indi2"></i>
        </div>
        <div class="red">
            <ul>
        
            <li><a href=""  title="Facebook" class="icon-facebook2"></a>
                
            </li>
            <li><a href=""  title="Youtube" class="icon-youtube"></a></li>
            <li><a href=""  title="Twitter" class="icon-twitter"></a></li>

            </ul>
        </div>
        <!-- href="#top"-->
        <a href="#" class="to-top" title="@lang('home.boton_subir')"><div style="text-align:center;"><i class="fas fa-angle-up nbu" style="float:center; padding-bottom:8px;"></i></div></a>
        <!--<script type="text/javascript" src="{{url('/js/popper.min.js')}}"></script>-->
        
        
<script src="{{url('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>

@yield('js')
        
        
        <script type="text/javascript" src="{{url('/js/popper-1.12.9.min.js')}}"></script>
        <script type="text/javascript" src="{{url('/js/swiper.min.js')}}"></script>
        <script>
                var swiper = new Swiper('.swiper-container', {
                  effect: 'coverflow',
                  grabCursor: true,
                  centeredSlides: true,
                  slidesPerView: 'auto',
                  coverflowEffect: {
                    rotate: 50,//60
                    stretch: 0,
                    depth: 100,//500
                    modifier: 1,//5
                    slideShadows : true,
                  },
                  pagination: {
                    el: '.swiper-pagination',
                  },
                });
              </script>

   
    
    <script>
          $(function(){
            $("#dance_id2").hide();

            $("#danza_id").mouseover(function(){
              $("#dance_id1").hide();
              $("#dance_id2").show();
                });

            $("#danza_id").mouseout(function(){
              $("#dance_id2").hide();
              $("#dance_id1").show();
                });
            
          

               


          });
        </script>
     <script>
$("#indi2").hide();
$('.red').hide();

//if(document.getElementById("ccd1").style.visibility = "hidden"){   
function cr1() {
$("#indi2").show();
$("#indi1").hide();
$('.red').animate({width: 'toggle'}, "slow");
}

function cr2() {
$("#indi1").show();
$("#indi2").hide();
$('.red').animate({width: 'toggle'}, "slow");
}


</script>


        <script>
            function mf(X){
                X.classList.toggle("change");
            }
        </script>





        <script>
function myFunction(x) {
  if (x.matches) { // If media query matches
    $(".c3").show();
  } else {
    $(".c3").hide();
  }
}

var x = window.matchMedia("(max-width: 1199px)")
myFunction(x) // Call listener function at run time
x.addListener(myFunction) // Attach listener function on state changes
</script>
    
    </body>

</html>
<script src="{{url('/js/podcast.js')}}"></script>