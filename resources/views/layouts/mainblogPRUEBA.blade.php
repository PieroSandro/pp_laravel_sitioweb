<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link rel="shortcut icon" type="image/x-icon" href="{{url('/images/ESTRELLA.png')}}"/>
        <title >Blog - @yield('titleblog')</title>


        <!--style="color:white; font-family: 'Montserrat', sans-serif;"-->


        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet"> 
       <link rel="stylesheet" href="{{url('/css/animate.css')}}">
       <link rel="stylesheet" type="text/css" href="{{url('/css/swiper.min.css')}}">
       <link rel="stylesheet" href="{{url('/font.css')}}">
       <link rel='stylesheet' type='text/css' href="{{url('/css/stylesheet.css')}}">
        <link rel='stylesheet' type='text/css' href="{{url('/css/bootstrap.min.css')}}">
        <link rel='stylesheet' type='text/css' href="{{url('/css/font.css')}}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        


        
































        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
        <link rel="stylesheet" href="{{url('/css/bootnavbar.css')}}">
	<script src="{{url('/js/bootnavbar.js')}}" ></script>
        
        
        
        
        
        
        




<!-------------------------------------
        <link href="css/bootnavbar.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css" rel="styleshete">
        
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        
  ----------------->      
        
        


        
        <script type='text/javascript' src="{{url('/js/windowresize.js')}}"></script>
        <script type='text/javascript' src="{{url('/js/jquery-2.1.3.min.js')}}"></script>
        <script type='text/javascript' src="{{url('/js/jquery-3.3.1.min.js')}}"></script>
        <script type='text/javascript' src="{{url('/js/bootstrap.min.js')}}"></script>
        <script defer src="{{url('/js/all.js')}}" integrity="sha384-kW+oWsYx3YpxvjtZjFXqazFpA7UP/MbiY4jvs+RWZo2+N94PFZ36T6TFkc9O3qoB" crossorigin="anonymous"></script>
        <script src="{{url('/js/arriba.js')}}"></script>
        <script src="{{url('/js/podcast.js')}}"></script>
        <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5c87d103200ac0001700be5e&product=inline-share-buttons' async='async'></script>

        <script src='https://www.google.com/recaptcha/api.js'></script>

        <style>
          #linkbottom{
  margin:0;
  padding:0;
}


#linkbottom li{
  list-style:none;
  display:inline-block;
}


#linkbottom li a{
  text-decoration:none;
  position:relative;
  color:white;
  display:block;
  overflow:hidden;
  transition:0.7s all;


}
/*
#linkbottom li a:before{
  content:'';
  width:35px;
  
  position:absolute;
  border-bottom:3px solid;
  bottom:0;
  right:310px;
  transition:0.7s all;
  
}

#linkbottom li a:hover:before{
  right:0;
}*/

.thecard{
	
	width:250px;
	height:320px;
	transform-style:preserve-3d;
	transition:all 0.5s ease;
	
}

.thecard:hover{
	transform:rotateY(180deg);
}

.tfront{
	position:absolute;
	width:250px;
	height:320px;
	backface-visibility:hidden;
	background: url('images/cardf1.jpg')no-repeat;
	color:#333;
	text-align:center;
	border-radius:20px;
}

#text1{
	padding-top: 50%;
	align-items:center;/* 4s*/
	/*-webkit-animation:spin 14s linear infinite;
    -moz-animation:spin 14s linear infinite;
    animation:spin 14s linear infinite;*/
    /*-webkit-animation-delay:2s;*/


}



.tback{
	position:absolute;
	width:250px;
	height:320px;
	backface-visibility:hidden;
	
	background:url('images/cardf2.jpg')no-repeat;
	color:#333;
	text-align:center;
	border-radius:20px;
	transform:rotateY(180deg);
}

#bt1car:before{
	
	content:'@lang('home.ver_slider')';
  align-content: center;
  font-family: 'Montserrat', sans-serif;
	font-size:11.1px;
	position:absolute;
	top:50%;
	left:50%;
	transition:all 0.3s;
	transform:translate(-50%,-50%);
}
/*15px */ /*12.4px; */
#bt1car2:before{
	
	content:'@lang('home.ver_slider')';
  align-content: center;
  font-family: 'Montserrat', sans-serif;
	font-size:11.1px;/*15px *//*12.4px; */
	position:absolute;
	top:50%;
	left:50%;
	transition:all 0.3s;
	transform:translate(-50%,-50%);
}

#bt1car3:before{
	
	content:'@lang('home.ver_slider')';
  align-content: center;
  font-family: 'Montserrat', sans-serif;
	font-size:11.1px;/*15px *//*12.4px; */
	position:absolute;
	top:50%;
	left:50%;
	transition:all 0.3s;
	transform:translate(-50%,-50%);
}
#bt2car:before{
	
	
	content:'@lang('home.ver_slider')';
  align-content: center;
  font-family: 'Montserrat', sans-serif;
	font-size:10px;
	position:absolute;
	top:50%;
	left:50%;
	transition:all 0.3s;
  transform:translate(-50%,-50%);
  
}

.dropdown:hover>.dropdown-menu{
  display: block;
}


.insta {
  padding-right: 43px;
  padding-left: 43px;
}
        </style>
    </head>
    <body>

        <div class="container-fluid">
            

            <div class="row" id="top">

            <nav class="col-12 navbar navbar-expand-xl bg navbar fixed-top" style="background-color:white;"><!-- id="navbar"|fixed-top -->
  <a href="{{url('/blog/home')}}" id="lglogo" class="navbar-brand"><img src="{{url('/images/CCURRlogo.png')}}" style="width:165px;"></a><!--padding-top:20px; width:150px-->
<!------------------------------------------->




    <div class="order-xl-1 offset-1 offset-sm-4 offset-md-4 offset-lg-5 offset-xl-0">

 

            <?php
           
            $locale=App::getLocale();

            if(App::isLocale('en')){
              ?>
         
         
         <!--<div class="mz" style="padding-right:auto;">-->
         <a href="{{ route('change_lang', ['lang' => 'es']) }}"><img src="{{url('/images/spain.jpg')}}" id="esflag" style="width:50px; padding-top:1px;"></a>  
               <!--</div>-->
                <?php    
                        
          }else{?>
             
             <a href="{{ route('change_lang', ['lang' => 'en']) }}"><img src="{{url('/images/usa.jpg')}}" id="usflag" style="width:50px; padding-top:1px;"></a><!-- background-position:right;-->
            <?php
            }

          ?>
           </div>






    
            <!------------------------------------>
        <button class="navbar-toggler navbar-toggler-right custom-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup">
    <span class="navbar-toggler-icon"></span>
  </button>
        <div class="navbar-collapse justify-content-xl-center collapse" id="navbarNavAltMarkup"><!--<div class="navbar-collapse justify-content-md-center collapse" id="navbarNavAltMarkup"> -->
              <ul class="navbar-nav">
              <li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" style="font-family: 'Montserrat', sans-serif;" data-toggle="dropdown">@lang('home.bl_carrerast')</a>
                <div class="dropdown-menu">
                    
                    <a href="{{url('/blog/administracion')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-tasks"></i> @lang('home.bl_admin')</a>
                    <a href="{{url('/blog/computacion')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-laptop"></i> @lang('home.bl_compu')</a>
                    <a href="{{url('/blog/contabilidad')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-calculator"></i> @lang('home.bl_conta')</a>
                    <a href="{{url('/blog/electronica')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-microchip"></i> @lang('home.bl_elec1')</a>
                    <a href="{{url('/blog/electrotecnia')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="far fa-lightbulb"></i> @lang('home.bl_elec2')</a>
                    <a href="{{url('/blog/laboratorio')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-notes-medical"></i> @lang('home.bl_lacli')</a>
                    <a href="{{url('/blog/mautomotriz')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-wrench"></i> @lang('home.bl_mecau')</a>
                    <a href="{{url('/blog/mproduccion')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-cogs"></i> @lang('home.bl_mepro')</a>
                    <a href="{{url('/blog/metalurgia')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-gem"></i> @lang('home.bl_metal')</a>
                </div></li>  
              <li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" style="font-family: 'Montserrat', sans-serif;" data-toggle="dropdown">@lang('home.bl_acultural')</a>
                <div class="dropdown-menu">
                    
                    <a href="{{url('/blog/danza')}}" id="danza_id" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><img src="{{url('/images/dancen.png')}}" id="dance_id1" style="width:13px;"><img src="{{url('/images/danceb.png')}}" id="dance_id2" style="width:13px;"> @lang('home.bl_danza')</a>
                    <a href="{{url('/blog/musica')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-music"></i> @lang('home.bl_musica')</a>
                    <a href="{{url('/blog/teatro')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-theater-masks"></i> @lang('home.bl_teatro')</a>
                    
                </div></li>  
            <li class="nav-item"><a href="{{ url('/blog/v_estudiantil') }}" class="nav-link" style="font-family: 'Montserrat', sans-serif;">@lang('home.bl_vestudiantil')</a>
            </li>
            <li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" style="font-family: 'Montserrat', sans-serif;">Podcasts</a>
              <div class="dropdown-menu">
                    
                    <a href="{{url('/blog/podcast_basico')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-one"></i> @lang('home.bl_basico')</a>
                    <a href="{{url('/blog/podcast_intermedio')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-two"></i> @lang('home.bl_intermedio')</a>
                    <a href="{{url('/blog/podcast_avanzado')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-three"></i> @lang('home.bl_avanzado')</a>
                    
                </div>
            </li>
            <li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" style="font-family: 'Montserrat', sans-serif;" data-toggle="dropdown">@lang('home.bl_ginglesa')</a>
                <div class="dropdown-menu">
                    
                    <a href="{{url('/blog/basic_grammar')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-one"></i> @lang('home.bl_basico')</a>
                    <a href="{{url('/blog/intermediate_grammar')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-two"></i> @lang('home.bl_intermedio')</a>
                    <a href="{{url('/blog/advanced_grammar')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-three"></i> @lang('home.bl_avanzado')</a>
                    
                </div></li>
            <!-------------------------------XXXXXXXXXXXX-------------------------------------------->
            <!---------------------------------------------------------------------------->

            <!--<li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">@lang('home.bl_ginglesa')</a>
                <div class="dropdown-menu">
                    
                    <a href="{{url('/blog/basic_grammar')}}" class="dropdown-item"><i class="fas fa-dice-one"></i> @lang('home.bl_basico')</a>
                    <a href="{{url('/blog/intermediate_grammar')}}" class="dropdown-item"><i class="fas fa-dice-two"></i> @lang('home.bl_intermedio')</a>
                    <li class="nav-item dropdown"><a href="{{url('/blog/advanced_grammar')}}" class="dropdown-item"><i class="fas fa-dice-three"></i> @lang('home.bl_avanzado')</a>

                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
	              <li><a class="dropdown-item xy2" href="#"><i class="fas fa-dice-one"></i> @lang('home.bl_basico')</a></li>
	              <li><a class="dropdown-item xy2" href="#"><i class="fas fa-dice-two"></i> @lang('home.bl_intermedio')</a></li>
                <li><a class="dropdown-item xy2" href="#"><i class="fas fa-dice-three"></i> @lang('home.bl_avanzado')</a></li>
	              
	            </ul></li>
                </div></li>-->


            <!---------------------------------------------------------------------------->
            <!----------------------------XXXXXXXXXXXXXXXX------------------------------------------------->

            <li class="nav-item dropdown">
	        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" style="font-family: 'Montserrat', sans-serif;" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          @lang('home.bl_itecnico')</a>
<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
<!--<div class="dropdown-divider"></div>-->
<li class="nav-item dropdown">
<a class="dropdown-item dropdown-toggle" id="xy1" href="#" id="navbarDropdown1" style="font-family: 'Montserrat', sans-serif;" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-tasks"></i>
@lang('home.bl_admin')
</a>
	<ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
	              <li><a class="dropdown-item" id="xy2" href="{{url('/blog/administracion_basico')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-one"></i> @lang('home.bl_basico')</a></li>
	              <li><a class="dropdown-item" id="xy2" href="{{url('/blog/administracion_intermedio')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-two"></i> @lang('home.bl_intermedio')</a></li>
                <li><a class="dropdown-item" id="xy2" href="{{url('/blog/administracion_avanzado')}}" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-three"></i> @lang('home.bl_avanzado')</a></li>
	              
	              
	            </ul>
	          </li>
<!--<li></li><a class="dropdown-item" href="#">Something else here</a></li>-->
<li class="nav-item dropdown">
<a class="dropdown-item dropdown-toggle" id="xy1" href="#" style="font-family: 'Montserrat', sans-serif;" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-laptop"></i>
@lang('home.bl_compu')
</a>
	<ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
	              <li><a class="dropdown-item" id="xy2" href="#" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-one"></i> @lang('home.bl_basico')</a></li>
	              <li><a class="dropdown-item" id="xy2" href="#" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-two"></i> @lang('home.bl_intermedio')</a></li>
                <li><a class="dropdown-item" id="xy2" href="#" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-three"></i> @lang('home.bl_avanzado')</a></li>
	              
	              
	            </ul>
	          </li>
<!---------------------------------------------->
<li class="nav-item dropdown">
<a class="dropdown-item dropdown-toggle" id="xy1" href="#" style="font-family: 'Montserrat', sans-serif;" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-calculator"></i>
@lang('home.bl_conta')
</a>
	<ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
	              <li><a class="dropdown-item" id="xy2" href="#" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-one"></i> @lang('home.bl_basico')</a></li>
	              <li><a class="dropdown-item" id="xy2" href="#" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-two"></i> @lang('home.bl_intermedio')</a></li>
                <li><a class="dropdown-item" id="xy2" href="#" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-three"></i> @lang('home.bl_avanzado')</a></li>
	              
	              
	            </ul>
	          </li>
            <!------------------------------------------------>
            <li class="nav-item dropdown">
<a class="dropdown-item dropdown-toggle" id="xy1" href="#" style="font-family: 'Montserrat', sans-serif;" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-microchip"></i>
@lang('home.bl_elec1')
</a>
	<ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
	              <li><a class="dropdown-item" id="xy2" href="#" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-one"></i> @lang('home.bl_basico')</a></li>
	              <li><a class="dropdown-item" id="xy2" href="#" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-two"></i> @lang('home.bl_intermedio')</a></li>
                <li><a class="dropdown-item" id="xy2" href="#" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-three"></i> @lang('home.bl_avanzado')</a></li>
	              
	            </ul>
	          </li>

<!---------------------------------------------------------------------------------->
<li class="nav-item dropdown">
<a class="dropdown-item dropdown-toggle" style="font-family: 'Montserrat', sans-serif;" id="xy1" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-lightbulb"></i>
@lang('home.bl_elec2')
</a>
	<ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
	              <li><a class="dropdown-item" id="xy2" href="#" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-one"></i> @lang('home.bl_basico')</a></li>
	              <li><a class="dropdown-item" id="xy2" href="#" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-two"></i> @lang('home.bl_intermedio')</a></li>
                <li><a class="dropdown-item" id="xy2" href="#" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-three"></i> @lang('home.bl_avanzado')</a></li>
	              
	            </ul>
	          </li> 
<!------------------------------------------------------------------>
<li class="nav-item dropdown">
<a class="dropdown-item dropdown-toggle" style="font-family: 'Montserrat', sans-serif;" id="xy1" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-notes-medical"></i>
@lang('home.bl_lacli')
</a>
	<ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
	              <li><a class="dropdown-item" id="xy2" href="#" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-one"></i> @lang('home.bl_basico')</a></li>
	              <li><a class="dropdown-item" id="xy2" href="#" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-two"></i> @lang('home.bl_intermedio')</a></li>
                <li><a class="dropdown-item" id="xy2" href="#" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-three"></i> @lang('home.bl_avanzado')</a></li>
	              
	            </ul>
            </li> 
            
<!----------------------------------------------------------------------->
<li class="nav-item dropdown">
<a class="dropdown-item dropdown-toggle" style="font-family: 'Montserrat', sans-serif;" id="xy1" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-wrench"></i>
@lang('home.bl_mecau')
</a>
	<ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
	              <li><a class="dropdown-item" id="xy2" href="#" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-one"></i> @lang('home.bl_basico')</a></li>
	              <li><a class="dropdown-item" id="xy2" href="#" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-two"></i> @lang('home.bl_intermedio')</a></li>
                <li><a class="dropdown-item" id="xy2" href="#" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-three"></i> @lang('home.bl_avanzado')</a></li>
	              
	            </ul>
	          </li> 

<!-------------------------------------------------->

<li class="nav-item dropdown">
<a class="dropdown-item dropdown-toggle" style="font-family: 'Montserrat', sans-serif;" id="xy1" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-cogs"></i>
@lang('home.bl_mepro')
</a>
	<ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
	              <li><a class="dropdown-item" id="xy2" href="#" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-one"></i> @lang('home.bl_basico')</a></li>
	              <li><a class="dropdown-item" id="xy2" href="#" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-two"></i> @lang('home.bl_intermedio')</a></li>
                <li><a class="dropdown-item" id="xy2" href="#" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-three"></i> @lang('home.bl_avanzado')</a></li>
	              
	            </ul>
	          </li> 

<!-------------------------------------------------------------------------------------------->

<li class="nav-item dropdown">
<a class="dropdown-item dropdown-toggle" style="font-family: 'Montserrat', sans-serif;" id="xy1" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-gem"></i>
@lang('home.bl_metal')
</a>
	<ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
	              <li><a class="dropdown-item" id="xy2" href="#" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-one"></i> @lang('home.bl_basico')</a></li>
	              <li><a class="dropdown-item" id="xy2" href="#" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-two"></i> @lang('home.bl_intermedio')</a></li>
                <li><a class="dropdown-item" id="xy2" href="#" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-dice-three"></i> @lang('home.bl_avanzado')</a></li>
	              
	            </ul>
	          </li> 


<!------------------------------------------------------------------------------->
	        </ul>
	      </li>

            <!---------------------------------------------------------->
            <!----1------------------------------------------------------------------------------------------------------------------------->
            
            <li class="nav-item"><a href="{{ url('/blog/preguntas_frecuentes') }}" style="font-family: 'Montserrat', sans-serif;" class="nav-link">@lang('home.bl_pfrecuentes')</a>
            </li>
            
            
            

            <!--@yield('languages')-->
            
                     
            
            
            
              
              
              



























<!-------------------------------------------->

<!--
<li class="nav-item dropdown">
	        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
Dropdown</a>
<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
<li><a class="dropdown-item" href="#">Action</a></li>
<li><a class="dropdown-item" href="#">Another action</a></li>
<div class="dropdown-divider"></div>
<li></li><a class="dropdown-item" href="#">Something else here</a></li>
<li class="nav-item dropdown">
<a class="dropdown-item dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
Dropdown
</a>
	<ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
	              <li><a class="dropdown-item" href="#">Action</a></li>
	              <li><a class="dropdown-item" href="#">Another action</a></li>
	              <div class="dropdown-divider"></div>
	              <li></li><a class="dropdown-item" href="#">Something else here</a></li>
	              <li class="nav-item dropdown">
	                <a class="dropdown-item dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown"
	                    aria-haspopup="true" aria-expanded="false">
	                  Dropdown
	                </a>
	                <ul class="dropdown-menu" aria-labelledby="navbarDropdown2">
	                  <li><a class="dropdown-item" href="#">Action</a></li>
	                  <li><a class="dropdown-item" href="#">Another action</a></li>
	                  <div class="dropdown-divider"></div>
	                  <li></li><a class="dropdown-item" href="#">Something else here</a></li>
	                </ul>
	              </li>
	            </ul>
	          </li>
	        </ul>
	      </li>-->










<!-------------------------------------------->




































              </ul>
              

              
              </div>
             
              </nav>


              
              
            </div>

            <div class="row" id="fig">
           @yield('carusel')
            </div>





            <div class="row">
                
        <div class="col-12 col-md-6 col-lg-8 borde3">
            
           @yield('content')
            
            
        </div>
        
        
        <div class="col-12 col-md-6 col-lg-4 borde4">

            
            <div class="fg" align="center">

            <br>
            <div class="jumbotron text-center">
              
            
@yield('content2')


              
              
              
              
              
              
              
              
              
              </div>  
            



            <div id="sidebar">
                <ul>
                    <li>
                        <div class="content">
                            <h2 style="font-family: 'Montserrat', sans-serif;">Que son estos muchos</h2>
                            <p style="font-family: 'Montserrat', sans-serif;">gfgggfgdfggfffgfgfgggf
                            gggfdddddddddddfdfffgfggg
                            fffgfffffffffffffffffggfg
                            sssfcdccdcddddddddddddddd
                            vbbbbbbbbbbbbbfggbggbggbg
                            cffffffd <a class="btn animated shake" href="https://elcomercio.pe/" id="bt2car" target="_blank" role="button"></a></p>
                        </div>
                    </li>
                    <li>
                        <div class="content">
                            <h2 style="font-family: 'Montserrat', sans-serif;">Que son estos muchos</h2>
                            <p style="font-family: 'Montserrat', sans-serif;">gfgggfgdfggfffgfgfgggf
                            gggfdddddddddddfdfffgfggg
                            fffgfffffffffffffffffggfg
                            sssfcdccdcddddddddddddddd
                            vbbbbbbbbbbbbbfggbggbggbg
                            cffffffd. <a class="btn animated shake" href="https://elcomercio.pe/" id="bt2car" target="_blank" role="button"></a></p>
                        </div>
                    </li>
                    <li>
                        <div class="content">
                            <h2 style="font-family: 'Montserrat', sans-serif;">Que son estos muchos</h2>
                            <p style="font-family: 'Montserrat', sans-serif;">gfgggfgdfggfffgfgfgggf
                            gggfdddddddddddfdfffgfggg
                            fffgfffffffffffffffffggfg
                            sssfcdccdcddddddddddddddd
                            vbbbbbbbbbbbbbfggbggbggbg
                            cffffffd. <a class="btn animated shake" href="https://elcomercio.pe/" id="bt2car" target="_blank" role="button"></a></p>
                        </div>
                    </li>
                </ul>
            </div><br>
        
          <!----------INSTAGRAM--------------->
          <div class="insta">
<blockquote class="embedly-card"><h4><a href="http://instagram.com/juventudglobalizada" style="font-family: 'Montserrat', sans-serif;">Juventud Globalizada (@juventudglobalizada) * @lang('home.instagram_menu')</a></h4><p style="font-family: 'Montserrat', sans-serif;">@lang('home.insta2_menu')</p></blockquote>
<script async src="//cdn.embedly.com/widgets/platform.js" charset="UTF-8"></script>
</div>
<!----------INSTAGRAM--------------->



   <br></div><!---->
          
        </div>
    </div>
        <div class="row" id="patroc">
          <div class="col-12">
            <div class="cont" align="center">
              
              
              <div class="mx-auto d-block">
                <!--<div class="float-center">-->
                <div class="box">
          <div class="imgBox">
              <img src="{{url('/images/fort.jpg')}}">
          </div>
          <div class="details">
              <div class="cont2">
                            <h2 style="font-family: 'Montserrat', sans-serif;">Que son estos muchos</h2>
                            <p style="font-family: 'Montserrat', sans-serif;">gfgggfgdfggfffgfgfgggf
                            gggfdddddddddddfdf
                            fffgffffffffffffff
                            sssfcdccdcdddddddd
                            bbbfggb
                            cfff.</p>
</div>        
          </div>
        </div>
        <div class="box">
        <div class="imgBox">
              <img src="{{url('/images/fort.jpg')}}">
          </div>
          <div class="details">
              <div class="cont2">
                            <h2 style="font-family: 'Montserrat', sans-serif;">Que son estos muchos</h2>
                            <p style="font-family: 'Montserrat', sans-serif;">gfgggfgdfggfffgfgfgggf
                            gggfdddddddddddfdf
                            fffgffffffffffffff
                            sssfcdccdcdddddddd
                            bbbfggb
                            cfff.</p>
</div>        
          </div>
        </div>
        <div class="box">
        <div class="imgBox">
              <img src="{{url('/images/fort.jpg')}}">
          </div>
          <div class="details">
              <div class="cont2">
                            <h2 style="font-family: 'Montserrat', sans-serif;">Que son estos muchos</h2>
                            <p style="font-family: 'Montserrat', sans-serif;">gfgggfgdfggfffgfgfgggf
                            gggfdddddddddddfdf
                            fffgffffffffffffff
                            sssfcdccdcdddddddd
                            bbbfggb
                            cfff.</p>
</div>        
          </div>
        </div>
        <div class="box">
        <div class="imgBox">
              <img src="{{url('/images/fort.jpg')}}">
          </div>
          <div class="details">
              <div class="cont2">
                            <h2 style="font-family: 'Montserrat', sans-serif;">Que son estos muchos</h2>
                            <p style="font-family: 'Montserrat', sans-serif;">gfgggfgdfggfffgfgfgggf
                            gggfdddddddddddfdf
                            fffgffffffffffffff
                            sssfcdccdcdddddddd
                            bbbfggb
                            cfff.</p>
</div>        
          </div>
        </div>
               </div> 
              
              
            </div>
            </div>
          
        </div> 
        
        <div class="row" id="bottom">

        <div class="col-12 col-sm-6 col-md-2 col-lg-4 order-1 order-sm-1 bordeo">
          <br>
            <p class="text-center" style="color:white; font-family: 'Montserrat', sans-serif;"><i class="fas fa-smile"></i>
              <strong> @lang('home.conocenos_menu')</strong></p>
            <p class="text-center" id="tx"><a class="i2" href="{{url('/index')}}"><img id="ongvjg" src="{{url('/images/iconofinal1.png')}}" style="width:100px;"></a></p>
            <p class="text-center" id="tz" style="color:white; font-family: 'Roboto', sans-serif;"
            >
              <strong>Juventud</strong>Globalizada</p>
              <br>
            <ul id="linkbottom">
            <ul>
              
              <center>
              <li><a href="#" class="ulink" style="color:white; font-family: 'Montserrat', sans-serif;">@lang('home.reclamaciones_menu')</a></li>
              <br>
              <li><a href="#" class="ulink" style="color:white; font-family: 'Montserrat', sans-serif;">Libro de Reclamaciones</a></li>
            </center>
            </ul>
            </ul>

          <br>
          </div>
          <div class="col-12 col-sm-12 col-md-8 col-lg-4 order-md-1 order-lg-1 bordep">
            
          <div id="fi" align="center">
            <br>
            
          <p><iframe id="map1" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4027.294599494253!2d-77.05698433243407!3d-11.963111892243681!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105cfd2b037c99f%3A0xcbc9d5e8896808e5!2sI.S.T.P.+Carlos+Cueto+Fernandini!5e0!3m2!1ses-419!2sus!4v1547516063940" width="85%" height="300px" frameborder="0" style="border:0" allowfullscreen></iframe>

        <!--  width="400" height="300"-->
</p>  

            </div>
          </div>
          <div class="col-12 col-sm-6 col-md-2 col-lg-4 order-2 order-sm-2 order-md-2 order-lg-2 bordeq">
            <br>
            <p class="text-center" style="color:white; font-family: 'Montserrat', sans-serif;"><i class="fas fa-phone"></i><strong> @lang('home.contactanos_menu')</strong></p>
            <p class="text-center" style="color:white; font-family: 'Montserrat', sans-serif;">947326744</p>
          <br>
          <div id="fh">
            
              <a class="btn2" href="">
                    <i class="icon icon-facebook2"></i>
                </a>
              
              <a class="btn2" href="">
                    <i class="icon icon-youtube"></i>
                </a>
              
              <a class="btn2" href="">
                    <i class="icon icon-twitter"></i>
                </a>
                
              </div>     
          <br>
          </div>













        </div>
        <div class="row">
        <div class="col-12 col-md-6 col-lg-6 bordea">
            <div class="container text-center text-md-left text-lg-left">
            <div class="tc" style="color:white; font-family: 'Montserrat', sans-serif;">&copy 2019 ONG VJG</div>
            </div>
        <!--<p class="tc">Text on the left.</p>-->
        </div>
        
        <div class="col-12 col-md-6 col-lg-6 bordeb">
            <div class="container text-center text-md-right text-lg-right">
            <div class="tc" style="color:white; font-family: 'Montserrat', sans-serif;">Desarrollado por PR</div>
            </div>
        </div>
        <!--<p class="alignleft">Text on the left.</p>
        <p class="alignright">Text on the right.</p>-->
        </div>
        </div>

        <div class="red">
            <ul>
        
            <li><a href=""  title="Facebook" class="icon-facebook2"></a>
                
            </li>
            <li><a href=""  title="Youtube" class="icon-youtube"></a></li>
            <li><a href=""  title="Twitter" class="icon-twitter"></a></li>

            </ul>
        </div>
        <!-- href="#top"-->
        <a href="#" class="to-top" title="@lang('home.boton_subir')"><div style="text-align:center;"><i class="fas fa-angle-up nbu" style="float:center; padding-bottom:8px;"></i></div></a>
        <!--<script type="text/javascript" src="{{url('/js/popper.min.js')}}"></script>-->
        <script type="text/javascript" src="{{url('/js/popper-1.12.9.min.js')}}"></script>
        <script type="text/javascript" src="{{url('/js/swiper.min.js')}}"></script>
        <script>
                var swiper = new Swiper('.swiper-container', {
                  effect: 'coverflow',
                  grabCursor: true,
                  centeredSlides: true,
                  slidesPerView: 'auto',
                  coverflowEffect: {
                    rotate: 50,//60
                    stretch: 0,
                    depth: 100,//500
                    modifier: 1,//5
                    slideShadows : true,
                  },
                  pagination: {
                    el: '.swiper-pagination',
                  },
                });
              </script>

<script src="{{url('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>

    @yield('js')
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    
    <script>
          $(function(){
            $("#dance_id2").hide();

            $("#danza_id").mouseover(function(){
              $("#dance_id1").hide();
              $("#dance_id2").show();
                });

            $("#danza_id").mouseout(function(){
              $("#dance_id2").hide();
              $("#dance_id1").show();
                });
            
          

                $(window).resize(function(){
                    winSize();
               });

               function winSize(){
               
        if ($(window).width() < 568) {
          document.getElementById("bt1car").style.marginTop = "0px";
          document.getElementById("bt1car2").style.marginTop = "0px";
          document.getElementById("bt1car3").style.marginTop = "0px";
        }
    
    else{
      if ($(window).width() < 880) {
      document.getElementById("bt1car").style.marginTop = "50px";
      document.getElementById("bt1car2").style.marginTop = "50px";
      document.getElementById("bt1car3").style.marginTop = "50px";
      }else{
        if($(window).width() < 1200){
          document.getElementById("bt1car").style.marginTop = "120px";
          document.getElementById("bt1car2").style.marginTop = "120px";
          document.getElementById("bt1car3").style.marginTop = "120px";
        }else{
          document.getElementById("bt1car").style.marginTop = "205px";
          document.getElementById("bt1car2").style.marginTop = "205px";
          document.getElementById("bt1car3").style.marginTop = "205px";
        }
      }
    }
          }
           

               $(window).show(function(){
                    win2();
               });

               function win2(){
               
        if ($(window).width() < 568) {
          document.getElementById("bt1car").style.marginTop = "0px";
          document.getElementById("bt1car2").style.marginTop = "0px";
          document.getElementById("bt1car3").style.marginTop = "0px";
        }
    
    else{
      if ($(window).width() < 880) {
      document.getElementById("bt1car").style.marginTop = "50px";
      document.getElementById("bt1car2").style.marginTop = "50px";
      document.getElementById("bt1car3").style.marginTop = "50px";
      }else{
        if($(window).width() < 1200){
          document.getElementById("bt1car").style.marginTop = "120px";
          document.getElementById("bt1car2").style.marginTop = "120px";
          document.getElementById("bt1car3").style.marginTop = "120px";
        }else{
          document.getElementById("bt1car").style.marginTop = "205px";
          document.getElementById("bt1car2").style.marginTop = "205px";
          document.getElementById("bt1car3").style.marginTop = "205px";
        }
      }
    }
          }


          });
        </script>
    
    
    </body>

</html>
