<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link rel="shortcut icon" type="image/x-icon" href="{{url('/images/ESTRELLA.png')}}"/>
        <title>@yield('title')</title>
        <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet"> 
       
       <link rel="stylesheet" href="{{url('/css/animate.css')}}">
       <link rel="stylesheet" type="text/css" href="{{url('/css/swiper.min.css')}}">
       <link rel="stylesheet" href="{{url('/font.css')}}">
       <link rel='stylesheet' type='text/css' href="{{url('/css/stylesheet.css')}}">
        <link rel='stylesheet' type='text/css' href="{{url('/css/bootstrap.min.css')}}">
        <link rel='stylesheet' type='text/css' href="{{url('/css/font.css')}}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <script type='text/javascript' src="{{url('/js/jquery-2.1.3.min.js')}}"></script>
        <script type='text/javascript' src="{{url('/js/jquery-3.3.1.min.js')}}"></script>
        <script type='text/javascript' src="{{url('/js/bootstrap.min.js')}}"></script>
        <script defer src="{{url('/js/all.js')}}" integrity="sha384-kW+oWsYx3YpxvjtZjFXqazFpA7UP/MbiY4jvs+RWZo2+N94PFZ36T6TFkc9O3qoB" crossorigin="anonymous"></script>
        <script src="{{url('/js/arriba.js')}}"></script>
        <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5c87d103200ac0001700be5e&product=inline-share-buttons' async='async'></script>
        <script type="text/javascript">
       $(document).ready(function(){

$("#myModal").modal('show');

});
</script>


        <!-- ----------------------------------------->

        
 <!-- ----------------------------------------->
        <style>



          #linkbottom{
  margin:0;
  padding:0;
}


#linkbottom li{
  list-style:none;
  display:inline-block;
}


#linkbottom li a{
  text-decoration:none;
  position:relative;
  color:white;
  display:block;
  overflow:hidden;
  transition:0.7s all;


}

#linkbottom li a:before{
  content:'';
  width:35px;
  
  position:absolute;
  border-bottom:3px solid;
  bottom:0;
  right:310px;
  transition:0.7s all;
  
}

#linkbottom li a:hover:before{
  right:0;
}


        </style>
    </head>
    <body>

        <div class="container-fluid">
            

            


            <div class="row" id="top">

            <nav class="col-12 navbar navbar-expand-lg bg navbar fixed-top" style="background-color:#4682B4;"><!--fixed-top <nav class="col-12 navbar navbar-expand-md bg-dark navbar-dark fixed-top" style="background-color:yellow;">-->
  <a href="{{url('/index')}}" class="navbar-brand"><img src="{{url('/images/CCURRlogo.png')}}" style="width:150px;"></a>
        <!--30 -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" style="color:#cbc7c6;">
    <span class="navbar-toggler-icon"></span>
  </button>
        <div class="navbar-collapse justify-content-md-center collapse" id="navbarNavAltMarkup">
              <ul class="navbar-nav">
              <li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">@lang('home.home_menu')</a>
                <div class="dropdown-menu">
                    
                    <a href="{{url('/somos')}}" class="dropdown-item">@lang('home.about_menu')</a>
                    <a href="{{url('/mision')}}" class="dropdown-item">@lang('home.mision_menu')</a>
                    <a href="{{url('/vision')}}" class="dropdown-item">@lang('home.vision_menu')</a>
                    <a href="{{url('/docentes')}}" class="dropdown-item">Docentes de Transversales/ Inglés</a>
                
                </div></li>
            <li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">@lang('home.examenes_menu')</a>
                <div class="dropdown-menu">
                    <a href="{{url('/suficiencia')}}" class="dropdown-item">Exámenes de suficiencia</a>
                    <a href="#" class="dropdown-item">Exámenes extraordinarios</a>
                </div>
            </li>
            <li class="nav-item"><a href="{{ url('/t_english') }}" class="nav-link">@lang('home.testenglish_menu')</a></li>
            <li class="nav-item"><a href="{{ url('/videos') }}" class="nav-link">Videos</a></li>
            <li class="nav-item"><a href="{{ url('/blog/home') }}" class="nav-link" target="_blank">Blog</a></li>
            <li class="nav-item"><a href="{{ url('/login') }}" class="nav-link">Intranet</a></li>
            <li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">@lang('home.publi_menu')</a>
                <div class="dropdown-menu">
                    <a href="{{url('/suficiencia')}}" class="dropdown-item">@lang('home.revistas_menu')</a>
                    <a href="#" class="dropdown-item">Exámenes extraordinarios</a>
                </div>
            </li>
            <li class="nav-item"><a href="{{ url('/contactenos') }}" class="nav-link">Registro cursos</a></li>
            <li class="nav-item"><a href="{{ url('/contactenos') }}" class="nav-link">@lang('home.biblioteca_menu')</a></li>
            <!--<li class="nav-item"><a href="{{ url('/contactenos') }}" class="nav-link"><i class="fas fa-poll"></i> Encuestas</a></li>-->
            <li class="nav-item"><a href="{{ url('/contactenos') }}" class="nav-link">@lang('home.contactenos_menu')</a></li>
        
              </ul>
              </div>
              </nav>

            </div>
            <!--<div class="row" id="ab">
</div>-->


            <div class="row" id="fig">
              <div class="carousel slide" id="MagicCarousel" data-ride="carousel">
                <ol id="myCarousel-indicators" class="carousel-indicators">
                  <li class="active" data-slide-to="0" data-target="#MagicCarousel"></li>
                  <li data-slide-to="1" data-target="#MagicCarousel"></li>
                  <li data-slide-to="2" data-target="#MagicCarousel"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                      <div class="carousel-item active">
                          
                          <img class="d-block w-100" src="{{url('/images/banner-de-iniciacion-1.png')}}" alt="First slide">
                          <div class="carousel-caption">
                          
                               <a class="btn animated shake" href="https://elcomercio.pe/" id="bt1car" target="_blank" role="button">
                                
                               </a>
<!--
<a class="btn btn-primary" href="Balotario-suficiencia.pdf" target="_blank" role="button">Balotario</a><br><br>-->



                          </div>  
                      </div>
                      <div class="carousel-item">
                          <img class="d-block w-100" src="{{url('/images/BANER-DE-INICIACION-2.png')}}" alt="Second slide">
                          <div class="carousel-caption">
                          
                                  <a class="btn animated shake" href="https://www.goltelevision.com/" id="bt1car" target="_blank" role="button">
                                
                               </a>
                          </div>  
                      </div>
                      <div class="carousel-item">
                          <img class="d-block w-100" src="{{url('/images/baner-de-iniciacion-3.png')}}" alt="Third slide">
                          
                          <div class="carousel-caption">
                          <a class="btn animated shake" href="https://elcomercio.pe/" id="bt1car" target="_blank" role="button">
                                
                               </a> 
                                  
                          </div>  
                      </div>
                </div>
                <a class="carousel-control-prev" href="#MagicCarousel"
            data-slide="prev" role="button">
              <span class="carousel-control-prev-icon"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#MagicCarousel"
            data-slide="next" role="button">
              <span class="carousel-control-next-icon"></span>
              <span class="sr-only">Next</span>
            </a>
              </div>
            </div>





            <div class="row">
                
        <div class="col-12 col-md-6 col-lg-8 borde3">
            
            
            @yield('content')
            
            
        </div>
        
        
        <div class="col-12 col-md-6 col-lg-4 borde4">
            @guest

@yield('content2')
                @else
                          <br>

            <div class="jumbotron text-center">
                
                            
                               <p> <a id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                  <b>  Welcome {{ Auth::user()->name }}! </b><span class="caret"></span>
                                </a>
                                <br>
                                    <br>
                                <a class="btn btn-primary btn-lg" href="{{url('/blog/create')}}" role="button">Crear Anuncio</a>
                                    <br>
                                    <br>
                                    <a class="btn btn-success btn-lg" href="{{url('/blog/home')}}" role="button">Editar/Eliminar Anuncio</a>
                                    <br>
                                    <br>
                                    <a class="btn btn-danger btn-lg" href="{{ route('logout') }}" role="button"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    </p>
                                
                                    <!--<a class="ooo" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>-->

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                
                            </div>
                        @endguest

            
            <div class="fg" align="center">
            <div id="sidebar">
                <ul>
                    <li>
                        <div class="content">
                            <h2>Que son estos muchos</h2>
                            <p>gfgggfgdfggfffgfgfgggf
                            gggfdddddddddddfdfffgfggg
                            fffgfffffffffffffffffggfg
                            sssfcdccdcddddddddddddddd
                            vbbbbbbbbbbbbbfggbggbggbg
                            cffffffd <a class="btn animated shake" href="https://elcomercio.pe/" id="bt2car" target="_blank" role="button"></a></p>
                        </div>
                    </li>
                    <li>
                        <div class="content">
                            <h2>Que son estos muchos</h2>
                            <p>gfgggfgdfggfffgfgfgggf
                            gggfdddddddddddfdfffgfggg
                            fffgfffffffffffffffffggfg
                            sssfcdccdcddddddddddddddd
                            vbbbbbbbbbbbbbfggbggbggbg
                            cffffffd. <a class="btn animated shake" href="https://elcomercio.pe/" id="bt2car" target="_blank" role="button"></a></p>
                        </div>
                    </li>
                    <li>
                        <div class="content">
                            <h2>Que son estos muchos</h2>
                            <p>gfgggfgdfggfffgfgfgggf
                            gggfdddddddddddfdfffgfggg
                            fffgfffffffffffffffffggfg
                            sssfcdccdcddddddddddddddd
                            vbbbbbbbbbbbbbfggbggbggbg
                            cffffffd. <a class="btn animated shake" href="https://elcomercio.pe/" id="bt2car" target="_blank" role="button"></a></p>
                        </div>
                    </li>
                </ul>
            </div><br>
            
            
            <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.2';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-page" data-href="https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/" data-tabs="timeline" data-width="421" data-height="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Crosscurricular-ModuleEnglish-914570952000066/">Crosscurricular Module English</a></blockquote></div><br></div><!---->
          <br>
        </div>
        
    </div>
        <div class="row" id="patroc">
          <div class="col-12">
            <div class="cont" align="center">
              
              
              <div class="mx-auto d-block">
                <!--<div class="float-center">-->
                <div class="box">
          <div class="imgBox">
              <img src="{{url('/images/fort.jpg')}}">
          </div>
          <div class="details">
              <div class="cont2">
                            <h2>Que son estos muchos</h2>
                            <p>gfgggfgdfggfffgfgfgggf
                            gggfdddddddddddfdf
                            fffgffffffffffffff
                            sssfcdccdcdddddddd
                            bbbfggb
                            cfff.</p>
</div>        
          </div>
        </div>
        <div class="box">
        <div class="imgBox">
              <img src="{{url('/images/fort.jpg')}}">
          </div>
          <div class="details">
              <div class="cont2">
                            <h2>Que son estos muchos</h2>
                            <p>gfgggfgdfggfffgfgfgggf
                            gggfdddddddddddfdf
                            fffgffffffffffffff
                            sssfcdccdcdddddddd
                            bbbfggb
                            cfff.</p>
</div>        
          </div>
        </div>
        <div class="box">
        <div class="imgBox">
              <img src="{{url('/images/fort.jpg')}}">
          </div>
          <div class="details">
              <div class="cont2">
                            <h2>Que son estos muchos</h2>
                            <p>gfgggfgdfggfffgfgfgggf
                            gggfdddddddddddfdf
                            fffgffffffffffffff
                            sssfcdccdcdddddddd
                            bbbfggb
                            cfff.</p>
</div>        
          </div>
        </div>
        <div class="box">
        <div class="imgBox">
              <img src="{{url('/images/fort.jpg')}}">
          </div>
          <div class="details">
              <div class="cont2">
                            <h2>Que son estos muchos</h2>
                            <p>gfgggfgdfggfffgfgfgggf
                            gggfdddddddddddfdf
                            fffgffffffffffffff
                            sssfcdccdcdddddddd
                            bbbfggb
                            cfff.</p>
</div>        
          </div>
        </div>
               </div> 
              
              
            </div>
            </div>
          
        </div> 
        <div class="row" id="bottom">

       
          <div class="col-12 col-sm-6 col-md-2 col-lg-4 order-1 order-sm-1 bordeo">
          <br>
            <p class="text-center" style="color:white;"><i class="fas fa-smile"></i><strong> Conócenos</strong></p>
            <p class="text-center" id="tx"><a class="i2" href="{{url('/index')}}"><img id="ongvjg" src="{{url('/images/iconofinal1.png')}}" style="width:100px;"></a></p>
            <p class="text-center" id="tz" style="color:white;"
            >
              JUVENTUD GLOBALIZADA</p>
          <br>
          <ul id="linkbottom">
            <ul>
              
              <center>
              <li><a href="{{ url('/reclamaciones') }}">Libro de Reclamaciones</a></li>
              <br>
              <li><a href="#">Libro</a></li>
            </center>
            </ul>
            </ul>

          <br>

          </div>
          <div class="col-12 col-sm-12 col-md-8 col-lg-4 order-md-1 order-lg-1 bordep">
            
          <div id="fi" align="center">
            <br>
            
          <p><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4027.294599494253!2d-77.05698433243407!3d-11.963111892243681!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105cfd2b037c99f%3A0xcbc9d5e8896808e5!2sI.S.T.P.+Carlos+Cueto+Fernandini!5e0!3m2!1ses-419!2sus!4v1547516063940" width="85%" height="300px" frameborder="0" style="border:0" allowfullscreen></iframe>

        <!--  width="400" height="300"-->
</p>  

            </div>
          </div>
          <div class="col-12 col-sm-6 col-md-2 col-lg-4 order-2 order-sm-2 order-md-2 order-lg-2 bordeq">
            <br>
            <p class="text-center" style="color:white;"><i class="fas fa-phone"></i><strong> Contáctanos</strong></p>
            <p class="text-center" style="color:white;">947326744</p>
          <br>
          <div id="fh">
            
              <a class="btn2" href="">
                    <i class="icon icon-facebook2"></i>
                </a>
              
              <a class="btn2" href="">
                    <i class="icon icon-youtube"></i>
                </a>
              
              <a class="btn2" href="">
                    <i class="icon icon-twitter"></i>
                </a>
                
              </div>     
          <br>
          </div>
        </div>
        <div class="row">
        <div class="col-12 col-md-6 col-lg-6 bordea">
            <div class="container text-center text-md-left text-lg-left">
            <div class="tc" style="color:white;">&copy 2019 ONG VJG</div>
            </div>
        <!--<p class="tc">Text on the left.</p>-->
        </div>
        
        <div class="col-12 col-md-6 col-lg-6 bordeb">
            <div class="container text-center text-md-right text-lg-right">
            <div class="tc" style="color:white;">Desarrollado por PR</div>
            </div>
        </div>
        <!--<p class="alignleft">Text on the left.</p>
        <p class="alignright">Text on the right.</p>-->
        </div>

        <!--<div class="row">
            <div class="container text-right text-md-center text-lg-left">
                amigos
            </div>
        </div>-->    

        </div>


        <!-- .........................................................-->
        
        <!-- .........................................................-->
        <div class="red">
            <ul>
        
            <li><a href=""  title="Facebook" class="icon-facebook2"></a>
                
            </li>
            <li><a href=""  title="Youtube" class="icon-youtube"></a></li>
            <li><a href=""  title="Twitter" class="icon-twitter"></a></li>
            <!--<a class="link" href="" id="btred"><i class="fas fa-caret-right"></i></a>-->
            </ul>

        </div>
        <!-- href="#top"-->
        <div id="myModal" class="modal fade">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">
            <a href="#" class="navbar-brand"><img src="{{url('/images/CCURRlogo.png')}}" style="width:65px;"></a>
              
            <h4 class="modal-title" style="margin:0 auto;">Hola</h4>
          
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                

            </div>

            <div class="modal-body">

                <p style="text-align:center">Mensaje</p>

                <form>

                    <div class="form-group">

                        <input type="text" class="form-control" placeholder="Nombre">

                    </div>

                    <div class="form-group">

                        <input type="email" class="form-control" placeholder="Correo">

                    </div>

                    <button type="submit" class="btn btn-primary">Enviar</button>

                </form>

            </div>

        </div>

    </div>

</div>
        <!--<div class="modal hide fade" id="myModal">
  <div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Modal header</h3>
  </div>
  <div class="modal-body">
    <p>One fine body…</p>
  </div>
  <div class="modal-footer">
    <a href="#" class="btn">Close</a>
    <a href="#" class="btn btn-primary">Save changes</a>
  </div>
</div>
<a class="btn" data-toggle="modal" href="#myModal" >Launch Modal</a>-->
        <a href="#" class="to-top" title="Subir"><i class="fas fa-arrow-alt-circle-up"></i></a>
        <!--<script type="text/javascript" src="{{url('/js/popper.min.js')}}"></script>-->
        <script type="text/javascript" src="{{url('/js/popper-1.12.9.min.js')}}"></script>
        <script type="text/javascript" src="{{url('/js/swiper.min.js')}}"></script>
        <script>
                var swiper = new Swiper('.swiper-container', {
                  effect: 'coverflow',
                  grabCursor: true,
                  centeredSlides: true,
                  slidesPerView: 'auto',
                  coverflowEffect: {
                    rotate: 50,//60
                    stretch: 0,
                    depth: 100,//500
                    modifier: 1,//5
                    slideShadows : true,
                  },
                  pagination: {
                    el: '.swiper-pagination',
                  },
                });
              </script>
              
    </body>

</html>
