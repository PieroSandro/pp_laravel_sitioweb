<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link rel="shortcut icon" type="image/x-icon" href="{{url('/images/ESTRELLA.png')}}"/>
        <title>Blog - @yield('titleblog')</title>
        



        <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet"> 
       
       <link rel="stylesheet" href="{{url('/css/animate.css')}}">
       <link rel="stylesheet" type="text/css" href="{{url('/css/swiper.min.css')}}">
       <link rel="stylesheet" href="{{url('/font.css')}}">
       <link rel='stylesheet' type='text/css' href="{{url('/css/stylesheet.css')}}">
        <link rel='stylesheet' type='text/css' href="{{url('/css/bootstrap.min.css')}}">
        <link rel='stylesheet' type='text/css' href="{{url('/css/font.css')}}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <script type='text/javascript' src="{{url('/js/jquery-2.1.3.min.js')}}"></script>
        <script type='text/javascript' src="{{url('/js/jquery-3.3.1.min.js')}}"></script>
        <script type='text/javascript' src="{{url('/js/bootstrap.min.js')}}"></script>
        <script defer src="{{url('/js/all.js')}}" integrity="sha384-kW+oWsYx3YpxvjtZjFXqazFpA7UP/MbiY4jvs+RWZo2+N94PFZ36T6TFkc9O3qoB" crossorigin="anonymous"></script>
        <script src="{{url('/js/arriba.js')}}"></script>
        
        <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5c87d103200ac0001700be5e&product=inline-share-buttons' async='async'></script>
        <style>
          #linkbottom{
  margin:0;
  padding:0;
}


#linkbottom li{
  list-style:none;
  display:inline-block;
}


#linkbottom li a{
  text-decoration:none;
  position:relative;
  color:white;
  display:block;
  overflow:hidden;
  transition:0.7s all;


}

#linkbottom li a:before{
  content:'';
  width:35px;
  
  position:absolute;
  border-bottom:3px solid;
  bottom:0;
  right:310px;
  transition:0.7s all;
  
}

#linkbottom li a:hover:before{
  right:0;
}

#bt1car:before{
	
	content:'@lang('home.ver_slider')';
  align-content: center;
  font-family: 'Montserrat', sans-serif;
	font-size:12.4px;/*15px */
	position:absolute;
	top:50%;
	left:50%;
	transition:all 0.3s;
	transform:translate(-50%,-50%);
}
#bt2car:before{
	
	
	content:'@lang('home.ver_slider')';
  align-content: center;
  font-family: 'Montserrat', sans-serif;
	font-size:10px;
	position:absolute;
	top:50%;
	left:50%;
	transition:all 0.3s;
	transform:translate(-50%,-50%);
}
        </style>
    </head>
    <body>

        <div class="container-fluid">
            

            <div class="row" id="top">

            <nav class="col-12 navbar navbar-expand-md bg navbar fixed-top" style="background-color:#4682B4;"><!--fixed-top -->
  <a href="{{url('/blog/home')}}" class="navbar-brand"><img src="{{url('/images/CCURRlogo.png')}}" style="width:150px;"></a>

<!-------------------------------------------------------------->
  <div class="order-md-1 offset">

 

<?php

$locale=App::getLocale();

if(App::isLocale('en')){
  ?>


<!--<div class="mz" style="padding-right:auto;">-->
<a href="{{ route('change_lang', ['lang' => 'es']) }}"><img src="{{url('/images/spain.jpg')}}" id="esflag" style="width:50px; padding-top:1px;"></a>  
   <!--</div>-->
    <?php    
            
}else{?>
 
 <a href="{{ route('change_lang', ['lang' => 'en']) }}"><img src="{{url('/images/usa.jpg')}}" id="usflag" style="width:50px; padding-top:1px;"></a><!-- background-position:right;-->
<?php
}

?>
</div>

<!---------------------------------------------------------------->


        <button class="navbar-toggler navbar-toggler-right custom-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup">
    <span class="navbar-toggler-icon"></span>
  </button>
        <div class="navbar-collapse justify-content-md-center collapse" id="navbarNavAltMarkup">
              <ul class="navbar-nav">
              <li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" style="font-family: 'Montserrat', sans-serif;" data-toggle="dropdown">@lang('home.bl_carrerast')</a>
                <div class="dropdown-menu">
                    
                    <a href="{{url('/blog/administracion')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-tasks"></i> @lang('home.bl_admin')</a>
                    <a href="{{url('/blog/computacion')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-laptop"></i> @lang('home.bl_compu')</a>
                    <a href="{{url('/blog/contabilidad')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-calculator"></i> @lang('home.bl_conta')</a>
                    <a href="{{url('/blog/electronica')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-microchip"></i> @lang('home.bl_elec1')</a>
                    <a href="{{url('/blog/electrotecnia')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="far fa-lightbulb"></i> @lang('home.bl_elec2')</a>
                    <a href="{{url('/blog/laboratorio')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-notes-medical"></i> @lang('home.bl_lacli')</a>
                    <a href="{{url('/blog/mautomotriz')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-wrench"></i> @lang('home.bl_mecau')</a>
                    <a href="{{url('/blog/mproduccion')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-cogs"></i> @lang('home.bl_mepro')</a>
                    <a href="{{url('/blog/metalurgia')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-gem"></i> @lang('home.bl_metal')</a>
                </div></li>  
              <!--<li class="nav-item"><a href="{{ url('/blog/all_actividades') }}" class="nav-link">Actividades Culturales</a></li>-->
              <li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" style="font-family: 'Montserrat', sans-serif;" data-toggle="dropdown">@lang('home.bl_acultural')</a>
                <div class="dropdown-menu">
                    
                    <a href="{{url('/blog/danza')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-tasks"></i> @lang('home.bl_danza')</a>
                    <a href="{{url('/blog/musica')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-laptop"></i> @lang('home.bl_musica')</a>
                    <a href="{{url('/blog/teatro')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-calculator"></i> @lang('home.bl_teatro')</a>
                    
                </div></li>  
            <li class="nav-item"><a href="{{ url('/blog/v_estudiantil') }}" class="nav-link" style="font-family: 'Montserrat', sans-serif;">@lang('home.bl_vestudiantil')</a>
            </li>
            <li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" style="font-family: 'Montserrat', sans-serif;" data-toggle="dropdown">Podcasts</a>
              <div class="dropdown-menu">
                    
                    <a href="{{url('/blog/podcast_basico')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-tasks"></i> @lang('home.bl_basico')</a>
                    <a href="{{url('/blog/podcast_intermedio')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-laptop"></i> @lang('home.bl_intermedio')</a>
                    <a href="{{url('/blog/podcast_avanzado')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-calculator"></i> @lang('home.bl_avanzado')</a>
                    
                </div>
            </li>
            <li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" style="font-family: 'Montserrat', sans-serif;"  data-toggle="dropdown">@lang('home.bl_ginglesa')</a>
                <div class="dropdown-menu">
                    
                    <a href="{{url('/blog/basic_grammar')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-tasks"></i> @lang('home.bl_basico')</a>
                    <a href="{{url('/blog/intermediate_grammar')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-laptop"></i> @lang('home.bl_intermedio')</a>
                    <a href="{{url('/blog/advanced_grammar')}}" class="dropdown-item" style="font-family: 'Montserrat', sans-serif;"><i class="fas fa-calculator"></i> @lang('home.bl_avanzado')</a>
                    
                </div></li>  
            <li class="nav-item"><a href="{{ url('/blog/preguntas_frecuentes') }}" class="nav-link" style="font-family: 'Montserrat', sans-serif;">@lang('home.bl_pfrecuentes')</a>
            </li>
            
            
            
            
            
              
              
              
              </ul>


              
              </div>
              </nav>

            </div>



            <div class="row">
              <div class="carousel slide" id="MagicCarousel" data-ride="carousel">
                <ol id="myCarousel-indicators" class="carousel-indicators">
                  <li class="active" data-slide-to="0" data-target="#MagicCarousel"></li>
                  <li data-slide-to="1" data-target="#MagicCarousel"></li>
                  <li data-slide-to="2" data-target="#MagicCarousel"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                      <div class="carousel-item active">
                          
                          <img class="d-block w-100" src="{{url('/images/ship.jpg')}}" alt="First slide">
                          <div class="carousel-caption">
                          <h1 class="animated zoomInLeft">ggggfggfgfg</h1>
                                  <h3 class="animated zoomInLight">gghvvvdvvdvdvdvddvvd</h3>
                                  <a class="btn animated shake" href="https://elcomercio.pe/" id="bt1car" target="_blank" role="button">
                                
                               </a>
                          </div>  
                      </div>
                      <div class="carousel-item">
                          <img class="d-block w-100" src="{{url('/images/bunny.jpg')}}" alt="Second slide">
                          <div class="carousel-caption">
                          <h1 class="animated fadeInRight">LIMA</h1>
                                  <h3 class="animated fadeInLeft">atmosphere</h3>
                                  <a class="btn animated shake" href="https://www.goltelevision.com/" id="bt1car" target="_blank" role="button">
                                
                               </a>
                          </div>  
                      </div>
                      <div class="carousel-item">
                          <img class="d-block w-100" src="{{url('/images/english_cae.jpg')}}" alt="Third slide">
                          <div class="carousel-caption">
                          <h1 class="animated flip">ICA</h1>
                                  <h3 class="animated slideInUp">milo</h3>
                                  <a class="btn animated shake" href="https://elcomercio.pe/" id="bt1car" target="_blank" role="button">
                                
                               </a>
                          </div>  
                      </div>
                </div>
                <a class="carousel-control-prev" href="#MagicCarousel"
            data-slide="prev" role="button">
              <span class="carousel-control-prev-icon"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#MagicCarousel"
            data-slide="next" role="button">
              <span class="carousel-control-next-icon"></span>
              <span class="sr-only">Next</span>
            </a>
              </div>
            </div>





            <div class="row">
                
        <div class="col-12 col-md-6 col-lg-8 borde3">
            
        @if(count($errors)>0)
    @foreach($errors->all() as $error)
        <div class="alert alert-danger">
            {{$error}}
        </div>
    @endforeach
    @endif
    @if(session('success'))
    <div class="alert alert-success">
        {{session('success')}}
    </div>
    @endif

    @if(session('danger'))
    <div class="alert alert-danger">
        {{session('danger')}}
    </div>
    @endif
            @yield('content')
            
            
        </div>
        
        
        <div class="col-12 col-md-6 col-lg-4 borde4">

            
            <div class="fg" align="center">

            <br>
            <div class="jumbotron text-center">
              
            @guest



@yield('content2')



                @else
                            <p> <a id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                  <b>  Welcome {{ Auth::user()->name }}! </b><span class="caret"></span>
                                </a>
                                <br>
                                    <br>
                                <a class="btn btn-primary btn-lg" href="{{url('/blog/create')}}" role="button">Crear Post</a>
                                    <br>
                                    <br>
                                    <a class="btn btn-success btn-lg" href="{{url('/blog/home')}}" role="button">Editar/Eliminar Post</a>
                                    <br>
                                    <br>
                                    <a class="btn btn-danger btn-lg" href="{{ route('logout') }}" role="button"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    </p>
                                
                                    <!--<a class="ooo" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>-->

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                        @endguest
              
              
              
              
              
              
              
              
              
              </div>  
            



            <div id="sidebar">
                <ul>
                    <li>
                        <div class="content">
                            <h2>Que son estos muchos</h2>
                            <p>gfgggfgdfggfffgfgfgggf
                            gggfdddddddddddfdfffgfggg
                            fffgfffffffffffffffffggfg
                            sssfcdccdcddddddddddddddd
                            vbbbbbbbbbbbbbfggbggbggbg
                            cffffffd <a class="btn animated shake" href="https://elcomercio.pe/" id="bt2car" target="_blank" role="button"></a></p>
                        </div>
                    </li>
                    <li>
                        <div class="content">
                            <h2>Que son estos muchos</h2>
                            <p>gfgggfgdfggfffgfgfgggf
                            gggfdddddddddddfdfffgfggg
                            fffgfffffffffffffffffggfg
                            sssfcdccdcddddddddddddddd
                            vbbbbbbbbbbbbbfggbggbggbg
                            cffffffd. <a class="btn animated shake" href="https://elcomercio.pe/" id="bt2car" target="_blank" role="button"></a></p>
                        </div>
                    </li>
                    <li>
                        <div class="content">
                            <h2>Que son estos muchos</h2>
                            <p>gfgggfgdfggfffgfgfgggf
                            gggfdddddddddddfdfffgfggg
                            fffgfffffffffffffffffggfg
                            sssfcdccdcddddddddddddddd
                            vbbbbbbbbbbbbbfggbggbggbg
                            cffffffd. <a class="btn animated shake" href="https://elcomercio.pe/" id="bt2car" target="_blank" role="button"></a></p>
                        </div>
                    </li>
                </ul>
            </div><br>
        



   <br></div><!---->
          
        </div>
    </div>
        <div class="row" id="patroc">
          <div class="col-12">
            <div class="cont" align="center">
              
              
              <div class="mx-auto d-block">
                <!--<div class="float-center">-->
                <div class="box">
          <div class="imgBox">
              <img src="{{url('/images/fort.jpg')}}">
          </div>
          <div class="details">
              <div class="cont2">
                            <h2>Que son estos muchos</h2>
                            <p>gfgggfgdfggfffgfgfgggf
                            gggfdddddddddddfdf
                            fffgffffffffffffff
                            sssfcdccdcdddddddd
                            bbbfggb
                            cfff.</p>
</div>        
          </div>
        </div>
        <div class="box">
        <div class="imgBox">
              <img src="{{url('/images/fort.jpg')}}">
          </div>
          <div class="details">
              <div class="cont2">
                            <h2>Que son estos muchos</h2>
                            <p>gfgggfgdfggfffgfgfgggf
                            gggfdddddddddddfdf
                            fffgffffffffffffff
                            sssfcdccdcdddddddd
                            bbbfggb
                            cfff.</p>
</div>        
          </div>
        </div>
        <div class="box">
        <div class="imgBox">
              <img src="{{url('/images/fort.jpg')}}">
          </div>
          <div class="details">
              <div class="cont2">
                            <h2>Que son estos muchos</h2>
                            <p>gfgggfgdfggfffgfgfgggf
                            gggfdddddddddddfdf
                            fffgffffffffffffff
                            sssfcdccdcdddddddd
                            bbbfggb
                            cfff.</p>
</div>        
          </div>
        </div>
        <div class="box">
        <div class="imgBox">
              <img src="{{url('/images/fort.jpg')}}">
          </div>
          <div class="details">
              <div class="cont2">
                            <h2>Que son estos muchos</h2>
                            <p>gfgggfgdfggfffgfgfgggf
                            gggfdddddddddddfdf
                            fffgffffffffffffff
                            sssfcdccdcdddddddd
                            bbbfggb
                            cfff.</p>
</div>        
          </div>
        </div>
               </div> 
              
              
            </div>
            </div>
          
        </div> 
        
        <div class="row" id="bottom">

        <div class="col-12 col-sm-6 col-md-2 col-lg-4 order-1 order-sm-1 bordeo">
          <br>
            <p class="text-center" style="color:white;"><i class="fas fa-smile"></i><strong> @lang('home.conocenos_menu')</strong></p>
            <p class="text-center" id="tx"><a class="i2" href="{{url('/index')}}"><img id="ongvjg" src="{{url('/images/iconofinal1.png')}}" style="width:100px;"></a></p>
            <p class="text-center" id="tz" style="color:white;"
            >
              JUVENTUD GLOBALIZADA</p>
              <br>
            <ul id="linkbottom">
            <ul>
              
              <center>
              <li><a href="#">@lang('home.reclamaciones_menu')</a></li>
              <br>
              <li><a href="#">Libro de Reclamaciones</a></li>
            </center>
            </ul>
            </ul>

          <br>
          </div>
          <div class="col-12 col-sm-12 col-md-8 col-lg-4 order-md-1 order-lg-1 bordep">
            
          <div id="fi" align="center">
            <br>
            
          <p><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4027.294599494253!2d-77.05698433243407!3d-11.963111892243681!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105cfd2b037c99f%3A0xcbc9d5e8896808e5!2sI.S.T.P.+Carlos+Cueto+Fernandini!5e0!3m2!1ses-419!2sus!4v1547516063940" width="400px" height="300px" frameborder="0" style="border:0" allowfullscreen></iframe>

        <!--  width="400" height="300"-->
</p>  

            </div>
          </div>
          <div class="col-12 col-sm-6 col-md-2 col-lg-4 order-2 order-sm-2 order-md-2 order-lg-2 bordeq">
            <br>
            <p class="text-center" style="color:white;"><i class="fas fa-phone"></i><strong> @lang('home.contactanos_menu')</strong></p>
            <p class="text-center" style="color:white;">947326744</p>
          <br>
          <div id="fh">
            
              <a class="btn2" href="">
                    <i class="icon icon-facebook2"></i>
                </a>
              
              <a class="btn2" href="">
                    <i class="icon icon-youtube"></i>
                </a>
              
              <a class="btn2" href="">
                    <i class="icon icon-twitter"></i>
                </a>
                
              </div>     
          <br>
          </div>













        </div>
        <div class="row">
        <div class="col-12 col-md-6 col-lg-6 bordea">
            <div class="container text-center text-md-left text-lg-left">
            <div class="tc" style="color:white;">&copy 2019 ONG VJG</div>
            </div>
        <!--<p class="tc">Text on the left.</p>-->
        </div>
        
        <div class="col-12 col-md-6 col-lg-6 bordeb">
            <div class="container text-center text-md-right text-lg-right">
            <div class="tc" style="color:white;">Desarrollado por PR</div>
            </div>
        </div>
        <!--<p class="alignleft">Text on the left.</p>
        <p class="alignright">Text on the right.</p>-->
        </div>
        </div>

        <div class="red">
            <ul>
        
            <li><a href=""  title="Facebook" class="icon-facebook2"></a>
                
            </li>
            <li><a href=""  title="Youtube" class="icon-youtube"></a></li>
            <li><a href=""  title="Twitter" class="icon-twitter"></a></li>

            </ul>
        </div>
        <!-- href="#top"-->
        <a href="#" class="to-top" title="Subir"><i class="fas fa-arrow-alt-circle-up"></i></a>
        <!--<script type="text/javascript" src="{{url('/js/popper.min.js')}}"></script>-->
        <script type="text/javascript" src="{{url('/js/popper-1.12.9.min.js')}}"></script>
        <script type="text/javascript" src="{{url('/js/swiper.min.js')}}"></script>
        <script>
                var swiper = new Swiper('.swiper-container', {
                  effect: 'coverflow',
                  grabCursor: true,
                  centeredSlides: true,
                  slidesPerView: 'auto',
                  coverflowEffect: {
                    rotate: 50,//60
                    stretch: 0,
                    depth: 100,//500
                    modifier: 1,//5
                    slideShadows : true,
                  },
                  pagination: {
                    el: '.swiper-pagination',
                  },
                });
              </script>





<!--<script src="{{url('/js/app.js')}}" defer></script>-->

    </body>

</html>
