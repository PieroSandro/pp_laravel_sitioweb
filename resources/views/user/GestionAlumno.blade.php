@extends('layouts.master')
@section('contenido')

                      
<div class="card shadow mb-4">
            <div class="card-header py-3"></div>
            <div class="card-body">
                 @if(session('message-success'))
            <div class="alert alert-success ">
                {{ session('message-success') }}
            </div>
            @elseif(session('message-danger'))
            <div class="alert alert-danger ">
                {{ session('message-danger') }}
            </div>
            @endif
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>

            
            <th class="text-center">Nombres</th>
            <th class="text-center">Apellidos</th>
            <th class="text-center">Sexo</th>
            <th class="text-center">Foto</th>
            <th class="text-center">Dni</th>
            <th class="text-center">Editar</th>
            <th class="text-center">Eliminar</th>


            </thead>
            <tbody>
               @foreach($alumnos as $alumno)
                <tr>
                    <td class="text-center">{{$alumno->nombre}}</td>
                    <td class="text-center">{{$alumno->apellidos}}</td>
                    <td class="text-center">{{$alumno->sexo}}</td>
                    <td class="text-center">
                        
                        <img src="{{ Storage::url($alumno->foto)}}" alt="" width="75">
                        
                    </td>
                    <td class="text-center">{{$alumno->dni}}</td>
                    <td class="text-center"><a href="{{ url('/user/gestion/alumno/'.$alumno->id.'/edit') }}" class="btn btn-success">Editar</a></td>
                    <td class="text-center">
                        
                        <form id="delete-form" method="POST" action="{{ url('/user/gestion/alumno/'.$alumno->id) }}">
                             @method('DELETE')
                            {{ csrf_field() }}

                            <button type="button" class="btn btn-danger dalumno">Eliminar</button>
                            
                        </form>
                    </td>
                </tr>
                @endforeach
        

            </tbody>

        </table>

        
        
    </div>
</div>
</div>

@endsection