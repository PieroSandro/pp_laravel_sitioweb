@extends('layouts.master')
@section('contenido')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

             
            <div class="card">
                <div class="card-header">{{ __('ACTUALIZAR DATOS') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('user.update') }}" enctype="multipart/form-data">
                        @csrf

                        <input type="hidden" name="id" value="{{ Auth::user()->Persona->id}}">
                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right">{{ __('Nombre') }}</label>

                            <div class="col-md-4">
                                <input id="name" type="text" class="form-control" name="name" value="{{ ucwords(strtolower(Auth::user()->Persona->nombre)) }}" readonly >
                             
                            </div>
                             <label for="surname" class="col-md-2 col-form-label text-md-right">{{ __('Apellidos') }}</label>
                            <div class="col-md-4">
                                <input id="surname" type="text" class="form-control" name="surname" value="{{ Auth::user()->Persona->apellidos }}" readonly >
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="dni" class="col-md-2 col-form-label text-md-right">{{ __('Dni') }}</label>

                            <div class="col-md-4">
                                <input id="dni" type="text" class="form-control{{ $errors->has('Dni') ? ' is-invalid' : '' }}" name="dni" value="{{ Auth::user()->Persona->dni }}" required autofocus>

                                @if ($errors->has('dni'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('dni') }}</strong>
                                </span>
                                @endif
                            </div>

                            <label for="fecha_n" class="col-md-2 col-form-label text-md-right">{{ __('F.Nacimiento') }}</label>

                            <div class="col-md-4">
                                <input id="fecha_n" type="date" class="form-control{{ $errors->has('fecha_n') ? ' is-invalid' : '' }}" name="fecha_n" value="{{ Auth::user()->Persona->fecha_n }}" required autofocus>

                                @if ($errors->has('fecha_n'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('fecha_n') }}</strong>
                                </span>
                                @endif
                            </div>

                        
                        </div>

                        <div class="form-group row">
                            <label for="direccion" class="col-md-2 col-form-label text-md-right">{{ __('Dirección') }}</label>

                            <div class="col-md-10">
                                <input id="direccion" type="text" class="form-control{{ $errors->has('direccion') ? ' is-invalid' : '' }}" name="direccion" value="{{ Auth::user()->Persona->direccion }}" required>

                                @if ($errors->has('direccion'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('direccion') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">


                            <label for="foto" class="col-md-2 col-form-label text-md-right">{{ __('Sube tu Foto') }}</label>

                            <div class="col-md-10">

                                <input id="foto" type="file" name="foto" value="" style="width: 100%;">

                                @if ($errors->has('foto'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('foto') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        
                        
                        <div class="col-md-12">
                               
                            </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-10 offset-md-2">
                                <button type="submit" class="btn btn-primary">
                                    GUARDAR CAMBIOS
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>

        </div>
    </div>
</div>
  

@endsection