@extends('layouts.master')
@section('contenido')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if(session('message-success'))
                <div class="alert alert-success">
                    {{ session('message-success') }}
                </div>
            @elseif(session('message-danger'))
                <div class="alert alert-danger">
                    {{ session('message-danger') }}
                </div>
            @endif
            <div class="card">
                <div class="card-header">{{ __('Perfil Del Usuario') }}</div>

                <div class="card-body">
                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right">{{ __('Nombre') }}</label>

                            <div class="col-md-4">
                                <input id="name" type="text" class="form-control" name="name" value="{{ ucwords(strtolower(Auth::user()->Persona->nombre)) }}" readonly >
                             
                            </div>
                             <label for="surname" class="col-md-2 col-form-label text-md-right">{{ __('Apellidos') }}</label>
                            <div class="col-md-4">
                                <input id="surname" type="text" class="form-control" name="surname" value="{{ Auth::user()->Persona->apellidos }}" readonly >
                            </div>
                        </div>
                         <div class="form-group row">
                            <label for="dni" class="col-md-2 col-form-label text-md-right">{{ __('Dni') }}</label>

                            <div class="col-md-4">
                                <input id="dni" type="text" class="form-control" name="dni" value="{{ Auth::user()->Persona->dni }}" readonly >      
                            </div>

                            <label for="fecha_n" class="col-md-2 col-form-label text-md-right">{{ __('F.Nacimiento') }}</label>
                            <div class="col-md-4">
                                <input id="fecha_n" type="text" class="form-control" name="fecha_n" value="{{ Auth::user()->Persona->fecha_n }}" readonly >
                            </div>
                             
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-md-2 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                            <div class="col-md-4">
                                <input id="email" type="email" class="form-control" name="email" value="{{ Auth::user()->email }}" readonly>
                            </div>

                            <label for="sexo" class="col-md-2 col-form-label text-md-right">{{ __('Género') }}</label>
                            <div class="col-md-4">
                                <input id="sexo" type="text" class="form-control" name="sexo" value="{{ Auth::user()->Persona->sexo }}" readonly >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="direccion" class="col-md-2 col-form-label text-md-right">{{ __('Dirección') }}</label>
                            <div class="col-md-10">
                                <input id="direccion" type="text" class="form-control" name="direccion" value="{{ Auth::user()->Persona->direccion }}" readonly >
                            </div>
                        </div>

                          <div class="form-group row">
                          
                          <img src="{{ Storage::url(Auth::user()->Persona->foto) }}" style="display:block;margin:auto; width: 90%; height: 400px;"/>
                          
                            <div class="col-md-12">
                               
                            </div>
                        </div>
                             
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="mb-5">
        
    </div>
@endsection