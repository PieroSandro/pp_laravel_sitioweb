@extends('layouts.master')

@section('contenido')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @if(session('message'))
            <div class="alert alert-success">
                {{session('message')}}
            </div>
            @endif
            <div class="card">
                <div class="card-header">{{ __('ACTUALIZAR DATOS') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{url('/user/gestion/profesor/'.$user->id)}}" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <input type="hidden" name="id" value="{{$user->id}}">
                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right">{{ __('Nombre') }}</label>

                            <div class="col-md-4">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $user->nombre}}" required autofocus>

                                @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>

                            <label for="surname" class="col-md-2 col-form-label text-md-right">{{ __('Apellidos') }}</label>

                            <div class="col-md-4">
                                <input id="surname" type="text" class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" value="{{ $user->apellidos }}" required autofocus>

                                @if ($errors->has('surname'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('surname') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="dni" class="col-md-2 col-form-label text-md-right">{{ __('Dni') }}</label>

                            <div class="col-md-4">
                                <input id="dni" type="text" class="form-control{{ $errors->has('Dni') ? ' is-invalid' : '' }}" name="dni" value="{{ $user->dni  }}" required autofocus>

                                @if ($errors->has('dni'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('dni') }}</strong>
                                </span>
                                @endif
                            </div>
                            <label for="sexo" class="col-md-2 col-form-label text-md-right">{{ __('Sexo') }}</label>

                            <div class="col-md-4">
                                <input id="sexo" type="text" class="form-control{{ $errors->has('sexo') ? ' is-invalid' : '' }}" name="sexo" value="{{ ucwords(strtolower($user->sexo))  }}" required autofocus>
                    
                                @if ($errors->has('sexo'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('sexo') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="foto" class="col-md-2 col-form-label text-md-right">{{ __('Sube tu Foto') }}</label>

                            <div class="col-md-10">
                                <input id="foto" type="file" class="form-control{{ $errors->has('foto') ? ' is-invalid' : '' }}" name="foto" value="" >

                                @if ($errors->has('foto'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('foto') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            @if($user->foto)
                            <img src="{{Storage::url($user->foto)}}" alt="" style="display:block;margin:auto; width: 500px; height: 400%;">
                            @endif
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-2">
                                <button type="submit" class="btn btn-primary">
                                    GUARDAR CAMBIOS
                                </button>

                            </div>
                        </div>
                    </form>
                       
                </div>
            </div>
        </div>
    </div>
</div>
@endsection