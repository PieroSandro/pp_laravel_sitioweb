@extends('layouts.master')
@section('contenido')

               
    <div class="card shadow mb-4">
            <div class="card-header py-3"></div>
            <div class="card-body">
                 
            <div class="alert alert-success" id="alert-s" hidden=""></div>
            <div class="alert alert-danger" id="alert-danger" hidden=""></div>
            
    
    <div class="table-responsive">
        <table class="table table-bordered" width="100%" cellspacing="0">
            <thead>
                <th class="text-center">Apellidos y Nombres</th>
                <th class="text-center">Asistio</th>
                <th class="text-center">Falto</th>
                
            </thead>
            <tbody>
                <?php $i = 1; ?>
                
                <form action="{{ route('update.asistencia') }}" method="get" id="form-a">
                     {{ csrf_field() }}
                @foreach($asistencias as $asistencia)
               <tr>
                  
                    <td class="text-center">{{ mb_strtoupper($asistencia->Registro->Persona->apellidos)}} {{ $asistencia->Registro->Persona->nombre}}</td>
                    <input type="text" name='<?php echo "id[".$i."]";?>' value="{{ $asistencia->id }}" hidden="">
                    <td class="text-center"><input type="radio" name='<?php echo "asistencia[".$i."]";?>' id="asistio" value="1" 
                        @if($asistencia->estado == '1') checked @endif required></td>
                    <td class="text-center"><input type="radio" name='<?php echo "asistencia[".$i."]";?>' id="falto" value="0" 
                        @if($asistencia->estado == '0') checked @endif required></td>
                    <?php $i++; ?>
                    
                </tr>
                 
                 @endforeach
                 <input type="submit" name="update" value="Actualizar" class="btn btn-info mb-4">
                 </form>
            </tbody>
        </table>
        
    </div>
    
</div>
</div>
<script src="{{asset('js/asistencia/asistencia.js')}}"></script>
@endsection