@extends('layouts.master')
@section('contenido')


<div class="col-md-12 ">
    <div class="card">
        <div class="card-header">
             <h2>Asistencia</h2>
        </div>
         <div class="panel-body">

        <div id="container">
            <table class="table table-bordered mt-4 container" >

            <thead>
            <th class="text-center">Dia - Mes</th>
          
            <th class="text-center">Estado</th>
    
            </thead>
            <tbody>
                @forelse($asistencias as $asistencia)
                @if($asistencia->Registro->user_id == Auth::user()->Persona->id)
                <tr>

                    <td class="text-center"><span  style="color: #000000;font-weight: bold; font-size: 18px;">{{ date('d F',strtotime($asistencia->created_at)) }}</span></td>
                    @if($asistencia->estado == 1)  
                    <td class="text-center text-success">Asistio</td> 
                    @else
                    <td class="text-center text-danger">Falto</td>     
                    @endif
                </tr>
                @endif
                @empty
            <h2>No hay datos a cargar</h2>
            @endforelse
            </tbody>
        </table>
        </div>

       
    </div>
    </div>
   
</div>
@endsection