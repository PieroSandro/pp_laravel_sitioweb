@extends('layouts.master')
@section('contenido')
       
    
        <div class="alert alert-success" id="alert" hidden="">
        </div>
        <div class="alert alert-danger" id="alert-danger" hidden="">
        </div>

    <div id="box">
                <h2>Horarios</h2>
                <div id="tabs">
                  <ul>
                    <li><a href="#tabs-1">Turno Mañana</a></li>
                    <li><a href="#tabs-2">Turno Noche</a></li>
                  </ul>

                  <div id="tabs-1">
                        <div id="acordeon">

                                    <h3>Ver</h3>
                    <div>
                        <div class="card shadow mb-4">
            <div class="card-header text-center">
                <h1>ASISTENCIA</h1>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered"width="100%" cellspacing="0">
                  <thead>
           
            <th class="text-center">Nombre del Alumno</th>
            <th class="text-center">Asistio</th>
            <th class="text-center">Falto</th>
            
            
            </thead>
            <tbody>
                <?php $i = 1; ?>
                <form action="{{route('asistencia.mañana')}}" method="post" id="formu">
                    {{ csrf_field() }}
                @forelse($registros as $registro)
                 @if($registro->turno_id == 1)
                <tr>
                   
                    <input type="text" name='<?php echo "id[".$i."]";?>' value="{{ $registro->id }}" hidden="" id="id">
                    <td class="text-center">{{ $registro->Persona->nombre }} {{$registro->Persona->apellidos}}</td>
                        <td class ="text-center"><input type="radio" name='<?php echo "asistencia[".$i."]";?>' value ="1" id="asistio" required></td>
                        <td class ="text-center"><input type="radio" name='<?php echo "asistencia[".$i."]";?>' value ="0" id="falto" required></td>
                    <input type="text" name='<?php echo "turno[".$i."]";?>' value="1" hidden>
                    <input type="text" name='<?php echo "fecha[".$i."]";?>' value="{{ date('Y-m-d') }}" hidden>
                    <?php $i++; ?>
                    
                </tr>
                @endif
                @empty
            <h2>NO EXISTE ACTUALMENTE REGISTROS</h2>
            @endforelse
                <input type="submit" class="btn btn-success mb-2 save" value="Guardar" id="enviar">
                </form>
                <form action="{{ route('editar.asistencia') }}" method="get">
                    <input type="text" name="id" value="1" hidden="">
                    <input type="submit" name="update" value="Editar" class="btn btn-warning mb-2 ml-2">
                </form>
            </tbody>
        </table>
        
    </div>
</div>
</div>
                    
                    </div>
                    

                        </div>
                  </div>
            <!-- ############################################################### -->
                  <div id="tabs-2">
                     <div id="acordeon-2">
                         
                                    <h3>Ver</h3>
                    <div>
                        <div class="card shadow mb-4">
            <div class="card-header text-center">
                <h1>ASISTENCIA</h1>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered"width="100%" cellspacing="0">
                  <thead>
           
            <th class="text-center">Nombre del Alumno</th>
            <th class="text-center">Asistio</th>
            <th class="text-center">Falto</th>
            
            
            </thead>
            <tbody>
                <?php $i = 1; ?>
                <form action="{{route('asistencia.noche')}}" method="post" id="form-n">
                    {{ csrf_field() }}
                @forelse($registros as $registro)
                 @if($registro->turno_id == 2)
                <tr >
                    <input type="text" name='<?php echo "id[".$i."]";?>' value="{{ $registro->id }}" hidden>
                    <td class="text-center">{{ $registro->Persona->nombre }} {{$registro->Persona->apellidos}}</td>
                        <td class ="text-center"><input type="radio" name='<?php echo "asistencia[".$i."]";?>' value ="1" required></td>
                        <td class ="text-center"><input type="radio" name='<?php echo "asistencia[".$i."]";?>' value ="0" required></td>
                    <input type="text" name='<?php echo "turno[".$i."]";?>' value="2" hidden>
                    <input type="text" name='<?php echo "fecha[".$i."]";?>' value="{{ date('Y-m-d') }}" hidden>
                    <?php $i++; ?>
                    
                </tr>
                @endif
                @empty
            <h2>NO EXISTE ACTUALMENTE REGISTROS</h2>
            @endforelse
                    <input type="submit" class="btn btn-success mb-2 save" value="Guardar" id="enviar" >
                    </form>
                     <form action="{{ route('editar.asistencia') }}" method="get">
                    <input type="text" name="id" value="2" hidden="">
                    <input type="submit" name="update" value="Editar" class="btn btn-warning mb-2 ml-2">
                </form>
            </tbody>
        </table>
        
    </div>
</div>
</div>
                    </div>
                    
                    </div>

                     </div>     
                  </div>

                </div>

<script src="{{asset('js/asistencia/asistencia.js')}}"></script>

@endsection
