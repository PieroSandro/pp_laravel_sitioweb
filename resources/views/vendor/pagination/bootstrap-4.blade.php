<style>



.pid{
    color:rgba(11, 11, 132, 0.993);
    
}

.pid:hover{
    color:white;
    background-color:rgba(11, 11, 132, 0.993);
    border-color:white;
    
}

.pid2{
    color:rgba(11, 11, 132, 0.993);
    
}

.page-item span{
  background-color: rgba(228, 20, 20, 0.931) !important;

color: white !important;

border-color: white !important;
}
</style>


@if ($paginator->hasPages())
    <ul class="pagination justify-content-center" role="navigation">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="page-item disabled pid" style="font-family: 'Montserrat', sans-serif; font-weight: bold;" aria-disabled="true" aria-label="@lang('pagination.previous')">
                <span class="page-link" aria-hidden="true">&lsaquo;</span>
            </li>
        @else
            <li class="page-item">
                <a class="page-link pid" style="font-family: 'Montserrat', sans-serif;" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>
            </li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="page-item active" aria-current="page"><span class="page-link" style="font-family: 'Montserrat', sans-serif; font-weight: bold;">{{ $page }}</span></li>
                    @else
                        <li class="page-item pid2"><a class="page-link pid" style="font-family: 'Montserrat', sans-serif;" href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="page-item">
                <a class="page-link pid" style="font-family: 'Montserrat', sans-serif;" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>
            </li>
        @else
            <li class="page-item disabled" style="font-family: 'Montserrat', sans-serif; font-weight: bold;" aria-disabled="true" aria-label="@lang('pagination.next')">
                <span class="page-link" aria-hidden="true">&rsaquo;</span>
            </li>
        @endif
    </ul>
@endif

