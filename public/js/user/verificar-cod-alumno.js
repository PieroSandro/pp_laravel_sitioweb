$(document).ready(function() {
    $('form').submit(function(e) {
        e.preventDefault();
        var codigo = $('#cod_al').val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{route('codigo.query')}}",
            type: 'POST',
            data: {
                cod_al: codigo
            },
            success: function(result) {
                if (result == 'existe') {
                    $('#row-verificar').attr('hidden', 'hidden');
                    $('#row-registrar').removeAttr('hidden');
                } else {
                    $('#cod_al').val("");
                }
            }
        });
    });
});