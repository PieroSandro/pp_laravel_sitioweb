var questions = [
	{
		"question": "I only paid ₤20 for this jacket! It was a real ____.",
		"option1": "a. buy",
		"option2": "b. price",
		"option3": "c. bargain",
		"option4": "d. sale",
		"answer": "3"
	
	}, {
		"question": "Jane is always poking her nose in other people's business. She's so ____!",
		"option1": "a. inquisitive",
		"option2": "b. obedient",
		"option3": "c. playful",
		"option4": "d. unreliable",
		"answer": "1"
	
	}, {
		"question": "As far as I'm ____, I do not support the new government.",
		"option1": "a. matter",
		"option2": "b. referred",
		"option3": "c. according",
		"option4": "d. concerned",
		"answer": "4"
	
	}, {
		"question": "The window ____ was really imaginative.",
		"option1": "a. exhibition",
		"option2": "b. display",
		"option3": "c. collection",
		"option4": "d. vision",
		"answer": "2"
	
	}, {
		"question": "She ____ the sack last week and is now looking for a new job.",
		"option1": "a. did",
		"option2": "b. made",
		"option3": "c. took",
		"option4": "d. got",
		"answer": "4"
	
	}, {
		"question": "She doesn't ____ of my decision.",
		"option1": "a. agree",
		"option2": "b. approve",
		"option3": "c. accept",
		"option4": "d. support",
		"answer": "2"
	
	}, {
		"question": "During his stay in Indonesia he went ____ with malaria.",
		"option1": "a. up",
		"option2": "b. off",
		"option3": "c. down",
		"option4": "d. over",
		"answer": "3"
	
	}, {
		"question": "When the customs officers found some illegal goods hidden in the car, he was arrested for ____.",
		"option1": "a. assault",
		"option2": "b. mugging",
		"option3": "c. hijacking",
		"option4": "d. smuggling",
		"answer": "4"
	
	}, {
		"question": "I can't move the sofa. Could you ____ me a hand with it, please?",
		"option1": "a. give",
		"option2": "b. get",
		"option3": "c. take",
		"option4": "d. borrow",
		"answer": "1"
	
	}, {
		"question": "I couldn't get in ____ with you all week! Where have you been?",
		"option1": "a. contact",
		"option2": "b. call",
		"option3": "c. touch",
		"option4": "d. talk",
		"answer": "3"
	
	},
	{
	"question": "She ____ obsessed with rock climbing at a young age.",
	"option1": "a. becomes",
	"option2": "b. became",
	"option3": "c. has become",
	"option4": "d. would become",
	"answer": "2"
}, {
	"question": "He’s not a stamp collector, ___?",
	"option1": "a. was he",
	"option2": "b. wasn’t he",
	"option3": "c. is he",
	"option4": "d. isn’t he",
	"answer": "3"
}, {
	"question": "How long ____ you had this car?",
	"option1": "a. did",
	"option2": "b. do",
	"option3": "c. have",
	"option4": "d. were",
	"answer": "3"
}, {
	"question": "____ anyone get hurt?",
	"option1": "a. did",
	"option2": "b. were",
	"option3": "c. have",
	"option4": "d. had",
	"answer": "1"
}, {
	"question": "He ____ about birds. It drives me mad!",
	"option1": "a. forever talk",
	"option2": "b. is forever talking",
	"option3": "c. will forever be talking",
	"option4": "d. has forever been talking",
	"answer": "2"

}, {
	"question": "She keeps ____ her things all around the place which is so annoying.",
	"option1": "a. to leave",
	"option2": "b. leaves",
	"option3": "c. leave",
	"option4": "d. leaving",
	"answer": "4"

}, {
	"question": "He ____ me to the first game when I was only 6.",
	"option1": "a. used to take",
	"option2": "b. would take",
	"option3": "c. took",
	"option4": "d. has taken",
	"answer": "3"

}, {
	"question": "At first I ____ starting work so early but this has changed.",
	"option1": "a. didn't use to",
	"option2": "b. wouldn't",
	"option3": "c. didn't have to",
	"option4": "d. wasn't used to",
	"answer": "4"

}, {
	"question": "My new PC, ____ I bought last week, has already broken down.",
	"option1": "a. that",
	"option2": "b. which",
	"option3": "c. whose",
	"option4": "d. --",
	"answer": "2"

}, {
	"question": "I'd like to see the photos ____ you took on holiday.",
	"option1": "a. who",
	"option2": "b. whose",
	"option3": "c. where",
	"option4": "d. --",
	"answer": "4"

}, {
	"question": "He realized that he ____ his car keys in the office.",
	"option1": "a. left",
	"option2": "b. has left",
	"option3": "c. had left",
	"option4": "d. was leaving",
	"answer": "3"

}, {
	"question": "We couldn't fall asleep because our neighbours ____ a lot of noise.",
	"option1": "a. made",
	"option2": "b. had made",
	"option3": "c. have made",
	"option4": "d. were making",
	"answer": "4"

}, {
	"question": "____ plans you might have for the weekend, you'll have to change them.",
	"option1": "a. Wherever",
	"option2": "b. Whovever",
	"option3": "c. Whatever",
	"option4": "d. However",
	"answer": "3"

}, {
	"question": "They ____ out for a few years before they decided to get married.",
	"option1": "a. had gone",
	"option2": "b. have been going",
	"option3": "c. were going",
	"option4": "d. had been going",
	"answer": "4"

}, {
	"question": "You won't pass the exam ____ you start revising immediately.",
	"option1": "a. as long as",
	"option2": "b. provided",
	"option3": "c. unless",
	"option4": "d. if",
	"answer": "3"

}, {
	"question": "We wouldn't have missed the bus if you ____ to chat with Mary!",
	"option1": "a. didn't stop",
	"option2": "b. hadn't stopped",
	"option3": "c. don't stop",
	"option4": "d. wouldn't have stopped",
	"answer": "2"

}, {
	"question": "The party was so boring I wish I ____ there at all.",
	"option1": "a. hadn't gone",
	"option2": "b. wouldn't go",
	"option3": "c. haven't gone",
	"option4": "d. didn't go",
	"answer": "1"

}, {
	"question": "If only you ____ more time to spend with the family.",
	"option1": "a. would have",
	"option2": "b. have had",
	"option3": "c. had",
	"option4": "d. have",
	"answer": "3"

}, {
	"question": "Oh, you're busy? I ____ you later, OK?",
	"option1": "a. am calling",
	"option2": "b. call",
	"option3": "c. have called",
	"option4": "d. will call",
	"answer": "4"

}, {
	"question": "By the time the guests arrive, we ____ everything for the party.",
	"option1": "a. will be preparing",
	"option2": "b. will have prepared",
	"option3": "c. prepare",
	"option4": "d. have prepared",
	"answer": "2"

}, {
	"question": "During the next meeting we ____ about setting goals.",
	"option1": "a. are talking",
	"option2": "b. will have talked",
	"option3": "c. will be talking",
	"option4": "d. talk",
	"answer": "3"

}, {
	"question": "I can't find my keys. I ____ them.",
	"option1": "a. may lose",
	"option2": "b. must lost",
	"option3": "c. might have lost",
	"option4": "d. should have lost",
	"answer": "3"

}, {
	"question": "The police stopped us and said we ____ to enter the building.",
	"option1": "a. can't",
	"option2": "b. couldn't",
	"option3": "c. didn't allow",
	"option4": "d. weren't allowed",
	"answer": "4"

}, {
	"question": "Admission was free so we ____ any tickets.",
	"option1": "a. needn't buy",
	"option2": "b. mustn't buy",
	"option3": "c. didn't need to buy",
	"option4": "d. mustn't have bought",
	"answer": "3"

}, {
	"question": "I'm not sure if you're aware ____ the risk.",
	"option1": "a. of",
	"option2": "b. to",
	"option3": "c. at",
	"option4": "d. in",
	"answer": "1"

}, {
	"question": "The horror movie wasn't just frightening! It was ____ terrifying!",
	"option1": "a. extremely",
	"option2": "b. absolutely",
	"option3": "c. very",
	"option4": "d. fairly",
	"answer": "2"

}, {
	"question": "____ the weather was horrible, we decided to go out for a short walk.",
	"option1": "a. Even though",
	"option2": "b. However",
	"option3": "c. In spite of",
	"option4": "d. Despite",
	"answer": "1"

}, {
	"question": "We should remind _____ to be thankful for all that we have.",
	"option1": "a. us",
	"option2": "b. --",
	"option3": "c. ourselves",
	"option4": "d. we",
	"answer": "3"

}, {
	"question": "His father asked Dan where ____ all day.",
	"option1": "a. had he been",
	"option2": "b. was he",
	"option3": "c. he had been",
	"option4": "d. he has been",
	"answer": "3"

}, {
	"question": "She advised him ______  sun cream.",
	"option1": "a. putting",
	"option2": "b. put on",
	"option3": "c. to putting on",
	"option4": "d. to put on",
	"answer": "4"

}, {
	"question": "She was only 19 when she sailed across ____ Atlantic.",
	"option1": "a. a",
	"option2": "b. an",
	"option3": "c. the",
	"option4": "d. --",
	"answer": "3"

}, {
	"question": "Your leg could be broken so you must have ____ X-ray.",
	"option1": "a. a",
	"option2": "b. an",
	"option3": "c. the",
	"option4": "d. --",
	"answer": "2"

}, {
	"question": "The square was ____ crowded we couldn't pass.",
	"option1": "a. so",
	"option2": "b. such",
	"option3": "c. very",
	"option4": "d. as",
	"answer": "1"

}, {
	"question": "Two climbers are reported to ____ during the storm last night.",
	"option1": "a. die",
	"option2": "b. have died",
	"option3": "c. had died",
	"option4": "d. died",
	"answer": "2"

}, {
	"question": "I'll need to have the stairs ____.",
	"option1": "a. renovate",
	"option2": "b. renovating",
	"option3": "c. to renovate",
	"option4": "d. renovated",
	"answer": "4"

},
{
	"question": "People __________ from the illness find it difficult to relax.",
	"option1": "a. suffered",
	"option2": "b. suffering",
	"option3": "c. who suffering",
	"option4": "d. were suffering",
	"answer": "2"

}, {
	"question": "You'd better take your coat ____ the weather gets worse.",
	"option1": "a. in case",
	"option2": "b. otherwise",
	"option3": "c. so that",
	"option4": "d. in order to",
	"answer": "1"

}, {
	"question": "She did a course in confidence building ____ overcome her phobia.",
	"option1": "a. so that",
	"option2": "b. in order to",
	"option3": "c. although",
	"option4": "d. in case",
	"answer": "2"

}, {
	"question": "He ____ a therapist for several years after he left school.",
	"option1": "a. has seen",
	"option2": "b. has been seeing",
	"option3": "c. saw",
	"option4": "d. used to seeing",
	"answer": "3"

}, {
	"question": "She ____ much better since she left the hospital last week.",
	"option1": "a. is feeling",
	"option2": "b. feels",
	"option3": "c. felt",
	"option4": "d. has been feeling",
	"answer": "4"

}
];