var currentQuestion = 0;
var score = 0;
var totQuestions = questions.length;

var container = document.getElementById('quizContainer');
var questionEl = document.getElementById('question');
var opt1 = document.getElementById('opt1');
var opt2 = document.getElementById('opt2');
var opt3 = document.getElementById('opt3');
var opt4 = document.getElementById('opt4');
var nextButton = document.getElementById('nextButton');
var resultCont = document.getElementById('result');

function loadQuestion (questionIndex) {
	var q = questions[questionIndex];
	questionEl.textContent = (questionIndex + 1) + '. ' + q.question;
	opt1.textContent = q.option1;
	opt2.textContent = q.option2;
	opt3.textContent = q.option3;
	opt4.textContent = q.option4;
};

function loadNextQuestion () {
	var selectedOption = document.querySelector('input[type=radio]:checked');
	if(!selectedOption){
		
		Swal.fire({
			
			html: '<i class="fas fa-exclamation-circle" style="color:#F2A741; font-size:150px;"></i>'+
			'<br><br><h1>Warning!</h1><br>Please select your answer!',
			
			showConfirmButton: false,
			timer:1500
		  })
		  
		return;
	}
	var answer = selectedOption.value;
	if(questions[currentQuestion].answer == answer){
		score += 1;//10
	}
	selectedOption.checked = false;
	currentQuestion++;
	if(currentQuestion == totQuestions - 1){
		nextButton.textContent = 'Finish';
		nextButton.style.fontFamily ="Montserrat,sans-serif";
	}
	if(currentQuestion == totQuestions){
		container.style.display = 'none';
		resultCont.style.display = '';
		

		if(score<36)
		{
			Swal.fire({
				
				html: '<i class="fas fa-info-circle" style="color:#1CDDE6; font-size:150px;"></i>'+
			'<br><br><h1>The test is over!</h1><br><p>You are in <b>Beginner Level</b>, your score is:</p><br>'+score,
			animation: false,
			customClass: {
			  popup: 'animated wobble'
			}
				
			  })
			
			
		return;
		}
		else
		{
			
			
			Swal.fire({
				html: '<i class="fas fa-info-circle" style="color:#1CDDE6; font-size:150px;"></i>'+
			'<br><br><h1>The test is over!</h1><br><p>You are in <b>Elementary Level</b>, your score is:</p><br>'+score+
			'<br><br><h3>Good Job!</h3>',
			animation: false,
			customClass: {
			  popup: 'animated wobble'
			}
				
				
			  })
			return;
		
		
			
		}
		
	}
	loadQuestion(currentQuestion);
}

loadQuestion(currentQuestion);