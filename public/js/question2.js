var questions = [{
	"question": "A lot of ____ came to Ireland in the 1990s.",
	"option1": "a. immigrants",
	"option2": "b. emigrants",
	"option3": "c. invaders",
	"option4": "d. colonies",
	"answer": "1"

}, {
	"question": "There was a nice meal and a band at the wedding ____.",
	"option1": "a. ceremony",
	"option2": "b. reception",
	"option3": "c. speech",
	"option4": "d. group",
	"answer": "2"

}, {
	"question": "I mostly ____ my friends via email.",
	"option1": "a. get on well with",
	"option2": "b. have in commn",
	"option3": "c. keep in touch with",
	"option4": "d. see each other",
	"answer": "3"

}, {
	"question": "Bob has had a very interesting ___ . He has had jobs in many countries and industries.",
	"option1": "a. carrier",
	"option2": "b. job",
	"option3": "c. career",
	"option4": "d. work",
	"answer": "3"

}, {
	"question": "She’s very successful. Her ___ has risen a lot in the past few years.",
	"option1": "a. money",
	"option2": "b. salary",
	"option3": "c. job",
	"option4": "d. earnings",
	"answer": "2"

}, {
	"question": "I am very ___ in old cars.",
	"option1": "a. keen",
	"option2": "b. interesting",
	"option3": "c. interested",
	"option4": "d. fond",
	"answer": "3"

}, {
	"question": "He ___ his exam because he didn’t study.",
	"option1": "a. failed",
	"option2": "b. passed",
	"option3": "c. missed",
	"option4": "d. fell",
	"answer": "1"

}, {
	"question": "The house will look cleaner when you have finished the ____.",
	"option1": "a. home",
	"option2": "b. housewife",
	"option3": "c. housework",
	"option4": "d. homework",
	"answer": "3"

}, {
	"question": "Stress is not an illness, but it can ___ to many illnesses.",
	"option1": "a. get",
	"option2": "b. celebrate",
	"option3": "c. contribute",
	"option4": "d. affect",
	"answer": "3"

}, {
	"question": "He ____ off his holiday until after the winter.",
	"option1": "a. took",
	"option2": "b. put",
	"option3": "c. called",
	"option4": "d. logged",
	"answer": "2"

},
{
	"question": "________ at school yesterday?",
	"option1": "a. Was you",
	"option2": "b. Were you",
	"option3": "c. Did you",
	"option4": "d. Is you",
	"answer": "2"
}, {
	"question": "Is your family large? ________",
	"option1": "a. Yes, it is.",
	"option2": "b. Yes, they are.",
	"option3": "c. No, it not.",
	"option4": "d. No, they isn’t.",
	"answer": "1"
}, {
	"question": "What ____ he want?",
	"option1": "a. does",
	"option2": "b. do",
	"option3": "c. have",
	"option4": "d. was",
	"answer": "1"
}, {
	"question": "____ do you have dinner?",
	"option1": "a. When time",
	"option2": "b. What time",
	"option3": "c. What kind of",
	"option4": "d. What for",
	"answer": "2"
}, {
	"question": "He ____ to go home. ",
	"option1": "a. want",
	"option2": "b. did",
	"option3": "c. didn't want",
	"option4": "d. didn't wanted",
	"answer": "3"

}, {
	"question": "Where ____  to school?",
	"option1": "a. did you go",
	"option2": "b. went you",
	"option3": "c. did you went",
	"option4": "d. did go",
	"answer": "1"

}, {
	"question": "Latin ____ compulsory in Irish schools. ",
	"option1": "a. used to be",
	"option2": "b. would be",
	"option3": "c. has",
	"option4": "d. has been",
	"answer": "1"

}, {
	"question": "The boy  ____ cake when his mother came into the room.",
	"option1": "a. was eat",
	"option2": "b. eats",
	"option3": "c. was eating",
	"option4": "d. has eating",
	"answer": "3"

}, {
	"question": "There ____ milk for my breakfast.",
	"option1": "a. isn't some",
	"option2": "b. isn't any",
	"option3": "c. any",
	"option4": "d. --",
	"answer": "2"

}, {
	"question": "____ people from Poland went to Scotland in the 20th century.",
	"option1": "a. Many of",
	"option2": "b. Many",
	"option3": "c. Some of",
	"option4": "d. --",
	"answer": "2"

}, {
	"question": "I ____ to Peru on holiday next month.",
	"option1": "a. am flying",
	"option2": "b. flying",
	"option3": "c. am go flying",
	"option4": "d. will flying",
	"answer": "1"

}, {
	"question": "Oh! It ____. I’ll take an umbrella with me.",
	"option1": "a. raining",
	"option2": "b. will raining",
	"option3": "c. rains",
	"option4": "d. ’s raining ",
	"answer": "4"

}, {
	"question": "Do you have any plans for tonight? Yes, we ____ to the cinema.",
	"option1": "a. will go",
	"option2": "b. going",
	"option3": "c. go",
	"option4": "d. are going",
	"answer": "4"

}, {
	"question": "I plan to ____ two weeks by the beach.",
	"option1": "a. bring",
	"option2": "b. spend",
	"option3": "c. spending",
	"option4": "d. making",
	"answer": "2"

}, {
	"question": "The fast food restaurant was _____ dirty. We didn’t eat there.",
	"option1": "a. extreme",
	"option2": "b. extremely",
	"option3": "c. bit",
	"option4": "d. very much",
	"answer": "2"

}, {
	"question": "This restaurant is ____ the one over there.",
	"option1": "a. traditional",
	"option2": "b. traditionaler",
	"option3": "c. more traditional than",
	"option4": "d. traditionaler than",
	"answer": "3"

}, {
	"question": "My coffee was ____ yours. I almost burned by mouth.",
	"option1": "a. hotter than",
	"option2": "b. more hot than",
	"option3": "c. hotter as",
	"option4": "d. as hot",
	"answer": "1"

}, {
	"question": "The ____ coffee in the world comes from Indonesia.",
	"option1": "a. expensive",
	"option2": "b. expensivest",
	"option3": "c. more expensive",
	"option4": "d. most expensive",
	"answer": "4"

}, {
	"question": "I ____ sushi.",
	"option1": "a. eaten",
	"option2": "b. have eat",
	"option3": "c. have ever eaten",
	"option4": "d. have never eaten",
	"answer": "4"

}, {
	"question": "She has ____ finished this week’s report.",
	"option1": "a. yet",
	"option2": "b. already",
	"option3": "c. ever",
	"option4": "d. never",
	"answer": "2"

}, {
	"question": "I don’t think you ____ them.",
	"option1": "a. should to email",
	"option2": "b. should email",
	"option3": "c. should emailing",
	"option4": "d. --",
	"answer": "2"

}, {
	"question": "In the future there ____ cures to the world’s worst diseases.",
	"option1": "a. might be",
	"option2": "b. is going to being",
	"option3": "c. will being",
	"option4": "d. might have",
	"answer": "1"

}, {
	"question": "The space tourists ___ certainly need to be very fit.",
	"option1": "a. won’t",
	"option2": "b. will",
	"option3": "c. --",
	"option4": "d. going to",
	"answer": "2"

}, {
	"question": "If my new company is successful, I  ____ employ people to help me.",
	"option1": "a. will",
	"option2": "b. be able to",
	"option3": "c. will be able to",
	"option4": "d. will able to",
	"answer": "3"

}, {
	"question": "The first reality TV show ____ in Sweden in 1997.",
	"option1": "a. showed",
	"option2": "b. shown",
	"option3": "c. is shown",
	"option4": "d. was shown",
	"answer": "4"

}, {
	"question": "The film Avatar was directed ____ James Cameron.",
	"option1": "a. by",
	"option2": "b. from",
	"option3": "c. for",
	"option4": "d. with",
	"answer": "1"

}, {
	"question": "I’ve had my cat ____ 4 years.",
	"option1": "a. since",
	"option2": "b. for",
	"option3": "c. with",
	"option4": "d. it",
	"answer": "2"

}, {
	"question": "Her horse is lovely. She _____ it since she was a teenager.",
	"option1": "a. had",
	"option2": "b. has had",
	"option3": "c. had",
	"option4": "d. is had",
	"answer": "2"

}, {
	"question": "I’ve received 33 emails  ____.",
	"option1": "a. on Friday",
	"option2": "b. yesterday",
	"option3": "c. two days ago",
	"option4": "d. this week",
	"answer": "4"

}, {
	"question": "How often have you been to the doctor  ______.",
	"option1": "a. one year ago?",
	"option2": "b. in the last twelve months?",
	"option3": "c. yesterday?",
	"option4": "d. last week?",
	"answer": "2"

}, {
	"question": "I was saving up  ____ a new computer.",
	"option1": "a. for buying",
	"option2": "b. to buy",
	"option3": "c. to buying",
	"option4": "d. --",
	"answer": "2"

}, {
	"question": "You ____ wear a suit to work, but you can if you want.",
	"option1": "a. must",
	"option2": "b. mustn’t",
	"option3": "c. could",
	"option4": "d. don’t have to",
	"answer": "4"

}, {
	"question": "I had to ____ a uniform to school when I was younger.",
	"option1": "a. have",
	"option2": "b. wearing",
	"option3": "c. wear",
	"option4": "d. having",
	"answer": "3"

}, {
	"question": "Cecilia knows someone ____ went to the carnival in Rio de Janeiro.",
	"option1": "a. who",
	"option2": "b. which",
	"option3": "c. she",
	"option4": "d. where",
	"answer": "1"

}, {
	"question": "Oxfam is a charity ____ tries to find lasting solutions to poverty.",
	"option1": "a. who",
	"option2": "b. which",
	"option3": "c. it",
	"option4": "d. --",
	"answer": "2"

},
 {
	"question": "There are  __________ French speakers in Montreal",
	"option1": "a. too much",
	"option2": "b. a lot of",
	"option3": "c. a little",
	"option4": "d. not much",
	"answer": "2"

}, {
	"question": "She ____ with her friends on Facebook™ everyday",
	"option1": "a. is communicating",
	"option2": "b. communicates",
	"option3": "c. will communicating",
	"option4": "d. --",
	"answer": "2"

}, {
	"question": "More and more people ____ divorced every year. ",
	"option1": "a. are wanting",
	"option2": "b. wanting",
	"option3": "c. getting",
	"option4": "d. are getting",
	"answer": "4"

}, {
	"question": "Many, but not all, people ____  get married in a church.",
	"option1": "a. want to",
	"option2": "b. are wanting to",
	"option3": "c. wanting to",
	"option4": "d. used to want",
	"answer": "1"

}, {
	"question": "Would you like  ____ to the theatre tonight?",
	"option1": "a. go",
	"option2": "b. to go",
	"option3": "c. going",
	"option4": "d. to going",
	"answer": "2"

}];