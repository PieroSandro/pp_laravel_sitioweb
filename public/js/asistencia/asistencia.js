 $(document).ready(function() {
     $("#acordeon").accordion({
         active: false,
         collapsible: true
     });
     $("#acordeon-2").accordion({
         active: false,
         collapsible: true
     });
     $("#tabs").tabs();
     // Guardar Asistencias Ajax - Turno Mañana
     $('#formu').submit(function(e) {
         e.preventDefault();
         var datos = $(this).serializeArray();
         $.ajax({
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
             url: '/asistencia/store',
             type: 'POST',
             data: datos,
             success: function(result) {
                 console.log(result);
                 if (result == "Asistencia guardada exitosamente") {
                     $('#alert').removeAttr('hidden');
                     $('#alert').text(result);
                     $(".save").attr('disabled', 'disabled');
                     $('#colum').removeAttr('hidden');
                     $('.accion').removeAttr('hidden');
                     setTimeout(function() {
                         $('#alert').attr('hidden', 'hidden');
                     }, 5000);
                 } else {
                     $('#alert-danger').removeAttr('hidden');
                     $('#alert-danger').text(result);
                     setTimeout(function() {
                         $('#alert-danger').attr('hidden', 'hidden');
                     }, 5000);
                 }
             }
         });
     });
     // Guardar Asistencias Ajax - Turno Noche
     $('#form-n').submit(function(e) {
         e.preventDefault();
         var datos = $(this).serializeArray();
         $.ajax({
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
             url: '/asistencia/store/noche',
             type: 'POST',
             data: datos,
             success: function(result) {
                 console.log(result);
                 if (result == "Asistencia guardada exitosamente") {
                     $('#alert').removeAttr('hidden');
                     $('#alert').text(result);
                     $("input[type='submit']").attr('disabled', 'disabled');
                     $('#colum').removeAttr('hidden');
                     $('.accion').removeAttr('hidden');
                     setTimeout(function() {
                         $('#alert').attr('hidden', 'hidden');
                     }, 5000);
                 } else {
                     $('#alert-danger').removeAttr('hidden');
                     $('#alert-danger').text(result);
                     setTimeout(function() {
                         $('#alert-danger').attr('hidden', 'hidden');
                     }, 5000);
                 }
             }
         });
     });
     // Actualizar Asistencias Ajax
     $('#form-a').submit(function(e) {
         e.preventDefault();
         var datos = $(this).serializeArray();
         // console.log(datos);
         $.ajax({
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
             url: '/asistencia/update',
             type: 'GET',
             data: datos,
             success: function(result) {
                 if (result == "Se actualizo correctamente") {
                     $('#alert-s').removeAttr('hidden');
                     $('#alert-s').text(result);
                     setTimeout(function() {
                         $('#alert-s').attr('hidden', 'hidden');
                     }, 5000);
                 } else {
                     $('#alert-danger').removeAttr('hidden');
                     $('#alert-danger').text(result);
                 }
             }
         });
     });
     // Seleccionar Fechas Ajax
     // $('option:selected').click(function(){
     //         var $target = $("option:selected",$(this));
     //         console.log($target);
     // });
     // $.ajax({
     //     headers: {
     //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     //     },
     //     url: '/Asistencia/update',
     //     type: 'GET',
     //     data: datos,
     //     success: function(result) {
     //        if (result == "Se actualizo correctamente") {
     //            $('#alert-s').removeAttr('hidden');
     //            $('#alert-s').text(result);
     //        }else{
     //            $('#alert-danger').removeAttr('hidden');
     //            $('#alert-danger').text(result);
     //         }
     //     }
     // });
 });