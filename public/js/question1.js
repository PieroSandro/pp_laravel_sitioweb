


var questions = [{
	"question": "Gina is married to John. He's her ____",
	"option1": "a. uncle",
	"option2": "b. husband",
	"option3": "c. wife",
	"option4": "d. parent",
	"answer": "2"

}, {
	"question": "We usually ____ the shopping in a supermarket.",
	"option1": "a. make",
	"option2": "b. do",
	"option3": "c. have",
	"option4": "d. go",
	"answer": "2"

}, {
	"question": "I love this watch! It's ____.",
	"option1": "a. cheap",
	"option2": "b. small",
	"option3": "c. beautiful",
	"option4": "d. ugly",
	"answer": "3"

}, {
	"question": "He doesn't have a car so he often uses public ____.",
	"option1": "a. taxi",
	"option2": "b. transport",
	"option3": "c. car",
	"option4": "d. bus",
	"answer": "2"

}, {
	"question": "I don't go to ____ on Sundays.",
	"option1": "a. job",
	"option2": "b. office",
	"option3": "c. factory",
	"option4": "d. work",
	"answer": "4"

}, {
	"question": "Do you like Chinese ____?",
	"option1": "a. kitchen",
	"option2": "b. meal",
	"option3": "c. food",
	"option4": "d. cook",
	"answer": "3"

}, {
	"question": "They hardly ____ visit us.",
	"option1": "a. ever",
	"option2": "b. sometimes",
	"option3": "c. never",
	"option4": "d. usually",
	"answer": "1"

}, {
	"question": "I'm Jeff Caine. Nice to ____ you, Mr Caine.",
	"option1": "a. speak",
	"option2": "b. talk",
	"option3": "c. meet",
	"option4": "d. watch",
	"answer": "3"

}, {
	"question": "Can I help you? Thanks, but I'm just ____.",
	"option1": "a. watching ",
	"option2": "b. looking",
	"option3": "c. seeing",
	"option4": "d. shopping",
	"answer": "2"

}, {
	"question": "Mandy is over there. She's ____ a blue T-shirt and jeans.",
	"option1": "a. having ",
	"option2": "b. wearing",
	"option3": "c. doing",
	"option4": "d. walking",
	"answer": "2"

}, {
	"question": "Oh, ____ are my keys!",
	"option1": "a. This",
	"option2": "b. These",
	"option3": "c. That",
	"option4": "d. It",
	"answer": "2"

}, {
	"question": "I'd like ____ omelette, please.",
	"option1": "a. a",
	"option2": "b. --",
	"option3": "c. an",
	"option4": "d. two",
	"answer": "3"

}, {
	"question": "And here is your ____.",
	"option1": "a. desk",
	"option2": "b. desks",
	"option3": "c. a desk",
	"option4": "d. an desk",
	"answer": "1"

}, {
	"question": "My name's Pete and this is Sylvia. ____ doctors from France.",
	"option1": "a. I'm",
	"option2": "b. We're",
	"option3": "c. She's",
	"option4": "d. They´re",
	"answer": "2"

}, {
	"question": "Sorry, ____ Paul. My name's Eric.",
	"option1": "a. I isn't",
	"option2": "b. I is not",
	"option3": "c. I aren't",
	"option4": "d. I'm not",
	"answer": "4"

}, {
	"question": "____? No, he isn't.",
	"option1": "a. Are they teachers?",
	"option2": "b. Are you from Italy?",
	"option3": "c. Is Mr Banning a teacher?",
	"option4": "d. Is this your phone?",
	"answer": "3"

}, {
	"question": "____ is the school? It's 50 years old.",
	"option1": "a. How many years",
	"option2": "b. How much years",
	"option3": "c. What years",
	"option4": "d. How old",
	"answer": "4"

}, {
	"question": "What is ____?",
	"option1": "a. job Mary",
	"option2": "b. Mary job",
	"option3": "c. Mary's job",
	"option4": "d. job's Mary",
	"answer": "3"

}, {
	"question": "Your bag is next ____ the table.",
	"option1": "a. on",
	"option2": "b. to",
	"option3": "c. in",
	"option4": "d. of",
	"answer": "2"

}, {
	"question": "____ are the keys? On the table.",
	"option1": "a. What",
	"option2": "b. When",
	"option3": "c. Where",
	"option4": "d. Who",
	"answer": "3"

}, {
	"question": "I go to work ____ train.",
	"option1": "a. with",
	"option2": "b. by",
	"option3": "c. for",
	"option4": "d. in",
	"answer": "2"

}, {
	"question": "She ____ a dog.",
	"option1": "a. not have",
	"option2": "b. don't have",
	"option3": "c. don't has",
	"option4": "d. doesn't have",
	"answer": "4"

}, {
	"question": "Stephen ____ in our company.",
	"option1": "a. work",
	"option2": "b. works",
	"option3": "c. is work",
	"option4": "d. working",
	"answer": "2"

}, {
	"question": "____ they live in London?",
	"option1": "a. Are",
	"option2": "b. Is",
	"option3": "c. Do",
	"option4": "d. Does",
	"answer": "3"

}, {
	"question": "____ to the cinema.",
	"option1": "a. We not often go",
	"option2": "b. We don't go often",
	"option3": "c. We don't often go",
	"option4": "d. Often we don't go",
	"answer": "3"

}, {
	"question": "When do you play tennis? ____ Mondays.",
	"option1": "a. On",
	"option2": "b. In",
	"option3": "c. At",
	"option4": "d. By",
	"answer": "1"

}, {
	"question": "What time ____ work?",
	"option1": "a. starts he",
	"option2": "b. do he starts",
	"option3": "c. does he starts",
	"option4": "d. does he start",
	"answer": "4"

}, {
	"question": "____ two airports in the city.",
	"option1": "a. It is",
	"option2": "b. There is",
	"option3": "c. There are",
	"option4": "d. This is",
	"answer": "3"

}, {
	"question": "There aren't ____ here.",
	"option1": "a. a restaurants",
	"option2": "b. any restaurants",
	"option3": "c. any restaurant",
	"option4": "d. a restaurant",
	"answer": "2"

}, {
	"question": "I'm afraid it's ____.",
	"option1": "a. a hotel expensive",
	"option2": "b. expensive hotel",
	"option3": "c. expensive a hotel",
	"option4": "d. an expensive hotel",
	"answer": "4"

}, {
	"question": "They ____ popular TV programmes in the 1980s.",
	"option1": "a. are",
	"option2": "b. were",
	"option3": "c. was",
	"option4": "d. is",
	"answer": "2"

}, {
	"question": "____ at school last week?",
	"option1": "a. Do you were",
	"option2": "b. Was you",
	"option3": "c. Were you",
	"option4": "d. You were",
	"answer": "3"

}, {
	"question": "Brad Pitt is a popular actor but I don't like ____.",
	"option1": "a. him",
	"option2": "b. his",
	"option3": "c. her",
	"option4": "d. them",
	"answer": "1"

}, {
	"question": "We ____ the film last week.",
	"option1": "a. see",
	"option2": "b. saw",
	"option3": "c. sees",
	"option4": "d. were see",
	"answer": "2"

}, {
	"question": "He ____ tennis with me yesterday.",
	"option1": "a. doesn't played",
	"option2": "b. didn't played",
	"option3": "c. not played",
	"option4": "d. didn't play",
	"answer": "4"

}, {
	"question": "She was born ____ May 6th, 1979.",
	"option1": "a. in",
	"option2": "b. at",
	"option3": "c. on",
	"option4": "d. from",
	"answer": "3"

}, {
	"question": "Where ____ last summer?",
	"option1": "a. you went",
	"option2": "b. did you went",
	"option3": "c. do you went",
	"option4": "d. did you go",
	"answer": "4"

}, {
	"question": "Were you at the shops at 5 p.m. yesterday?	No, I ____",
	"option1": "a. didn't",
	"option2": "b. am not",
	"option3": "c. wasn't",
	"option4": "d. weren't",
	"answer": "3"

}, {
	"question": "Excuse me, ____ is the T-shirt? It's ₤25.99.",
	"option1": "a. what expensive",
	"option2": "b. how much",
	"option3": "c. how many",
	"option4": "d. how price",
	"answer": "2"

}, {
	"question": "She's only four but she ____.",
	"option1": "a. can read",
	"option2": "b. cans read",
	"option3": "c. can reads",
	"option4": "d. cans reads",
	"answer": "1"

}, {
	"question": "This party is boring. We ____ a good time.",
	"option1": "a. don't have",
	"option2": "b. aren't having",
	"option3": "c. don't having",
	"option4": "d. aren't have",
	"answer": "2"

}, {
	"question": "Sorry, I ____ you at the moment.",
	"option1": "a. can't help",
	"option2": "b. don't can help",
	"option3": "c. can't helping",
	"option4": "d. can't helps",
	"answer": "1"

}, {
	"question": "I ____ my computer very often.",
	"option1": "a. am not using",
	"option2": "b. don't use",
	"option3": "c. doesn't use",
	"option4": "d. am not use",
	"answer": "2"

}, {
	"question": "It's my mum's birthday next week. I ____ her a present.",
	"option1": "a. buy",
	"option2": "b. buys",
	"option3": "c. am going to buy",
	"option4": "d. buying",
	"answer": "3"

}, {
	"question": "What ____ do after school today?",
	"option1": "a. are you going to",
	"option2": "b. are you",
	"option3": "c. do you",
	"option4": "d. you",
	"answer": "1"

}, {
	"question": "____'s your name? Thomas",
	"option1": "a. How",
	"option2": "b. Who",
	"option3": "c. What",
	"option4": "d. Where",
	"answer": "3"
}, {
	"question": "This is Lucy and her brother, Dan. ____ my friends.",
	"option1": "a. We're",
	"option2": "b. I'm",
	"option3": "c. You're",
	"option4": "d. They're",
	"answer": "4"
}, {
	"question": "____? I'm from Italy.",
	"option1": "a. Where are you from?",
	"option2": "b. Where you are from?",
	"option3": "c. Where from you are?",
	"option4": "d. From where you are?",
	"answer": "1"
}, {
	"question": "I'm from Milan. ____ is in Italy.",
	"option1": "a. They",
	"option2": "b. It",
	"option3": "c. He",
	"option4": "d. She",
	"answer": "2"
}, {
	"question": "Excuse me, how ____ your last name? R-I-L-E-Y",
	"option1": "a. spell",
	"option2": "b. you spell",
	"option3": "c. do you spell",
	"option4": "d. spell you",
	"answer": "3"

}];