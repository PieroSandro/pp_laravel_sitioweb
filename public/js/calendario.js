$(function() {
    var evt = [];
    $.ajax({
        url: "eventos/get",
        type: "GET",
        dataType: "JSON",
        async: false
    }).done(function(r) {
        evt = r;
    });
    // page is now ready, initialize the calendar...
    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay,listDay'
        },
        navLinks: true, // can click day/week names to navigate views
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        events: evt,
        dayClick: function(date, jsEvent, view) {
            $("#fecha_inicio").val(date.format());
            $("#mdlEvent").modal();
        }
    });
    $("#fecha_final").datepicker({
        format: 'yyyy-mm-dd'
    });
});