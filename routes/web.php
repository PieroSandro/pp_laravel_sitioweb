<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Login - Session
Auth::routes();
Route::get('/', function () { return view('auth.bloglogin');});


Route::get('blog/login', 'Auth\LoginController@showLoginForm')->name('bloglogin');
Route::post('blog/login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');





        // Registration Routes...
        Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
        Route::post('register', 'Auth\RegisterController@register');

        // Password Reset Routes...
        Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('password/reset', 'Auth\ResetPasswordController@reset');
    



//RUTAS DE FRONT

/*Route::group(['middleware'=>'web'],function(){
  Route::get('/',function(){
    return view('welcome');
  });
  Route::auth();
  Route::get('/home','HomeController@index');
  Route::get('/blog/login','ClavelController@showloginform');
});*/
Route::resource('audios','AudioController');

Route::resource('patros','PatroController');
Route::resource('events','EventController');
Route::resource('banners','BannerController');

Route::resource('posts','BlogController');
Route::resource('animaciones','AnimacioneController');
Route::resource('anuncios', 'AnuncioController');
Route::resource('clavels','ClavelController');
Route::resource('comentarios','ComentarioController');
Route::resource('respuestas','RespuestaController');
Route::resource('vcursos','VcursoController');
Route::resource('videos','VideoController');
Route::resource('libros','LibroController');
Route::resource('revistas','RevistaController');
Route::resource('mainpages','MainpageController');
Route::resource('basicopreguntas','BasicopreguntaController');
Route::resource('intermediopreguntas','IntermediopreguntaController');
Route::resource('avanzadopreguntas','AvanzadopreguntaController');
Route::resource('/quizresultados','QuizresultadoController');
Route::resource('basicogramaticas','BasicogramaticaController');
Route::resource('intermediogramaticas','IntermediogramaticaController');
Route::resource('avanzadogramaticas','AvanzadogramaticaController');
Route::resource('chatbots','ChatbotController');
Route::resource('frequentepreguntas','FrequentepreguntaController');
Route::resource('mainiconos','MainiconoController');
Route::resource('mainwords','MainwordController');
Route::resource('main2pages','Main2pageController');



Route::get('/index/sesion', 'HomeController@index2')->name('home2');
Route::get('/index', function () {
    return view('front.index');
})->name('principal');
Route::get('/somos', function () {
    return view('front.quienes');
})->name('somos');
Route::get('/mision', function () {
    return view('front.mision');
})->name('mision');
Route::get('/vision', function () {
    return view('front.vision');
})->name('vision');
Route::get('/docentes', function () {
    return view('front.docentes');
})->name('docentes');
Route::get('/suficiencia', function () {
    return view('front.suficiencia');
})->name('suficiencia');
Route::get('/extraordinarios', function () {
    return view('front.extraordinarios');
})->name('extraordinarios');
Route::get('/t_english', function () {
    return view('front.t_english');
})->name('t_english');

Route::get('/videos', function () {
    return view('front.videos');
})->name('videos');
Route::get('/reclamaciones', function () {
    return view('front.reclamaciones');
})->name('reclamaciones');
Route::get('/contactenos', function () {
    return view('front.contactenos');
})->name('contactenos');

//----19_12

//-----26_01_2020

Route::get('/chatbotvista', function () {
  return view('front.chatbotvista');
})->name('chatbotvista');


//------26_01_2020












Route::get('/blog/sesion', 'HomeController@index')->name('home');



//-----------------------------------------------------------------CORREGIR LAS 4 LINEAS DE ABAJO¨***************************
//------------------------------------------------------------------
Route::post('/contactenos','ContactenosController@contactenossend' 
    //return redirect()->route('contactenos')->with('success','Correo enviado!!');
)->name('contactenos.send');

Route::post('/reclamaciones','ReclamacionesController@reclamacionessend' 
    //return redirect()->route('reclamaciones')->with('success','Correo enviado!!');
)->name('reclamaciones.send');

Route::get('/t_english/basico', function () {
    return view('front.quiz.basico');
})->name('basico');
Route::get('/t_english/intermedio', function () {
    return view('front.quiz.intermedio');
})->name('intermedio');
Route::get('/t_english/intermedio_2', function () {
    return view('front.quiz.intermedio_2');
})->name('intermedio_2');

use App\Post;

Route::get('/blog/home', function () {
    
  $posts = Post::orderBy('created_at','DESC')->paginate(10);
  return view('front.blog2.home',compact(
    'posts'));
    
});


Route::get('/blog/administracion', function () {
    
  $posts = Post::orderBy('created_at','DESC')->where('categoria_id',1)->paginate(10);
  return view('front.blog2.c_administracion',compact(
    'posts'));
    
});


Route::get('/blog/computacion', function () {
    
  $posts = Post::orderBy('created_at','DESC')->where('categoria_id',2)->paginate(10);
  return view('front.blog2.c_computacion',compact(
    'posts'));
    
});


Route::get('/blog/contabilidad', function () {
    
  $posts = Post::orderBy('created_at','DESC')->where('categoria_id',3)->paginate(10);
  return view('front.blog2.c_contabilidad',compact(
    'posts'));
    
});


Route::get('/blog/electronica', function () {
    
  $posts = Post::orderBy('created_at','DESC')->where('categoria_id',4)->paginate(10);
  return view('front.blog2.c_electronica',compact(
    'posts'));
    
});


Route::get('/blog/electrotecnia', function () {
    
  $posts = Post::orderBy('created_at','DESC')->where('categoria_id',5)->paginate(10);
  return view('front.blog2.c_electrotecnia',compact(
    'posts'));
    
});


Route::get('/blog/laboratorio', function () {
    
  $posts = Post::orderBy('created_at','DESC')->where('categoria_id',6)->paginate(10);
  return view('front.blog2.c_laboratorio',compact(
    'posts'));
    
});


Route::get('/blog/mautomotriz', function () {
    
  $posts = Post::orderBy('created_at','DESC')->where('categoria_id',7)->paginate(10);
  return view('front.blog2.c_mautomotriz',compact(
    'posts'));
    
});


Route::get('/blog/mproduccion', function () {
    
  $posts = Post::orderBy('created_at','DESC')->where('categoria_id',8)->paginate(10);
  return view('front.blog2.c_mproduccion',compact(
    'posts'));
    
});


Route::get('/blog/metalurgia', function () {
    
  $posts = Post::orderBy('created_at','DESC')->where('categoria_id',9)->paginate(10);
  return view('front.blog2.c_metalurgia',compact(
    'posts'));
    
});

use App\Anuncio;
Route::get('/index/anuncios', function () {
    
  $anuncios = Anuncio::orderBy('created_at','DESC')->paginate(10);
  return view('front.blog2.home_an',compact(
    'anuncios'));
    
});

use App\Banner;
Route::get('/index/banners', function () {
    
  $banners = App\Banner::all();
  return view('front.blog2.homebanner',compact(
    'banners'));
    
})->name('homebanners');
Route::get('/index/banners', 'HomeController@backbannerhome')->name('backbannerhome');


use App\Patro;
Route::get('/index/patros', function () {
    
 //$patros = Patro::orderBy('id','ASC')->paginate();
 $patros = App\Patro::all();
 //$patros = Patro::orderBy('id','ASC');
  return view('front.blog2.homepatro',compact(
    'patros'));
    
})->name('homepatros');

Route::get('/index/patros', 'HomeController@backpatrohome')->name('backpatrohome');

Route::get('/index/patros/{id}/edit', 'HomeController@editpatro')->name('editpatro');
Route::put('index/patros/{id}/edit', 'PatroController@updatepatro')->name('updatepatro');
//---------------------------------------------------
/*Route::get('/index/patros2', function () {
    
 $patros = App\Patro::all();
  return view('front.blog2.homepatro2',compact(
    'patros'));
    
});*/

//----------------------------------------------
/*Route::get('/index/patrocinadores1',function(){
    return view('front.blog2.homepatro');
});
Route::post('/index/patrocinadores','HomeController@homepatros')->name('homepatros');*/

Route::get('/blog/home','BlogController@bloghome')->name('bloghome');
Route::get('/blog/administracion','BlogController@blogadministracion')->name('blogadministracion');
Route::get('/blog/computacion','BlogController@blogcomputacion')->name('blogcomputacion');
Route::get('/blog/contabilidad','BlogController@blogcontabilidad')->name('blogcontabilidad');
Route::get('/blog/electronica','BlogController@blogelectronica')->name('blogelectronica');
Route::get('/blog/electrotecnia','BlogController@blogelectrotecnia')->name('blogelectrotecnia');
Route::get('/blog/laboratorio','BlogController@bloglaboratorio')->name('bloglaboratorio');
Route::get('/blog/mautomotriz','BlogController@blogmautomotriz')->name('blogmautomotriz');
Route::get('/blog/metalurgia','BlogController@blogmetalurgia')->name('blogmetalurgia');
Route::get('/blog/mproduccion','BlogController@blogmproduccion')->name('blogmproduccion');

Route::get('/blog/podcast_basico','BlogController@podcastbasico')->name('podcastbasico');
Route::get('/blog/podcast_intermedio','BlogController@podcastintermedio')->name('podcastintermedio');
Route::get('/blog/podcast_avanzado','BlogController@podcastavanzado')->name('podcastavanzado');

Route::get('/blog/basic_grammar','BlogController@n_basico')->name('n_basico');
use App\Basicogramatica;

//----------------------------
Route::get('/blog/basic_grammar', function () {
  //$basicogramaticas = Basicogramatica::orderBy('created_at','DESC')->paginate(8);
  //$mainpages = App\Mainpage::all();
 $basicogramaticas = Basicogramatica::orderBy('id','ASC')->paginate(10);
 //$basicogramaticas = Basicogramatica::all()->paginate(10);
return view('front.n_basico',compact(
  'basicogramaticas'));
})->name('n_basico');
//----------------------------
Route::get('/blog/preguntas_frecuentes','BlogController@p_frecuentes')->name('p_frecuentes');
use App\Frequentepregunta;

//----------------------------
Route::get('/blog/preguntas_frecuentes', function () {
  //$basicogramaticas = Basicogramatica::orderBy('created_at','DESC')->paginate(8);
  //$mainpages = App\Mainpage::all();
 $frequentepreguntas = Frequentepregunta::orderBy('id','ASC')->paginate(10);
 //$basicogramaticas = Basicogramatica::all()->paginate(10);
return view('front.p_frecuentes',compact(
  'frequentepreguntas'));
})->name('p_frecuentes');

//----------------------------

Route::get('/blog/intermediate_grammar','BlogController@n_intermedio')->name('n_intermedio');
use App\Intermediogramatica;

//----------------------------
Route::get('/blog/intermediate_grammar', function () {
  //$basicogramaticas = Basicogramatica::orderBy('created_at','DESC')->paginate(8);
  //$mainpages = App\Mainpage::all();
 $intermediogramaticas = Intermediogramatica::orderBy('id','ASC')->paginate(10);
 //$basicogramaticas = Basicogramatica::all()->paginate(10);
return view('front.n_intermedio',compact(
  'intermediogramaticas'));
})->name('n_intermedio');
//----------------------------
Route::get('/blog/advanced_grammar','BlogController@n_avanzado')->name('n_avanzado');
use App\Avanzadogramatica;

//----------------------------
Route::get('/blog/advanced_grammar', function () {
  //$basicogramaticas = Basicogramatica::orderBy('created_at','DESC')->paginate(8);
  //$mainpages = App\Mainpage::all();
 $avanzadogramaticas = Avanzadogramatica::orderBy('id','ASC')->paginate(10);
 //$basicogramaticas = Basicogramatica::all()->paginate(10);
return view('front.n_avanzado',compact(
  'avanzadogramaticas'));
})->name('n_avanzado');
//----------------------------

Route::get('/blog/all_actividades','BlogController@all_actividades')->name('all_actividades');
Route::get('/blog/musica','BlogController@musica')->name('musica');
Route::get('/blog/danza','BlogController@danza')->name('danza');
Route::get('/blog/teatro','BlogController@teatro')->name('teatro');


Route::get('/blog/v_estudiantil','BlogController@v_estudiantil')->name('v_estudiantil');

//-----------------------------------------------------------------------------------



//-----------------------------------------------------------------------------------
//front.blog2
//------------------------------------------------------------------------
//Route::post('/blog/home/{id}', ['uses' => 'RespuestaController@store']);
//Route::post('/blog/home/{id}','RespuestaController@store')->name('storerespuesta');




//-----------------------------------------------------CHRISTIAN---------------------------------------------------
Route::post('/blog/home/respuesta/{post_id}/{comentario_id}',['uses' => 'ComentarioController@store'])->name('storerespuesta');
//-----------------------------------------------------CHRISTIAN---------------------------------------------------




//Route::post('/blog/home/{comentario_id}','ComentarioController@store')->name('storerespuesta');

Route::post('/blog/home/{id}','BlogController@storecoment')->name('storecoment');

Route::get('/blog/home/{id}', 'BlogController@show')->name('blogid');











//Route::get('/index/anuncios/{id}', 'AnuncioController@show_an')->name('anuncioid');
Route::get('/index/anuncios/{id}', 'HomeController@show_an')->name('anuncioid');

//Route::get('/index/anuncios/{id}/edit', 'AnuncioController@edit_an')->name('editanuncio');
Route::get('/index/anuncios/{id}/edit', 'HomeController@edit_an')->name('editanuncio');













//Route::get('/blog/home/{id}/edit', 'BlogController@edit')->name('editpost');
Route::get('/blog/home/{id}/edit', 'HomeController@edit')->name('editpost');

//-----------------rutas nuevas---front
//Route::get('/blog/home/{id}', 'ComentarioController@destroy_co')->name('destroy_co');
Route::delete('/blog/home/{id}', 'BlogController@destroy')->name('destroy');
Route::delete('/index/banners/{id}', 'BannerController@destroybanner')->name('destroybanner');
Route::post('/index/banners', 'BannerController@destroybannermore')->name('destroybannermore');

//----------Route::post('/ajax/comentario', 'BlogController@storecoment')->name('ajax.blog');
Route::delete('/index/anuncios/{id}', 'AnuncioController@destroy_an')->name('destroy_an');

//---------------------------------------------------------------------------------------------------Route::get('/blog/home', 'HomeController@backbloghome')->name('backbloghome');

Route::delete('/blog/home/{post_id}/{id}', 'ComentarioController@destroy')->name('destroy_co');

Route::delete('/blog/home/{post_id}/{comentario_id}/{id}', 'RespuestaController@destroy')->name('destroy_res');



//-----------------------------------------------------------------------
Route::get('/index/anuncios', 'HomeController@backanunciohome')->name('backanunciohome');
Route::put('index/anuncios/{id}/edit', 'AnuncioController@update_an')->name('update_an');
//Route::put('/blog/home', 'BlogController@update')->name('posts.update');

Route::put('blog/home/{id}/edit', 'BlogController@update')->name('update');

//Route::get('/blog/home', 'HomeController@bloghome')->name('bloghome');
//--------------------------------------------------------------

//Route::get('/index/anuncios/{id}/edit', 'HomeController@edit_an');

Route::get('/blog_login', 'HomeController@bloglogin');
Route::get('/blog_register', 'HomeController@blogregister');

Route::get('/blog/create','HomeController@create');
Route::get('/index/create','HomeController@create_an');
Route::get('/index/createbanner','HomeController@createbanner');

//Route::get('/index/create','HomeController@create_an')->name('create_an');
Route::post('/index/create','HomeController@store2' 
    
)->name('create_an');
Route::post('/blog/create','HomeController@store' 
    
)->name('create');
Route::post('/index/createbanner','HomeController@storebanner' 
    
)->name('createbanner');
Route::post('/blog_store','HomeController@storeuser')->name('storeuser');
Route::post('/blog_loger','HomeController@loger')->name('loger');


Route::get('/index/banners/{id}/edit', 'HomeController@editbanner')->name('editbanner');
Route::put('index/banners/{id}/edit', 'BannerController@updatebanner')->name('updatebanner');













//------------------------------------------------------------------
Route::get('/index/createevent','HomeController@createevent');
Route::post('/index/createevent','HomeController@storeevent' 
    
)->name('createevent');
Route::put('index/events/{id}/edit', 'EventController@updateevent')->name('updateevent');

//---14-2-2020
Route::get('/index/createmainicono','HomeController@createmainicono');
Route::post('/index/createmainicono','HomeController@storemainicono' 
    
)->name('createmainicono');
Route::put('index/mainiconos/{id}/edit', 'MainiconoController@updatemainicono')->name('updatemainicono');


//---14-2-2020

//---15-2-2020
Route::get('/index/createmainword','HomeController@createmainword');
Route::post('/index/createmainword','HomeController@storemainword' 
    
)->name('createmainword');

//--15-2-2020
//Route::put('index/vcursos/{id}/edit', 'VcursoController@updatevcurso')->name('updatevcurso');

Route::get('/index/createpatro','HomeController@createpatro');
Route::post('/index/createpatro','HomeController@storepatro' 
    
)->name('createpatro');

Route::get('/index/patros/{id}/edit', 'HomeController@editpatro')->name('editpatro');



Route::post('index/patros/create', 'HomeController@storepatros')->name('patros.create');
//------------------------------------------------------------------
Route::get('/blog/administracion_basico','BlogController@it_administracion1')->name('administracion_basico');
Route::get('/blog/administracion_intermedio','BlogController@it_administracion2')->name('administracion_intermedio');
Route::get('/blog/administracion_avanzado','BlogController@it_administracion3')->name('administracion_avanzado');

Route::get('/blog/computacion_basico','BlogController@it_computacion1')->name('computacion_basico');
Route::get('/blog/computacion_intermedio','BlogController@it_computacion2')->name('computacion_intermedio');
Route::get('/blog/computacion_avanzado','BlogController@it_computacion3')->name('computacion_avanzado');

Route::get('/blog/contabilidad_basico','BlogController@it_contabilidad1')->name('contabilidad_basico');
Route::get('/blog/contabilidad_intermedio','BlogController@it_contabilidad2')->name('contabilidad_intermedio');
Route::get('/blog/contabilidad_avanzado','BlogController@it_contabilidad3')->name('contabilidad_avanzado');

Route::get('/blog/electronica_basico','BlogController@it_electronica1')->name('electronica_basico');
Route::get('/blog/electronica_intermedio','BlogController@it_electronica2')->name('electronica_intermedio');
Route::get('/blog/electronica_avanzado','BlogController@it_electronica3')->name('electronica_avanzado');

Route::get('/blog/electrotecnia_basico','BlogController@it_electrotecnia1')->name('electrotecnia_basico');
Route::get('/blog/electrotecnia_intermedio','BlogController@it_electrotecnia2')->name('electrotecnia_intermedio');
Route::get('/blog/electrotecnia_avanzado','BlogController@it_electrotecnia3')->name('electrotecnia_avanzado');

Route::get('/blog/laboratorio_basico','BlogController@it_laboratorio1')->name('laboratorio_basico');
Route::get('/blog/laboratorio_intermedio','BlogController@it_laboratorio2')->name('laboratorio_intermedio');
Route::get('/blog/laboratorio_avanzado','BlogController@it_laboratorio3')->name('laboratorio_avanzado');

Route::get('/blog/mautomotriz_basico','BlogController@it_mautomotriz1')->name('mautomotriz_basico');
Route::get('/blog/mautomotriz_intermedio','BlogController@it_mautomotriz2')->name('mautomotriz_intermedio');
Route::get('/blog/mautomotriz_avanzado','BlogController@it_mautomotriz3')->name('mautomotriz_avanzado');

Route::get('/blog/metalurgia_basico','BlogController@it_metalurgia1')->name('metalurgia_basico');
Route::get('/blog/metalurgia_intermedio','BlogController@it_metalurgia2')->name('metalurgia_intermedio');
Route::get('/blog/metalurgia_avanzado','BlogController@it_metalurgia3')->name('metalurgia_avanzado');

Route::get('/blog/mproduccion_basico','BlogController@it_mproduccion1')->name('mproduccion_basico');
Route::get('/blog/mproduccion_intermedio','BlogController@it_mproduccion2')->name('mproduccion_intermedio');
Route::get('/blog/mproduccion_avanzado','BlogController@it_mproduccion3')->name('mproduccion_avanzado');

//---------------------------------------------------------------------------------------

//Route::controller('')

Route::post('index/vcursos','VcursoController@store');
Route::put('index/vcursos/{id}','VcursoController@update');
Route::delete('index/vcursos/{id}','VcursoController@delete');


//--------------------------------------------------------------------------
//--25-agosto



//---------------------------------------------------------------------------
use App\Vcurso;
Route::get('/index/vcursos', function () {
    
  $vcursos = App\Vcurso::all();
  return view('front.blog2.homevcurso',compact(
    'vcursos'));
    
})->name('homevcursos');














//-------------------------------------------------
Route::get('/cursosonline', function () {
    $vcursos = Vcurso::orderBy('created_at','DESC')->paginate(5);
  return view('front.cursosonline',compact(
    'vcursos'));
})->name('cursosonline');

use App\Revista;
Route::get('/revistas', function () {
  $revistas = Revista::orderBy('created_at','DESC')->paginate(5);
return view('front.revistas',compact(
  'revistas'));
})->name('revistas');

Route::get('/index/createvcurso','HomeController@createvcurso');

Route::post('/index/createvcurso','HomeController@storevcurso' 
    
)->name('createvcurso');

Route::get('/index/vcursos/{id}/edit', 'HomeController@editvcurso')->name('editvcurso');
Route::put('index/vcursos/{id}/edit', 'VcursoController@updatevcurso')->name('updatevcurso');
Route::get('/index/vcursos', 'HomeController@backvcursohome')->name('backvcursohome');

Route::delete('/index/vcursos/{id}', 'VcursoController@destroyvcurso')->name('destroyvcurso');

Route::post('/index/vcursos', 'VcursoController@destroyvcursomore')->name('destroyvcursomore');
//---------------------------------------
use App\Event;
Route::get('/index/events', function () {
    
  $events = App\Event::all();
  return view('front.blog2.homeevent',compact(
    'events'));
    
})->name('homeevents');

Route::get('/index/events', 'HomeController@backeventhome')->name('backeventhome');

Route::get('/index/events/{id}/edit', 'HomeController@editevent')->name('editevent');
Route::put('index/events/{id}/edit', 'EventController@updateevent')->name('updateevent');
Route::delete('/index/events/{id}', 'EventController@destroyevent')->name('destroyevent');

Route::post('/index/events', 'EventController@destroyeventmore')->name('destroyeventmore');

//---------------------------------------------------------------------------------------

//---------------------------------------
use App\Mainicono;
Route::get('/index/mainiconos', function () {
    
  $mainiconos = App\Mainicono::all();
  return view('front.blog2.homemainicono',compact(
    'mainiconos'));
    
})->name('homemainiconos');

Route::get('/index/mainiconos', 'HomeController@backmainiconohome')->name('backmainiconohome');

Route::get('/index/mainiconos/{id}/edit', 'HomeController@editmainicono')->name('editmainicono');
Route::put('index/mainiconos/{id}/edit', 'MainiconoController@updatemainicono')->name('updatemainicono');
Route::delete('/index/mainiconos/{id}', 'MainiconoController@destroymainicono')->name('destroymainicono');

Route::post('/index/mainiconos', 'MainiconoController@destroymainiconomore')->name('destroymainiconomore');

//---------------------------------------------------------------------------------------

use App\Mainpage;
Route::get('/index/mainpages', function () {
    
  $mainpages = App\Mainpage::all();
  return view('front.blog2.homemainpage',compact(
    'mainpages'));
    
})->name('homemainpages');

Route::get('/index/mainpages', 'HomeController@backmainpagehome')->name('backmainpagehome');
Route::get('/index/mainpages/{id}/edit', 'HomeController@editmainpage')->name('editmainpage');
Route::put('index/mainpages/{id}/edit', 'MainpageController@updatemainpage')->name('updatemainpage');

//---16-2-2020

use App\Main2page;
Route::get('/index/main2pages', function () {
    
  $main2pages = App\Main2page::all();
  return view('front.blog2.homemain2page',compact(
    'main2pages'));
    
})->name('homemain2pages');

Route::get('/index/main2pages', 'HomeController@backmain2pagehome')->name('backmain2pagehome');


Route::get('/index/main2pages/{id}/edit', 'HomeController@editmain2page')->name('editmain2page');


Route::put('index/main2pages/{id}/edit', 'Main2pageController@updatemain2page')->name('updatemain2page');

//---16-2-2020
//---------------------------------------------------------------------------------------
use App\Efecto;
use App\Animacione;
//----ANIMACIONES-------------------------------------------------------------------------------
Route::get('/index/createanima','HomeController@createanima');
Route::put('index/animaciones/{id}/edit', 'AnimacioneController@updateanima')->name('updateanima');
Route::post('/index/createanima','HomeController@storeanima'     
)->name('createanima');
Route::get('/index/animaciones', function () {
    
    $efectos=Efecto::all();
    $animaciones = Animacione::with(array('efecto'))->get();
    $animaciones = Animacione::orderBy('id','ASC')->paginate();
    return view('front.blog2.homeanima',compact(
      'animaciones','efectos'));
      
  })->name('homeanimas');

//--------------------------VIDEO---------------------------------------------
use App\Video;
Route::get('/index/videos', function () {
    
  //$videos = Video::orderBy('id','ASC')->paginate();
  $videos=App\Video::all();
  return view('front.blog2.homevideo',compact(
    'videos'));
    
})->name('homevideos');
Route::get('/index/videos', 'HomeController@backvideohome')->name('backvideohome');



Route::get('/videos', function () {
    $videos = Video::orderBy('created_at','DESC')->paginate(8);
  return view('front.videos',compact(
    'videos'));
})->name('videos');
//---21octubre-----------------------------------------------------------
Route::get('/index/createvideo','HomeController@createvideo');
Route::post('/index/createvideo','HomeController@storevideo' 
    
)->name('createvideo');
Route::put('index/videos/{id}/edit', 'VideoController@updatevideo')->name('updatevideo');
Route::delete('/index/videos/{id}', 'VideoController@destroyvideo')->name('destroyvideo');
Route::post('/index/videos', 'VideoController@destroyvideomore')->name('destroyvideomore');

//-----27octubre-----LIBROS-------------------------------------------------------------
use App\Libro;
Route::get('/index/libros', function () {
    
  $libros =App\Libro::all();
  return view('front.blog2.homelibro',compact(
    'libros'));
    
})->name('homelibros');

Route::get('/biblioteca', function () {
  $libros = Libro::orderBy('created_at','DESC')->paginate(5);
return view('front.libros',compact(
  'libros'));
})->name('libros');

Route::get('/index/libros','HomeController@backlibrohome')->name('backlibrohome');


Route::get('/index/createlibro','HomeController@createlibro');

Route::post('/index/createlibro','HomeController@storelibro' 
    
)->name('createlibro');
Route::put('index/libros/{id}/edit', 'LibroController@updatelibro')->name('updatelibro');
Route::delete('/index/libros/{id}', 'LibroController@destroylibro')->name('destroylibro');
Route::post('/index/libros', 'LibroController@destroylibromore')->name('destroylibromore');

//--------------REVISTAS---------------------------------------------------------------------

Route::get('/index/revistas', function () {
    
    $revistas =Revista::orderBy('id','ASC')->paginate();
    return view('front.blog2.homerevista',compact(
      'revistas'));
      
  })->name('homerevistas');

  Route::get('/index/revistas','HomeController@backrevistahome')->name('backrevistahome');

Route::get('/index/createrevista','HomeController@createrevista');

Route::post('/index/createrevista','HomeController@storerevista'  
)->name('createrevista');
Route::put('index/revistas/{id}/edit', 'RevistaController@updaterevista')->name('updaterevista');
Route::delete('/index/revistas/{id}', 'RevistaController@destroyrevista')->name('destroyrevista');
Route::post('/index/revistas', 'RevistaController@destroyrevistamore')->name('destroyrevistamore');
//-------------------AUDIOS----------------------------------------------------------------------
use App\Audio;
Route::get('/index/audios', function () {
    
    $audios =App\Audio::all();
    return view('front.blog2.homeaudio',compact(
      'audios'));
      
  })->name('homeaudios');

Route::get('/index/createaudio','HomeController@createaudio');

Route::post('/index/createaudio','HomeController@storeaudio' 
    
)->name('createaudio');
Route::put('index/audios/{id}/edit', 'AudioController@updateaudio')->name('updateaudio');
Route::delete('/index/audios/{id}', 'AudioController@destroyaudio')->name('destroyaudio');
Route::post('/index/audios', 'AudioController@destroyaudiomore')->name('destroyaudiomore');
Route::get('/index/audios', 'HomeController@backaudiohome')->name('backaudiohome');




Route::get('/blog/podcast_basico', function () {
  $audios = Audio::orderBy('created_at','DESC')->paginate(10);
return view('front.blog2.podcast_basico',compact(
  'audios'));
})->name('podcastbasico');

Route::get('/blog/podcast_intermedio', function () {
  $audios = Audio::orderBy('created_at','DESC')->paginate(10);
return view('front.blog2.podcast_intermedio',compact(
  'audios'));
})->name('podcastintermedio');


Route::get('/blog/podcast_avanzado', function () {
  $audios = Audio::orderBy('created_at','DESC')->paginate(10);
return view('front.blog2.podcast_avanzado',compact(
  'audios'));
})->name('podcastavanzado');













Route::get('/blog/homeprueba', function () {
    
  $posts = Post::orderBy('created_at','DESC')->paginate(10);
  return view('front.blog2.home_prueba',compact(
    'posts'));
    
});
Route::get('/blog/homeprueba','HomeController@bloghome')->name('bloghome2');


//------------------23_12------------------------------------------------------------
Route::get('/index/createmainpage','HomeController@createmainpage');

Route::post('/index/createmainpage','HomeController@storemainpage' 
    
)->name('createmainpage');
//-----------------------------------------------------------------------------------
//-----------------------22_01_2020------------------------------------------------------------

//16-2-2020

Route::get('/index/createmain2page','HomeController@createmain2page');

Route::post('/index/createmain2page','HomeController@storemain2page' 
    
)->name('createmain2page');
//16-2-2020
use App\Basicopregunta;
Route::get('/index/basicopreguntas', function () {
    
    $basicopreguntas =App\Basicopregunta::all();
    return view('front.blog2.homebasicopregunta',compact(
      'basicopreguntas'));
      
  })->name('homebasicopreguntas');

Route::get('/index/createbasicopregunta','HomeController@createbasicopregunta');

Route::post('/index/createbasicopregunta','HomeController@storebasicopregunta' 
    
)->name('createbasicopregunta');
Route::put('index/basicopreguntas/{id}/edit', 'BasicopreguntaController@updatebasicopregunta')->name('updatebasicopregunta');
Route::delete('/index/basicopreguntas/{id}', 'BasicopreguntaController@destroybasicopregunta')->name('destroybasicopregunta');
Route::post('/index/basicopreguntas', 'BasicopreguntaController@destroybasicopreguntamore')->name('destroybasicopreguntamore');
Route::get('/index/basicopreguntas', 'HomeController@backbasicopreguntahome')->name('backbasicopreguntahome');

//-----------------------22_01_2020------------------------------------------------------------
//----------------23_01_2020------------------------------------------------------------


use App\Intermediopregunta;
use App\Quizresultado;

use App\Avanzadopregunta;


Route::get('/index/intermediopreguntas', function () {
 
    $intermediopreguntas =App\Intermediopregunta::all();

    return view('front.blog2.homeintermediopregunta',compact('intermediopreguntas'));
    
  })->name('homeintermediopreguntas');


Route::get('/index/createintermediopregunta','HomeController@createintermediopregunta');

Route::post('/index/createintermediopregunta','HomeController@storeintermediopregunta' 
    
)->name('createintermediopregunta');
Route::put('index/intermediopreguntas/{id}/edit', 'IntermediopreguntaController@updateintermediopregunta')->name('updateintermediopregunta');


Route::put('/index/intermediopreguntas/{quizresultado}/update', 'QuizresultadoController@updatequizresultado')->name('quizresultados2.update');

Route::delete('/index/intermediopreguntas/{id}', 'IntermediopreguntaController@destroyintermediopregunta')->name('destroyintermediopregunta');
Route::post('/index/intermediopreguntas', 'IntermediopreguntaController@destroyintermediopreguntamore')->name('destroyintermediopreguntamore');
Route::get('/index/intermediopreguntas', 'HomeController@backintermediopreguntahome')->name('backintermediopreguntahome');




Route::get('/index/avanzadopreguntas', function () {
 
  $avanzadopreguntas =App\Avanzadopregunta::all();

  return view('front.blog2.homeavanzadopregunta',compact('avanzadopreguntas'));
  
})->name('homeavanzadopreguntas');


Route::get('/index/createavanzadopregunta','HomeController@createavanzadopregunta');

Route::post('/index/createavanzadopregunta','HomeController@storeavanzadopregunta' 
  
)->name('createavanzadopregunta');
Route::put('index/avanzadopreguntas/{id}/edit', 'AvanzadopreguntaController@updateavanzadopregunta')->name('updateavanzadopregunta');


Route::put('/index/avanzadopreguntas/{quizresultado}/update', 'QuizresultadoController@updatequizresultado3')->name('quizresultados3.update');

Route::delete('/index/avanzadopreguntas/{id}', 'AvanzadopreguntaController@destroyavanzadopregunta')->name('destroyavanzadopregunta');
Route::post('/index/avanzadopreguntas', 'AvanzadopreguntaController@destroyavanzadopreguntamore')->name('destroyavanzadopreguntamore');
Route::get('/index/avanzadopreguntas', 'HomeController@backavanzadopreguntahome')->name('backavanzadopreguntahome');


//---------------23_01_2020------------------------------------------------------------

//---------------26_01_2020------------------------------------------------------------


Route::get('/index/chatbots', function () {
 
  $chatbots =App\Chatbot::all();

  return view('front.blog2.homechatbot',compact('chatbots'));
  
})->name('homechatbots');


Route::get('/index/createchatbot','HomeController@createchatbot');

Route::post('/index/createchatbot','HomeController@storechatbot' 
  
)->name('createchatbot');
Route::put('index/chatbots/{id}/edit', 'ChatbotController@updatechatbot')->name('updatechatbot');

//Route::put('/index/avanzadopreguntas/{quizresultado}/update', 'QuizresultadoController@updatequizresultado3')->name('quizresultados3.update');

Route::delete('/index/chatbots/{id}', 'ChatbotController@destroychatbot')->name('destroychatbot');
Route::post('/index/chatbots', 'ChatbotController@destroychatbotmore')->name('destroychatbotmore');
Route::get('/index/chatbots', 'HomeController@backchatbothome')->name('backchatbothome');

//-----

Route::get('/index/frequentepreguntas', function () {
 
  $frequentepreguntas =App\Frequentepregunta::all();

  return view('front.blog2.homefrequentepregunta',compact('frequentepreguntas'));
  
})->name('homefrequentepreguntas');


Route::get('/index/createfrequentepregunta','HomeController@createfrequentepregunta');

Route::post('/index/createfrequentepregunta','HomeController@storefrequentepregunta' 
  
)->name('createfrequentepregunta');
Route::put('index/frequentepreguntas/{id}/edit', 'FrequentepreguntaController@updatefrequentepregunta')->name('updatefrequentepregunta');

//Route::put('/index/avanzadopreguntas/{quizresultado}/update', 'QuizresultadoController@updatequizresultado3')->name('quizresultados3.update');

Route::delete('/index/frequentepreguntas/{id}', 'FrequentepreguntaController@destroyfrequentepregunta')->name('destroyfrequentepregunta');
Route::post('/index/frequentepreguntas', 'FrequentepreguntaController@destroyfrequentepreguntamore')->name('destroyfrequentepreguntamore');
Route::get('/index/frequentepreguntas', 'HomeController@backfrequentepreguntahome')->name('backfrequentepreguntahome');
//----------------26_01_2020--------------------------------------------------------


//15-2-2020
use App\Mainword;

Route::get('/index/mainwords', function () {
 
  $mainwords =App\Mainword::all();

  return view('front.blog2.homemainword',compact('mainwords'));
  
})->name('homemainwords');

Route::put('index/mainwords/{id}/edit', 'MainwordController@updatemainword')->name('updatemainword');

//15-2-2020
//-----------------------CHANGETEXT------------------------------------------


Route::get('/index', function () {
  $main2pages=Main2page::orderBy('created_at','DESC')->where('id',1)->paginate();
return view('front.index',compact(
  'main2pages'));
})->name('index');

Route::get('/somos', function () {
  $main2pages=Main2page::orderBy('created_at','DESC')->where('id',2)->paginate();
return view('front.quienes',compact(
  'main2pages'));
})->name('quienes');


Route::get('/mision', function () {
  $main2pages=Main2page::orderBy('created_at','DESC')->where('id',3)->paginate();
return view('front.mision',compact(
  'main2pages'));
})->name('mision');


Route::get('/vision', function () {
  $main2pages=Main2page::orderBy('created_at','DESC')->where('id',4)->paginate();
return view('front.vision',compact(
  'main2pages'));
})->name('vision');

Route::get('/docentes', function () {
  $main2pages=Main2page::orderBy('created_at','DESC')->where('id',5)->paginate();
return view('front.docentes',compact(
  'main2pages'));
})->name('docentes');

Route::get('/suficiencia', function () {
  $main2pages=Main2page::orderBy('created_at','DESC')->where('id',6)->paginate();
return view('front.suficiencia',compact(
  'main2pages'));
})->name('suficiencia');

Route::get('/extraordinarios', function () {
  $main2pages=Main2page::orderBy('created_at','DESC')->where('id',7)->paginate();
return view('front.extraordinarios',compact(
  'main2pages'));
})->name('extraordinarios');

Route::get('/t_english', function () {
  $main2pages=Main2page::orderBy('created_at','DESC')->where('id',8)->paginate();
return view('front.t_english',compact(
  'main2pages'));
})->name('t_english');


//---------------------------------------------------------------
//Route::get('/blog/danza','BlogController@danza')->name('danza');


Route::get('/blog/danza', function () {
  $main2pages=Main2page::orderBy('created_at','DESC')->where('id',9)->paginate();
return view('front.danza',compact(
  'main2pages'));
})->name('danza');


Route::get('/blog/musica', function () {
  $main2pages=Main2page::orderBy('created_at','DESC')->where('id',10)->paginate();
return view('front.musica',compact(
  'main2pages'));
})->name('musica');


Route::get('/blog/teatro', function () {
  $main2pages=Main2page::orderBy('created_at','DESC')->where('id',11)->paginate();
return view('front.teatro',compact(
  'main2pages'));
})->name('teatro');

//------------------------CHANGETEXT---------------------------------------
//----------------------------------------------------------------
//---------------------------------------------------------------
//---------------------------------------------------------------
//----------------------------------------------------------------
//31-01----------------------------------------------------------------------

//use App\Basicogramatica;
Route::get('/index/createbasicogramatica','HomeController@createbasicogramatica');

Route::post('/index/createbasicogramatica','HomeController@storebasicogramatica' 
    
)->name('createbasicogramatica');


Route::get('/index/basicogramaticas', function () {
    
  $basicogramaticas = App\Basicogramatica::all();
  return view('front.blog2.homebasicogramatica',compact(
    'basicogramaticas'));
    
})->name('homebasicogramaticas');
Route::get('/index/basicogramaticas', 'HomeController@backbasicogramaticahome')->name('backbasicogramaticahome');


Route::get('/index/basicogramaticas/{id}/edit', 'HomeController@editbasicogramatica')->name('editbasicogramatica');
Route::put('index/basicogramaticas/{id}/edit', 'BasicogramaticaController@updatebasicogramatica')->name('updatebasicogramatica');
Route::delete('/index/basicogramaticas/{id}', 'BasicogramaticaController@destroybasicogramatica')->name('destroybasicogramatica');
Route::post('/index/basicogramaticas', 'BasicogramaticaController@destroybasicogramaticamore')->name('destroybasicogramaticamore');

//---------------------------------------------------------------------------------

//use App\Intermediogramatica;
Route::get('/index/createintermediogramatica','HomeController@createintermediogramatica');

Route::post('/index/createintermediogramatica','HomeController@storeintermediogramatica' 
    
)->name('createintermediogramatica');


Route::get('/index/intermediogramaticas', function () {
    
  $intermediogramaticas = App\Intermediogramatica::all();
  return view('front.blog2.homeintermediogramatica',compact(
    'intermediogramaticas'));
    
})->name('homeintermediogramaticas');
Route::get('/index/intermediogramaticas', 'HomeController@backintermediogramaticahome')->name('backintermediogramaticahome');


Route::get('/index/intermediogramaticas/{id}/edit', 'HomeController@editintermediogramatica')->name('editintermediogramatica');
Route::put('index/intermediogramaticas/{id}/edit', 'IntermediogramaticaController@updateintermediogramatica')->name('updateintermediogramatica');
Route::delete('/index/intermediogramaticas/{id}', 'IntermediogramaticaController@destroyintermediogramatica')->name('destroyintermediogramatica');
Route::post('/index/intermediogramaticas', 'IntermediogramaticaController@destroyintermediogramaticamore')->name('destroyintermediogramaticamore');
//-------------------------------------------------------------------------------------------------------

//use App\Avanzadogramatica;
Route::get('/index/createavanzadogramatica','HomeController@createavanzadogramatica');

Route::post('/index/createavanzadogramatica','HomeController@storeavanzadogramatica' 
    
)->name('createavanzadogramatica');


Route::get('/index/avanzadogramaticas', function () {
    
  $avanzadogramaticas = App\Avanzadogramatica::all();
  return view('front.blog2.homeavanzadogramatica',compact(
    'avanzadogramaticas'));
    
})->name('homeavanzadogramaticas');
Route::get('/index/avanzadogramaticas', 'HomeController@backavanzadogramaticahome')->name('backavanzadogramaticahome');


Route::get('/index/avanzadogramaticas/{id}/edit', 'HomeController@editavanzadogramatica')->name('editavanzadogramatica');
Route::put('index/avanzadogramaticas/{id}/edit', 'AvanzadogramaticaController@updateavanzadogramatica')->name('updateavanzadogramatica');
Route::delete('/index/avanzadogramaticas/{id}', 'AvanzadogramaticaController@destroyavanzadogramatica')->name('destroyavanzadogramatica');
Route::post('/index/avanzadogramaticas', 'AvanzadogramaticaController@destroyavanzadogramaticamore')->name('destroyavanzadogramaticamore');




//31-01----------------------------------------------------------------------

//------------------------------------------------------------
//-------------------------------------------------------------
//-------------------------------------------------------------
//-------------------------------------------------------------
Route::post('ckeditor/uploads', 'CKEditorController@upload')->name('upload');
Route::get('locale/{locale}',function ($locale){
    Session::put('locale',$locale);
    
    return redirect()->back();
})->middleware('web')->name('change_lang');

