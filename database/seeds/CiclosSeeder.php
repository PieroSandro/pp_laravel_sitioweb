<?php

use Illuminate\Database\Seeder;
use App\Ciclo;

class CiclosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ciclo::create([
        	"id" => 1,
        	// "user_id" => 1,
        	// "carrera_id" => 4,
        	// "curso_id" => 1,
        	"nombre" => "III"
        ]);

        Ciclo::create([
        	"id" => 2,
        	// "user_id" => 2,
        	// "carrera_id" => 5,
        	// "curso_id" => 1,
        	"nombre" => "IV"
        ]);
    }
}
