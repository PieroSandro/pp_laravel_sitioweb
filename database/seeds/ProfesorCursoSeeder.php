<?php

use Illuminate\Database\Seeder;
use App\ProfesorCurso;

class ProfesorCursoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProfesorCurso::create([
        	'persona_id' => 4,
        	'curso_id' => 1,
        ]);
    }
}
