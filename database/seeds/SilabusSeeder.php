<?php

use Illuminate\Database\Seeder;
use App\Silabus;

class SilabusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Silabus::create([
        	"id" => 1,
        	"carrera_id" => 5,
        	"curso_id" => 1,
        	"ciclo_id" => 2,
        	"periodo_id" => 1,
        	"nombre" => "wwww",
            "nombre_original" => "ESQUEMA-DE-PLAN-DE-EDUCACIÓN-AMBIENTAL.docx",
        	"archivo" => "1551925288ESQUEMA-DE-PLAN-DE-EDUCACIÓN-AMBIENTAL.docx"
        ]);
    }
}
