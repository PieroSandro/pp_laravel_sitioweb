<?php

use Illuminate\Database\Seeder;
use App\Turno;

class TurnosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Turno::create([
        	"nombre" => "Mañana",
        ]);

        Turno::create([
        	"nombre" => "Noche",
        ]);
    }
}
