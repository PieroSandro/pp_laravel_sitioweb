<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	"id" => 1,
         //    "cod_alu" => "ALU0001",
        	// "rol_id" => 1,
        	// "name" => "christian",
        	// "surname" => "diaz",
            "persona_id" => 1,
        	"email" => "brayan@gmail.com",
        	"password" => bcrypt("12345"),
        	// "dni" => "75008475",
        	// "foto" => "hola.jpg",
        	// "estado" => 1,

        ]);

        User::create([
        	"id" => 2,
         //    "cod_alu" => null,
        	// "rol_id" => 2,
        	// "name" => "Juan",
        	// "surname" => "perez",
            "persona_id" => 2,
        	"email" => "juan@gmail.com",
        	"password" => bcrypt("12345"),      	
        	// "dni" => "78002478",
        	// "foto" => "hola.jpg",
        	// "estado" => 1,
        	
        ]);

        User::create([
            "id" => 3,
            // "cod_alu" => null,
            // "rol_id" => 3,
            // "name" => "Lucas",
            // "surname" => "domingo",
            "persona_id" => 3,
            "email" => "chris@gmail.com",
            "password" => bcrypt("12345"),          
            // "dni" => "78002548",
            // "foto" => "hola.jpg",
            // "estado" => 1,
            
        ]);

        User::create([
            "id" => 4,
            // "cod_alu" => null,
            // "rol_id" => 3,
            // "name" => "Lucas",
            // "surname" => "domingo",
            "persona_id" => 4,
            "email" => "vilma@gmail.com",
            "password" => bcrypt("12345"),          
            // "dni" => "78002548",
            // "foto" => "hola.jpg",
            // "estado" => 1,
            
        ]);
    }
}
