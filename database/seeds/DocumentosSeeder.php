<?php

use Illuminate\Database\Seeder;
use App\Documento;

class DocumentosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Documento::create([
        	"id" => 1,
        	"carrera_id" => 5,
        	"curso_id" => 1,
        	"ciclo_id" => 1,
            "periodo_id" => 1,
        	"nombre" => "fff",
            "nombre_original" => "buenas practicas.docx",
        	"archivo" => "1551926648buenas practicas.docx"
        ]);

        Documento::create([
        	"id" => 2,
        	"carrera_id" => 8,
        	"curso_id" => 1,
        	"ciclo_id" => 2,
            "periodo_id" => 1,
        	"nombre" => "ssss",
            "nombre_original" => "1 (1).docx",
        	"archivo" => "15522545741 (1).docx"
        ]);
    }
}
