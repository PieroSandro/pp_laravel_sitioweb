<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //Model::unguard();
        $this->call(PeriodosSeeder::class);
        $this->call(CiclosSeeder::class);
        $this->call(CarrerasSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(PersonasSeeder::class);
        $this->call(CursosSeeder::class);
        $this->call(ProfesorCursoSeeder::class);
        $this->call(SilabusSeeder::class);
        $this->call(EventosSeeder::class);
        $this->call(DocumentosSeeder::class);
        $this->call(TurnosSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(RegistrosSeeder::class);
        $this->call(AsistenciasSeeder::class);
        $this->call(ClavelsSeeder::class);
        //Model::reguard();
        // $this->call(NotasSeeder::class);
    }
}
