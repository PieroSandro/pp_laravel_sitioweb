<?php

use Illuminate\Database\Seeder;
use App\Curso;

class CursosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Curso::create([
        	"id" => 1,
        	"nombre" => "INGLES",
        	"descripcion" => "es un curso teorico de mucha importancia"
        ]);
    }
}
