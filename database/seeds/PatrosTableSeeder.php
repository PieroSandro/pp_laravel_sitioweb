<?php

use Illuminate\Database\Seeder;
use App\Patro;

class PatrosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(Patro::class,20000)->create();
    }
}
