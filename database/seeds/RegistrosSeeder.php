<?php

use Illuminate\Database\Seeder;
use App\Registro;

class RegistrosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Registro::create([
        	"id" => 1,
            "user_id" => 1,
        	"carrera_id" => 4,
        	"curso_id" => 1,
        	"ciclo_id" => 1,
        	"periodo_id" => 1,
            "turno_id" => 1,
        	
        ]);

        Registro::create([
        	"id" => 2,
            "user_id" => 2,
        	"carrera_id" => 7,
        	"curso_id" => 1,
        	"ciclo_id" => 1,
            "periodo_id" => 1,
        	"turno_id" => 2,
        	
        ]);
    }
}
