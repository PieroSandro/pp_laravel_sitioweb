<?php

use Illuminate\Database\Seeder;
use App\Periodo;

class PeriodosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Periodo::create([
        	"id" => 1,
        	"nombre" => "2019-I",
        	"fecha_inicio" => "2019-01-01",
        	"fecha_final" => "2019-07-31"
        ]);

        Periodo::create([
        	"id" => 2,
        	"nombre" => "2019-II",
        	"fecha_inicio" => "2019-08-01",
        	"fecha_final" => "2019-12-31"
        ]);
    }
}
