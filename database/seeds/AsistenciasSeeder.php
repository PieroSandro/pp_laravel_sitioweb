<?php

use Illuminate\Database\Seeder;
use App\Asistencia;

class AsistenciasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Asistencia::create([
        	"id" => 1,
        	"fecha" => "2019-04-08",
            "registro_id" => 1,
            'turno_id' => 1,
        	"estado" => 1
        ]);
    }
}
