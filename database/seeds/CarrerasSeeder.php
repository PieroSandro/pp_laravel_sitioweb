<?php

use Illuminate\Database\Seeder;
use App\Carrera;

class CarrerasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Carrera::create([
        	"id" => 1,
        	"nombre" => "COMPUTACION E INFORMATICA",
        	// "descripcion" => "es una carrera sobre computadoras",
        	// "clave_carrera" => "COMP"
        ]);

        Carrera::create([
        	"id" => 2,
        	"nombre" => "ADMINISTRACION",
        	// "descripcion" => "es un curso teorico de mucha importancia",
        	// "clave_carrera" => "ADMI"
        ]);

        Carrera::create([
            "id" => 3,
            "nombre" => "CONTABILIDAD",
            // "descripcion" => "es un curso teorico de mucha importancia",
            // "clave_carrera" => "CONT"
        ]);

        Carrera::create([
            "id" => 4,
            "nombre" => "LABORATORIO CLINICO",
            // "descripcion" => "es un curso teorico de mucha importancia",
            // "clave_carrera" => "LAB"
        ]);

        Carrera::create([
            "id" => 5,
            "nombre" => "ELECTRONICA",
            // "descripcion" => "es un curso teorico de mucha importancia",
            // "clave_carrera" => "ELE"
        ]);

        Carrera::create([
        	"id" => 6,
        	"nombre" => "ELECTRICIDAD",
        	// "descripcion" => "es un curso teorico de mucha importancia",
        	// "clave_carrera" => "ELD"
        ]);

        Carrera::create([
        	"id" => 7,
        	"nombre" => "MECANICA AUTOMOTRIZ",
        	// "descripcion" => "es un curso teorico de mucha importancia",
        	// "clave_carrera" => "MEC"
        ]);

        Carrera::create([
        	"id" => 8,
        	"nombre" => "MECANICA DE PRODUCCION",
        	// "descripcion" => "es un curso teorico de mucha importancia",
        	// "clave_carrera" => "MEP"
        ]);

        Carrera::create([
        	"id" => 9,
        	"nombre" => "METALURGIA",
        	// "descripcion" => "es un curso teorico de mucha importancia",
        	// "clave_carrera" => "MET"
        ]);

    }
}
