<?php

use Illuminate\Database\Seeder;
use App\Nota;

class NotasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Nota::create([
        	
        	"curso_id" => 1,
            "registro_id" => 1,
        	"nota1" => 15.05,
        	"nota2" => 15.05,
        	"nota3" => 14.05,
        	"nota4" => 17.05,
        	"nota5" => 20.05,
        	"nota6" => 15.05,
        	"nota7" => 15.05,
        	"nota8" => 18.05,
            "pf1" => 17,
            "pf2" => 18,
            "prueba_en" => 0,
            "prueba_sa" => 0,
        	"estado_nota" => 1,
        	"estado_periodo" => 1
        ]);

        // Nota::create([
        	
        // 	"curso_id" => 1,
        // 	"nota1" => 0,
        // 	"nota2" => 0,
        // 	"nota3" => 0,
        // 	"nota4" => 0,
        // 	"nota5" => 0,
        // 	"nota6" => 0,
        // 	"nota7" => 0,
        // 	"nota8" => 0,
        //     "pf1" => 20,
        //     "pf2" => 18,
        // 	"estado_nota" => 1,
        // 	"estado_periodo" => 1
        // ]);
    }
}
