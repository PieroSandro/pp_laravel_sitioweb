<?php

use Illuminate\Database\Seeder;
use App\Evento;

class EventosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Evento::create([
            "curso_id" => 1,
        	"titulo" => "Prueba 1",
        	"fecha_inicio" => date("Y-m-d H:i:s"),
        	"fecha_final" => "2019-03-20 13:42:00",
        	"color" => ""
        ]);
    }
}
