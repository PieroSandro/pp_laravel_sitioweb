<?php

use Illuminate\Database\Seeder;
use App\Persona;

class PersonasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Persona::create([
            "cod_p" => "44444444",
            "nombre" => "Brayan",
            "apellidos" => "Ramoz Lopez",
            "dni" => "74000000",
            "edad" => "18",
            "fecha_n" => "2019-04-09",
            "sexo" => "Masculino",
            "direccion" => "Villasol - Los olivos",
            "rol_id" => "1",
            
        ]);

        Persona::create([
            "cod_p" => "33333333",
            "nombre" => "Juan",
            "apellidos" => "Flores Gomez",
            "dni" => "73000000",
            "edad" => "19",
            "fecha_n" => "2019-04-09",
            "sexo" => "Masculino",
            "direccion" => "Alisos - Los olivos",
            "rol_id" => "1",
            
        ]);

        Persona::create([
            "cod_p" => "88888888",
            "nombre" => "Christian",
            "apellidos" => "Díaz Carlos",
            "dni" => "78000000",
            "edad" => "25",
            "fecha_n" => "2019-04-09",
            "sexo" => "Masculino",
            "direccion" => "Shangrila - P.Piedra",
            "rol_id" => "3",
            
        ]);

        Persona::create([
            "cod_p" => "55555555",
            "nombre" => "Vilma",
            "apellidos" => "Juarez",
            "dni" => "75000000",
            "edad" => "35",
            "fecha_n" => "2019-04-09",
            "sexo" => "Femenino",
            "direccion" => "Belaunde - Comas",
            "rol_id" => "2",
            
        ]);
    }
}
