<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMain2pagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main2pages', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumText('body_es');
            $table->string('foto_es',255);
            $table->mediumText('body_en');
            $table->string('foto_en',255);
            $table->string('si_primero_foto')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main2pages');
    }
}
