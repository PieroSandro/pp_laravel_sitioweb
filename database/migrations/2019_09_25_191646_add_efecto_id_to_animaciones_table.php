<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEfectoIdToAnimacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('animaciones', function (Blueprint $table) {
            //
            $table->unsignedInteger('efecto_id')->after('titulo');
            $table->foreign('efecto_id')->references('id')->on('efectos')->onDelete('cascade');
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('animaciones', function (Blueprint $table) {
            //
            $table->dropColumn('efecto_id');
        });
    }
}
