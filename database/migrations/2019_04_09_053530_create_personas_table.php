<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cod_p',15)->unique();
            $table->string('nombre',30);
            $table->string('apellidos',50);
            $table->string('dni',8);
            $table->string('edad',2);
            $table->date('fecha_n');
            $table->string('sexo',10);
            $table->string('direccion',100);
            $table->string('foto')->default('public/users/default.jpg');
            $table->unsignedInteger('rol_id');
            $table->foreign('rol_id')->references('id')->on('roles')->onDelete('cascade');
            $table->timestamps();
            $table->engine='InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
