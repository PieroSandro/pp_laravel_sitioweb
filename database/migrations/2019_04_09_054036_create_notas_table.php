<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('curso_id');
            $table->foreign('curso_id')->references('id')->on('cursos')->onDelete('cascade');
            $table->unsignedInteger('registro_id');
            $table->foreign('registro_id')->references('id')->on('registros')->onDelete('cascade');
            
            $table->float('nota1', 4,2);
            $table->float('nota2', 4,2);
            $table->float('nota3', 4,2);
            $table->float('nota4', 4,2);
            $table->float('nota5', 4,2);
            $table->float('nota6', 4,2);
            $table->float('nota7', 4,2);
            $table->float('nota8', 4,2);
            $table->integer('pf1');
            $table->integer('pf2');
            $table->float('prueba_en',4,2);
            $table->float('prueba_sa',4,2);
            $table->boolean('estado_nota');
            $table->boolean('estado_periodo');
            $table->timestamps();
            $table->engine ='InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notas');
    }
}
