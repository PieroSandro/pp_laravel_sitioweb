<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasicopreguntasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('basicopreguntas', function (Blueprint $table) {
            $table->increments('id');



            $table->mediumText('question');
            $table->string('option1', 255);
            $table->string('option2', 255);
            $table->string('option3', 255);
            $table->string('option4', 255);
            $table->string('answer', 5);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('basicopreguntas');
    }
}
